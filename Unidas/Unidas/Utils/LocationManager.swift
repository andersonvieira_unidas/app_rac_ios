//
//  LocationManager.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 06/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import CoreLocation

enum LocationManagerError: Error, LocalizedError {
    case deniedAuthorization
    
    var errorDescription: String? {
        return NSLocalizedString("You have not allowed access to your current location", comment: "Location access denied message")
    }
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    private var currentLocationRequestHandler: ((CLLocation?, Error?) -> Void)?
    private var locationManager: CLLocationManager!
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    private func requestAuthorizationIfNeeded() {
        switch CLLocationManager.authorizationStatus() {
        case .denied, .restricted:
            self.currentLocationRequestHandler?(nil, LocationManagerError.deniedAuthorization)
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied, .restricted:
            self.currentLocationRequestHandler?(nil, LocationManagerError.deniedAuthorization)
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            break
        @unknown default:
            break
        }
    }
    
    func requestCurrentLocation(completion: @escaping (CLLocation?, Error?) -> Void) {
        self.currentLocationRequestHandler = completion
        self.requestAuthorizationIfNeeded()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { self.currentLocationRequestHandler = nil }
        self.locationManager.stopUpdatingLocation()
        guard let location = locations.first else { return }
        self.currentLocationRequestHandler?(location, nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        defer { self.currentLocationRequestHandler = nil }
        self.currentLocationRequestHandler?(nil, error)
    }
    
}

