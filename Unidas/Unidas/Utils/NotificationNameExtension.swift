//
//  NotificationNameExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let UNDidMakeReservation = Notification.Name("UNDidMakeReservation")
    static let UNDidPaidReservation = Notification.Name("UNDidPaidReservation")
    static let UNUserDidLogin = Notification.Name("UNUserDidLogin")
    static let UNUserDidLogout = Notification.Name("UNUserDidLogout")
    static let UNDidCanceledReservation = Notification.Name("UNDidCanceledReservation")
    static let UNDidUpdateCurrentUser = Notification.Name("UNDidUpdateCurrentUser")
    static let UNDidContractFinish = Notification.Name("UNDidContractFinish")
    static let UNDidChangeProfileImage = Notification.Name("UNDidChangeProfileImage")
    static let UNDidExtendReturn = Notification.Name("UNDidExtendReturn")
    static let UNDidUnreadMessage = Notification.Name("UNDidUnreadMessage")
    static let UNDidOpenContract = Notification.Name("UNDidOpenContract")
    static let UNDidCloseContract = Notification.Name("UNDidCloseContract")
    static let UNDidUpdateContract = Notification.Name("UNDidUpdateContract")
    static let UNDidKeyboardHidden = Notification.Name("UNDidKeyboardHidden")
}
