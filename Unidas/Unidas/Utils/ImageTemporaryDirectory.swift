//
//  ImageTemporaryDirectory.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum ImageType: Int {
    case profile = 3
    case document = 1
    case signature = 9
    
    var resourceName: String {
        switch self {
        case .profile:
            return "profileImage"
        case .document:
            return "documentImage"
        case .signature:
            return "signatureImage"
        }
    }
}

class ImageTemporaryDirectory {
    private var accountImageService: AccountImageService
    
    init(accountImageService: AccountImageService) {
        self.accountImageService = accountImageService
    }
    
    func addImage(userResponse: UserResponse?, type: ImageType) {
        guard let userResponse = userResponse else { return }

        accountImageService.getImage(documentNumber: userResponse.account.documentNumber, type: type, response: (success: { userImage in
            self.copyBundleResourceToTemporaryDirectory(data: userImage, imageType: type, userResponse: userResponse)
        }, failure: { error in
            
        }, completion: {
        }))
    }
    
    
    private func copyBundleResourceToTemporaryDirectory(data: Data?, imageType: ImageType, userResponse: UserResponse) {
        guard let data = data else { return }

        addFile(data: data, imageType: imageType, userResponse: userResponse, success: { (url) in
            switch imageType {
            case .profile:
                session.currentUser?.account.profileImage = url
                NotificationCenter.default.post(name: .UNDidChangeProfileImage, object: url)
                break
            case .document:
                break
            case .signature:
                break
            }
        }) { (error) in
            print(error)
        }
    }
    
    private func addFile(data: Data, imageType: ImageType, userResponse: UserResponse, success: @escaping (URL) -> Void, failure: @escaping (Error) -> Void) {
        guard let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }

        do {
            let filePath = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        
            for url in filePath {
                try FileManager.default.removeItem(at: url)
            }

            let filename = documentsUrl.appendingPathComponent("\(UUID().uuidString).png")
            try data.write(to: filename)
            
            success(filename)
        }
        catch {
            failure(error)
        }
    }
}

