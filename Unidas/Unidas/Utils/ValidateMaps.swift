//
//  ValidateMaps.swift
//  Unidas
//
//  Created by Felipe Machado on 28/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import MapKit
import EventKit

class ValidateMaps: NSObject {

    static func validateGoogleMaps(coordinate: CLLocationCoordinate2D) -> URL? {
        if let googleMaps = URL(string:"comgooglemaps://") {
            if (UIApplication.shared.canOpenURL(googleMaps)) {
                if let saveGoogleMapsUrl = URL(string:"comgooglemaps://?center=\(coordinate.latitude),\(coordinate.longitude)&zoom=14&views=traffic&q=\(coordinate.latitude),\(coordinate.longitude)") {
                    return saveGoogleMapsUrl
                }
            }
        }
        return nil
    }
    
    static func validateWaze(coordinate: CLLocationCoordinate2D) -> URL? {
        if let waze = URL(string:"waze://") {
            if (UIApplication.shared.canOpenURL(waze)) {
                let wazeUrl = URL(string: "waze://?ll=\(coordinate.latitude),\(coordinate.longitude)&navigate=yes")
                return wazeUrl
            }
        }        
        return nil
    }
}
