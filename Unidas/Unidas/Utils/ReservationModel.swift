//
//  ReservationModel.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 26/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

class ReservationModelObject: NSObject {
    
    static var reservation: Reservation?
    
    static func buildReservation(oldReservation: VehicleReservationsDataView) -> Reservation {
        let openingHours = OpeningHours(informativeText: nil, periods: [OpeningHoursPeriod(day: .weekday, open: "", close: "")])
        
        let hollidays = [Holiday(date: Date(), description: "")]
        let pickupGarage = GarageReservationDetails(date: oldReservation.model.dataHoraRetirada, garage: Garage(name: oldReservation.pickUpStoreAndState ?? "", code: oldReservation.pickUpLocation ?? "", description: "", address: nil, returnTax: 0.0, icon24Hours: false, type: .store, openingHours: openingHours, holidays: hollidays, franchiseCode: nil))
        
        let returnGarage = GarageReservationDetails(date: oldReservation.model.dataHoraDevolucao, garage: Garage(name: oldReservation.returnStoreAndState ?? "", code: oldReservation.returnLocation ?? "", description: "", address: nil, returnTax: 0.0, icon24Hours: false, type: .store, openingHours: openingHours, holidays: hollidays, franchiseCode: nil))
        
        let group = VehMakeModel(name: oldReservation.groupDescription, code: oldReservation.group, vehiclesGroups: oldReservation.vehicleGroup, codeCategory: oldReservation.group)
        
        let reservation = Reservation(id: oldReservation.reserveNumber, number: nil, totalValue: oldReservation.totalValue, prePaymentValue: oldReservation.totalValue, preAuthorizationValue: oldReservation.totalValue, pickUp: pickupGarage, return: returnGarage, group: group, protections: nil, equipments: nil, rateQualifier: oldReservation.rateQualifier, duration: nil, returnTax: nil, extraHourTax: nil, administrativeTaxValue: nil, administrativeTaxPercentValue: nil, voucher: nil, airlineInformation: nil, selectedPaymentOption: .payInAdvance, selectedCasterWeekday: nil, prePaymentDiscountValue: 0, sendSMS: nil, promotionCode: nil, acceptTermLocation: true, acceptTermRefound: nil, pictureURL: oldReservation.picture, express: oldReservation.express)
        return reservation
    }
}
