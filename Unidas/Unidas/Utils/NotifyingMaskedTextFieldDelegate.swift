//
//  NotifyingMaskedTextFieldDelegate.swift
//  Unidas
//
//  Created by Anderson Vieira on 06/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import InputMask

protocol NotifyingMaskedTextFieldDelegateListener: class {
    func onEditingChanged(inTextField: UITextField)
}

class NotifyingMaskedTextFieldDelegate: MaskedTextFieldDelegate {
    weak var editingListener: NotifyingMaskedTextFieldDelegateListener?
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.changeColorBorder(isOn: false)
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    override func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        defer {
            self.editingListener?.onEditingChanged(inTextField: textField)
        }
        return super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
}
