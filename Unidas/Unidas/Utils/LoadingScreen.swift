//
//  LoadingScreen.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 14/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import UIKit

class LoadingScreen: NSObject {
    
    static let loadingView = UIView()
    static let spinnerBackView = EnhancedView()
    static let spinner = UIActivityIndicatorView()
    
    
    static func setLoadingScreen(view: UIView) {
        loadingView.isHidden = false
        loadingView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        loadingView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        
        spinnerBackView.isHidden = false
        spinnerBackView.cornerRadius = 10

        spinnerBackView.frame = CGRect(x: view.frame.width / 2 - 20 , y: view.frame.height / 2 - 20, width: 40, height: 40)
        spinnerBackView.backgroundColor = UIColor.white
   
        // Sets spinner
        spinner.style = UIActivityIndicatorView.Style.gray
        spinner.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
 
        
        view.addSubview(loadingView)
        loadingView.addSubview(spinnerBackView)
        spinnerBackView.addSubview(self.spinner)
        
        spinner.startAnimating()
    }
    
    static func removeLoadingScreen(view: UIView) {
        self.spinner.stopAnimating()
        self.loadingView.isHidden = true
        self.loadingView.removeFromSuperview()
    }
}




