//
//  VersionControl.swift
//  Unidas
//
//  Created by Felipe Machado on 07/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

class VersionControl: NSObject {
    
    static func compareAppVersion() -> Bool {

        let store_version = AppPersistence.getValue(withKey: "ios_version_firebase") ?? ""
        let current_version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
        let versionCompare = current_version.compare(store_version, options: .numeric)
        if versionCompare == .orderedSame {
            return false
        } else if versionCompare == .orderedAscending {
            return true
        } else if versionCompare == .orderedDescending {
            return false
        }
        return false
    }
    
    static func openAppStore() {
        
        if let url = URL(string: "https://itunes.apple.com/br/app/unidas-aluguel-de-carros/id1436660799"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
        }
    }
}
