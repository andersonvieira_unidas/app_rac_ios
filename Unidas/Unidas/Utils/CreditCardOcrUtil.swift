//
//  CreditCardOcrUtil.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation


class CreditCardOcrUtil: NSObject {

    static func extractValidThru(results: [String]) -> Date?{
        let joined = results.joined(separator:"-")
        print(joined)
        let datesString = matches(for: "((0[1-9])|(1[02]))/\\d{2}", in: joined)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/yy"
        
        var dates: [Date] = []
        
        for date in datesString {
            guard let dateFormatted = formatter.date(from: date) else { continue }
            dates.append(dateFormatted)
        }
        
        if dates.isEmpty {
            return nil
        }
        
        for date in datesString {
            guard let dateFormatted = formatter.date(from: date) else { continue }
            dates.append(dateFormatted)
        }
        
        let dateValidThru = dates.sorted(by: { $0.compare($1) == .orderedDescending })
        
        if let date = dateValidThru.first {
            return date
        } else {
            return nil
        }
    }
    
    static func extractCardNumber(results: [String]) -> String?{
        let joined = results.joined(separator: "-")
        print(joined)
        if let cardNumber = matches(for: "\\d{4}-?\\d{4}-?\\d{4}-?\\d{4}", in: joined).first{
            return cardNumber
        }
        if let cardNumberAmex = matches(for: "\\d{4}-?\\d{6}-?\\d{5}", in: joined).first{
            return cardNumberAmex
        }
        if let cardNumberDinners = matches(for: "\\d{4}-?\\d{6}-?\\d{4}", in: joined).first{
            return cardNumberDinners
        }
        if let cardNumberEnroute = matches(for: "\\d{4}-?\\d{7}-?\\d{4}", in: joined).first{
            return cardNumberEnroute
        }
        if let cardNumberVoyager = matches(for: "\\d{5}-?\\d{4}-?\\d{5}-?\\d{1}", in: joined).first{
            return cardNumberVoyager
        }
        return nil
    }
    
    static func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch {
            return []
        }
    }
}
