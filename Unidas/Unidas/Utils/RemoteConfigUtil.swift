//
//  RemoteConfigUtil.swift
//  Unidas
//
//  Created by Anderson Vieira on 06/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import FirebaseRemoteConfig

class RemoteConfigUtil : NSObject {
    
    static var expirationDuration = 21600
    
    static func getRemoteConfig() -> RemoteConfig {
        
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = RemoteConfigSettings()
        
        #if DEVELOPMENT && DEBUG
        expirationDuration = 0
        #endif
        
        remoteConfig.setDefaults(fromPlist: "RemoteConfigDefaults")
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                remoteConfig.activate(completionHandler: nil)
            }
        }
        return remoteConfig
    }
    
    static func getRemoteConfigActivate() -> RemoteConfig {
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.configSettings = RemoteConfigSettings()
        return remoteConfig
    }
    
    static func recoverParameterIntValue(with key: String) -> Int{
        let remoteConfig = getRemoteConfig()
        if let value = remoteConfig[key].stringValue {
            return Int(value) ?? 0
        }
        return 0
    }
    
    static func recoverParameterValue(with key: String) -> String{
        let remoteConfig = getRemoteConfig()
        return remoteConfig[key].stringValue ?? ""
    }
}
