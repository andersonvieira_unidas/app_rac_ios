//
//  MaintenanceInvoker.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 05/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import UIKit

class MaintenanceInvoker: NSObject {
    
    static func presenterMaintenanceController(navigation: UINavigationController) {
        let storyboard = UIStoryboard(name: "Maintenance", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "MaintenanceVC") as? MaintenanceViewController {
            let navBarOnModal: UINavigationController = UINavigationController(rootViewController: viewController)
            navBarOnModal.modalPresentationStyle = .fullScreen
            navigation.present(navBarOnModal, animated: true, completion: nil)
        }
    }
}
