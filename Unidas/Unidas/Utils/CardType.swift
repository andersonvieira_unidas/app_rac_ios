//
//  CardType.swift
//  Unidas
//
//  Created by Mateus Padovani on 18/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

enum CardType: String {
    
    case unknown, amex, masterCard, visa, diners, enroute, discover, jcb, aura, hipercard, elo

    static let allCards = [amex, masterCard, visa, diners, enroute, discover, jcb, aura, hipercard, elo]
    
    var regex : String {
        switch self {
        case .amex:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_amex_regex")
        case .masterCard:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_mastercard_regex")
        case .visa:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_visa_regex")
        case .diners:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_dinners_regex")
        case .enroute:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_enroute_regex")
        case .discover:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_discover_regex")
        case .jcb:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_jcb_regex")
        case .aura:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_aura_regex")
        case .hipercard:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_hipercard_regex")
        case .elo:
            return RemoteConfigUtil.recoverParameterValue(with: "credit_card_elo_regex")
        case .unknown:
            return ""
        }
    }
    
    var brandImage: UIImage {
        switch self {
        case .amex:
            return  #imageLiteral(resourceName: "minicard-brand-amex")
        case .visa:
            return  #imageLiteral(resourceName: "minicard-brand-visa")
        case .masterCard:
            return  #imageLiteral(resourceName: "minicard-brand-mastercard")
        case .diners:
            return  #imageLiteral(resourceName: "minicard-brand-diners")
        case .hipercard:
            return  #imageLiteral(resourceName: "minicard-brand-hiper")
        case .elo:
            return  #imageLiteral(resourceName: "minicard-brand-elo")
        default:
            return  #imageLiteral(resourceName: "card-off")
        }
    }
    
    var authorizeId: AuthorizerId {
        switch self {
        case .visa:
            return .visa
        case .masterCard:
            return .masterCard
        case .amex:
            return .americanExpress
        case .hipercard:
            return .hiper
        case .aura:
            return .aura
        case .diners:
            return .diners
        case .elo:
            return .elo
        default:
            return .unknow
        }
    }
}

class CardUtils {
    static func cardBrandImage(for cardNumber: String) -> UIImage {
        for card in CardType.allCards {
            if (CardUtils.matchesRegex(regex: card.regex, text: cardNumber) == true) {
                return card.brandImage
            }
        }
        
        return  #imageLiteral(resourceName: "card-off")
    }
    
    static func authorizerId(for cardNumber: String) -> AuthorizerId {
        for card in CardType.allCards {
            if (CardUtils.matchesRegex(regex: card.regex, text: cardNumber) == true) {
                return card.authorizeId
            }
        }
        
        return CardType.unknown.authorizeId
    }
    
    static func matchesRegex(regex: String!, text: String!) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [.caseInsensitive])
            let nsString = text as NSString
            let match = regex.firstMatch(in: text, options: [], range: NSMakeRange(0, nsString.length))
            
            return (match != nil)
        } catch {
            return false
        }
    }
    
    static func findByName(for name: String) -> CardType {
        switch name {
        case "AMEX", "AMERICAN EXPRESS", "AMERICANEXPRESS":
            return CardType.amex
        case "MASTERCARD":
            return CardType.masterCard
        case "VISA":
            return CardType.visa
        case "DINERS", "DINERS CLUB", "DINERSCLUB":
            return CardType.diners
        case "ELO":
            return CardType.elo
        case "HIPER":
            return CardType.hipercard
        default:
            return CardType.unknown
        }
    }
    
    static func isValid(expiration: String, for date: Date) -> Bool {
        let middleIndex = expiration.index(expiration.startIndex, offsetBy: 2)
        guard expiration.count == 4,
            let expirationMonth = Int(String(expiration[expiration.startIndex..<middleIndex])),
            let expirationYear = Int(String(expiration[middleIndex..<expiration.endIndex])),
            (1...12).contains(expirationMonth)
            else { return false }
        
        let calendar = Calendar.current
        let currentMonth = calendar.component(.month, from: date)
        let currentYear = calendar.component(.year, from: date) - 2000
        
        if expirationYear > currentYear {
            return true
        } else {
            return expirationYear == currentYear && expirationMonth >= currentMonth
        }
    }
}
