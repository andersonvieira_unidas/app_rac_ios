//
//  ValueFormatter.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/02/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit

struct ValueFormatter {
    static func format(date: Date?, format: String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = format
        dateFormat.locale = Locale(identifier: "pt_BR")
        
        if let date = date {
            return dateFormat.string(from: date)
        }        
        return ""
    }
}
