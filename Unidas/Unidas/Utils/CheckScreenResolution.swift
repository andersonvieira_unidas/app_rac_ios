//
//  CheckScreenResolution.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 27/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

class CheckScreenResolution: NSObject {
    
    static func checkScreenSize() -> Bool {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //print("iPhone 5 or 5S or 5C or SE")
                return true
            case 1334:
                //print("iPhone 6/6S/7/8")
                return true
            case 1920, 2208:
                //print("iPhone 6+/6S+/7+/8+")
                return true
            case 2436:
                //print("iPhone X, Xs")
                return false
            case 1792:
                //print("iPhone Xr")
                return false
            case 2688:
                //print("iPhone Xs Max")
                return false
            default:
                //print("unknown")
                return false
            }
        }
        return false
    }
}
