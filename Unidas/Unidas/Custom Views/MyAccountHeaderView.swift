//
//  MyAccountHeaderView.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol MyAccountHeaderDelegate: class {
    func didTapProfileImage()
}

class MyAccountHeaderView: UIView, NibReusable, MyAccountHeaderViewDelegate {
    @IBOutlet weak var photo: UIImageView!
    weak var delegate: MyAccountHeaderDelegate?
    private var presenter: MyAccountHeaderPresenterDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    private func initialSetup() {
        presenter = MyAccountHeaderPresenter()
        presenter.delegate = self
    }
    
    @IBAction func touchChangeProfileImage(_ sender: UITapGestureRecognizer) {
        delegate?.didTapProfileImage()
    }
    
    func setup(userResponseDataView: UserResponseDataView) {
        print("teste 123")
       // photo.setImage(withURL: userResponseDataView.account.profileImage, placeholderImage: #imageLiteral(resourceName: "photo-replace"))
    }

    
    func showCurrentUserImage(imageUrl: URL?) {
        photo.setImage(withURL: imageUrl, placeholderImage: #imageLiteral(resourceName: "photo-replace"))
    }
}
