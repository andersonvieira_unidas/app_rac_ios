//
//  CheckinPaymentView.swift
//  Unidas
//
//  Created by Anderson Vieira on 01/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit
import Reusable
import FirebaseRemoteConfig

class CheckinPaymentView: UIView, NibLoadable  {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var messageLabel:  UILabel?
    @IBOutlet weak var moneyLabel:  UILabel?
    @IBOutlet weak var backView: UIView!
    
    
    func setup(title: String, message: String?, money: String) {
        guard   let titleLabel = titleLabel,
                let messageLabel = messageLabel,
                let moneyLabel = moneyLabel else { return}
        
        titleLabel.text = title
        moneyLabel.text = money
        
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
    }
    
    func setup(remoteConfigTitleKey: String, remoteConfigMessageKey: String, money: String, showMoney: Bool, backgroundColor: UIColor) {
        guard   let titleLabel = titleLabel,
                let messageLabel = messageLabel,
                let moneyLabel = moneyLabel else { return}
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        
        let title = remoteConfig[remoteConfigTitleKey].stringValue
        let message = remoteConfig[remoteConfigMessageKey].stringValue
        var descriptionPre = message
 
        titleLabel.text = title
        
        if showMoney {
            moneyLabel.text = money
        }else{
            if (message?.contains("#v#"))! {
                descriptionPre = descriptionPre?.replacingOccurrences(of: "#v#", with: money)
            }
            moneyLabel.text = ""
        }
        
        if message != nil {
            messageLabel.text = descriptionPre
        } else {
            messageLabel.isHidden = true
        }        
        backView.backgroundColor = backgroundColor
    }
    
    func setup(title: String, remoteConfigMessageKey: String, money: String) {
        guard   let titleLabel = titleLabel,
                let messageLabel = messageLabel,
                let moneyLabel = moneyLabel else { return}
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        
        titleLabel.text = title
        moneyLabel.text = money
        
        let message = remoteConfig[remoteConfigMessageKey].stringValue
        
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
    }
}
