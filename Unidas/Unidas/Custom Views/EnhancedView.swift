//
//  EnhancedView.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/02/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class EnhancedView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            applyCornerRadius()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var roundTopLeftCorner: Bool = true {
        didSet {
            applyCornerRadius()
        }
    }
    
    @IBInspectable var roundTopRightCorner: Bool = true {
        didSet {
            applyCornerRadius()
        }
    }
    
    @IBInspectable var roundBottomLeftCorner: Bool = true {
        didSet {
            applyCornerRadius()
        }
    }
    
    
    @IBInspectable var roundBottomRightCorner: Bool = true {
        didSet {
            applyCornerRadius()
        }
    }
    
    var rectCorner: UIRectCorner {
        var rectCorner = UIRectCorner(rawValue: 0)
        if roundTopLeftCorner { rectCorner.insert(.topLeft) }
        if roundTopRightCorner { rectCorner.insert(.topRight) }
        if roundBottomLeftCorner { rectCorner.insert(.bottomLeft) }
        if roundBottomRightCorner { rectCorner.insert(.bottomRight) }
        return rectCorner
    }
    
    var cornerMask: CACornerMask {
        return CACornerMask(rawValue: rectCorner.rawValue)
    }
    
    func applyCornerRadius() {
        if #available(iOS 11, *) {
            layer.cornerRadius = cornerRadius
            layer.maskedCorners = cornerMask
        } else {
            let mask = CAShapeLayer()
            mask.bounds = frame
            mask.position = center
            mask.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: rectCorner, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
            layer.mask = mask
        }
    }
    
   
}
