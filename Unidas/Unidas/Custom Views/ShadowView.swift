//
//  ShadowView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class ShadowView: EnhancedView {
    
    @IBInspectable var shadowColor: UIColor? = nil {
        didSet {
            self.layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = .zero {
        didSet {
            self.layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if shadowRadius > 0 {
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        }
    }
}
