//
//  LogoNavigationController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class LogoNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func show(_ vc: UIViewController, sender: Any?) {
        super.show(vc, sender: sender)
        setupLogo(in: vc)
    }
    
    private func setupLogo(in viewController: UIViewController) {
        if viewController.navigationItem.titleView == nil && viewController.navigationItem.title == nil {
            viewController.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        }
    }
    
    override func viewWillLayoutSubviews() {
        if let visibleViewController = visibleViewController {
            setupLogo(in: visibleViewController)
        }
    }
}
