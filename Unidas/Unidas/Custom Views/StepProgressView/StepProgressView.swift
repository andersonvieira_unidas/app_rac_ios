//
//  StepProgressView.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class StepProgressView: UIView, NibLoadable {
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

