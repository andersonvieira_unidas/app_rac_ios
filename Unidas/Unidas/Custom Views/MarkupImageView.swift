//
//  MarkupImageView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol MarkupImageViewDelegate: class {
    func markupImageView(_ markupImageView: MarkupImageView, didAddMarkupAt location: CGPoint)
}

protocol MarkupImageViewDataSource: class {
    func titleForNewMarkupItem(in markupImageView: MarkupImageView) -> String
}

class MarkupImageView: UIImageView {

    private lazy var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addMarkup(sender:)))
    private lazy var impactGenerator = UIImpactFeedbackGenerator(style: .medium)
    
    weak var delegate: MarkupImageViewDelegate?
    weak var dataSource: MarkupImageViewDataSource?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    override init(image: UIImage?) {
        super.init(image: image)
        setup()
    }
    
    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        setup()
    }
    
    private func setup() {
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func addMarkup(sender: UITapGestureRecognizer) {
        insertMarkupItem(location: sender.location(in: self))
    }
    
    
    func insertMarkupItem(location: CGPoint) {
        let label = self.label(for: dataSource?.titleForNewMarkupItem(in: self))
        label.center = location
        delegate?.markupImageView(self, didAddMarkupAt: location)
        addSubview(label)
        label.layer.add(animationForNewMarkupItem(), forKey: "newMarkupItem")
        
        impactGenerator.prepare()
        impactGenerator.impactOccurred()
    }
    
    func deleteMarkupItem(with title: String) {
        guard let label = subviews.first(where: { (view) -> Bool in
            guard let label = view as? UILabel else { return false }
            return label.text == title
        }) else { return }
        let animator = UIViewPropertyAnimator(duration: 0.1, curve: .easeInOut, animations: {
            label.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        })
        animator.addCompletion { _ in
            label.removeFromSuperview()
        }
        animator.startAnimation()
    }
    
    func animationForNewMarkupItem() -> CAAnimation {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 1.0
        scaleAnimation.toValue = 1.4
        scaleAnimation.autoreverses = true
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        scaleAnimation.duration = 0.1
        scaleAnimation.isRemovedOnCompletion = true
        return scaleAnimation
    }
    
    func label(for text: String?) -> UILabel {
        let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 24, height: 24)))
        label.text = text
        label.font = .systemFont(ofSize: 13)
        label.textColor = #colorLiteral(red: 0, green: 0.6549019608, blue: 1, alpha: 1)
        label.backgroundColor = .white
        label.layer.borderColor = #colorLiteral(red: 0, green: 0.6549019608, blue: 1, alpha: 1).cgColor
        label.layer.borderWidth = 1.0
        label.textAlignment = .center
        label.clipsToBounds = true
        label.layer.cornerRadius = 12
        return label
    }
}

extension Array where Element: MarkupImageView {
    func deleteMarkupItem(with title: String) {
        forEach { $0.deleteMarkupItem(with: title) }
    }
}
