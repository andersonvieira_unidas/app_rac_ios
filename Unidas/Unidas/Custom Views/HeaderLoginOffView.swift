//
//  MoreHeaderLoginOff.swift
//  Unidas
//
//  Created by Mateus Padovani on 24/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol HeaderLoginOffViewDelegate: class {
    func didTapActionButton(_ headerLoginOffView: HeaderLoginOffView)
}

class HeaderLoginOffView: UIView, NibLoadable {

    weak var delegate: HeaderLoginOffViewDelegate?
 
    @IBAction func didTapActionButton(_ sender: UIButton) {
        delegate?.didTapActionButton(self)
    }
}
