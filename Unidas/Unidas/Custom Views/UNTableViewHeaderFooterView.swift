//
//  UNTableViewHeaderFooterView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class UNTableViewHeaderFooterView: UITableViewHeaderFooterView, NibReusable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedLabel: UILabel!
    @IBOutlet weak var buttonExpandClose: UIButton!
    
}
