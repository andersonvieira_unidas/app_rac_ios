//
//  EnhancedImageView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class EnhancedImageView: UIImageView {

    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}
