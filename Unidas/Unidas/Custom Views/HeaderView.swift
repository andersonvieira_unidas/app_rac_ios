//
//  HeaderView.swift
//  Unidas
//
//  Created by Anderson Vieira on 29/07/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit
import Reusable
import FirebaseRemoteConfig

class HeaderView: UIView, NibLoadable {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var messageLabel:  UILabel?
    
    func setup(title: String, message: String?) {
        guard let titleLabel = titleLabel, let messageLabel = messageLabel else { return}
        titleLabel.text = title
        
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
    }
    
    func setup(remoteConfigTitleKey: String, remoteConfigMessageKey: String) {
        guard let titleLabel = titleLabel, let messageLabel = messageLabel else { return}
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()

        let title = remoteConfig[remoteConfigTitleKey].stringValue
        let message = remoteConfig[remoteConfigMessageKey].stringValue
        
       
        titleLabel.text = title
        
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
    }
    
    func setup(title: String, remoteConfigMessageKey: String) {
        guard let titleLabel = titleLabel, let messageLabel = messageLabel else { return}
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        
        titleLabel.text = title
        let message = remoteConfig[remoteConfigMessageKey].stringValue
        
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.isHidden = true
        }
    }
}
