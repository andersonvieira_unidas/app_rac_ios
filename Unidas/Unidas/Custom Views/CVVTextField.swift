//
//  CVVTextField.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 9/20/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class CVVTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
    }
    
    @objc func textDidChange(_ sender: UITextField) {
        if let text = sender.text, text.count > 4 {
            sender.text = String(text[text.startIndex..<text.index(text.startIndex, offsetBy: 4)])
        }
    }
    
}
