//
//  TransactionPaymentView.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 31/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class TransactionPaymentView: UIView, NibReusable {
    
    @IBOutlet weak var squareView: EnhancedView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusDescriptionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var creditCardImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var creditCardView: EnhancedView!
    @IBOutlet weak var statusDescriptionLabelHeight: NSLayoutConstraint?
    
    var transactionPayment: TransactionPayment?
    var type: TransactionPaymentType?
    var express: Bool = false
    
    func setup(type: TransactionPaymentType, transactionPayment: TransactionPayment, express: Bool? = false){
        
        self.transactionPayment = transactionPayment
        self.type = type
        self.express = express ?? false
        
        if transactionPayment.status != nil {
            setupDetails()
        } else if transactionPayment.statusConfirmation != nil {
            setupReserveConfirmation()
        } else {
            setupEmptyTransaction()
        }
    }
    
    private func setupEmptyTransaction(){
        let status = transactionPayment?.status ?? .notPay
        
        statusLabel.text = type?.title
        statusDescriptionLabel.text = status.text
        
        statusDescriptionLabel.textColor = status.colorText
        squareView.borderColor = status.colorText
        statusDescriptionLabel.numberOfLines = 2
        statusDescriptionLabelHeight?.constant = 35
        creditCardView.isHidden = true
        valueLabel.text = transactionPayment?.paymentValue
    
        if type == .prePayment && express {
            statusDescriptionLabel.text = NSLocalizedString("Payment Status - Presencial Transaction", comment: "")
        }
    }
    
    private func setupDetails(){
        
        let status = transactionPayment?.status ?? .notPay
        
        statusLabel.text = type?.title
        statusDescriptionLabel.text = status.text
        statusDescriptionLabel.textColor = status.colorText
        statusDescriptionLabel.sizeToFit()
        
        squareView.borderColor = status.colorText
        
        if status == .waitingConfirmation || status == .notPay {
            statusDescriptionLabel.numberOfLines = 2
            statusDescriptionLabelHeight?.constant = 35
            creditCardView.isHidden = true
            valueLabel.text = transactionPayment?.paymentValue
        } else {
            creditCardImageView.image = transactionPayment?.brandImage
            cardNumberLabel.text = transactionPayment?.fourLastNumberCard
            valueLabel.text = transactionPayment?.paymentValue
        }
        
        if type == .prePayment && express {
            
            statusDescriptionLabel.text = NSLocalizedString("Payment Status - Presencial Transaction", comment: "")
        }
    }
    
    private func setupReserveConfirmation(){
        let status = transactionPayment?.statusConfirmation ?? .noPayment

        statusLabel.text = type?.title
        statusDescriptionLabel.text = status.text
        
        statusDescriptionLabel.textColor = status.colorText
        squareView.borderColor = status.colorText
        
        if status == .processing || status == .noPayment {
            statusDescriptionLabel.numberOfLines = 2
            creditCardView.isHidden = true
            valueLabel.text = transactionPayment?.paymentValue
        } else {
            creditCardImageView.image = transactionPayment?.brandImage
            cardNumberLabel.text = transactionPayment?.fourLastNumberCard
            valueLabel.text = transactionPayment?.paymentValue
        }
        
        if type == .prePayment && express {
            statusDescriptionLabel.text = NSLocalizedString("Payment Status - Presencial Transaction", comment: "")
        }
        
    }
    
    
    func update(valuePayment value: String){
        valueLabel.text = value
    }
    
    override func layoutSubviews() {
        self.frame = (self.superview?.bounds)!
    }
}

struct TransactionPayment {
    let fourLastNumberCard: String?
    let brandImage: UIImage?
    let paymentValue: String?
    let status: PaymentStatus?
    let statusConfirmation: ReservationObjectPaymentStatus?
}

enum TransactionPaymentType {
    case payment
    case prePayment
    
    var title: String {
        switch self {
        case .payment:
            return NSLocalizedString("Payment Type", comment: "")
        case .prePayment:
            return NSLocalizedString("Pre Authorization Type", comment: "")
        }
    }
}
