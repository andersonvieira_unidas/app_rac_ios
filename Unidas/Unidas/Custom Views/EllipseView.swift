//
//  EllipseView.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class DashedCircleView: UIView {
    private var shapeLayer: CAShapeLayer = {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
        shapeLayer.fillColor = UIColor.clear.cgColor
        return shapeLayer
    }()

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        updatePath()
    }
}

private extension DashedCircleView {
    func configure() {
        layer.addSublayer(shapeLayer)
    }

    func updatePath() {
        let rect = bounds.insetBy(dx: shapeLayer.lineWidth / 2, dy: shapeLayer.lineWidth / 2)
        let radius = min(rect.width, rect.height) / 2
        let center = CGPoint(x: rect.midX, y: rect.midY)

        let path = UIBezierPath()
        let count = 30
        let relativeDashLength: CGFloat = 0.25
        let increment: CGFloat = .pi * 2 / CGFloat(count)
        
        
        
        let w = bounds.size.width, h = bounds.size.height
        var from: CGPoint, to: CGPoint, d: CGFloat
        
            from = CGPoint(x:w/2,y:0)
            to = CGPoint(x:w/2,y:h)
            d = w
        
        path.lineWidth = 0
        path.dottedLine(from: from, to: to, radius: d/2)
        path.fill()
        path.stroke()
        

        for i in 0 ..< count {
            let startAngle = increment * CGFloat(i)
            let endAngle = startAngle + relativeDashLength * increment
            path.move(to: CGPoint(x: center.x + radius * cos(startAngle),
                                  y: center.y + radius * sin(startAngle)))
            path.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        }
        shapeLayer.path = path.cgPath
    }
    
}
