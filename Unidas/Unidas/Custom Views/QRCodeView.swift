//
//  QRCodeView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

@IBDesignable
class QRCodeView: UIView, NibLoadable {
    
    @IBInspectable var message: String? = nil {
        didSet {
            drawQRCode(with: frame.size)
        }
    }
    
    var imageView: UIImageView
    
    required init?(coder aDecoder: NSCoder) {
        imageView = UIImageView()
        super.init(coder: aDecoder)
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        imageView = UIImageView(frame: frame)
        super.init(frame: frame)
        setupConstraints()
    }
    
    init(frame: CGRect = .zero, message: String) {
        imageView = UIImageView(frame: frame)
        super.init(frame: frame)
        setupConstraints()
        self.message = message
    }
    
    override func draw(_ rect: CGRect) {
        drawQRCode(with: rect.size)
    }
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        drawQRCode(with: frame.size)
    }
    
    private func setupConstraints() {
        addSubview(imageView)
        let topConstraint = topAnchor.constraint(equalTo: imageView.topAnchor)
        let trailingConstraint = trailingAnchor.constraint(equalTo: imageView.trailingAnchor)
        let bottomConstraint = bottomAnchor.constraint(equalTo: imageView.bottomAnchor)
        let leadingConstraint = leadingAnchor.constraint(equalTo: imageView.leadingAnchor)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints([topConstraint, trailingConstraint, bottomConstraint, leadingConstraint])
    }
    
    private func drawQRCode(with size: CGSize) {
        guard let message = message else { return }
        imageView.image = drawQRCodeImage(from: message, with: size)
    }
    
    private func drawQRCodeImage(from text: String, with size: CGSize) -> UIImage? {
        guard let data = text.data(using: .isoLatin1, allowLossyConversion: false),
            let filter = CIFilter(name: "CIQRCodeGenerator"),
            let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
        
        let tintColor = self.tintColor ?? .black
        let backgroundColor = self.backgroundColor ?? .white
        
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("Q", forKey: "inputCorrectionLevel")
        colorFilter.setValue(filter.outputImage, forKey: "inputImage")
        colorFilter.setValue(CIColor(color: tintColor), forKey: "inputColor0")
        colorFilter.setValue(CIColor(color: backgroundColor), forKey: "inputColor1")
        
        guard let ciImage = colorFilter.outputImage else { return nil }
        let scaleX = size.width / ciImage.extent.width
        let scaleY = size.height / ciImage.extent.height
        let scaledImage = ciImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        let image = UIImage(ciImage: scaledImage)
        return image
    }
    
}

