//
//  HomeDefaultView.swift
//  Unidas
//
//  Created by Felipe Machado on 26/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit
import Reusable

class HomeDefaultViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var cellType = HomeCellType.Reservations
    
    func setupView(cell: HomeCellObject) {
        cellType = cell.type!
        backView.layer.cornerRadius = 20
        titleLabel.text = cell.title
        descriptionLabel.text = cell.description
        iconImage.image = cell.image
    }
}
