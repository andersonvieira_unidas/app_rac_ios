//
//  MapSnapshotView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import MapKit

class MapSnapshotView: UIView {

    private(set) lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    private(set) lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 15, weight: .regular)
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [activityIndicatorView, messageLabel])
        stackView.axis = .vertical
        stackView.spacing = 8.0
        stackView.alignment = .center
        stackView.distribution = .fill
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: nil)
        imageView.contentMode = .center
        return imageView
    }()
    
    private var snapshoter: MKMapSnapshotter?
    
    private var oldBounds: CGRect?
    
    // MARK: - Parameters
    
    var coordinate: CLLocationCoordinate2D? = nil {
        didSet {
            if coordinate != oldValue {
                generateSnapshot()
            }
        }
    }
    
    var annotationViews: [MKAnnotationView] = []
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = bounds
        if bounds != oldBounds {
            oldBounds = bounds
            generateSnapshot()
        }
    }
    
    // MARK: - Setup
    
    private func setupView() {
        backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        addSubview(stackView)
        addSubview(imageView)
        setupStackViewConstraints()
    }
    
    private func setupStackViewConstraints() {
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        stackView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: 8.0).isActive = true
        bottomAnchor.constraint(greaterThanOrEqualTo: stackView.bottomAnchor, constant: 8.0).isActive = true
        stackView.leadingAnchor.constraint(greaterThanOrEqualTo: leadingAnchor, constant: 8.0).isActive = true
        trailingAnchor.constraint(greaterThanOrEqualTo: stackView.trailingAnchor, constant: 8.0).isActive = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    // MARK: - Snapshot
    
    private func generateSnapshot() {
        guard let coordinate = coordinate else {
            imageView.image = nil
            messageLabel.text = NSLocalizedString("Nothing to show", comment: "Nothing to show label")
            messageLabel.sizeToFit()
            return
        }
        let snapshotOptions = MKMapSnapshotter.Options()
        let camera = MKMapCamera(lookingAtCenter: coordinate, fromEyeCoordinate: coordinate, eyeAltitude: 1500.0)
        snapshotOptions.size = bounds.size
        snapshotOptions.scale = UIScreen.main.scale
        snapshotOptions.camera = camera
        
        let snapshoter = MKMapSnapshotter(options: snapshotOptions)
        startLoading()
        self.snapshoter?.cancel()
        snapshoter.start(with: DispatchQueue.global()) { [weak self] (snapshot, error) in
            DispatchQueue.main.async { [weak self] in
                self?.stopLoading()
                guard let snapshot = snapshot else {
                    if let error = error {
                        self?.present(error: error)
                    }
                    self?.imageView.image = nil
                    return
                }
                self?.imageView.image = snapshot.image
                self?.imageView.subviews.forEach { $0.removeFromSuperview() }
                self?.annotationViews.forEach { annotationView in
                    guard let coordinate = annotationView.annotation?.coordinate else { return }
                    let point = snapshot.point(for: coordinate)
                    guard let imageView = self?.imageView, imageView.frame.contains(point) else { return }
                    imageView.addSubview(annotationView)
                    annotationView.center = point
                    annotationView.centerOffset = CGPoint(x: 0.0, y: -14.5)
                }
            }
        }
        self.snapshoter = snapshoter
    }
    
    // MARK: - State management
    
    private func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    private func stopLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    private func present(error: Error) {
        messageLabel.text = error.localizedDescription
        messageLabel.sizeToFit()
    }

}
