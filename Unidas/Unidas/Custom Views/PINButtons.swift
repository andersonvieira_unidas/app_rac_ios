//
//  PINButtons.swift
//  Unidas
//
//  Created by Mateus Padovani on 06/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class PINButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        self.adjustsImageWhenHighlighted = false

        self.setBackgroundColor(color: .clear, forState: .normal)
        self.setBackgroundColor(color: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), forState: .highlighted)
        
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(UIColor(red: 51.0/255.0, green: 76.0/255.0, blue: 139.0/255.0, alpha: 1.0), for: .highlighted)
        
        
        if let image = self.image(for: .normal)?.withRenderingMode(.alwaysOriginal) {
            self.setImage(image, for: .highlighted)
        }
    }
}
