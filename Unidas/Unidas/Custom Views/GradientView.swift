//
//  GradientView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 16/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var firstColor: UIColor = #colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1) {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable var secondColor: UIColor = #colorLiteral(red: 0.007843137255, green: 0.431372549, blue: 0.6549019608, alpha: 0.3) {
        didSet {
            updateGradient()
        }
    }
    
    @IBInspectable var degrees: CGFloat = 0.0 {
        didSet {
            updateGradient()
        }
    }
    
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        updateGradient()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        updateGradient()
    }
    
    func updateGradient() {
        let gradientLayer = layer as? CAGradientLayer
        gradientLayer?.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer?.transform = CATransform3DMakeRotation(degreesToRadians(degrees), 0.0, 0.0, 1.0)
    }
    
    private func degreesToRadians(_ degrees: CGFloat) -> CGFloat {
        return degrees * .pi / 180.0
    }
}
