//
//  PaymentHeaderView.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 06/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class PaymentHeaderView: UICollectionReusableView {
        
    override init(frame:CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .red
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
