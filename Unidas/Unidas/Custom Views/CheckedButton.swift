//
//  CheckedButton.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class CheckedButton: UIButton {
    var backgroundCheckImage: UIImage? = nil
    var backgroundNotCheckImage: UIImage? = nil
    
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var checkedBackgroundImage: UIImage? = nil {
        didSet {
            self.backgroundCheckImage = checkedBackgroundImage
            isChecked(isChecked)
        }
    }
    
    @IBInspectable var noCheckedBackgroundImage: UIImage? = nil {
        didSet {
            self.backgroundNotCheckImage = noCheckedBackgroundImage
            isChecked(isChecked)
        }
    }
    
    @IBInspectable var isChecked: Bool = false {
        didSet {
            isChecked(isChecked)
        }
    }
    
    private func isChecked(_ checked: Bool) {
        (checked) ? setBackgroundImage(backgroundCheckImage, for: .normal) : setBackgroundImage(backgroundNotCheckImage, for: .normal)
    }
}
