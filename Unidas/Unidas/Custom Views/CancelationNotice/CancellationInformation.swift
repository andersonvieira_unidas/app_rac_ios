//
//  CancellationInformation.swift
//  Unidas
//
//  Created by Felipe Machado on 30/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

public struct CancellationStep {
    let percentage: String?
    let description: String?
    var order: Int?
    
    public init(percentage: String? = nil,
                description: String? = nil,
                order: Int? = nil
) {
        self.percentage = percentage
        self.description = description
        self.order = order
    }
}

struct Cancellation: Decodable {
    let percentage: String?
    let description: String?
    var order: Int?
    
    enum CodingKeys: String, CodingKey {
        case percentage
        case description
        case order
    }
}

