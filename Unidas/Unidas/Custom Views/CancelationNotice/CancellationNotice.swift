//
//  CancelationNotice.swift
//  Unidas
//
//  Created by Felipe Machado on 27/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol CancellationNoticeDelegate {
    func openUrl(with url: URL)
}

class CancellationNotice: UIView, UITextViewDelegate, NibOwnerLoadable  {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var refoundTitleLabel: UILabel!
    @IBOutlet weak var refoundTitleBLabel: UILabel!
    @IBOutlet weak var percentageOneLabel: UILabel!
    @IBOutlet weak var percentageTwoLabel: UILabel!
    @IBOutlet weak var percentageThreeLabel: UILabel!
    @IBOutlet weak var descriptionOneLabel: UILabel!
    @IBOutlet weak var descriptionTwoLabel: UILabel!
    @IBOutlet weak var descriptionThreeLabel: UILabel!
    @IBOutlet weak var refoundPolicyLabel: UITextView!
    @IBOutlet weak var borderView: EnhancedView!
    
    var delegate: CancellationNoticeDelegate?
    var cancellation: [CancellationStep] = []
    var policyURL = String()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
            commonInit()
    }
  
    func loadData(key: String, url: String) {
        cancellation = loadData(key: key)
        policyURL = url
    }

    private func commonInit() {
        loadNibContent()
        cancellation = loadData(key: "refund_info_policy")
    }
    
    func setupView() {
        
        refoundPolicyLabel.textContainerInset = .zero
        refoundPolicyLabel.textContainer.lineFragmentPadding = 0.0
        
        for item in cancellation {
            switch item.order {
            case 1:
                let str = "\(item.percentage ?? "") \(item.description ?? "")"
                percentageOneLabel.attributedText = setBoldToText(string: str)
            case 2:
                let str = "\(item.percentage ?? "") \(item.description ?? "")"
                percentageTwoLabel.attributedText = setBoldToText(string: str)
            case 3:
                let str = "\(item.percentage ?? "") \(item.description ?? "")"
                percentageThreeLabel.attributedText = setBoldToText(string: str)
            default:
                break
            }
        }
        refoundTitleLabel.text = NSLocalizedString("Cancelation title", comment: "")
    }
    
    func setBoldToText(string: String) -> NSMutableAttributedString {
        
        let fontBold = UIFont.medium(ofSize: 12)
        let font = UIFont.regular(ofSize: 12)
        
        let boldRanges = find(inText: string, pattern: "#b#")
        let colorAttribute: [NSAttributedString.Key : Any] = [.font: fontBold]
        
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font : font])
        
        boldRanges.reversed().forEach { (aResult) in
            let subTextNSRange = aResult.range(at: 1)
            guard let subTextRange = Range(subTextNSRange, in: attributedString.string) else { return }
            let replacementString = String(attributedString.string[subTextRange])
            let replacementAttributedString = NSAttributedString(string: replacementString, attributes: colorAttribute)
            attributedString.replaceCharacters(in: aResult.range, with: replacementAttributedString)
        }
        return attributedString
    }
    
    func find(inText text: String, pattern: String) -> [NSTextCheckingResult] {
        let regex = try! NSRegularExpression(pattern:pattern+"(.*?)"+pattern, options: [])
        return regex.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.utf16.count))
    }
    
    func setUrl(urlPolicy: URL){
        let refundPolicyText = NSLocalizedString("Returning Policy", comment: "")
        let refundPolicyLinkText = NSLocalizedString("Returning Policy", comment: "")
        let colorFont = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
        
        let rangeLink = (refundPolicyText as NSString).range(of: refundPolicyLinkText)
        let policyText = NSMutableAttributedString(string: refundPolicyText, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 14), NSAttributedString.Key.foregroundColor: colorFont])
        policyText.addAttribute(.link, value: urlPolicy, range: rangeLink)
        policyText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.font, value: UIFont.bold(ofSize: 14), range: rangeLink)

        refoundPolicyLabel.attributedText = policyText
        refoundPolicyLabel.textAlignment = .left
        refoundPolicyLabel.delegate = self
        refoundPolicyLabel.isEditable = false
    }
    
    private func loadData(key: String) -> [CancellationStep]{
        
        let jsonString = RemoteConfigUtil.recoverParameterValue(with: key)
        
        let decoder = JSONDecoder()
        do {
            let json = jsonString.data(using: .utf8)!
            var cancellations = try decoder.decode([Cancellation].self, from: json)
            cancellations = cancellations.sorted { $0.order ?? 0 < $1.order ?? 0 }
            
            for item in cancellations {
                cancellation.append(CancellationStep(percentage: item.percentage, description: item.description, order: item.order))
            }
            setupView()
        } catch {
            print(error.localizedDescription)
        }
        return cancellation
    }
    
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        delegate?.openUrl(with: url)
        return false
    }
}
