//
//  CreditCardPrePaymentView.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol CreditCardPrePaymentDelegate: class {
    func present(error: Error)
}

class CreditCardPrePaymentView: UIView, NibOwnerLoadable, UITextFieldDelegate, CreditCardPrePaymentViewDelegate {
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var holderNameLabel: UILabel!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet var cardView: EnhancedView!
    @IBOutlet weak var validTitleLabel: UILabel!
    @IBOutlet weak var toolTipMessageView: EnhancedView!
    @IBOutlet weak var cvvToolTipView: UIView!
    
    private var presenter: CreditCardPrePaymentPresenterDelegate!
    weak var delegate: CreditCardPrePaymentDelegate?
    
    var creditCardPrePaymentDataView: CreditCardPrePaymentDataView? {
        return presenter.creditCardPrePaymentDataView
    }
    
    lazy var cvvToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(cvvToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    private func initialSetup() {
        presenter = CreditCardPrePaymentPresenter()
        presenter.delegate = self

        loadNibContent()
        setupTextField()
        setupToolBoxMessageView()
    }
    
    private func setupTextField() {
        cvvTextField.delegate = self
        cvvTextField.inputAccessoryView = cvvToolbar
    }
    
    private func setupToolBoxMessageView() {
        toolTipMessageView.alpha = 0
    }
    
    func setCard(at listCardDataView: ListCardDataView) {
        presenter.setCard(at: listCardDataView)
    }
    
    func setAmount(at amount: Double) {
        presenter.setAmount(of: amount)
    }
    
    @objc func cvvToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        cvvTextField.resignFirstResponder()
    }
    
    // MARK: - CreditCardPrePaymentViewDelegate
    
    func applyCardDesigne() {
        self.layer.cornerRadius = 15
        self.backgroundColor = #colorLiteral(red: 0.8705146313, green: 0.8706371188, blue: 0.8704759479, alpha: 1)
    }
    
    func applyValidCard() {
        cvvToolTipView.backgroundColor = .clear
        cvvToolTipView.layer.cornerRadius = 5
        cvvToolTipView.layer.borderColor = UIColor.gray.cgColor
        cvvToolTipView.layer.borderWidth = 0
        cvvToolTipView.layer.masksToBounds = true
        cardView.cornerRadius = 15
        cardView.borderWidth = 1
        cardView.borderColor = UIColor.clear
        validTitleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        expirationDateLabel.textColor = #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
    }
    
    func applyInvalidCards() {
        cardView.borderColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        validTitleLabel.textColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        expirationDateLabel.textColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
    }
    
    var securityCode: String? {
        return cvvTextField.text
    }
    
    func present(error: Error) {
        delegate?.present(error: error)
    }
    
    func showCard(listCardDataView: ListCardDataView) {
        cardNumberLabel.text = listCardDataView.cardNumberShortFormatted
        expirationDateLabel.text = "\(NSLocalizedString("Credit Card Vality", comment: "")) \(listCardDataView.cardDate)" 
        holderNameLabel.text = listCardDataView.holder.uppercased()
        brandImage.image = listCardDataView.brandImage
        cvvTextField.text = ""
        applyCardDesigne()
    }

    // MARK: - TextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        return newLength <= 4
    }
    
    func checkToolTipButtonState() {
        if toolTipMessageView.alpha == 0 {
            
            toolTipMessageView.alpha = 0
            
            UIView.animate(withDuration: 0.5) {
                self.toolTipMessageView.alpha = 1.0
            }
            
        }else{
            UIView.animate(withDuration: 0.5) {
                self.toolTipMessageView.alpha = 0
            }
        }
    }
    @IBAction func tapShowTooltipMessage(_ sender: Any) {
        checkToolTipButtonState()
    }
    @IBAction func closeToolTipMessageView() {
        checkToolTipButtonState()
    }
    
}
