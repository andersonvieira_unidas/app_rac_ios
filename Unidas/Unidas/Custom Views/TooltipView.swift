//
//  TooltipView.swift
//  Unidas
//
//  Created by Anderson Vieira on 09/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable


class TooltipView: UIView, NibLoadable {
    @IBOutlet weak var descriptionLabel: UILabel?
    
    func setup(description: String) {
        descriptionLabel?.text = description
    }
    
    @IBAction func dismiss() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0.0
        }, completion: { (Bool) in
            self.removeFromSuperview()
        })
    }
}
