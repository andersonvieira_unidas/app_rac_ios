//
//  CreditCardReservePaymentView.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol CreditCardReservePaymentDelegate: class {
    func present(error: Error)
}

class CreditCardReservePaymentView: UIView, NibOwnerLoadable, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, CreditCardReservePaymentViewDelegate {
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var paymentOptionsTextField: UITextField!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet var cardView: EnhancedView!
    @IBOutlet weak var installmentsTitle: UILabel!
    @IBOutlet weak var toolTipButton: UnidasButton!
    @IBOutlet weak var toolTipMessageView: EnhancedView!
    @IBOutlet weak var cvvToolTipView: UIView!
    @IBOutlet weak var arrowDownImage: UIImageView!
    
    weak var delegate: CreditCardReservePaymentDelegate?

    private var presenter: CreditCardReservePaymentPresenterDelegate!
    
    var creditCardReservePaymentDataView: CreditCardReservePaymentDataView? {
        return presenter.creditCardReservePayment
    }
    
    var hasCard: Bool {
        return presenter.hasCard
    }
    
    lazy var paymentOptionsPicker = { () -> UIPickerView in
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        return pickerView
    }()
    
    lazy var paymentOptionsToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(paymentOptionsToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var cvvToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(cvvToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    private func initialSetup() {
        presenter = CreditCardReservePaymentPresenter(paymentService: PaymentService())
        presenter.delegate = self
        
        loadNibContent()
        setupTextField()
        setupToolBoxMessageView()
        paymentOptionsToolbarDoneButtonTapped()
    }
    
    private func setupToolBoxMessageView() {
        toolTipMessageView.alpha = 0
    }
    
    private func setupTextField() {
        paymentOptionsTextField.inputView = paymentOptionsPicker
        paymentOptionsTextField.inputAccessoryView = paymentOptionsToolbar
        cvvTextField.delegate = self
        cvvTextField.inputAccessoryView = cvvToolbar
    }
    
    func setCard(at listCardDataView: ListCardDataView) {
        presenter.setCard(at: listCardDataView)
    }
    
    func setInstallments(at amount: Double) {
        presenter.setInstallments(at: amount)
        
        let chevronIsHidden = presenter.numberOfInstallments == 1 ? true : false
        arrowDownImage.isHidden = chevronIsHidden
        
        let numberOfInstallments = "1"
        
        let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: amount))
        
        paymentOptionsTextField.text = "\(String(numberOfInstallments))x \(amountString ?? "")"
        paymentOptionsTextField.setLeftPaddingPoints(10)
    }
    
    @objc func paymentOptionsToolbarDoneButtonTapped() {
        presenter.didSetInstallments(at: paymentOptionsPicker.selectedRow(inComponent: 0))
        paymentOptionsTextField.resignFirstResponder()
    }
    
    @objc func cvvToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        cvvTextField.resignFirstResponder()
    }
    
    // MARK: - Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.numberOfInstallments
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.titleForRow(at: row)
    }
    
    // MARK: - CreditCardReservePaymentViewDelegate
    
    var securityCode: String? {
        return cvvTextField.text
    }
    
    func startLoadingInstallments() {
        paymentOptionsTextField.isEnabled = false
    }
    
    func endLoadingInstallments() {
        paymentOptionsTextField.isEnabled = true
    }
    
    func present(error: Error) {
        delegate?.present(error: error)
    }
    
    func showInstallments(value: String) {
        paymentOptionsTextField.text = value
        
    }
    
    func showCard(listCardDataView: ListCardDataView) {
        cvvTextField.text = ""
        brandImage.image = listCardDataView.brandImage
        cardNumberLabel.text = listCardDataView.cardNumberShortFormatted
    }
    
    func applyValidCard() {
        cardNumberLabel.textColor = #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
        cardView.cornerRadius = 15
        cardView.borderWidth = 1
        cardView.borderColor = UIColor.clear
        installmentsTitle.textColor = #colorLiteral(red: 0.6941176471, green: 0.6941176471, blue: 0.6941176471, alpha: 1)
        cvvToolTipView.backgroundColor = .clear
        cvvToolTipView.layer.cornerRadius = 5
        cvvToolTipView.layer.borderColor = UIColor.gray.cgColor
        cvvToolTipView.layer.borderWidth = 0
        cvvToolTipView.layer.masksToBounds = true
        paymentOptionsTextField.layer.cornerRadius = 4
        paymentOptionsTextField.layer.borderWidth = 0
        paymentOptionsTextField.layer.borderColor = #colorLiteral(red: 0.6941176471, green: 0.6941176471, blue: 0.6941176471, alpha: 1)
    }
    
    func applyInvalidCards() {
        cardView.borderColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        installmentsTitle.textColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
    }
    
    // MARK: - TextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }

        let newLength = text.count + string.count - range.length
        return newLength <= 4
    }
    
    func checkToolTipButtonState() {
        if toolTipMessageView.alpha == 0 {

            toolTipMessageView.alpha = 0
            
            UIView.animate(withDuration: 0.5) {
                self.toolTipMessageView.alpha = 1.0
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.toolTipMessageView.alpha = 0
            }
        }
    }
    
    @IBAction func tapShowToolBoxMessage() {
        checkToolTipButtonState()
    }
    
    @IBAction func closeToolTipMessageView(_ sender: UIButton) {
        checkToolTipButtonState()
    }
}

