//
//  DynamicHeightCollectionView.swift
//  Unidas
//
//  Created by Anderson Vieira on 25/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
