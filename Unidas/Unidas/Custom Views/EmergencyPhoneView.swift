//
//  EmergencyPhoneView.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class EmergencyPhoneView: UIView, NibReusable {

    var phoneSACNumber: String = "-"
    @IBOutlet weak var phoneNumberLabel: UILabel?
    @IBOutlet weak var backBorderView: UIView!
    @IBOutlet weak var emergencyPhoneImage: UIImageView!
    
    func setNumber(phone: String) {
        phoneSACNumber = phone.filter({$0 != " "})
        
        guard let phoneNumber = phoneNumberLabel else { return }
        phoneNumber.text = phone
    }
    
    func setupView() {
        backBorderView.layer.cornerRadius = 5
        backBorderView.layer.borderColor = #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
        backBorderView.layer.borderWidth = 1
        emergencyPhoneImage.image = #imageLiteral(resourceName: "icon-phone-store").withRenderingMode(.alwaysTemplate)
        emergencyPhoneImage.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    @IBAction func tapCallPhone(_ sender: UITapGestureRecognizer) {
        guard let number = URL(string: "tel://" + phoneSACNumber) else { return }
        UIApplication.shared.open(number)
    }
}
