//
//  EmptyDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol EmptyDataViewDelegate: class {
    func emptyDataView(_ emptyDataView: EmptyDataView, didTapActionButton actionButton: UIButton)
    func touchLink(link: String)
}

class EmptyDataView: UIView, NibLoadable, UITextViewDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var squareView: EnhancedView!
    
    weak var delegate: EmptyDataViewDelegate?
    
    var linkAttributed: [NSAttributedString.Key: Any] = {
        return [
            .font: UIFont.bold(ofSize: 14),
            .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineColor:#colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    func setup(title: String?, message: String?, image: UIImage?, buttonTitle: String?, buttonIsHidden: Bool, squareViewColor: UIColor? = nil) {
        titleLabel.text = title
        imageView.image = image
        actionButton.isHidden = buttonIsHidden
        actionButton.setTitle(buttonTitle, for: .normal)
        
        if let color = squareViewColor {
            squareView.backgroundColor = color
        }
        
        messageTextView.text = message
    }
    
    func setup(empty: Empty) {
        titleLabel.text = empty.title
        imageView.image = empty.image
        actionButton.isHidden = empty.buttonIsHidden
        actionButton.setTitle(empty.buttonTitle, for: .normal)
        
        if let color = empty.squareViewColor {
            squareView.backgroundColor = color
        }
        messageTextView.text = empty.message
        
        if let linkText = empty.linkText, let link = empty.link,
            let message = empty.message {
            let emptyMessageMutable = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 14), NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)])
           
            if let fullRange = message.range(of: linkText) {
                let range = NSRange(fullRange, in: linkText)
                emptyMessageMutable.addAttribute(.link, value: link, range: range)
                emptyMessageMutable.addAttributes(linkAttributed, range: range)
                messageTextView.attributedText = emptyMessageMutable
                messageTextView.textAlignment = .center
                messageTextView.delegate = self
            }
        }
    }
    
    @IBAction func didTapActionButton(_ sender: UIButton) {
        delegate?.emptyDataView(self, didTapActionButton: sender)
    }
    
    // MARK: - TextView delegate
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        delegate?.touchLink(link: url.absoluteString)
        return true
    }
}

struct Empty {
    let title: String?
    let message: String?
    let image: UIImage?
    let buttonTitle: String?
    let buttonIsHidden: Bool
    var squareViewColor: UIColor? = nil
    var linkText: String? = nil
    var link: String? = nil
}
