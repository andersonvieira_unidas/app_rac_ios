//
//  StepView.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class StepView: UIView, NibLoadable {
    @IBOutlet weak var stepStackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    private var total = 0
    private var selected = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        removeAllArrangedSubview()
    }
    
    func setupStep(total: Int, selected: Int) {
        removeAllArrangedSubview()
        
        self.total = total
        self.selected = selected
        
        createSteps(index: 0)
    }
    
    private func removeAllArrangedSubview() {
        for arrangedSubview in stepStackView.arrangedSubviews {
            stepStackView.removeArrangedSubview(arrangedSubview)
            arrangedSubview.removeFromSuperview()
        }
    }
    
    private func createSteps(index: Int) {
        if index < total {
            if selected == index {
                selectedStep(index: index)
            }
            else if selected < index {
                nextStep(index: index)
            }
            else {
                previousStep(index: index)
            }
            
            createSteps(index: index + 1)
        }
    }
    
    private func selectedStep(index: Int) {
        let number = index + 1
        let image = self.image(for: number)
        
        let stepView = SelectedStep.loadFromNib()
        
        stepView.setupStep(number: number, image: image)
        
        stepStackView.addArrangedSubview(stepView)
    }
    
    private func previousStep(index: Int) {
        let number = index + 1
        let previousStep = PreviousStep.loadFromNib()
        previousStep.setup(number: number)
        
        stepStackView.addArrangedSubview(previousStep)
    }
    
    private func nextStep(index: Int) {
        let number = index + 1
        let nextStep = NextStep.loadFromNib()
        nextStep.setup(number: number)
        
        stepStackView.addArrangedSubview(nextStep)
    }
    
    private func image(for step: Int) -> UIImage {
        switch step {
        case 1: return #imageLiteral(resourceName: "ic_stepview1")
        case 2: return #imageLiteral(resourceName: "ic_stepview2")
        case 3: return #imageLiteral(resourceName: "ic_stepview3")
        case 4: return #imageLiteral(resourceName: "ic_stepview4")
            
        default: return UIImage()
        }
    }
}
