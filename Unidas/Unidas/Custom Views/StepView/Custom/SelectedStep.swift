//
//  SelectedStep.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class SelectedStep: UIView, NibLoadable {
    @IBOutlet weak var stepImage: UIImageView!
    @IBOutlet weak var stepNumber: UILabel!
    
    func setupStep(number: Int, image: UIImage) {
        stepNumber.text = String(number)
        stepImage.image = image
    }
}
