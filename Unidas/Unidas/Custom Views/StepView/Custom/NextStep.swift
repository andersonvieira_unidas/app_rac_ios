//
//  NextStep.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class NextStep: UIView, NibLoadable {
    @IBOutlet weak var stepNumber: UILabel!
    
    func setup(number: Int) {
        stepNumber.text = String(number)
    }
}
