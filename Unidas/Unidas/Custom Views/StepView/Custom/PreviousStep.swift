//
//  PreviousStep.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class PreviousStep: UIView, NibLoadable {
    @IBOutlet weak var numberStep: UILabel!
    
    func setup(number: Int) {
        numberStep.text = String(number)
    }
}
