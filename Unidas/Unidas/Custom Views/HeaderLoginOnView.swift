//
//  MoreHeaderLoginOnView.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/05/2018.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol HeaderLoginDelegate: class {
    func didTapProfileImage()
}

class HeaderLoginOnView: UIView, NibLoadable, HeaderLoginOnViewDelegate {
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var photoButton: EnhancedButton!
    weak var delegate: MyAccountHeaderDelegate?
    
    var delegateMyAccount: MyAccountViewControllerDelegate?
    var delegateProfile: ProfileOnViewControllerDelegate?
    
    private var presenter: HeaderLoginOnPresenterDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    private func initialSetup() {
        presenter = HeaderLoginOnPresenter(unidasUserStatusService: UnidasUserStatusService())
        presenter.delegate = self
    }
    
    func reloadPendencies(){
        presenter.viewDidLoad()
    }

    func setup(userResponseDataView: UserResponseDataView, showMaskPhotoButton: Bool? = false) {
     
        let helloUser = NSLocalizedString("Hello, %@", comment: "Hello, %@ Comment")
        
        let firstNameUpperCased = userResponseDataView.account.firstName.uppercased()
        let hello = String(format: helloUser, firstNameUpperCased)
        
        nameField.text = hello
        profileImage.image = nil
        profileImage.setImageAvatar(withURL: userResponseDataView.account.profileImage, borderColor: #colorLiteral(red: 0.2274329066, green: 0.5870787501, blue: 0.9447389245, alpha: 1))
        if let showMaskPhotoButton = showMaskPhotoButton, showMaskPhotoButton {
            photoButton.backgroundColor = #colorLiteral(red: 0.2862745098, green: 0.4784313725, blue: 0.7411764706, alpha: 1)
            profileImage.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            profileImage.layer.borderColor = #colorLiteral(red: 0.2862745098, green: 0.4784313725, blue: 0.7411764706, alpha: 1)
            photoButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            photoButton.isHidden = false
            photoButton.isEnabled = true
        } else {
            photoButton.isEnabled = false
            if userResponseDataView.account.profileImage == nil {
                photoButton.isHidden = false
                photoButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                profileImage.tintColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
                profileImage.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
                photoButton.tintColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
            } else {
                showWhenExistsPhoto()
            }
        }
    }
    
    @IBAction func tapChangeProfileImage() {
        delegate?.didTapProfileImage()
    }
    
    func showCurrentUserImage(imageUrl: URL?) {
        profileImage.setImage(withURL: imageUrl)
        showWhenExistsPhoto()
    }
    
    func updateStatusUser(invalid: Bool?) {
        if let userInValid = invalid, userInValid {
            descriptionTitle.text = NSLocalizedString("Account Invalid", comment: "")
            delegateProfile?.addBadge(index: 1)
            delegateMyAccount?.addBadge(index: 1)
        } else {
            descriptionTitle.text = NSLocalizedString("Account Valid", comment: "")
            delegateProfile?.removeBadge(index: 1)
            delegateMyAccount?.removeBadge(index: 1)
        }
    }
    
    func endLoading() {
        
    }
    
    func present(error: Error) {
        
    }
    
    // MARK: Private Functions
    
    private func showWhenExistsPhoto(){
        photoButton.isHidden = true
        photoButton.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        profileImage.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        profileImage.layer.borderColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        photoButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
    }
}
