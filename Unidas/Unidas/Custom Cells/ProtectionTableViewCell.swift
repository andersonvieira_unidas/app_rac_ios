//
//  ProtectionTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 12/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol ProtectionTableViewCellDelegate: class {
    func protectionTableViewCell(_ cell: ProtectionTableViewCell, didUpdateStepperValue newValue: Int)
    func showTooltip(_ row: Int, button: UIButton)
}

class ProtectionTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var tooltipButton: UIButton!
    
    var row: Int? = nil
    
    override var isUserInteractionEnabled: Bool {
        didSet {
            if isUserInteractionEnabled {
                backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            } else {
                backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
            }
        }
    }
    
    weak var delegate: ProtectionTableViewCellDelegate?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkImageView.image = selected ? #imageLiteral(resourceName: "icon-button-signup") : #imageLiteral(resourceName: "icon-checkmark-off-background-white")
        titleLabel.textColor = selected ? #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1) : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    func setup(with protection: ProtectionDataView) {
        titleLabel.text = protection.title?.lowerCapitalize()
        subtitleLabel.text = protection.promotionalDailyCostsText
        tooltipButton.isHidden = true
    }
    
    func setup(with protection: ProtectionsDataView, row: Int) {
        self.row = row
        titleLabel.text = protection.name?.lowerCapitalize()
        subtitleLabel.text = protection.valueDescription
        tooltipButton.isHidden = protection.hasDescription ? false : true
    }
    
    func setup(with equipment: EquipmentDataView) {
        titleLabel.text = equipment.name?.lowerCapitalize()
        subtitleLabel.text = equipment.valueQuantityDescription
        tooltipButton.isHidden = true
    }
    
    @objc func stepperValueChanged(_ sender: UIStepper) {
        delegate?.protectionTableViewCell(self, didUpdateStepperValue: Int(sender.value))
    }

    @IBAction func tapTooltip(_ sender: UIButton) {
        if let rowIndex = row {
            delegate?.showTooltip(rowIndex, button: sender)
        }
    }
}
