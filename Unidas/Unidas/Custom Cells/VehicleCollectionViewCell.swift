//
//  VehicleCollectionViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

final class VehicleCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var modelNameLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var illustrativePictureImageView: UIImageView!
    @IBOutlet weak var fuelLevelLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        applyShadow(for: rect)
    }
    
    func applyShadow(for rect: CGRect) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 8.0
        layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: 13.0).cgPath
        layer.shouldRasterize = true
    }
    
    func setup(with vehicle: VehicleDataView) {
        modelNameLabel.text = vehicle.vehicleModel.nameWithModel
        groupLabel.text = vehicle.group?.name
        illustrativePictureImageView.setImage(withURL: vehicle.vehicleModel.illustrativePicture, placeholderImage: nil)
        fuelLevelLabel.text = vehicle.telemetryInformation?.estimatedFuelLevelDescription
    }
    
    override func prepareForReuse() {
        illustrativePictureImageView.cancelImageRequest()
    }
}
