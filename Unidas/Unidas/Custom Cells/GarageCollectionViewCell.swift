//
//  GarageCollectionViewCell.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol GarageCollectionViewCellDelegate: class {
    func didTapShowCharacteristicButton(in garageCell: GarageCollectionViewCell)
}

final class GarageCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var voucherAppliedIndicator: UIView!
    @IBOutlet weak var voucherDescriptionLabel: UILabel!
    @IBOutlet weak var totalValueWithoutDiscountView: UIView!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var groupTypeLabel: UILabel!
    @IBOutlet weak var mileageInformationLabel: UILabel!
    @IBOutlet weak var vehicleNamesLabel: UILabel!
    @IBOutlet weak var vehiclePictureImageView: UIImageView!
    @IBOutlet weak var totalCostWithoutDiscountLabel: UILabel!
    @IBOutlet weak var totalCostWithDiscountLabel: UILabel!
    @IBOutlet weak var totalCostWithPrePaymentDiscountLabel: UILabel!
    @IBOutlet weak var moreDetailsButton: UIButton!
    @IBOutlet weak var payInAdvanceView: UIView!
    @IBOutlet weak var payAtPickupView: UIView!
    @IBOutlet weak var expressPriceView: UIView!
    @IBOutlet weak var expressPriceLabel: UILabel!
    
    weak var delegate: GarageCollectionViewCellDelegate?
    
    private var sizingOnlyWidthConstraint: NSLayoutConstraint? = nil
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        applyShadow(for: rect)
        applyButtonDetailsLayout()
    }
    
    func applyButtonDetailsLayout() {
        moreDetailsButton.layer.cornerRadius = 0.5 * moreDetailsButton.bounds.size.width
        moreDetailsButton.clipsToBounds = true
    }
    
    func applyShadow(for rect: CGRect) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 8.0
        layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: 13.0).cgPath
        layer.shouldRasterize = true
    }
    
    func setup(with quotationResumeDataView: QuotationResumeDataView, isFranchise: Bool, isExpress: Bool) {
        let diaryValue = (quotationResumeDataView.calculation?.unitChargePerValue ?? quotationResumeDataView.calculation?.unitChargeValue) ?? 0.0
        let prePaymentValue =  quotationResumeDataView.calculation?.unitPrePaymentValue ?? 0.0
        
        groupDescriptionLabel.text = quotationResumeDataView.quotation?.description
        groupNameLabel.text = quotationResumeDataView.quotation?.formattedGroup
        
        groupTypeLabel.text = quotationResumeDataView.quotation?.vehicleGroups
        
        vehicleNamesLabel.text = quotationResumeDataView.quotation?.vehicle
        totalCostWithoutDiscountLabel.text = quotationResumeDataView.calculation?.unitCharge
        
        let totalCostWithDiscount = NumberFormatter.currencyFormatter.string(from: diaryValue as NSNumber)
        totalCostWithDiscountLabel.text = totalCostWithDiscount
        
        let totalCostWithPrePaymentDiscount = NumberFormatter.currencyFormatterWithouRounding.string(from: prePaymentValue as NSNumber)
        totalCostWithPrePaymentDiscountLabel.text = totalCostWithPrePaymentDiscount
        
        
        mileageInformationLabel.text = quotationResumeDataView.mileageDescription
        voucherAppliedIndicator.isHidden = !quotationResumeDataView.showsVoucherDescription
        voucherDescriptionLabel.text = quotationResumeDataView.voucherDescription
        DispatchQueue.main.async {
            self.vehiclePictureImageView.setImage(withURL: quotationResumeDataView.quotation?.picture, placeholderImage: nil)
        }
        
        if isFranchise {
            payInAdvanceView.isHidden = true
        }
        
        if isExpress {
            expressPriceView.isHidden = false
            payAtPickupView.isHidden = true
            payInAdvanceView.isHidden = true
            
            expressPriceLabel.text = quotationResumeDataView.calculation?.unitCharge
        }
    }
    
    
    override func prepareForReuse() {
        vehiclePictureImageView.cancelImageRequest()
    }
    
    @IBAction func touchShowCharacteristic(_ sender: UIButton) {
        delegate?.didTapShowCharacteristicButton(in: self)
    }

    func size(width: CGFloat, height: CGFloat) -> CGSize {
        let constraint = sizingOnlyWidthConstraint ?? contentView.widthAnchor.constraint(equalToConstant: width)
        if width != constraint.constant {
            constraint.constant = width
        }
        constraint.isActive = true
        var fittingSize = UIView.layoutFittingCompressedSize
        fittingSize.width = width
        fittingSize.height = height - 20
        
        return contentView.systemLayoutSizeFitting(fittingSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .defaultLow)
    }    
}

