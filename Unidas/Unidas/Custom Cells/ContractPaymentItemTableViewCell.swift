//
//  ContractPaymentItemTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 10/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ContractPaymentItemTableViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var paymentInstallmentsLabel: UILabel!
    @IBOutlet weak var paymentCardBrandImageView: UIImageView!
    @IBOutlet weak var paymentCardFinalNumbersLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    
    func setup(for paymentListItem: ContractDetailPaymentListItemDataView, isLegalEntityPaymentRequired: Bool = false,  isLegalEntityPrePaymentRequired: Bool = false) {
        paymentInstallmentsLabel.text = paymentListItem.installmentsDescription
        paymentCardBrandImageView.image = paymentListItem.cardBrandImage
        paymentCardFinalNumbersLabel.text = paymentListItem.cardLastNumbersWithoutDots
        paymentStatusLabel.text = paymentListItem.paymentStatus
    }
    
}
