//
//  CancellationReasonTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 19/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class CancellationReasonTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var selectorImageView: UIImageView!
    @IBOutlet weak var reasonLabel: UILabel!
    
    func setup(reason: String) {
        reasonLabel?.text = reason
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectorImageView?.image = selected ?   #imageLiteral(resourceName: "icon-checkmark-on-general") :  #imageLiteral(resourceName: "icon-checkmark-off-general")
    }
    
}
