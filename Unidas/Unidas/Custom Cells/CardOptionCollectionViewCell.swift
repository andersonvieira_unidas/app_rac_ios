//
//  CardOptionCollectionViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 06/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class CardOptionCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardBrandImageView: UIImageView!
    @IBOutlet weak var cardOptionDescriptionLabel: UILabel!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            checkmarkImageView.image = isSelected ? #imageLiteral(resourceName: "icon-rounded-checkbox") : #imageLiteral(resourceName: "icon-rounded-checkbox-unchecked")
        }
    }

}
