//
//  ReservationExpressStepCollectionViewCell.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 05/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol ReservationExpressStepCollectionViewCellDelegate {
    func reloadExpired()
}

class ReservationExpressStepCollectionViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var reservationButton: EnhancedButton!
    @IBOutlet weak var paymentButton: EnhancedButton!
    @IBOutlet weak var qrCodeButton: EnhancedButton!
    @IBOutlet weak var informationView: UIView!
    
    private var countdownTimer: Timer? = nil
    private var reservationStep: ReservationStep? = nil
    
    var delegate: ReservationExpressStepCollectionViewCellDelegate? = nil
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.bold(ofSize: 14)
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.regular(ofSize: 14)
        label.numberOfLines = 2
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var counterLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.light(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var stackWithCounter: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var stackWithInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func willRemoveSubview(_ subview: UIView) {
        countdownTimer?.invalidate()
        countdownTimer = nil
    }
    
    func setup(reservationStep: ReservationStep){
        
        self.reservationStep = reservationStep
        checkStatusDone(reservationStatus: reservationStep.status)
        reservationLabel.text = reservationStep.reservation
        titleLabel.text = reservationStep.title
        descriptionLabel.text = reservationStep.description
        
        if reservationStep.financialPendency {
            titleLabel.textColor = #colorLiteral(red: 0.9474454522, green: 0.2112037241, blue: 0.1881176829, alpha: 1)
            errorButton(button: paymentButton)
            setupInfo()
            return
        }else if (reservationStep.expressDateExpired ?? false && !reservationStep.paid) {
            errorButton(button: paymentButton)
            setupInfo()
            return
        } else {
            if let expressDateExpired = reservationStep.expressDateExpired, !expressDateExpired, !reservationStep.paid {
                countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
                
                titleLabel.font = UIFont.regular(ofSize: 14)
                titleLabel.numberOfLines = 2
                
                stackWithCounter.addArrangedSubview(titleLabel)
                stackWithCounter.addArrangedSubview(counterLabel)
              
                informationView.addSubview(stackWithCounter)
                
                NSLayoutConstraint.activate([
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    stackWithCounter.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
                    stackWithCounter.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
                    stackWithCounter.trailingAnchor.constraint(equalTo: informationView.trailingAnchor, constant: 0),
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    counterLabel.widthAnchor.constraint(equalToConstant: 75)
                ])
            } else {
                setupInfo()
            }
        }
    }
    
    private func setupInfo(){
        informationView.subviews.forEach({ $0.removeFromSuperview() })
        titleLabel.font = UIFont.bold(ofSize: 14)
        
        stackWithInfo.addArrangedSubview(titleLabel)
        stackWithInfo.addArrangedSubview(descriptionLabel)
        
        informationView.addSubview(stackWithInfo)
        
        NSLayoutConstraint.activate([
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
            stackWithInfo.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
            stackWithInfo.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0)
        ])
    }
    
    private func checkStatusDone(reservationStatus: [ReservationStatus]) {
        doneButton(button: reservationButton)
        
        if !reservationStatus.filter({ $0 == .checkin }).isEmpty{
            doneButton(button: paymentButton)
            doneButton(button: qrCodeButton)
            return
        }
        
        if !reservationStatus.filter({ $0 == .paid }).isEmpty {
            doneButton(button: paymentButton)
            return
        }
        
        if reservationStep?.financialPendency ?? false || reservationStep?.expressDateExpired ?? false {
            errorButton(button: paymentButton)
        }
    }
    
    @objc func updateTime() {
        if let dateExpired = reservationStep?.expressDateExpired, !dateExpired, let dueDate = reservationStep?.dueDate {
            let timeElapsed = timeFormatted(dueDate: dueDate, dateNow: Date())
            counterLabel.text = timeElapsed
        }
    }
    
    private func timeFormatted(dueDate: Date, dateNow: Date) -> String? {
        if reservationStep?.expressDateExpired ?? false {
            countdownTimer?.invalidate()
            countdownTimer = nil
            delegate?.reloadExpired()
            return "00:00"
        } else {
            let timeInterval = dueDate.timeIntervalSince(dateNow)
            
            if timeInterval <= 0 {
                countdownTimer?.invalidate()
                countdownTimer = nil
                delegate?.reloadExpired()
                return "00:00"
            }
            
            let interval = Int(timeInterval)
            let hour = interval / 3600
            let minutes = interval / (60) % 60
            
            return String(format: "%02d:%02d", hour, minutes)
        }
    }
    
    private func doneButton(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
    }
    
    private func errorButton(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
    }
}
