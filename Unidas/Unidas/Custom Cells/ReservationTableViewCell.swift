//
//  ReservationTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol ReservationTableViewCellDelegate: class {
    func reservationTableViewCellDidTapNewReservationButton(_ reservationCell: ReservationTableViewCell)
    func reservationTableViewCellDidTapCancelReservationButton(_ reservationCell: ReservationTableViewCell)
    func reservationTableViewCellDidTapChangeReservationButton(_ reservationCell: ReservationTableViewCell)
    //func reservationTableViewCellDidTapCheckInReservationButton(_ reservationCell: ReservationTableViewCell, isCheckinConfirmed: Bool)
    //func reservationTableViewCellDidTapPreviewReservationButton(_ reservationCell: ReservationTableViewCell)
    func reservationTableViewCellDidTapRoutePickUpButton(_ reservationCell: ReservationTableViewCell)
    func reservationTableViewCellDidTapRouteReturnButton(_ reservationCell: ReservationTableViewCell)
    func reservationTableViewCellDidTapAddToCalendarButton(_ reservationCell: ReservationTableViewCell)
}

protocol ReservationPaymentDelegate {
    func doReservationPayment(cell: ReservationTableViewCell)
    func showUserInValidate(with message: String)
}

class ReservationTableViewCell: UITableViewCell, NibReusable, ReservationCellViewDelegate {

    @IBOutlet weak var paymentValuesStackView: UIStackView!
    @IBOutlet weak var legalEntityStackView: UIStackView!
    @IBOutlet weak var legalEntityTitle: UILabel!
    @IBOutlet weak var legalEntityNameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnLocationLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupVehiclesLabel: UILabel!
    @IBOutlet weak var groupMainCharacteristic: UILabel!
    @IBOutlet weak var pickUpObservationStack: UIStackView!
    @IBOutlet weak var pickUpObservationLabel: UILabel!
    @IBOutlet weak var returnObservationStack: UIStackView!
    @IBOutlet weak var returnObservationLabel: UILabel!
    @IBOutlet weak var cardStackView: UIStackView!
    @IBOutlet weak var cardBrandImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var paymentValueLabel: UILabel!
    @IBOutlet weak var paymentTitleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var previewButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var returnRouteButton: EnhancedButton!
    @IBOutlet weak var pickUpRouteButton: EnhancedButton!
    @IBOutlet weak var newReservationButton: EnhancedButton?
    @IBOutlet weak var paymentButton: EnhancedButton!
    @IBOutlet weak var paymentButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarButton: UIButton!
    
    private var presenter: ReservationCellPresenterDelegate!
    weak var delegate: ReservationTableViewCellDelegate?
    var reservationDelegate: ReservationPaymentDelegate?
    
    var storeCode = String()
    
    func setup(with vehicleReservationsDataView: VehicleReservationsDataView) {
 
        if let _ = vehicleReservationsDataView.pickupGeolocation {
            pickUpRouteButton.setHidden(false)
        }
        if let _ = vehicleReservationsDataView.returnGeolocation {
            returnRouteButton.setHidden(false)
        }
        
        numberLabel.text = vehicleReservationsDataView.reservationNumberWithText
        pickUpDateLabel.text = vehicleReservationsDataView.pickUpDate
        pickUpLocationLabel.text = vehicleReservationsDataView.formattedPickUpLocation
        returnDateLabel.text = vehicleReservationsDataView.returnDate
        returnLocationLabel.text = vehicleReservationsDataView.formattedReturnLocation
        groupNameLabel.text = vehicleReservationsDataView.groupDescription
        totalValueLabel.text = vehicleReservationsDataView.totalReservationPayed
        groupVehiclesLabel.text = vehicleReservationsDataView.vehicleGroup
        groupMainCharacteristic.text = nil
        pickUpObservationStack.isHidden = vehicleReservationsDataView.isHiddenPickUp
        pickUpObservationLabel.text = vehicleReservationsDataView.pickUpObservation
        returnObservationStack.isHidden = vehicleReservationsDataView.isHiddenReturn
        returnObservationLabel.text = vehicleReservationsDataView.returnObservation
        cardStackView.isHidden = !vehicleReservationsDataView.reserveIsPaid
        paymentTitleLabel.text = vehicleReservationsDataView.paymentTitle
        cardNumberLabel.text = vehicleReservationsDataView.cardNumberShortFormatted
        cardBrandImageView.image = vehicleReservationsDataView.brandImage
        paymentValueLabel.text = vehicleReservationsDataView.paymentMade
        returnRouteButton.isHidden = false //vehicleReservationsDataView.isHiddenReturn
        pickUpRouteButton.isHidden = false //vehicleReservationsDataView.isHiddenPickUp
        storeCode = vehicleReservationsDataView.storePickup
        legalEntityNameLabel.text = vehicleReservationsDataView.legalEntityName
   
        presenter = ReservationCellPresenter(vehicleReservationsDataView: vehicleReservationsDataView, paymentService: PaymentService(),
                                             reservationUnidasService: ReservationUnidasService(), unidasUserStatusService: UnidasUserStatusService())
        presenter.delegate = self
        
        checkInButton.isHidden = true
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .doPayment) { (toggle) in
            self.presenter.doPaymentToggle = toggle
        }

        FirebaseRealtimeDatabase.toggleValue(withKey: .checkinReservation) { (toggle) in
            if !toggle {
                self.checkInButton.isHidden = true
            } else {
                self.presenter.getCheckinButton(canCheckin: false, vehicleReservationsDataView: vehicleReservationsDataView)
            }
        }

        legalEntityStackView.isHidden = true
        
        if vehicleReservationsDataView.isLegalEntity == true {
            legalEntityStackView.isHidden = false
            
            if vehicleReservationsDataView.isLegalEntityRequiredPayment == false {
                cardStackView.isHidden = true
                paymentValueLabel.isHidden = true
                paymentTitleLabel.isHidden = true
            }
            
            if vehicleReservationsDataView.hiddenLegalEntityPaymentInformation == false {
                paymentValueLabel.isHidden = true
            }else{
                cardStackView.isHidden = false
                paymentValueLabel.isHidden = false
                paymentTitleLabel.isHidden = false
            }
        }
        
        updateButtons(vehicleReservationsDataView: vehicleReservationsDataView)
    }
    
    func showInstallmentsValue(installments: String, vehicleReservationsDataView: VehicleReservationsDataView) {
        paymentValueLabel.text = vehicleReservationsDataView.valuePayment(installments: installments)
    }
    
    func showCheckInButton(vehicleReservationsDataView: VehicleReservationsDataView) {
        
        if vehicleReservationsDataView.isCheckin24HoursAvailable {
            if vehicleReservationsDataView.hideCheckinButton {
                checkInButton.setHidden(true)
                return
            }
            checkInButton.setHidden(false)
        }else{
            checkInButton.setHidden(true)
        }
    }
    
    func setFieldsEnabled(_ isEnabled: Bool) {
        cancelButton.isEnabled = isEnabled
        previewButton.isEnabled = isEnabled
        changeButton.isEnabled = isEnabled
        checkInButton.isEnabled = isEnabled
    }
    
    //func didUpdateButtons(vehicleReservationsDataView: VehicleReservationsDataView) {
   //     updateButtons(vehicleReservationsDataView: vehicleReservationsDataView)
   // }
    
    private func setupPayInAdvanceOnReserveButton(vehicleReservationsDataView: VehicleReservationsDataView) {

        paymentButton.isHidden = true
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .doPayment) { (value) in
            if !value {
                self.paymentButton?.isHidden = true
            } else {
                let hiddenButtonPayment = vehicleReservationsDataView.hiddenButtonPaymentWhenStoreIsAcceptPayment
                self.paymentButton.isHidden = hiddenButtonPayment
            }
        }
    }
    
    func showUserInValidate(message: String) {
        if let _ = reservationDelegate {
            reservationDelegate?.showUserInValidate(with: message)
        }
    }
    
    func doReservationPayment(){
        if let _ = reservationDelegate {
            reservationDelegate?.doReservationPayment(cell: self)
        }
    }
    
    func enablePaymentButton() {
        paymentButton.isEnabled = true
    }
    
    
    @IBAction func doPaymentButtonTapped(_ sender: UIButton) {
        paymentButton.isEnabled = false
        presenter.checkUserValidate()
    }
    
    @IBAction func newReservationButtonTapped(_ sender: UIButton) {
        delegate?.reservationTableViewCellDidTapNewReservationButton(self)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        delegate?.reservationTableViewCellDidTapCancelReservationButton(self)
    }
    
    @IBAction func changeButtonTapped(_ sender: UIButton) {
        delegate?.reservationTableViewCellDidTapChangeReservationButton(self)
    }
    
    @IBAction func previewButtonTapped(_ sender: UIButton) {
       // delegate?.reservationTableViewCellDidTapPreviewReservationButton(self)
    }
    
    @IBAction func calendarButtonTapped(_ sender: Any) {
        delegate?.reservationTableViewCellDidTapAddToCalendarButton(self)
    }
    
    @IBAction func checkInButtonTapped(_ sender: UIButton) {
        //TODO checar confirmacao
        //delegate?.reservationTableViewCellDidTapCheckInReservationButton(self, isCheckinConfirmed: false)
    }

    @IBAction func routePickUpButtonTapped(_ sender: EnhancedButton) {
        delegate?.reservationTableViewCellDidTapRoutePickUpButton(self)
    }
    
    @IBAction func routeReturnButtonTapped(_ sender: UIButton) {
        delegate?.reservationTableViewCellDidTapRouteReturnButton(self)
    }
    
    private func isLegalEntityHideButtons(vehicleReservationsDataView: VehicleReservationsDataView) {
        if vehicleReservationsDataView.isLegalEntity {
            cancelButton.isHidden = true
            changeButton.isHidden = true
            previewButton.isHidden = false
            calendarButton.isHidden = false
        } else {
           calendarButton.isHidden = true
        }
    }
    
    private func updateButtons(vehicleReservationsDataView: VehicleReservationsDataView) {
        if session.currentUser == nil {
            changeButton.isHidden = true
            paymentButton.isHidden = true
            return
        }
        
        cancelButton.isHidden = false
        changeButton.isHidden = vehicleReservationsDataView.isHiddenChange
        newReservationButton?.isHidden = false
        
        setupPayInAdvanceOnReserveButton(vehicleReservationsDataView: vehicleReservationsDataView)
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                self.newReservationButton?.isHidden = true
            }
        }

        FirebaseRealtimeDatabase.toggleValue(withKey: .changeReservation) { (toggle) in
            if !toggle {
                self.changeButton.isHidden = true
                self.paymentButton.isHidden = true
            }
        }

        if !checkInButton.isHidden {
            changeButton.isHidden = true
        }
        
        /*if let checkinReservation = ToggleProfile.model?.config?.checkinReservation, !checkinReservation {
            checkInButton.isHidden = true
        } else {
            checkInButton.setHidden(vehicleReservationsDataView.hideCheckinButton)
        }*/
        
        isLegalEntityHideButtons(vehicleReservationsDataView: vehicleReservationsDataView)
    }
}
