//
//  VehicleCharacteristicTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 02/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class VehicleCharacteristicTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func setup(with characteristic: VehicleCharacteristicDataView) {
        iconImageView.setImage(withURL: characteristic.image, placeholderImage: nil)
        nameLabel.text = characteristic.text
        iconImageView.tintColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
    }
    
}
