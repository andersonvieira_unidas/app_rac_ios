//
//  UpcomingReservationTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/05/2020.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable


protocol UpcomingReservationTableViewCellDelegate {
    func showUpcomingReservationDetails(reservation: VehicleReservationsDataView)
}

class UpcomingReservationTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var statusButton: EnhancedButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var detailsButton: UIButton!

    var delegate: UpcomingReservationTableViewCellDelegate?
    var reservation: VehicleReservationsDataView?
    
    let detailsButtonAttributes: [NSAttributedString.Key: Any] = [
    .font: UIFont(name: "Roboto-Medium", size: 14) ?? UIFont.systemFont(ofSize: 14),
    .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
    .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    @IBAction func detailButtonTapped(_ sender: UIButton)
    {
        if let reservation = reservation {
            delegate?.showUpcomingReservationDetails(reservation: reservation)
        }
    }

    func setup(with vehicleReservationDataView: VehicleReservationsDataView, rules: RulesDataView?) {

        reservation = vehicleReservationDataView
        
        let attributeString = NSMutableAttributedString(string: NSLocalizedString("Visualize", comment: ""), attributes: detailsButtonAttributes)
        detailsButton.setAttributedTitle(attributeString, for: .normal)
        
        if let rules = rules {
            let reservationRulesDataView = createReservationRulesDataView(vehicleReservationsDataView: vehicleReservationDataView, rulesDataView: rules)
            let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
            let reservationStep = reservationRules.make()

            statusButton.setImage(statusImage(status: reservationStep.status), for: .normal)
            statusLabel.text = reservationStep.title
            descriptionLabel.text = reservationStep.description
        } else {
            let rules = RulesDataView(pendencies: true, userAvailableToCheckin: false, financialPendency: false)
            
            let reservationRulesDataView = createReservationRulesDataView(vehicleReservationsDataView: vehicleReservationDataView, rulesDataView: rules)
            let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
            let reservationStep = reservationRules.make()
            
            statusButton.setImage(statusImage(status: reservationStep.status), for: .normal)
            statusLabel.text = reservationStep.title
            descriptionLabel.text = reservationStep.description
        }
        reservationLabel.text = vehicleReservationDataView.reservationNumberWithText?.uppercased()
    }
    
    private func statusImage(status: [ReservationStatus]) -> UIImage {
        if !status.filter({ $0 == .checkin }).isEmpty{
            return #imageLiteral(resourceName: "icon-qrcode-done")
        }
        
        if !status.filter({ $0 == .paid }).isEmpty {
            return #imageLiteral(resourceName: "icon-payment-done")
        }
        return #imageLiteral(resourceName: "icon-reservation-done")
    }
    
    private func createReservationRulesDataView(vehicleReservationsDataView: VehicleReservationsDataView,
                                                   rulesDataView: RulesDataView) -> ReservationRulesDataView {
           return ReservationRulesDataView(userValidated: rulesDataView.userValidated,
                                           reservationNumber: vehicleReservationsDataView.reserveNumber,
                                           checkinAvailable: vehicleReservationsDataView.checkinAvailable,
                                           reservationPaid: vehicleReservationsDataView.reservationPaid,
                                           hoursToCheckin: vehicleReservationsDataView.hoursToCheckin,
                                           userAvailableToCheckin: rulesDataView.userAvailableToCheckin,
                                           preAuthorizationPaid: vehicleReservationsDataView.preAuthorizationPaid,
                                           isFranchise: vehicleReservationsDataView.isFranchise,
                                           dueDate: vehicleReservationsDataView.dueDate,
                                           dueTime: vehicleReservationsDataView.dueTime,
                                           express: vehicleReservationsDataView.express,
                                           financialPendency: rulesDataView.financialPendency)
       }
}
