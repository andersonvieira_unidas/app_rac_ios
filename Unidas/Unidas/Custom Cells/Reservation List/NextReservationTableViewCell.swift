//
//  NextReservationTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 09/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol NextReservationTableViewDelegate {
    func showDetails(reservation: VehicleReservationsDataView)
    func showAction(action: ReservationAction, reservation: VehicleReservationsDataView)
}

class NextReservationTableViewCell: UITableViewCell, NibReusable {
    var delegate: NextReservationTableViewDelegate?
    
    @IBOutlet weak var reservationLabel: UILabel!
    
    @IBOutlet weak var reservationButton: EnhancedButton!
    @IBOutlet weak var paymentButton: EnhancedButton!
    @IBOutlet weak var qrCodeButton: EnhancedButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var reservationActionButton: UnidasButton!
    @IBOutlet weak var detailsButton: UIButton!
    
    @IBOutlet var buttons: [EnhancedButton]!
    
    var reservationStep: ReservationStep?
    var reservation: VehicleReservations?
    
    let detailsButtonAttributes: [NSAttributedString.Key: Any] = [
    .font: UIFont(name: "Roboto-Medium", size: 14) ?? UIFont.systemFont(ofSize: 14),
    .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
    .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    @IBAction func tapDetails(_ sender: Any) {
        if  let reservation = reservation {
            let vehicleReservationDataView = VehicleReservationsDataView(model: reservation)
            delegate?.showDetails(reservation: vehicleReservationDataView)
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        if let action = reservationStep?.action, let reservation = reservation {
            let vehicleReservationDataView = VehicleReservationsDataView(model: reservation)
            delegate?.showAction(action: action, reservation: vehicleReservationDataView)
        }
    }
    
    func setup(myReservationDataView: MyReservationDataView){
        let reservationRulesDataView = createReservationRulesDataView(myReservationDataView: myReservationDataView)
        let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
        self.reservationStep = reservationRules.make()
        self.reservation = myReservationDataView.vehicleReservation
        
        if let status = reservationStep?.status {
            checkStatusDone(reservationStatus: status)
        }
        reservationLabel.text = reservationStep?.reservation.uppercased()
        titleLabel.text = reservationStep?.title
        descriptionLabel.text = reservationStep?.description
        
        reservationActionButton.setTitle(reservationStep?.buttonTitle, for: .normal)
        reservationActionButton.isEnabled = reservationStep?.buttonEnabled ?? true
        let attributeString = NSMutableAttributedString(string: NSLocalizedString("Visualize", comment: ""), attributes: detailsButtonAttributes)
        detailsButton.setAttributedTitle(attributeString, for: .normal)
        checkToggles()
    }
    
    private func createReservationRulesDataView(myReservationDataView: MyReservationDataView) -> ReservationRulesDataView {
        return ReservationRulesDataView(userValidated: myReservationDataView.userValidated,
                                        reservationNumber: myReservationDataView.reservationNumber,
                                        checkinAvailable: myReservationDataView.checkinAvailable,
                                        reservationPaid: myReservationDataView.reservationPaid,
                                        hoursToCheckin: myReservationDataView.hoursToCheckin,
                                        userAvailableToCheckin: myReservationDataView.userAvailableToCheckin,
                                        preAuthorizationPaid: myReservationDataView.preAuthorizationPaid,
                                        isFranchise: myReservationDataView.isFranchise,
                                        dueDate: myReservationDataView.dueDate,
                                        dueTime: myReservationDataView.dueTime,
                                        express: myReservationDataView.express,
                                        financialPendency: myReservationDataView.financialPendency)
    }
    
    private func checkToggles(){
        guard let currentStatus = reservationStep?.currentStatus else {
            reservationActionButton.isEnabled = false
            return
        }
        
        if reservationStep?.action == .editProfile {
            reservationActionButton.isEnabled = true
            return
        }
        
        if currentStatus == .paid || currentStatus == .checkin {
            reservationActionButton.isEnabled = false
        }
        
        if currentStatus == .paid {
            FirebaseRealtimeDatabase.toggleValue(withKey: .doPayment) { (toggle) in
                if !toggle {
                    self.reservationActionButton.isEnabled = false
                } else {
                    self.reservationActionButton.isEnabled = true
                }
            }
        }
        
        if currentStatus == .checkin {
            FirebaseRealtimeDatabase.toggleValue(withKey: .checkinReservation) { (toggle) in
                if !toggle {
                    self.reservationActionButton.isEnabled = false
                } else {
                    self.reservationActionButton.isEnabled = true
                }
            }
        }
    }
    
    private func checkStatusDone(reservationStatus: [ReservationStatus]) {
        
        buttons.forEach { normalState(button: $0) }
        
        doneButton(button: reservationButton)
        
        if !reservationStatus.filter({ $0 == .checkin }).isEmpty{
            doneButton(button: paymentButton)
            doneButton(button: qrCodeButton)
            return
        }
        
        if !reservationStatus.filter({ $0 == .paid }).isEmpty {
            doneButton(button: paymentButton)
            return
        }
    }
    
    private func doneButton(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
    }
    
    private func normalState(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
    }
}
