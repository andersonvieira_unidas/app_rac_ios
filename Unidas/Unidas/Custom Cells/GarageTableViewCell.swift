//
//  GarageTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

final class GarageTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var firstLineAddressLabel: UILabel?
    @IBOutlet weak var secondLineAddressLabel: UILabel?
    @IBOutlet weak var distanceLabel: UILabel?
    @IBOutlet weak var iconImageView: UIImageView?
    @IBOutlet weak var cardView: ShadowView?
    @IBOutlet weak var Hours24ImageView: UIImageView?

    
    func setup(with garage: GarageDataView, currentLocation: LocationDataView?) {

        self.selectionStyle = .none
        
        guard   let name = nameLabel, let firstLineAddress = firstLineAddressLabel,
                let secondLineAddress = secondLineAddressLabel, let distance = distanceLabel,
            let iconImage = iconImageView, let iconHours = Hours24ImageView  else { return }
        
        name.text = garage.name
        firstLineAddress.text = garage.address?.firstAddressLine
        secondLineAddress.text = garage.address?.secondAddressLine
        distance.text = garage.address?.formattedCalculeDistance(at: currentLocation)
        iconImage.image = garage.icon
        iconHours.image = garage.icon24Hours
        
    }
}
