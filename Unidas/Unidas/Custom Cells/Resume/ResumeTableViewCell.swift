//
//  ResumeTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 03/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ResumeTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    func setup(with fee: FeeDataView) {
        if let description = fee.description?.lowerCapitalize(), let calculation = fee.calculation?.description {
            titleLabel.text = "\(description) - \(calculation)"
        } else {
            titleLabel.text = fee.description?.lowerCapitalize()
        }
        detailLabel.text = fee.amount
    }
    
    func setup(with protection: ProtectionsDataView) {
        if let name = protection.name?.lowerCapitalize(), let value = protection.valueDescription {
           titleLabel.text = "\(name) - \(value)"
        } else {
            titleLabel.text = protection.name?.lowerCapitalize()
        }
        detailLabel.text = protection.totalValue
    }
    
    func setup(with equipment: EquipmentDataView) {
        if let name = equipment.name?.lowerCapitalize(), let value = equipment.valueQuantityDescription {
            titleLabel.text = "\(name) - \(value)"
        } else {
            titleLabel.text = equipment.name?.lowerCapitalize()
        }

        detailLabel.text = equipment.totalValueWithQuantity
    }
    
    func setup(with resumeInformation: ResumeInformationDataView) {
        
        if let title = resumeInformation.title?.lowerCapitalize(), let value = resumeInformation.subtitle {
            titleLabel.text = "\(title) - \(value)"
        } else {
            titleLabel.text = resumeInformation.title?.lowerCapitalize()
        }
        detailLabel.text = resumeInformation.value
    }
    
    func setup(with surplus: SurplusDataView) {
        titleLabel.text = surplus.description.lowerCapitalize()
        detailLabel.text = surplus.value
    }
    
   /* func setup(with inspectionSheetFailure: InspectionSheetFailureDataView) {
        titleLabel.text = inspectionSheetFailure.failureType?.name?.lowerCapitalize()
        detailLabel.text = inspectionSheetFailure.failureType?.value
    }*/
}
