//
//  ResumeHeaderFooterView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 03/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ResumeHeaderFooterView: UITableViewHeaderFooterView, NibReusable {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
}
