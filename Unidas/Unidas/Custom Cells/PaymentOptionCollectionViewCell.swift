//
//  PaymentOptionCollectionViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 10/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class PaymentOptionCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var roundedCheckImageView: UIImageView!
    @IBOutlet weak var paymentNameLabel: UILabel!
    @IBOutlet weak var paymentDescriptionLabel: UILabel!
    @IBOutlet weak var paymentValueLabel: UILabel!
    @IBOutlet weak var selectorBackView: UIView!
    
    static var height: CGFloat = 120.0
    
    override var isSelected: Bool {
        didSet {
            setSelected(isSelected)
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //applyShadow(for: rect)
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        
        let _ = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        let frame = layoutAttributes.frame
        //frame.size.height = 50
        layoutAttributes.frame = frame
        
        return layoutAttributes
    }
    
    func applyShadow(for rect: CGRect) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = shadowOpacity(forSelectedState: isSelected)
        layer.shadowOffset = shadowOffset(forSelectedState: isSelected)
        layer.shadowRadius = 4.0
        layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: 4.0).cgPath
        layer.shouldRasterize = true
    }
    
    func setup(with paymentOption: PaymentOptionDataView) {
        
        if paymentOption.model == .payAtPickup {
            paymentNameLabel.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
            paymentValueLabel.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        }
        
        paymentNameLabel.text = paymentOption.name
        paymentDescriptionLabel.text = paymentOption.description
    }
    
    private func setSelected(_ selected: Bool) {
        roundedCheckImageView.image = selected ? #imageLiteral(resourceName: "icon-checkmark-on-general") : #imageLiteral(resourceName: "icon-checkmark-off-background-white")
        
        let labelColor = selected ? #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1) : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        paymentNameLabel.textColor = labelColor
        paymentValueLabel.textColor = labelColor
        
        let boldText = selected ? UIFont.bold(ofSize: 13) : UIFont.regular(ofSize: 13)
        paymentDescriptionLabel.font = boldText
                
//        let shadowOffsetAnimation = CASpringAnimation(keyPath: #keyPath(CALayer.shadowOffset))
//        shadowOffsetAnimation.fromValue = shadowOffset(forSelectedState: !selected)
//        shadowOffsetAnimation.toValue = shadowOffset(forSelectedState: selected)
//
//        let shadowOpacityAnimation = CASpringAnimation(keyPath: #keyPath(CALayer.shadowPath))
//        shadowOpacityAnimation.fromValue = shadowOpacity(forSelectedState: !selected)
//        shadowOpacityAnimation.toValue = shadowOpacity(forSelectedState: selected)
//
//        let animationGroup = CAAnimationGroup()
//        animationGroup.animations = [shadowOffsetAnimation, shadowOpacityAnimation]
//
//        layer.add(animationGroup, forKey: "selection")
//        layer.shadowOffset = shadowOffset(forSelectedState: selected)
//        layer.shadowOpacity = shadowOpacity(forSelectedState: selected)
    }
    
    private func shadowOffset(forSelectedState selected: Bool) -> CGSize {
        return selected ? CGSize(width: 0, height: 2) : CGSize.zero
    }
    
    private func shadowOpacity(forSelectedState selected: Bool) -> Float {
        return selected ? 0.2 : 0.0
    }
    
}
