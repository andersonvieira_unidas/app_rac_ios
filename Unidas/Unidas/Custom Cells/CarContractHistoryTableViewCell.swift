//
//  CarContractHistoryTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 03/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class CarContractHistoryTableViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var licensePlate: UILabel!
    @IBOutlet weak var vehiclesNames: UILabel!
        
    func setup(contractDataView: ContractDataView) {
        groupDescriptionLabel.text = contractDataView.groupDescription
        groupNameLabel.text = contractDataView.groupCode
        licensePlate.text = contractDataView.vehicle?.licensePlate
        vehiclesNames.text = contractDataView.groupVehicles
    }
}
