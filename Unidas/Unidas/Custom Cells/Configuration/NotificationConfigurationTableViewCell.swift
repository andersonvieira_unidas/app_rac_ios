//
//  NotificationConfigurationTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol NotificationConfigurationTableViewCellDelegate {
    func changeActivate(id: Int, active: Bool)
}

class NotificationConfigurationTableViewCell: UITableViewCell, NibReusable {
    
    var delegate: NotificationConfigurationTableViewCellDelegate?
    var id: Int?
    
    @IBOutlet weak var notificationDescriptionLabel: UILabel?
    @IBOutlet weak var notificationSwitch: UISwitchCustom?
    
    func setup(notificationConfiguration: NotificationConfiguration?) {
        guard let notificationConfiguration = notificationConfiguration else { return }
        
        id = notificationConfiguration.code
        notificationDescriptionLabel?.text = notificationConfiguration.description
        notificationSwitch?.isOn = notificationConfiguration.active
        
        
        changeSwitch(switchOn: notificationConfiguration.active)
    }
    @IBAction func tapSwitch(_ sender: UISwitch) {
        changeSwitch(switchOn: sender.isOn)
    }
    
    private func changeSwitch(switchOn: Bool){
        if switchOn {
            notificationDescriptionLabel?.font = UIFont.bold(ofSize: 14)
        } else {
            notificationDescriptionLabel?.font = UIFont.regular(ofSize: 14)
        }
        if let id = id {
            self.delegate?.changeActivate(id: id, active: switchOn)
        }
    }
}
