//
//  ConfigurationItemTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class ConfigurationItemTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var configurationItemImage: UIImageView?
    @IBOutlet weak var configurationItemLabel: UILabel?
    @IBOutlet weak var configurationItemDotted: DottedLineView?
    
    func setup(configurationItemDataView: ConfigurationItemDataView?) {
        guard let configurationItem = configurationItemDataView else { return }
        self.configurationItemImage?.image = configurationItem.image
        self.configurationItemLabel?.text = configurationItem.name
        
        configurationItemDotted?.isHidden = !configurationItem.isVisibleDotted
    }
}
