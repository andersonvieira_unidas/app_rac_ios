//
//  MessageTableViewCell.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

enum MessageType: String {
    case pen = "PEN"
    case men = "MEN"
}

class MessageTableViewCell: UITableViewCell, NibReusable  {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var circleEnhancedView: EnhancedView!
    
    var messageType: String = ""
    
    func setup(messageDataView: MessageDataView) {
        
        let title = messageDataView.title ?? ""
        
        messageType = messageDataView.model.type
        
        if let date = messageDataView.date {
            titleLabel.text = "\(title) - \(date)"
        } else {
            titleLabel.text = title
        }
        
        descriptionLabel.text = messageDataView.message
        descriptionLabel.sizeToFit()
        
        if messageDataView.read {
            titleLabel.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
            circleEnhancedView.backgroundColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
            descriptionLabel.font = UIFont.regular(ofSize: 14)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
