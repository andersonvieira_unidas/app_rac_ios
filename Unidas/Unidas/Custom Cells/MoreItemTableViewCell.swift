//
//  MoreItemTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 21/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class MoreItemTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var moreItemImage: UIImageView?
    @IBOutlet weak var moreItemLabel: UILabel?
    @IBOutlet weak var moreItemDotted: DottedLineView?
    
    
    func setup(moreItemDataView: MoreItemDataView?) {
        guard let moreItem = moreItemDataView else { return }
        self.moreItemImage?.image = moreItem.image
        self.moreItemLabel?.text = moreItem.name
        
        moreItemDotted?.isHidden = !moreItem.isVisibleDotted
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

