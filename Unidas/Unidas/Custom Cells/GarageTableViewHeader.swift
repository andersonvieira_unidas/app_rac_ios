//
//  GarageTableViewHeader.swift
//  Unidas
//
//  Created by Anderson Vieira on 04/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import UIKit

class GarageTableViewHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var label: UILabel!
}
