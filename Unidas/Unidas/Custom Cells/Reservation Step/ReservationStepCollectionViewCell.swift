//
//  ReservationStepCollectionViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class ReservationStepCollectionViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var reservationButton: EnhancedButton!
    @IBOutlet weak var paymentButton: EnhancedButton!
    @IBOutlet weak var qrCodeButton: EnhancedButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func setup(reservationStep: ReservationStep){
        clearButtons()
        checkStatusDone(reservationStatus: reservationStep.status)
        
        reservationLabel.text = reservationStep.reservation
        titleLabel.text = reservationStep.title
        descriptionLabel.text = reservationStep.description
        
    }
    
    private func checkStatusDone(reservationStatus: [ReservationStatus]) {
        doneButton(button: reservationButton)
        
        if !reservationStatus.filter({ $0 == .checkin }).isEmpty{
            doneButton(button: paymentButton)
            doneButton(button: qrCodeButton)
            return
        }
        
        if !reservationStatus.filter({ $0 == .paid }).isEmpty {
            doneButton(button: paymentButton)
            return
        }
    }
    
    private func doneButton(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
    }
    
    private func clearButtons(){
        let buttons: [EnhancedButton] = [reservationButton, paymentButton, qrCodeButton]
        
        for button in buttons {
            button.tintColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
            button.borderColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        }
    }
}
