//
//  ReservationDetailsTableViewCell.swift
//  Unidas
//
//  Created by Mateus Padovani on 03/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ReservationDetailsTableViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var unity: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func setup(purchaseDataView: PurchaseDataView?) {
        title.text = purchaseDataView?.name
        unity.text = nil
        descriptionLabel.text = purchaseDataView?.description
    }
    
    func setup(contractDetailListItem: ContractDetailListItemDataView) {
        title.text = contractDetailListItem.description
        unity.text = nil
        descriptionLabel.text = contractDetailListItem.observation
    }
    
}
