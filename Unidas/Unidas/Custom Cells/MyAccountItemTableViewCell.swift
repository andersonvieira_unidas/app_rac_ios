//
//  MyAccountItemTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class MyAccountItemTableViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var myAccountItemImage: UIImageView?
    @IBOutlet weak var myAccountItemLabel: UILabel?
    @IBOutlet weak var myAccountItemDotted: DottedLineView?
    
    func setup(myAccountItemDataView: MyAccountItemDataView?) {
        guard let myAccountItem = myAccountItemDataView else { return }
        self.myAccountItemImage?.image = myAccountItem.image
        self.myAccountItemLabel?.text = myAccountItem.name
        
        myAccountItemDotted?.isHidden = !myAccountItem.isVisibleDotted
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
