//
//  OnboardHelpItemTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class OnboardHelpItemTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var helpLabel: UILabel!
    
    func setup(onboardHelpContent: OnboardHelpContent){
        let fontBold = UIFont.medium(ofSize: 12)
        let font = UIFont.regular(ofSize: 12)
        
        let description = onboardHelpContent.description ?? ""
        let attributedString = NSMutableAttributedString(string: description, attributes: [NSAttributedString.Key.font : font])
 
        let boldRanges = find(inText: description, pattern: "#b#")
        let colorAttribute: [NSAttributedString.Key : Any] = [.font: fontBold]

        boldRanges.reversed().forEach { (aResult) in
            let subTextNSRange = aResult.range(at: 1)
            guard let subTextRange = Range(subTextNSRange, in: attributedString.string) else { return }
            let replacementString = String(attributedString.string[subTextRange])
            let replacementAttributedString = NSAttributedString(string: replacementString, attributes: colorAttribute)
            attributedString.replaceCharacters(in: aResult.range, with: replacementAttributedString)
        }
        helpLabel.attributedText = attributedString
        icon.setImage(withURL: onboardHelpContent.ulrIcon)
    }
    
    func find(inText text: String, pattern: String) -> [NSTextCheckingResult] {
        let regex = try! NSRegularExpression(pattern:pattern+"(.*?)"+pattern, options: [])
        return regex.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.utf16.count))
    }
}
