//
//  ProfileItemTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class ProfileItemTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var profileItemImage: UIImageView?
    @IBOutlet weak var profileItemLabel: UILabel?
    @IBOutlet weak var profileItemDotted: DottedLineView?
    
    func setup(profileItemDataView: ProfileItemDataView?) {
        guard let profileItem = profileItemDataView else { return }
        self.profileItemImage?.image = profileItem.image
        self.profileItemLabel?.text = profileItem.name
        
        profileItemDotted?.isHidden = !profileItem.isVisibleDotted
    }
}
