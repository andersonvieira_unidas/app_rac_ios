//
//  NextReservationExpressTableViewCell.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 08/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//


import UIKit
import Reusable

protocol NextReservationExpressTableViewDelegate {
    func showDetails(reservation: VehicleReservationsDataView)
    func showAction(action: ReservationAction, reservation: VehicleReservationsDataView)
    func reloadNextReservationExpired()
}

class NextReservationExpressTableViewCell: UITableViewCell, NibReusable {
    var delegate: NextReservationExpressTableViewDelegate?
    
    @IBOutlet weak var reservationLabel: UILabel!
    
    @IBOutlet weak var reservationButton: EnhancedButton!
    @IBOutlet weak var paymentButton: EnhancedButton!
    @IBOutlet weak var qrCodeButton: EnhancedButton!
    @IBOutlet weak var informationView: UIView!
    
    
    @IBOutlet weak var reservationActionButton: UnidasButton!
    @IBOutlet weak var detailsButton: UIButton!
    
    @IBOutlet var buttons: [EnhancedButton]!
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.bold(ofSize: 14)
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.regular(ofSize: 14)
        label.numberOfLines = 2
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var stackWithCounter: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var stackWithInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var counterLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.light(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    var reservationStep: ReservationStep?
    var reservation: VehicleReservations?
    private var countdownTimer: Timer? = nil
    
    let detailsButtonAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.medium(ofSize: 14),
        .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    @IBAction func tapDetails(_ sender: Any) {
        if  let reservation = reservation {
            let vehicleReservationDataView = VehicleReservationsDataView(model: reservation)
            delegate?.showDetails(reservation: vehicleReservationDataView)
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        if let action = reservationStep?.action, let reservation = reservation {
            let vehicleReservationDataView = VehicleReservationsDataView(model: reservation)
            delegate?.showAction(action: action, reservation: vehicleReservationDataView)
        }
    }
    
    @objc func updateTime() {
        if let dateExpired = reservationStep?.expressDateExpired, !dateExpired, let dueDate = reservationStep?.dueDate {
            let timeElapsed = timeFormatted(dueDate: dueDate, dateNow: Date())
            counterLabel.text = timeElapsed
        }
    }
    
    func setup(myReservationDataView: MyReservationDataView){
        let reservationRulesDataView = createReservationRulesDataView(myReservationDataView: myReservationDataView)
        
        let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
 
        self.reservationStep = reservationRules.make()
        self.reservation = myReservationDataView.vehicleReservation
        titleLabel.text = reservationStep?.title
        descriptionLabel.text = reservationStep?.description
        
        
        if reservationStep?.express ?? false {
            reservationLabel.text = reservationStep?.reservation
        } else {
            reservationLabel.text = reservationStep?.reservation.uppercased()
        }
        
        if let status = reservationStep?.status {
            checkStatusDone(reservationStatus: status)
        }
        
        guard let step = reservationStep else { return }
        
        if step.financialPendency {
            titleLabel.textColor = #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
            errorState(button: paymentButton)
            setupInfo()
        } else {
            if let expressDateExpired = step.expressDateExpired, !expressDateExpired && !step.paid {
                countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
                
                titleLabel.font = UIFont.regular(ofSize: 14)
                titleLabel.numberOfLines = 2
                
                stackWithCounter.addArrangedSubview(titleLabel)
                stackWithCounter.addArrangedSubview(counterLabel)
                
                informationView.addSubview(stackWithCounter)
                
                NSLayoutConstraint.activate([
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    stackWithCounter.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
                    stackWithCounter.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
                    stackWithCounter.trailingAnchor.constraint(equalTo: informationView.trailingAnchor, constant: 0),
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    counterLabel.widthAnchor.constraint(equalToConstant: 75)
                ])
            } else if step.expressDateExpired ?? false && !step.paid {
                errorState(button: paymentButton)
                setupInfo()
            } else {
                setupInfo()
            }
        }
    
        reservationActionButton.setTitle(step.buttonTitle, for: .normal)
        reservationActionButton.isEnabled = step.buttonEnabled
        let attributeString = NSMutableAttributedString(string: NSLocalizedString("Visualize", comment: ""), attributes: detailsButtonAttributes)
        detailsButton.setAttributedTitle(attributeString, for: .normal)
        setupPaymentExpressToggle()
    }
    
    private func timeFormatted(dueDate: Date, dateNow: Date) -> String? {
        if reservationStep?.expressDateExpired ?? false {
            countdownTimer?.invalidate()
            countdownTimer = nil
            delegate?.reloadNextReservationExpired()
            return "00:00"
        } else {
            let timeInterval = dueDate.timeIntervalSince(dateNow)
            
            if timeInterval <= 0 {
                countdownTimer?.invalidate()
                countdownTimer = nil
                delegate?.reloadNextReservationExpired()
                return "00:00"
            }
            
            let interval = Int(timeInterval)
            let hour = interval / 3600
            let minutes = interval / (60) % 60
            
            return String(format: "%02d:%02d", hour, minutes)
        }
    }
    
    private func setupInfo(){
        informationView.subviews.forEach({ $0.removeFromSuperview() })
        titleLabel.font = UIFont.bold(ofSize: 14)
        
        stackWithInfo.addArrangedSubview(titleLabel)
        stackWithInfo.addArrangedSubview(descriptionLabel)
        
        informationView.addSubview(stackWithInfo)
        
        NSLayoutConstraint.activate([
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
            stackWithInfo.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
            stackWithInfo.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0)
        ])
    }
    
    private func createReservationRulesDataView(myReservationDataView: MyReservationDataView) -> ReservationRulesDataView {
        return ReservationRulesDataView(userValidated: myReservationDataView.userValidated,
                                        reservationNumber: myReservationDataView.reservationNumber,
                                        checkinAvailable: myReservationDataView.checkinAvailable,
                                        reservationPaid: myReservationDataView.reservationPaid,
                                        hoursToCheckin: myReservationDataView.hoursToCheckin,
                                        userAvailableToCheckin: myReservationDataView.userAvailableToCheckin,
                                        preAuthorizationPaid: myReservationDataView.preAuthorizationPaid,
                                        isFranchise: myReservationDataView.isFranchise,
                                        dueDate: myReservationDataView.dueDate,
                                        dueTime: myReservationDataView.dueTime,
                                        express: myReservationDataView.express,
                                        financialPendency: myReservationDataView.financialPendency)
    }
    
    func setupPaymentExpressToggle() {
        if reservationStep?.express == true {
            if let currentStatus = reservationStep?.currentStatus, currentStatus == .reservation {
                FirebaseRealtimeDatabase.toggleValue(withKey: .doPaymentExpress) { (value) in
                    if !value {
                        self.reservationActionButton.isEnabled = false
                    }
                }
            }
        }
    }
    
    private func checkStatusDone(reservationStatus: [ReservationStatus]) {
        
        buttons.forEach { normalState(button: $0) }
        
        doneButton(button: reservationButton)
        
        if !reservationStatus.filter({ $0 == .checkin }).isEmpty{
            doneButton(button: paymentButton)
            doneButton(button: qrCodeButton)
            return
        }
        
        if !reservationStatus.filter({ $0 == .paid }).isEmpty {
            doneButton(button: paymentButton)
            return
        }
        
        if reservationStep?.expressDateExpired ?? false {
            errorState(button: paymentButton)
        }
    }
    
    private func doneButton(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
    }
    
    private func normalState(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
    }
    
    private func errorState(button: EnhancedButton){
        button.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        button.borderColor = #colorLiteral(red: 0.9459537864, green: 0.4344328642, blue: 0.4142383933, alpha: 1)
    }
}
