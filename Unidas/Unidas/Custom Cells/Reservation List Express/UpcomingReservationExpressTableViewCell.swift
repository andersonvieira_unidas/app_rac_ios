//
//  UpcomingReservationExpressTableViewCell.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 13/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol UpcomingReservationExpressTableViewCellDelegate {
    func showUpcomingReservationExpressDetails(reservation: VehicleReservationsDataView)
    func reloadUpcomingReservationExpired()
}

class UpcomingReservationExpressTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var statusButton: EnhancedButton!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var informationView: UIView!
    
    var delegate: UpcomingReservationExpressTableViewCellDelegate?
    var reservation: VehicleReservationsDataView?
    var reservationStep: ReservationStep?
    private var countdownTimer: Timer? = nil
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.bold(ofSize: 14)
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.regular(ofSize: 14)
        label.numberOfLines = 2
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
    
    private lazy var stackWithCounter: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var stackWithInfo: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var counterLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.light(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.init(named: "textColor")
        return label
    }()
   
    
    let detailsButtonAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.medium(ofSize: 14),
    .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
    .underlineStyle: NSUnderlineStyle.single.rawValue]
    
    @IBAction func detailButtonTapped(_ sender: UIButton)
    {
        if let reservation = reservation {
            delegate?.showUpcomingReservationExpressDetails(reservation: reservation)
        }
    }

    func setup(with vehicleReservationDataView: VehicleReservationsDataView, rules: RulesDataView?, upcoming: Bool = false) {

        reservation = vehicleReservationDataView
        
        let attributeString = NSMutableAttributedString(string: NSLocalizedString("Visualize", comment: ""), attributes: detailsButtonAttributes)
        detailsButton.setAttributedTitle(attributeString, for: .normal)
        
        if let rules = rules {
            let reservationRulesDataView = createReservationRulesDataView(vehicleReservationsDataView: vehicleReservationDataView, rulesDataView: rules)
            let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
            reservationStep = reservationRules.make()

        } else {
            let rules = RulesDataView(pendencies: true, userAvailableToCheckin: false, financialPendency: false)
            
            let reservationRulesDataView = createReservationRulesDataView(vehicleReservationsDataView: vehicleReservationDataView, rulesDataView: rules)
            let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
            reservationStep = reservationRules.make()
        }
        guard let step = reservationStep else { return }
        
        if step.express {
            reservationLabel.text = step.reservation
        } else {
            reservationLabel.text = step.reservation.uppercased()
        }
            
        statusButton.setImage(statusImage(status: step.status), for: .normal)
        titleLabel.text = step.title
        descriptionLabel.text = step.description
        
        if step.financialPendency {
            titleLabel.textColor = #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
            setupInfo()
            errorState()
        } else {
            if let expressDateExpired = step.expressDateExpired, !expressDateExpired && !step.paid {
                countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
                
                titleLabel.font = UIFont.regular(ofSize: 14)
                titleLabel.numberOfLines = 2
                
                stackWithCounter.addArrangedSubview(titleLabel)
                stackWithCounter.addArrangedSubview(counterLabel)
                
                informationView.addSubview(stackWithCounter)
                
                NSLayoutConstraint.activate([
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    stackWithCounter.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
                    stackWithCounter.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
                    stackWithCounter.trailingAnchor.constraint(equalTo: informationView.trailingAnchor, constant: 0),
                    stackWithCounter.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
                    counterLabel.widthAnchor.constraint(equalToConstant: 75)
                ])
            } else if step.expressDateExpired ?? false && !step.paid {
                errorState()
                setupInfo()
            } else {
                setupInfo()
            }
        }
    }
    
    @objc func updateTime() {
        if let dateExpired = reservationStep?.expressDateExpired, !dateExpired, let dueDate = reservationStep?.dueDate {
            let timeElapsed = timeFormatted(dueDate: dueDate, dateNow: Date())
            counterLabel.text = timeElapsed
        }
    }
    
    
    //MARK:- Private functions
    
    private func setupInfo(){
        informationView.subviews.forEach({ $0.removeFromSuperview() })
        titleLabel.font = UIFont.bold(ofSize: 14)
        
        stackWithInfo.addArrangedSubview(titleLabel)
        stackWithInfo.addArrangedSubview(descriptionLabel)
        
        informationView.addSubview(stackWithInfo)
        
        NSLayoutConstraint.activate([
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0),
            stackWithInfo.bottomAnchor.constraint(equalTo: informationView.bottomAnchor, constant: 0),
            stackWithInfo.leadingAnchor.constraint(equalTo: informationView.leadingAnchor, constant: 0),
            stackWithInfo.topAnchor.constraint(equalTo: informationView.topAnchor, constant: 0)
        ])
    }
    
    private func timeFormatted(dueDate: Date, dateNow: Date) -> String? {
        if reservationStep?.expressDateExpired ?? false {
            countdownTimer?.invalidate()
            countdownTimer = nil
            delegate?.reloadUpcomingReservationExpired()
            return "00:00"
        } else {
            let timeInterval = dueDate.timeIntervalSince(dateNow)
            
            if timeInterval <= 0 {
                countdownTimer?.invalidate()
                countdownTimer = nil
                delegate?.reloadUpcomingReservationExpired()
                return "00:00"
            }
            
            let interval = Int(timeInterval)
            let hour = interval / 3600
            let minutes = interval / (60) % 60
            
            return String(format: "%02d:%02d", hour, minutes)
        }
    }
    
    private func statusImage(status: [ReservationStatus]) -> UIImage {
        if !status.filter({ $0 == .checkin }).isEmpty{
            return #imageLiteral(resourceName: "icon-qrcode-done")
        }
        
        if !status.filter({ $0 == .paid }).isEmpty {
            return #imageLiteral(resourceName: "icon-payment-done")
        }
        return #imageLiteral(resourceName: "icon-reservation-done")
    }
    
    private func errorState(){
        statusButton.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        statusButton.borderColor = #colorLiteral(red: 0.9459537864, green: 0.4344328642, blue: 0.4142383933, alpha: 1)
    }
    
    private func createReservationRulesDataView(vehicleReservationsDataView: VehicleReservationsDataView,
                                                   rulesDataView: RulesDataView) -> ReservationRulesDataView {
           return ReservationRulesDataView(userValidated: rulesDataView.userValidated,
                                           reservationNumber: vehicleReservationsDataView.reserveNumber,
                                           checkinAvailable: vehicleReservationsDataView.checkinAvailable,
                                           reservationPaid: vehicleReservationsDataView.reservationPaid,
                                           hoursToCheckin: vehicleReservationsDataView.hoursToCheckin,
                                           userAvailableToCheckin: rulesDataView.userAvailableToCheckin,
                                           preAuthorizationPaid: vehicleReservationsDataView.preAuthorizationPaid,
                                           isFranchise: vehicleReservationsDataView.isFranchise,
                                           dueDate: vehicleReservationsDataView.dueDate,
                                           dueTime: vehicleReservationsDataView.dueTime,
                                           express: vehicleReservationsDataView.express,
                                           financialPendency: rulesDataView.financialPendency)
       }
}

