//
//  ProtectionsContractHistoryCell.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 03/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol ProtectionsContractHistory {
    func didPressToolTipFromProtections(cell: ProtectionsContractHistoryCell, button: UIButton, observation: String?)
}

class ProtectionsContractHistoryCell: UITableViewCell, NibReusable  {
    
    var delegate: ProtectionsContractHistory?
    var observation: String?
    
    @IBOutlet weak var protectionsLabel: UILabel!
    @IBOutlet weak var toolTipButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    
    func setup(contractDetailListItem: ContractDetailListItemDataView, totalDiaries: String, hiddenPaymentInformation: Bool) {
        if let observation = contractDetailListItem.observation {
            self.observation = observation
            toolTipButton.layer.cornerRadius = toolTipButton.frame.size.width / 2
            toolTipButton.layer.masksToBounds = true
            toolTipButton.isHidden = false
            
        } else {
            toolTipButton.isHidden = true
        }
        
        let description = contractDetailListItem.description?.trimmingCharacters(in: .whitespacesAndNewlines)
        protectionsLabel.text = description?.lowerCapitalize()
        
        let localizedString = NSLocalizedString("Contract History Values Diaries @", comment: "")
        
        if hiddenPaymentInformation {
            valueLabel.text = ""
        } else {
            if let valueDiary = contractDetailListItem.value, contractDetailListItem.model.value != 0 {
                let valuesDiaries = String(format: localizedString, totalDiaries, valueDiary)
                valueLabel.text = valuesDiaries
            }
        }
    }
    
    func setup(purchaseDataView: PurchaseDataView, totalDiaries: String,  hiddenPaymentInformation: Bool){
        if let observation = purchaseDataView.description {
            self.observation = observation
            toolTipButton.layer.cornerRadius = toolTipButton.frame.size.width / 2
            toolTipButton.layer.masksToBounds = true
            toolTipButton.isHidden = false
        } else {
            toolTipButton.isHidden = true
        }
        
        let description = purchaseDataView.name?.trimmingCharacters(in: .whitespacesAndNewlines)
        protectionsLabel.text = description?.lowerCapitalize()
        
        let localizedString = NSLocalizedString("Contract History Values Diaries @", comment: "")
        if hiddenPaymentInformation {
            valueLabel.text = ""
        } else {
            if let valueDiary = purchaseDataView.value {
                let valuesDiaries = String(format: localizedString, totalDiaries, valueDiary)
                valueLabel.text = valuesDiaries
            }
        }
    }
    
    @IBAction func didPressToolTip(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.didPressToolTipFromProtections(cell: self, button: sender, observation: observation)
        }
    }
}
