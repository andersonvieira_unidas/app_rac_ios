//
//  ContractTableTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ContractTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var contractNumberLabel: UILabel!
    @IBOutlet weak var datePickupLabel: UILabel!
    @IBOutlet weak var dateReturnLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(contractDataView: ContractDataView) {
        contractNumberLabel.text = contractDataView.contract
        
        let until = NSLocalizedString("Contract List Until - Date", comment: "")
        datePickupLabel.text = String(format: until, contractDataView.pickUpDateShortFormatted ?? "")
        dateReturnLabel.text = contractDataView.returnDateShortFormatted
    }
    
}
