//
//  PendenciesCell.swift
//  Unidas
//
//  Created by Felipe Machado on 06/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit
import Reusable

protocol PendenciesButtonDelegate {
    func getPendencieButtonIndex(cell:PendenciesCell)
}

class PendenciesCell: UITableViewCell, NibReusable {
    
    var delegate: PendenciesButtonDelegate?
    
    @IBOutlet weak var pendenciesStatusImage: UIImageView!
    @IBOutlet weak var pendenciesLabel: UILabel!
    @IBOutlet weak var pendenciesButton: UnidasBorderButton!
    
    var pendencieType : PendencyType = .PersonalData
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func pressPendencieButton(_ sender: UnidasBorderButton) {
        if let delegate = delegate {
            delegate.getPendencieButtonIndex(cell: self)
        }
    }
    
    func setup(type: PendencyType, status:Bool) {
        
        pendencieType = type
        
        if status {
            pendenciesStatusImage.image = #imageLiteral(resourceName: "icon_pendencies_on")
            pendenciesButton.isHidden = true
        }else{
            pendenciesStatusImage.image = #imageLiteral(resourceName: "icon_pendencies_off")
        }
        
        switch type {
        case  .PersonalData:
            pendenciesLabel.text = NSLocalizedString("Pendencie Personal Data", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Alter", comment: ""), for: .normal)}
        case  .Documents:
            pendenciesLabel.text = NSLocalizedString("Pendencie Documents", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Add", comment: ""), for: .normal)}
        case  .Payments:
            pendenciesLabel.text = NSLocalizedString("Pendencie Payments", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Register", comment: ""), for: .normal)}
        case  .Signatures:
            pendenciesLabel.text = NSLocalizedString("Pendencie Signature", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Register", comment: ""), for: .normal)}
        case .Email:
            pendenciesLabel.text = NSLocalizedString("Pendencie Email", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Email", comment: ""), for: .normal)}
        case .Selfie:
            pendenciesLabel.text = NSLocalizedString("Pendencie Selfie", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Selfie", comment: ""), for: .normal)}
        case .Mobile:
            pendenciesLabel.text = NSLocalizedString("Pendencie Mobile", comment: "")
            if status == false { pendenciesButton.setTitle(NSLocalizedString("Pendencie Button Resend", comment: ""), for: .normal)}
        }
        
    }
}
