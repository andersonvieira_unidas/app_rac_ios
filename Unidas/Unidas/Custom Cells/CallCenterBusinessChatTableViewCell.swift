//
//  CallCenterBusinessChatTableViewCell.swift
//  Unidas
//
//  Created by Anderson Vieira on 04/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

class CallCenterBusinessChatTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var icon: EnhancedButton!
    
    func setup(title: String, description: String){
        titleLabel.text = title
        descriptionLabel.text = description
    }
}
