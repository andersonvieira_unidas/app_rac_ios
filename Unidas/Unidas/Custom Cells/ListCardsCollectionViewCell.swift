//
//  ListCardsCollectionViewCell.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol ListCardCollectionViewCellDelegate: class {
    func didTapMoreAction(_ listCardsCollectionViewCell: ListCardsCollectionViewCell)
}

class ListCardsCollectionViewCell: UICollectionViewCell, NibReusable {
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var cardDate: UILabel!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var checkedButton: CheckedButton!
    @IBOutlet weak var favIcon: UIImageView!
    @IBOutlet weak var moreActionsButton: UIButton!
    @IBOutlet weak var validTitleLabel: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    weak var delegate: ListCardCollectionViewCellDelegate?

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        apply(for: rect)
    }
    
    func apply(for rect: CGRect) {
//        layer.cornerRadius = 6.2
//        layer.borderWidth = 2.0
//        layer.masksToBounds = true
    }
    
    func applyCardDesigne() {
        backgroundCardView.layer.cornerRadius = 15
        cardName.text = cardName.text?.uppercased()
       
    }
    
    func applyValidCard() {
        //layer.borderColor = #colorLiteral(red: 0.6941176471, green: 0.6941176471, blue: 0.6941176471, alpha: 1)
        validTitleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cardDate.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    func applyInvalidCards() {
        layer.borderColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        validTitleLabel.textColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
        cardDate.textColor = #colorLiteral(red: 0.6196078431, green: 0.1450980392, blue: 0.1607843137, alpha: 1)
    }
    
    func selectedCard(_ selected: Bool) {
        checkedButton.isChecked = selected
    }
    
    func isHiddenButton(isHiddenCheckbox: Bool, isHiddenMoreActions: Bool) {
        checkedButton.isHidden = isHiddenCheckbox
        moreActionsButton.isHidden = isHiddenMoreActions
    }
    
    func setup(listCardDataView: ListCardDataView, isHiddenFavoriteIcon: Bool) {
        
        let localized = NSLocalizedString("Credit Card Vality", comment: "")
        cardNumber.text = listCardDataView.cardNumberShortFormatted
        cardName.text = listCardDataView.holder
        cardDate.text = "\(localized) \(listCardDataView.cardDate)"
        brandImage.image = listCardDataView.brandImage
        (listCardDataView.isValid) ? applyValidCard() : applyInvalidCards()
        setFavoriteCard(listCardDataView: listCardDataView, isHiddenFavoriteIcon: isHiddenFavoriteIcon)
        applyCardDesigne()
    }
    
    private func setFavoriteCard(listCardDataView: ListCardDataView, isHiddenFavoriteIcon: Bool) {
        if !isHiddenFavoriteIcon {
            favIcon.isHidden = (!listCardDataView.isFavoriteCard)
            return
        }
        
        favIcon.isHidden = isHiddenFavoriteIcon
    }
    
    @IBAction func tapMoreAction(_ sender: UIButton) {
        delegate?.didTapMoreAction(self)
    }
    
}
