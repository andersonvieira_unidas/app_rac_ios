//
//  CallCenterTableViewCell.swift
//  Unidas
//
//  Created by Mateus Padovani on 30/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class CallCenterTableViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var icon: EnhancedImageView!
    @IBOutlet weak var callButton: UIButton!
    
    var callCenterDataView: CallCenterDataView?
    
    func setup(callCenterDataView: CallCenterDataView?) {
        self.callCenterDataView = callCenterDataView
        
        self.title.text = callCenterDataView?.title
        self.descriptionLabel.text = callCenterDataView?.description
        self.icon.image = callCenterDataView?.image
        callButton.setTitle(callCenterDataView?.phoneNumber, for: .normal)
    }
    
    @IBAction func touchCall(_ sender: UIButton) {
        guard let formattedPhone = callCenterDataView?.formattedPhone else { return }
    
        if let url = URL(string: "tel://\(formattedPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
