//
//  CheckMarkOffersTableViewCell.swift
//  Unidas
//
//  Created by Felipe Machado on 10/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable

protocol OpenUrlDelegate {
    func openUrl(with url: URL)
}

class CheckMarkOffersTableViewCell: UITableViewCell, NibReusable, UITextViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellDescription: UITextView!
    
    var delegate: OpenUrlDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellImage.image = #imageLiteral(resourceName: "icon_afterTerms_elipse")
        cellImage.layer.masksToBounds = true
        cellDescription.font = UIFont.regular(ofSize: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(selected: Bool) {
        if selected {
            cellImage.image = #imageLiteral(resourceName: "icon_afterTerms_full_elipse")
            cellDescription.font = UIFont.bold(ofSize: 12)
        }else{
            cellImage.image = #imageLiteral(resourceName: "icon_afterTerms_elipse")
            cellDescription.font = UIFont.regular(ofSize: 12)
        }
    }
    
    func setUrl(description: String, urlPolicy: URL, font: UIFont){
        let description = description
        let linkText = NSLocalizedString("Privacy Policy Terms", comment: "")
        let colorFont = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        let rangeLink = (description as NSString).range(of: linkText)
        let policyText = NSMutableAttributedString(string: description, attributes: [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: colorFont])
        policyText.addAttribute(.link, value: urlPolicy, range: rangeLink)
                
        policyText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.foregroundColor, value: colorFont, range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.font, value: font, range: rangeLink)
        
        cellDescription.attributedText = policyText
        cellDescription.linkTextAttributes = NSAttributedString.linkAttributedWhite
        cellDescription.textAlignment = .left
        cellDescription.delegate = self
        cellDescription.isEditable = false
    }
    
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        delegate?.openUrl(with: url)
        return false
    }
}
