//
//  CheckmarkTableViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable
import SafariServices

protocol OpenSafatiFromContractURLDelegate {
    func openContractUrl(url: URL)
}

protocol SetAirLineInformationDelegate {
    func setAirLineInformation(flightNumber: String, flightCompany: FlightCompany)
}

class CheckmarkTableViewCell: UITableViewCell, NibReusable, UITextViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var delegate: OpenSafatiFromContractURLDelegate?
    var airLineDelegate: SetAirLineInformationDelegate?
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellDescription: UITextView!
    @IBOutlet weak var cellCiaAerea: UITextField!
    @IBOutlet weak var cellNVoo: UITextField!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var cellStackView: UIStackView!
    
    var flightCompanies: [FlightCompany] = []
    var flightCompanie = FlightCompany(code: "", description: "")
    
    lazy var flightCompanyPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.sizeToFit()
        return pickerView
    }()
    
    lazy var flightCompanyToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissFlightComanyTextField(_:)))
        toolbar.setItems([flexibleSpaceItem, doneButtonItem], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    @objc func dismissFlightComanyTextField(_ sender: Any) {
        setAirLineInformation()
        cellCiaAerea.resignFirstResponder()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let labelFont = selected ? UIFont.bold(ofSize: 13) : UIFont.regular(ofSize: 13)
        cellImage.image = selected ? #imageLiteral(resourceName: "icon-checkmark-on-general") : #imageLiteral(resourceName: "icon-checkmark-off-background-white")
        cellDescription.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        cellDescription.font = labelFont
        cellDescription.textAlignment = .left
        cellCiaAerea.delegate = self
        cellNVoo.delegate = self
    }
    
    func setUrl(urlPolicy: URL?, urlPolicy2: URL?){
        
        cellDescription.delegate = self
        
        var refundPolicyText = String()
        var refundPolicyLinkText = String()
        var refundPolicyLinkText2 = String()
        
        let colorFont = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        
        switch cellDescription.text {
        case NSLocalizedString("I declare that I have read the Refund Policy in case of cancellation", comment: ""):
            refundPolicyText = NSLocalizedString("I declare that I have read the Refund Policy in case of cancellation", comment: "")
            refundPolicyLinkText = NSLocalizedString("Cancelation Policy in case of cancelation", comment: "")
            
            let attributedString = NSMutableAttributedString(string: refundPolicyText, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 13), NSAttributedString.Key.foregroundColor: colorFont])

            let attrs: [NSAttributedString.Key: Any] = [
                .font: UIFont.bold(ofSize: 13),
                .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .link: urlPolicy as Any]

            let rangeLink = (refundPolicyText as NSString).range(of: refundPolicyLinkText)
            attributedString.addAttributes(attrs, range: rangeLink)
            
            cellDescription.attributedText = attributedString
            
        case NSLocalizedString("I am aware and in accordance with the Rental Requirements and the Contractual Conditions", comment: ""):
            refundPolicyText = NSLocalizedString("I am aware and in accordance with the Rental Requirements and the Contractual Conditions", comment: "")
            refundPolicyLinkText = NSLocalizedString("Rental Requirements", comment: "")
            refundPolicyLinkText2 = NSLocalizedString("Contract Conditions", comment: "")
            
            let attributedString = NSMutableAttributedString(string: refundPolicyText, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 13), NSAttributedString.Key.foregroundColor: colorFont])
            
            let attrs: [NSAttributedString.Key: Any] = [
                .font: UIFont.bold(ofSize: 13),
                .underlineStyle: NSUnderlineStyle.single.rawValue,
                .link: urlPolicy as Any,
                .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)]
            
            let rangeLink = (refundPolicyText as NSString).range(of: refundPolicyLinkText)
            attributedString.addAttributes(attrs, range: rangeLink)
            
            if let secondUrl = urlPolicy2 {
                let attrs2: [NSAttributedString.Key: Any] = [
                    .font: UIFont.bold(ofSize: 13),
                    .underlineStyle: NSUnderlineStyle.single.rawValue,
                    .link: secondUrl,
                    .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)]
                
                let rangeLink2 = (refundPolicyText as NSString).range(of: refundPolicyLinkText2)
                attributedString.addAttributes(attrs2, range: rangeLink2)
            }
            cellDescription.attributedText = attributedString
            
        case NSLocalizedString("Are you coming by Airline?", comment: ""):
            descriptionHeight.constant = 50
            let attributedStringZero = NSMutableAttributedString(string: cellDescription.text, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 13), NSAttributedString.Key.foregroundColor: colorFont])
            cellDescription.attributedText = attributedStringZero
            cellDescription.centerVerticalText()
        default:
            let attributedStringZero = NSMutableAttributedString(string: cellDescription.text, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 13), NSAttributedString.Key.foregroundColor: colorFont])
            cellDescription.attributedText = attributedStringZero
        }
    }
    
    func setAirLineInformation() {
        if let _ = airLineDelegate {
            airLineDelegate?.setAirLineInformation(flightNumber: cellNVoo.text!, flightCompany: flightCompanie)
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if let _ = delegate {
            delegate?.openContractUrl(url: url)
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let result = textField == cellCiaAerea ? false : true
        return result
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == cellCiaAerea {
            textField.inputView = self.flightCompanyPickerView
            textField.inputAccessoryView = self.flightCompanyToolbar
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.contentView.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setAirLineInformation()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return flightCompanies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        cellCiaAerea.text = flightCompanies[row].description
        flightCompanie = flightCompanies[row]
        return flightCompanies[row].description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cellCiaAerea.text = flightCompanies[row].description
    }
}

extension UITextView {

    func centerVerticalText() {
        self.textAlignment = .center
        let fitSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fitSize)
        let calculate = (bounds.size.height - size.height * zoomScale) / 2
        let offset = max(1, calculate)
        contentOffset.y = -offset
    }
}
