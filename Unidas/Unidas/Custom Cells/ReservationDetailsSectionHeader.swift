//
//  ReservationDetailsSectionHeader.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ReservationDetailsSectionHeader: UITableViewHeaderFooterView, NibReusable {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expandCloseButton: UIButton!
}
