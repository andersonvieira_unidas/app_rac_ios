//
//  ImageCollectionViewCell.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 30/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class ImageCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dimmingView: UIView!
    
    override var isHighlighted: Bool {
        didSet {
            dimmingView.alpha = isHighlighted ? 0.3 : 0.0
        }
    }
    
}
