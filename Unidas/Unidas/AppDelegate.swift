//
//  AppDelegate.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 14/02/18.
//  Copyright © 2018 Unidas. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import FirebaseCore
import FirebaseDynamicLinks
import FacebookCore
import Adjust
import FirebaseDatabase
import MarketingCloudSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, AdjustDelegate {
    
    var window: UIWindow?
    var restrictRotation:UIInterfaceOrientationMask = .all
    let notificationCenter = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        var filePath:String!
        #if DEVELOPMENT
        print("[FIREBASE] Development mode.")
        filePath = Bundle.main.path(forResource: "GoogleService-Info-Dev", ofType: "plist", inDirectory: "")
        #else
        print("[FIREBASE] Production mode.")
        filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "")
        #endif
        
        let options = FirebaseOptions.init(contentsOfFile: filePath)!
        FirebaseApp.configure(options: options)
        Database.database().isPersistenceEnabled = true
        
        DynamicLinks.performDiagnostics(completion: nil)
        
        session.validateSession()
        splashScreen()
        setupTabBarSettings()
        registerShortcuts()
        
        //Facebook Instace
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        
        //Push Notification
        Messaging.messaging().delegate = self
        resetNotificationBadge()
        checkPreferencesForPushNotification(application: application)
        //registerRemoteNotifications(application: application)
        //------------------
        
        if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
            handleShortCutItem(shortcutItem: shortcutItem)
        }
        
        setupNavigationBar()
        configureAdjustSdk()
        
        #if !DEVELOPMENT || !DEBUG
        configureMarketingCloudSDK()
        #endif
        
        return true
    }
    
    //MARK: - Push Notification
    ///////////////////////////////////// - Push Notification - //////////////////////////////////////
    
    func checkPreferencesForPushNotification(application: UIApplication) {
        let defaults = UserDefaults.standard
        let sharedPrefForOnboard = UserDefaults.standard.string(forKey: "appVersion")
        let sharedPrefForPush = UserDefaults.standard.string(forKey: "pushNotification")
        
        if sharedPrefForOnboard != nil && sharedPrefForPush == nil {
            registerRemoteNotifications(application: application)
            defaults.set(sharedPrefForPush, forKey: "pushNotification")
        }
    }
    
    func registerRemoteNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            notificationCenter.delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
                if granted {
                    self.grantedAllNotifications()
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    func connectToFCM() {
        let _ = InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error.localizedDescription)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    
    func resetNotificationBadge() {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    //Mark: - FCM Delegates
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        connectToFCM()
        
//        let token = AppPersistence.getValue(withKey: "fcmToken")
//        let tokenCheck = fcmToken == token ? true : false
//
//        if tokenCheck {
//            AppPersistence.save(key: "fcmTokenValidator", value: true)
//        }else{
//            AppPersistence.save(key: "fcmTokenValidator", value: false)
//        }
        
        AppPersistence.save(key: "fcmToken", value: fcmToken)
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        Messaging.messaging().apnsToken = deviceToken
        
        #if !DEVELOPMENT || !DEBUG
        MarketingCloudSDK.sharedInstance().sfmc_setDeviceToken(deviceToken)
        #endif
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber += 1
        print("Original Remote Notification:\n\(userInfo)")
        
        #if !DEVELOPMENT || !DEBUG
        MarketingCloudSDK.sharedInstance().sfmc_setNotificationUserInfo(userInfo)
        #endif
    
        completionHandler(.newData)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print(notification.request.content.categoryIdentifier)
        if notification.request.content.categoryIdentifier ==
            "CustomSamplePush" {
        }
          
        if let userInfo = notification.request.content.userInfo as? [String : AnyObject] {
            print(userInfo)
        }
        completionHandler([.alert, .sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        #if !DEVELOPMENT || !DEBUG
        MarketingCloudSDK.sharedInstance().sfmc_setNotificationRequest(response.notification.request)
        #endif
        
        if let userInfo = response.notification.request.content.userInfo as? [String : AnyObject] {
            print(userInfo)
        }
        if response.notification.request.content.categoryIdentifier == "CustomSamplePush" {
            switch response.actionIdentifier
            {
            case "remindLater":
                print("remindLater")
                completionHandler()
            case "accept":
                print("accept")
                completionHandler()
            case "decline":
                print("decline")
                completionHandler()
            case "comment":
                print("comment")
                completionHandler()
            default:
                break
            }
        }
    }
    
    //MARK: - Dynamic Links
    ///////////////////////////////////// - Dynamic Links - //////////////////////////////////////
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            handleDynamicLink(dynamicLink)
            return true
        }
        return false
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamiclink, error) in
                if let dynamiclink = dynamiclink, let _ = dynamiclink.url {
                    self.handleDynamicLink(dynamiclink)
                }
            })
            return linkHandled
        }
        return false
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        handleShortCutItem(shortcutItem: shortcutItem)
    }
    
    func registerShortcuts(){
        ShortcutParser.shared.registerShortcuts(for: ProfileType.reservation)
        ShortcutParser.shared.registerShortcuts(for: ProfileType.mycar)
        ShortcutParser.shared.registerShortcuts(for: ProfileType.profile)
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        if session.currentUser == nil { return }
        
        if let url = dynamicLink.url,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: false),
            let queryItems = components.queryItems{
            for queryItem in queryItems {
                if queryItem.name == "reservaApp", let reservationCode = queryItem.value {
                    redirectToReservationDetails(reservationCode: reservationCode)
                }
            }
        }
    }
    
    func handleShortCutItem(shortcutItem: UIApplicationShortcutItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let main = storyboard.instantiateInitialViewController() as? MainTabBarViewController {
            
            self.window?.rootViewController = main
            
            let type = shortcutItem.type
            let types = type.components(separatedBy: ":")
            
            let tabNo = (types[0] as NSString).substring(from: 3)
            
            var index = Int()
            
            switch tabNo {
            case "com.unidas.apprac.ios.debug.home":
                index = 0
            case "com.unidas.apprac.ios.debug.alerts":
                index = 1
            case "com.unidas.apprac.ios.debug.reservations":
                index = 2
            case "com.unidas.apprac.ios.debug.profile":
                index = 3
            case "com.unidas.apprac.ios.debug.more":
                index = 4
            default:
                index = 0
            }
            
            main.selectedIndex = index
            let navController = main.selectedViewController as? UINavigationController
            navController?.popToRootViewController(animated: false)
        }
    }
    
    func redirectToReservationDetails(reservationCode: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if let main = storyboard.instantiateInitialViewController() as? MainTabBarViewController {
            main.reservationCode = reservationCode
            self.window?.rootViewController = main
            self.window?.makeKeyAndVisible()
        }
    }
    
    //MARK: - Rotations
    ///////////////////////////////////// - Rotations - //////////////////////////////////////
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.restrictRotation
    }
    
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.restrictRotation = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
    //MARK: - Splash Screen
    ///////////////////////////////////// - Splash Screen - //////////////////////////////////////
    
    private func loadAppInTabBarIndex(position: Int) {
        if let tabBarController = self.window!.rootViewController as? UITabBarController {
            tabBarController.selectedIndex = position
        }
    }
    
    private func splashScreen(){
        let launchScreenVC = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let rootVC = launchScreenVC.instantiateViewController(withIdentifier: "SplashController")
        self.window?.rootViewController = rootVC
        self.window?.makeKeyAndVisible()
    }
    
    private func setupTabBarSettings(){
        let fontAttributes = [NSAttributedString.Key.font: UIFont.regular(ofSize: 12)]
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .selected)
        loadAppInTabBarIndex(position: 0)
    }
    
    private func setupNavigationBar() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let insets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        
        /*if #available(iOS 13.0, *) {
         let coloredAppearance = UINavigationBarAppearance()
         
         coloredAppearance.configureWithOpaqueBackground()
         coloredAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
         coloredAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
         
         let buttonAppearance = UIBarButtonItemAppearance(style: .plain)
         buttonAppearance.normal.backgroundImage = #imageLiteral(resourceName: "icon-back-button")
         
         coloredAppearance.buttonAppearance = buttonAppearance
         
         UINavigationBar.appearance().standardAppearance = coloredAppearance
         UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
         
         } else {*/
        let backButton = #imageLiteral(resourceName: "icon-back-button")
        let backButtonWithPadding = backButton.imageWith(padding: insets)
        
        UINavigationBar.appearance().backIndicatorImage = backButtonWithPadding
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backButtonWithPadding
        // }
    }
    
    private func grantedAllNotifications(){
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async{
                if let user = session.currentUser {
                    let notificationConfigurationService = NotificationConfigurationService()
                    notificationConfigurationService.fetch(documentNumber: user.account.documentNumber, response: (success: { notifications in
                        guard let items = notifications else { return }
                        
                        var notificationsSave: [NotificationConfiguration] = []
                        
                        for item in items {
                            let notification = NotificationConfiguration(code: item.code, description: item.description, active: true)
                            notificationsSave.append(notification)
                        }
                        
                        notificationConfigurationService.updateNotifications(documentNumber: user.account.documentNumber, notifications: notificationsSave, response: (success: {
                            }, failure: { error in
                               print(error)
                        }, completion: {
                           // self.delegate?.endLoading()
                            
                        })
                        )
 
                        }, failure: { error in
                    }, completion: {
                    })
                    )
                } else {
                    AppPersistence.save(key: AppPersistence.postNotificationPermission, value: true)
                }
                
            }
        }
    }
    
    private func configureAdjustSdk(){
        let appToken = "gtkb6sd8p6gw"
        
        #if DEVELOPMENT
        print("[ADJUST] Development mode.")
        let adjustConfig = ADJConfig(appToken: appToken, environment: ADJEnvironmentSandbox)
        adjustConfig?.logLevel = ADJLogLevelVerbose
        adjustConfig?.delegate = self
        Adjust.appDidLaunch(adjustConfig)
 
        #else
        print("[ADJUST] Production mode.")
        let adjustConfig = ADJConfig(appToken: appToken, environment: ADJEnvironmentProduction)
        adjustConfig?.setAppSecret(1, info1: 1176870074, info2: 738339636, info3: 1019057549, info4: 215271883)
        adjustConfig?.delegate = self
        Adjust.appDidLaunch(adjustConfig)
        #endif
        
    }
    
    @discardableResult
    private func configureMarketingCloudSDK() -> Bool {
            
        let mid = "514002873"
        let appEndpoint = "https://mc6zmhpdd8s893qk50y0wwjnf1wm.device.marketingcloudapis.com/"
        let appID = "5e15c6d9-9a00-4e75-9b35-f47bc6e17f5d"
        let accessToken = "p1o3djRYdfJ3v5Ul6pRVcSfv"

        let inbox = false
        let location = false
        let analytics = true
        
        let builder = MarketingCloudSDKConfigBuilder()
            .sfmc_setApplicationId(appID)
            .sfmc_setAccessToken(accessToken)
            .sfmc_setMarketingCloudServerUrl(appEndpoint)
            .sfmc_setMid(mid)
            .sfmc_setInboxEnabled(inbox as NSNumber)
            .sfmc_setLocationEnabled(location as NSNumber)
            .sfmc_setAnalyticsEnabled(analytics as NSNumber)
            .sfmc_build()!
        
        var success = false
        
        do {
            try MarketingCloudSDK.sharedInstance().sfmc_configure(with:builder)
            MarketingCloudSDK.sharedInstance().sfmc_setPushEnabled(true)
            print("push enabled: \(MarketingCloudSDK.sharedInstance().sfmc_pushEnabled())")
            success = true
        
        } catch let error as NSError {
            let configErrorString = String(format: "MarketingCloudSDK sfmc_configure failed with error = %@", error)
            print(configErrorString)
        }
        
        if success {
            #if DEBUG
            MarketingCloudSDK.sharedInstance().sfmc_setDebugLoggingEnabled(true)
            #endif
        }
        return success
    }
}

