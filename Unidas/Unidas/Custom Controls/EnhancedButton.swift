//
//  EnhancedButton.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

@IBDesignable
class EnhancedButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var leftImage: UIImage? = nil {
        didSet {
            updateLeftImage()
        }
    }
    
    @IBInspectable var rightImage: UIImage? = nil {
        didSet {
            updateRightImage()
        }
    }
    
    @IBInspectable var adjustsFontSizeToFitWidth: Bool = false {
        didSet {
            self.titleLabel?.adjustsFontSizeToFitWidth = true
            self.titleLabel?.numberOfLines = 1;
            self.titleLabel?.minimumScaleFactor = 0.1
            self.titleLabel?.lineBreakMode = .byClipping
        }
    }
    
    var trailingMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var leadingMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var topMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var bottomMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    
    let leftImageView = UIImageView()
    let rightImageView = UIImageView()
    
    func updateLeftImage() {
        leftImageView.image = leftImage
        leftImageView.contentMode = .scaleAspectFill
        leftImageView.tintColor = UIColor.init(named: "tintColor")
        self.addSubview(leftImageView)
        
        leadingMargin = leftImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0)
        topMargin = leftImageView.topAnchor.constraint(equalTo: topAnchor, constant: 18.0)
        bottomMargin = leftImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -18.0)
        
        leftImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([leadingMargin, topMargin, bottomMargin])
    }
    
    func updateRightImage() {
        rightImageView.image = rightImage
        rightImageView.contentMode = .scaleAspectFill
        
        self.addSubview(rightImageView)
        
        trailingMargin = rightImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0)
        topMargin      = rightImageView.topAnchor.constraint(equalTo: topAnchor, constant: 18.0)
        bottomMargin   = rightImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -18.0)
        
        rightImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([trailingMargin, topMargin, bottomMargin])
    }
    
}
