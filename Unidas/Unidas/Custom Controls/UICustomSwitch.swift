//
//  UICustomSwitch.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class UISwitchCustom: UISwitch {
    @IBInspectable var offTint: UIColor? {
        didSet {
            self.tintColor = offTint
            self.backgroundColor = offTint
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 16.0 {
        didSet {
        
          // self.layer.cornerRadius = cornerRadius
        }
    }
}
