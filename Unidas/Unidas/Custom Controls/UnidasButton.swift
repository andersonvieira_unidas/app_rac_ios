//
//  UnidasButton.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/06/19.
//  Copyright © 2019 Unidas. All rights reserved.
//


import UIKit

@IBDesignable
class UnidasButton: UIButton {
    
    let defaultBackground: UIColor = #colorLiteral(red: 0.2274329066, green: 0.5870787501, blue: 0.9447389245, alpha: 1)
   // let highlightedBackground: UIColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
    let disabledBackground: UIColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
    let defaultTextColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = self.cornerRadius
            layer.masksToBounds = self.cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? = nil {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var buttonFont: UIFont? = nil {
        didSet {
            self.titleLabel?.font = UIFont.medium(ofSize: 100)
        }
    }
    
    @IBInspectable var leftImage: UIImage? = nil {
        didSet {
            updateLeftImage()
        }
    }
    
    @IBInspectable var rightImage: UIImage? = nil {
        didSet {
            updateRightImage()
        }
    }
    
    @IBInspectable var adjustsFontSizeToFitWidth: Bool = false {
        didSet {
            self.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    /*override open var isHighlighted: Bool {
        didSet {
            self.backgroundColor = isHighlighted ? highlightedBackground : defaultBackground
            self.titleLabel?.textColor = defaultTextColor
            
        }
    }*/
    
    override open var isEnabled: Bool {
        didSet {
            self.backgroundColor = isEnabled ? disabledBackground : defaultBackground
            self.titleLabel?.textColor = defaultTextColor
        }
    }
    
    var trailingMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var leadingMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var topMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    var bottomMargin: NSLayoutConstraint! { didSet { setNeedsLayout() } }
    
    let leftImageView = UIImageView()
    let rightImageView = UIImageView()
    
    func updateLeftImage() {
        leftImageView.image = leftImage
        leftImageView.contentMode = .scaleAspectFill
        
        self.addSubview(leftImageView)
        
        leadingMargin = leftImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0)
        topMargin = leftImageView.topAnchor.constraint(equalTo: topAnchor, constant: 18.0)
        bottomMargin = leftImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -18.0)
        
        leftImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([leadingMargin, topMargin, bottomMargin])
        
        self.titleLabel?.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 52).isActive = true
    }
    
    func updateRightImage() {
        rightImageView.image = rightImage
        rightImageView.contentMode = .scaleAspectFill
        
        self.addSubview(rightImageView)
        
        trailingMargin = rightImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0)
        topMargin      = rightImageView.topAnchor.constraint(equalTo: topAnchor, constant: 18.0)
        bottomMargin   = rightImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -18.0)
        
        rightImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([trailingMargin, topMargin, bottomMargin])
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        shared()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        shared()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        shared()
    }
    
    func shared() {
        
        let controlStates = [UIControl.State]()
        controlStates.forEach { setTitleColor(defaultTextColor, for: $0) }
        
        self.setBackgroundColor(color: defaultBackground, forState: .normal)
        //self.setBackgroundColor(color: highlightedBackground, forState: .highlighted)
        self.setBackgroundColor(color: disabledBackground, forState: .disabled)
        
        cornerRadius = 25
        self.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.titleLabel?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        titleLabel?.font =  UIFont.medium(ofSize: 16)
    }

}
