//
//  ShortcutIdentifier.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 19/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum ShortcutIdentifier: String {
    case reservations
    case mycar
    case profile
}

extension ShortcutIdentifier {
    
    init?(fullIdentifier: String) {
        guard let identifier = fullIdentifier.components(separatedBy: ".").last else { return nil }
        self.init(rawValue: identifier)
    }   
}
