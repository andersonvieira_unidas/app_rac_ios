//
//  ProfileLoggedPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileLoggedViewDelegate: class {
    func showMoreHeaderLoginOnView(userResponseDataView: UserResponseDataView)
    func performSegue(identifier: String)
    func showLogoutConfirmationMessage()
    func removeBadge()
}

protocol ProfileLoggedDelegate {
    var delegate: ProfileLoggedViewDelegate? { get set }
    var profileItems: [ProfileItem] { get }
    func showUserHeaderView()
    func setupHeaderProfile()
    func profileItem(at: Int) -> ProfileItemDataView
    func to(segueIdentifier: String)
    func logout()
    func addEventSessionEnd(success: Bool)
}

class ProfileLoggedPresenter: ProfileLoggedDelegate {
    weak var delegate: ProfileLoggedViewDelegate?
    var profileItems: [ProfileItem] = []
    private var reservationDataStore: ReservationDataStore
    private var imageTemporaryDirectory: ImageTemporaryDirectory
    private var unidasUserStatusService: UnidasUserStatusService
     private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(reservationDataStore: ReservationDataStore, imageTemporaryDirectory: ImageTemporaryDirectory, unidasUserStatusService: UnidasUserStatusService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.reservationDataStore = reservationDataStore
        self.imageTemporaryDirectory = imageTemporaryDirectory
        self.unidasUserStatusService = unidasUserStatusService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogin(_ :)), name: .UNUserDidLogin, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogout), name: .UNUserDidLogout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogin(_ :)), name: .UNDidUpdateCurrentUser, object: nil)
        insertItems()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    @objc func userDidLogin(_ notification: Notification) {
        guard let currentUser = notification.object as? UserResponse else { return }
        delegate?.showMoreHeaderLoginOnView(userResponseDataView: UserResponseDataView(model: currentUser))
    }
    
    @objc func userDidLogout(_ notification: Notification) {
    }
    
    @objc func showUserHeaderView() {
        guard let currentUser = currentUser else { return }
        imageTemporaryDirectory.addImage(userResponse: currentUser, type: .profile)
        delegate?.showMoreHeaderLoginOnView(userResponseDataView: UserResponseDataView(model: currentUser))
    }

    func to(segueIdentifier: String) {
        delegate?.performSegue(identifier: segueIdentifier)
    }
    
    func setupHeaderProfile() {
        guard let currentUser = session.currentUser else { return }
        delegate?.showMoreHeaderLoginOnView(userResponseDataView: UserResponseDataView(model: currentUser))
    }
    
    private func closureExit (){
        delegate?.showLogoutConfirmationMessage()
    }
    
    func addEventSessionEnd(success: Bool) {
           var parameters: [String : Any] = [:]
           parameters["success"] = success
           firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.sessionEnd, withValue: parameters)
    }
    
    private func insertItems(){
        let editProfile = ProfileItem(image: "icon-menu-edit-profile", localizedString: "Profile Menu Item Edit Profile", action: "showMyAccount")
        profileItems.append(editProfile)
        
        let contractHistory = ProfileItem(image: "icon-menu-historic", localizedString: "Profile Menu Item Contract History", action: "showContractHistory")
        profileItems.append(contractHistory)
        
        let loyalty = ProfileItem(image: "icon-menu-loyaltie-program", localizedString: "Profile Menu Item Loyalty", action: "showLoyaltyProgram")
        profileItems.append(loyalty)
        
        let debit = ProfileItem(image: "icon-menu-debits", localizedString: "Profile Menu Item Debit", action: "showMyDebits")
        profileItems.append(debit)
        
        let configuration = ProfileItem(image: "icon-configuration", localizedString: "Profile Menu Item Configuration", action: "showConfigurations")
        //profileItems.append(configuration)

        let logout = ProfileItem(image: "icon-logout", localizedString: "Profile Menu Item Logout", action: nil, separator: false, closure: closureExit)
        profileItems.append(logout)

    }
    
    func profileItem(at: Int) -> ProfileItemDataView {
       return ProfileItemDataView(model: profileItems[at])
    }
    
    func logout() {
        session.currentUser = nil
        reservationDataStore.reset()
        ServiceConfiguration.unidas.headers?.remove(name: "Authorization")
        NotificationCenter.default.post(name: .UNUserDidLogout, object: nil)
        
        self.delegate?.removeBadge()
    }
}

