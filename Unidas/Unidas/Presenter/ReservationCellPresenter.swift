//
//  ReservationCellPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol ReservationCellViewDelegate: class {
    func showInstallmentsValue(installments: String, vehicleReservationsDataView: VehicleReservationsDataView)
    func showCheckInButton(vehicleReservationsDataView: VehicleReservationsDataView)
    func showUserInValidate(message: String)
    func doReservationPayment()
    func enablePaymentButton()
}

protocol ReservationCellPresenterDelegate {
    var delegate: ReservationCellViewDelegate? { get set }
    var doPaymentToggle: Bool { get set }
    
    func getInstallments()
    func getCheckinButton(canCheckin: Bool, vehicleReservationsDataView: VehicleReservationsDataView)
    func checkUserValidate()
    func calendarReservation(vehicleReservationsDataView: VehicleReservationsDataView)
}

class ReservationCellPresenter: ReservationCellPresenterDelegate {

    weak var delegate: ReservationCellViewDelegate?
    
    private var vehicleReservations: VehicleReservations
    private var paymentService: PaymentService
    private var reservationUnidasService: ReservationUnidasService
    private var unidasUserStatusService: UnidasUserStatusService
    
    var doPaymentToggle: Bool = true
    
    init(vehicleReservationsDataView: VehicleReservationsDataView, paymentService: PaymentService,
         reservationUnidasService: ReservationUnidasService, unidasUserStatusService: UnidasUserStatusService) {
        self.vehicleReservations = vehicleReservationsDataView.model
        self.paymentService = paymentService
        self.reservationUnidasService = reservationUnidasService
        self.unidasUserStatusService = unidasUserStatusService
    }
    
    func getInstallments() {
        if vehicleReservations.numeroCartao == nil {
            paymentService.getInstallments(of: String(vehicleReservations.valorTotal), response: (success: { [weak self] installments in
                guard let installments = installments.first?.installments, let vehicleReservations = self?.vehicleReservations else { return }

                self?.delegate?.showInstallmentsValue(installments: installments,
                                                      vehicleReservationsDataView: VehicleReservationsDataView(model: vehicleReservations))
            }, failure: { error in
            }, completion: {
            
            }))
        }
    }

    func getCheckinButton(canCheckin: Bool = true, vehicleReservationsDataView: VehicleReservationsDataView) {
        self.delegate?.showCheckInButton(vehicleReservationsDataView: vehicleReservationsDataView)
    }
    
    func checkUserValidate() {
        
        guard let currentUser = session.currentUser else { return }
        let documentNumber = currentUser.account.documentNumber
        
        unidasUserStatusService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            let userInValidateDataView = UserInValidateDataView(model: usersDocumentsUser.userCheck)
            
            if let accountIsValid = userInValidateDataView.model?.accountIsValid, !accountIsValid {
                let remoteConfig = RemoteConfigUtil.getRemoteConfig()
                let message = remoteConfig["payment_now_user_in_validate_error"].stringValue ?? ""
                self?.delegate?.showUserInValidate(message: message)
            } else {
                self?.delegate?.doReservationPayment()
            }
          
            }, failure: { error in
        }, completion: {
            self.delegate?.enablePaymentButton()
        }))
    }
    
    func calendarReservation(vehicleReservationsDataView: VehicleReservationsDataView) {
        
    }
}
