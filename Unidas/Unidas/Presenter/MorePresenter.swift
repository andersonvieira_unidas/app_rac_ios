//
//  MorePresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 22/01/2020.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol MoreViewDelegate: class {
    func performSegue(identifier: String)
    func reloadData()
}

protocol MorePresenterDelegate {
    var delegate: MoreViewDelegate? { get set }
    var moreItems: [MoreItem] { get }
    func to(segueIdentifier: String)
    func moreItem(at: Int) -> MoreItemDataView
    func viewDidLoad()
}

class MorePresenter: MorePresenterDelegate {
    
    weak var delegate: MoreViewDelegate?
    var moreItems: [MoreItem] = []
    
    func to(segueIdentifier: String) {
        delegate?.performSegue(identifier: segueIdentifier)
    }
    
    func moreItem(at: Int) -> MoreItemDataView {
        return MoreItemDataView(model: moreItems[at])
    }
    
    func viewDidLoad() {
        insertItems()
        delegate?.reloadData()
    }
    
    private func insertItems(){
        let findStore = MoreItem(image: "icon-more-store", localizedString: "More Menu Item Find Store", action: "showFindStore")
        moreItems.append(findStore)
        let callCenter = MoreItem(image: "icon-more-support", localizedString: "More Menu Item Call Center", action: "showCallCenter")
        moreItems.append(callCenter)
        let faq = MoreItem(image: "icon-more-faq", localizedString: "More Menu Item Faq", action: "showFaq")
        moreItems.append(faq)
        let about = MoreItem(image: "icon-more-about", localizedString: "More Menu Item About", action: "showAbout", separator: false)
        moreItems.append(about)
    }
}
