//
//  ForgotPIN2Presenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 20/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation


enum ForgotPINValidateError: LocalizedError {
    case methodRequired
    
    var errorDescription: String? {
        switch self {
        case .methodRequired:
            return NSLocalizedString("Code is required", comment: "Code is required error description")
        }
    }
}

protocol ForgotPINViewDelegate: class {
    func startLoading()
    func endLoading()
    func startButtonLoading()
    func endButtonLoading()
    
    func present(error: Error)
    func showValidateCode()
    func updateInfoEmailAndSMS(userResponse: UserResponse)
}

protocol ForgotPINPresenterDelegate {
    var delegate: ForgotPINViewDelegate? { get set }
    var unidasToken: UnidasToken?  { get set }
    var recoveryMethod: AccountRecoveryMethod? { get set }
    func viewDidLoad()
    func didChangeRecoveryMethod(to methodSelected: Int)
    func addEventForgotPassword(success: Bool)
    func sendForgotCode()
}

class ForgotPINPresenter: ForgotPINPresenterDelegate {
    
    weak var delegate: ForgotPINViewDelegate?
    private var recoveryService: RecoveryService
    private var userResponse: UserResponse
    private var userCheckService: UserCheckService
    private var firebaseAnalyticsService: FirebaseAnalyticsService


    var unidasToken: UnidasToken?
    var recoveryMethod: AccountRecoveryMethod? = nil
    
    init(recoveryService: RecoveryService, userResponseDataView: UserResponseDataView, userCheckService: UserCheckService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.recoveryService = recoveryService
        self.userResponse = userResponseDataView.model
        self.userCheckService = userCheckService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func viewDidLoad() {
        fetchUnidasToken()
    }
    
    func addEventForgotPassword(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.forgotPassword, withValue: parameters)
    }
    
    func fetchUnidasToken() {
        self.delegate?.startLoading()
        userCheckService.generateExpiredToken(documentNumber: userResponse.account.documentNumber, response: (success: { [weak self] unidasToken in
            self?.unidasToken = unidasToken
            self?.fetchUserInformation()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                self?.delegate?.endLoading()
            }, completion: {
                
        }))
    }
    
    func didChangeRecoveryMethod(to methodSelected: Int) {
        guard let selectedMethod = AccountRecoveryMethod(rawValue: methodSelected) else { return }
        self.recoveryMethod = selectedMethod
    }

    func sendForgotCode() {
        self.delegate?.startButtonLoading()
        
        guard let recoveryMethod = self.recoveryMethod else {
            delegate?.present(error: ForgotPINValidateError.methodRequired)
            return
        }
        
        recoveryService.sendSecretCode(for: userResponse, method: recoveryMethod, response: (success: { [weak self] in
            self?.delegate?.showValidateCode()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endButtonLoading()
        }))
    }
    
    // MARK: - Private function
    private func fetchUserInformation() {
        guard let unidasToken = unidasToken else { return }
        userCheckService.fetchUserInformationForAccountRecovery(documentNumber: userResponse.account.documentNumber, unidasToken: unidasToken, response: (success: { [weak self] usersUnidas in
            guard let userUnidas = usersUnidas.first, let userResponse = UserResponse(userUnidas: userUnidas) else { return }
            self?.userResponse = userResponse
            self?.delegate?.updateInfoEmailAndSMS(userResponse: userResponse)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
}


