//
//  LoyaltyProgramPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

protocol LoyaltyProgramViewDelegate: class {
    
    func startLoading()
    func endLoading()
    func setup(for loyaltyProgram: LoyaltyProgramDataView)
    func present(error: Error)
    func presentLoyaltyProgramCallToAction()
    
}

protocol LoyaltyProgramPresenterDelegate {
    
    var delegate: LoyaltyProgramViewDelegate? { get }
    
    func viewDidLoad()
    
}

class LoyaltyProgramPresenter: LoyaltyProgramPresenterDelegate {
    
    weak var delegate: LoyaltyProgramViewDelegate?
    
    let loyaltyProgramService: LoyaltyProgramService
    
    init(loyaltyProgramService: LoyaltyProgramService) {
        self.loyaltyProgramService = loyaltyProgramService
    }
    
    func viewDidLoad() {
        fetchLoyaltyProgram()
    }
    
    private func fetchLoyaltyProgram() {
        guard let user = session.currentUser else { return }
        delegate?.startLoading()
        loyaltyProgramService.fetchLoyaltyProgramPoints(for: user, response: (success: { [weak self] loyaltyPrograms in
            if let loyaltyProgram = loyaltyPrograms.first {
                self?.delegate?.setup(for: LoyaltyProgramDataView(model: loyaltyProgram))
            } else {
                self?.delegate?.presentLoyaltyProgramCallToAction()
            }
        }, failure: { [weak self] error in
            self?.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
        }))
    }
    
}
