//
//  ProtectionsAndEquipmentsPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 23/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import FacebookCore

protocol ProtectionsAndEquipmentsViewDelegate: class {
    
    var indexesForSelectedProtection: [Int] { get set}
    var indexesForSelectedEquipment: [Int] { get set}
    var pickUp: GarageReservationDetailsDataView! { get }
    var `return`: GarageReservationDetailsDataView! { get }
    
    func startLoading()
    func endLoading()
    func goBack()
    func startLoadingPreAuthorizationValue()
    func endLoadingPreAuthorizationValue()
    func show(preAuthorizationValueEstimate: PreAuthorizationEstimateDataView)
    func present(error: Error)
    func continueReservation()
    func updateResume(totalValue: String, prePaymentValue: String?)
    func reloadRows(at indexPaths: [IndexPath])
    func selectProtection(at indexPath: IndexPath)
    func fetchPreAuthorizationValue(preAuthorization: Float?)
}

protocol ProtectionsAndEquipmentsPresenterDelegate {
    
    var delegate: ProtectionsAndEquipmentsViewDelegate? { get set }
    
    var numberOfProtections: Int { get }
    var numberOfEquipments: Int { get }
    var totalValueString: String { get }
    var administrativeTaxString: String { get }
    var administrativeTaxPercentString: String { get }
    var reservation: ReservationDataView { get }
    var quotationResume: QuotationResumeDataView { get }
    var oldReservation: VehicleReservationsDataView? { get set }
    
    var selectedProtectionsCount: Int { get }
    var selectedEquipmentsCount: Int { get }
    
    var selectedProtections: [PricedCoverages] { get }
    var selectedEquipments: [PricedEquips]? { get }
    
    func viewDidLoad()
    func protection(at index: Int) -> ProtectionsDataView
    func equipment(at index: Int) -> EquipmentDataView
    func didTapContinueButton()
    func isProtecationSelectable(at indexPath: IndexPath) -> Bool
    func selectionDidChange(at indexPath: IndexPath)
    func continueWithoutProtection()
    func didUpdateQuantityForEquipment(at indexPath: IndexPath, to value: Int)
    func calculateTotalValue()
    func fetchProtectionRules()
    
    var getProtections: [PricedCoverages] { get }
    var getEquipments: [PricedEquips] { get }
    
    func logAddToCartEvent()
}

class ProtectionsAndEquipmentsPresenter: ProtectionsAndEquipmentsPresenterDelegate {
    
    weak var delegate: ProtectionsAndEquipmentsViewDelegate?
    
    private var protections: [PricedCoverages] {
        return quotations.pricedCoverages?.filter { $0.coverageType != "100" } ?? []
    }
    private var equipments: [PricedEquips] {
        return quotations.pricedEquips ?? []
    }
    var selectedProtections: [PricedCoverages] {
        guard let protections = delegate?.indexesForSelectedProtection.map({ protections[$0] }), !protections.isEmpty else { return [] }
        return protections
    }
    var selectedEquipments: [PricedEquips]? {
        guard let equipments = delegate?.indexesForSelectedEquipment.map({ equipments[$0] }), !equipments.isEmpty else { return nil }
        return equipments
    }
    
    var selectedProtectionsCount: Int {
        return selectedProtections.count
    }
    var selectedEquipmentsCount: Int {
        return selectedEquipments?.count ?? 0
    }
    
    private var notAllowedProtectionCodes: [Int] {
        let selectedProtectionCodes: [Int] = selectedProtections.compactMap { protection in
            guard let type = protection.coverageType else { return nil }
            return Int(type)
        }
        return protectionCombinations.filter { selectedProtectionCodes.contains($0.code) }.flatMap { $0.notAllowedProtectionCodes }
    }
    
    var oldReservation: VehicleReservationsDataView?
    private var totalValue: Double = 0.0
    private var prePaymentValue: Double = 0.0
    private var administrativeTax: Double = 0.0
    private var administrativeTaxPercent: Double = 0.0
    private var didSelectedDefaultProtection: Bool = false
    
    var quotationResume: QuotationResumeDataView {
        var quotationResume = QuotationResume()
        quotationResume.quotation = quotations
        quotationResume.pickUp = delegate?.pickUp.model
        quotationResume.return = delegate?.return.model
        quotationResume.selectedEquipments = selectedEquipments
        quotationResume.selectedProtections = selectedProtections
        return QuotationResumeDataView(model: quotationResume)
    }
    
    var totalValueString: String = ""
    var administrativeTaxString: String = ""
    var administrativeTaxPercentString: String = ""
    var protectionsTotalValue: String = ""
    var equipmentsTotalValue: String = ""
    var selectedCasterWeekday: Int? = nil
    var reservation: ReservationDataView {
        var reservation = Reservation()
        reservation.totalValue = totalValue.fractionDigits(to: 2)
        reservation.prePaymentValue = prePaymentValue.fractionDigits(to: 2)
        reservation.protections = selectedProtections
        reservation.equipments = selectedEquipments
        reservation.pickUp = delegate?.pickUp.model
        reservation.return = delegate?.return.model
        reservation.rateQualifier = quotations.rateQualifier?.rateQualifierNumber
        if let code = quotations.vehicle?.code,
            let name = quotations.vehicle?.name,
            let vehiclesGroups = quotations.vehicle?.vehicleGroups,
            let codeCategory = quotations.vehicle?.vehicleCategory{
            let group = VehMakeModel(name: name, code: code, vehiclesGroups: vehiclesGroups, codeCategory: codeCategory)
            reservation.group = group
        }
        reservation.selectedCasterWeekday = selectedCasterWeekday
        reservation.prePaymentDiscountValue = quotationResume.calculation?.prePaymentDiscountPercentValue
        reservation.promotionCode = quotations.rateQualifier?.promotionCode
        reservation.pictureURL = quotations.vehicle?.pictureURL
        return ReservationDataView(model: reservation)
    }
    
    private var quotations: VehAvail
    private var protectionsService: ProtectionsService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var adjustAnalyticsService: AdjustAnalyticsService
    private var protectionCombinations: [ProtectionCombination] = []
    
    init(quotations: QuotationsDataView,
         protectionsService: ProtectionsService,
         firebaseAnalyticsService: FirebaseAnalyticsService,
         adjustAnalyticsService: AdjustAnalyticsService) {
        self.quotations = quotations.model
        self.protectionsService = protectionsService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.adjustAnalyticsService = adjustAnalyticsService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func viewDidLoad() {
        fetchProtectionRules()
        calculateTotalValue()
    }
    
    var numberOfProtections: Int {
        return protections.count
    }
    
    var numberOfEquipments: Int {
        return equipments.count
    }
    
    func protection(at index: Int) -> ProtectionsDataView {
        return ProtectionsDataView(model: protections[index])
    }
    
    func equipment(at index: Int) -> EquipmentDataView {
        return EquipmentDataView(model: equipments[index])
    }
    
    var getProtections: [PricedCoverages] {
        return quotations.pricedCoverages?.filter { $0.coverageType != "100" } ?? []
    }
    
    var getEquipments: [PricedEquips] {
        return quotations.pricedEquips ?? []
    }
    
    func didTapContinueButton() {
        validateProtectionSelection()
        registerEventProtectionsAndEquipments()
        logAddToCartEvent()
    }
    
    func isProtecationSelectable(at indexPath: IndexPath) -> Bool {
        if let protectionCode = protectionCode(for: protections[indexPath.row]) {
            return !notAllowedProtectionCodes.contains(protectionCode)
        }
        return true
    }
    
    func selectionDidChange(at indexPath: IndexPath) {
        if indexPath.section == 0 {
            selectionDidChangeForProtection(at: indexPath)
        }
        calculateTotalValue()
    }
    
    func continueWithoutProtection() {
        delegate?.continueReservation()
    }
    
    func validateProtectionSelection() {
        if let selectedGroupCode = quotations.vehicle?.vehicleCategory,
            let rateQualifierStr = quotations.rateQualifier?.rateQualifierNumber,
            let rateQualifier = Int(rateQualifierStr) {
            fetchPreAuthorizationValue(for: selectedProtections, groupCode: selectedGroupCode, rateQualifier: rateQualifier)
        }
    }
    
    func didUpdateQuantityForEquipment(at indexPath: IndexPath, to value: Int) {
        if indexPath.section == 1 {
            quotations.pricedEquips?[indexPath.row].selectedQuantityItem = value
            calculateTotalValue()
        }
    }
    
    private func selectDefaultProtection() {
        if oldReservation == nil {
            guard let indexForDefaultProtection = protections.firstIndex(where: { protection in protection.coverageType == "30" }), didSelectedDefaultProtection == false else { return }
            let indexPath = IndexPath(row: indexForDefaultProtection, section: 0)
            delegate?.selectProtection(at: indexPath)
            selectionDidChange(at: indexPath)
            didSelectedDefaultProtection = true
        }
    }
    
    func calculateTotalValue() {
        self.totalValue = quotationResume.totalWithAdministrativeTaxValue ?? 0.0
        self.prePaymentValue = quotationResume.prePaymentTotalWithAdministrativeTaxValue ?? 0.0
        self.administrativeTax = quotationResume.administrativeTaxValue ?? 0.0
        self.administrativeTaxPercent = quotationResume.administrativeTaxPercentageValue ?? 0.0
        self.totalValueString = quotationResume.totalWithAdministrativeTax ?? ""
        self.administrativeTaxString = quotationResume.administrativeTax ?? ""
        self.administrativeTaxPercentString = quotationResume.administrativeTaxPercentage ?? ""
        self.protectionsTotalValue = quotationResume.totalProtections ?? ""
        self.equipmentsTotalValue = quotationResume.totalEquipments ?? ""
        self.delegate?.updateResume(totalValue: totalValueString, prePaymentValue: quotationResume.prePaymentTotalWithAdministrativeTax)
    }
    
    func fetchProtectionRules() {
        delegate?.startLoading()
        protectionsService.fetchProtectionRules(response: (success: { [weak self] protectionCombinations in
            self?.protectionCombinations = protectionCombinations
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                self?.delegate?.goBack()
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
                self?.selectDefaultProtection()
        }))
    }
    
    private func selectionDidChangeForProtection(at indexPath: IndexPath) {
        guard let selectedProtectionCode = protectionCode(for: protections[indexPath.row]),
            let protectionCombination = protectionCombinations.first(where: { $0.code == selectedProtectionCode })
            else { return }
        let indexPaths = protectionCombination.notAllowedProtectionCodes.compactMap({ code in
            return self.protections.firstIndex(where: { protection in self.protectionCode(for: protection) == code })
        }).map({
            IndexPath(row: $0, section: indexPath.section) })
        delegate?.reloadRows(at: indexPaths)
    }
    
    private func protectionCode(for protection: PricedCoverages) -> Int? {
        guard let codeStr = protection.coverageType else { return nil }
        return Int(codeStr)
    }
    
    private func fetchPreAuthorizationValue(for protections: [PricedCoverages], groupCode: String, rateQualifier: Int) {
        delegate?.startLoadingPreAuthorizationValue()
        protectionsService.fetchPreAuthorizationValue(for: protections, groupCode: groupCode, rateQualifier: rateQualifier, response: (success: { [weak self] preAuthorizationEstimate in
            if let message = preAuthorizationEstimate.message, !message.isEmpty {
                self?.delegate?.show(preAuthorizationValueEstimate: PreAuthorizationEstimateDataView(model: preAuthorizationEstimate))
            } else {
                self?.delegate?.fetchPreAuthorizationValue(preAuthorization: preAuthorizationEstimate.value)
                self?.delegate?.continueReservation()
            }
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoadingPreAuthorizationValue()
        }))
    }
    
    func logAddToCartEvent() {
        logFacebookAddToCartEvent()
        logAdjustAddToCartEvent()
    }
    
    private func logFacebookAddToCartEvent(){
        var price = Double()
        
        guard let currentUser = currentUser else { return }
        
        guard let enviroment = AppPersistence.getValue(withKey: "enviroment") else {return}
        
        var parameters = [
            AppEvents.ParameterName.currency.rawValue: "BRL",
            AppEvents.ParameterName.contentID.rawValue: currentUser.account.documentNumberHash ?? "",
        ]
        
        //recover protections
        for protection in selectedProtections {
            if let name = protection.details?.first?.text, let charge = protection.unitCharge {
                price += charge
                parameters[name.eventParameter] = String(format:"%.1f", charge)
            }
        }
        //recover equipments
        if let selectedEquipments = selectedEquipments {
            for equipment in selectedEquipments {
                if let name = equipment.description, let charge = equipment.charge?.unitCharge {
                    price += charge
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        parameters["enviroment"] = enviroment
        
        AppEvents.logEvent(.addedToCart, valueToSum: price, parameters: parameters)
    }
    
    private func logAdjustAddToCartEvent(){
        var price = Double()
        
        guard let currentUser = currentUser else { return }
        
        var parameters = [
            "user": currentUser.account.documentNumberHash ?? "",
            "currency": "BRL",
        ]
        
        //recover protections
        for protection in selectedProtections {
            if let name = protection.details?.first?.text, let charge = protection.unitCharge {
                price += charge
                parameters[name.eventParameter] = String(format:"%.1f", charge)
            }
        }
        //recover equipments
        if let selectedEquipments = selectedEquipments {
            for equipment in selectedEquipments {
                if let name = equipment.description, let charge = equipment.charge?.unitCharge {
                    price += charge
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        adjustAnalyticsService.addEvent(withEvent: .addToCart, withValue: parameters, amount: price, currency: "BRL")
    }
    
    private func registerEventProtectionsAndEquipments(){
        
        var parameters: [String : Any] = [:]
        
        //recover protections
        for protection in selectedProtections {
            if let name = protection.details?.first?.text, let charge = protection.unitCharge {
                parameters[name.eventParameter] = String(format:"%.1f", charge)
            }
        }
        //recover equipments
        if let selectedEquipments = selectedEquipments {
            for equipment in selectedEquipments {
                if let name = equipment.description, let charge = equipment.charge?.unitCharge {
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }        
        firebaseAnalyticsService.addEvent(withEvent: .protectionsAndEquipments, withValue: parameters)
    }
    
}
