//
//  GaragesTableViewPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 23/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseAnalytics
import FacebookCore

protocol GaragesTableViewDelegate: class {
    func startLoading()
    func endLoading()
    func reloadData()
    func showVehicles(for garage: GarageDataView)
    func present(error: Error)
    func showEmptyResultsView()
}

protocol GaragesTableViewPresenterDelegate {
    var delegate: GaragesTableViewDelegate? { get set }
    var numberOfGarages: Int { get }
    var currentLocation: LocationDataView? { get }
    
    func fetchCurrentLocation(filterExpress: Bool)
    func searchGarages(with filter: String?, filterExpress: Bool)
    func searchButtonTapped(with filter: String?, filterExpress: Bool)
    func didSelectGarage(at index: Int)
    func garage(at index: Int) -> GarageDataView
    func setupExpressToggle(garages:[Garage], filterExpress: Bool)
}

class GaragesTableViewPresenter: GaragesTableViewPresenterDelegate {
    
    weak var delegate: GaragesTableViewDelegate?
    private var garages: [Garage] = []
    private var garageService: UnidasGarageService
    private var firebaseAnalytics: FirebaseAnalyticsService
    private var currentRequest: DataRequest? = nil
    private var requestTimer: Timer? = nil
    private var locationManager: LocationManager
    var currentLocation: LocationDataView? = nil
    
    init(garageService: UnidasGarageService,
         firebaseAnalytics: FirebaseAnalyticsService,
         locationManager: LocationManager) {
        self.garageService = garageService
        self.firebaseAnalytics = firebaseAnalytics
        self.locationManager = locationManager
    }
    
    var numberOfGarages: Int {
        return garages.count
    }
    
    func fetchCurrentLocation(filterExpress: Bool) {
        locationManager.requestCurrentLocation { [weak self] (currentLocation, error) in
            if let currentLocation = currentLocation {
                let location = LocationDataView(location: currentLocation)
                self?.currentLocation = location
                self?.fetchGarages(filterExpress:filterExpress)
            }
            else {
                self?.fetchGarages(with: nil, filterExpress: filterExpress)
            }
        }
    }
    
    func garage(at index: Int) -> GarageDataView {
        return GarageDataView(model: garages[index])
    }
    
    func didSelectGarage(at index: Int) {
        delegate?.showVehicles(for: garage(at: index))
    }
    
    func searchGarages(with filter: String?, filterExpress: Bool) {
        guard let count = filter?.count, count > 2 else { return }
        currentRequest?.cancel()
        requestTimer?.invalidate()
        requestTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { [weak self] (timer) in
            timer.invalidate()
            self?.fetchGarages(with: filter, filterExpress: filterExpress)
            self?.requestTimer = nil
        }
    }
    
    func searchButtonTapped(with filter: String?, filterExpress: Bool) {
        guard let count = filter?.count, count > 2 else { return }
        currentRequest?.cancel()
        requestTimer?.invalidate()
        fetchGarages(with: filter, filterExpress: filterExpress)
    }
    
    func setupExpressToggle(garages: [Garage], filterExpress: Bool) {
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .expressStores) { (value) in
            if !value || filterExpress == true {
                self.garages = garages.filter({ (garage) -> Bool in
                    return garage.type != .express
                })
            } else {
                self.garages = garages
            }
            
            if self.garages.isEmpty {
                self.delegate?.reloadData()
                self.delegate?.showEmptyResultsView()
            }else{
                self.order(by: self.currentLocation)
                self.delegate?.reloadData()
            }
            self.delegate?.endLoading()
        }
    }
    
    func fetchGarages(with filter: String?, filterExpress: Bool) {
        self.delegate?.startLoading()
        
        currentRequest = garageService.fetchGarages(filter: filter, response: (success: { [weak self] garages in
            
            self?.setupExpressToggle(garages: garages, filterExpress: filterExpress)
            
        }, failure: { [weak self] error in
            if (error as NSError).code == NSURLErrorCancelled { return }
            if let error = error as? ServiceError, error.code == 404 {
                self?.garages = []
                return
            }
            if (error as? AFError)?.isExplicitlyCancelledError ?? false {
                return
            }
            self?.delegate?.present(error: error)
        }, completion: { 
        }))
        
        if let term = filter {
            firebaseAnalytics.addEvent(withEvent: AnalyticsEventSearch, withValue: ["term" : term])
        }
    }
    
    // MARK: - Private function
    
    private func fetchGarages(filterExpress:Bool) {
        currentRequest?.cancel()
        requestTimer?.invalidate()
        requestTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] (timer) in
            timer.invalidate()
            self?.fetchGarages(with: nil, filterExpress: filterExpress)
            self?.requestTimer = nil
        }
    }
    
    private func order(by location: LocationDataView?) {
        guard let location = location else {
            self.delegate?.reloadData()
            return
        }
        
        var garageDataView = garages.map{ GarageDataView(model: $0) }
        
        garageDataView.sort { (a, b) -> Bool in
            
            let firstDistance = a.address?.calculeDistance(at: location) ?? 99999999
            let secondDistance = b.address?.calculeDistance(at: location) ?? 9999999
 
            return firstDistance < secondDistance

        }

        self.garages = garageDataView.map({$0.model})
        self.delegate?.reloadData()
    }
}
