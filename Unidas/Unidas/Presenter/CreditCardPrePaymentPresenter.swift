//
//  CreditCardPrePaymentPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum CreditCardPrePaymentError: LocalizedError {
    case securityCodeRequired, amountNotEmpty, noPrePaymentCardFound, invalidCard
    
    var errorDescription: String? {
        switch self {
        case .amountNotEmpty:
            return NSLocalizedString("Pre payment Amount not empty", comment: "Amount not empty error message")
        case .noPrePaymentCardFound:
            return NSLocalizedString("No pre payment card found", comment: "No pre payment card error message")
        case .securityCodeRequired:
            return NSLocalizedString("Pre payment security code is required", comment: "Pre payment security code is required message")
        case .invalidCard:
            return NSLocalizedString("Invalid pre card", comment: "Invalid pre card error message")
        }
    }
}

protocol CreditCardPrePaymentViewDelegate: class {
    func present(error: Error)
    
    func showCard(listCardDataView: ListCardDataView)
    func applyValidCard()
    func applyInvalidCards()
    
    var securityCode: String? { get }
}

protocol CreditCardPrePaymentPresenterDelegate {
    var delegate: CreditCardPrePaymentViewDelegate? { get set }
    
    func setCard(at listCardDataView: ListCardDataView)
    func setAmount(of amount: Double)
    
    var creditCardPrePaymentDataView: CreditCardPrePaymentDataView? { get }
}

class CreditCardPrePaymentPresenter: CreditCardPrePaymentPresenterDelegate {
    weak var delegate: CreditCardPrePaymentViewDelegate?
    
    private var amount: Double?
    private var card: ListCard?
    
    var creditCardPrePaymentDataView: CreditCardPrePaymentDataView? {
        guard let securityCode = delegate?.securityCode else {
            self.delegate?.present(error: CreditCardPrePaymentError.securityCodeRequired)
            return nil
        }
        
        if securityCode.isEmpty {
            self.delegate?.present(error: CreditCardPrePaymentError.securityCodeRequired)
            return nil
        }
        
        guard let card = card else {
            self.delegate?.present(error: CreditCardPrePaymentError.noPrePaymentCardFound)
            return nil
        }
        
        if !CardUtils.isValid(expiration: card.val, for: Date()) {
            self.delegate?.present(error: CreditCardPrePaymentError.invalidCard)
            return nil
        }
        
        guard let amount = amount else {
            self.delegate?.present(error: CreditCardPrePaymentError.amountNotEmpty)
            return nil
        }
        
        return CreditCardPrePaymentDataView(model: CreditCardPrePayment(card: card, amount: amount, securityCode: securityCode))
    }
    
    func setCard(at listCardDataView: ListCardDataView) {
        self.card = listCardDataView.model
        
        self.delegate?.showCard(listCardDataView: listCardDataView)
        (listCardDataView.isValid) ? self.delegate?.applyValidCard() : self.delegate?.applyInvalidCards()
    }
    
    func setAmount(of amount: Double) {
        self.amount = amount
    }
}
