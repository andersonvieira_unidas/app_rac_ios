//
//  ReservationSuccessPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 02/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import EventKit
import MapKit

protocol ReservationSuccessViewDelegate: class {
    func setupView(for reservation: ReservationDataView)
    func setupView(for user: UserResponseDataView)
    func setupView(for creditCard: CreditCardReservePaymentDataView)
    func setupInstallmentLabel(string: String)
    func showAddedToCalendar()
    func askPermissionToSettings()
    func present(error: Error)
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
    func checkAppReview()
    func setupRatingToggle()
}

protocol ReservationSuccessPresenterDelegate {
    
    var delegate: ReservationSuccessViewDelegate? { get }
    var creditCardDataView: CreditCardReservePaymentDataView? { get }
    var reservationTotalValue: String? { get }
    
    func viewDidLoad(reservation: ReservationDataView, user: UserResponseDataView, creditCard: CreditCardReservePaymentDataView?)
    func addPickupDateToCalendarButtonTapped()
    func requestPermissionCalendar()
    func requestPermissionReminder()
    
    func routePickUpReserve()
    func routeReturnReserve()
}

class ReservationSuccessPresenter: ReservationSuccessPresenterDelegate {

    weak var delegate: ReservationSuccessViewDelegate?
    lazy var eventStore = EKEventStore()
    lazy var reminderStore = EKEventStore()
    var reservation: Reservation!
    var creditCardDataView: CreditCardReservePaymentDataView?
    var paymentService: PaymentService
    
    var reservationTotalValue: String? {
        let dataView = ReservationDataView(model: reservation)
        return creditCardDataView != nil ? dataView.prePaymentValue : dataView.totalValue
    }
    
    lazy var reminderCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = reminderStore.calendars(for: .reminder).first(where: { $0.title == title }) else {
        
            let newCalendar = EKCalendar(for: .reminder, eventStore: reminderStore)
            newCalendar.title = title
            newCalendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            newCalendar.source = reminderStore.defaultCalendarForNewReminders()?.source
            do {
                try reminderStore.saveCalendar(newCalendar, commit: true)
            } catch {
                self.delegate?.present(error: error)
            }
            return newCalendar
        }
        return calendar
        
    }()
    
    lazy var eventCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = eventStore.calendars(for: .event).first(where: { $0.title == title }) else {
            let calendar = EKCalendar(for: .event, eventStore: eventStore)
            
            calendar.title = title
            calendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            calendar.source = eventStore.defaultCalendarForNewEvents?.source
            do {
                try eventStore.saveCalendar(calendar, commit: true)
            } catch {
                self.delegate?.present(error: error)
            }
            return calendar
        }
        return calendar
    }()
    
    init(paymentService: PaymentService) {
        self.paymentService = paymentService
    }
    
    func viewDidLoad(reservation: ReservationDataView, user: UserResponseDataView, creditCard: CreditCardReservePaymentDataView?) {
        self.reservation = reservation.model
        self.creditCardDataView = creditCard
        delegate?.setupView(for: reservation)
        delegate?.setupView(for: user)
        if let creditCard = creditCard {
            delegate?.setupView(for: creditCard)
        } else {
            fetchNumberOfInstallments()
        }
        delegate?.setupRatingToggle()
    }
    
    func requestPermissionCalendar() {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .event) { _,_  in
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    func requestPermissionReminder() {
        switch EKEventStore.authorizationStatus(for: .reminder) {
        case .authorized:
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .reminder) { _,_  in
                self.requestPermissionCalendar()
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    func addPickupDateToCalendarButtonTapped() {
        switch EKEventStore.authorizationStatus(for: .reminder) {
        case .authorized:
            addPickupDateToReminders()
            break
        case .denied, .restricted:
            self.delegate?.askPermissionToSettings()
            break
        case .notDetermined:
            break
        @unknown default:
            break
        }
        
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            addPickupDateToCalendar()
            break
        case .denied, .restricted:
            self.delegate?.askPermissionToSettings()
            break
        case .notDetermined:
            break
        @unknown default:
            break
        }
    }
    
    func routePickUpReserve() {

        guard  let address = reservation.pickUp?.garage.address else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        let addressDataView = AddressDataView(model: address)
        
        guard  let location = addressDataView.location?.clLocationCoordinate else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
            
        validateMapAppsForCoordinate(coordinate: location.coordinate)
    }
    
    func routeReturnReserve() {
        guard  let address = reservation.return?.garage.address else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        let addressDataView = AddressDataView(model: address)
        
        guard  let location = addressDataView.location?.clLocationCoordinate else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
            
        validateMapAppsForCoordinate(coordinate: location.coordinate)
    }
    
    private func addPickupDateToReminders() {
        guard let reservationNumber = reservation.number, let reminderDate = reservation.pickUp  else { return }
        
        let reminder = EKReminder(eventStore: reminderStore)
        let localizedNotes = NSLocalizedString("Reservation number: %@", comment: "Pickup reminder notes")
        let reminderDateAlarm = reminderDate.date.addingTimeInterval(-3600)
        reminder.calendar = self.reminderCalendar
        reminder.title = NSLocalizedString("Pick up my car at Unidas in one hour!", comment: "Pick up car at Unidas reminder title")
        reminder.notes = String(format: localizedNotes, reservationNumber)
        reminder.priority = Int(EKReminderPriority.low.rawValue)
        reminder.addAlarm(EKAlarm(absoluteDate: reminderDateAlarm))
        do {
            try reminderStore.save(reminder, commit: true)
           // delegate?.showAddedToReminders()
            
        } catch {
            delegate?.present(error: error)
        }
    }

    private func addPickupDateToCalendar() {
        //pickup
        let pickupStartDate = reservation.pickUp!.date
        let pickupEndDate = pickupStartDate.addingTimeInterval(3600)
        let pickupTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(reservation.number!)"
        //let pickupNotes = String(format: localizedPickupNotes, "\(reservation.number!)")
        let pickupLocation = recoverLocation(address: reservation.pickUp?.garage.address)
        let pickupEvent = registerEvent(withTitle: pickupTitle,
                                        withNotes: "",
                                        withLocation: pickupLocation,
                                        startDate: pickupStartDate,
                                        endDate: pickupEndDate)

        pickupEvent.addAlarm(EKAlarm(absoluteDate: pickupStartDate.addingTimeInterval(-3600)))
        
        ///return
        let returnStartDate = reservation.return!.date
        let returnEndDate = returnStartDate.addingTimeInterval(3600)
        let returnTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(reservation.number!)"
        //let returnNotes = String(format: localizedReturnNotes, "\(reservation.number!)")
        let returnLocation = recoverLocation(address: reservation.return?.garage.address)
        
        let returnEvent = registerEvent(withTitle: returnTitle,
                                        withNotes: "",
                                        withLocation: returnLocation,
                                        startDate: returnStartDate,
                                        endDate: returnEndDate)
        
        returnEvent.addAlarm(EKAlarm(absoluteDate: returnStartDate.addingTimeInterval(-3600)))
        
        do {
            try eventStore.save(pickupEvent, span: .thisEvent)
            try eventStore.save(returnEvent, span: .thisEvent)
            delegate?.showAddedToCalendar()
           
        } catch {
            delegate?.present(error: error)
        }
    }
    
    private func recoverLocation(address: Address?) -> String {
        var location = ""
        if let addressLocation = address {
            location.append(addressLocation.street ?? "")
            location.append(" - ")
            location.append(addressLocation.neighborhood ?? "")
            location.append(" \n ")
            location.append(addressLocation.city ?? "")
            location.append("/")
            location.append(addressLocation.state ?? "")
        }
        
        return location
    }
    
    private func registerEvent(withTitle title: String,
                               withNotes notes: String,
                               withLocation location: String?,
                               startDate: Date,
                               endDate: Date) -> EKEvent{
        
        let event = EKEvent(eventStore: eventStore)
        event.calendar = self.eventCalendar

        event.title = title
        event.notes = notes
        event.addAlarm(EKAlarm(absoluteDate: startDate.addingTimeInterval(-3600)))
        event.location = location
        event.startDate = startDate
        event.endDate = endDate
        
        return event
    }
    
    private func fetchNumberOfInstallments() {
        
        var amountValue = reservation.totalValue ?? 0.0
                
        if let status = reservation.paymentStatus, status == .success {
            amountValue = reservation.payedValue ?? 0.0
        }
        
        let amount = NumberFormatter.apiNumberFormatter.string(from: NSNumber(value: amountValue))!
        paymentService.getInstallments(of: amount, response: (success: { [weak self] installments in
            guard let firstInstallment = installments.first, let installmentValue = Int(firstInstallment.installments) else {
                self?.setMaximumNumber(of: 1, for: amountValue)
                return
            }
            self?.setMaximumNumber(of: installmentValue, for: amountValue)
        }, failure: { [weak self] error in
            self?.delegate?.present(error: error)
        }, completion: {
            
        }))
    }
    
    private func setMaximumNumber(of installments: Int, for amount: Double) {
        let intallmentValue = NumberFormatter.currencyFormatterWithouRounding.string(from: NSNumber(value: amount / Double(installments)))
        delegate?.setupInstallmentLabel(string: "\(installments)x \(intallmentValue!)")
    }
    
    // MARK: - Private functions
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
    
}
