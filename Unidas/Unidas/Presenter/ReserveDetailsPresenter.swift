//
//  ReserveDetailsPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

protocol ReserveDetailsViewDelegate: class {
    func reloadData()
    func showInstallmentsValue(installments: String)
    func updateTransactionPayment(installments: String)
    
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView)
    func setupView()
    func setupReservationRules(rules: RulesDataView)
    func setupNavigationController()
    func setupTableView()
    func dismiss()
    func startLoading()
    func endLoading()
    func present(error: Error)
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
    
    func showCheckIn(vehicleReservationDataView: VehicleReservationsDataView)
    func showConfirmCheckin(reservation: VehicleReservationsDataView)
    func hideCancellationNotice()
    func canShowPendencies(value:Bool)
    func updatePrePayment(value: String)
    func startPrePaymentLoading()
    func endPrePaymentLoading()
}

protocol ReserveDetailsPresenterDelegate {
    var delegate: ReserveDetailsViewDelegate? { get set }
    
    var numberOfProtections: Int { get set }
    var numberOfEquipments: Int { get set }
    var numberOfSection: Int { get }
    var protectionsCount: Int { get }
    var equipmentsCount: Int { get }
    var refundPolicy: CompanyTermsDataView? { get }
    var vehicleReservationsDataView: VehicleReservationsDataView? { get set }
    
    func titleForSection(section: Int) -> String
    func heightForHeaderSection(section: Int) -> CGFloat
    func purchaseDataView(indexPath: IndexPath) -> PurchaseDataView?
    func getInstallments()
    func refreshPurchaseTable()
    func checkInReservation(reservation: VehicleReservationsDataView, isCheckinConfirmed: Bool)
    
    func fetchRefundPolicy()
    //func fetchReservation(reservation: String)
    
    func viewDidLoad()
    
    func routePickUpReserve()
    func routeReturnReserve()
    func addEventChange(reservation: VehicleReservationsDataView)
    func fetchCredDefenseSerasaStatus()
    func compareAppVersionToUpdate() -> Bool
    func fetchPrePaymentValue()
}

class ReserveDetailsPresenter: ReserveDetailsPresenterDelegate {
    var numberOfProtections: Int = 0
    var numberOfEquipments: Int = 0
    
    var vehicleReservationsDataView: VehicleReservationsDataView?
    
    weak var delegate: ReserveDetailsViewDelegate?
    private var protections: [Purchase] = []
    private var equipaments: [Purchase] = []
    private var vehicleReservations: VehicleReservations?
    private var paymentService: PaymentService
    private var companyTermsService: CompanyTermsService
    private var reservationUnidasService: ReservationUnidasService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    var refundPolicy: CompanyTermsDataView? = nil
    
    var numberOfSection: Int {
        return 2
    }
    
    var protectionsCount: Int {
        return protections.count
    }
    
    var equipmentsCount: Int {
        return equipaments.count
    }
    
    init(vehicleReservationsDataView: VehicleReservationsDataView?,
         paymentService: PaymentService,
         companyTermsService: CompanyTermsService,
         reservationUnidasService: ReservationUnidasService,
         firebaseAnalyticsService: FirebaseAnalyticsService) {
        
        if let dataView = vehicleReservationsDataView, let reservation = vehicleReservationsDataView?.model {
            self.vehicleReservationsDataView = dataView
            self.vehicleReservations = reservation
        }
        self.paymentService = paymentService
        self.companyTermsService = companyTermsService
        self.reservationUnidasService = reservationUnidasService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func viewDidLoad() {
    }
    
    func getInstallments() {
        guard let reservation = vehicleReservations else { return }
        if reservation.numeroCartao == nil {
            paymentService.getInstallments(of: String(reservation.valorTotal), response:  (success: { [weak self] installments in
                guard let installments = installments.first?.installments else { return }
                self?.delegate?.showInstallmentsValue(installments: installments)
                self?.delegate?.updateTransactionPayment(installments: installments)
                }, failure: { error in
                    //                    self?.delegate?.present(error: error)
            }, completion: {
                //                    self?.delegate?.endLoadingInstallments()
            })
            )
        }
    }
    
    func fetchRefundPolicy() {
        companyTermsService.fetchTerms(termsCompanyType: .refundPolicy, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            let companyTermsDataView = CompanyTermsDataView(model: companyTerms)
            
            self?.refundPolicy = companyTermsDataView
            self?.delegate?.showRefundPolicy(companyTermsDataView: companyTermsDataView)
            }, failure: { error in
                
        }, completion: {
            
        })
        )
    }
    
    func fetchCredDefenseSerasaStatus() {
        self.delegate?.startLoading()
        
        guard let currentUser = session.currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        reservationUnidasService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                var serasaIsValid = true
                var credDefenseIsValid = true
                
                if let isValid = userStatus.serasaIsValid {
                    serasaIsValid = isValid
                }
                if let isValid = userStatus.credDefenseIsValid {
                    credDefenseIsValid = isValid
                }
                
                let result = serasaIsValid == true && credDefenseIsValid == true ? true : false
                
                self?.delegate?.canShowPendencies(value: result)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
                self.delegate?.canShowPendencies(value: false)
        }, completion: {
            
        }))
    }
    
    func refreshPurchaseTable() {
        guard let purchase = vehicleReservations?.servico else {
            protections = []
            equipaments = []
            self.delegate?.reloadData()
            return
        }
        
        protections = purchase.filter({$0.tipo == "P" && $0.quantidade > 0 })
        equipaments = purchase.filter({$0.tipo == "E" && $0.quantidade > 0 })
        
        numberOfProtections = protections.count
        numberOfEquipments = equipaments.count
        
        self.delegate?.reloadData()
    }
    
    func titleForSection(section: Int) -> String {
        switch section {
        case 0: return NSLocalizedString("Protections", comment: "Protections title for section")
        case 1: return NSLocalizedString("Accessory and Services", comment: "Accessory and Services title for section")
        default: return ""
        }
    }
    
    func heightForHeaderSection(section: Int) -> CGFloat {
        switch section {
        case 0: return (protections.count > 0) ? 50.0 : 0.0
        case 1: return (equipaments.count > 0) ? 50.0 : 0.0
        default: return 0.0
        }
    }
    
    func purchaseDataView(indexPath: IndexPath) -> PurchaseDataView? {
        switch indexPath.section {
        case 0: return PurchaseDataView(model: protections[indexPath.row])
        case 1: return PurchaseDataView(model: equipaments[indexPath.row])
        default: return nil
        }
    }
    
    func compareAppVersionToUpdate() -> Bool {
        let compared = VersionControl.compareAppVersion()
        return compared
    }
        
   /* func fetchReservation(reservation: String) {
        delegate?.startLoading()
        
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        reservationUnidasService.get(by: reservation, documentNumber: documentNumber, response: (success: { [weak self] myReservationDetail in
            
            guard let pendencies = myReservationDetail.pendencies, let userAvailableToCheckin = myReservationDetail.userAvailableToCheckin, let vehicleReservations = myReservationDetail.reservation else {
                self?.delegate?.dismiss()
                return
            }
            
            let vehicleReservation = VehicleReservationsDataView(model: vehicleReservations)
            self?.vehicleReservationsDataView = vehicleReservation
            self?.vehicleReservations = vehicleReservations
            let rules = RulesDataView(pendencies: pendencies, userAvailableToCheckin: userAvailableToCheckin)
            self?.delegate?.setupReservationRules(rules: rules)
            self?.delegate?.load()
            
            }, failure: { error in
                self.delegate?.dismiss()
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }*/
    
    func routePickUpReserve() {
        guard let reservation = vehicleReservationsDataView else { return }
        
        guard let coordinate = reservation.pickupGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func routeReturnReserve() {
        guard let reservation = vehicleReservationsDataView else { return }
        
        guard let coordinate = reservation.returnGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func addEventChange(reservation: VehicleReservationsDataView) {
        var parameters: [String : Any] = [:]
        parameters["reservation_id"] = reservation.reserveNumber
        
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.changeReservation, withValue: parameters)
    }
    
    func checkInReservation(reservation: VehicleReservationsDataView, isCheckinConfirmed: Bool) {
        if reservation.noRequiredPayments && !isCheckinConfirmed {
            delegate?.showConfirmCheckin(reservation: reservation)
            return
        }
        delegate?.showCheckIn(vehicleReservationDataView: reservation)
    }
    
    func fetchPrePaymentValue() {
        
        self.delegate?.startPrePaymentLoading()
       
        guard let rate = vehicleReservationsDataView?.model.tarifa, let group = vehicleReservationsDataView?.model.grupo else {
            self.delegate?.updatePrePayment(value: "")
            return
        }
        
        let protections = vehicleReservationsDataView?.model.servico?.filter { $0.tipo == "P" }.compactMap { $0.codigoOTA } ?? []
        
        paymentService.fetchPreAuthorizationValue(for: protections, groupCode: group, rateQualifier: rate, response: (success: { [weak self] preAuthorizationEstimate in
            let preAuthorizationEstimate = PreAuthorizationEstimateDataView(model: preAuthorizationEstimate)
            self?.delegate?.updatePrePayment(value: preAuthorizationEstimate.value)
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endPrePaymentLoading()
        }))
    }
    
    
    // MARK: - Private functions
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
}
