//
//  ContractDetailsPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import CoreLocation

protocol ContractDetailsViewDelegate: class {
    func startLoading()
    func endLoading()
    func enabledDownloadContractButton()
    func present(error: Error)
    
    func reloadData()
    func openContractPdf(url: URL)
    
    func isHiddenQrCode()
    func setQrCodeMessage(message: String)
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
}

protocol ContractDetailsPresenterDelegate {
    var delegate: ContractDetailsViewDelegate? { get set }
    
    var numberOfSection: Int { get }
    var numberOfProtections: Int { get set }
    var numberOfServices: Int { get set }
    var numberOfEquipments: Int { get set}
    var numberOfPayments: Int { get set}
    
    var protectionsCount: Int { get }
    var servicesCount: Int { get }
    var equipmentsCount: Int { get }
    var paymentsCount: Int { get }
    
    //var isLegalEntity: Bool { get }
    
    func titleForSection(section: Int) -> String?
    func heightForHeaderSection(section: Int) -> CGFloat
    func contractItem(at indexPath: IndexPath) -> ContractDetailListItemDataView
    func paymentItem(at indexPath: IndexPath) -> ContractDetailPaymentListItemDataView
    
    func generateContractPdf()
    func showQrCode()
    func viewDidLoad()
    
    func routePickUpReserve()
    func routeReturnReserve()
}

class ContractsDetailsPresenter: ContractDetailsPresenterDelegate {
    
    weak var delegate: ContractDetailsViewDelegate?
    
    private var contract: Contract
    private var contractDataView: ContractDataView
    private var documentUnidasService: DocumentUnidasService
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    init(contractDataView: ContractDataView, documentUnidasService: DocumentUnidasService) {
        self.contractDataView = contractDataView
        self.contract = contractDataView.model
        self.documentUnidasService = documentUnidasService
    }
    
    func viewDidLoad() {
        
        verifyLegalEntity()
        
        numberOfProtections = contract.protections?.count ?? 0
        numberOfServices = contract.services?.count ?? 0
        numberOfEquipments = contract.additional?.count ?? 0
        numberOfPayments = contract.payments?.count ?? 0
        
    }
    
    func showQrCode() {
        guard let contractStatus = contract.status else {
            self.delegate?.isHiddenQrCode()
            return
        }
        
        if contractStatus == .closed {
            //self.delegate?.isHiddenQrCode()
            //return
        }
        
        if contract.group?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "CA" {
            let message = NSLocalizedString("Need help? Use QRCode at a Unidas store", comment: "Need help? Use QRCode at a Unidas store message")
            
            self.delegate?.setQrCodeMessage(message: message)
            return
        }
        
        let message = NSLocalizedString("Show this QRCode to the Unidas agent to finish your contract", comment: "Show this QRCode to the Unidas agent to finish your contract message")
        
        self.delegate?.setQrCodeMessage(message: message)
    }
    
    
    var numberOfSection: Int {
        return 4
    }
    
    var numberOfProtections: Int = 0
    
    var numberOfServices: Int = 0
    
    var numberOfEquipments: Int = 0
    
    var numberOfPayments: Int = 0
    
    
    var protectionsCount: Int {
        return contract.protections?.count ?? 0
    }
    
    var servicesCount: Int {
        return contract.services?.count ?? 0
    }
    
    var equipmentsCount: Int {
        return contract.additional?.count ?? 0
    }
    
    var paymentsCount: Int {
        return contract.payments?.count ?? 0
    }
    
    func titleForSection(section: Int) -> String? {
        switch section {
        case 0: return NSLocalizedString("Protections", comment: "Protections title for section")
        case 1: return NSLocalizedString("Services", comment: "Services title for section")
        case 2: return NSLocalizedString("Accessories", comment: "Accessory title for section")
        default: return nil
        }
    }
    
    func heightForHeaderSection(section: Int) -> CGFloat {
        switch section {
        case 0 where contract.protections != nil && !contract.protections!.isEmpty: return 50.0
        case 1 where contract.services != nil && !contract.services!.isEmpty: return 50.0
        case 2 where contract.additional != nil && !contract.additional!.isEmpty: return 50.0
        default: return 0.0
        }
    }
    
    func contractItem(at indexPath: IndexPath) -> ContractDetailListItemDataView {
        switch indexPath.section {
        case 0: return ContractDetailListItemDataView(model: contract.protections![indexPath.row])
        case 1: return ContractDetailListItemDataView(model: contract.services![indexPath.row])
        case 2: return ContractDetailListItemDataView(model: contract.additional![indexPath.row])
        default: fatalError("invalid section")
        }
    }
    
    func paymentItem(at indexPath: IndexPath) -> ContractDetailPaymentListItemDataView {
        return ContractDetailPaymentListItemDataView(model: contract.payments![indexPath.row])
    }
    
    func generateContractPdf() {
        guard let contractNumber = contract.id, let pickUpGarage = contract.pickupGarage else { return }
        
        self.delegate?.startLoading()
        
        documentUnidasService.generateDocument(contractNumber: contractNumber, pickupGarage: pickUpGarage, response: (success: { [weak self] in
            self?.fetchContractPdf()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
                self?.delegate?.enabledDownloadContractButton()
        }))
    }
    
    func routePickUpReserve() {
        let contract = contractDataView
        
        guard let coordinate = contract.pickupGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func routeReturnReserve() {
        
        let contract = contractDataView
        
        guard let coordinate = contract.returnGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
        
    }
    
    //MARK: Private Functions
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
    
    private func fetchContractPdf() {
        guard let contractNumber = contract.id, let pickUpGarage = contract.pickupGarage else { return }
        
        self.delegate?.startLoading()
        
        documentUnidasService.fetch(contractNumber: String(contractNumber), pickupGarage: pickUpGarage, response: (success: { [weak self] contractPdf in
            guard let contractPdf = contractPdf.first, let contractURL = URL(string: contractPdf) else { return }
            
            self?.delegate?.openContractPdf(url: contractURL)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    private func verifyLegalEntity(){
        let prePaymentRequired = contractDataView.isLegalEntityPrePaymentRequired
        let paymentRequired = contractDataView.isLegalEntityPaymentRequired
        
        contract.payments?.removeAll { (item) -> Bool in
            
            if item.type == .payment && !paymentRequired {
                return true
            }
            if item.type == .prePayment && !prePaymentRequired {
                return true
            }
            return false
        }
    }
}

