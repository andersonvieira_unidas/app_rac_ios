//
//  MyCarPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/02/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol MyCarViewDelegate: class {

    func startLoading()
    func endLoading()
    func present(error: Error)
    func showContractDetails()
    func updateView()
    func updateViewWithError(error: Error)
    func setup(for contract: ContractDataView)

    func processWallet(dataWallet: Data?)
    func startLoadingWallet()
    func endLoadingWallet()
    func alertErrorWallet()
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
}

protocol MyCarPresenterDelegate {
    var delegate: MyCarViewDelegate? { get set }
    var showsCarPickerButton: Bool { get }
    var showsQRCodeView: Bool { get }
    var showsGroupHeader: Bool { get }
    
    var contractDataView: ContractDataView? { get }
    var contractCurrentStoreDescription: String? { get }
    
    func viewDidLoad(contractNumber: Int)
    func refreshContract()
    func contractDetailsButtonTapped()

    func generateAppleWallet()
    func routePickUpReserve()
}

class MyCarPresenter: MyCarPresenterDelegate {

    weak var delegate: MyCarViewDelegate?
    
    private var vehicleDataStore: VehicleDataStore
    private var contractService: ContractService
    private var garageService: UnidasGarageService
    private var companyTermsService: CompanyTermsService
    private var passbookService: PassbookService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var contractNumber: Int!
    private var contract: Contract? = nil {
        didSet {
            if let contract = contract {
                delegate?.setup(for: ContractDataView(model: contract))
            }
        }
    }
    
    var contractDataView: ContractDataView? {
        guard let contract = contract else { return nil }
        return ContractDataView(model: contract)
    }
    
    var showsCarPickerButton: Bool {
        return isConnectedCarContract && contract?.status == .registered
    }

    var showsQRCodeView: Bool {
        return !isConnectedCarContract
    }
    
    var showsGroupHeader: Bool {
        return !isConnectedCarContract && contract?.status == .registered
    }
    
    var contractCurrentStoreDescription: String? {
        return contract?.status == .open ? contractDataView?.formattedReturnLocation : contractDataView?.formattedPickUpLocation
    }
    
    private var isConnectedCarContract: Bool {
        return contract?.group?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) == "CX"
    }
    
    init(vehicleDataStore: VehicleDataStore,
         contractService: ContractService,
         garageService: UnidasGarageService,
         companyTermsService: CompanyTermsService,
         passbookService: PassbookService,
         firebaseService: FirebaseAnalyticsService) {
        self.vehicleDataStore = vehicleDataStore
        self.contractService = contractService
        self.garageService = garageService
        self.companyTermsService = companyTermsService
        self.passbookService = passbookService
        self.firebaseAnalyticsService = firebaseService
    }
    
    func viewDidLoad(contractNumber: Int) {
        self.contractNumber = contractNumber
        fetchContract(contractNumber: contractNumber)
        registerForNotification()
        delegate?.updateView()
    }
    
    deinit {
        unregisterForNotifications()
    }
    
    func refreshContract() {
        updateView()
    }

    func contractDetailsButtonTapped() {
        delegate?.showContractDetails()
    }
    
    func generateAppleWallet() {
        self.delegate?.startLoadingWallet()
        
        var parameter: [String : Any] = [:]
        parameter["activate_wallet"] = true
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.activateWallet, withValue: parameter)

        var name = ""
        if let renterName = contractDataView?.model.renterName, !renterName.isEmpty{
            name = renterName
        }
        
        if let legalEntityName = contractDataView?.model.empresaNome, !legalEntityName.isEmpty{
            name = legalEntityName
        }
        
        if  let reservationId = contractDataView?.model.id,
            let pickupLocation = contractDataView?.model.pickupGarageDescription,
            let returnLocation = contractDataView?.model.returnGarageDescription,
            let qrCode = contractDataView?.qrCodeInformation,
            let pickupDate = contractDataView?.pickUpDate,
            let returnDate = contractDataView?.returnDate,
            let pickupTime = contractDataView?.model.pickupHour,
            let returnTime = contractDataView?.model.returnHour,
            let group = contractDataView?.model.group,
            let groupDescription = contractDataView?.model.groupDescription,
            let vehiclesGroup = contractDataView?.model.groupVehicles {
        
            let passbookRequest = PassbookRequest(reservationCode: String(reservationId),
                                                  pickupLocation: pickupLocation,
                                                  returnLocation: returnLocation,
                                                  qrCode: qrCode,
                                                  pickupDate: pickupDate.description,
                                                  returnDate: returnDate.description,
                                                  pickupTime: pickupTime,
                                                  returnTime: returnTime,
                                                  name: name,
                                                  group: group,
                                                  groupDescription: groupDescription,
                                                  vehiclesGroup: vehiclesGroup)
            
            passbookService.generatePassbook(passbook: passbookRequest, response: (success: { [weak self] data in
                self?.delegate?.processWallet(dataWallet: data)
                }, failure: { [weak self] error in
                    if let serviceError = error as? ServiceError, serviceError.code == 400 {
                        self?.delegate?.alertErrorWallet()
                        return
                    }
                    self?.delegate?.present(error: error)
                }, completion: { [weak self] in
                    self?.delegate?.endLoadingWallet()
            }))
        }
    }
    
    func routePickUpReserve() {
        guard let contract = contractDataView else { return }
        
        guard let coordinate = contract.pickupGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }

    
    // MARK: - Private function
    
    private func registerForNotification() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNUserDidLogin, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNUserDidLogout, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNDidContractFinish, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNDidExtendReturn, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNDidOpenContract, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNDidCloseContract, object: nil)
        notificationCenter.addObserver(self, selector: #selector(updateView), name: .UNDidUpdateContract, object: nil)
    }
    
    private func unregisterForNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateView() {
        fetchContract(contractNumber: self.contractNumber)
    }
    
    // MARK: Contract
    
    private func fetchContract(contractNumber: Int) {
        guard let user = session.currentUser else {
            contract = nil
            session.deliveryRequestId = nil
            delegate?.updateView()
            delegate?.endLoading()
            return
        }
        delegate?.startLoading()
        contractService.fetchContracts(for: user, and: contractNumber, response: (success: { [weak self] contracts in
            guard let contract = contracts.first(where: { $0.status == .open }) ?? contracts.first(where: {$0.status == .registered }) else {
                session.deliveryRequestId = nil
                self?.contract = nil
                self?.delegate?.updateView()
                return
            }
            self?.didFetch(contract: contract)
        }, failure: { error in
            if let serviceError = error as? ServiceError, serviceError.code == 404 {
                session.deliveryRequestId = nil
                self.contract = nil
                self.delegate?.updateViewWithError(error: error)
                return
            } else {
                self.delegate?.present(error: error)
                return
            }
        }, completion: {
            self.delegate?.endLoading()
            self.delegate?.updateView()
        }))
    }
    
    private func didFetch(contract: Contract) {
        self.contract = contract
        if contract.status == .open {
            session.deliveryRequestId = nil
        }
    }
    
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
}
