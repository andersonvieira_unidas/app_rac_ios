//
//  MyAccountDocumentPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 29/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit



protocol MyAccountDocumentViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func didOpenGalery(data: Data, imageType: ImageType)
    func centerStartLoading()
    func centerEndLoading()
    func hidePreviewDocumentCnh()
    func hideUpdateDocumentCnh()
    func showPreviewDocumentCnh()
    func showUpdateDocumentCnh()
    func showFinishDocument()
    func hideDriverLicensePhotoInstruction()
    func showDocumentDriverMessage(checkDocumentDataView: CheckDocumentDataView)
    func setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView)
}

protocol MyAccountDocumentPresenterDelegate {
    var delegate: MyAccountDocumentViewDelegate? { get set }
    
    func checkDocument()
    func loadImageGalery(imageType: ImageType)
    func fetchSacPhoneNumber()
}

class MyAccountDocumentPresenter: MyAccountDocumentPresenterDelegate {
    weak var delegate: MyAccountDocumentViewDelegate?
    private var accountImageService: AccountImageService
    private var checkDocumentService: CheckDocumentService
    private var companyTermsService: CompanyTermsService
    private var documentCNH: CheckDocument? = nil
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    init(accountImageService: AccountImageService, checkDocumentService: CheckDocumentService,
         companyTermsService: CompanyTermsService) {
        self.accountImageService = accountImageService
        self.checkDocumentService = checkDocumentService
        self.companyTermsService = companyTermsService
    }
    
    func fetchSacPhoneNumber() {
        companyTermsService.fetchTerms(termsCompanyType: .phoneNumberSac, response: (success: { [weak self] companyTerms in
            guard let companyTerm = companyTerms.first else { return }
            
            self?.delegate?.setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView(model: companyTerm))
            }, failure: { error in
                
        }, completion: {
            
        }))
    }
    
    @objc func checkDocument() {
        guard let currentUser = currentUser else { return }
        self.delegate?.centerStartLoading()
        
        checkDocumentService.fetch(documentNumber: currentUser.account.documentNumber, response: (success: { [weak self] checkDocument in
            self?.documentCNH = checkDocument.filter({$0.type == .cnhDocument}).first
            self?.updateDocumentInformations()
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 404 {
                    self?.delegate?.showFinishDocument()
                    return
                }
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.centerEndLoading()
        }))
    }
    
    func loadImageGalery(imageType: ImageType) {
        guard let currentUser = currentUser else { return }
        
        self.delegate?.startLoading()
        
        accountImageService.getImage(documentNumber: currentUser.account.documentNumber, type: imageType, response: (success: { [weak self] image in
            self?.delegate?.didOpenGalery(data: image, imageType: imageType)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    // MARK: - Private Function
    
    private func updateDocumentInformations() {
        if documentCNH == nil {
            self.delegate?.showFinishDocument()
            return
        }
        self.updateDocumentDriverInformation()
    }
    
    private func updateDocumentDriverInformation() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        
        guard let documentCNH = documentCNH, let validateAt = documentCNH.validateAt else {
            self.delegate?.showFinishDocument()
            return
        }
        
        let validateAtFormatted = String(validateAt.prefix(10))
       
        guard let documentCNHValidateDate =  dateFormatter.date(from: validateAtFormatted) else {
            self.delegate?.showUpdateDocumentCnh()
            self.delegate?.hidePreviewDocumentCnh()
            return
        }
        if documentCNHValidateDate > Date() {
            self.delegate?.showPreviewDocumentCnh()
            //self.delegate?.hideUpdateDocumentCnh()
            //self.delegate?.hideDriverLicensePhotoInstruction()
           // return
        }
        
        self.delegate?.showDocumentDriverMessage(checkDocumentDataView: CheckDocumentDataView(model: documentCNH))
    }
}
