//
//  StandartCarInformationPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 20/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
import FacebookCore

struct StandartCarInformationViewError: OptionSet, LocalizedError {
    
    var rawValue: Int
    
    static let pickUpLocationNotSet = StandartCarInformationViewError(rawValue: 1 << 0)
    static let returnLocationNotSet = StandartCarInformationViewError(rawValue: 1 << 1)
    static let bothLocationNotSet: StandartCarInformationViewError = [.pickUpLocationNotSet, .returnLocationNotSet]
    static let rateQualifierNotSet = StandartCarInformationViewError(rawValue: 3)
    
    var errorDescription: String? {
        switch self {
        case .bothLocationNotSet: return NSLocalizedString("You must set both locations", comment: "Both locations are not set error message")
        case .pickUpLocationNotSet: return NSLocalizedString("You must set the pickup location", comment: "Pickup location not set error message")
        case .returnLocationNotSet: return NSLocalizedString("You must set the return location", comment: "Return location not set error message")
        case .rateQualifierNotSet: return NSLocalizedString("Rate Qualifier not set", comment: "Rate Qualifier not set error message")
        default:
            return NSLocalizedString("unkwnon error occurred", comment: "Unknown error occured error message")
        }
    }
    
}

enum DateType: Int {
    case pickup, `return`
    
    var title: String {
        switch self {
        case .pickup:
            return NSLocalizedString("Pick up date", comment: "Pick up date data type title")
        case .return:
            return NSLocalizedString("Return date", comment: "Return date type title")
        }
    }
}

enum GaragesPickerSelected: Int {
    case pickup, `return`
}

protocol StandartCarInformationViewDelegate: class {
    
    var voucherCode: String? { get }
    var returnAtSamePlace: Bool { get }
    
    func showPickerDate(date: Date, of type: DateType)
    
    func pickUpDateChange(date: String, hour: String)
    func returnDateChange(date: String, hour: String)
    
    func showReturnPlace()
    func hiddenReturnPlace()
    
    func startLoading()
    func endLoading()
    func present(error: Error)
    func presentCustomAlert(error: Error)
    
    func updatePickUpVehicle(garageDataView: GarageDataView)
    func updateReturnVehicle(garageDataView: GarageDataView)
    func removeReturnVehicle()

    func nextStepGroupsVehicles(quotationDataView: [QuotationsDataView])
    func showStoreAvailabilityError(_ garage: GarageErrorDataView?)
    func hideStoreAvailabilityError()
    func showWorkingHours(for garage: GarageErrorDataView)
    func showAlert(withMessage message: String)
    func redirectToMensalMais(message: String?)
    func checkForExpressAvailability(pendencie:Bool, financialPendencie:Bool)
}

protocol StandartCarInformationPresenterDelegate {
    var delegate: StandartCarInformationViewDelegate? { get set }
    var pickUpDate: Date! { get set }
    var returnDate: Date! { get set }
    
    var garageSelected: GaragesPickerSelected { get set }
    var pickUpLocation: GarageDataView? { get set }
    var returnLocation: GarageDataView? { get set }
    var samePlace: Bool { get set }
    var pickUp: GarageReservationDetailsDataView? { get }
    var `return`: GarageReservationDetailsDataView? { get }
    var minuteInterval: Int { get }
    var minimumPickUpDate: Date { get }
    
    func viewDidLoad()
    func pickerDate(of type: DateType)
    func formatedDate(of type: DateType)
    func setDate(of type: DateType, date: Date)
    
    func selectedVehicleLocation(garageDataView: GarageDataView)
    func changedSamePlace(samePlace: Bool, update: Bool)
    
    func fetchQuotations()
    func fetchStatusCliente()
    func getGarageReturn(bycode code: String, samePlace: Bool)
    func getGaragePickup(bycode code: String)
    func logSearchEvent(searchString: String)
}

class StandartCarInformationPresenter: StandartCarInformationPresenterDelegate {
    weak var delegate: StandartCarInformationViewDelegate?
    
    private var quotationsService: QuotationsService
    private var systemConfigurationService: SystemConfigurationService
    private var unidasGarageService: UnidasGarageService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var adjustAnalyticsService: AdjustAnalyticsService
    
    var garageSelected: GaragesPickerSelected = .pickup
    
    var garage: Garage? = nil
    var pickUpLocation: GarageDataView? = nil
    var returnLocation: GarageDataView? = nil
    var samePlace: Bool = true
    
    var pickUpDate: Date!
    var returnDate: Date!
    
    var pickUp: GarageReservationDetailsDataView? {
        guard let location = pickUpLocation else { return nil }
        let details = GarageReservationDetails(date: pickUpDate, garage: location.model)
        return GarageReservationDetailsDataView(model: details)
    }
    
    var `return`: GarageReservationDetailsDataView? {
        guard let location = returnLocation else { return nil }
        let details = GarageReservationDetails(date: returnDate, garage: location.model)
        return GarageReservationDetailsDataView(model: details)
    }
    
    var minuteInterval: Int = 15
    
    var minimumPickUpDate: Date {
        let now = Date()
        let calendar = Calendar.current
        let nextFifteenMinutes = calendar.date(byAdding: .minute, value: minuteInterval, to: now)!
        let dateWithMinuteInterval = date(minuteInterval: minuteInterval, from: nextFifteenMinutes)!
        return dateWithMinuteInterval
    }
    
    init(quotationsService: QuotationsService,
         systemConfigurationService: SystemConfigurationService,
         unidasGarageService: UnidasGarageService,
         firebaseAnalyticsService: FirebaseAnalyticsService,
         adjustAnalyticsService: AdjustAnalyticsService,
         garageDataView: GarageDataView?) {
        self.quotationsService = quotationsService
        self.systemConfigurationService = systemConfigurationService
        self.unidasGarageService = unidasGarageService
        self.garage = garageDataView?.model
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.adjustAnalyticsService = adjustAnalyticsService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func viewDidLoad() {
        let calendar = Calendar.current
        pickUpDate = minimumPickUpDate
        returnDate = calendar.date(byAdding: .day, value: 1, to: pickUpDate)
        
        let pickUpDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: pickUpDate)
        let returnDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: returnDate)
        let pickUpHourFormatted = DateFormatter.timeOnlyFormatter.string(from: pickUpDate)
        let returnHourFormatted = DateFormatter.timeOnlyFormatter.string(from: returnDate)
        
        delegate?.pickUpDateChange(date: pickUpDateFormatted, hour: pickUpHourFormatted)
        delegate?.returnDateChange(date: returnDateFormatted, hour: returnHourFormatted)
        
        guard let garage = garage else { return }
        
        self.selectedVehicleLocation(garageDataView: GarageDataView(model: garage))
    }
    
    func setDate(of type: DateType, date: Date) {
        let normalizedDate = removeSeconds(from: date)
        if type == .pickup {
            pickUpDate = normalizedDate
            
            
            let pickUpDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: normalizedDate)
            let pickUpHourFormatted = DateFormatter.timeOnlyFormatter.string(from: normalizedDate)
            
            delegate?.pickUpDateChange(date: pickUpDateFormatted, hour: pickUpHourFormatted)
            self.delegate?.pickUpDateChange(date: pickUpDateFormatted, hour: pickUpHourFormatted)
            
            if pickerUpGreaterThenOrEqual(to: returnDate) {
                if let newDate = Calendar.current.date(byAdding: .day, value: 1, to: normalizedDate) {
                    returnDate = newDate
                    let returnDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: newDate)
                    let returnHourFormatted = DateFormatter.timeOnlyFormatter.string(from: newDate)
                    delegate?.returnDateChange(date: returnDateFormatted, hour: returnHourFormatted)
                }
            }
        }
        else {
            returnDate = normalizedDate
            let returnDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: normalizedDate)
            let returnHourFormatted = DateFormatter.timeOnlyFormatter.string(from: normalizedDate)
            delegate?.returnDateChange(date: returnDateFormatted, hour: returnHourFormatted)
        }
        validate()
    }
    
    func removeSeconds(from date: Date) -> Date {
        let calendar = Calendar.current
        let dateComponents = calendar.dateComponents([.era , .year , .month , .day , .hour , .minute], from: date)
        return calendar.date(from: dateComponents)!
    }
    
    func formatedDate(of type: DateType) {
        let pickUpDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: pickUpDate)
        let returnDateFormatted = DateFormatter.shortDateFormatterAPI.string(from: returnDate)
        let pickUpHourFormatted = DateFormatter.timeOnlyFormatter.string(from: pickUpDate)
        let returnHourFormatted = DateFormatter.timeOnlyFormatter.string(from: returnDate)
        
        (type == .pickup) ?  delegate?.pickUpDateChange(date: pickUpDateFormatted, hour: pickUpHourFormatted) : delegate?.returnDateChange(date: returnDateFormatted, hour: returnHourFormatted)
    }
    
    func pickerDate(of type: DateType) {
        let date : Date = (type == .pickup) ? pickUpDate : returnDate
        
        self.delegate?.showPickerDate(date: date, of: type)
    }
    
    
    func selectedVehicleLocation(garageDataView: GarageDataView) {
        if garageSelected == .pickup {
            self.pickUpLocation = garageDataView
            self.returnLocation = (delegate?.returnAtSamePlace ?? false) ? garageDataView : self.returnLocation
            self.delegate?.updatePickUpVehicle(garageDataView: garageDataView)
        }
        else {
            self.returnLocation = garageDataView
            self.delegate?.updateReturnVehicle(garageDataView: garageDataView)
        }
        validate()
    }
    
    func changedSamePlace(samePlace: Bool, update: Bool = false) {
        self.samePlace = samePlace
        

        if !update {
            self.returnLocation = samePlace ? pickUpLocation : nil
            
            if pickUpLocation == nil && returnLocation == nil {
                if samePlace {
                    self.delegate?.hiddenReturnPlace()
                    return
                } else {
                    self.delegate?.showReturnPlace()
                    return
                }
            }
  
            if let returnLocation = returnLocation {
                self.delegate?.hiddenReturnPlace()
                self.delegate?.updateReturnVehicle(garageDataView: returnLocation)
            } else {
                self.delegate?.removeReturnVehicle()
                self.delegate?.showReturnPlace()
                
            }
        } else {
            if !samePlace {
                self.delegate?.showReturnPlace()
                if let returnLocation = self.returnLocation {
                    self.delegate?.updateReturnVehicle(garageDataView: returnLocation)
                }
            } else {
                self.delegate?.hiddenReturnPlace()
            }
        }
        
        validate()
    }
    
    func fetchQuotations() {
        self.delegate?.startLoading()
        systemConfigurationService.fetch(codeKey: "TARIFA_PADRAO_APP", response: (success: { [weak self] systemConfiguration in
            if let rateQualifierString = systemConfiguration?[0].value, let rateQualifier = Int(rateQualifierString) {
                self?.getQuotations(rateQualifier: rateQualifier)
            } else {
                self?.delegate?.present(error: StandartCarInformationViewError.rateQualifierNotSet)
            }
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                self?.delegate?.endLoading()
            }, completion: {
        }))
    }
    
    func fetchStatusCliente() {
        self.delegate?.startLoading()
        
        guard let currentUser = session.currentUser else {
            self.delegate?.checkForExpressAvailability(pendencie: false, financialPendencie: false)
            self.delegate?.endLoading()
            return
        }
        
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        unidasGarageService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                let mobilePhoneIsValid = userStatus.mobilePhoneIsValid ?? false
                let emailIsValid = userStatus.emailIsValid ?? false
                let serasaIsValid = userStatus.serasaIsValid ?? false
                let credDefenseIsValid = userStatus.credDefenseIsValid ?? false
                let documentLicenceIsValid = userStatus.documentLicenceIsValid ?? false
                let signatureIsValid = userStatus.signatureIsValid ?? false
                let accountIsValid = userStatus.accountIsValid ?? false
                let paymentIsValid = userStatus.paymentIsValid ?? false
                let selfieIsValid = userStatus.selfieIsValid ?? false
                let financialPendencie = userStatus.financialPendencie ?? false
                var pendencie = false
                
                if mobilePhoneIsValid == false || emailIsValid == false || serasaIsValid == false || credDefenseIsValid == false || documentLicenceIsValid == false || signatureIsValid == false || accountIsValid == false || paymentIsValid == false || selfieIsValid == false {
                    pendencie = true
                }
                
                self?.delegate?.checkForExpressAvailability(pendencie: pendencie, financialPendencie: financialPendencie)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            
        }))
    }
    
    private func getQuotations(rateQualifier: Int){
        let pickUpDate = ValueFormatter.format(date: self.pickUpDate, format: "yyyy-MM-dd HH:mm:ss")
        let returnDate = ValueFormatter.format(date: self.returnDate, format: "yyyy-MM-dd HH:mm:ss")
        let garagePickUp = pickUpLocation
        let garageReturn = returnLocation
        let voucherCode = delegate?.voucherCode
        
        if let garagePickUp = garagePickUp, let garageReturn = garageReturn {
            quotationsService.fetchQuotations(pickUpDate: pickUpDate, returnDate: returnDate, garagePickUp: garagePickUp, garageReturn: garageReturn, voucher: voucherCode, rateQualifier: rateQualifier, response: (
                success: { [weak self] results in
                    let quotationDataView: [QuotationsDataView] = results.quotations?.compactMap { quotation in
                        guard let vehAvail = quotation.vehAvail else { return nil }
                        return QuotationsDataView(model: vehAvail)
                        } ?? []
                    self?.delegate?.nextStepGroupsVehicles(quotationDataView: quotationDataView)
                }, failure: { [weak self] error in
                    if let serviceError = error as? ServiceError, serviceError.code == 302 {
                        self?.delegate?.redirectToMensalMais(message: serviceError.message)
                        return
                    }
                    if let serviceError = error as? ServiceError, serviceError.code == 404 {
                        self?.delegate?.presentCustomAlert(error: error)
                        return
                    }
                    self?.delegate?.present(error: error)
                }, completion: { [weak self] in
                    self?.delegate?.endLoading()
                    self?.registerEventOpenReservation(rateQualifier: String(rateQualifier))
                    self?.logSearchEvent(searchString: garagePickUp.name)
            }))
        } else {
            self.delegate?.present(error: StandartCarInformationViewError.bothLocationNotSet)
            self.delegate?.endLoading()
        }
    }
    
    func getGarageReturn(bycode code: String, samePlace: Bool) {
        unidasGarageService.get(by: code,  response: (success: { [weak self] garages in
            if let garage = garages.first {
                let garageDataView = GarageDataView(model: garage)
                self?.returnLocation = garageDataView
                self?.changedSamePlace(samePlace: samePlace, update: true)
            }
            }, failure: {  _ in
        }, completion: {
        }))
    }
    
    func getGaragePickup(bycode code: String) {
        unidasGarageService.get(by: code,  response: (success: { [weak self] garages in
            if let garage = garages.first {
                let garageDataView = GarageDataView(model: garage)
                self?.pickUpLocation = garageDataView
                self?.delegate?.updatePickUpVehicle(garageDataView: garageDataView)
            }
            }, failure: { _ in
        }, completion: {
        }))
    }
    
    // MARK: - Private Functions
    
    private func date(minuteInterval: Int, from date: Date) -> Date? {
        let calendar = Calendar.current
        let currentHour = calendar.component(.hour, from: date)
        let currentMinute = calendar.component(.minute, from: date)
        let currentSecond = calendar.component(.second, from: date)
        let intervalOutOfHourRange = (((currentMinute / minuteInterval) + 1) * minuteInterval)
        let hoursToIncrement = intervalOutOfHourRange / 60
        let hour = (currentHour + hoursToIncrement) % 24
        let minute = intervalOutOfHourRange % 60
        var newDate = calendar.date(bySettingHour: hour, minute: minute, second: currentSecond, of: date)
        if let date = newDate, (currentHour + hoursToIncrement) > 23 {
            newDate = calendar.date(byAdding: .day, value: 1, to: date)!
        }
        return newDate
    }
    
    private func pickerUpGreaterThenOrEqual(to date: Date) -> Bool {
        let compare = Calendar.current.compare(date, to: pickUpDate, toGranularity: .day)
        return compare == .orderedSame || compare == .orderedAscending
    }
    
    private func validate() {
        let errors = validate(pickUp: `pickUp`?.model, andReturn: `return`?.model)
        
        if `pickUp` == nil || `return` == nil {
            self.delegate?.showStoreAvailabilityError(nil)
            return
        }
        
        if let garage = errors {
             self.delegate?.showStoreAvailabilityError(garage)
        } else {
            self.delegate?.hideStoreAvailabilityError()
        }
    }
    
    private func validate(pickUp: GarageReservationDetails?, andReturn return: GarageReservationDetails?) -> GarageErrorDataView? {
        let calendar = Calendar.current
        if let pickUp = pickUp {
            if !isGarage(pickUp.garage, openAt: pickUp.date, workday: workday(for: pickUp.date)) {
                
                return GarageErrorDataView(garage: pickUp.garage, selected: .pickup)
                
                //return pickUp.garage
            } else if pickUp.garage.holidays?.first(where: { calendar.isDate($0.date, inSameDayAs: pickUp.date) }) != nil, !isGarage(pickUp.garage, openAt: pickUp.date, workday: .holiday) {
                
                return GarageErrorDataView(garage: pickUp.garage, selected: .pickup)
                
                //return pickUp.garage
            }
        }
        if let `return` = `return` {
            if !isGarage(`return`.garage, openAt: `return`.date, workday: workday(for: `return`.date)) {
                
               return GarageErrorDataView(garage: `return`.garage, selected: .return)
                
                //return `return`.garage
            } else if `return`.garage.holidays?.first(where: { calendar.isDate($0.date, inSameDayAs: `return`.date) }) != nil, !isGarage(`return`.garage, openAt: `return`.date, workday: .holiday) {
                
                return GarageErrorDataView(garage: `return`.garage, selected: .return)
                //return `return`.garage
            }
        }
        return nil
    }
    
    private func isGarage(_ garage: Garage, openAt date: Date, workday: Workday) -> Bool {
        return garage.openingHours.periods.contains(where: { (period) -> Bool in
            if period.day == workday,
                let open = self.date(bySettingTimeString: period.open, of: date),
                let close = self.date(bySettingTimeString: period.close, of: date) {
                if close < open {
                    return !isDate(date, between: close, and: open, inclusive: false)
                }
                return isDate(date, between: open, and: close)
            }
            return false
        })
    }
    
    private func isDate(_ date: Date, between initalDate: Date, and finalDate: Date, inclusive: Bool = true) -> Bool {
        if inclusive {
            return (initalDate...finalDate).contains(date)
        } else {
            return initalDate < date && finalDate > date
        }
    }
    
    private func workday(for date: Date) -> Workday {
        let weekday = Calendar.current.component(.weekday, from: date)
        switch weekday {
        case 7: return .saturday
        case 1: return .sunday
        default: return .weekday
        }
    }
    
    private func date(bySettingTimeString timeString: String, of date: Date) -> Date? {
        let components = timeString.components(separatedBy: ":").compactMap { Int($0) }
        let hour = components[0]
        let minute = components[1]
        let second = 0
        return Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: date)
    }
    
    func logSearchEvent(searchString: String) {
        logFacebookSearchEvent(searchString: searchString)
        logAdjustSearchEvent(searchString: searchString)
    }
    
    private func logFacebookSearchEvent(searchString: String){
        guard let currentUser = currentUser else { return }
        
        guard let enviroment = AppPersistence.getValue(withKey: "enviroment") else {return}
        
        var parameters = [
            AppEvents.ParameterName.contentID.rawValue: currentUser.account.documentNumberHash ?? "",
            AppEvents.ParameterName.searchString.rawValue: searchString,
            ] as [String : Any]
        
        parameters["enviroment"] = enviroment
        
        AppEvents.logEvent(.findLocation, parameters: parameters)
    }
    
    private func logAdjustSearchEvent(searchString: String){
        guard let currentUser = currentUser else { return }
        
        let parameters: [String : String] = [
            "user": currentUser.account.documentNumberHash ?? "",
            AppEvents.ParameterName.searchString.rawValue: searchString,
        ]
        
        adjustAnalyticsService.addEvent(withEvent: .findLocation, withValue: parameters)
    }
    
    private func registerEventOpenReservation(rateQualifier: String){
        
        let pickUpDate = ValueFormatter.format(date: self.pickUpDate, format: "yyyy-MM-dd HH:mm:ss")
        let returnDate = ValueFormatter.format(date: self.returnDate, format: "yyyy-MM-dd HH:mm:ss")
        let garagePickUp = pickUpLocation
        let garageReturn = returnLocation
        let promoCode = delegate?.voucherCode
        
        var parameters: [String : Any] = [:]
        
        parameters["pickup_date"] = pickUpDate
        parameters["return_date"] = returnDate
        parameters["pickup_store"] = garagePickUp?.name
        parameters["return_store"] = garageReturn?.name
        parameters["rate_qualifier"] = rateQualifier
        parameters["promo_code"] = promoCode
        
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.day], from: self.pickUpDate, to: self.returnDate)
        
        parameters["days"] = components.day
        
        firebaseAnalyticsService.addEvent(withEvent: .openReservation, withValue: parameters)
    }
}
