//
//  ReservationsPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 10/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import CoreLocation
import UIKit
import EventKit

enum ReservationsError: LocalizedError {
    case geolocationNotFound
    
    var errorDescription: String? {
        switch self {
        case .geolocationNotFound:
            return NSLocalizedString("Geolocation not found", comment: "Geolocation not found error description")
        }
    }
}

protocol ReservationsViewDelegate: class {
    func startLoadingAtIndexPath(indexPath: IndexPath)
    func endLoadingAtIndexPath(indexPath: IndexPath)
    func startLoading()
    func endLoading()
    func reloadData()
    func present(error: Error)
    func showEmptyReservationsView()
    func showEmptyOffLineView()
    func showNotConnectedView()
    func hideNewReserve()
    func showNewReserve()
    func goToReserveDetails(reservation: VehicleReservationsDataView)
    func setupToggle()
    func showCheckIn(vehicleReservationDataView: VehicleReservationsDataView)
    func isEnabledNextReservation(_ isEnabled: Bool)
    func showConfirmCheckin(reservation: VehicleReservationsDataView)
    func canShowPendencies(value:Bool)
}

protocol ReservationsPresenterDelegate: class {
    
    var delegate: ReservationsViewDelegate? { get set }
    
    var numberOfCurrentReservations: Int { get }
    var numberOfUpcomingReservations: Int { get }
    func refreshReservations()
    func titleForHeader(in section: Int) -> String?
    func reservation(at indexPath: IndexPath) -> MyReservationDataView
    func rules() -> RulesDataView?

    func cancellationMessage(at indexPath: IndexPath)
    func routePickUpReserve()
    func routeReturnReserve()

    func checkInReservation(reservation: VehicleReservationsDataView, isCheckinConfirmed: Bool)
    
    func isEnabledNextReservation(_ isEnabled: Bool)
    func didSelectRowAt(at indexPath: IndexPath, reservation: VehicleReservationsDataView)
    func verifyConnection() -> Bool
    func isLegalEntity(at indexPath: IndexPath) -> Bool
    func fetchCredDefenseSerasaStatus()
    func compareAppVersionToUpdate() -> Bool
}

class ReservationsPresenter: ReservationsPresenterDelegate{
    enum Section: Int {
        case nextReservation, upcomingReservations
    }
    
    weak var delegate: ReservationsViewDelegate?
    var rulesDataView: RulesDataView? = nil
    
    var numberOfCurrentReservations: Int {
        return nextReservation != nil ? 1 : 0
    }
    
    var numberOfUpcomingReservations: Int {
        return upcomingReservations.count
    }
    
    private var nextReservation: MyReservationDataView? = nil
    private var upcomingReservations: [MyReservationDataView] = []
    private var reservationUnidasService: ReservationUnidasService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(reservationUnidasService: ReservationUnidasService,
         firebaseAnalyticsService: FirebaseAnalyticsService) {

        self.reservationUnidasService = reservationUnidasService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReservations), name: .UNDidMakeReservation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReservations), name: .UNDidCanceledReservation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReservations), name: .UNUserDidLogin, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReservations), name: .UNDidContractFinish, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshReservations), name: .UNDidPaidReservation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didLogout), name: .UNUserDidLogout, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didLogout() {
        nextReservation = nil
        upcomingReservations = []
        delegate?.hideNewReserve()
        delegate?.reloadData()
        delegate?.showEmptyOffLineView()
    }
    
    @objc func refreshReservations() {
        delegate?.startLoading()
        
        if !verifyConnection() {
            upcomingReservations = []
            nextReservation = nil
            delegate?.reloadData()
            delegate?.showNotConnectedView()
            return
        }
        
        guard let currentUser = session.currentUser else {
            delegate?.showEmptyOffLineView()
            return
        }
     
        nextReservation = nil
        upcomingReservations = []
        delegate?.reloadData()
        delegate?.hideNewReserve()
        
        let documentNumber = currentUser.account.documentNumber
        
        reservationUnidasService.fetch(by: documentNumber, response: (success: { [weak self] myReservations in
            if let reservations = myReservations.reservations {
                var myReservationsDataview: [MyReservationDataView] = []
                
                self?.rulesDataView = RulesDataView(pendencies: myReservations.pendencies ?? true, userAvailableToCheckin: myReservations.userAvailableToCheckin ?? false, financialPendency: myReservations.financialPendency ?? false)
 
                reservations.forEach { (vehicleReservations) in
                    let myReservationDataView = MyReservationDataView(vehicleReservation: vehicleReservations, pendencies: myReservations.pendencies ?? true, userAvailableToCheckin: myReservations.userAvailableToCheckin ?? false, financialPendency: myReservations.financialPendency ?? false)
                    myReservationsDataview.append(myReservationDataView)
                    
                }
                self?.loaded(vehicleReservations: myReservationsDataview)
            }else{
                self?.delegate?.showEmptyReservationsView()
                self?.delegate?.hideNewReserve()
            }  
            }, failure: { [weak self] error in 
                if let error = error as? ServiceError, error.code == 404 {
                    self?.delegate?.showEmptyReservationsView()
                    self?.delegate?.hideNewReserve()
                    return
                }
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self?.delegate?.showNotConnectedView()
                    self?.delegate?.hideNewReserve()
                    return
                }
                self?.delegate?.showEmptyReservationsView()
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func fetchCredDefenseSerasaStatus() {
        self.delegate?.startLoading()
        
        guard let currentUser = session.currentUser else { return }
        
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        reservationUnidasService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                var serasaIsValid = true
                var credDefenseIsValid = true
                
                if let isValid = userStatus.serasaIsValid {
                    serasaIsValid = isValid
                }
                if let isValid = userStatus.credDefenseIsValid {
                    credDefenseIsValid = isValid
                }
                
                let result = serasaIsValid == true && credDefenseIsValid == true ? true : false
                
                self?.delegate?.canShowPendencies(value: result)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
                self.delegate?.canShowPendencies(value: false)
        }, completion: {
            
        }))
    }
    
    func didSelectRowAt(at indexPath: IndexPath, reservation: VehicleReservationsDataView) {
        delegate?.goToReserveDetails(reservation: reservation)
    }
    
    func isLegalEntity(at indexPath: IndexPath) -> Bool {
        var vehicleReservations: VehicleReservationsDataView? = nil
        vehicleReservations = VehicleReservationsDataView(model: upcomingReservations[indexPath.row].vehicleReservation)
        if let isLegalEntity = vehicleReservations?.isLegalEntity {
            return isLegalEntity
        }
        return false
    }
    
    func verifyConnection()  -> Bool {
        return ReachabilityTest.isConnectedToNetwork()
    }
    
    func titleForHeader(in section: Int) -> String? {
        let section = self.section(for: section)
        switch section {
        case .nextReservation: return nil
        case .upcomingReservations where !upcomingReservations.isEmpty:
            return NSLocalizedString("Upcoming reservations", comment: "Upcoming reservations section title")
        default: return nil
        }
    }
    
    func reservation(at indexPath: IndexPath) -> MyReservationDataView {
        let section = self.section(for: indexPath.section)
        switch section {
        case .nextReservation:
            guard let nextReservation = nextReservation else {
                fatalError("Requiring next reservation while it's nil")
            }
            return nextReservation
        case .upcomingReservations:
            return upcomingReservations[indexPath.row]
        }
    }
    
    func cancellationMessage(at indexPath: IndexPath) {
        var vehicleReservations: VehicleReservationsDataView? = nil
        self.delegate?.startLoadingAtIndexPath(indexPath: indexPath)
        
        switch indexPath.section {
        case 0:
            guard let nextReservation = nextReservation else {
                self.delegate?.endLoadingAtIndexPath(indexPath: indexPath)
                return
            }
            vehicleReservations = VehicleReservationsDataView(model: nextReservation.vehicleReservation)
            break
        case 1:
            vehicleReservations = VehicleReservationsDataView(model: upcomingReservations[indexPath.row].vehicleReservation)
            break
        default:
            break
        }
        
        //guard let vehicleReservationsDataView = vehicleReservations else {
        //    return
        //}
        self.delegate?.endLoadingAtIndexPath(indexPath: indexPath)
        //self.delegate?.showCancellation(vehicleReservationDataView: vehicleReservationsDataView)
    }
    
    func rules() -> RulesDataView? {
        return rulesDataView
    }
    
    private func loaded(vehicleReservations: [MyReservationDataView]) {
        let filterVehicles = vehicleReservations.filter({$0.status == "CONFIRMADA"})
            .sorted { $0.pickupDate < $1.pickupDate }
        if filterVehicles.isEmpty {
            delegate?.showEmptyReservationsView()
            delegate?.hideNewReserve()
        } else {
            upcomingReservations = filterVehicles
            nextReservation = upcomingReservations.removeFirst()
            delegate?.reloadData()
            delegate?.showNewReserve()
        }
        delegate?.setupToggle()
    }
    
    private func section(for number: Int) -> Section {
        guard let section = Section(rawValue: number) else {
            fatalError("Invalid section number (\(number))")
        }
        return section
    }
    
    func compareAppVersionToUpdate() -> Bool {
           let compared = VersionControl.compareAppVersion()
           return compared
    }
    
    func checkInReservation(reservation: VehicleReservationsDataView, isCheckinConfirmed: Bool) {
        if reservation.noRequiredPayments && !isCheckinConfirmed {
            delegate?.showConfirmCheckin(reservation: reservation)
            return
        }
       delegate?.showCheckIn(vehicleReservationDataView: reservation)
    }
    
    func routePickUpReserve() {
        guard let nextReservation = nextReservation else { return }
        
        let vehicleReservationsDataView = VehicleReservationsDataView(model: nextReservation.vehicleReservation)
        
        guard let _ = vehicleReservationsDataView.pickupGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        //validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func routeReturnReserve() {
        guard let nextReservation = nextReservation else { return }
        
        let vehicleReservationsDataView = VehicleReservationsDataView(model: nextReservation.vehicleReservation)
        
        guard let _ = vehicleReservationsDataView.returnGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        //validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func isEnabledNextReservation(_ isEnabled: Bool) {
        if nextReservation != nil {
            self.delegate?.isEnabledNextReservation(isEnabled)
        }
    }
}
