//
//  ReserveCompletedPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import MapKit
import EventKit

protocol ReserveCompletedViewDelegate: class {
    func setupView(vehicleReservationDataView: VehicleReservationsDataView, preAuthorizationCardDataView: ListCardDataView?, openContractDataView: OpenContractDataView, preAuthorizationDataView: PreAuthorizationEstimateDataView?, userResponseDataView: UserResponseDataView)
    func setupViewPaymentWithPrePayment(reservationCard: ListCardDataView, vehicleReservationDataView: VehicleReservationsDataView, installments: Int)
    func setupViewPayment(vehicleReservationDataView: VehicleReservationsDataView)
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
    func finishTapMyCar()
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView)
    
    func showAddedToCalendar()
    func askPermissionToSettings()
    func present(error: Error)
}

protocol ReserveCompletedPresenterDelegate {
    var delegate: ReserveCompletedViewDelegate? { get set }
    var refundPolicy: CompanyTermsDataView? { get }

    func viewDidLoad()
    func routePickUp()
    func tapMyCar()
    func fetchRefundPolicy()
    func addPickupDateToCalendarButtonTapped()
    func requestPermissionCalendar()
    func requestPermissionReminder()
    
    func routePickUpReserve()
    func routeReturnReserve()
}

class ReserveCompletedPresenter: ReserveCompletedPresenterDelegate {
    
    weak var delegate: ReserveCompletedViewDelegate?
    
    private var preAuthorizationCard: ListCard?
    private var reservationCard: ListCard?
    private var vehicleReservations: VehicleReservations
    var reservation: Reservation!
    private var openContract: OpenContract
    private var preAuthorization: PreAuthorizationEstimate?
    private var installments: Int?
    private var companyTermsService: CompanyTermsService
    var refundPolicy: CompanyTermsDataView? = nil
    
    lazy var eventStore = EKEventStore()
    lazy var reminderStore = EKEventStore()
    
    lazy var reminderCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = reminderStore.calendars(for: .reminder).first(where: { $0.title == title }) else {
        
            let newCalendar = EKCalendar(for: .reminder, eventStore: reminderStore)
            newCalendar.title = title
            newCalendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            newCalendar.source = reminderStore.defaultCalendarForNewReminders()?.source
            do {
                try reminderStore.saveCalendar(newCalendar, commit: true)
            } catch {
                self.delegate?.present(error: error)
            }
            return newCalendar
        }
        return calendar
        
    }()
    
    lazy var eventCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = eventStore.calendars(for: .event).first(where: { $0.title == title }) else {
            let calendar = EKCalendar(for: .event, eventStore: eventStore)
            
            calendar.title = title
            calendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            calendar.source = eventStore.defaultCalendarForNewEvents?.source
            do {
                try eventStore.saveCalendar(calendar, commit: true)
            } catch {
                self.delegate?.present(error: error)
            }
            return calendar
        }
        return calendar
    }()

    init(preAuthorizationCardDataView: ListCardDataView?, reservartionCardDataView: ListCardDataView?, vehicleReservationsDataView: VehicleReservationsDataView,
         openContractDataView: OpenContractDataView, preAuthorizationDataView: PreAuthorizationEstimateDataView?, installments: Int?, companyTermsService: CompanyTermsService) {
        self.preAuthorizationCard = preAuthorizationCardDataView?.model
        self.reservationCard = reservartionCardDataView?.model
        self.vehicleReservations = vehicleReservationsDataView.model
        self.openContract = openContractDataView.model
        self.preAuthorization = preAuthorizationDataView?.model
        self.installments = installments
        self.companyTermsService = companyTermsService
    }
    
    func viewDidLoad() {
        guard let userResponse = session.currentUser else { return }
        
        let preAuthorizationCardDataView = preAuthorizationCard != nil ? ListCardDataView(model: preAuthorizationCard!) : nil
        let preAuthorizationDataView = preAuthorization != nil ? PreAuthorizationEstimateDataView(model: preAuthorization!) : nil

        self.delegate?.setupView(vehicleReservationDataView: VehicleReservationsDataView(model: vehicleReservations),
                                 preAuthorizationCardDataView: preAuthorizationCardDataView,
                                 openContractDataView: OpenContractDataView(model: openContract),
                                 preAuthorizationDataView: preAuthorizationDataView,
                                 userResponseDataView: UserResponseDataView(model: userResponse))
        
        guard let reservationCard = reservationCard else {
            self.delegate?.setupViewPayment(vehicleReservationDataView: VehicleReservationsDataView(model: vehicleReservations))
            return
        }
        
        guard let installments = installments else { return }

        self.delegate?.setupViewPaymentWithPrePayment(reservationCard: ListCardDataView(model: reservationCard),
                                                      vehicleReservationDataView: VehicleReservationsDataView(model: vehicleReservations),
                                                      installments: installments)
    }
    
    func routePickUp() {
        let vehicleReservationsDataView = VehicleReservationsDataView(model: vehicleReservations)
        
        guard let coordinate = vehicleReservationsDataView.pickupGeolocation else {
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func tapMyCar() {
        NotificationCenter.default.post(name: .UNDidContractFinish, object: nil)
        self.delegate?.finishTapMyCar()
    }
    
    func fetchRefundPolicy() {
        companyTermsService.fetchTerms(termsCompanyType: .refundPolicy, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            let companyTermsDataView = CompanyTermsDataView(model: companyTerms)
            
            self?.refundPolicy = companyTermsDataView
            self?.delegate?.showRefundPolicy(companyTermsDataView: companyTermsDataView)
            }, failure: { error in
                
        }, completion: {
            
        }))
    }
    
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
    
    func requestPermissionCalendar() {
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .event) { _,_  in
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    func requestPermissionReminder() {
        switch EKEventStore.authorizationStatus(for: .reminder) {
        case .authorized:
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .reminder) { _,_  in
                self.requestPermissionCalendar()
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    func addPickupDateToCalendarButtonTapped() {
        switch EKEventStore.authorizationStatus(for: .reminder) {
        case .authorized:
            addPickupDateToReminders()
            break
        case .denied, .restricted:
            self.delegate?.askPermissionToSettings()
            break
        case .notDetermined:
            break
        @unknown default:
            break
        }
        
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            addPickupDateToCalendar()
            break
        case .denied, .restricted:
            self.delegate?.askPermissionToSettings()
            break
        case .notDetermined:
            break
        @unknown default:
            break
        }
    }
    
    func routePickUpReserve() {
        let vehicleReservationsDataView = VehicleReservationsDataView(model: vehicleReservations)
              
        guard let coordinate = vehicleReservationsDataView.pickupGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func routeReturnReserve() {
        let vehicleReservationsDataView = VehicleReservationsDataView(model: vehicleReservations)
        
        guard let coordinate = vehicleReservationsDataView.returnGeolocation else {
            self.delegate?.present(error: ReservationsError.geolocationNotFound)
            return
        }
        
        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    private func recoverLocation(address: Address?) -> String {
        var location = ""
        if let addressLocation = address {
            location.append(addressLocation.street ?? "")
            location.append(" - ")
            location.append(addressLocation.neighborhood ?? "")
            location.append(" \n ")
            location.append(addressLocation.city ?? "")
            location.append("/")
            location.append(addressLocation.state ?? "")
        }
        return location
    }
    
    private func registerEvent(withTitle title: String,
                               withNotes notes: String,
                               withLocation location: String?,
                               startDate: Date,
                               endDate: Date) -> EKEvent{
        
        let event = EKEvent(eventStore: eventStore)
        event.calendar = self.eventCalendar

        event.title = title
        event.notes = notes
        event.addAlarm(EKAlarm(absoluteDate: startDate.addingTimeInterval(-3600)))
        event.location = location
        event.startDate = startDate
        event.endDate = endDate
        
        return event
    }
    
    private func addPickupDateToReminders() {
        
       // guard let reservationNumber = reservation.number, let reminderDate = reservation.pickUp  else { return }
        
        let reservationNumber = "\(vehicleReservations.numeroReserva)"
        let reminderDate = vehicleReservations.dataHoraRetirada
        
        let garageReservations = GarageReservationDetails(date: reminderDate, garage: Garage(name: "", code: "", description: "", address: nil, returnTax: nil, icon24Hours: nil, type: .store, openingHours: OpeningHours(informativeText: nil, periods: [OpeningHoursPeriod(day: .saturday, open: "", close: "")]), holidays: nil, franchiseCode: nil))

        let reminder = EKReminder(eventStore: reminderStore)
        let localizedNotes = NSLocalizedString("Reservation number: %@", comment: "Pickup reminder notes")
        let reminderDateAlarm = garageReservations.date.addingTimeInterval(-3600)
        //let reminderDateAlarm = reminderDate.date.addingTimeInterval(-3600)
        reminder.calendar = self.reminderCalendar
        reminder.title = NSLocalizedString("Pick up my car at Unidas in one hour!", comment: "Pick up car at Unidas reminder title")
        reminder.notes = String(format: localizedNotes, reservationNumber)
        reminder.priority = Int(EKReminderPriority.low.rawValue)
        reminder.addAlarm(EKAlarm(absoluteDate: reminderDateAlarm))
        do {
            try reminderStore.save(reminder, commit: true)
           // delegate?.showAddedToReminders()

        } catch {
            delegate?.present(error: error)
        }
    }

    private func addPickupDateToCalendar() {
        //pickup
        let pickupStartDate = vehicleReservations.dataHoraRetirada
        //let pickupStartDate = reservation.pickUp!.date
        
        let pickupEndDate = pickupStartDate.addingTimeInterval(3600)
        
        let pickupTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(vehicleReservations.numeroReserva)"
//        let pickupTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(reservation.number!)"
        //let pickupNotes = String(format: localizedPickupNotes, "\(reservation.number!)")
        
        //let pickupLocation = recoverLocation(address: reservation.pickUp?.garage.address)
        let pickupLocation = recoverLocation(address: nil)
        
        let pickupEvent = registerEvent(withTitle: pickupTitle,
                                        withNotes: "",
                                        withLocation: pickupLocation,
                                        startDate: pickupStartDate,
                                        endDate: pickupEndDate)

        pickupEvent.addAlarm(EKAlarm(absoluteDate: pickupStartDate.addingTimeInterval(-3600)))

        ///return
        let returnStartDate = vehicleReservations.dataHoraDevolucao
        //let returnStartDate = reservation.return!.date
        
        let returnEndDate = returnStartDate.addingTimeInterval(3600)
        
        let returnTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(vehicleReservations.numeroReserva)"
//        let returnTitle = NSLocalizedString("Reservation number:", comment: "Calendar description title") + "\(reservation.number!)"
        
        //let returnNotes = String(format: localizedReturnNotes, "\(reservation.number!)")
        
        //let returnLocation = recoverLocation(address: reservation.return?.garage.address)
        let returnLocation = recoverLocation(address: nil)

        let returnEvent = registerEvent(withTitle: returnTitle,
                                        withNotes: "",
                                        withLocation: returnLocation,
                                        startDate: returnStartDate,
                                        endDate: returnEndDate)

        returnEvent.addAlarm(EKAlarm(absoluteDate: returnStartDate.addingTimeInterval(-3600)))
        
        do {
            try eventStore.save(pickupEvent, span: .thisEvent)
            try eventStore.save(returnEvent, span: .thisEvent)
            delegate?.showAddedToCalendar()

        } catch {
            delegate?.present(error: error)
        }
    }
}
