//
//  MyAccountSignaturePresenter.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 19/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol MyAccountSignatureViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func didOpenGalery(data: Data, imageType: ImageType)
    func centerStartLoading()
    func centerEndLoading()
    func hidePreviewSignature()
    func hideUpdateSignature()
    func showPreviewSignature()
    func showUpdateSignature()
    func showFinishSignature()
    
    func showSignatureMessage(checkDocumentDataView: CheckDocumentDataView)
    func setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView)
}

protocol MyAccountSignaturePresenterDelegate {
    var delegate: MyAccountSignatureViewDelegate? { get set }
    
    func checkDocument()
    func loadImageGalery(imageType: ImageType)
    func fetchSacPhoneNumber()
}

class MyAccountSignaturePresenter: MyAccountSignaturePresenterDelegate {
    var delegate: MyAccountSignatureViewDelegate?
    private var accountImageService: AccountImageService
    private var checkDocumentService: CheckDocumentService
    private var companyTermsService: CompanyTermsService
    private var documentCNH: CheckDocument? = nil
    private var signature: CheckDocument? = nil
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    init(accountImageService: AccountImageService, checkDocumentService: CheckDocumentService,
         companyTermsService: CompanyTermsService) {
        self.accountImageService = accountImageService
        self.checkDocumentService = checkDocumentService
        self.companyTermsService = companyTermsService
    }
    
    func fetchSacPhoneNumber() {
        companyTermsService.fetchTerms(termsCompanyType: .phoneNumberSac, response: (success: { [weak self] companyTerms in
            guard let companyTerm = companyTerms.first else { return }
            
            self?.delegate?.setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView(model: companyTerm))
            }, failure: { error in
                
        }, completion: {
            
        }))
    }
    
    @objc func checkDocument() {
        guard let currentUser = currentUser else { return }
        self.delegate?.centerStartLoading()
        
        checkDocumentService.fetch(documentNumber: currentUser.account.documentNumber, response: (success: { [weak self] checkDocument in
            self?.documentCNH = checkDocument.filter({$0.type == .cnhDocument}).first
            self?.signature = checkDocument.filter({$0.type == .signature}).first
            self?.updateSignatureInformation()
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 404 {
                    return
                }
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.centerEndLoading()
        }))
    }
    
    func loadImageGalery(imageType: ImageType) {
        guard let currentUser = currentUser else { return }
        
        self.delegate?.startLoading()
        
        accountImageService.getImage(documentNumber: currentUser.account.documentNumber, type: imageType, response: (success: { [weak self] image in
            self?.delegate?.didOpenGalery(data: image, imageType: imageType)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    // MARK: - Private Function
    
    private func updateSignatureInformation() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSS"
        
        guard let signature = signature else {
            self.delegate?.showFinishSignature()
            return
        }

        self.delegate?.showSignatureMessage(checkDocumentDataView: CheckDocumentDataView(model: signature))

        self.delegate?.showPreviewSignature()
        self.delegate?.showUpdateSignature()
    }
    
}
