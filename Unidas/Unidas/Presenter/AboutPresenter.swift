//
//  AboutPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol AboutViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
}

protocol AboutPresenterDelegate {
    var delegate: AboutViewDelegate? { get set }
    
    func fetchAbout()
}

class AboutPresenter: AboutPresenterDelegate {
    weak var delegate: AboutViewDelegate?
    private var companyTermsService: CompanyTermsService
    
    init(companyTermsService: CompanyTermsService) {
        self.companyTermsService = companyTermsService
    }

    func fetchAbout() {
        /*companyTermsService.fetchTerms(termsCompanyType: .about, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
        
            self?.delegate?.showAbout(companyTermsDataView: CompanyTermsDataView(model: companyTerms))
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))*/
    }
    
    
}
