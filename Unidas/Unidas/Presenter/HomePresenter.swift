//
//  OnboardingPresenter.swift
//  Unidas
//
//  Created by Felipe Machado on 24/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

import FirebaseRemoteConfig

protocol HomeViewDelegate {
    func showOnboarding(pages: [OnboardPage])
    func showHowToRent(pages: [OnboardPage])
    func showPreview()
    func startLoading()
    func endLoading()
    func present(error: Error)
    func reload()
    func loadBadgePendencies(value:Bool)
    func animateCollection()
    func removeBage()
    func disableNotifications()
    func enableNotifications()
    func showCheckin(reservation: VehicleReservationsDataView)
    func canShowPendencies(value:Bool)
    func showConfirmCheckin(reservation: VehicleReservationsDataView)
    func askToUpdateApp()
    func showPostTerms()
    func checkAppReview()
    func setupRatingToggle()
    func showDeprecatedVersion()
    func showQrCode(qrCode: String)
}

protocol HomePresenterDelegate {
    var delegate: HomeViewDelegate? { get }
    var homeCollectionsArray: [HomeCellObject] { get set }
    var reservationStep: ReservationStep? { get }
    var reservation: VehicleReservationsDataView? { get set }
    
    func viewDidLoad()
    func viewWillAppear()
    func loadHowToRent()
    func showReservationDetails()
    func homeItem(index: Int) -> HomeCellObject
    func addEventFirstOpenAfterUpdate(success: Bool)
    func checkInReservation(reservation: VehicleReservationsDataView?, isCheckinConfirmed: Bool)
    func rules() -> RulesDataView?
    func fetchCredDefenseSerasaStatus()
    func compareAppVersionToUpdate() -> Bool
    func openQrCode()
}

class HomePresenter: HomePresenterDelegate {

    var delegate: HomeViewDelegate?
    
    private var homeService: HomeService
    var homeCollectionsArray: [HomeCellObject] = []
    var reservationStep: ReservationStep?
    var reservation: VehicleReservationsDataView?
    var rulesDataView: RulesDataView? = nil
    var checkinEnabled: Bool? = nil
    
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(homeService: HomeService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.homeService = homeService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func viewDidLoad() {
        DispatchQueue.main.async {
            self.checkPreferences()
            self.fetchTermsStatus()
            self.fetchAppVersionFromFirebase()
            self.fetchAppReviewTryCountFromFirebase()
        }
    }
    
    func viewWillAppear(){
        loadData()
    }
    
    func rules() -> RulesDataView? {
        return rulesDataView
    }
    
    func addEventFirstOpenAfterUpdate(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.firstOpenAfterUpdate, withValue: parameters)
    }
    
    private func loadData(){
        self.delegate?.startLoading()
        homeCollectionsArray = []
        reservationStep = nil
        reservation = nil
        
        guard let currentUser = currentUser else {
            self.reservationStep = nil
            
            self.delegate?.endLoading()
            self.delegate?.animateCollection()
            self.delegate?.removeBage()
            self.delegate?.disableNotifications()
            homeCollectionsArray = []
            self.homeCollectionsArray.append(HomeCellObject(title: NSLocalizedString("HomeCell Title How it works", comment: ""), image: #imageLiteral(resourceName: "icon_home_howToRent"), description: NSLocalizedString("HomeCell Subtitle How it works", comment: ""), type: .HowToRent))
            self.delegate?.reload()
            return
        }
        
        DispatchQueue.main.async {
            self.sendFcmTokenToServer()
        }
        
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        homeService.get(documentNumber: documentNumber, response: (success: { [weak self] homeResponse in
            guard let home = homeResponse else { return }
            
            
            self?.homeCollectionsArray = []
            
            let homeDataView = HomeResponseDataView(model: home)
            let express = homeDataView.reservation?.express ?? false
            
            AppPersistence.save(key: AppPersistence.pendencies, value: homeDataView.userHasPendencies)
            
            if homeDataView.userHasPendencies {
                let pendencies = HomeCellObject(title: NSLocalizedString("HomeCell Title Pendencies", comment: ""), image: #imageLiteral(resourceName: "icon_home_pendencies"), description: NSLocalizedString("HomeCell Subtitle Pendencies", comment: ""), type: .Pendencies)
                self?.homeCollectionsArray.append(pendencies)
            }
            
            self?.rulesDataView = RulesDataView(pendencies: home.pendencies ?? true, userAvailableToCheckin: home.userAvailableToCheckin ?? false, financialPendency: home.financialPendency ?? false)
            
            if homeDataView.userHasReservation {
                
                if let reservation = homeDataView.reservation {
                    self?.reservation = reservation
                }
                
                self?.homeCollectionsArray.append(HomeCellObject(title: NSLocalizedString("HomeCell Title Reservations", comment: ""), image: #imageLiteral(resourceName: "icon_home_reservations"), description: NSLocalizedString("HomeCell Subtitle Reservations", comment: ""), type: .Reservations))
                
                let reservationRulesDataView = self!.createReservationRulesDataView(homeDataView: HomeDataView(model: home))
                
                let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
         
                self?.reservationStep = reservationRules.make()
                
                let encoder = JSONEncoder()
                if  let reservationStep = self?.reservationStep,
                    let sharedDefaults = UserDefaults(suiteName: "group.br.unidas.apprac.ios"),
                    let encoded = try? encoder.encode(reservationStep),
                    let pickupStore = self?.reservation?.pickUpLocation,
                    let pickupDate = self?.reservation?.pickUpDateFormatted {
                    sharedDefaults.setValue(encoded, forKey: "reservation")
                    sharedDefaults.setValue(pickupStore, forKey: "pickupStore")
                    sharedDefaults.setValue(pickupDate, forKey: "pickupDate")
                }
            } else {
                if let sharedDefaults = UserDefaults(suiteName: "group.br.unidas.apprac.ios") {
                    sharedDefaults.setValue(nil, forKey: "reservation")
                    sharedDefaults.setValue(nil, forKey: "pickupStore")
                    sharedDefaults.setValue(nil, forKey: "pickupDate")
                }
            }
            
            self?.homeCollectionsArray.append(HomeCellObject(title: NSLocalizedString("HomeCell Title How it works", comment: ""), image: #imageLiteral(resourceName: "icon_home_howToRent"), description: NSLocalizedString("HomeCell Subtitle How it works", comment: ""), type: .HowToRent))
            
            //checkin
            if homeDataView.userHasReservationWithCheckin && !express {
                FirebaseRealtimeDatabase.toggleValue(withKey: .checkinReservation) { (toggle) in
                    if toggle {
                        if homeDataView.userHasReservationWithCheckin {
                            self?.homeCollectionsArray.append(HomeCellObject(title: NSLocalizedString("HomeCell Title Checkin", comment: ""), image: #imageLiteral(resourceName: "icon-home-qrcode"), description: NSLocalizedString("HomeCell Subtitle Checkin", comment: ""), type: .CheckIn))
                        }
                    }
                }
             
            } else if express && (self?.reservation?.reservationPaid ?? false) {
                self?.homeCollectionsArray.append(HomeCellObject(title: NSLocalizedString("HomeCell Title Checkin Express", comment: ""), image: #imageLiteral(resourceName: "icon-home-qrcode"), description: NSLocalizedString("HomeCell Subtitle Checkin Express", comment: ""), type: .CheckIn))
            }
            
            self?.delegate?.loadBadgePendencies(value: homeDataView.userHasPendencies)
            self?.delegate?.reload()
            self?.delegate?.enableNotifications()
            
            DispatchQueue.main.async {
                self?.fetchAppVersionFromFirebase()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            self.delegate?.animateCollection()
        }))
    }
    
    func loadHowToRent() {
        var key = ""
        if let _ = session.currentUser {
            key = "how_to_rent_resumed"
        } else {
            key = "how_to_rent_complete"
        }
        let pagesLoaded = loadData(key: key)
        guard let pages = pagesLoaded else { return }
        
        delegate?.showHowToRent(pages: pages)
    }
    
    func showReservationDetails() {
        delegate?.showPreview()
    }
    
    func homeItem(index: Int) -> HomeCellObject{
        return homeCollectionsArray[index]
    }
    
    func checkInReservation(reservation: VehicleReservationsDataView?, isCheckinConfirmed: Bool) {
        guard let reservation = reservation else { return }
        if reservation.noRequiredPayments && !isCheckinConfirmed {
            delegate?.showConfirmCheckin(reservation: reservation)
            return
        }
        delegate?.showCheckin(reservation: reservation)
    }
    
    func compareAppVersionToUpdate() -> Bool {
        let compared = VersionControl.compareAppVersion()
        return compared
    }
    
    func fetchCredDefenseSerasaStatus() {
        self.delegate?.startLoading()
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        homeService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                var serasaIsValid = true
                var credDefenseIsValid = true
                
                if let isValid = userStatus.serasaIsValid {
                    serasaIsValid = isValid
                }
                if let isValid = userStatus.credDefenseIsValid {
                    credDefenseIsValid = isValid
                }
                
                let result = serasaIsValid == true && credDefenseIsValid == true ? true : false
                
                self?.delegate?.canShowPendencies(value: result)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
                self.delegate?.canShowPendencies(value: false)
        }, completion: {
            
        }))
    }
    
    func openQrCode() {
        guard let qrCode = reservation?.qrCode else { return }
        delegate?.showQrCode(qrCode: qrCode)
    }
    
    //MARK : Private Functions
    private func checkPreferences(){
        let defaults = UserDefaults.standard
        let sharedPrefForOnboard = UserDefaults.standard.string(forKey: "appVersion")
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
        if sharedPrefForOnboard == nil || sharedPrefForOnboard != currentVersion {
            loadDataOnboard()
            resetAppReview()
            addEventFirstOpenAfterUpdate(success: true)
        }
        defaults.set(currentVersion, forKey: "appVersion")
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func buildPages(data: Onboard) -> OnboardPage? {
        guard let title = data.title else {return nil}
        guard let step = data.step else {return nil}
        guard let subtitle = data.subtitle else {return nil}
        guard let description = data.description else {return nil}
        
        var page = OnboardPage(title: title, subTitle: subtitle, description: description, step: "\(step)")
        
        if let helpContent = data.helpContent {
            page.helpContent = helpContent
        }
        if let urlImage = data.urlImage{
            page.imageUrl = urlImage
        }
        return page
    }
    
    private func loadData(key: String) -> [OnboardPage]?{
        var pages: [OnboardPage] = []
        let jsonString = RemoteConfigUtil.recoverParameterValue(with: key)
        
        if jsonString.isEmpty { return nil }
        let decoder = JSONDecoder()
        do {
            let json = jsonString.data(using: .utf8)!
            var onboards = try decoder.decode([Onboard].self, from: json)
            onboards = onboards.sorted { $0.step ?? 0 < $1.step ?? 0 }
            
            for onboard in onboards {
                if let onboardPage = buildPages(data: onboard) {
                    pages.append(onboardPage)
                }
            }
            
        } catch {
            print(error)
        }
        return pages
    }
    
    private func resetAppReview() {
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "neverRate")
    }
    
    private func loadDataOnboard(){
        var pages: [OnboardPage] = []
        
        let remoteConfig = RemoteConfigUtil.getRemoteConfigActivate()
        
        remoteConfig.fetchAndActivate { (status, error) in
            if status == .successFetchedFromRemote || status == .successUsingPreFetchedData {
                let jsonString = remoteConfig["onboarding_steps"].stringValue ?? ""
                
                if jsonString.isEmpty { return }
                let decoder = JSONDecoder()
                do {
                    let json = jsonString.data(using: .utf8)!
                    var onboards = try decoder.decode([Onboard].self, from: json)
                    onboards = onboards.sorted { $0.step ?? 0 < $1.step ?? 0 }
                    
                    for onboard in onboards {
                        if let onboardPage = self.buildPages(data: onboard) {
                            pages.append(onboardPage)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.delegate?.showOnboarding(pages: pages)
                    }    
                } catch {
                    print(error)
                }
            }
        }
    }
    
    private func createReservationRulesDataView(homeDataView: HomeDataView) -> ReservationRulesDataView {
        
        return ReservationRulesDataView(userValidated: homeDataView.userValidated,
                                        reservationNumber: homeDataView.reservationNumber,
                                        checkinAvailable: homeDataView.checkinAvailable,
                                        reservationPaid: homeDataView.reservationPaid,
                                        hoursToCheckin: homeDataView.hoursToCheckin,
                                        userAvailableToCheckin: homeDataView.userAvailableToCheckin,
                                        preAuthorizationPaid: homeDataView.preAuthorizationPaid,
                                        isFranchise: homeDataView.isFranchise,
                                        dueDate: homeDataView.dueDate,
                                        dueTime: homeDataView.dueTime,
                                        express: homeDataView.express,
                                        financialPendency: homeDataView.financialPendency)
    }
    
    private func sendFcmTokenToServer() {
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        guard let token = AppPersistence.getValue(withKey: "fcmToken") else {return}
        
        homeService.updateFcmToken(documentNumber: documentNumber, token: token, response: (success: { 
            print("success")
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            print("success completion token registration to Unidas")
        }))
    }
    
    
    func didSelect(card: HomeCellObject) {
        
    }
    
    
    
    func fetchTermsStatus() {
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        homeService.fetchTerms(documentNumber: documentNumber, response: (success: { [weak self] terms in
            
            if let privacyTerms = terms.termoPrivacidade?.termoPoliticaPrivacidade {
                if privacyTerms == false {
                    self?.delegate?.showPostTerms()
                }
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            
        }))
    }
    
    func fetchAppVersionFromFirebase() {
        if #available(iOS 11.0, *) {
            let _ = FirebaseRealtimeDatabase.fetchAppVersion(withKey: Toggle.version, completionHandler: { (version) in
                AppPersistence.save(key: "ios_version_firebase", value: version)
                if self.compareAppVersionToUpdate() == true {
                    self.delegate?.askToUpdateApp()
                }
            })
        } else {
            self.delegate?.showDeprecatedVersion()
        }
    }
    
    func fetchAppReviewTryCountFromFirebase() {
        let _ = FirebaseRealtimeDatabase.fetchAppReviewCount(withKey: Toggle.appReviewTrySession, completionHandler: { (count) in
            AppPersistence.save(key: "app_review_count", value: count)
            self.delegate?.setupRatingToggle()
        })
    }
}
