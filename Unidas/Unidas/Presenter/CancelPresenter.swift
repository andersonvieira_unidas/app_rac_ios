//
//  CancelPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol CancelViewDelegate: class {
    func reloadData()
    func startLoading()
    func endLoading()
    func present(error: Error)
}

protocol CancelPresenterDelegate {
    var delegate: CancelViewDelegate? { get set }
    var numberOfReasons: Int { get }
    
    func getCancellationReasons()
    func cancellationReason(at index: Int) -> CancellationReasonDataView
}

class CancelPresenter: CancelPresenterDelegate {
    weak var delegate: CancelViewDelegate?
    private var cancellationReasonService: CancellationReasonService
    private var cancellationReason: [CancellationReason] = []
    
    var numberOfReasons: Int {
        return cancellationReason.count
    }
    
    init(cancellationReasonService: CancellationReasonService) {
        self.cancellationReasonService = cancellationReasonService
    }
    
    func getCancellationReasons() {
        self.delegate?.startLoading()
        
        cancellationReasonService.get(response: (success: { [weak self] cancellationReasons in
            self?.cancellationReason = cancellationReasons
            self?.delegate?.reloadData()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func cancellationReason(at index: Int) -> CancellationReasonDataView {
        return CancellationReasonDataView(model: cancellationReason[index])
    }
}
