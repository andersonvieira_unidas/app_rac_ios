//
//  ReservationPaymentStepPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import FacebookCore

enum ReservationPaymentStepError: LocalizedError {
    case userFormIncomplete
    case emptyCiaAerea
    case noValidUserProvided
    case rentalRequirementsAndContractualConditionsUnchecked
    case creditCardNotSelected
    case refundPolicyCheckUnchecked
    case emptyCVV
    case creditCardExpiredDate
    
    var errorDescription: String? {
        switch self {
        case .userFormIncomplete: return NSLocalizedString("All fields from user form are required", comment: "User form is incomplete error message")
        case .emptyCiaAerea: return NSLocalizedString("All fields from Air Cia. are required", comment: "")
        case .noValidUserProvided: return NSLocalizedString("No user provided", comment: "No user provided error message")
        case .rentalRequirementsAndContractualConditionsUnchecked: return NSLocalizedString("In order to make a reservation you must to agree with the Rental Requirements and Contractual Conditions", comment: "Rental requirements and contractual conditions unchecked error message")
        case .creditCardNotSelected: return NSLocalizedString("In order to make a reservation you must select a credit card and the payment form", comment: "Credit card not selected when paying in advance")
        case .refundPolicyCheckUnchecked: return NSLocalizedString("In order to make a reservation you must check refund policy check", comment: "Refund policy check unchecked error message")
        case .emptyCVV: return NSLocalizedString("In order to make a reservation you must fill CVV number", comment: "CVV empty")
        case .creditCardExpiredDate: return NSLocalizedString("Card Expired Date", comment: "CVV empty")
            
        }
    }    
}

protocol ReservationPaymentStepViewDelegate: class {
    
    var userFormData: UserFormDataView? { get }
    var creditCardReservePaymentDataView: CreditCardReservePaymentDataView? { get }
    
    var isRefundPoliceChecked: Bool { get }
    var isComingByAirlineChecked: Bool { get }
    var isRentalRequirementsAndContractualConditionsChecked: Bool { get }
    var isCiaAereaChecked: Bool { get }
    var isCiaAereaDataEmpty: Bool { get }
    var isReceivePromotionalEmailChecked: Bool { get }
    var selectedPaymentOptionIndex: Int { get }
    
    func selectPaymentOption(at index: Int)
    func updateView()
    func showAirlineForm()
    func updateAirlineInformation()
    func setListCard(_ listCard: ListCardDataView)
    func startLoading()
    func endLoading()
    func present(error: Error)
    func disableBackButton()
    func enableBackButton()
    func showSuccessReservationView()
    func showUpdateDocument()
    func showCardList()
    func showCreateCard()
    func updateFlightCompany(name: String)
    func showRentalRequirements(companyTermsDataView: CompanyTermsDataView)
    func showFlightCompanies(flightCompany: [FlightCompany])
    func showContractConditions(companyTermsDataView: CompanyTermsDataView)
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView)
    func showUpdateDriverLicence(message: String?)
    func updatePaymentCollection(with height: Int)
    func showNotificationsOffers()
    func fetchNotificationStatus(status:Bool)
    func showExpressPayment(value:Bool)
    func showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView)
}

protocol ReservationPaymentStepPresenterDelegate {
    
    var delegate: ReservationPaymentStepViewDelegate? { get set }
    var numberOfPaymentOptions: Int { get }
    var numberOfFlightCompanies: Int { get }
    var showsCreditCardWidget: Bool { get }
    var showsLoginCallout: Bool { get }
    var showsCreateAccountCallout: Bool { get }
    var showsUserForm: Bool { get }
    var showsRefundNotice: Bool { get }
    var showsRefundPolicyCheck: Bool { get }
    var showsTermsAndAgreementsView: Bool { get }
    var showsFinishReservationButton: Bool { get }
    var payInAdvanceEnable : Bool { get }
    var payAtPickupEnable : Bool { get }
    var reservationDataView: ReservationDataView? { get }
    var userDataView: UserResponseDataView? { get }
    var prePaymentValue: Double { get }
    var totalValue: Double { get }
    var preAuthorizationValue: Double { get }
    var isPaymentInAdvance: Bool { get set }
    var isFranchise: Bool { get }
    var isExpress: Bool { get }
    var hasPendencies: Bool { get }
    var franchisePayAtPickupDescription: String { get }
    
    func viewDidLoad(reservation: ReservationDataView?, oldReservation: VehicleReservationsDataView?, isAntecipatedPayment:Bool)
    func paymentOption(at index: Int) -> PaymentOptionDataView
    func price(for paymentOption: PaymentOptionDataView) -> String?
    func didSelectPaymentOption(at index: Int)
    func didSelectComingByAirline()
    func didDeselectComingByAirline()
    func setAirlineInformation(flightNumber: String, companyAt row: Int)
    func setNewAirlineInformation(flightNumber: String, flightCompany: FlightCompany)
    func getAirlineInformation() -> AirlineInformationDataView?
    func makeReservation(voucher: String?)
    func fetchCards()
    func creditCardWidgetDidTapChangeCardButton()
    func flightCompanyName(at row: Int) -> String
    func didSelectFlightCompany(at row: Int)
    func fetchRentalRequirements()
    func fetchContractConditions()
    func fetchFlightCompanies()
    func addEventEcommercePurchase(success: Bool, reservationNumber: String)
    func logInitiateCheckoutEvent(reservation: ReservationDataView, reserveNumber: String?, quotationDataView: QuotationsDataView?)
    func updateNotifications(id: Int, active: Bool, voucher: String?)
    func fetchNotifications()
    func setupPaymentExpressToggle()
}

class ReservationPaymentStepPresenter: ReservationPaymentStepPresenterDelegate {
    
    weak var delegate: ReservationPaymentStepViewDelegate?
    var loggedUser: UserResponse? {
        return session.currentUser
    }
    var paymentOptions: [PaymentOption] = [.payInAdvance, .payAtPickup]
    var flightCompanies: [FlightCompany] = []
    var selectedPaymentOption: PaymentOption = .payInAdvance
    var airlineInformation: AirlineInformation? = nil
    var reservation: Reservation? = nil
    var oldReservation: VehicleReservations? = nil
    var reservationDataView: ReservationDataView? {
        guard let reservation = reservation else { return nil }
        return ReservationDataView(model: reservation)
    }
    var userDataView: UserResponseDataView?
    var reservationService: ReservationService
    var reservationDataStore: ReservationDataStore
    var paymentService: PaymentService
    var companyTermsService: CompanyTermsService
    var flightInformationService: FlightInformationService
    var firebaseAnalyticsService: FirebaseAnalyticsService
    var adjustAnalyticsService: AdjustAnalyticsService
    
    var isPaymentInAdvance = false
    
    var numberOfPaymentOptions: Int {
        if isPaymentInAdvance {
            return 1
        }
        if let isFranchise = reservationDataView?.pickUp?.garage?.isFranchise {
            if isFranchise {
                return 1
            }
        }
        if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
            if pendencies == true {
                return 1
            }
        }
        if let isExpress = reservationDataView?.pickUp?.garage?.model.type {
            if isExpress == .express {
                return 1
            }
        }
        return paymentOptions.count
    }
    var numberOfFlightCompanies: Int {
        return flightCompanies.count
    }
    var hasSetCard: Bool = false
    
    var prePaymentValue: Double {
        return reservation?.prePaymentValue ?? 0.0
    }
    
    var totalValue: Double {
        return reservation?.totalValue ?? 0.0
    }
    
    var preAuthorizationValue: Double {
        return reservation?.preAuthorizationValue ?? 0.0
    }
    
    var showsCreditCardWidget: Bool {
        if !(reservationDataView?.express ?? false) {
            return loggedUser != nil && selectedPaymentOption == .payInAdvance
        }
        return loggedUser != nil
    }
    
    var showsLoginCallout: Bool {
        return loggedUser == nil
    }
    
    var showsCreateAccountCallout: Bool {
        return loggedUser == nil && selectedPaymentOption == .payInAdvance
    }
    
    var showsUserForm: Bool {
        return loggedUser == nil && selectedPaymentOption == .payAtPickup
    }
    
    var showsRefundNotice: Bool {
        return loggedUser != nil && selectedPaymentOption == .payInAdvance
    }
    
    var showsRefundPolicyCheck: Bool {
        return selectedPaymentOption == .payInAdvance
    }
    
    var showsTermsAndAgreementsView: Bool {
        return loggedUser != nil || selectedPaymentOption == .payAtPickup
    }
    
    var showsFinishReservationButton: Bool {
        return loggedUser != nil || selectedPaymentOption == .payAtPickup
    }
     
    var payInAdvanceEnable : Bool {
        if let franchise = reservationDataView?.pickUp?.garage?.isFranchise{
            if franchise { return true }
        }
        
        if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
            if pendencies == true {
                return true
            }
        }
        
        if isExpress {
            return true
        }
        
        return ToggleProfile.model?.config?.payInAdvance ?? true
    }
    
    var payAtPickupEnable : Bool {
        return ToggleProfile.model?.config?.payAtPickup ?? true
    }
    
    var isFranchise: Bool {
        if let franchise = reservationDataView?.pickUp?.garage?.isFranchise {
            if franchise { return true }
        }
        return false
    }
    
    var isExpress: Bool {
        if let isExpress = reservationDataView?.pickUp?.garage?.model.type {
            if isExpress == .express {
                return true
            }
        }
        if reservation?.express ?? false {
            return true
        }
        return false
    }
    
    var hasPendencies: Bool {
        if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
            return pendencies
        }
        return false
    }
    
    var franchisePayAtPickupDescription: String {
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        guard let franchiseDescription = remoteConfig["franchise_payAtPickup_description"].stringValue else { return "" }
        return franchiseDescription
    }
    
    init(reservationService: ReservationService,
         reservationDataStore: ReservationDataStore,
         paymentService: PaymentService,
         companyTermsService: CompanyTermsService,
         flightInformationService: FlightInformationService,
         unidasGarageService: UnidasGarageService,
         firebaseAnalyticsService: FirebaseAnalyticsService,
         adjustAnalyticsService: AdjustAnalyticsService) {
        self.reservationService = reservationService
        self.reservationDataStore = reservationDataStore
        self.paymentService = paymentService
        self.companyTermsService = companyTermsService
        self.flightInformationService = flightInformationService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.adjustAnalyticsService = adjustAnalyticsService
        registerForLoginNotifications()
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    deinit {
        unregisterForLoginNotifications()
    }
    
    func viewDidLoad(reservation: ReservationDataView?, oldReservation: VehicleReservationsDataView?, isAntecipatedPayment: Bool) {
        
        isPaymentInAdvance = isAntecipatedPayment
        
        if let reserv = reservation?.model {
            self.reservation = reserv
        }else{
            self.reservation = ReservationModelObject.buildReservation(oldReservation: oldReservation!)
        }
        
        self.oldReservation = oldReservation?.model
        
        if reservation?.pickUp?.garage?.isFranchise ?? false {
            paymentOptions = [.payAtPickup]
            selectedPaymentOption = .payAtPickup
            delegate?.updatePaymentCollection(with: 144)
        }
        
        if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
            if pendencies == true {
                paymentOptions = [.payAtPickup]
                selectedPaymentOption = .payAtPickup
                delegate?.updatePaymentCollection(with: 144)
            }
        }
        
        if isPaymentInAdvance {
            paymentOptions = [.payInAdvance]
            selectedPaymentOption = .payInAdvance
            delegate?.updatePaymentCollection(with: 144)
        }
        
        guard let index = paymentOptions.firstIndex(of: selectedPaymentOption) else { return }
        if payInAdvanceEnable {
            delegate?.selectPaymentOption(at: index)
        } else if !payInAdvanceEnable && payAtPickupEnable {
            guard let index = paymentOptions.firstIndex(of: PaymentOption.payAtPickup) else { return }
            delegate?.selectPaymentOption(at: index)
            let indexPath = IndexPath(row: index, section: 0)
            didSelectPaymentOption(at: indexPath.row)
        }
        
        //delegate?.updateView()
        fetchCards()
        fetchFlightCompanies()
        fetchRefundPolicy()
        fetchPrePaymentValue()
    }
    
    func paymentOption(at index: Int) -> PaymentOptionDataView {
        return PaymentOptionDataView(model: paymentOptions[index], discount: reservation?.prePaymentDiscountValue)
    }
    
    func addEventEcommercePurchase(success: Bool, reservationNumber: String) {
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        parameters["reservation_id"] = reservationNumber
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.ecommercePurchase, withValue: parameters)
    }
    
    func price(for paymentOption: PaymentOptionDataView) -> String? {
        let value: Double?
        switch paymentOption.model {
        case .payInAdvance:
            value = reservation?.prePaymentValue
            break
        case .payAtPickup:
            value = reservation?.totalValue
            break
        }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value ?? 0.0))
    }
    
    func didSelectPaymentOption(at index: Int) {
        selectedPaymentOption = paymentOptions[index]
        delegate?.updateView()
    }
    
    func didSelectComingByAirline() {
        delegate?.showAirlineForm()
    }
    
    func didDeselectComingByAirline() {
        airlineInformation = nil
        delegate?.updateAirlineInformation()
    }
    
    func setAirlineInformation(flightNumber: String, companyAt row: Int) {
        airlineInformation = AirlineInformation(flightNumber: flightNumber, company: flightCompanies[row])
        delegate?.updateAirlineInformation()
    }
    
    func setNewAirlineInformation(flightNumber: String, flightCompany: FlightCompany) {
        airlineInformation = AirlineInformation(flightNumber: flightNumber, company: flightCompany)
    }
    
    func getAirlineInformation() -> AirlineInformationDataView? {
        guard let airlineInformation = airlineInformation else { return nil }
        return AirlineInformationDataView(model: airlineInformation)
    }
    
    func makeReservation(voucher: String?) {
        guard let user = getValidUser() else {
            if showsUserForm {                
                if let _ = delegate?.userFormData?.model {
                    //delegate?.present(error: ReservationPaymentStepError.emptyCiaAerea)
                    return
                }else{
                    //delegate?.present(error: ReservationPaymentStepError.userFormIncomplete)
                    return
                }
            } else {
                delegate?.present(error: ReservationPaymentStepError.noValidUserProvided)
                return
            }
        }
        
        if let isCiaAereaChecked = delegate?.isCiaAereaChecked {
            if isCiaAereaChecked == true {
                if let isCiaAereaDataEmpty = delegate?.isCiaAereaDataEmpty {
                    if isCiaAereaDataEmpty {
                        //delegate?.present(error: ReservationPaymentStepError.emptyCiaAerea)
                        return
                    }
                }
            }
        }
        
        var paymentInformation: CreditCardReservePayment? = nil
        guard let isRentalRequirementsAndContractualConditionsChecked = delegate?.isRentalRequirementsAndContractualConditionsChecked,
            isRentalRequirementsAndContractualConditionsChecked else {
                delegate?.present(error: ReservationPaymentStepError.rentalRequirementsAndContractualConditionsUnchecked)
                return
        }
        
        guard var reservation = reservation, let selectedPaymentOptionIndex = delegate?.selectedPaymentOptionIndex else {
            delegate?.present(error: GenericError.unknown)
            return
        }
        
        let selectedPaymentOption = getPaymentOption(selectedPaymentOptionIndex: selectedPaymentOptionIndex)
        reservation.selectedPaymentOption = selectedPaymentOption
        reservation.sendSMS = delegate?.isReceivePromotionalEmailChecked
        reservation.airlineInformation = airlineInformation
        reservation.promotionCode = voucher
        
        if oldReservation != nil {
            if let reserveNumber = oldReservation?.numeroReserva {
                reservation.number = String(reserveNumber)
            }
        }
        
        if selectedPaymentOption == .payInAdvance {
            guard let _ = delegate?.creditCardReservePaymentDataView?.model.securityCode else {
                delegate?.present(error: ReservationPaymentStepError.emptyCVV)
                return
            }
            if let cardDate = delegate?.creditCardReservePaymentDataView?.model.card.val,
                !CardUtils.isValid(expiration: cardDate, for: Date()){
                self.delegate?.present(error: ReservationPaymentStepError.creditCardExpiredDate)
                return
            }
        }
        
        if selectedPaymentOption == .payInAdvance {
            guard let creditCardReservePayment = delegate?.creditCardReservePaymentDataView?.model else {
                delegate?.present(error: ReservationPaymentStepError.creditCardNotSelected)
                return
            }
            paymentInformation = creditCardReservePayment
        }
        
        if showsRefundPolicyCheck {
            guard let isRefundNoticeChecked = delegate?.isRefundPoliceChecked, isRefundNoticeChecked else {
                delegate?.present(error: ReservationPaymentStepError.refundPolicyCheckUnchecked)
                return
            }
        }
        
        delegate?.startLoading()
        let uuid = UUID()
        
        reservation.acceptTermLocation = delegate?.isRentalRequirementsAndContractualConditionsChecked
        reservation.acceptTermRefound = delegate?.isRefundPoliceChecked
        reservation.express = isExpress
        
        if oldReservation == nil {
            
            if selectedPaymentOption == .payAtPickup {
                reservation.acceptTermRefound = nil
            }
            
            insertReservation(reservationInsert: reservation,
                              user: user,
                              paymentInformation: paymentInformation,
                              uuid: uuid)
        } else {
            if isPaymentInAdvance == false {
                
                if selectedPaymentOption == .payAtPickup {
                    reservation.acceptTermRefound = nil
                }
                
                updateReservation(reservation: reservation,
                                  user: user,
                                  paymentInformation: paymentInformation,
                                  uuid: uuid)
            }else{
                makePaymentReservation(reservation: reservation, user: user, paymentInformation: paymentInformation, uuid: uuid)
            }
        }
    }
    
    private func getPaymentOption(selectedPaymentOptionIndex: Int) -> PaymentOption {
        
        if isPaymentInAdvance {
            return PaymentOption.payInAdvance
        }
        
        if let isFranchise = reservationDataView?.pickUp?.garage?.isFranchise {
            if isFranchise { return PaymentOption.payAtPickup }
        }
        
        if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
            if pendencies == true {
                return PaymentOption.payAtPickup
            }
        }
        
        return PaymentOption(rawValue: selectedPaymentOptionIndex + 1) ?? PaymentOption.payInAdvance
        
    }
    
    private func insertReservation(reservationInsert: Reservation,
                                   user: UserResponse,
                                   paymentInformation: CreditCardReservePayment?,
                                   uuid: UUID) {
        
        reservationService.make(reservation: reservationInsert, user: user, paymentInformation: paymentInformation, and: uuid, response: (success: { [weak self] newReservationResponse in
            guard let reserveNumber = newReservationResponse.object?.reserveCode ?? reservationInsert.number else {
                if let errorMessage = newReservationResponse.message {
                    let error = ServiceError(message: errorMessage)
                    if self?.reservation?.number == nil {
                        self?.delegate?.enableBackButton()
                    }
                    self?.delegate?.present(error: error)
                }
                return
            }
            self?.reservation = reservationInsert
            self?.reservation?.number = reserveNumber
            self?.reservation?.paymentStatus = newReservationResponse.object?.payment?.status
            
            if let code = newReservationResponse.code, 200...201 ~= code {
                self?.userDataView = UserResponseDataView(model: user)
                ((try? self?.reservationDataStore.save(recoveryReservationInformation: RecoverReservationInformation(uuid: uuid, documentNumber: user.account.documentNumber, reserveNumber: reserveNumber))) as ()??)
                NotificationCenter.default.post(name: .UNDidMakeReservation, object: nil)
                self?.delegate?.showSuccessReservationView()
                
                self?.registerEvent(reservation: reservationInsert, reserveNumber: reserveNumber)
                
                self?.logPurchasedEvent(reservation: reservationInsert, reserveNumber: reserveNumber)
                if self?.selectedPaymentOption == .payInAdvance {
                    self?.addEventEcommercePurchase(success: true, reservationNumber: reserveNumber)
                }
            } else if let errorMessage = newReservationResponse.message {
                let error = ServiceError(message: errorMessage)
                self?.delegate?.present(error: error)
            }
            }, failure: { [weak self] error in
                
                //check status code = 300 - show redirect to update driver information
                if let serviceError = error as? ServiceError, serviceError.code == 300 {
                    self?.delegate?.showUpdateDriverLicence(message: serviceError.message)
                    return
                }
                
                self?.delegate?.enableBackButton()
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
        
    }
    
    private func updateReservation(reservation: Reservation,
                                   user: UserResponse,
                                   paymentInformation: CreditCardReservePayment?,
                                   uuid: UUID) {
        
        reservationService.update(reservation: reservation, user: user, paymentInformation: paymentInformation, and: uuid, response: (success: { [weak self] updateReservationResponse in
            
            guard let reserveNumber = updateReservationResponse.object?.reserveCode ?? reservation.number else {
                if let errorMessage = updateReservationResponse.message {
                    let error = ServiceError(message: errorMessage)
                    if self?.reservation?.number == nil {
                        self?.delegate?.enableBackButton()
                    }
                    self?.delegate?.present(error: error)
                }
                return
            }
            self?.reservation = reservation
            self?.reservation?.number = reserveNumber
            self?.reservation?.paymentStatus = updateReservationResponse.object?.payment?.status
            
            if let code = updateReservationResponse.code, 200...201 ~= code {
                self?.userDataView = UserResponseDataView(model: user)
                ((try? self?.reservationDataStore.save(recoveryReservationInformation: RecoverReservationInformation(uuid: uuid, documentNumber: user.account.documentNumber, reserveNumber: reserveNumber))) as ()??)
                NotificationCenter.default.post(name: .UNDidMakeReservation, object: nil)
                self?.delegate?.showSuccessReservationView()
                self?.registerEvent(reservation: reservation, reserveNumber: reserveNumber)
                self?.logPurchasedEvent(reservation: reservation, reserveNumber: reserveNumber)
                if self?.selectedPaymentOption == .payInAdvance {
                    self?.addEventEcommercePurchase(success: true, reservationNumber: reserveNumber)
                }
            } else if let errorMessage = updateReservationResponse.message {
                let error = ServiceError(message: errorMessage)
                self?.delegate?.present(error: error)
            }
            }, failure: { [weak self] error in
                
                //check status code = 300 - show redirect to update driver information
                if let serviceError = error as? ServiceError, serviceError.code == 300 {
                    self?.delegate?.showUpdateDriverLicence(message: serviceError.message)
                    return
                }
                
                self?.delegate?.enableBackButton()
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    private func makePaymentReservation(reservation: Reservation,
                                        user: UserResponse,
                                        paymentInformation: CreditCardReservePayment?,
                                        uuid: UUID) {
        
        reservationService.makePayment(reservation: reservation, user: user, paymentInformation: paymentInformation, and: uuid, response: (success: { [weak self] newReserationPaymentResponse in
            guard let reserveNumber = newReserationPaymentResponse.object?.reserveCode else {
                if let errorMessage = newReserationPaymentResponse.message {
                    let error = ServiceError(message: errorMessage)
                    if self?.reservation?.number == nil {
                        self?.delegate?.enableBackButton()
                    }
                    self?.delegate?.present(error: error)
                }
                return
            }
            self?.reservation = reservation
            self?.reservation?.number = reserveNumber
            self?.reservation?.paymentStatus = newReserationPaymentResponse.object?.payment?.status

            if let code = newReserationPaymentResponse.code, code == 201 {
                self?.userDataView = UserResponseDataView(model: user)
                ((try? self?.reservationDataStore.save(recoveryReservationInformation: RecoverReservationInformation(uuid: uuid, documentNumber: user.account.documentNumber, reserveNumber: "\(reserveNumber)"))) as ()??)
                NotificationCenter.default.post(name: .UNDidMakeReservation, object: nil)
                self?.delegate?.showSuccessReservationView()
                self?.addEventEcommercePurchase(success: true, reservationNumber: String(reserveNumber))
                self?.registerEvent(reservation: reservation)
                self?.logPurchasedEvent(reservation: reservation, reserveNumber: String(reserveNumber))
            } else if let errorMessage = newReserationPaymentResponse.message {
                let error = ServiceError(message: errorMessage)
                self?.delegate?.present(error: error)
            }
            }, failure: { [weak self] error in
                
                //check status code = 300 - show redirect to update driver information
                if let serviceError = error as? ServiceError, serviceError.code == 300 {
                    self?.delegate?.showUpdateDriverLicence(message: serviceError.message)
                    return
                }
                self?.delegate?.enableBackButton()
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
        
    }
    
    private func fetchPrePaymentValue(){
        if isPaymentInAdvance && isExpress {
            let protections = oldReservation?.servico?.filter { $0.tipo == "P" }.compactMap { $0.codigoOTA } ?? []
            
            guard let groupCode = oldReservation?.grupo, let rateQualifier = oldReservation?.tarifa else { return }
            paymentService.fetchPreAuthorizationValue(for: protections, groupCode: groupCode, rateQualifier: rateQualifier, response: (success: { [weak self] preAuthorizationEstimate in
                self?.delegate?.showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView(model: preAuthorizationEstimate))
                }, failure: { error in
                    self.delegate?.present(error: error)
            }, completion: {
            }))
        }
    }
    
    func creditCardWidgetDidTapChangeCardButton() {
        if hasSetCard {
            delegate?.showCardList()
        } else {
            delegate?.showCreateCard()
        }
    }
    
    func fetchRentalRequirements() {
        self.delegate?.startLoading()
        
        self.companyTermsService.fetchTerms(termsCompanyType: .rentalRequirements, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            
            self?.delegate?.showRentalRequirements(companyTermsDataView: CompanyTermsDataView(model: companyTerms))
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func fetchContractConditions() {
        self.delegate?.startLoading()
        
        self.companyTermsService.fetchTerms(termsCompanyType: .contractConditions, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            
            self?.delegate?.showContractConditions(companyTermsDataView: CompanyTermsDataView(model: companyTerms))
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func fetchFlightCompanies() {
        flightInformationService.fetchFlightCompanies(response: (success: { [weak self] flightCompanies in
            self?.flightCompanies = flightCompanies
            self?.delegate?.showFlightCompanies(flightCompany: flightCompanies)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: {
        }))
    }
    
    func flightCompanyName(at row: Int) -> String {
        return flightCompanies[row].description
    }
    
    func didSelectFlightCompany(at row: Int) {
        delegate?.updateFlightCompany(name: flightCompanyName(at: row))
    }
    
    private func registerForLoginNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(userDidLogin(notification:)), name: .UNUserDidLogin, object: nil)
        notificationCenter.addObserver(self, selector: #selector(userDidLogout(notification:)), name: .UNUserDidLogout, object: nil)
    }
    
    private func unregisterForLoginNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func userDidLogout(notification: Notification) {
        delegate?.updateView()
    }
    
    @objc func userDidLogin(notification: Notification) {
        fetchCards()
        delegate?.updateView()
    }
    
    func fetchCards() {
        guard let user = session.currentUser else { return }
        fetchCards(for: user)
    }
    
    func fetchRefundPolicy() {
        companyTermsService.fetchTerms(termsCompanyType: .refundPolicy, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            let companyTermsDataView = CompanyTermsDataView(model: companyTerms)
            
            self?.delegate?.showRefundPolicy(companyTermsDataView: companyTermsDataView)
            }, failure: { error in
                
        }, completion: {
            
        })
        )
    }
    
    private func getValidUser() -> UserResponse? {
        return loggedUser ?? userResponse()
    }
    
    private func userResponse() -> UserResponse? {
        guard let userForm = delegate?.userFormData?.model else { return nil }
        
        if delegate!.isCiaAereaChecked {
            if delegate!.isCiaAereaDataEmpty {
                return nil
            }
        }
        
        return UserResponse(token: nil, account: Account(id: nil, user:  nil, documentNumber: userForm.documentNumber, fcm: nil, mobilePhone: userForm.phoneNumber, email: userForm.email, name: userForm.name, companyName: nil, deviceId: nil, profileImage: nil, birthDate: userForm.birthDate, motherName: userForm.motherName), unidas: nil)
    }
    
    private func fetchCards(for user: UserResponse) {
        paymentService.listCard(user: user, response: (success: { cards in
            guard let card = cards.filter({$0.metodoPagamentoCartaoCreditoPreferencial == 1}).first ?? cards.first else { return }
            self.hasSetCard = true
            self.delegate?.setListCard(ListCardDataView(model: card))
        }, failure: { [weak self] error in
            if let error = error as? ServiceError, error.code == 404 {
                return
            }
            self?.delegate?.present(error: error)
            }, completion: {
                
        }))
    }
    
    func setupPaymentExpressToggle() {
        FirebaseRealtimeDatabase.toggleValue(withKey: .doPaymentExpress) { (value) in
            self.delegate?.showExpressPayment(value: value)
        }
    }
    
    func logInitiateCheckoutEvent(reservation: ReservationDataView, reserveNumber: String?, quotationDataView: QuotationsDataView?) {
        logFacebookInitiateCheckoutEvent(reservation: reservation, reserveNumber: reserveNumber, quotationDataView: quotationDataView)
        logAdjustInitiateCheckoutEvent(reservation: reservation, reserveNumber: reserveNumber, quotationDataView: quotationDataView)
    }
    
    private func logFacebookInitiateCheckoutEvent(reservation: ReservationDataView, reserveNumber: String?, quotationDataView: QuotationsDataView?){
        guard let currentUser = currentUser else { return }
        
        guard let enviroment = AppPersistence.getValue(withKey: "enviroment") else {return}
        
        var parameters = [
            AppEvents.ParameterName.contentID.rawValue: currentUser.account.documentNumberHash ?? ""
            ] as [String : Any]
        
        let daily_amount = quotationDataView?.model.vehicleCharge?.unitCharge
        let start_date = reservation.pickUp?.date
        let end_date = reservation.return?.date
        let totalPrice = reservation.model.totalValue ?? 0.0
        
        var protection_value = Double()
        var equipment_value = Double()
        
        if let protections = reservation.model.protections {
            for protection in protections {
                if let charge = protection.unitCharge {
                    protection_value += charge
                }
            }
        }
        
        if let selectedEquipments = reservation.model.equipments {
            for equipment in selectedEquipments {
                if let charge = equipment.charge?.unitCharge {
                    equipment_value += charge
                }
            }
        }
        
        parameters["daily_amount"] = daily_amount
        parameters["start_date"] = start_date
        parameters["end_date"] = end_date
        parameters["protections_amount"] = protection_value
        parameters["equipments_amount"] = equipment_value
        parameters["enviroment"] = enviroment
        
        AppEvents.logEvent(.initiatedCheckout, valueToSum: totalPrice, parameters: parameters)
    }
    
    private func logAdjustInitiateCheckoutEvent(reservation: ReservationDataView, reserveNumber: String?, quotationDataView: QuotationsDataView?){
        
        guard let currentUser = currentUser else { return }
        
        var parameters: [String : String] = [
            "user": currentUser.account.documentNumberHash ?? ""
        ]
        
        let daily_amount = quotationDataView?.model.vehicleCharge?.unitCharge
        let start_date = reservation.pickUp?.date
        let end_date = reservation.return?.date
        let totalPrice = reservation.model.totalValue ?? 0.0
        
        var protection_value = Double()
        var equipment_value = Double()
        
        if let protections = reservation.model.protections {
            for protection in protections {
                if let charge = protection.unitCharge {
                    protection_value += charge
                }
            }
        }
        
        if let selectedEquipments = reservation.model.equipments {
            for equipment in selectedEquipments {
                if let charge = equipment.charge?.unitCharge {
                    equipment_value += charge
                }
            }
        }

        parameters["daily_amount"] = String(describing: daily_amount ?? 0.0)
        parameters["start_date"] = start_date
        parameters["end_date"] = end_date
        parameters["protections_amount"] = String(describing: protection_value)
        parameters["equipments_amount"] = String(describing: equipment_value)
        parameters["total_price"] = String(describing: totalPrice)
        
        adjustAnalyticsService.addEvent(withEvent: .initiatedCheckout, withValue: parameters, amount: totalPrice, currency: "BRL")
    }
    
    func logPurchasedEvent(reservation: Reservation, reserveNumber: String? = nil) {
        logFacebookPurchasedEvent(reservation: reservation, reserveNumber: reserveNumber)
        logAdjustPurchasedEvent(reservation: reservation, reserveNumber: reserveNumber)
    }
    
    private func logFacebookPurchasedEvent(reservation: Reservation, reserveNumber: String? = nil) {
        guard let currentUser = currentUser else { return }
        
        guard let enviroment = AppPersistence.getValue(withKey: "enviroment")
            else {return}
        
        let duration = reservationDataView?.duration ?? 0
        let equipments = reservation.equipments?.count ?? 0
        let protections = reservation.protections?.count ?? 0
        let totalPrice = reservation.totalValue ?? 0.0
        
        var parameters = [
            AppEvents.ParameterName.contentID.rawValue: currentUser.account.documentNumberHash ?? "",
            AppEvents.ParameterName.numItems.rawValue: "\(Int(duration) + equipments + protections)",
            AppEvents.ParameterName.currency.rawValue: "BRL"
        ]
        
        if let reserveNumber = reserveNumber {
            parameters["reservation_number"] = reserveNumber
        }
        if let id = reservation.id {
            parameters["reservation_id"] = id
        }
        if let totalValue = reservation.totalValue {
            parameters["totalAmount"] = "\(totalValue)"
        }
        if let store = reservation.pickUp?.garage.description {
            parameters["store_name"] = store
        }
        if reservation.selectedPaymentOption == .payAtPickup{
            parameters["payment_option"] = "pay_at_pickup"
        } else if reservation.selectedPaymentOption == .payInAdvance {
            parameters["payment_option"] = "pay_in_advance"
        } else {
            parameters["payment_option"] = "unavailable"
        }
        if let airline = reservation.airlineInformation {
            parameters["airline_company"] = airline.company.description.removeSpecialChars.eventParameter
            parameters["airline_flight_number"] = airline.flightNumber.removeSpecialChars.eventParameter
        }
        
        //recover protections
        if let protections = reservation.protections {
            for protection in protections {
                if let name = protection.details?.first?.text, let charge = protection.unitCharge {
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        //recover equipments
        if let selectedEquipments = reservation.equipments {
            for equipment in selectedEquipments {
                if let name = equipment.description, let charge = equipment.charge?.unitCharge {
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        parameters["enviroment"] = enviroment
        
        AppEvents.logEvent(.purchased, valueToSum: totalPrice, parameters: parameters)
    }
    
    private func logAdjustPurchasedEvent(reservation: Reservation, reserveNumber: String? = nil){
        guard let currentUser = currentUser else { return }
        
        let duration = reservationDataView?.duration ?? 0
        let equipments = reservation.equipments?.count ?? 0
        let protections = reservation.protections?.count ?? 0
        let totalPrice = reservation.totalValue ?? 0.0
        
        var parameters = [
            "user" : currentUser.account.documentNumberHash ?? "",
            "num_items" : "\(Int(duration) + equipments + protections)",
            "currency" : "BRL"
        ]
        
        if let reserveNumber = reserveNumber {
            parameters["reservation_number"] = reserveNumber
        }
        if let id = reservation.id {
            parameters["reservation_id"] = id
        }
        if let totalValue = reservation.totalValue {
            parameters["totalAmount"] = "\(totalValue)"
        }
        if let store = reservation.pickUp?.garage.description {
            parameters["store_name"] = store
        }
        if reservation.selectedPaymentOption == .payAtPickup{
            parameters["payment_option"] = "pay_at_pickup"
        } else if reservation.selectedPaymentOption == .payInAdvance {
            parameters["payment_option"] = "pay_in_advance"
        } else {
            parameters["payment_option"] = "unavailable"
        }
        if let airline = reservation.airlineInformation {
            parameters["airline_company"] = airline.company.description.removeSpecialChars.eventParameter
            parameters["airline_flight_number"] = airline.flightNumber.removeSpecialChars.eventParameter
        }
        
        //recover protections
        if let protections = reservation.protections {
            for protection in protections {
                if let name = protection.details?.first?.text, let charge = protection.unitCharge {
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        //recover equipments
        if let selectedEquipments = reservation.equipments {
            for equipment in selectedEquipments {
                if let name = equipment.description, let charge = equipment.charge?.unitCharge {
                    parameters[name.eventParameter] = String(format:"%.1f", charge)
                }
            }
        }
        
        adjustAnalyticsService.addEvent(withEvent: .purchase, withValue: parameters, amount: totalPrice, currency: "BRL")
    }
    
    private func registerEvent(reservation: Reservation, reserveNumber: String? = nil){
        var parameters: [String : Any] = [:]
        
        if reservation.selectedPaymentOption == .payAtPickup{
            parameters["payment_option"] = "pay_at_pickup"
        } else if reservation.selectedPaymentOption == .payInAdvance {
            parameters["payment_option"] = "pay_in_advance"
        } else {
            parameters["payment_option"] = "unavailable"
        }
        if let reserveNumber = reserveNumber {
            parameters["reservation_number"] = reserveNumber
        }
        if let id = reservation.id {
            parameters["reservation_id"] = id
        }
        if let airline = reservation.airlineInformation {
            parameters["airline_company"] = airline.company.description.removeSpecialChars.eventParameter
            parameters["airline_flight_number"] = airline.flightNumber.removeSpecialChars.eventParameter
        }
        if let sms = reservation.sendSMS {
            parameters["send_sms"] = Bool(truncating: sms as NSNumber)
        }
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.finishReservation, withValue: parameters)
    }
    
    func updateNotifications(id: Int, active: Bool, voucher: String?) {
        delegate?.startLoading()
        
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        reservationService.updateNotifications(documentNumber: documentNumber, id: id, active: active, response: (success: {
            self.makeReservation(voucher: voucher)
        }, failure: { error in
            self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }
    
    func fetchNotifications(){
        delegate?.startLoading()
        
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        reservationService.fetchNotifications(documentNumber: documentNumber, response: (success: { [weak self] notifications in
            guard let items = notifications else { return }
            items.forEach { (item) in
                if item.code == 2 {
                    self?.delegate?.fetchNotificationStatus(status: item.active)
                }
            }
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }
}
