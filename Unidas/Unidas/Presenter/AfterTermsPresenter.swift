//
//  AfterTermsPresenter.swift
//  Unidas
//
//  Created by Felipe Machado on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol AfterTermsViewDelegate {
    func present(error: Error)
    func loadingScreen(value: Bool)
    func dismissViewController()
}

protocol AfterTermsPresenterDelegate {
    var delegate: AfterTermsViewDelegate? {get}
    func sendTermsToServer(privacyTerm: Bool) 
}

class AfterTermsPresenter: AfterTermsPresenterDelegate {
    var delegate: AfterTermsViewDelegate?
    
    private var postTermsService: PostTermsService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(postTermsService: PostTermsService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.postTermsService = postTermsService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func sendTermsToServer(privacyTerm: Bool) {
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        self.delegate?.loadingScreen(value: true)
        
        postTermsService.updateTerms(documentNumber: documentNumber, privacyTerm: privacyTerm, response: (success: { [weak self] in
                self?.delegate?.loadingScreen(value: false)
                self?.delegate?.dismissViewController()
            }, failure: { error in
                self.delegate?.loadingScreen(value: false)
                self.delegate?.present(error: error)
        }, completion: {
            print("success completion")
        }))
    }    
}
