//
//  MyAccountPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum PINError: LocalizedError {
    case confirmPin, currentPinIsInvalid
    
    var errorDescription: String? {
        switch self {
        case .currentPinIsInvalid:
            return NSLocalizedString("Invalid current PIN", comment: "Invalid current PIN error descrition")
        case .confirmPin:
            return NSLocalizedString("Invalid confirm PIN", comment: "Invalid confirm Pin error description")
        }
    }
}

protocol MyAccountViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func showMyAccountHeaderView(userResponseDataView: UserResponseDataView)
    func performSegue(identifier: String)
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController)
    func didFinishUpdatePin(pinViewController: PINViewController)
    func didFailConfirmPin(pinViewController: PINViewController, error: Error, callbackDidFailFinishPin: Bool)
    func didPinEndLoading(pinViewController: PINViewController)
    func didShowChooseProfileImage(isPreview: Bool, isUpdate: Bool, userResponseDataView: UserResponseDataView)
    func didOpenProfileGalery(data: Data)
}

protocol MyAccountPresenterDelegate {
    var delegate: MyAccountViewDelegate? { get set }
    var myAccountItems: [MyAccountItem] { get }
    func showMyAccountHeaderView()
    func to(segueIdentifier: String?)
    func showUpdatePhotoPerfil()
    func didFinishPin(userReponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController)
    func didTapCancelPin()
    func didTapProfileImage()
    func loadProfileImageGalery(userResponseDataView: UserResponseDataView)
    func myAccountItem(at: Int) -> MyAccountItemDataView
    func loadUserAccount()
}

class MyAccountPresenter: MyAccountPresenterDelegate {
    weak var delegate: MyAccountViewDelegate?
    var validatePIN: Bool = false
    var confirmPIN: Bool = false
    var pin: String?
    var accountImageService: AccountImageService
    var myAccountItems: [MyAccountItem] = []
    private var unidasLoginService: UnidasLoginService
    private var accountsService: AccountsService
    private var authUnidasService: AuthUnidasService
    private var recoveryService: RecoveryService
    private var userCheckService: UserCheckService
    
    init(accountImageService: AccountImageService, unidasLoginService: UnidasLoginService,
         accountsService: AccountsService, authUnidasService: AuthUnidasService,
         recoveryService: RecoveryService, userCheckService: UserCheckService) {
        
        self.accountImageService = accountImageService
        self.unidasLoginService = unidasLoginService
        self.accountsService = accountsService
        self.authUnidasService = authUnidasService
        self.recoveryService = recoveryService
        self.userCheckService = userCheckService
        
        insertItems()
    }
    
    func showMyAccountHeaderView() {
        guard let currentUser = session.currentUser else {
            return
        }
        delegate?.showMyAccountHeaderView(userResponseDataView: UserResponseDataView(model: currentUser))
    }
    
    func showUpdatePhotoPerfil() {
        delegate?.performSegue(identifier: "showUpdatePhotoPerfil")
    }
    
    func to(segueIdentifier: String?) {
        if let segue = segueIdentifier {
            delegate?.performSegue(identifier: segue)
        }
    }
    
    func didFinishPin(userReponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        if !validatePIN {
            self.didConfirmPassword(pin: pin, pinViewController: pinViewController)
            return
        }
        else if !confirmPIN {
            self.pin = pin
            confirmPIN = true
            
            delegate?.showNewPinController(title: "Confirme o novo PIN", isHiddenForgotPin: true, isHiddenCancelButton: true, userResponseDataView: userReponseDataView, pinViewController: pinViewController)
            delegate?.didPinEndLoading(pinViewController: pinViewController)
            return
        }
        else if confirmPIN {
            if self.pin == pin {
                updatePassword(pin: pin, pinViewController: pinViewController)
                return
            }
            
            confirmPIN = false
            delegate?.didFailConfirmPin(pinViewController: pinViewController, error: PINError.confirmPin, callbackDidFailFinishPin: true)
            delegate?.didPinEndLoading(pinViewController: pinViewController)
            return
        }
    }
    
    func didTapProfileImage() {
        guard let currentUser = session.currentUser else {
            return
        }
        
        self.delegate?.didShowChooseProfileImage(isPreview: (currentUser.account.profileImage != nil),
                                                 isUpdate: true,
                                                 userResponseDataView: UserResponseDataView(model: currentUser))
    }
    
    func loadProfileImageGalery(userResponseDataView: UserResponseDataView) {
        self.delegate?.startLoading()
        
        accountImageService.getImage(documentNumber: userResponseDataView.model.account.documentNumber, type: ImageType.profile, response: (success: { [weak self] imageProfile in
            self?.delegate?.didOpenProfileGalery(data: imageProfile)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func loadUserAccount() {
        guard let currentUser = session.currentUser,
            let userUnidasSession = currentUser.unidas?.first,
            let documentNumber = userUnidasSession.clidoc else { return }
        
        userCheckService.checkDocument(documentNumber: documentNumber,
                                       response: (success: { usersUnidas in
                                        guard let userUnidas = usersUnidas.first, let userResponse = UserResponse(userUnidas: userUnidas) else { return }
                                        
                                        let urlImage = session.currentUser?.account.profileImage
                                        session.currentUser = userResponse
                                        session.currentUser?.account.profileImage = urlImage
                                        
                                        // self?.delegate?.refreshAllLoading(userResponseDataView: UserResponseDataView(model: userResponse))
                                       }, failure: { [weak self] error in
                                        self?.delegate?.present(error: error)
                                        }, completion: {
                                       }))
    }
    
    func didTapCancelPin() {
        self.validatePIN = false
        self.confirmPIN = false
        self.pin = nil
    }
    
    func myAccountItem(at: Int) -> MyAccountItemDataView {
        return MyAccountItemDataView(model: myAccountItems[at])
    }
    
    // MARK: - Private Function
    
    private func didConfirmPassword(pin: String, pinViewController: PINViewController) {
        guard let userResponse = session.currentUser else { return }
        
        unidasLoginService.auth(documentNumber: userResponse.account.documentNumber, pin: pin, response: (success: { [weak self] userUnidas in
            if let alertError = userUnidas.contaClienteAlerta {
                self?.delegate?.didFailConfirmPin(pinViewController: pinViewController, error: ServiceError(message: alertError), callbackDidFailFinishPin: false)
                return
            }
            
            session.currentUser = self?.userResponseDataView(userUnidas: userUnidas)?.model
            
            if let token = userUnidas.contaClienteToken {
                ServiceConfiguration.unidas.headers?.add(name: "Authorization", value: "Bearer \(token)")
            }
            
            self?.validatePIN = true
            self?.delegate?.showNewPinController(title: NSLocalizedString("Type your new PIN", comment: ""), isHiddenForgotPin: true, isHiddenCancelButton: false, userResponseDataView: self?.userResponseDataView(userUnidas: userUnidas), pinViewController: pinViewController)
            }, failure: { [weak self] error in
                self?.delegate?.didFailConfirmPin(pinViewController: pinViewController, error: PINError.currentPinIsInvalid, callbackDidFailFinishPin: false)
            }, completion: { [weak self] in
                self?.delegate?.didPinEndLoading(pinViewController: pinViewController)
        })
        )
    }
    
    private func userResponseDataView(userUnidas: UserUnidas) -> UserResponseDataView? {
        guard let codePhone = userUnidas.contaClienteLoginCelularDDD,
            let phoneNumber = userUnidas.contaClienteLoginCelular,
            let documentNumber = userUnidas.clidoc,
            let email = userUnidas.contaClienteLoginEmail,
            let name = userUnidas.contaClienteNome,
            let birthDate = userUnidas.contaClienteNascimento else { return nil }
        
        let formattedPhone = "\(String(codePhone))\(String(phoneNumber))"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        
        let user = UserResponseDataView(model: UserResponse(token: nil, account: Account(id: nil, user: nil, documentNumber: documentNumber, fcm: nil, mobilePhone: formattedPhone, email: email, name: name, companyName: nil, deviceId: nil, profileImage: session.currentUser?.account.profileImage, birthDate: dateFormatter.date(from: birthDate), motherName: userUnidas.contaClienteNomeMae), unidas: [userUnidas]))
        
        return user
    }
    
    private func updatePassword(pin: String, pinViewController: PINViewController) {
        guard let currentUser = session.currentUser else { return }
        authUnidasService.update(user: currentUser, unidasToken: nil, pin: pin, response: (success: { [weak self] in
            self?.validatePIN = false
            self?.confirmPIN = false
            self?.pin = nil
            self?.delegate?.didFinishUpdatePin(pinViewController: pinViewController)
            }, failure: { [weak self] error in
                self?.delegate?.didFailConfirmPin(pinViewController: pinViewController, error: error, callbackDidFailFinishPin: true)
            }, completion: { [weak self] in
                self?.removeKeyChain(withKey: currentUser.account.documentNumber)
                self?.addCredentials(documentNumber: currentUser.account.documentNumber, pin: pin)
                self?.delegate?.didPinEndLoading(pinViewController: pinViewController)
        }))
    }
    
    private func addCredentials(documentNumber: String, pin: String) {
        let r = KeychainHelper.createBioProtectedEntry(key: documentNumber.removeDocumentNumberCharacters, data: Data(pin.utf8))
        print(r == noErr ? "Entry created" : "Entry creation failed, osstatus=\(r)")
    }
    
    
    private func removeKeyChain(withKey key: String) {
        KeychainHelper.remove(key: key.removeDocumentNumberCharacters)
    }
    
    private func insertItems(){
        let personalData = MyAccountItem(image: "icon-personal-data-menu", localizedString: "My Account Menu Item - Personal Data", action: "showUserFormsDetails")
        myAccountItems.append(personalData)
        
        let personalAddress = MyAccountItem(image: "icon-residencial-menu", localizedString: "My Account Menu Item - Personal Address", action: "showAddress")
        myAccountItems.append(personalAddress)
        
        let documents = MyAccountItem(image: "icon-document-menu", localizedString: "My Account Menu Item - Document", action: "showDriverLicense")
        myAccountItems.append(documents)
        
        let signature = MyAccountItem(image: "icon-signature-menu", localizedString: "My Account Menu Item - Signature", action: "showSignature")
        myAccountItems.append(signature)
        
        let paymentsData = MyAccountItem(image: "icon-payment-menu", localizedString: "My Account Menu Item - Payment Data", action: "showPayments")
        myAccountItems.append(paymentsData)
        
        let changePassword = MyAccountItem(image: "icon-change-password-menu", localizedString: "My Account Menu Item - Change password", action: "showPIN", separator: false)
        myAccountItems.append(changePassword)
        
    }
}
