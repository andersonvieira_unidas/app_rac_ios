//
//  PINPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 06/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol PINViewDelegate: class {
    func showPin(at index: Int)
    func addNewPin(count: Int)
    func removePin(count: Int)
    func removeAllPins()
    
    func startLoading()
    func endLoading()
    func present(error: Error, callbackDidFailFinishPin: Bool)
    func enableNotifications()
    func didFinishTyping(at pin: String)
}

protocol PINPresenterDelegate {
    var delegate: PINViewDelegate? { get set }
    var numberOfPins: Int { get }
    var pinValues: [Character] { get set }
    
    func viewDidLoad(navigationTitle: String?, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool)
    func selectNewPinValue(at value: Int)
    func backspacePin()
    func removeAllValue()
}

class PINPresenter: PINPresenterDelegate {
    weak var delegate: PINViewDelegate?
    var pinValues: [Character] = []
    var numberOfPins: Int = 6

    func viewDidLoad(navigationTitle: String?, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool) {
        self.loadPin(at: 0)
    }
    
    func selectNewPinValue(at value: Int) {
        if let newValue = newPinValue(value: value) {
            if pinValues.count < numberOfPins {
                pinValues.append(newValue)
                self.delegate?.addNewPin(count: pinValues.count)
                
                if (pinValues.count) == numberOfPins {
                    didFinishTyping()
                }
            }
        }
    }
    
    func backspacePin() {
        if pinValues.count > 0 {
            pinValues.removeLast()
            self.delegate?.removePin(count: pinValues.count)
        }
    }
    
    func removeAllValue() {
        pinValues.removeAll()
    }
    
    // MARK - Private function
    
    private func didFinishTyping() {
        self.delegate?.startLoading()
        
        let pin = String(self.pinValues)
        
        delegate?.didFinishTyping(at: pin)
    }

    private func newPinValue(value: Int) -> Character? {
        return String(value).first
    }
    
    private func loadPin(at index: Int) {
        self.delegate?.showPin(at: index)
        
        if numberOfPins > index + 1 {
            loadPin(at: index + 1)
        }
    }
}
