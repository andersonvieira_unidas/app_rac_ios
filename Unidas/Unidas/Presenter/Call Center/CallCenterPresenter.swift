//
//  CallCenterPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 30/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol CallCenterViewDelegate: class {
    func startLoading()
    func endLoading()
    func reloadData()
    func openBusinessChat(url: URL)
    func present(error: Error)
}

protocol CallCenterPresenterDelegate {
    var delegate: CallCenterViewDelegate? { get set }
    var numberOfRows: Int { get }
    func fetchCallCenter()
    func callCenter(at row: Int) -> CallCenterDataView?
    func didSelectRowAt(at indexPath: IndexPath)
}

class CallCenterPresenter: CallCenterPresenterDelegate {
    weak var delegate: CallCenterViewDelegate?
    private var callCenter: [CallCenter] = []
    private var callCenterService: CallCenterService
    
    private var assistance: Bool = false
    private var reservationCenter: Bool = false
    
    var numberOfRows: Int {
        return callCenter.count
    }
    
    init(callCenterService: CallCenterService) {
        self.callCenterService = callCenterService
    }
    
    func callCenter(at row: Int) -> CallCenterDataView? {
        return CallCenterDataView(model: callCenter[row])
    }
    
    func fetchCallCenter() {
        if !ReachabilityTest.isConnectedToNetwork() {
            self.populateCallCenter(callCenterResponse: session.callCenter)
            return
        }
        
        self.delegate?.startLoading()
        
        callCenterService.fetch(response: (success: { [weak self] callCenterResponse in
            session.callCenter = callCenterResponse
            
            self?.populateCallCenter(callCenterResponse: callCenterResponse)
            }, failure: { [weak self] error in
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self?.populateCallCenter(callCenterResponse: session.callCenter)
                    return
                }
                
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func didSelectRowAt(at indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            let url = generateURLBusinessChat()
            delegate?.openBusinessChat(url: url)
        }
    }
    
    private func generateURLBusinessChat() -> URL {
        let paramBodyLocalized = NSLocalizedString("Business Chat Body Param", comment: "")
        return BCUrlConstructor(intentId: "SAC", groupId: "SAC", bodyParam: paramBodyLocalized)
    }
       
    private  func BCUrlConstructor (intentId: String, groupId: String, bodyParam: String) -> URL {
       let bizId = "f019a75a-9217-457d-8819-b81b575b1ecd"
       let url : NSString = "https://bcrw.apple.com/sms:open?service=iMessage&recipient=urn:biz:\(bizId)&biz-intent-id=\(intentId)&bizgroup-id=\(groupId)&body=\(bodyParam)" as NSString
       let urlString : NSString = url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)! as NSString
       let bcUrl : NSURL = NSURL(string: urlString as String)!
       return bcUrl as URL
    }

    private func populateCallCenter(callCenterResponse: CallCenterResponse?) {
        callCenter.removeAll()
        
        guard let callCenterResponse = callCenterResponse else {
            self.delegate?.reloadData()
            return
        }
        
        callCenter.append(CallCenter(type: .customerService,
                                     title: callCenterResponse.titleSAC,
                                     description: callCenterResponse.descriptionSAC,
                                     phoneNumber: callCenterResponse.phoneNumberSAC))
        
        callCenter.append(CallCenter(type: .reservationCenter,
                                     title: callCenterResponse.tileCentralReserve,
                                     description: callCenterResponse.descriptionCentralReserve,
                                     phoneNumber: callCenterResponse.phoneNumberCentralReserve))
        
        callCenter.append(CallCenter(type: .assistance,
                                     title: callCenterResponse.titleAssistence,
                                     description: callCenterResponse.descriptionAssistence,
                                     phoneNumber: callCenterResponse.phoneNumberAssistence))
        self.delegate?.reloadData()
    }
}
