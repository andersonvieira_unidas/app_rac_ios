//
//  VehicleCharacteristicsPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 02/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//
import Foundation
import Alamofire
import Kanna

protocol VehicleCharacteristicsViewDelegate: class {
    func reloadData()
    func showEmptyView()
    func hideEmptyView()
}

protocol VehicleCharacteristicsPresenterDelegate {
    
    var delegate: VehicleCharacteristicsViewDelegate? { get set }
    
    var numberOfCharacteritics: Int { get }
    
    //func viewDidLoad(vehicleCharacteristicDataView: [VehicleCharacteristicDataView])
    func viewDidLoad(vehicleCategory: String)
    
    func characterict(at index: Int) -> VehicleCharacteristicDataView
    
}

class VehicleCharacteristicsPresenter: VehicleCharacteristicsPresenterDelegate {
    
    weak var delegate: VehicleCharacteristicsViewDelegate?
    var vehicleCharacterstic: [VehicleCharacteristic]!
    
    func viewDidLoad(vehicleCharacteristicDataView: [VehicleCharacteristicDataView]) {
        self.vehicleCharacterstic = vehicleCharacteristicDataView.map { $0.model }
        self.delegate?.reloadData()
    }
    
    func viewDidLoad(vehicleCategory: String) {
        //self.vehicleCharacterstic = vehicleCharacteristicDataView.map { $0.model }
        vehicleCharacterstic = []
        self.delegate?.hideEmptyView()
        
        if let url = recoverUrl(byGroup: vehicleCategory){
            scraperGroup(url: url)
        }else {
            self.delegate?.showEmptyView()
        }
        self.delegate?.reloadData()
    }
    
    var numberOfCharacteritics: Int {
        return vehicleCharacterstic.count
    }
    
    func characterict(at index: Int) -> VehicleCharacteristicDataView {
        return VehicleCharacteristicDataView(model: vehicleCharacterstic[index])
    }
    
    private func recoverUrl(byGroup group : String) -> URL? {
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        let allGroupsRemoteConfig = remoteConfig["umbraco_groups"].stringValue
        
        guard let allGroups = allGroupsRemoteConfig else { return nil }

        let jsonData = allGroups.data(using: .utf8)!
        let decoder = JSONDecoder()
        
        if let groups = try? decoder.decode([UmbracoGroup].self, from: jsonData){
            let groupFiltered = groups.filter { (umbracoGroup) -> Bool in
                umbracoGroup.group == group
            }

            if let urlGroup = groupFiltered.first?.url, let safeURL = urlGroup.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: safeURL) {
                return url
            }
        }
        return nil
    }
    
    private func scraperGroup(url: URL){
        AF.request(url.absoluteString).responseString { dataResponse in
            switch(dataResponse.result) {
            case .success(let data):
                self.parseHtml(html: data)
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }
    
    private func parseHtml(html: String){
        guard let doc = try? Kanna.HTML(html: html, encoding: String.Encoding.utf8) else { return }
        for item in doc.xpath("//*[contains(@class, 'banner__features-list')]//ul//li") {
            if let title = item.text, let imageExtract = item.xpath("img/@src").first?.text, let image = URL(string: "https://unidas.com.br\(imageExtract)") {
                vehicleCharacterstic.append(VehicleCharacteristic(image: image, name: title))
            }
        }
        self.delegate?.reloadData()
    }
}

struct UmbracoGroup: Codable{
    let group: String
    let url: String
}
