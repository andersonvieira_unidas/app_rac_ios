//
//  SignUpPhotoInformationPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 20/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol SelfieRecomendationsViewDelegate: class {
    func startLoadingCamera()
    func endLoadingCamera()
    func presentCamera(error: Error)
    func finishAddDocument()
    func finishUpdateDocument()
    func showSelfieFailureView()
}

protocol SelfieRecomendationsPresenterDelegate {
    var delegate: SelfieRecomendationsViewDelegate? { get set }
    
    func addDocument(image: Data?, formsAction: FormsAction)
    func addEventSignUpPersonalPhotoContinueLaterStep(success: Bool)
}

class SelfieRecomendationsPresenter: SelfieRecomendationsPresenterDelegate {
    weak var delegate: SelfieRecomendationsViewDelegate?
    private var accountPhotoUnidasService: AccountPhotoUnidasService
    private var imageTemporaryDirectory: ImageTemporaryDirectory
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(accountPhotoUnidasService: AccountPhotoUnidasService, imageTemporaryDirectory: ImageTemporaryDirectory, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.accountPhotoUnidasService = accountPhotoUnidasService
        self.imageTemporaryDirectory = imageTemporaryDirectory
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func addEventSignUpPersonalPhotoStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_personal_photo"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpPersonalPhoto, withValue: parameters)
    }
    
    func addEventSignUpPersonalPhotoContinueLaterStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_personal_photo_continue_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpPersonalPhotoContinueLater, withValue: parameters)
    }
    
    func addEventSelfieError(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters[AnalyticsEvents.selfieError.name] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.selfieError, withValue: parameters)
    }

    func addDocument(image: Data?, formsAction: FormsAction) {
        guard let currentUser = session.currentUser else { return }
        self.delegate?.startLoadingCamera()

        guard let imageBase64 = image?.base64EncodedString() else { return }
        let imageType = ImageType.profile

        accountPhotoUnidasService.saveDocument(user: currentUser, userImage: imageBase64, imageType: imageType, response: (success: { [weak self] in

            self?.imageTemporaryDirectory.addImage(userResponse: currentUser, type: .profile)
            (formsAction == .create) ? self?.delegate?.finishAddDocument() : self?.delegate?.finishUpdateDocument()
                self?.addEventSignUpPersonalPhotoStep(success: true)
            }, failure: { [weak self] error in
                self?.addEventSignUpPersonalPhotoStep(success: false)
                self?.addEventSelfieError(success: false)
                self?.delegate?.showSelfieFailureView()
            }, completion: { [weak self] in
                self?.delegate?.endLoadingCamera()
            })
        )
    }
}


