//
//  DriverLicenseDetailsFormPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

protocol DriverLicenseDetailsFormViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func didFinishCreateDriverLicense()
}

protocol DriverLicenseDetailsFormPresenterDelegate {
    var delegate: DriverLicenseDetailsFormViewDelegate? { get set }
    func updateDriverLicenseDetails(driverLicenseFormDataView: DriverLicenseFormDataView?)
}

class DriverLicenseDetailsFormPresenter: DriverLicenseDetailsFormPresenterDelegate {
    weak var delegate: DriverLicenseDetailsFormViewDelegate?

    private var accountsService: AccountsService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(accountsService: AccountsService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.accountsService = accountsService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }

    func updateDriverLicenseDetails(driverLicenseFormDataView: DriverLicenseFormDataView?) {
        guard   let driverLicenseForm = driverLicenseFormDataView?.model,
                let userResponse = session.currentUser else {
            return
        }
        
        self.delegate?.startLoading()

        accountsService.updateDriverLicense(user: userResponse, driverLicense: driverLicenseForm , response: (success: {  
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
                self?.delegate?.didFinishCreateDriverLicense()
        }))
    }
}
