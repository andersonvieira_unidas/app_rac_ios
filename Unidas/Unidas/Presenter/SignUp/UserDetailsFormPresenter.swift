//
//  SignUpPersonalInformationPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 13/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol UserDetailsFormViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func didFinishCreateUser(userResponseDataView: UserResponseDataView)
    func didFinishUpdateUser(userResponseDataView: UserResponseDataView, isPerformSegue: Bool)
    func reloadData()
}

protocol UserDetailsFormPresenterDelegate {
    var delegate: UserDetailsFormViewDelegate? { get set }
    
    func updateOrCreateUserDetails(userFormDataView: UserFormDataView?, oldPhoneNumber: String?, formsAction: FormsAction)
    func addEventSignUpPersonalDataStep(success: Bool, documentNumber: String)
}

class UserDetailsFormPresenter: UserDetailsFormPresenterDelegate {
   
    weak var delegate: UserDetailsFormViewDelegate?
    private var accountsService: AccountsService
    private var recoveryService: RecoveryService
    private var userCheckService: UserCheckService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(accountsService: AccountsService, recoveryService: RecoveryService, userCheckService: UserCheckService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.accountsService = accountsService
        self.recoveryService = recoveryService
        self.userCheckService = userCheckService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func updateOrCreateUserDetails(userFormDataView: UserFormDataView?, oldPhoneNumber: String?, formsAction: FormsAction) {
        guard let userForm = userFormDataView?.model else {
            //self.delegate?.present(error: UserDetailsFormError.allFieldsRequired)
            return
        }
        
        switch formsAction {
        case .create:
            self.createUser(userForm: userForm)
            break
        case .update:
            //check changed phone number
            //self.delegate
            var changedPhoneNumber = false
            if let oldPhoneNumber = oldPhoneNumber, oldPhoneNumber != userFormDataView?.model.phoneNumber {
                changedPhoneNumber = true
            }
            
            self.updateUser(userForm: userForm, changedPhoneNumber: changedPhoneNumber)
            break
        case .preUpdate:
            self.generateExpiredToken(userForm: userForm, formsAction: .preUpdate)
            break
        }
    }
    
    func addEventSignUpPersonalDataStep(success: Bool, documentNumber: String) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_personal_data"] = success
        
        let utf8str = documentNumber.data(using: String.Encoding.utf8)
        if let base64DocumentNumber = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: String.Encoding.utf8.rawValue)) {
            self.firebaseAnalyticsService.addUserEvent(withProperty: AnalyticsUserProperty.documentNumber, withValue: base64DocumentNumber)
        }        
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpPersonalDataStep, withValue: parameters)
    }
    
    // MARK: - Private Functions
    
    private func createUser(userForm: UserForm) {
        self.delegate?.startLoading()
        
        let documentNumber = userForm.documentNumber
        let email = userForm.email
        let mobilePhone = userForm.phoneNumber

        userCheckService.checkUser(documentNumber: documentNumber,
                                   email: email,
                                   ddd: mobilePhone.areaCode ?? 0,
           phone: mobilePhone.phoneNumber ?? 0, response: (success: { [weak self] _ in
            
                AppPersistence.save(key: AppPersistence.documentNumber, value: userForm.documentNumber)
                AppPersistence.save(key: AppPersistence.name, value: userForm.name)
            
                if let birthday = userForm.birthDate {
                    let birthdayString = ValueFormatter.format(date: birthday, format: "YYYY-MM-dd'T'HH:mm:ss.SSS'Z'")
 
                    AppPersistence.save(key: AppPersistence.birthday, value: birthdayString)
                }
            
                AppPersistence.save(key: AppPersistence.email, value: userForm.email)
                AppPersistence.save(key: AppPersistence.mobilePhone, value: userForm.phoneNumber)
            
                if let motherName = userForm.motherName {
                    AppPersistence.save(key: AppPersistence.motherName, value: motherName)
                }
            
                guard let userResponseDataView = self?.userResponseDataView(userForm: userForm, unidasToken: nil) else { return }
                self?.delegate?.didFinishCreateUser(userResponseDataView: userResponseDataView)
            self?.addEventSignUpPersonalDataStep(success: true, documentNumber: documentNumber)
            
            }, failure: { [weak self] error in
                self?.delegate?.endLoading()
                self?.delegate?.present(error: error)
                self?.addEventSignUpPersonalDataStep(success: false, documentNumber: documentNumber)
            }, completion: {}
        ))
    }
    
    private func updateUser(userForm: UserForm, changedPhoneNumber: Bool) {
        self.delegate?.startLoading()

        accountsService.updateUser(user: userForm, unidasToken: nil, password: nil, response: (success: { [weak self] in
            self?.updateCurrentUserInformation(userForm: userForm, changedPhoneNumber: changedPhoneNumber)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))

    }
    
    private func preUpdateUser(userForm: UserForm, unidasToken: UnidasToken) {
        self.delegate?.startLoading()
        
        accountsService.updateUser(user: userForm, unidasToken: unidasToken.token, password: nil, response: (success: { [weak self] in
            self?.delegate?.endLoading()
            guard let userResponseDataView = self?.userResponseDataView(userForm: userForm, unidasToken: unidasToken) else { return }
            
            self?.resendCode(userResponse: userResponseDataView.model)
            self?.delegate?.didFinishUpdateUser(userResponseDataView: userResponseDataView,
                                                isPerformSegue: true)
            }, failure: { [weak self] error in
                self?.delegate?.endLoading()
                self?.delegate?.present(error: error)
            }, completion: {
        }))
    }
    
    private func generateExpiredToken(userForm: UserForm, formsAction: FormsAction) {
        self.delegate?.startLoading()
        
        userCheckService.generateExpiredToken(documentNumber: userForm.documentNumber, response: (success: { [weak self] unidasToken in
            self?.delegate?.endLoading()
            guard let userResponseDataView = self?.userResponseDataView(userForm: userForm, unidasToken: unidasToken) else { return }
            
            if formsAction == .create {
                self?.delegate?.didFinishCreateUser(userResponseDataView: userResponseDataView)
            }
            else if formsAction == .preUpdate {
                self?.preUpdateUser(userForm: userForm, unidasToken: unidasToken)
            }
            
            }, failure: { [weak self] error in
                self?.delegate?.endLoading()
                self?.delegate?.present(error: error)
            }, completion: {
        }))
    }
    
    private func updateCurrentUserInformation(userForm: UserForm, changedPhoneNumber: Bool) {
        self.delegate?.startLoading()
        
        let newCpf = userForm.documentNumber.removeDocumentNumberCharacters
 
        userCheckService.checkDocument(documentNumber: newCpf, response: (success: { [weak self] userUnidas in
            guard let userUnidas = userUnidas.first,
                let userResponseDataView = self?.userResponseDataView(userUnidas: userUnidas) else { return }
            
            let token = session.currentUser?.unidas?.first?.contaClienteToken
            session.currentUser = userResponseDataView.model
            session.currentUser?.unidas?[0].contaClienteToken = token
        
            guard let currentUser = session.currentUser else { return }
            
            //if changed mobile phone - perfomSegue = true
            self?.delegate?.didFinishUpdateUser(userResponseDataView: UserResponseDataView(model: currentUser), isPerformSegue: changedPhoneNumber)
            
            NotificationCenter.default.post(name: .UNDidUpdateCurrentUser, object: session.currentUser)
            }, failure: { [weak self] error in
            self?.delegate?.present(error: error)
            }, completion: { [weak self] in
            self?.delegate?.endLoading()
        }))
    }
    
    private func resendCode(userResponse: UserResponse) {
        guard let userUnidas = userResponse.unidas?.first,
            let name = userUnidas.contaClienteNome,
            let phoneCodeNumber = userUnidas.contaClienteLoginCelularDDD,
            let phoneNumber = userUnidas.contaClienteLoginCelular,
            let documentNumber = userUnidas.clidoc,
            let email = userUnidas.contaClienteLoginEmail else { return }
        
        recoveryService.resendSecretCode(name: name, phoneCodeNumber: phoneCodeNumber, phoneNumber: phoneNumber, documentNumber: documentNumber, email: email, response: (success: {
            }, failure: { error in
            }, completion: {
        }))
    }
    
    private func userResponseDataView(userUnidas: UserUnidas) -> UserResponseDataView? {
        guard let codePhone = userUnidas.contaClienteLoginCelularDDD,
            let phoneNumber = userUnidas.contaClienteLoginCelular,
            let documentNumber = userUnidas.clidoc,
            let email = userUnidas.contaClienteLoginEmail, let name = userUnidas.contaClienteNome,
            let birthDate = userUnidas.contaClienteNascimento else { return nil }
        
        let formattedPhone = "\(String(codePhone))\(String(phoneNumber))"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        
        let userResponse = UserResponse(token: nil, account: Account(id: nil, user: nil, documentNumber: documentNumber, fcm: nil, mobilePhone: formattedPhone, email: email, name: name, companyName: nil, deviceId: nil, profileImage: session.currentUser?.account.profileImage, birthDate: dateFormatter.date(from: birthDate), motherName: userUnidas.contaClienteNomeMae), unidas: [userUnidas])
        
        return UserResponseDataView(model: userResponse)
    }
    
    private func userResponseDataView(userForm: UserForm, unidasToken: UnidasToken?) -> UserResponseDataView {

        let newCpf = userForm.documentNumber.removeDocumentNumberCharacters
        let phoneDDD = userForm.phoneNumber[userForm.phoneNumber.startIndex..<userForm.phoneNumber.index(userForm.phoneNumber.startIndex, offsetBy: 2)]
        let phoneNumber = userForm.phoneNumber[userForm.phoneNumber.index(userForm.phoneNumber.startIndex, offsetBy: 2)..<userForm.phoneNumber.endIndex]
        
        let unidas = UserUnidas(contaClienteId: nil,
                                flgAreaLogada: nil,
                                contaStatusId: UserStatus.userCreated,
                                contaStatusDescricao: "1",
                                ContaClienteTipoDescricao: "Conta criada",
                                clidoc: newCpf,
                                contaClienteNome: userForm.name,
                                contaClienteNomeMae: userForm.motherName,
                                contaClienteNascimento: "",
                                contaClienteTelResDDD: nil,
                                contaClienteTelRes: nil,
                                contaClienteAutenticacaoChave: nil,
                                contaClienteLoginEmail: userForm.email,
                                contaClienteLoginEmailAtivo: 1,
                                contaClienteLoginEmailMascarado: nil,
                                contaClienteLoginEmailValidacao: nil,
                                contaClienteLoginCelularDDD: String(phoneDDD),
                                contaClienteLoginCelular: String(phoneNumber),
                                contaClienteLoginCelularValidacao: nil,
                                contaClienteLoginCelularMascarado: nil,
                                contaClienteLoginCelularDeviceID: nil,
                                contaClienteEnderecoResidencialPais: nil,
                                contaClienteEnderecoResidencialCEP: nil,
                                contaClienteEnderecoResidencialLogradouro: nil,
                                contaClienteEnderecoResidencialLogradouroNumero: nil,
                                contaClienteEnderecoResidencialLogradouroComplemento: nil,
                                contaClienteEnderecoResidencialBairro: nil,
                                contaClienteEnderecoResidencialCidade: nil,
                                contaClienteEnderecoResidencialUF: nil,
                                contaClienteToken: unidasToken?.token,
                                contaClienteAlerta: nil,
                                emailConfirmado: false)
        
        let account = Account(id: nil, user: nil, documentNumber: newCpf, fcm: nil,
                              mobilePhone: userForm.phoneNumber, email: userForm.email,
                              name: userForm.name, companyName: nil, deviceId: nil,
                              profileImage: nil, birthDate: userForm.birthDate,
                              motherName: userForm.motherName)
        
        let userResponse = UserResponse(token: nil, account: account, unidas: [unidas])
        
        return UserResponseDataView(model: userResponse)
    }

}
