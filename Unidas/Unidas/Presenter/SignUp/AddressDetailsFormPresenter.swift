//
//  SignUpAddressPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 14/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol AddressDetailsFormViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func finishCreateAddress()
    func finishUpdateAddress()
}

protocol AddressDetailsFormPresenterDelegate {
    var delegate: AddressDetailsFormViewDelegate? { get set }
    
    func createAddress(addressFormDataView: AddressFormDataView?)
    func updateAddress(addressFormDataView: AddressFormDataView?)
    func addEventSignUpResidentialData(success: Bool)
    func addEventSignUpResidentialContinueLater(success: Bool)
}

class AddressDetailsFormPresenter: AddressDetailsFormPresenterDelegate {
    
    weak var delegate: AddressDetailsFormViewDelegate?
    private var accountAddressesUnidasService: AccountAddressesUnidasService
    private var recoveryService: RecoveryService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var recoveryMethod: AccountRecoveryMethod = .sms
    
    init(accountAddressesUnidasService: AccountAddressesUnidasService,
         recoveryService: RecoveryService,
         firebaseAnalyticsService: FirebaseAnalyticsService,accountRecoveryMethod: AccountRecoveryMethod) {
        self.accountAddressesUnidasService = accountAddressesUnidasService
        self.recoveryService = recoveryService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.recoveryMethod = accountRecoveryMethod
    }
    
    func createAddress(addressFormDataView: AddressFormDataView?) {
        guard let addressForm = addressFormDataView?.model else { return }
        
        delegate?.startLoading()

        AppPersistence.save(key: AppPersistence.postalCode, value: addressForm.zipcode.cep)
        AppPersistence.save(key: AppPersistence.uf, value: addressForm.zipcode.uf)
        AppPersistence.save(key: AppPersistence.localidade, value: addressForm.zipcode.localidade)
        if let bairro = addressForm.zipcode.bairro {
            AppPersistence.save(key: AppPersistence.bairro, value: bairro)
        }
        if let logradouro = addressForm.zipcode.logradouro {
            AppPersistence.save(key: AppPersistence.logradouro, value: logradouro)
        }
        AppPersistence.save(key: AppPersistence.idLocalidade, value: String(addressForm.zipcode.idLocalidade))
        if let idUf = addressForm.zipcode.idUf {
            AppPersistence.save(key: AppPersistence.idUf, value: idUf)
        }
        if let nmLogradouroTipo = addressForm.zipcode.nmLogradouroTipo {
            AppPersistence.save(key: AppPersistence.nmLogradouroTipo, value: nmLogradouroTipo)
        }
        if let nmLogradouroAbreviacao = addressForm.zipcode.nmLogradouroAbreviacao {
            AppPersistence.save(key: AppPersistence.nmLogradouroAbreviacao, value: nmLogradouroAbreviacao)
        }
        AppPersistence.save(key: AppPersistence.number, value: addressForm.number)
        
        if let complement = addressForm.complement {
            AppPersistence.save(key: AppPersistence.complement, value: complement)
        }
        if let phone = addressForm.residencialPhone {
            AppPersistence.save(key: AppPersistence.phone, value: phone)
        }
        
        if let documentNumber = AppPersistence.getValue(withKey: AppPersistence.documentNumber){
            recoveryService.sendSecretCode(documentNumber: documentNumber, method: recoveryMethod, response: (success: {
                }, failure: { error in
                    self.addEventSignUpResidentialData(success: false)
                    self.delegate?.present(error: error)
                    self.delegate?.endLoading()
                }, completion: {
                    self.addEventSignUpResidentialData(success: true)
                    self.delegate?.finishCreateAddress()
                    self.delegate?.endLoading()
            }))
        }
    }
    
    func updateAddress(addressFormDataView: AddressFormDataView?) {
        guard   let currentUser = session.currentUser,
            let addressForm = addressFormDataView?.model  else {return}
        
        delegate?.startLoading()
        
        accountAddressesUnidasService.updateAddress(user: currentUser, addressForm: addressForm, response: (success: { [weak self] _ in
            self?.delegate?.finishUpdateAddress()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        })
        )
    }
    
    func addEventSignUpResidentialData(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_residencial_data"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpResidentialDataStep, withValue: parameters)
    }
    
    func addEventSignUpResidentialContinueLater(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_residencial_data_continue_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpResidencialDataContinueLater, withValue: parameters)
    }
}

