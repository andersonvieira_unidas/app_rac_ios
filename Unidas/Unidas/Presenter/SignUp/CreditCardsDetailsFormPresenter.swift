//
//  SignUpCreditCarPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 18/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

enum CreditCardsDetailsFormError: LocalizedError {
    case allFieldsRequired, cardNotCreated, authorizerIdNotFound
    
    var errorDescription: String? {
        switch self {
        case .allFieldsRequired:
            return NSLocalizedString("All fields required", comment: "All fields required error description")
        case .cardNotCreated:
            return NSLocalizedString("Card not created", comment: "Card not created error description")
        case .authorizerIdNotFound:
            return NSLocalizedString("Brand card not authorized", comment: "Brand card not authorized error description")
        }
    }
}

protocol CreditCardsDetailsFormViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    
    func finishCreateCard()
}

protocol CreditCardsDetailsFormPresenterDelegate {
    var delegate: CreditCardsDetailsFormViewDelegate? { get set }
    
    func create(creditCardFormDataView: CreditCardFormDataView?)
    func addEventSignUpCreditCardLaterStep(success: Bool)
}

class CreditCardsDetailsFormPresenter: CreditCardsDetailsFormPresenterDelegate {
    weak var delegate: CreditCardsDetailsFormViewDelegate?
    private var paymentService: PaymentService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var loginPciService: LoginPciService
    private var creditCardPciService: CreditCardPciService
    
    init(paymentService: PaymentService,
         firebaseAnalyticsService: FirebaseAnalyticsService,
         loginPciService: LoginPciService,
         creditCardPciService: CreditCardPciService) {
        self.paymentService = paymentService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.loginPciService = loginPciService
        self.creditCardPciService = creditCardPciService
    }
    
    func addEventSignUpCreditCardStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_credit_card"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpCreditCard, withValue: parameters)
    }
    
    func addEventAddPaymentInfo(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.addPaymentoInfo, withValue: parameters)
    }
    
    func addEventSignUpCreditCardLaterStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_credit_card_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpCreditCardLater, withValue: parameters)
    }
    
    func create(creditCardFormDataView: CreditCardFormDataView?) {
        guard let creditCardForm = creditCardFormDataView?.model, let user = session.currentUser else {
            self.delegate?.present(error: CreditCardsDetailsFormError.allFieldsRequired)
            self.delegate?.endLoading()
            return
        }
        
        if creditCardForm.authorizerId == nil {
            self.delegate?.present(error: CreditCardsDetailsFormError.authorizerIdNotFound)
            self.delegate?.endLoading()
            return
        }
        
        let addInfo = "holderName:\(creditCardForm.holderName)"
        
        self.delegate?.startLoading()
        
        var userPCI = "APP"
        var tokenPCI = "b660d36b-6b6b-480e-a2a9-3664e1517a55"
        
        #if DEVELOPMENT 
        userPCI = "APP"
        tokenPCI = "792F09F9-50E0-4FFB-8813-7C1FE4B69D1E"
        #endif
    
        loginPciService.login(user: userPCI, token: tokenPCI, response: (success: { [weak self] login in
            self?.callPciCreateCard(user: user, codeStore: "unidaslocadora", creditCardForm: creditCardForm, numberSequenceStore: "1", addInfo: addInfo, token: login.data)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                self?.delegate?.endLoading()
            }, completion: { 
                
        })
        )
    }
    
    private func callPciCreateCard(user: UserResponse, codeStore: String, creditCardForm: CreditCardForm, numberSequenceStore: String, addInfo: String, token: String){
        
        creditCardPciService.createCard(user: user, codeStore: codeStore, creditCardForm: creditCardForm, numberSequenceStore: numberSequenceStore, addInfo: addInfo, token: token, response: (success: { [weak self]  card in
            
            guard let token = card.dados?.card?.token, let nita = card.dados?.store?.nita, let cardNumber = card.dados?.card?.number else {
                let error = NSError(domain: "", code: 500, userInfo: nil)
                self?.delegate?.present(error: error)
                self?.delegate?.endLoading()
                return
            }
            
            self?.createNewCard(user: user, codeStore: codeStore, creditCardForm: creditCardForm, numberSequenceStore: numberSequenceStore, addInfo: addInfo, token: token, nita: nita, cardNumber: cardNumber)
            
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: {
                self.delegate?.endLoading()
        })
        )
    }
    
    private func createNewCard(user: UserResponse, codeStore: String, creditCardForm: CreditCardForm, numberSequenceStore: String, addInfo: String, token: String, nita: String, cardNumber: String){
        paymentService.newCreateCard(user: user, codeStore: "unidaslocadora", creditCardForm: creditCardForm, numberSequenceStore: "1", addInfo:  addInfo, token: token, nita: nita, cardNumber: cardNumber, response: (success: { [weak self] in
            self?.delegate?.finishCreateCard()
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 302 {
                    self?.delegate?.finishCreateCard()
                } else {
                    self?.delegate?.present(error: error)
                    self?.delegate?.endLoading()
                    self?.addEventSignUpCreditCardStep(success: false)
                }
            }, completion: { [weak self] in
                if session.currentUser != nil {
                    self?.addEventAddPaymentInfo(success: true)
                    self?.addEventSignUpCreditCardStep(success: true)
                }else{
                    self?.addEventSignUpCreditCardStep(success: true)
                }
                self?.delegate?.endLoading()
        })
        )
    }
}

