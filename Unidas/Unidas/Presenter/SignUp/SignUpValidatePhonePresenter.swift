//
//  SignUpValidatePhonePresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 13/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import Alamofire

enum SignUpValidateError: LocalizedError {
    case codeRequired
    
    var errorDescription: String? {
        switch self {
        case .codeRequired:
            return NSLocalizedString("Code is required", comment: "Code is required error description")
        }
    }
}

protocol SignUpValidatePhoneViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    
    func startLoadingResendCode()
    func endLoadingResendCode()
    
    func showPin()
    
    func finishUpdatePassword(pinViewController: PINViewController)
    func pinEndLoading(pinViewController: PINViewController)
    func pinPresent(error: Error, pinViewController: PINViewController)
    
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController)
    func didFinishUpdatePin(userResponseDataView: UserResponseDataView?, pinViewController: PINViewController, pin: String)
    func didFailConfirmPin(pinViewController: PINViewController, error: Error)
    
    func resendButtonIsEnabled(_ enabled: Bool)
    func showCountdown(value: String?)
    func finishResendCode()
    func finishValidateAccount()
    func changeSMSBorderColors(value: Bool)
}

protocol SignUpValidatePhonePresenterDelegate {
    var delegate: SignUpValidatePhoneViewDelegate? { get set }
    
    func resendValidate(formAction: FormsAction)
    func validate(at code: String, formAction: FormsAction, isValidate: Bool)
    func updatePassword(pin: String, pinViewController: PINViewController)
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController)
    func disabledResendButton()
    func addEventSignUpSMSConfirmation(success: Bool)
}

class SignUpValidatePhonePresenter: SignUpValidatePhonePresenterDelegate {
    
    weak var delegate: SignUpValidatePhoneViewDelegate?
    private var recoveryService: RecoveryService
    private var codeValidationService: CodeValidationService
    private var accountsService: AccountsService
    private var authUnidasService: AuthUnidasService
    private var countdownTimer: Timer? = nil
    private var dateForResend: Date? = nil
    private var accountAddressesUnidasService: AccountAddressesUnidasService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    var confirmPIN: Bool = false
    var pin: String?
    
    init(recoveryService: RecoveryService, codeValidationService: CodeValidationService,
         accountsService: AccountsService, authUnidasService: AuthUnidasService, firebaseAnalyticsService: FirebaseAnalyticsService, accountAddressUnidasService: AccountAddressesUnidasService) {
        self.recoveryService = recoveryService
        self.codeValidationService = codeValidationService
        self.accountsService = accountsService
        self.authUnidasService = authUnidasService
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.accountAddressesUnidasService = accountAddressUnidasService
    }
    
    func addEventSignUpSMSConfirmation(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_sms_confirmation"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpSMSConfirmationStep, withValue: parameters)
    }
    
    func disabledResendButton() {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: 2, to: Date())
        dateForResend = date
        
        self.delegate?.resendButtonIsEnabled(false)
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        guard let dateForResend = dateForResend else {
            endResendEnabledTimer()
            return
        }
        
        let dateNow = Date()
        self.delegate?.showCountdown(value: timeFormatted(dateForResend: dateForResend, dateNow: dateNow))
        
        if dateNow > dateForResend {
            endResendEnabledTimer()
        }
    }
    
    func resendValidate(formAction: FormsAction) {
        
        var name = ""
        var phoneCodeNumber = ""
        var phoneNumber = ""
        var documentNumber = ""
        var email = ""
        
        
        if formAction == .update {
            guard let currentUser = session.currentUser else { return }
            documentNumber =  currentUser.account.documentNumber
            name = currentUser.account.name
            phoneCodeNumber = currentUser.account.mobilePhone
            //TODO - completar na edicao
        } else {
            name = AppPersistence.getValue(withKey: AppPersistence.name) ?? ""
            let phone = AppPersistence.getValue(withKey: AppPersistence.mobilePhone) ?? ""
            phoneCodeNumber = String(phone.dropLast(9))
            phoneNumber = String(phone.dropFirst(2))
            documentNumber = AppPersistence.getValue(withKey: AppPersistence.documentNumber) ?? ""
            email = AppPersistence.getValue(withKey: AppPersistence.email) ?? ""
        }
        
        self.delegate?.startLoadingResendCode()
        
        recoveryService.resendSecretCode(name: name,
                                         phoneCodeNumber: phoneCodeNumber,
                                         phoneNumber: phoneNumber,
                                         documentNumber: documentNumber,
                                         email: email,response: (success: { [weak self] in
                                            self?.delegate?.endLoadingResendCode()
                                            self?.delegate?.finishResendCode()
                                            }, failure: { [weak self] error in
                                                self?.delegate?.present(error: error)
                                                self?.delegate?.endLoadingResendCode()
                                            }, completion: {
                                                
                                         }))
    }
    
    func validate(at code: String, formAction: FormsAction, isValidate: Bool) {
        self.delegate?.startLoading()
        
        if code.count != 6 {
            self.delegate?.present(error: SignUpValidateError.codeRequired)
            self.delegate?.changeSMSBorderColors(value: true)
            self.delegate?.endLoading()
            return
        }
        
        var documentNumber: String = ""
        
        if formAction == .update {
            guard let currentUser = session.currentUser else { return }
            documentNumber =  currentUser.account.documentNumber
        } else {
            guard let documentNumberPersistence = AppPersistence.getValue(withKey: AppPersistence.documentNumber) else { return }
            documentNumber = documentNumberPersistence
        }
        
        codeValidationService.validate(documentNumber: documentNumber, code: code, response: (success: { [weak self] in
            if isValidate {
                self?.delegate?.finishValidateAccount()
                return
            }
            self?.delegate?.changeSMSBorderColors(value: false)
            self?.addEventSignUpSMSConfirmation(success: true)
            self?.delegate?.showPin()
            
            }, failure: { [weak self] error in
                self?.addEventSignUpSMSConfirmation(success: false)
                self?.delegate?.present(error: error)
                self?.delegate?.changeSMSBorderColors(value: true)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func updatePassword(pin: String, pinViewController: PINViewController) {
        
        guard   let documentNumber = AppPersistence.getValue(withKey: AppPersistence.documentNumber),
            let name = AppPersistence.getValue(withKey: AppPersistence.name),
            let birthday = AppPersistence.getValue(withKey: AppPersistence.birthday),
            let mobilePhone = AppPersistence.getValue(withKey: AppPersistence.mobilePhone),
            let email = AppPersistence.getValue(withKey: AppPersistence.email) else { return }
        
        let motherName = AppPersistence.getValue(withKey: AppPersistence.motherName) ?? ""
        let areaCode = String(mobilePhone.areaCode ?? 0)
        let phone = String(mobilePhone.phoneNumber ?? 0)
        
        authUnidasService.create(documentNumber: documentNumber,
                                 name: name,
                                 motherName: motherName,
                                 birthday: birthday,
                                 areaCode: areaCode,
                                 mobilePhone: phone,
                                 email: email,
                                 pin: pin, response: (success: { [weak self] in
                                    
                                    self?.auth(with: pin, pinViewController: pinViewController)
                                    
                                    }, failure: { [weak self] error in
                                        self?.delegate?.pinPresent(error: error, pinViewController: pinViewController)
                                        self?.delegate?.pinEndLoading(pinViewController: pinViewController)
                                    }, completion: {
                                 }))
    }
    
    func addEventSignUpResidentialData(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_residencial_data"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpResidentialDataStep, withValue: parameters)
    }
    
    func addEventSignUpResidentialContinueLater(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_residencial_data_continue_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpResidencialDataContinueLater, withValue: parameters)
    }
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        if !confirmPIN {
            confirmPIN = true
            self.pin = pin
            delegate?.showNewPinController(title: NSLocalizedString("Confirm your PIN", comment: ""),
                                           isHiddenForgotPin: true,
                                           isHiddenCancelButton: true,
                                           userResponseDataView: userResponseDataView,
                                           pinViewController: pinViewController)
            return
        }
        else if confirmPIN {
            if self.pin == pin {
                delegate?.didFinishUpdatePin(userResponseDataView: userResponseDataView,
                                             pinViewController: pinViewController,
                                             pin: pin)
                return
            }
            
            confirmPIN = false
            delegate?.didFailConfirmPin(pinViewController: pinViewController, error: PINError.confirmPin)
            
            return
        }
    }
    
    private func auth(with pin: String, pinViewController: PINViewController) {
        
        guard let documentNumber = AppPersistence.getValue(withKey: AppPersistence.documentNumber) else { return }
        
        authUnidasService.auth(documentNumber: documentNumber, pin: pin, response: (success: { [weak self] userUnidas in
            
            guard   let userUnidas = [userUnidas].first,
                let account = Account(userUnidas: userUnidas) else { return }
            
            let userResponse = UserResponse(token: userUnidas.contaClienteToken, account: account, unidas: [userUnidas])
            session.currentUser = userResponse
            
            if let accessToken = userUnidas.contaClienteToken {
                let headers = ["Authorization" : "Bearer \(accessToken)"]
                let httpHeaders = HTTPHeaders(headers)
                ServiceConfiguration.unidas.headers = httpHeaders
            }
            
            self?.createAddress()
            
            NotificationCenter.default.post(name: .UNUserDidLogin, object: userResponse)
            self?.delegate?.finishUpdatePassword(pinViewController: pinViewController)
            
            }, failure: { [weak self] error in
                self?.delegate?.pinPresent(error: error, pinViewController: pinViewController)
            }, completion: { [weak self] in
                self?.delegate?.pinEndLoading(pinViewController: pinViewController)
        }))
    }
    
    private func timeFormatted(dateForResend: Date, dateNow: Date) -> String? {
        let timeInterval = dateForResend.timeIntervalSince(dateNow)
        
        let interval = Int(timeInterval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    private func endResendEnabledTimer() {
        countdownTimer?.invalidate()
        countdownTimer = nil
        dateForResend = nil
        self.delegate?.resendButtonIsEnabled(true)
        self.delegate?.showCountdown(value: nil)
    }
    
    private func createAddress(){
        
        guard let idLocalization = AppPersistence.getValue(withKey: AppPersistence.idLocalidade),
            let street = AppPersistence.getValue(withKey: AppPersistence.logradouro),
            let neighborhood = AppPersistence.getValue(withKey: AppPersistence.bairro),
            let streetAbbreviation = AppPersistence.getValue(withKey: AppPersistence.nmLogradouroAbreviacao),
            let state = AppPersistence.getValue(withKey: AppPersistence.uf),
            let localization = AppPersistence.getValue(withKey: AppPersistence.localidade),
            let zipcode = AppPersistence.getValue(withKey: AppPersistence.postalCode),
            let streetType = AppPersistence.getValue(withKey: AppPersistence.nmLogradouroTipo) else { return }
        
        let idLocalizationInt = Int(idLocalization) ?? 0
        
        let zipCode = Zipcode(idLocalidade: idLocalizationInt, idUf: 0, logradouro: street, bairro: neighborhood, nmLogradouroTipo: streetType, nmLogradouroAbreviacao: streetAbbreviation, uf: state, localidade: localization, cep: zipcode, logradouroTipo: streetType)
        
        let addressForm = AddressForm(neighborhood: AppPersistence.getValue(withKey: "bairro") ?? "", street: AppPersistence.getValue(withKey: "logradouro") ?? "", number: AppPersistence.getValue(withKey: "number") ?? "", complement: AppPersistence.getValue(withKey: "complement") ?? "", residencialPhone: AppPersistence.getValue(withKey: "phone") ?? "", zipcode: zipCode)
        
        guard let currentUser = session.currentUser else { return }
        
        self.accountAddressesUnidasService.updateAddress(user: currentUser, addressForm: addressForm, response: (success: { _ in
            
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                self?.addEventSignUpResidentialData(success: false)
            }, completion: { [weak self] in
                self?.addEventSignUpResidentialData(success: true)
                self?.delegate?.endLoading()
        })
        )
    }
}
