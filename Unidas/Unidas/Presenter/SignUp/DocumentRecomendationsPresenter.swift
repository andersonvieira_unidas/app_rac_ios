//
//  SignUpDocumentInformationPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 20/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol DocumentRecomendationsViewDelegate: class {
    func startLoadingCamera()
    func endLoadingCamera()
    func presentCamera(error: Error)
    func finishAddDocument()
}

protocol DocumentRecomendationsPresenterDelegate {
    var delegate: DocumentRecomendationsViewDelegate? { get set }
    
    func addDocument(image: Data?, formAction: FormsAction)
    func addEventSignUpDriverLicenseLaterStep(success: Bool)
}

class DocumentRecomendationsPresenter: DocumentRecomendationsPresenterDelegate {
    weak var delegate: DocumentRecomendationsViewDelegate?
    private var accountPhotoUnidasService: AccountPhotoUnidasService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(accountPhotoUnidasService: AccountPhotoUnidasService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.accountPhotoUnidasService = accountPhotoUnidasService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func addEventSignUpDriverLicenseStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_driver_license"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpDriverLicense, withValue: parameters)
    }
    
    func addEventDocumentOcrError(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters[AnalyticsEvents.documentOcrError.name] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.documentOcrError, withValue: parameters)
    }
    
    func addEventSignUpDriverLicenseLaterStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_driver_license_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpDriverLicenseLater, withValue: parameters)
    }
    
    func addDocument(image: Data?, formAction: FormsAction) {
        guard let currentUser = session.currentUser else { return }

        self.delegate?.startLoadingCamera()
        
        guard let imageBase64 = image?.base64EncodedString() else { return }
        let imageType = ImageType.document
        
        accountPhotoUnidasService.saveDocument(user: currentUser, userImage: imageBase64, imageType: imageType, response: (success: { [weak self] in
    
            self?.delegate?.finishAddDocument()
            }, failure: { [weak self] error in
                self?.delegate?.presentCamera(error: error)
                self?.addEventSignUpDriverLicenseStep(success: false)
                self?.addEventDocumentOcrError(success: false)
            }, completion: { [weak self] in
                if formAction == .create {
                    self?.addEventSignUpDriverLicenseStep(success: true)
                }
                self?.delegate?.endLoadingCamera()
            })
        )
    }
}

