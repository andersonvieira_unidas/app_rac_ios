//
//  SignUpSignatureInformationPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum SignatureRecomendationsError: LocalizedError {
    case termsAndConditionsNotAccepted
    case termsPrivacyPolicyNotAccepted
    
    var errorDescription: String? {
        switch self {
        case .termsAndConditionsNotAccepted:
            return NSLocalizedString("In order to continue you must accept the terms and conditions", comment: "Terms and conditions not accepted error description")
        case .termsPrivacyPolicyNotAccepted:
            return NSLocalizedString("In order to continue you must accept the privacy policy", comment: "Privacy policy not accepted error description")
        }
    }
}

protocol SignatureRecomendationsViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func finishUploadSignature(viewController: CustomSignatureViewController)
    func finishAcceptTerms()
    func customSignatureStartLoading(viewController: CustomSignatureViewController)
    func customSignatureEndLoading(viewController: CustomSignatureViewController)
    func customSignaturePresent(error: Error, viewController: CustomSignatureViewController)
    func showTerms(openingPolicy: CompanyTermsDataView, termsAndConditionsOfUse: CompanyTermsDataView)
    func showPrivacyPolicyTerms()
}

protocol SignatureRecomendationsPresenterDelegate {
    var delegate: SignatureRecomendationsViewDelegate? { get set }
    
    func finishAcceptTerms(terms: Bool, terms2: Bool)
    func addSignature(image: Data?, viewController: CustomSignatureViewController)
    func fetchTerms()
    func addEventSignUpSignatureLaterStep(success: Bool)
}

class SignatureRecomendationsPresenter: SignatureRecomendationsPresenterDelegate {
    weak var delegate: SignatureRecomendationsViewDelegate?
    private var accountPhotoUnidasService: AccountPhotoUnidasService
    private var companyTermsService: CompanyTermsService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    init(accountPhotoUnidasService: AccountPhotoUnidasService, companyTermsService: CompanyTermsService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.accountPhotoUnidasService = accountPhotoUnidasService
        self.companyTermsService = companyTermsService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func fetchTerms() {
        companyTermsService.fetchTerms(termsCompanyType: .accountOpeningPolicy, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            self?.fetchTermsAndConditionsOfUse(openingPolicy: CompanyTermsDataView(model: companyTerms))
            
            }, failure: { error in
                
        }, completion: {
            
        })
        )
    }
    
    private func fetchTermsAndConditionsOfUse(openingPolicy: CompanyTermsDataView) {
        companyTermsService.fetchTerms(termsCompanyType: .termsAndConditionsOfUse, response: (success: { [weak self] companyTerms in
            guard let companyTerms = companyTerms.first else { return }
            self?.delegate?.showTerms(openingPolicy: openingPolicy, termsAndConditionsOfUse: CompanyTermsDataView(model: companyTerms))
            self?.delegate?.showPrivacyPolicyTerms()
            }, failure: { error in
                
        }, completion: {
            
        })
        )
    }
    
    func addEventSignUpSignatureStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_signature"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpSignature, withValue: parameters)
    }
    
    func addEventSignUpSignatureLaterStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_signature_later"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpSignatureLater, withValue: parameters)
    }
    
    func addSignature(image: Data?, viewController: CustomSignatureViewController) {
        guard let currentUser = session.currentUser else { return }
        
        self.delegate?.customSignatureStartLoading(viewController: viewController)
        
        guard let imageBase64 = image?.base64EncodedString() else { return }
        let imageType = ImageType.signature
        
        accountPhotoUnidasService.saveDocument(user: currentUser, userImage: imageBase64, imageType: imageType, response: (success: { [weak self] in
            self?.delegate?.finishUploadSignature(viewController: viewController)
            }, failure: { [weak self] error in
                self?.delegate?.customSignaturePresent(error: error, viewController: viewController)
                self?.addEventSignUpSignatureStep(success: false)
            }, completion: { [weak self] in
                self?.addEventSignUpSignatureStep(success: true)
                self?.delegate?.customSignatureEndLoading(viewController: viewController)
        })
        )
    }
    
    func sendTermsToServer(privacyTerm: Bool) {
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        delegate?.startLoading()
        
        accountPhotoUnidasService.updateTerms(documentNumber: documentNumber, privacyTerm: privacyTerm, response: (success: { [weak self] in
            self?.delegate?.endLoading()
            self?.delegate?.finishAcceptTerms()
            }, failure: { error in
                self.delegate?.endLoading()
                self.delegate?.present(error: error)
        }, completion: {
            print("success completion")
        }))
    }
    
    func finishAcceptTerms(terms: Bool, terms2: Bool) {
        if terms == false {
            self.delegate?.present(error: SignatureRecomendationsError.termsAndConditionsNotAccepted)
        }else if terms2 == false {
            self.delegate?.present(error: SignatureRecomendationsError.termsPrivacyPolicyNotAccepted)
        }else{
            sendTermsToServer(privacyTerm: true)
        }
    }
}
