//
//  DriverLicensePresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

protocol DriverLicenseFormViewDelegate: class {
    
    var documentNumber: String? { get }
    var entity: String? { get }
    var state: String? { get }
    var registrationNumber: String? { get }
    var validity: Date? { get }
    var firstDocument: Date? { get }
    var fatherName: String? { get }
    var motherName: String? { get }
    func setFirstDocumentDateTextField(text: String)
    func setValidityDateTextField(text: String)
    func present(error: Error)
    
    func documentNumberError(isOn: Bool)
    func entityError(isOn: Bool)
    func stateError(isOn: Bool)
    func registrationNumber(isOn: Bool)
    func validityError(isOn: Bool)
    func firstLicenseError(isOn: Bool)
    func fatherNameError(isOn: Bool)
    func motherNameError(isOn: Bool)
}

protocol DriverLicenseFormPresenterDelegate {
    var delegate: DriverLicenseFormViewDelegate? { get set }
    func didSetFirstDocumentDate(to date: Date)
    func didSetValidityDate(to date: Date)
    var driverLicenseFormDataView: DriverLicenseFormDataView? { get }
}

class DriverLicenseFormPresenter: DriverLicenseFormPresenterDelegate {
    
    weak var delegate: DriverLicenseFormViewDelegate?
    
    var isDriverLicenseValid: Bool {
        let charset = CharacterSet(charactersIn: " ")
        var existsError = false
        
        guard let documentNumber = delegate?.documentNumber?.trimmingCharacters(in: charset),
            let entity = delegate?.entity?.trimmingCharacters(in: charset),
            let state = delegate?.state?.trimmingCharacters(in: charset),
            let registrationNumber = delegate?.registrationNumber?.trimmingCharacters(in: charset),
            let fatherName = delegate?.fatherName,
            let motherName = delegate?.motherName else {
                return false
        }
        
        if documentNumber.isEmpty  {
            existsError = true
            self.delegate?.documentNumberError(isOn: true)
        } else {
            self.delegate?.documentNumberError(isOn: false)
        }
        
        if entity.isEmpty {
            existsError = true
            self.delegate?.entityError(isOn: true)
        } else {
            self.delegate?.entityError(isOn: false)
        }
        
        if state.isEmpty {
            existsError = true
            self.delegate?.stateError(isOn: true)
        } else {
            self.delegate?.stateError(isOn: false)
        }
        
        if registrationNumber.isEmpty {
            existsError = true
            self.delegate?.registrationNumber(isOn: true)
        }else {
            self.delegate?.registrationNumber(isOn: false)
        }
        
        if let _ = delegate?.validity {
            self.delegate?.validityError(isOn: false)
            
        } else {
            existsError = true
            self.delegate?.validityError(isOn: true)
        }
        
        if let _ = delegate?.firstDocument {
            self.delegate?.firstLicenseError(isOn: false)
        } else {
            existsError = true
            self.delegate?.firstLicenseError(isOn: true)
        }
        
        if fatherName.isEmpty {
            existsError = true
            self.delegate?.fatherNameError(isOn: true)
        } else {
            self.delegate?.fatherNameError(isOn: false)
        }
        
        if motherName.isEmpty {
            existsError = true
            self.delegate?.motherNameError(isOn: true)
        } else {
            self.delegate?.motherNameError(isOn: false)
        }
        
        if existsError {
            return false
        }
        return true
    }
    
    var driverLicenseFormDataView: DriverLicenseFormDataView? {
        guard isDriverLicenseValid else { return nil }
        let charset = CharacterSet(charactersIn: " ")
        
        guard let documentNumber = delegate?.documentNumber?.trimmingCharacters(in: charset),
            let entity = delegate?.entity?.trimmingCharacters(in: charset),
            let state = delegate?.state?.trimmingCharacters(in: charset),
            let registrationNumber = delegate?.registrationNumber?.trimmingCharacters(in: charset),
            let fatherName = delegate?.fatherName,
            let motherName = delegate?.motherName,
            let validity = delegate?.validity,
            let firstDriverLicense = delegate?.firstDocument else {
                return nil
        }
        
        return DriverLicenseFormDataView(model: DriverLicenseForm(registrationId: documentNumber, issuingBody: entity, state: state, driverLicenseNumber: registrationNumber, validThru: validity, firstDriverLicense: firstDriverLicense, fatherName: fatherName, motherName: motherName))
    }
    
    func didSetValidityDate(to date: Date) {
        let dateString = DateFormatter.dateOnlyFormatter.string(from: date)
        delegate?.setValidityDateTextField(text: dateString)
    }
    
    func didSetFirstDocumentDate(to date: Date) {
        let dateString = DateFormatter.dateOnlyFormatter.string(from: date)
        delegate?.setFirstDocumentDateTextField(text: dateString)
    }
    
}
