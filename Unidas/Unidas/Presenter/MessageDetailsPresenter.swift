//
//  MessageDetailsPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 9/11/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol MessageDetailsViewDelegate: class {
    
    func setup(for message: MessageDataView)
}

protocol MessageDetailsPresenterDelegate {
    
    var delegate: MessageDetailsViewDelegate? { get set }
    
    func viewDidLoad(message: MessageDataView, shouldMarkAsRead: Bool)
    func markMessageAsRead()
}

class MessageDetailsPresenter: MessageDetailsPresenterDelegate {
    
    weak var delegate: MessageDetailsViewDelegate?
    
    var messageService: MessageService
    
    var message: Message!
    
    init(messageService: MessageService) {
        self.messageService = messageService
    }
    
    func viewDidLoad(message: MessageDataView, shouldMarkAsRead: Bool) {
        self.message = message.model
        delegate?.setup(for: message)
        if shouldMarkAsRead {
            markMessageAsRead()
        }
    }
    
    func markMessageAsRead() {
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        messageService.unreadMessage(documentNumber: documentNumber, message: message, response: (success: {
            NotificationCenter.default.post(name: .UNDidUnreadMessage, object: nil)
        }, failure: { _ in }, completion: { }))
    }
    
}
