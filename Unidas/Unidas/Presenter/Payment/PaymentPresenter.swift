//
//  PaymentPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 07/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum PaymentError: LocalizedError {
    case reserveNotFound, notAcceptAgreements, preAuthorizationNotAuthorized, paymentNotAuthorized, bothPaymentsWereNotAuthorized, userUnderValidation
    
    var errorDescription: String? {
        switch self {
        case .reserveNotFound:
            return NSLocalizedString("Reserve not found", comment: "Reserve not found error message")
        case .notAcceptAgreements:
            return NSLocalizedString("You need accept agreements", comment: "Need accept agreements message")
        case .preAuthorizationNotAuthorized:
            return NSLocalizedString("Pre authorization payment not authorized", comment: "Pre payment not authorized")
        case .paymentNotAuthorized:
            return NSLocalizedString("Reservation payment not authorized", comment: "Reservation payment not authorized")
        case .bothPaymentsWereNotAuthorized:
            return NSLocalizedString("Pre authorization and reservation payment were not authorized", comment: "Both payment not authorized")
        case .userUnderValidation:
            return NSLocalizedString("User under validation", comment: "User under validation error")
        }
    }
}

enum PaymentCardSelection: Int {
    case reserve, preAuthorization
}

protocol PaymentViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func present(withDismiss: Bool, error: Error)
    func present(message: String)
    
    func startLoadingPreAuthorizationValue()
    func endLoadingPreAuthorizationValue()
    
    func performCard()
    func showCards()
    func emptyCard()
    func showCreateCard()
    func adjustConstraints()
    
    func updateReserveCardInformation(listCardDataView: ListCardDataView)
    func updatePreAuthorizationCardInformation(listCardDataView: ListCardDataView)
    func setupView(listCardDataView: ListCardDataView)
    func showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView)
    func isHiddenPaymentView(_ isHidden: Bool)
    
    func finishPayment(preAuthorizationCardDataView: ListCardDataView?, reservationCardDataView: ListCardDataView?, vehicleReservationsDataView: VehicleReservationsDataView, openContractDataView: OpenContractDataView, preAuthorizationDataView: PreAuthorizationEstimateDataView?, installments: Int?)
    
    func setupPassStepPaymentCheckinView()
}

protocol PaymentPresenterDelegate {
    var delegate: PaymentViewDelegate? { get set }
    var isReservePayment: Bool { get }
    var isPassStepPaymentCheckin: Bool { get set }
    
    func viewDidLoad()
    func performFetchCard(createdNew : Bool?)
    func changeCard(paymentCardSelection: PaymentCardSelection?)
    func updateCardInformation(listCardDataView: ListCardDataView)
    
    func performPayment(reserveNumber: String, paymentCard: CreditCardReservePaymentDataView?, prePayment: CreditCardPrePaymentDataView?, agreements: Bool)
    func performLegalEntityPayment(vehicleReservations: VehicleReservationsDataView, paymentCard: CreditCardReservePaymentDataView?, prePayment: CreditCardPrePaymentDataView?, agreements: Bool)
    func addEventEcommercePurchase(success: Bool)
    
}

class PaymentPresenter: PaymentPresenterDelegate {
    
    weak var delegate: PaymentViewDelegate?
    private var paymentService: PaymentService
    private var contractService: ContractService
    private var vehicleReservations: VehicleReservations
    private var paymentCardSelection: PaymentCardSelection?
    private var preCard: ListCard?
    private var reserveCard: ListCard?
    private var finishedReservePayment: Bool = false
    private var preAuthorization: PreAuthorizationEstimate?
    private var installments: Int?
    internal var isPassStepPaymentCheckin: Bool = false
    
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    
    var isReservePayment: Bool {
        return finishedReservePayment
    }
    
    init(paymentService: PaymentService, vehicleReservationsDataView: VehicleReservationsDataView, contractService: ContractService, firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.paymentService = paymentService
        self.vehicleReservations = vehicleReservationsDataView.model
        self.contractService = contractService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func viewDidLoad() {
        
        let vehicleReservationsDataView = VehicleReservationsDataView(model: vehicleReservations)
        
        if vehicleReservationsDataView.isPassStepPaymentCheckin {
            isPassStepPaymentCheckin = true
            self.delegate?.setupPassStepPaymentCheckinView()
            performLegalEntityPayment(vehicleReservations: vehicleReservationsDataView, paymentCard: nil, prePayment: nil, agreements: true)
        }
        self.performFetchCard(createdNew: nil)
        self.performPreAuthorization()
        
        if vehicleReservations.numeroCartao != nil {
            finishedReservePayment = true
            self.delegate?.isHiddenPaymentView(true)
        }
    }
    
    func addEventEcommercePurchase(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.ecommercePurchase, withValue: parameters)
    }
    
    func performFetchCard(createdNew: Bool?) {
        guard let user = session.currentUser else { return }
        
        self.delegate?.startLoading()
        
        paymentService.listCard(user: user, response: (success: { [weak self] cards in
            guard let firstCard = cards.filter({$0.metodoPagamentoCartaoCreditoPreferencial == 1}).first ?? cards.first else { return }
            
            self?.preCard = firstCard
            self?.reserveCard = firstCard
            self?.delegate?.setupView(listCardDataView: ListCardDataView(model: firstCard))
            self?.delegate?.showCards()
            self?.delegate?.endLoading()
            
            if let _ = createdNew {
                self?.delegate?.adjustConstraints()
            }
            
            }, failure: { [weak self] error in
                self?.delegate?.endLoading()
                self?.delegate?.emptyCard()
            }, completion: {
                
        })
        )
    }
    
    func changeCard(paymentCardSelection: PaymentCardSelection?) {
        self.paymentCardSelection = paymentCardSelection
        
        (preCard == nil) ? delegate?.showCreateCard() : delegate?.performCard()
    }
    
    func updateCardInformation(listCardDataView: ListCardDataView) {
        if let paymentCardSelection = paymentCardSelection {
            switch paymentCardSelection {
            case .reserve:
                reserveCard = listCardDataView.model
                delegate?.updateReserveCardInformation(listCardDataView: listCardDataView)
                break
            case .preAuthorization:
                preCard = listCardDataView.model
                delegate?.updatePreAuthorizationCardInformation(listCardDataView: listCardDataView)
                break
            }
        }
    }
    
    func performPayment(reserveNumber: String, paymentCard: CreditCardReservePaymentDataView?, prePayment: CreditCardPrePaymentDataView?, agreements: Bool) {
        
        if checkInformationRequired(reserveNumber: reserveNumber, agreements: agreements) {
            if finishedReservePayment {
                self.performPaymentOnlyPrePayment(prePayment: prePayment, reserveNumber: reserveNumber, agreements: agreements)
                return
            }
            
            guard let paymentCard = paymentCard, let prePayment = prePayment else { return }
            
            self.delegate?.startLoading()
            
            self.contractService.generateContract(reserveNumber: reserveNumber, paymentCard: paymentCard.model, prePaymentCard: prePayment.model, preInstallments: 1, agreements: agreements, response: (success: { [weak self] contractResp in
                
                guard let contractResponse = contractResp.contract, let preAuthorization = self?.preAuthorization,  let vehicleReservations = self?.vehicleReservations else { return }
                
                self?.delegate?.finishPayment(preAuthorizationCardDataView: ListCardDataView(model: prePayment.model.card),
                                              reservationCardDataView: ListCardDataView(model: paymentCard.model.card),
                                              vehicleReservationsDataView: VehicleReservationsDataView(model: vehicleReservations),
                                              openContractDataView: OpenContractDataView(model: contractResponse),
                                              preAuthorizationDataView: PreAuthorizationEstimateDataView(model: preAuthorization),
                                              installments: paymentCard.model.installmentsOption)
                self?.addEventEcommercePurchase(success: true)
                }, failure: { [weak self] error in
                    self?.delegate?.present(error: error)
                }, completion: { [weak self] in
                    self?.delegate?.endLoading()
            })
            )
        } 
    }
    
    func performLegalEntityPayment(vehicleReservations: VehicleReservationsDataView, paymentCard: CreditCardReservePaymentDataView?, prePayment: CreditCardPrePaymentDataView?, agreements: Bool) {
        
        let voucherType = vehicleReservations.voucher ?? ""
        
        // pass checkin
        if isPassStepPaymentCheckin {
            generateContractLegalEntity(reserveNumber: vehicleReservations.reserveNumber, voucherType: voucherType, paymentCard: paymentCard, prePayment: prePayment, agreements: agreements)
            return
        }
        
        let informationRequired = checkInformationRequired(reserveNumber: vehicleReservations.reserveNumber, agreements: agreements)
        
        //payment e pre payment require
        if vehicleReservations.requireAllPayments {
            if !vehicleReservations.isPaid, informationRequired, let paymentCard = paymentCard, let prePayment = prePayment {
                generateContractLegalEntity(reserveNumber: vehicleReservations.reserveNumber, voucherType: voucherType, paymentCard: paymentCard, prePayment: prePayment, agreements: agreements)
            } else if vehicleReservations.isPaid, informationRequired, let prePayment = prePayment {
                generateContractLegalEntity(reserveNumber: vehicleReservations.reserveNumber, voucherType: voucherType, paymentCard: paymentCard, prePayment: prePayment, agreements: agreements)
            }
            //payment only
        } else if vehicleReservations.requiresPaymentOnly && !vehicleReservations.isPaid {
            if informationRequired, let paymentCard = paymentCard {
                generateContractLegalEntity(reserveNumber: vehicleReservations.reserveNumber, voucherType: voucherType, paymentCard: paymentCard, prePayment: prePayment, agreements: agreements)
            }
            //prepayment only
        } else if vehicleReservations.requiresPrePaymentOnly {
            if informationRequired, let prePayment = prePayment {
                generateContractLegalEntity(reserveNumber: vehicleReservations.reserveNumber, voucherType: voucherType, paymentCard: paymentCard, prePayment: prePayment, agreements: agreements)
            }
        }
    }
    
    private func generateContractLegalEntity(reserveNumber: String, voucherType: String, paymentCard: CreditCardReservePaymentDataView?, prePayment: CreditCardPrePaymentDataView?, agreements: Bool) {
        
        self.delegate?.startLoading()
        
        self.contractService.generateContractLegalEntity(reserveNumber: reserveNumber, voucherType: voucherType, paymentCard: paymentCard?.model, prePaymentCard: prePayment?.model, preInstallments: 1, agreements: agreements, response: (success: { [weak self] contractResp in
            
            guard let contractResponse = contractResp.contract, let vehicleReservations = self?.vehicleReservations else { return }
            
            if contractResp.statusCode == 200 {
                
                var preAuthorizationEstimateDataView: PreAuthorizationEstimateDataView? = nil
              
                let reservationCardDataView = paymentCard?.model.card != nil ? ListCardDataView(model: paymentCard!.model.card) : nil
                let preAuthorizationCardView = prePayment?.model.card != nil ? ListCardDataView(model: prePayment!.model.card) : nil
        
                if let preAuthorization = self?.preAuthorization {
                    preAuthorizationEstimateDataView = PreAuthorizationEstimateDataView(model: preAuthorization)
                }
                self?.delegate?.finishPayment(preAuthorizationCardDataView: preAuthorizationCardView, reservationCardDataView: reservationCardDataView, vehicleReservationsDataView: VehicleReservationsDataView(model: vehicleReservations), openContractDataView: OpenContractDataView(model: contractResponse), preAuthorizationDataView: preAuthorizationEstimateDataView, installments: paymentCard?.model.installmentsOption)
            }
            
            }, failure: { [weak self] error in
                if self?.isPassStepPaymentCheckin ?? false {
                    self?.delegate?.present(withDismiss: true, error: error)
                    return
                }
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        })
        )
    }
    
    func performPaymentOnlyPrePayment(prePayment: CreditCardPrePaymentDataView?, reserveNumber: String, agreements: Bool) {
        guard let prePayment = prePayment else { return }
        
        self.delegate?.startLoading()
        
        self.contractService.generateContract(reserveNumber: reserveNumber, paymentCard: nil, prePaymentCard: prePayment.model, preInstallments: 1, agreements: agreements, response: (success: { [weak self] contractResp in
            guard let contractResponse = contractResp.contract else { return }
            
            guard let preAuthorization = self?.preAuthorization, let vehicleReservations = self?.vehicleReservations, contractResponse.preAuthorizationFinished else {
                if !contractResponse.preAuthorizationFinished {
                    self?.delegate?.present(error: PaymentError.preAuthorizationNotAuthorized)
                }
                return
            }
            
            self?.delegate?.finishPayment(preAuthorizationCardDataView: ListCardDataView(model: prePayment.model.card),
                                          reservationCardDataView: nil,
                                          vehicleReservationsDataView: VehicleReservationsDataView(model: vehicleReservations),
                                          openContractDataView: OpenContractDataView(model: contractResponse),
                                          preAuthorizationDataView: PreAuthorizationEstimateDataView(model: preAuthorization),
                                          installments: self?.installments)
            
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        })
        )
    }
    
    private func checkInformationRequired(reserveNumber: String, agreements: Bool) -> Bool {
        if reserveNumber.isEmpty {
            self.delegate?.present(error: PaymentError.reserveNotFound)
            return false
        }
        
        if (!agreements) {
            self.delegate?.present(error: PaymentError.notAcceptAgreements)
            return false
        }
        
        return true
    }
    
    // MARK: - Private Functions
    
    private func performPreAuthorization() {
        self.delegate?.startLoadingPreAuthorizationValue()
        let protections = vehicleReservations.servico?.filter { $0.tipo == "P" }.compactMap { $0.codigoOTA } ?? []
        paymentService.fetchPreAuthorizationValue(for: protections, groupCode: vehicleReservations.grupo, rateQualifier: vehicleReservations.tarifa, response: (success: { [weak self] preAuthorizationEstimate in
            self?.preAuthorization = preAuthorizationEstimate
            self?.delegate?.showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView(model: preAuthorizationEstimate))
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoadingPreAuthorizationValue()
        }))
    }
    
    private var reservationCardDataView: ListCardDataView? {
        guard let listCard = reserveCard else { return nil }
        
        return ListCardDataView(model: listCard)
    }
}
