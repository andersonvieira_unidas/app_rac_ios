//
//  MyAccountHeaderPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol MyAccountHeaderViewDelegate: class {
    func showCurrentUserImage(imageUrl: URL?)
}

protocol MyAccountHeaderPresenterDelegate {
    var delegate: MyAccountHeaderViewDelegate? { get set }
    
}

class MyAccountHeaderPresenter: MyAccountHeaderPresenterDelegate {
    weak var delegate: MyAccountHeaderViewDelegate?
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeImage(_ :)), name: .UNDidChangeProfileImage, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didChangeImage(_ notification: Notification) {
        self.delegate?.showCurrentUserImage(imageUrl: notification.object as? URL)
    }
}
