//
//  ConfigurationPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol ConfigurationViewDelegate: class {
    func performSegue(identifier: String)
    func reloadData()
}

protocol ConfigurationPresenterDelegate {
    var delegate: ConfigurationViewDelegate? { get set }
    var configurationItems: [ConfigurationItem] { get }
    func configurationItem(at: Int) -> ConfigurationItemDataView
    func to(segueIdentifier: String)
    func viewDidLoad()
}

class ConfigurationPresenter: ConfigurationPresenterDelegate {
    weak var delegate: ConfigurationViewDelegate?
    var configurationItems: [ConfigurationItem] = []

    init() {
    }
    
    func viewDidLoad() {
        insertItems()
    }
 
    func to(segueIdentifier: String) {
        delegate?.performSegue(identifier: segueIdentifier)
    }
   
    private func insertItems(){
        let notifications = ConfigurationItem(image: "icon-config-notification", localizedString: "Configuration Menu Item Notification", action: "showNotifications")
        configurationItems.append(notifications)
        
        delegate?.reloadData()
    }
    
    func configurationItem(at: Int) -> ConfigurationItemDataView {
       return ConfigurationItemDataView(model: configurationItems[at])
    }
}
