//
//  NotificationConfigurationPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UserNotifications

protocol NotificationConfigurationViewDelegate: class {
    func reloadData()
    func startLoading()
    func endLoading()
    func present(error: Error)
    func openNotificationSettings()
}

protocol NotificationConfigurationPresenterDelegate {
    var delegate: NotificationConfigurationViewDelegate? { get set }
    var numberOfNotifications: Int { get }
    func update(id: Int, active: Bool)
    func notificationConfiguration(at: IndexPath) -> NotificationConfiguration
    func viewDidLoad()
}

class NotificationConfigurationPresenter: NotificationConfigurationPresenterDelegate {
    
    weak var delegate: NotificationConfigurationViewDelegate?
    private var notificationConfigurationService: NotificationConfigurationService
    
    var notificationsConfiguration: [NotificationConfiguration] = []
    var isPermittedNotifications: Bool = false
    
    var numberOfNotifications: Int {
        return notificationsConfiguration.count
    }
    
    init(notificationConfigurationService: NotificationConfigurationService) {
        self.notificationConfigurationService = notificationConfigurationService
    }
    
    func viewDidLoad() {
        fetchNotifications()
    }
    
    func notificationConfiguration(at: IndexPath) -> NotificationConfiguration {
        return notificationsConfiguration[at.row]
    }
    
    func update(id: Int, active: Bool) {
        delegate?.startLoading()
        checkNotificationNotPermitted()
 
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        notificationConfigurationService.update(documentNumber: documentNumber, id: id, active: active, response: (success: {
            
            if active, !self.isPermittedNotifications {
                self.delegate?.openNotificationSettings()
            }
        }, failure: { error in
            self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }
    
    private func fetchNotifications(){
        delegate?.startLoading()
        
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        notificationConfigurationService.fetch(documentNumber: documentNumber, response: (success: { [weak self] notifications in
            guard let items = notifications else { return }
            self?.notificationsConfiguration = items
            self?.delegate?.reloadData()
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }
    
    private func checkNotificationNotPermitted(){
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { settings in
            switch settings.authorizationStatus {
            case .notDetermined, .denied :
                self.isPermittedNotifications = false
                break
            case .authorized, .provisional, . ephemeral:
                self.isPermittedNotifications = true
                break
            @unknown default:
                self.isPermittedNotifications = false
                break
            }
        })
    }
}
