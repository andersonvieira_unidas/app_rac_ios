//
//  UserFormPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 08/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit

enum UserFormError: LocalizedError {
    case invalidDocument
    case documentIsRequired
    case nameIsRequired
    case emailIsRequired
    case invalidEmail
    case phoneNumberIsRequired
    case invalidPhoneNumber
    case invalidBirthdayDate
    case birthdayRequired
    
    var errorDescription: String? {
        switch self {
        case .invalidDocument:
            return NSLocalizedString("Invalid document", comment: "Invalid document error description")
        case .documentIsRequired:
            return NSLocalizedString("Required document", comment: "Required document error description")
        case .nameIsRequired:
            return NSLocalizedString("Name is required", comment: "Name is required error description")
        case .emailIsRequired:
            return NSLocalizedString("Required email", comment: "Required email error description")
        case .invalidEmail:
            return NSLocalizedString("Invalid email", comment: "Invalid email error description")
        case .phoneNumberIsRequired:
            return NSLocalizedString("Phone number is required", comment: "Required phone number error description")
        case .invalidPhoneNumber:
            return NSLocalizedString("Invalid phone number", comment: "Invalid phone number error description")
        case .birthdayRequired:
            return NSLocalizedString("Birthday is required", comment: "Birthday is required error description")
        case .invalidBirthdayDate:
            return NSLocalizedString("Invalid birthday date", comment: "Invalid birthday date error description")
        }
    }
}

protocol UserFormViewDelegate: class {
    
    var documentNumber: String? { get }
    var fullName: String? { get }
    var birthday: Date? { get }
    var email: String? { get }
    var mobilePhone: String? { get }
    var motherName: String? { get }
    
    var showsBirthdayTextField: Bool { get }
    var showsMotherNameTextField: Bool { get }
    var isHiddenBirthdayTextField: Bool { get }
    
    func setBirthdayDateTextField(text: String)
    func present(error: Error)
    
    func documentNumberError(isOn: Bool)
    func fullNameError(isOn: Bool)
    func birthdayError(isOn: Bool)
    func emailError(isOn: Bool)
    func phoneError(isOn: Bool)
    func motherNameError(isOn: Bool)
}

protocol UserFormPresenterDelegate {
    
    var delegate: UserFormViewDelegate? { get set }
    
    var maximumBirthdayDate: Date { get }
    var isUserValid: Bool { get }
    
    func didSetBirthday(to date: Date)
    var userFormDataView: UserFormDataView? { get }
}

class UserFormPresenter: UserFormPresenterDelegate {

    weak var delegate: UserFormViewDelegate?

    lazy var maximumBirthdayDate: Date = { () -> Date in
        let calendar = Calendar.current
        let now = Date()
        return calendar.date(byAdding: .year, value: -18, to: now)!
    }()
    
    var isUserValid: Bool {
        
        var documentNumberStatus = true
        var fullNameStatus = true
        var birthdayStatus = true
        var emailStatus = true
        var phoneStatus = true
        
        let charset = CharacterSet(charactersIn: " ")
        
        guard let documentNumber = delegate?.documentNumber else {
            return false
        }
        
        guard let mobilePhone = delegate?.mobilePhone else { return false }
        guard let email = delegate?.email?.trimmingCharacters(in: charset) else { return false }
        guard let fullName = delegate?.fullName else { return false }
        
        if documentNumber.isEmpty  {
            documentNumberStatus = false
            delegate?.documentNumberError(isOn: true)
            //self.delegate?.present(error: UserFormError.documentIsRequired)
            //return false
        }else{
            documentNumberStatus = true
            delegate?.documentNumberError(isOn: false)
        }
        if !documentNumber.isValidCPF {
            documentNumberStatus = false
            delegate?.documentNumberError(isOn: true)
            //self.delegate?.present(error: UserFormError.invalidDocument)
            //return false
        }else{
            documentNumberStatus = true
            delegate?.documentNumberError(isOn: false)
        }
        
        if fullName.isEmpty {
            fullNameStatus = false
            delegate?.fullNameError(isOn: true)
            //self.delegate?.present(error: UserFormError.nameIsRequired)
            //return false
        }else{
            fullNameStatus = true
            delegate?.fullNameError(isOn: false)
        }
        
        if email.isEmpty {
            emailStatus = false
            delegate?.emailError(isOn: true)
            //self.delegate?.present(error: UserFormError.emailIsRequired)
            //return false
        }else{
            emailStatus = true
            delegate?.emailError(isOn: false)
        }
        
        if !email.isValidEmail {
            emailStatus = false
            delegate?.emailError(isOn: true)
            //self.delegate?.present(error: UserFormError.invalidEmail)
            //return false
        }else{
            emailStatus = true
             delegate?.emailError(isOn: false)
        }
    
        if mobilePhone.isEmpty {
            phoneStatus = false
            delegate?.phoneError(isOn: true)
            //self.delegate?.present(error: UserFormError.phoneNumberIsRequired)
            //return false
        }else{
            phoneStatus = true
            delegate?.phoneError(isOn: false)
        }
        
        if !mobilePhone.isValidPhoneNumber {
            phoneStatus = false
            delegate?.phoneError(isOn: true)
            //self.delegate?.present(error: UserFormError.invalidPhoneNumber)
            //return false
        }else{
            phoneStatus = true
            delegate?.phoneError(isOn: false)
        }
        
        if delegate!.isHiddenBirthdayTextField == false {
            if let birthdayIsHidden = delegate?.isHiddenBirthdayTextField {
                if (!birthdayIsHidden && delegate?.birthday == nil) {
                    birthdayStatus = false
                    delegate?.birthdayError(isOn: true)
                }else{
                    birthdayStatus = true
                    delegate?.birthdayError(isOn: false)
                }
            }else{
                birthdayStatus = false
                delegate?.birthdayError(isOn: true)
            }
        }
        
        if documentNumberStatus == true && fullNameStatus == true && birthdayStatus == true && emailStatus == true && phoneStatus == true {
            return true
        }
        return false
    }
    
    var userFormDataView: UserFormDataView? {
        guard isUserValid else { return nil }
        let charset = CharacterSet(charactersIn: " ")
        
        guard let documentNumber = delegate?.documentNumber, let mobilePhone = delegate?.mobilePhone, let email = delegate?.email?.trimmingCharacters(in: charset), let fullName = delegate?.fullName else { return nil }
        
        return UserFormDataView(model: UserForm(documentNumber: documentNumber, name: fullName, birthDate: delegate?.birthday, email: email, phoneNumber: mobilePhone, motherName: delegate?.motherName))
   }
    
    func didSetBirthday(to date: Date) {
        guard date < maximumBirthdayDate else {
            self.delegate?.present(error: UserFormError.invalidBirthdayDate)
            return
        }
        let dateString = DateFormatter.dateOnlyFormatter.string(from: date)
        delegate?.setBirthdayDateTextField(text: dateString)
    }
    
}
