//
//  CreditCardFormPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol CreditCardFormViewDelegate: class {
    func showExpirateDate(value: String)
    func updateBrandImage(image: UIImage)
    func changeDefaultMask()
    func changeAmexMask()
    
    var cardNumber: String? { get }
    var holderName: String? { get }
}

protocol CreditCardFormPresenterDelegate {
    var delegate: CreditCardFormViewDelegate? { get set }
    
    var quantityMonths: Int { get }
    var quantityYears: Int { get }
    
    func year(at row: Int) -> String
    func month(at row: Int) -> String
    func didSetExpirationDate(at month: Int, at year: Int)
    func cardNumberRegexUpdate(for cardNumber: String)
    
    func set(year: String)
    func set(month: Int)
    
    var isCreditCardValid: Bool { get }
    var creditCardFormDataView: CreditCardFormDataView? { get }
}

class CreditCardFormPresenter: CreditCardFormPresenterDelegate {
    weak var delegate: CreditCardFormViewDelegate?
    
    private lazy var currentYear: Int = Calendar.current.component(.year, from: Date())
    private var cardAuthorizerId: AuthorizerId?
    private var selectedYear: String?
    private var selectedMonth: Int?
    
    var quantityMonths: Int = 12
    var quantityYears: Int = 10

    func month(at row: Int) -> String {
        return String(format: "%02d", row + 1)
    }
    
    func year(at row: Int) -> String {
        let year = currentYear + row
        return year.description
    }
    
    func set(year: String) {
        selectedYear = year
    }
    
    func set(month: Int) {
        selectedMonth = month
    }
    
    var isCreditCardValid: Bool {
        guard let _ = selectedYear, let _ = selectedMonth else { return false }
        
        return ![delegate?.cardNumber, delegate?.holderName].compactMap { $0 }.contains { $0.isEmpty }
    }
    
    func didSetExpirationDate(at month: Int, at year: Int) {
        selectedMonth = month + 1
        selectedYear = self.year(at: year)
        
        var shortYear = "\(selectedYear!)"
        shortYear.removeFirst(2)
        let value = "\(self.month(at: month))/\(shortYear)"
        self.delegate?.showExpirateDate(value: value)
    }
    
    func cardNumberRegexUpdate(for cardNumber: String) {
        let authorizerId = CardUtils.authorizerId(for: cardNumber)
        cardAuthorizerId = (authorizerId == .unknow) ? nil : authorizerId
        
        cardBrandMaskedChange(authorizerId: cardAuthorizerId)
        delegate?.updateBrandImage(image: CardUtils.cardBrandImage(for: cardNumber))
    }
    
    var creditCardFormDataView: CreditCardFormDataView? {
        guard isCreditCardValid else { return nil }
        
        guard let cardNumber = delegate?.cardNumber, let holderName = delegate?.holderName, let selectedYear = selectedYear, let selectedMonth = selectedMonth else { return nil }
        
        let cardNumberString = cardNumber.replacingOccurrences(of: " ", with: "")
        let yearsIntenger = Int(selectedYear)!
        let years = yearsIntenger - 2000
        let months = String(format: "%02d", selectedMonth)
        let expirationDate = "\(months)\(years.description)"
        
        return CreditCardFormDataView(model: CreditCardForm(cardNumber: cardNumberString, holderName: holderName, expirationDate: expirationDate, defaultCard: false, authorizerId: cardAuthorizerId))
    }
    
    private func cardBrandMaskedChange(authorizerId: AuthorizerId?) {
        guard let authorizerId = authorizerId else { return }
        
        if authorizerId == .americanExpress {
            delegate?.changeAmexMask()
        }
        else {
            delegate?.changeDefaultMask()
        }
    }
}
