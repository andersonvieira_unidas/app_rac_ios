//
//  ListCardPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol ListCardViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func reloadData()
    func enableDone()
    
    func showActions(at indexPath: IndexPath, isMarkFavoriteHidden: Bool)
    func showEmptyCardView()
    func addButtonIsHidden(isHidden: Bool)
    //func addCardButton()
}

protocol ListCardPresenterDelegate {
    var delegate: ListCardViewDelegate? { get set }
    
    var numberOfCards: Int { get }
    func fetchCard()
    func didSelectItemAt(at index: Int)
    func card(at index: Int) -> ListCardDataView
    func getSelectedCard() -> ListCardDataView
    func selectedCard(at index: Int) -> Bool
    func enableDone()
    func markFavoriteCard(at indexPath: IndexPath)
    func didTapMoreAction(at indexPath: IndexPath)
    func deleteCard(at indexPath: IndexPath)
}

class ListCardPresenter: ListCardPresenterDelegate {
    
    weak var delegate: ListCardViewDelegate?
    
    private var paymentService: PaymentService
    private var cards: [ListCard] = []    
    private var selectedCard: ListCard? = nil

    var numberOfCards: Int {
        return cards.count
    }
    
    init(paymentService: PaymentService) {
        self.paymentService = paymentService
    }
    
    func fetchCard() {
        self.performFetchCard()
    }
    
    private func performFetchCard() {
        guard let user = session.currentUser else { return }

        self.delegate?.startLoading()
        
        paymentService.listCard(user: user, response: (success: { [weak self] cards in
            self?.cards = cards.sorted(by: { $0.metodoPagamentoCartaoCreditoPreferencial > $1.metodoPagamentoCartaoCreditoPreferencial })
            self?.enableDone()
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 404 {
                    self?.cards = []
                    return
                }

                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
                self?.delegate?.reloadData()
                self?.delegate?.addButtonIsHidden(isHidden: true)
                if self?.cards.isEmpty ?? true {
                    self?.delegate?.showEmptyCardView()
                }else{
                    self?.delegate?.addButtonIsHidden(isHidden: false)
                }
            })
        )
    }
    
    func card(at index: Int) -> ListCardDataView {
        return ListCardDataView(model: cards[index])
    }
    
    func didSelectItemAt(at index: Int) {
        self.selectedCard = cards[index]
        self.enableDone()
        self.delegate?.reloadData()
    }
    
    func selectedCard(at index: Int) -> Bool {
        guard let selectedCard = selectedCard else {
            return false
        }

        return cards[index].pan == selectedCard.pan
    }
    
    func enableDone() {
        if let _ = selectedCard {
            self.delegate?.enableDone()
        }
    }
    
    func getSelectedCard() -> ListCardDataView {
        return ListCardDataView(model: selectedCard!)
    }
    
    func markFavoriteCard(at indexPath: IndexPath) {
        let listCardDataView = card(at: indexPath.row)
        
        guard let tkncodseq = listCardDataView.model.tkncodseq, let currentUser = session.currentUser else { return }
 
        self.delegate?.startLoading()
        
        paymentService.markFavoriteCard(user: currentUser, tknCodSeq: tkncodseq, response: (success: { [weak self] in
            self?.performFetchCard()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
            })
        )
    }
    

    func deleteCard(at indexPath: IndexPath) {
        let listCardDataView = card(at: indexPath.row)
        
        guard let tkncodseq = listCardDataView.model.tkncodseq, let currentUser = session.currentUser else { return }
        
        self.delegate?.startLoading()
        
        paymentService.deleteCard(user: currentUser, tknCodSeq: tkncodseq, response: (success: { [weak self] in
            self?.performFetchCard()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
            })
        )
    }
    
    func didTapMoreAction(at indexPath: IndexPath) {
        let listCardDataView = ListCardDataView(model: cards[indexPath.row])
        let isMarkFavoriteHidden = !(listCardDataView.isValid && !listCardDataView.isFavoriteCard)
        self.delegate?.showActions(at: indexPath, isMarkFavoriteHidden: isMarkFavoriteHidden)
    }
}
