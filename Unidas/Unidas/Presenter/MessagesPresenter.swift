//
//  MessagesPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol MessagesViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func reloadData()
    func showEmptyMessageView()
    func canShowPendencies(value:Bool)
}

protocol MessagesPresenterDelegate {
    var delegate: MessagesViewDelegate? { get set }
    
    var numberOfRows: Int { get }
    func message(at row: Int) -> MessageDataView
    func fetchMessage()
    func fetchCredDefenseSerasaStatus()
}

class MessagesPresenter: MessagesPresenterDelegate {
    weak var delegate: MessagesViewDelegate?
    
    private var messageService: MessageService
    private var messages: [Message] = []
    
    init(messageService: MessageService) {
        self.messageService = messageService
        NotificationCenter.default.addObserver(self, selector: #selector(fetchMessage), name: .UNDidUnreadMessage, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    
    @objc func fetchMessage() {
        guard let currentUser = session.currentUser else {
            messages = []
            delegate?.reloadData()
            return
        }
        self.delegate?.startLoading()
        
        messageService.fetch(documentNumber: currentUser.account.documentNumber, response: (success: { [weak self] messages in
            
            self?.messages = messages
            //.sorted { $0.type < $1.type }
            
            if messages.count == 0 {
                self?.delegate?.showEmptyMessageView()
                return
            }

            self?.delegate?.reloadData()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion:{ [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    var numberOfRows: Int {
        return messages.count
    }
    
    func message(at row: Int) -> MessageDataView {
        return MessageDataView(model: messages[row])
    }
    
    func fetchCredDefenseSerasaStatus() {
        self.delegate?.startLoading()
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        messageService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                var serasaIsValid = true
                var credDefenseIsValid = true
                
                if let isValid = userStatus.serasaIsValid {
                    serasaIsValid = isValid
                }
                if let isValid = userStatus.credDefenseIsValid {
                    credDefenseIsValid = isValid
                }
                
                let result = serasaIsValid == true && credDefenseIsValid == true ? true : false
                
                self?.delegate?.canShowPendencies(value: result)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
                self.delegate?.canShowPendencies(value: false)
        }, completion: {
            
        }))
    }
}
