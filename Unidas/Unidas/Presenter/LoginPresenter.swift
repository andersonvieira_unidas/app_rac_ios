//
//  LoginPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 06/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAnalytics
import Alamofire

enum LoginError: LocalizedError {
    
    case invalidDocument
    case requiredDocument
    
    var errorDescription: String? {
        switch self {
        case .invalidDocument:
            return NSLocalizedString("Invalid document", comment: "Invalid document error description")
        case .requiredDocument:
            return NSLocalizedString("Required document", comment: "Required document error description")
        }
        
    }
}

enum BiometryState: CustomStringConvertible {
    case available, locked, notAvailable
    
    var description: String {
        switch self {
        case .available:
            return "available"
        case .locked:
            return "locked (temporarily)"
        case .notAvailable:
            return "notAvailable (turned off/not enrolled)"
        }
    }
}

protocol LoginViewDelegate: class {
    func startLoading()
    func endLoading()
    func presentFormError()
    func present(error: Error)
    func present(error: String)
    
    func performSignUp(userResponseDataView: UserResponseDataView?, formAction: FormsAction)
    func performPin(userResponseDataView: UserResponseDataView)
    
    func finishPin(pinViewController: PINViewController)
    func pinEndLoading(pinViewController: PINViewController)
    func pinPresent(error: Error, pinViewController: PINViewController)
    func pinPresent(title: String, error: String, pinViewController: PINViewController)
    func dismiss()
    func showAlertManuallyLogin()
}

protocol LoginPresenterDelegate {
    var delegate: LoginViewDelegate? { get set }
    
    func authWithCpf(for cpf: String?)
    func authWithPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController)
    func authWithPin(documentNumber: String, pin: String)
    func addCredentials(documentNumber: String, pin: String)
    func removeKeyChain(withKey key: String)
}

class LoginPresenter: LoginPresenterDelegate {
    
    weak var delegate: LoginViewDelegate?
    private var unidasLoginService: UnidasLoginService
    private var unidasUserStatusService: UnidasUserStatusService
    private var userCheckService: UserCheckService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var userUnidas: UserUnidas?
    private var imageTemporaryDirectory: ImageTemporaryDirectory
    
    init(unidasLoginService: UnidasLoginService,
         userCheckService: UserCheckService,
         imageTemporaryDirectory: ImageTemporaryDirectory,
         unidasUserStatusService: UnidasUserStatusService,
         firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.unidasLoginService = unidasLoginService
        self.userCheckService = userCheckService
        self.imageTemporaryDirectory = imageTemporaryDirectory
        self.unidasUserStatusService = unidasUserStatusService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func authWithCpf(for cpf: String?) {
        
        guard let cpf = cpf else { return }
        
        if cpf.isEmpty, !cpf.isValidCPF {
            self.delegate?.presentFormError()
            return
        }
        
        self.delegate?.startLoading()
        
        let newCpf = cpf.removeDocumentNumberCharacters
        
        unidasUserStatusService.get(documentNumber: newCpf, response: (success: { [weak self] userStatus in
            guard let userStatus = userStatus.first else { return }
            
            let userResponseDataView = UserResponseDataView(model: UserResponse(token: nil, account: Account(id: nil, user: nil, documentNumber: newCpf, fcm: nil, mobilePhone: "", email: "", name: "", companyName: nil, deviceId: nil, profileImage: nil, birthDate: nil, motherName: nil), unidas: nil))
            
            self?.userCheckValidate(userUnidasStatus: userStatus, userResponseDataView: userResponseDataView)
            
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 404 {
                    let userResponseDataView = UserResponseDataView(model: UserResponse(token: nil, account: Account(id: nil, user: nil, documentNumber: newCpf, fcm: nil, mobilePhone: "", email: "", name: "", companyName: nil, deviceId: nil, profileImage: nil, birthDate: nil, motherName: nil), unidas: nil))
                    
                    self?.delegate?.performSignUp(userResponseDataView: userResponseDataView, formAction: FormsAction.create)
                    return
                }
                
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func authWithPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        guard let userResponse = userResponseDataView?.model else {
            self.delegate?.pinPresent(error: LoginError.invalidDocument, pinViewController: pinViewController)
            self.delegate?.endLoading()
            return
        }
        
        auth(documentNumber: userResponse.account.documentNumber, pin: pin, pinViewController: pinViewController)
    }
    
    func authWithPin(documentNumber: String, pin: String) {
        auth(documentNumber: documentNumber, pin: pin, pinViewController: nil)
    }
    
    func addCredentials(documentNumber: String, pin: String) {
        let r = KeychainHelper.createBioProtectedEntry(key: documentNumber.removeDocumentNumberCharacters, data: Data(pin.utf8))
        print(r == noErr ? "Entry created" : "Entry creation failed, osstatus=\(r)")
    }
    
    func removeKeyChain(withKey key: String) {
        KeychainHelper.remove(key: key.removeDocumentNumberCharacters)
    }
    
    private func auth(documentNumber: String, pin: String, pinViewController: PINViewController?){
        var logged = false
        unidasLoginService.auth(documentNumber: documentNumber, pin: pin, response: (success: { [weak self] userUnidas in
            
            if let _ = userUnidas.contaClienteAlerta, userUnidas.contaClienteId == 0, let viewController = pinViewController {
                let title = NSLocalizedString("Login Wrong Password Title", comment: "")
                let message = NSLocalizedString("Login Wrong Password Message", comment: "")
                self?.delegate?.pinPresent(title: title, error: message, pinViewController: viewController)
                return
            }
            
            if let alertError = userUnidas.contaClienteAlerta, let viewController = pinViewController {
                self?.delegate?.pinPresent(error: ServiceError(message: alertError), pinViewController: viewController)
                return
            }
            
            if let _ = userUnidas.contaClienteAlerta, pinViewController == nil{
                self?.delegate?.showAlertManuallyLogin()
                return
            }
            
            self?.userResponseDataView(userUnidas: userUnidas)
            
            if let accessToken = userUnidas.contaClienteToken {
                AppPersistence.save(key: AppPersistence.token, value: accessToken)
                let headers = HTTPHeaders.init(["Authorization": accessToken])
                ServiceConfiguration.unidas.headers = headers
            }
            
            self?.imageTemporaryDirectory.addImage(userResponse: session.currentUser, type: .profile)
            NotificationCenter.default.post(name: .UNUserDidLogin, object: session.currentUser)
            
            //analytics
            let utf8str = documentNumber.data(using: String.Encoding.utf8)
            
            if let base64DocumentNumber = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: String.Encoding.utf8.rawValue)) {
                
                self?.firebaseAnalyticsService.addUserEvent(withProperty: AnalyticsUserProperty.documentNumber, withValue: base64DocumentNumber)
            }
            let postNotificationPermission = AppPersistence.getValueBool(withKey: AppPersistence.postNotificationPermission)
            
            if postNotificationPermission ?? false {
                self?.updateNotificationConfiguration(documentNumber: documentNumber)
            }
            
            self?.firebaseAnalyticsService.addEvent(withEvent: AnalyticsEventLogin, withValue: [ "method": "document_number"])
            logged = true
            
            if let viewController = pinViewController {
                self?.delegate?.finishPin(pinViewController: viewController)
            }
            }, failure: { [weak self] error in
                if let viewController = pinViewController {
                    self?.delegate?.pinPresent(error: error, pinViewController: viewController)
                }
            }, completion: { [weak self] in
                if session.currentUser != nil{
                    self!.verifyPendencies(documentNumber: documentNumber)
                }
                if let viewController = pinViewController {
                    self?.delegate?.pinEndLoading(pinViewController: viewController)
                } else if logged {
                    self?.delegate?.dismiss()
                }
                
        })
        )
    }
    // Private Function
    private func verifyPendencies(documentNumber: String){
        unidasUserStatusService.checkUserDocuments(documentNumber: documentNumber, response: (success: { usersDocumentsUser in
            
            let userInValidateDataView = UserInValidateDataView(model: usersDocumentsUser.userCheck)
            
            AppPersistence.save(key: AppPersistence.pendencies, value: userInValidateDataView.userHasPendencies)
            }, failure: { error in
        }, completion: {
        }))
        
    }
    private func userResponseDataView(userUnidas: UserUnidas) {
        guard let codePhone = userUnidas.contaClienteLoginCelularDDD,
            let phoneNumber = userUnidas.contaClienteLoginCelular,
            let documentNumber = userUnidas.clidoc,
            let email = userUnidas.contaClienteLoginEmail, let name = userUnidas.contaClienteNome,
            let birthDate = userUnidas.contaClienteNascimento  else { return }
        
        let formattedPhone = "\(String(codePhone))\(String(phoneNumber))"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        let userResponse = UserResponse(token: nil, account: Account(id: nil, user: nil, documentNumber: documentNumber, fcm: nil, mobilePhone: formattedPhone, email: email, name: name, companyName: nil, deviceId: nil, profileImage: nil, birthDate: dateFormatter.date(from: birthDate), motherName: userUnidas.contaClienteNomeMae), unidas: [userUnidas])
        
        session.currentUser = userResponse
    }
    
    private func userCheckValidate(userUnidasStatus: UserUnidasStatus, userResponseDataView: UserResponseDataView) {
        switch userUnidasStatus.status {
        case .noUser:
            self.delegate?.performSignUp(userResponseDataView: userResponseDataView, formAction: FormsAction.create)
            break
        case .userCreated:
            self.delegate?.performSignUp(userResponseDataView: userResponseDataView, formAction: FormsAction.preUpdate)
            break
        case .userInValidate, .userValidate:
            self.delegate?.performPin(userResponseDataView: userResponseDataView)
            break
        }
    }
    
    private func updateNotificationConfiguration(documentNumber: String){
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async{
                let notificationConfigurationService = NotificationConfigurationService()
                notificationConfigurationService.fetch(documentNumber: documentNumber, response: (success: { notifications in
                    guard let items = notifications else { return }
                    
                    var notificationsSave: [NotificationConfiguration] = []
                    
                    for item in items {
                        let notification = NotificationConfiguration(code: item.code, description: item.description, active: true)
                        notificationsSave.append(notification)
                    }
                    
                    notificationConfigurationService.updateNotifications(documentNumber: documentNumber, notifications: notificationsSave, response: (success: {
                        AppPersistence.save(key: AppPersistence.postNotificationPermission, value: false)
                    }, failure: { error in
                        print(error)
                    }, completion: {
                    })
                    )
                    
                    }, failure: { error in
                }, completion: {
                })
                )
            }
        }
        
    }
}
