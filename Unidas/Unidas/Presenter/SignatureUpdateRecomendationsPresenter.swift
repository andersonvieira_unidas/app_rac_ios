//
//  SignatureUpdateRecomendationsPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol SignatureUpdateRecomendationsViewDelegate: class {
    func finishUploadSignature(viewController: CustomSignatureViewController)

    func customSignatureStartLoading(viewController: CustomSignatureViewController)
    func customSignatureEndLoading(viewController: CustomSignatureViewController)
    func customSignaturePresent(error: Error, viewController: CustomSignatureViewController)
}

protocol SignatureUpdateRecomendationsPresenterDelegate {
    var delegate: SignatureUpdateRecomendationsViewDelegate? { get set }
    
    func addSignature(image: Data?, viewController: CustomSignatureViewController)
}

class SignatureUpdateRecomendationsPresenter: SignatureUpdateRecomendationsPresenterDelegate {
    weak var delegate: SignatureUpdateRecomendationsViewDelegate?
    private var accountPhotoUnidasService: AccountPhotoUnidasService
    
    init(accountPhotoUnidasService: AccountPhotoUnidasService) {
        self.accountPhotoUnidasService = accountPhotoUnidasService
    }
    
    func addSignature(image: Data?, viewController: CustomSignatureViewController) {
        guard let currentUser = session.currentUser else { return }
        
        self.delegate?.customSignatureStartLoading(viewController: viewController)
        
        guard let imageBase64 = image?.base64EncodedString() else { return }
        let imageType = ImageType.signature
        
        accountPhotoUnidasService.saveDocument(user: currentUser, userImage: imageBase64, imageType: imageType, response: (success: { [weak self] in
 
            self?.delegate?.finishUploadSignature(viewController: viewController)
            }, failure: { [weak self] error in
                self?.delegate?.customSignaturePresent(error: error, viewController: viewController)
            }, completion: { [weak self] in
                self?.delegate?.customSignatureEndLoading(viewController: viewController)
            })
        )
    }
}
