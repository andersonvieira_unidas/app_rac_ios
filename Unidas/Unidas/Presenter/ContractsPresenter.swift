//
//  ContractsPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//


import UIKit

protocol ContractsViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    
    func showEmptyView(title: String, message: String)
    func reloadData()
    func didSelectRow(contractDataView: ContractDataView)
}

protocol ContractsPresenterDelegate {
    var delegate: ContractsViewDelegate? { get set }
    var numberOfRows: Int { get}
    func fetchContract()
    
    func contract(at row: Int) -> ContractDataView
    func didSelectRow(at row: Int)
}

class ContractsPresenter: ContractsPresenterDelegate {
    private var contractService: ContractService
    private var contract: [Contract] = []
    weak var delegate: ContractsViewDelegate?
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }

    var numberOfRows: Int {
        return contract.count
    }
    
    init(contractService: ContractService) {
        self.contractService = contractService
    }
    
    func fetchContract() {
        guard let currentUser = currentUser else { return }
        self.delegate?.startLoading()
        
        contractService.fetchContracts(for: currentUser, response: (success: { [weak self] contract in
            self?.contract = contract.filter({$0.status == .closed})
            self?.delegate?.reloadData()
            }, failure: { [weak self] error in
                if let error = error as? ServiceError, error.code == 404 || error.code == 400 {
                    self?.contract = []
                    return
                }
                
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                if self?.contract.isEmpty ?? true {
                    self?.delegate?.showEmptyView(title: NSLocalizedString("There are no registered contracts", comment: "Empty contracts title"), message: NSLocalizedString("You haven't had new reservations with Unidas", comment: "Empty contracts message"))
                }
                self?.delegate?.endLoading()
        }))
    }
    
    func contract(at row: Int) -> ContractDataView {
        return ContractDataView(model: contract[row])
    }
    
    func didSelectRow(at row: Int) {
        delegate?.didSelectRow(contractDataView: ContractDataView(model: contract[row]))
    }
    
}
