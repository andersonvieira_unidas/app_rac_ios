//
//  CancelConfirmationPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation


protocol CancelConfirmationViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func updateDescription(calculationCancel: CalculationCancelDataView?)
}

protocol CancelConfirmationPresenterDelegate {
    var delegate: CancelConfirmationViewDelegate? { get set }
    func viewDidLoad(vehicleReservationsDataView: VehicleReservationsDataView)
}

class CancelConfirmationPresenter: CancelConfirmationPresenterDelegate {
    weak var delegate: CancelConfirmationViewDelegate?
    private var calculationCancelService: CalculationCancelService

    init(calculationCancelService: CalculationCancelService) {
        self.calculationCancelService = calculationCancelService
    }
    
    func viewDidLoad(vehicleReservationsDataView: VehicleReservationsDataView) {
        self.delegate?.startLoading()
        
        if vehicleReservationsDataView.isPaid {
            calculationCancelService.calcule(reserveNumber: vehicleReservationsDataView.reserveNumber, response: (success: { calculationCancel in
                
                self.delegate?.updateDescription(calculationCancel: CalculationCancelDataView(model: calculationCancel))
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
                }, completion: {
                    self.delegate?.endLoading()
            }))
            return
        }
        else {
            self.delegate?.updateDescription(calculationCancel: nil)
        }
        self.delegate?.endLoading()
    }
}
