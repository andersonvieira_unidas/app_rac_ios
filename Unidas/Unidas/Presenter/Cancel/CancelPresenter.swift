//
//  CancelPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 10/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import EventKit

enum CancelError: LocalizedError {
    case noReasonChoosen
    case noReservationFound
    case remindersAccessNotAllowed
    case calendarAccessNotAllowed
    
    var errorDescription: String? {
        switch self {
        case .noReasonChoosen:
            return NSLocalizedString("You need to choose a reason", comment: "You need to choose a reason error message")
        case .noReservationFound:
            return NSLocalizedString("No reservation found", comment: "No reservation found error message")
        case .remindersAccessNotAllowed:
            return NSLocalizedString("Reminders access not allowed.", comment: "Reminders access not allowed message")
        case .calendarAccessNotAllowed:
            return NSLocalizedString("Celendar access not allowed.", comment: "Calendar access not allowed message")
        }
    }
}

protocol CancelViewDelegate: class {
    func reloadData()
    func startLoading()
    func endLoading()
    func startCancelLoading()
    func endCancelLoading()
    func present(error: Error)
    
    func finishConfirmCancellation()
    func dismiss()
}

protocol CancelPresenterDelegate {
    var delegate: CancelViewDelegate? { get set }
    var numberOfReasons: Int { get }
    
    func getCancellationReasons()
    func cancellationReason(at index: Int) -> CancellationReasonDataView
    func confirmCancellation(at indexPath: IndexPath?, vehicleReservationsDataView: VehicleReservationsDataView?, userDocument: String?)
    func finishCancellation()
}

class CancelPresenter: CancelPresenterDelegate {
    weak var delegate: CancelViewDelegate?
    private var cancellationReasonService: CancellationReasonService
    private var calculationCancelService: CalculationCancelService
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var cancellationReason: [CancellationReason] = []
    
    //events and reminders
    lazy var eventStore = EKEventStore()
    lazy var reminderStore = EKEventStore()
    
    var numberOfReasons: Int {
        return cancellationReason.count
    }
    
    lazy var reminderCalendar: EKCalendar? = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        var calendar = reminderStore.calendars(for: .reminder).first(where: { $0.title == title })
        if let calendar = calendar {
            return calendar
        }
        return nil
    }()
    
    lazy var eventCalendar: EKCalendar? = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        var calendar = eventStore.calendars(for: .event).first(where: { $0.title == title })
        if let calendar = calendar {
            return calendar
        }
        return nil
    }()
    
    init(cancellationReasonService: CancellationReasonService,
         calculationCancelService: CalculationCancelService,
         firebaseAnalyticsService: FirebaseAnalyticsService) {
        self.cancellationReasonService = cancellationReasonService
        self.calculationCancelService = calculationCancelService
        self.firebaseAnalyticsService = firebaseAnalyticsService
    }
    
    func getCancellationReasons() {
        self.delegate?.startLoading()
        
        cancellationReasonService.get(response: (success: { [weak self] cancellationReasons in
            self?.cancellationReason = cancellationReasons
            self?.delegate?.reloadData()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    func cancellationReason(at index: Int) -> CancellationReasonDataView {
        return CancellationReasonDataView(model: cancellationReason[index])
    }
    
    func confirmCancellation(at indexPath: IndexPath?, vehicleReservationsDataView: VehicleReservationsDataView?, userDocument: String?) {
        guard let indexPath = indexPath else {
            self.delegate?.present(error: CancelError.noReasonChoosen)
            return
        }
        
        guard let vehicleReservationsDataView = vehicleReservationsDataView else {
            self.delegate?.present(error: CancelError.noReservationFound)
            return
        }
        
        self.delegate?.startCancelLoading()
        
        let vehicleReservation = vehicleReservationsDataView.model
        
        calculationCancelService.cancelReservation(id: vehicleReservation.numeroReserva, userDocument: userDocument, reason: cancellationReason[indexPath.row].id, response: (success: { [weak self] in
            self?.delegate?.finishConfirmCancellation()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endCancelLoading()
                
                let reason = self?.cancellationReason[indexPath.row].motivoUsuario ?? ""
                self?.registerEvent(reservation: vehicleReservation.numeroReserva, reason: reason)
        }))
        
        removeReminderEvent(withReservationNumber: vehicleReservation.numeroReserva)
        removeCalendarEvent(withReservation: vehicleReservation)
    }
    
    private func removeReminderEvent(withReservationNumber reservation: Int){
        switch EKEventStore.authorizationStatus(for: .reminder) {
        case .authorized:
            self.removeReminder(withReservationNumber: reservation)
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .reminder) { [weak self] (granted, error) in
                if let error = error {
                    self?.delegate?.present(error: error)
                } else if granted {
                    self?.removeReminder(withReservationNumber: reservation)
                }
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    private func removeCalendarEvent(withReservation reservation: VehicleReservations){
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            self.removeCalendar(withReservation: reservation)
            break
        case .notDetermined:
            reminderStore.requestAccess(to: .event) { [weak self] (granted, error) in
                if let error = error {
                    self?.delegate?.present(error: error)
                } else if granted {
                    self?.removeCalendar(withReservation: reservation)
                }
            }
            break
        case .denied, .restricted:
            break
        @unknown default:
            break
        }
    }
    
    /** REMINDER **/
    private func removeReminder(withReservationNumber reservation: Int){
        guard let reminderCalendar = self.reminderCalendar else { return }
        
        let predicate = eventStore.predicateForReminders(in: [reminderCalendar])
        reminderStore.fetchReminders(matching: predicate) { reminders in
            if let list = reminders {
                let reminder = list.filter({ (reminder) -> Bool in
                    return reminder.notes?.contains(String(reservation)) ?? false
                }).first
                
                if let index = reminder {
                    do {
                        try self.reminderStore.remove(index, commit: true)
                    } catch {
                        self.delegate?.present(error: error)
                    }
                }
            }
        }
    }
    
    /** CALENDAR **/
    private func removeCalendar(withReservation reservation: VehicleReservations){
        
        guard let eventCalendar = self.eventCalendar else { return }
        
        let start = reservation.dataHoraRetirada.addingTimeInterval(-3600)
        let end = reservation.dataHoraDevolucao.addingTimeInterval(+3600)
        
        let predicate = eventStore.predicateForEvents(withStart: start, end: end, calendars: [eventCalendar])
        
        guard let listEvents = eventStore.events(matching: predicate) as [EKEvent]? else { return }
        
        let filterEvents = listEvents.filter({ (event) -> Bool in
            return event.notes?.contains(String(reservation.numeroReserva)) ?? false
        })
        
        for event in filterEvents {
            do {
                try self.eventStore.remove(event, span: EKSpan.thisEvent, commit: true)
            } catch {
                self.delegate?.present(error: error)
            }
        }
    }
    
    private func registerEvent(reservation: Int, reason: String){
        var parameters: [String : Any] = [:]
        
        parameters["reservation_id"] = reservation
        parameters["reason"] = reason
        
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.cancelReservation.name, withValue: parameters)
    }
    
    func finishCancellation() {
        NotificationCenter.default.post(name: .UNDidCanceledReservation, object: nil)        
        self.delegate?.dismiss()
    }
}
