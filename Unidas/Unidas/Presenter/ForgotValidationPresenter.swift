//
//  ForgotValidationPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 20/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

protocol ForgotValidationViewDelegate: class {
    func pinEndLoading()
    func pinPresent(error: Error)
    
    func startLoading()
    func endLoading()
    func present(error: Error)
    func presenteCodeError()
   
    func requestNewPin()
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController)
    func didPinEndLoading(pinViewController: PINViewController)
    func didFailConfirmPin(pinViewController: PINViewController, error: Error, callbackDidFailFinishPin: Bool)
    func didFinishUpdatePin(pinViewController: PINViewController)
}

protocol ForgotValidationPresenterDelegate {
    var delegate: ForgotValidationViewDelegate? { get set }
    
    func setRecoveryValidate(_ validate: Bool)
    
    func postForgotPin(code: String?)
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController)
    func resendCode(recoveryMethod: AccountRecoveryMethod)
}

class ForgotValidationPresenter: ForgotValidationPresenterDelegate {

    weak var delegate: ForgotValidationViewDelegate?
    private var recoveryService: RecoveryService
    private var userResponse: UserResponse
    private var accountsService: AccountsService
    private var authUnidasService: AuthUnidasService
    private var userCheckService: UserCheckService
    private var codeValidationService: CodeValidationService
    
    private var recoveryValidate: Bool = false
    private var confirmPIN: Bool = false
    private var pin: String? = nil
    private var unidasToken: UnidasToken? = nil
    
    init(recoveryService: RecoveryService, userResponseDataView: UserResponseDataView,
         accountsService: AccountsService, authUnidasService: AuthUnidasService, userCheckService: UserCheckService, codeValidationService: CodeValidationService, unidasToken: UnidasToken) {
        self.recoveryService = recoveryService
        self.userResponse = userResponseDataView.model
        self.accountsService = accountsService
        self.authUnidasService = authUnidasService
        self.userCheckService = userCheckService
        self.codeValidationService = codeValidationService
        self.unidasToken = unidasToken
    }
    
    func setRecoveryValidate(_ validate: Bool) {
        self.recoveryValidate = validate
    }
    
    func postForgotPin(code: String?) {
        guard let code = code, code.count == 6 else {
            delegate?.present(error: ForgotPINValidateError.methodRequired)
            return
        }
        
        delegate?.startLoading()
        let codeFormatted = code.replacingOccurrences(of: "-", with: "")
        
        userCheckService.generateExpiredToken(documentNumber: userResponse.account.documentNumber, response: (success: { [weak self] unidasToken in
            self?.unidasToken = unidasToken
            self?.codeValidationService.validateRemember(user: self!.userResponse, code: codeFormatted, unidasToken: unidasToken, response: (success: { [weak self] in
                self?.delegate?.requestNewPin()
                }, failure: { [weak self] error in
                    if let error = error as? ServiceError, error.code == 404 {
                        self?.delegate?.presenteCodeError()
                        return
                    }
                    self?.delegate?.present(error: error)
                }, completion: { [weak self] in
                    self?.delegate?.endLoading()
            }))
            
            
        }, failure: { [weak self] error in
            self?.delegate?.present(error: error)
            self?.delegate?.endLoading()
        }, completion: {
            
        }))
    }
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        if !confirmPIN {
            self.pin = pin
            confirmPIN = true
            
            delegate?.showNewPinController(title: NSLocalizedString("Confirm your new PIN", comment: "Confirm your new PIN navigation title"), isHiddenForgotPin: true, isHiddenCancelButton: true, userResponseDataView: userResponseDataView, pinViewController: pinViewController)
            delegate?.didPinEndLoading(pinViewController: pinViewController)
            return
        }
        else if confirmPIN {
            if self.pin == pin {
                updatePassword(pin: pin, pinViewController: pinViewController)
                return
            }
            confirmPIN = false
            delegate?.didFailConfirmPin(pinViewController: pinViewController, error: PINError.confirmPin, callbackDidFailFinishPin: true)
            delegate?.didPinEndLoading(pinViewController: pinViewController)
            return
        }
    }
    
    func resendCode(recoveryMethod: AccountRecoveryMethod) {
        self.delegate?.startLoading()
        recoveryService.sendSecretCode(for: userResponse, method: recoveryMethod, response: (success: { [weak self] in
            self?.recoveryValidate = true
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    // MARK: - Private function
    
    private func updatePassword(pin: String, pinViewController: PINViewController) {
        guard let unidasToken = unidasToken else { return }
        authUnidasService.update(user: userResponse, unidasToken: unidasToken.token, pin: pin, response: (success: { [weak self] in
            self?.delegate?.didFinishUpdatePin(pinViewController: pinViewController)
            }, failure: { [weak self] error in
                self?.delegate?.didFailConfirmPin(pinViewController: pinViewController, error: error, callbackDidFailFinishPin: true)
            }, completion: { [weak self] in
                self?.delegate?.didPinEndLoading(pinViewController: pinViewController)
        }))
    }
}

