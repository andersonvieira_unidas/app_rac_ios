//
//  StandartCarPickerVehiclePresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import FirebaseAnalytics
import FacebookCore

protocol StandartCarPickerVehicleViewDelegate: class {
    func showVehicleCharacteristics(vehicleCharacteristicDataView: [VehicleCharacteristicDataView])
    func showVehicleCharacteristics(vehicleCategory: String)
    func showProtectionsAndEquipments(for quotations: QuotationsDataView)
    func showUnavailableMessageForQuotation()
}

protocol StandartCarPickerVehiclePresenterDelegate {
    var delegate: StandartCarPickerVehicleViewDelegate? { get set }
    var numberOfGarages: Int { get }
    func quotationResume(at index: Int) -> QuotationResumeDataView
    func didTapCharacteristic(at index: Int)
    func didSelectQuotations(at index: Int)
    func logAddToWishlistEvent(group: String, groupDescription: String, currency: String, price: Double)
}

class StandartCarPickerVehiclePresenter: StandartCarPickerVehiclePresenterDelegate {
    weak var delegate: StandartCarPickerVehicleViewDelegate?
    private var quotationDataView: [QuotationsDataView]
    private var firebaseAnalyticsService: FirebaseAnalyticsService
    private var adjustAnalyticsService: AdjustAnalyticsService
    
    var numberOfGarages: Int {
        return quotationDataView.count
    }
    
    init(quotationDataView: [QuotationsDataView],
         firebaseAnalyticsService: FirebaseAnalyticsService,
         adjustAnalyticsService: AdjustAnalyticsService) {
        self.quotationDataView = quotationDataView
        self.firebaseAnalyticsService = firebaseAnalyticsService
        self.adjustAnalyticsService = adjustAnalyticsService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func quotationResume(at index: Int) -> QuotationResumeDataView {
        var quotationResume = QuotationResume()
        quotationResume.quotation = quotationDataView[index].model
        return QuotationResumeDataView(model: quotationResume)
    }
    
    func didTapCharacteristic(at index: Int) {
        guard let quotation = quotationResume(at: index).quotation else { return }
        
        if let vehicleCategory = quotation.model.vehicle?.vehicleCategory {
            self.delegate?.showVehicleCharacteristics(vehicleCategory: vehicleCategory)
            
            var parameters: [String : Any] = [:]
            parameters[AnalyticsEvents.showCharacteristic.name] = true
            firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.showCharacteristic, withValue: parameters)
        }
        //self.delegate?.showVehicleCharacteristics(vehicleCharacteristicDataView: quotation.characteristsDataView)
    }
    
    func didSelectQuotations(at index: Int) {
        guard let dataView = self.quotationResume(at: index).quotation,
            let status = dataView.model.status, status == .available else {
                delegate?.showUnavailableMessageForQuotation()
                return
        }
        sendEvent(at: index)
        delegate?.showProtectionsAndEquipments(for: dataView)
    }
    
    //MARK: - Facebook Events
    func logAddToWishlistEvent(group: String, groupDescription: String, currency: String, price: Double) {
        addWishEventFacebook(group: group, groupDescription: groupDescription, currency: currency, price: price)
        addWishEventAdjust(group: group, groupDescription: groupDescription, currency: currency, price: price)
    }
    
    private func addWishEventFacebook(group: String, groupDescription: String, currency: String, price: Double){
        guard let currentUser = currentUser else { return }
        guard let enviroment = AppPersistence.getValue(withKey: "enviroment") else {return}
        
        var parameters = [
            AppEvents.ParameterName.contentID.rawValue: currentUser.account.documentNumberHash ?? ""
            ] as [String : Any]
        
        parameters["currency"] = currency
        parameters["group"] = group
        parameters["groupDescription"] = groupDescription
        parameters["enviroment"] = enviroment
        
        AppEvents.logEvent(.addedToWishlist, valueToSum: price, parameters: parameters)
    }
    
    private func addWishEventAdjust(group: String, groupDescription: String, currency: String, price: Double){
        
        guard let currentUser = currentUser else { return }
 
        var parameters = [
            "user": currentUser.account.documentNumberHash ?? ""
            ] as [String : String]
        
        parameters["group"] = group
        parameters["groupDescription"] = groupDescription
        
        adjustAnalyticsService.addEvent(withEvent: .addToWishList, withValue: parameters, amount: price, currency: currency)
    }
    
    private func sendEvent(at index: Int){
        guard let dataView = self.quotationResume(at: index).quotation else { return }
        var parameters: [String : String] = [:]
        parameters[AnalyticsParameterCurrency] = "BRL"
        parameters[AnalyticsParameterItemCategory] = dataView.model.vehicle?.vehicleCategory
        parameters[AnalyticsParameterItemID] = dataView.model.vehicle?.code
        parameters[AnalyticsParameterItemName] = dataView.model.vehicle?.name
        parameters[AnalyticsParameterNumberOfPassengers] = dataView.model.vehicle?.passengerQuantity
        
        if let quantity = dataView.model.vehicleCharge?.quantity {
            parameters[AnalyticsParameterQuantity] = String(format:"%f", quantity)
        }
        
        if let price = dataView.model.vehicleCharge?.unitCharge {
            parameters[AnalyticsParameterPrice] = String(format:"%.1f", price)
        }
        
        if let prePayment = dataView.model.vehicleCharge?.unitPrePayment {
            parameters["pre_payment"] = String(format:"%.1f", prePayment)
        }
        
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEventViewItem, withValue: parameters)
    }
}
