//
//  MoreHeaderLoginOnPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol HeaderLoginOnViewDelegate: class {
    func showCurrentUserImage(imageUrl: URL?)
    func updateStatusUser(invalid: Bool?)
    func endLoading()
    func present(error: Error)
    
}

protocol HeaderLoginOnPresenterDelegate {
    var delegate: HeaderLoginOnViewDelegate? { get set }
    func viewDidLoad()
}

class HeaderLoginOnPresenter: HeaderLoginOnPresenterDelegate {
    weak var delegate: HeaderLoginOnViewDelegate?
    
    private var unidasUserStatusService: UnidasUserStatusService
    
    init(unidasUserStatusService: UnidasUserStatusService) {
        self.unidasUserStatusService = unidasUserStatusService
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeImage(_ :)), name: .UNDidChangeProfileImage, object: nil)
    }
    
    func viewDidLoad() {
        
        if let currentUser = session.currentUser {
            
            unidasUserStatusService.checkUserDocuments(documentNumber: currentUser.account.documentNumber, response: (success: { [weak self] usersDocumentsUser in
                if let userStatus = usersDocumentsUser.userCheck {
                    
                    let userCheckDataView = UserInValidateDataView(model: userStatus)
                    
                    if userCheckDataView.userHasPendencies {
                        self?.delegate?.updateStatusUser(invalid: true)
                        
                    } else {
                        self?.delegate?.updateStatusUser(invalid: false)
                    }
                }
                
                }, failure: { error in
                    self.delegate?.present(error: error)
                    self.delegate?.updateStatusUser(invalid: nil)
            }, completion: {
                self.delegate?.endLoading()
            }))
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didChangeImage(_ notification: Notification) {
        self.delegate?.showCurrentUserImage(imageUrl: notification.object as? URL)
    }
}
