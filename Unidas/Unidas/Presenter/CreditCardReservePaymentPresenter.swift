//
//  CreditCardReservePaymentPresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum CreditCardReservePaymentError: LocalizedError {
    case securityCodeRequired, amountNotEmpty, noReserveCardFound, installmentsOptionsRequired, invalidCard, TermNotAccepted
    
    var errorDescription: String? {
        switch self {
        case .amountNotEmpty:
            return NSLocalizedString("Amount not empty", comment: "Amount not empty error message")
        case .noReserveCardFound:
            return NSLocalizedString("No reserve card found", comment: "No reserve card error message")
        case .installmentsOptionsRequired:
            return NSLocalizedString("Installments option is required", comment: "Installments option is required error message")
        case .invalidCard:
            return NSLocalizedString("Invalid card", comment: "Invalid card error message")
        case .TermNotAccepted:
            return NSLocalizedString("Term not accepted", comment: "Term not accepted error message")
        case .securityCodeRequired:
            return NSLocalizedString("Reserve security code is required", comment: "Reserve security code is required message")
        }
    }
}

protocol CreditCardReservePaymentViewDelegate: class {
    func startLoadingInstallments()
    func endLoadingInstallments()
    func present(error: Error)
    
    func showInstallments(value: String)
    func showCard(listCardDataView: ListCardDataView)
    func applyValidCard()
    func applyInvalidCards()
    
    var securityCode: String? { get }
}

protocol CreditCardReservePaymentPresenterDelegate {
    var delegate: CreditCardReservePaymentViewDelegate? { get set }
    
    var numberOfInstallments: Int { get }
    
    func setCard(at listCardDataView: ListCardDataView)
    func setInstallments(at amount: Double)
    
    func titleForRow(at row: Int) -> String
    func didSetInstallments(at row: Int)
    
    var creditCardReservePayment: CreditCardReservePaymentDataView? { get }
    var hasCard: Bool { get }
}

class CreditCardReservePaymentPresenter: CreditCardReservePaymentPresenterDelegate {
    weak var delegate: CreditCardReservePaymentViewDelegate?
    private var paymentService: PaymentService
    private var installments: Installments?
    private var amount: Double?
    private var card: ListCard?
    private var selectedInstallments: Int?
    
    var hasCard: Bool { return card != nil }
    
    var numberOfInstallments: Int {
        guard let installments = installments?.installments, let installmentsInt = Int(installments) else { return 0 }
        return installmentsInt
    }
    
    var creditCardReservePayment: CreditCardReservePaymentDataView? {
        guard let securityCode = delegate?.securityCode else {
            self.delegate?.present(error: CreditCardReservePaymentError.securityCodeRequired)
            return nil
        }
        
        if securityCode.isEmpty {
            self.delegate?.present(error: CreditCardReservePaymentError.securityCodeRequired)
            return nil
        }
        
        guard let card = card else {
            self.delegate?.present(error: CreditCardReservePaymentError.noReserveCardFound)
            return nil
        }

        if !CardUtils.isValid(expiration: card.val, for: Date()) {
            self.delegate?.present(error: CreditCardReservePaymentError.invalidCard)
        }
        
        guard let selectedInstallments = selectedInstallments else {
            self.delegate?.present(error: CreditCardReservePaymentError.installmentsOptionsRequired)
            return nil
        }
        
        guard let amount = amount else {
            self.delegate?.present(error: CreditCardReservePaymentError.amountNotEmpty)
            return nil
        }
        
        return CreditCardReservePaymentDataView(model: CreditCardReservePayment(card: card, amount: amount, securityCode: securityCode, installmentsOption: selectedInstallments))
    }
    
    init(paymentService: PaymentService) {
        self.paymentService = paymentService
    }
    
    func setCard(at listCardDataView: ListCardDataView) {
        self.card = listCardDataView.model
        
        self.delegate?.showCard(listCardDataView: listCardDataView)
        (listCardDataView.isValid) ? self.delegate?.applyValidCard() : self.delegate?.applyInvalidCards()
    }
    
    func setInstallments(at amount: Double) {
        self.delegate?.startLoadingInstallments()
        
        self.amount = amount
        
        paymentService.getInstallments(of: String(amount), response:  (success: { [weak self] installments in
            self?.installments = installments.first
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoadingInstallments()
        })
        )
    }
    
    func didSetInstallments(at row: Int) {
        self.selectedInstallments = row + 1
        self.delegate?.showInstallments(value: titleForRow(at: row))
    }
    
    func titleForRow(at row: Int) -> String {
        guard let amount = amount else { return "" }
        
        let numberOfInstallments = String(row+1)
        let amountInstallments = amount / Double(row + 1)
        
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: amountInstallments)) else { return "" }
        
        return "  \(String(numberOfInstallments))x \(amountString)"
    }
}
