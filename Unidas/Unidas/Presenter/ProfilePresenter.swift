//
//  ProfilePresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 21/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

protocol ProfileViewDelegate: class {
    func showLoggedView()
    func showNotLogggedView()
}

protocol ProfilePresenterDelegate {
    var delegate: ProfileViewDelegate? { get set }
    func checkLogged()
}

class ProfilePresenter: ProfilePresenterDelegate {
    weak var delegate: ProfileViewDelegate?
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(userDidLogout), name: .UNUserDidLogout, object: nil)
    }

    @objc func userDidLogout(){
        delegate?.showNotLogggedView()
    }
    func checkLogged() {
        if let currentUser = session.currentUser,
            let userUnidasSession = currentUser.unidas?.first,
            userUnidasSession.clidoc != nil {
            delegate?.showLoggedView()
        } else {
            delegate?.showNotLogggedView()
        }
    }
}
