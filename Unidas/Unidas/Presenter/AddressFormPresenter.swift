//
//  AddressFormPresenter.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 16/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum AddressFormError: LocalizedError {
    case zipCodeNotFound
    case numberRequired
    case residencialPhoneRequired
    case neighborhoodRequired
    case streetRequired
    
    var errorDescription: String? {
        switch self {
        case .zipCodeNotFound:
            return NSLocalizedString("Logradouro não encontrado para o C.E.P.informado.", comment: "Logradouro não encontrado para o C.E.P.informado description")
        case .numberRequired:
            return NSLocalizedString("House number required.", comment: "House number required description")
        case .neighborhoodRequired:
            return NSLocalizedString("Neighborhood required.", comment: "Neighborhood required description")
        case .streetRequired:
            return NSLocalizedString("Street required.", comment: "Street required description")
        case .residencialPhoneRequired:
            return NSLocalizedString("Residencial phone required.", comment: "Residencial phone required description")
        }
    }
}

protocol AddressFormViewDelegate: class {
    func startLoadingPostalCode()
    func endLoadingPostalCode()
    func setAddress(_ address: ZipcodeDataView, formsAction: FormsAction)
    func present(error: Error)
    func clearAddress()
    
    var neighborhood: String? { get }
    var street: String? { get }
    var number: String? { get }
    var complement: String? { get }
    var residencialPhone: String? { get }
    var zipCode: String? { get }
    
    func zipCodeError(isOn: Bool)
    func neighborhoodError(isOn: Bool)
    func cityError(isOn: Bool)
    func streetError(isOn: Bool)
    func numberError(isOn: Bool)
    func complementError(isOn: Bool)
    func phoneError(isOn: Bool)
}

protocol AddressFormPresenterDelegate {
    var delegate: AddressFormViewDelegate? { get set }
    var isUserValid: Bool { get }
    var addressFormDataView: AddressFormDataView? { get }

    func didFillPostalCodeField(_ postalCode: String, formsAction: FormsAction)
}

class AddressFormPresenter: AddressFormPresenterDelegate {
    weak var delegate: AddressFormViewDelegate?
    
    private let zipcodeService: ZipcodeService
    private var zipcode: Zipcode?
    
    init(zipcodeService: ZipcodeService) {
        self.zipcodeService = zipcodeService
    }
    
    var addressFormDataView: AddressFormDataView? {
        guard isUserValid else { return nil }
        
        guard let neighborhood = delegate?.neighborhood, let street = delegate?.street, let number = delegate?.number, let zipcode = zipcode else { return nil }

        return AddressFormDataView(model: AddressForm(neighborhood: neighborhood, street: street, number: number, complement: delegate?.complement, residencialPhone: delegate?.residencialPhone, zipcode: zipcode))
    }
    
    var isUserValid: Bool {
        
        var zipcodeStatus = true
        var neighborhoodStatus = true
        var streetStatus = true
        var numberStatus = true
        
//        guard let _ = zipcode else { return false }
        
        let charset = CharacterSet(charactersIn: " ")
        
        guard let zipCode = delegate?.zipCode else { return false}
        
        if zipCode.isEmpty {
            self.delegate?.zipCodeError(isOn: true)
            zipcodeStatus = false
            //return false
        }else{
            self.delegate?.zipCodeError(isOn: false)
            zipcodeStatus = true
        }
        
        guard let neighborhood = delegate?.neighborhood, let street = delegate?.street?.trimmingCharacters(in: charset), let number = delegate?.number, var residencialPhone = delegate?.residencialPhone  else { return false }
        
        if neighborhood.isEmpty {
            self.delegate?.neighborhoodError(isOn: true)
            neighborhoodStatus = false
//            self.delegate?.present(error: AddressFormError.neighborhoodRequired)
//            return false
        }else{
            self.delegate?.neighborhoodError(isOn: false)
            neighborhoodStatus =  true
        }
        
        if street.isEmpty {
            self.delegate?.streetError(isOn: true)
            streetStatus = false
//            self.delegate?.present(error: AddressFormError.streetRequired)
//            return false
        }else{
            self.delegate?.streetError(isOn: false)
            streetStatus = true
        }
        
        if number.isEmpty {
            self.delegate?.numberError(isOn: true)
            numberStatus = false
//            self.delegate?.present(error: AddressFormError.numberRequired)
//            return false
        }else{
            self.delegate?.numberError(isOn: false)
            numberStatus = true
        }
        
        if residencialPhone != ""{
            if !residencialPhone.isValidPhoneNumber {
                //self.delegate?.phoneError(isOn: true)
//                self.delegate?.present(error: AddressFormError.residencialPhoneRequired)
//                return false
            }else{
                //self.delegate?.phoneError(isOn: false)
            }
        }else{
            residencialPhone = "+55"
            //self.delegate?.phoneError(isOn: false)
        }
        
        if zipcodeStatus == true && neighborhoodStatus == true && streetStatus == true && numberStatus == true {
            return true
        }
        return false
    }
    
    func didFillPostalCodeField(_ postalCode: String, formsAction: FormsAction) {
        let postalCode = postalCode.removeZipcodeCharacters
        
        self.delegate?.startLoadingPostalCode()
        zipcodeService.fetch(for: postalCode, response: (success: { [weak self] zipcode in
            guard let zipcode = zipcode.first else { return }
            self?.delegate?.setAddress(ZipcodeDataView(model: zipcode), formsAction: formsAction)
            self?.zipcode = zipcode
            }, failure: { [weak self] error in
                if formsAction != .update {
                    self?.zipcode = nil
                    self?.delegate?.clearAddress()
                }

                if let error = error as? ServiceError, error.code == 404 {
                    self?.delegate?.present(error: AddressFormError.zipCodeNotFound)
                    return
                }
                
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                self?.delegate?.endLoadingPostalCode()
        }))
    }
    
}
