//
//  MainPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/07/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation


protocol MainViewDelegate: class {
    func notFoundReserve()
    func startLoading()
    func endLoading()
    func present(error: Error)
    func showDetails(vehicleReservationsDataView: VehicleReservationsDataView, rulesDataView: RulesDataView)
}

protocol MainPresenterDelegate {
    var delegate: MainViewDelegate? { get set }
    
    func fetchReservation(reservation: String)
}

class MainPresenter: MainPresenterDelegate {
    
    weak var delegate: MainViewDelegate?
    private var reservationUnidasService: ReservationUnidasService
    
    
    init(reservationUnidasService: ReservationUnidasService) {
        self.reservationUnidasService = reservationUnidasService
    }
    
    func fetchReservation(reservation: String) {
        delegate?.startLoading()
        
        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
        
        reservationUnidasService.get(by: reservation, documentNumber: documentNumber, response: (success: { [weak self] myReservationDetail in
            
            guard let pendencies = myReservationDetail.pendencies, let userAvailableToCheckin = myReservationDetail.userAvailableToCheckin, let vehicleReservations = myReservationDetail.reservation else {
                self?.delegate?.notFoundReserve()
                return
            }
            
            let vehicleReservation = VehicleReservationsDataView(model: vehicleReservations)
            //TODO - incluir pendencia financeira
            let rules = RulesDataView(pendencies: pendencies, userAvailableToCheckin: userAvailableToCheckin, financialPendency: false)
            self?.delegate?.showDetails(vehicleReservationsDataView: vehicleReservation, rulesDataView: rules)
            
            }, failure: { error in
                if let error = error as? ServiceError, error.code == 404 {
                    self.delegate?.notFoundReserve()
                    return
                }
                self.delegate?.present(error: error)
        }, completion: {
            self.delegate?.endLoading()
            
        })
        )
    }
}
