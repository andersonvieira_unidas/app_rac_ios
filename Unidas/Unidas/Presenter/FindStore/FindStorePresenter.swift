//
//  FindStorePresenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 31/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import CoreLocation

protocol FindStoreViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func present(error: String)
    func askPermissionToSettings()
    
    func showCurrentLocation(locationDataView: LocationDataView, fetchStore: Bool, filterExpress: Bool)
    func showStores(garageDataView: [GarageDataView])
    
    func showGarageDetails(garageDataView: GarageDataView, locationDataView: LocationDataView?)
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String])
    func openAppleMaps(coordinate: CLLocationCoordinate2D)
    func showAvailableHours(availableHours: OpeningHours)
    func showStandartCar(garageDataView: GarageDataView)
    func setNearGarage(location: Location)
    func checkForExpressAvailability(pendencie: Bool, financialPendencie: Bool)
    func reloadMap()
}

protocol FindStorePresenterDelegate {
    var delegate: FindStoreViewDelegate? { get set }
    var phoneNumber: String? { get }
    var currentLocationDataView: LocationDataView? { get }
    var currentGarage: Garage?  { get set }
    func currentUserLocation(fetchStore: Bool, filterExpress: Bool)
    
    func fetchStores(nearby location: LocationDataView, immediately: Bool, filterExpress: Bool)
    func didSelectAnnotation(coordinate: CLLocationCoordinate2D)
    func didTapOpenMaps()
    func didTapOpenReserve()
    func didTapOnAvailableStoreHours()
    func compareAppVersionToUpdate() -> Bool
    func fetchStatusCliente()
    func removeExpressStores()
}

class FindStorePresenter: FindStorePresenterDelegate {
    
    weak var delegate: FindStoreViewDelegate?
    
    private var locationManager: LocationManager
    private var performDataRequest: Timer? = nil
    private var unidasGarageService: UnidasGarageService
    private var garages: [Garage] = []
    var currentGarage: Garage? = nil
    var currentLocationDataView: LocationDataView?
    
    init(locationManager: LocationManager, unidasGarageService: UnidasGarageService) {
        self.locationManager = locationManager
        self.unidasGarageService = unidasGarageService
    }
    
    var phoneNumber: String? {
        guard let phoneNumber = currentGarage?.address?.phoneNumber else { return nil }
        return phoneNumber
    }
    
    func compareAppVersionToUpdate() -> Bool {
        let compared = VersionControl.compareAppVersion()
        return compared
    }
    
    func currentUserLocation(fetchStore: Bool, filterExpress: Bool) {
        self.delegate?.startLoading()
        locationManager.requestCurrentLocation { [weak self] (currentLocation, error) in
            self?.delegate?.endLoading()
            if let currentLocation = currentLocation {
                let location = LocationDataView(location: currentLocation)
                
                self?.currentLocationDataView = location
                self?.delegate?.showCurrentLocation(locationDataView: location, fetchStore: fetchStore, filterExpress: filterExpress)
            } else if let error = error {
                if let locationError = error as? LocationManagerError, locationError == .deniedAuthorization {
                   self?.delegate?.askPermissionToSettings()
                } else if let _ = error as? CLError {
                    let localizedString = NSLocalizedString("Localization Not Found", comment: "")
                    self?.delegate?.present(error: localizedString)
                } else {
                    self?.delegate?.present(error: error)
                }
            }
        }
    }
    
    func removeExpressStores() {
        let garageFilter = self.garages.filter { $0.type != .express}
        self.garages = garageFilter
        self.order(by: currentLocationDataView)
        self.delegate?.reloadMap()
        
        if  let firstGarage = self.garages.first, let location = firstGarage.address?.location {
            self.delegate?.setNearGarage(location: location)
        }
    }
    
    func fetchStores(nearby location: LocationDataView, immediately: Bool, filterExpress: Bool) {
        performDataRequest?.invalidate()
        
        let fetchBlock = { [weak self] in
            self?.delegate?.startLoading()
            self?.unidasGarageService.fetchGarages(response: (success: { [weak self] garages in
                
                let garagesFiltered = garages.filter({ (garage) -> Bool in
                    guard let _ = garage.address?.location else { return false }
//                    if filterExpress {
//                        let result = garage.type != .express ? true : false
//                        return result
//                    }
                    return true
                })
      
                self?.garages = garagesFiltered
                self?.order(by: location)
                }, failure: { [weak self] error in
                    self?.delegate?.present(error: error)
                }, completion: { [weak self] in
                    if  let firstGarage = self?.garages.first,
                        let location = firstGarage.address?.location {
                        self?.delegate?.setNearGarage(location: location)
                    }
                    self?.delegate?.endLoading()
                    
                })
            )
        }
        
        if immediately {
            fetchBlock()
        }
        else {
            performDataRequest = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (timer) in
                fetchBlock()
            })
        }
    }
    
    func didSelectAnnotation(coordinate: CLLocationCoordinate2D) {
        guard let garage = garages.filter({$0.address?.location?.coordinate == coordinate}).first else {
            return
        }
        
        self.currentGarage = garage
        delegate?.showGarageDetails(garageDataView: GarageDataView(model: garage), locationDataView: currentLocationDataView)
    }
    
    func didTapOpenMaps() {
        guard let currentGarage = currentGarage else { return }
        
        let garageDataView = GarageDataView(model: currentGarage)
        
        guard let coordinate = garageDataView.address?.location?.coordinate else {
            return
        }

        validateMapAppsForCoordinate(coordinate: coordinate)
    }
    
    func didTapOpenReserve() {
        guard let currentGarage = currentGarage else { return }
        //TODO - checar se o usuário tem pendencia
        
        self.delegate?.showStandartCar(garageDataView: GarageDataView(model: currentGarage))
    }
    
    func didTapOnAvailableStoreHours() {
        guard let currentGarage = currentGarage else { return }
        guard let openingHours = currentGarage.openingHours as? OpeningHours else {return}
        self.delegate?.showAvailableHours(availableHours: openingHours)
    }
    
    func fetchStatusCliente() {
        self.delegate?.startLoading()
        
        guard let currentUser = session.currentUser else {
            self.delegate?.checkForExpressAvailability(pendencie: false, financialPendencie: false)
            self.delegate?.endLoading()
            return
        }
        
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        unidasGarageService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            if let userStatus = usersDocumentsUser.userCheck {
                
                let mobilePhoneIsValid = userStatus.mobilePhoneIsValid ?? false
                let emailIsValid = userStatus.emailIsValid ?? false
                let serasaIsValid = userStatus.serasaIsValid ?? false
                let credDefenseIsValid = userStatus.credDefenseIsValid ?? false
                let documentLicenceIsValid = userStatus.documentLicenceIsValid ?? false
                let signatureIsValid = userStatus.signatureIsValid ?? false
                let accountIsValid = userStatus.accountIsValid ?? false
                let paymentIsValid = userStatus.paymentIsValid ?? false
                let selfieIsValid = userStatus.selfieIsValid ?? false
                let financialPendencie = userStatus.financialPendencie ?? false
                var pendencie = false
                
                if mobilePhoneIsValid == false || emailIsValid == false || serasaIsValid == false || credDefenseIsValid == false || documentLicenceIsValid == false || signatureIsValid == false || accountIsValid == false || paymentIsValid == false || selfieIsValid == false {
                    pendencie = true
                }
                
                self?.delegate?.checkForExpressAvailability(pendencie: pendencie, financialPendencie: financialPendencie)
                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            
        }))
    }
    
    private func validateMapAppsForCoordinate(coordinate: CLLocationCoordinate2D) {
        var mapAppsURL: [URL] = []
        var appsName: [String] = []
        
        if let googleMaps = ValidateMaps.validateGoogleMaps(coordinate: coordinate) {
            appsName.append("Google Maps")
            mapAppsURL.append(googleMaps)
        }
        
        if let waze = ValidateMaps.validateWaze(coordinate: coordinate) {
            appsName.append("Waze")
            mapAppsURL.append(waze)
        }
        
        if mapAppsURL.count > 0 {
            self.delegate?.showAlertMaps(coordinate: coordinate, maps: mapAppsURL, appsName: appsName)
        }
        else {
            self.delegate?.openAppleMaps(coordinate: coordinate)
        }
    }
    
    private func order(by location: LocationDataView?) {
        guard let location = location else {
            return
        }
        
        let garageDataView = garages.map{ GarageDataView(model: $0) }
        var garageDataViewFiltered = garageDataView.filter { (garageDataView) -> Bool in
            return garageDataView.address?.location?.clLocationCoordinate != nil
        }
 
        garageDataViewFiltered.sort { (a, b) -> Bool in
            guard let firstDistance = a.address?.calculeDistance(at: location), let secondDistance = b.address?.calculeDistance(at: location) else { return false }
            return firstDistance < secondDistance
        }
        
        self.garages = garageDataViewFiltered.map({$0.model})
        self.delegate?.showStores(garageDataView: garageDataViewFiltered)
    }
}
