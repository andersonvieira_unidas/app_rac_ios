//
//  GarageViewPresenter.swift
//  Unidas
//
//  Created by Anderson Vieira on 31/07/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseAnalytics
//import FacebookCore


protocol GaragesFindStoreViewDelegate: class {
    func startLoading()
    func endLoading()
    func reloadData()
    func present(error: Error)
    func showEmptyResultsView()
}

protocol GaragesFindStoreViewPresenterDelegate {
    var delegate: GaragesFindStoreViewDelegate? { get set }
    var numberOfGarages: Int { get }
    var currentLocation: LocationDataView? { get }
    
    func fetchCurrentLocation()
    func searchGarages(with filter: String?)
    func searchButtonTapped(with filter: String?)
    func garage(at index: Int) -> GarageDataView
}

class GaragesFindStoreViewPresenter: GaragesFindStoreViewPresenterDelegate {

    weak var delegate: GaragesFindStoreViewDelegate?
    private var garages: [Garage] = []
    private var garageService: UnidasGarageService
    private var firebaseAnalytics: FirebaseAnalyticsService
    private var currentRequest: DataRequest? = nil
    private var requestTimer: Timer? = nil
    private var locationManager: LocationManager
    var currentLocation: LocationDataView? = nil
    
    init(garageService: UnidasGarageService,
         firebaseAnalytics: FirebaseAnalyticsService,
         locationManager: LocationManager) {
        self.garageService = garageService
        self.firebaseAnalytics = firebaseAnalytics
        self.locationManager = locationManager
    }
    
    var numberOfGarages: Int {
        return garages.count
    }
    
    func fetchCurrentLocation() {
        locationManager.requestCurrentLocation { [weak self] (currentLocation, error) in
            if let currentLocation = currentLocation {
                let location = LocationDataView(location: currentLocation)
                self?.currentLocation = location
                self?.fetchGarages()
            }
            else {
                self?.fetchGarages(with: nil)
            }
        }
    }
    
    func garage(at index: Int) -> GarageDataView {
        return GarageDataView(model: garages[index])
    }
    
    func searchGarages(with filter: String?) {
        guard let count = filter?.count, count > 3 else { return }
        currentRequest?.cancel()
        requestTimer?.invalidate()
        requestTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] (timer) in
            timer.invalidate()
            self?.fetchGarages(with: filter)
            self?.requestTimer = nil
        }
    }
    
    func searchButtonTapped(with filter: String?) {
        guard let count = filter?.count, count > 3 else { return }
        currentRequest?.cancel()
        requestTimer?.invalidate()
        fetchGarages(with: filter)
    }
    
    func fetchGarages(with filter: String?) {
        self.delegate?.startLoading()
        
        currentRequest = garageService.fetchGarages(filter: filter, response: (success: { [weak self] garages in
            let garagesFiltered = garages.filter({ (garage) -> Bool in
                guard let _ = garage.address?.location else { return false }
                return true
            })
            
            self?.garages = garagesFiltered
  
            self?.order(by: self?.currentLocation)
            }, failure: { [weak self] error in
                if (error as NSError).code == NSURLErrorCancelled { return }
                if let error = error as? ServiceError, error.code == 404 {
                    self?.garages = []
                    return
                }
                self?.delegate?.present(error: error)
            }, completion: { [weak self] in
                if self?.garages.isEmpty ?? true {
                    self?.delegate?.reloadData()
                    self?.delegate?.showEmptyResultsView()
                }
                
                self?.delegate?.endLoading()
        }))
        
        if let term = filter {
            firebaseAnalytics.addEvent(withEvent: AnalyticsEventSearch, withValue: ["term" : term])
            
        }
    }
    
    // MARK: - Private function
    
    private func fetchGarages() {
        currentRequest?.cancel()
        requestTimer?.invalidate()
        requestTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] (timer) in
            timer.invalidate()
            self?.fetchGarages(with: nil)
            self?.requestTimer = nil
        }
    }
    
    private func order(by location: LocationDataView?) {
        guard let location = location else {
            self.delegate?.reloadData()
            return
        }
        
        var garageDataView = garages.map{ GarageDataView(model: $0) }
        
        garageDataView.sort { (a, b) -> Bool in
            guard let firstDistance =  a.address?.calculeDistance(at: location), let secondDistance = b.address?.calculeDistance(at: location) else { return false }
            return firstDistance < secondDistance
        }
        
        self.garages = garageDataView.map({$0.model})
        self.delegate?.reloadData()
    }
}

