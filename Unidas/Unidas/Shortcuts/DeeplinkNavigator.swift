//
//  DeeplinkNavigator.swift
//  Unidas
//
//  Created by Pedro Machado on 15/07/19.
//

import Foundation
import UIKit

class DeeplinkNavigator {
    
    var window: UIWindow?
    
    static let shared = DeeplinkNavigator()
    private init() { }
    
    var alertController = UIAlertController()
    
}
