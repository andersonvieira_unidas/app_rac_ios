//
//  ShortcutParser.swift
//  Unidas
//
//  Created by Pedro Machado on 15/07/19.
//

import Foundation
import UIKit

enum ShortcutKey: String {
    case reservations = "br.com.unidas.apprac.ios.debug.reservations"
    case mycar = "br.com.unidas.apprac.ios.debug.mycar"
    case profile = "br.com.unidas.apprac.ios.debug.profile"
    case alerts = "br.com.unidas.apprac.ios.debug.alerts"
    case home = "br.com.unidas.apprac.ios.debug.home"
    case more = "br.com.unidas.apprac.ios.debug.more"
}

enum ProfileType: String {
    case reservation = "Reservation"
    case mycar = "MyCar"
    case profile = "Profile"
    case alerts = "Alerts"
    case home = "Home"
    case more = "More"
}

class ShortcutParser {
    static let shared = ShortcutParser()
    private init() { }
    
    func registerShortcuts(for profileType: ProfileType) {
        let activityIcon = UIApplicationShortcutIcon(templateImageName: "ico-alugar-on")
        let activityShortcutItem = UIApplicationShortcutItem(type: ShortcutKey.reservations.rawValue, localizedTitle: NSLocalizedString("Reservations", comment: ""), localizedSubtitle: nil, icon: activityIcon, userInfo: nil)
        
//        let messageIcon = UIApplicationShortcutIcon(templateImageName: "ico-alugar-on")
//        let messageShortcutItem = UIApplicationShortcutItem(type: ShortcutKey.mycar.rawValue, localizedTitle: NSLocalizedString("My Car", comment: ""), localizedSubtitle: nil, icon: messageIcon, userInfo: nil)
        
        let newListingIcon = UIApplicationShortcutIcon(templateImageName: "ico-profile")
        let newListingShortcutItem = UIApplicationShortcutItem(type: ShortcutKey.profile.rawValue, localizedTitle: NSLocalizedString("Profile", comment: ""), localizedSubtitle: nil, icon: newListingIcon, userInfo: nil)
        
        let alertsIcon = UIApplicationShortcutIcon(templateImageName: "icon-messages")
        let alertsShortcutItem = UIApplicationShortcutItem(type: ShortcutKey.alerts.rawValue, localizedTitle: NSLocalizedString("Notifications", comment: ""), localizedSubtitle: nil, icon: alertsIcon, userInfo: nil)

        let moreIcon = UIApplicationShortcutIcon(templateImageName: "ico-more")
        let moreShortcutItem = UIApplicationShortcutItem(type: ShortcutKey.more.rawValue, localizedTitle: NSLocalizedString("More", comment: ""), localizedSubtitle: nil, icon: moreIcon, userInfo: nil)
        
        UIApplication.shared.shortcutItems = [alertsShortcutItem, activityShortcutItem, newListingShortcutItem, moreShortcutItem ]
    }
}
