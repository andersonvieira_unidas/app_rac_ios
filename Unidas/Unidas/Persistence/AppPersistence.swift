//
//  AppPersistence.swift
//  Unidas
//
//  Created by Anderson on 12/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

class AppPersistence {
    
    static let name = "name"
    static let documentNumber = "documentNumber"
    static let birthday = "birthday"
    static let email = "email"
    static let mobilePhone = "mobilePhone"
    static let motherName = "motherName"
    static let postalCode = "postalCode"
    static let uf = "uf"
    static let localidade = "localidade"
    static let bairro = "bairro"
    static let logradouro = "logradouro"
    static let idLocalidade = "idLocalidade"
    static let idUf = "idUf"
    static let nmLogradouroTipo = "nmLogradouroTipo"
    static let nmLogradouroAbreviacao = "nmLogradouroAbreviacao"
    static let logradouroTipo = "logradouroTipo"
    static let number = "number"
    static let complement = "complement"
    static let phone = "phone"
    static let pin = "pin"
    static let token = "token"
    static let pendencies = "pendencies"
    static let fcmToken = "fcmToken"
    static let postNotificationPermission = "postNotificationPermission"
    
    static func save(key: String, value: Any){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func getValue(withKey key: String)-> String? {
        return UserDefaults.standard.value(forKey: key) as? String
    }
    
    static func getValueBool(withKey key: String)-> Bool? {
        return UserDefaults.standard.bool(forKey: key) 
    }
    
    static func clearData(withKey key: String){
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    typealias Return = (success: () -> Void, failure: (Error) -> Void, completion: () -> Void)
}
