//
//  FirebaseRealtimeDatabase.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

struct ToggleProfile {
    static var model : ToggleMenu?
}

enum Toggle: String {
    case newReservation = "new_reservation"
    case changeReservation = "change_reservation"
    case checkinReservation = "checkin_reservation"
    case payAtPickup = "pay_at_pickup"
    case payInAdvance = "pay_in_advance"
    case appleWallet = "apple_wallet"
    case creditCardOcr = "credit_card_ocr"
    case doPayment = "do_payment"
    case maintenance = "maintenance"
    case version = "version"
    case appReviewTrySession = "app_review_try_session"
    case rateHome = "rating_home"
    case reteReserve = "rating_reserve"
    case expressStores = "express_stores"
    case doPaymentExpress = "do_payment_express"
}

class FirebaseRealtimeDatabase {
    
    static func fetchAppVersion(withKey key: Toggle, completionHandler: @escaping ((_ version : String) -> Void)) {
        
        var enviroment = "config_ios_prd"
        #if DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
       let ref =  Database.database().reference()
        
       ref.child("menu").child(enviroment).child(key.rawValue).observe(.value, with: { snapshot in
           
        if let value = snapshot.value as? String{
            completionHandler(value)
        }else{
            let remoteConfigVersion = RemoteConfigUtil.recoverParameterValue(with: "app_version_ios")
            completionHandler(remoteConfigVersion)
        }
                        
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    static func fetchAppReviewCount(withKey key: Toggle, completionHandler: @escaping ((_ version : String) -> Void)) {
        
        var enviroment = "config_ios_prd"
        #if DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
       let ref =  Database.database().reference()
        
        ref.child("menu").child(enviroment).child(key.rawValue).observeSingleEvent(of: .value, with: { snapshot in
           
        if let value = snapshot.value as? String{
            completionHandler(value)
        }else{
            completionHandler("5")
        }
                        
        }) { (error) in
            print(error.localizedDescription)
        }
    }

    static func toggleValue(withKey key: Toggle, completionHandler: @escaping ((_ toggle : Bool) -> Void)) {
        var enviroment = "config_ios_prd"
        #if DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
        let ref =  Database.database().reference()
        
        ref.child("menu").child(enviroment).child(key.rawValue).observeSingleEvent(of: .value, with: { (snapshot) in
            if let value = snapshot.value as? Bool{
                completionHandler(value)
            }else{
                completionHandler(true)
            }
        }){ (error) in
            print(error.localizedDescription)
        }
    }
    
    static func toggleValues(withKeys keys: [Toggle], completionHandler: @escaping ((_ toggle : [Toggle: Bool]) -> Void)) {
        var enviroment = "config_ios_prd"
        #if DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
        let ref =  Database.database().reference()
        var returnValues: [Toggle: Bool] = [:]
        
        ref.child("menu").child(enviroment).observe(.value, with: { (snapshot) in
            for key in keys {
                if let value = snapshot.childSnapshot(forPath: key.rawValue).value as? Bool {
                    returnValues[key] = value
                }
            }
            completionHandler(returnValues)
        }){ (error) in
            print(error.localizedDescription)
        }
            
    }
    
    static func toggleListenerValue(withKey key: Toggle, completionHandler: @escaping ((_ toggle : Bool) -> Void)) {
        var enviroment = "config_ios_prd"
        #if DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
        let ref =  Database.database().reference()
        
        ref.child("menu").child(enviroment).child(key.rawValue).observe(.value, with: { (snapshot) in
            if let value = snapshot.value as? Bool{
                completionHandler(value)
            }else{
                completionHandler(true)
            }
        }){ (error) in
            print(error.localizedDescription)
        }
    }
}
