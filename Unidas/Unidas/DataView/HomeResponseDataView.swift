//
//  File.swift
//  Unidas
//
//  Created by Anderson Vieira on 22/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct HomeResponseDataView {
    let model: Home
    
    var userHasPendencies: Bool {
        return model.pendencies ?? false
    }
    
    var userHasReservation: Bool {
        if let _ = model.lastReservation {
            return true
        }
        return false
    }
    
    var userHasReservationWithCheckin: Bool {
        if let lastReservation = model.lastReservation, model.userAvailableToCheckin ?? false, lastReservation.checkInAvailable ?? false {
            let vehicleReservationsDataView = VehicleReservationsDataView(model: lastReservation)
            if !vehicleReservationsDataView.preAuthorizationPaid && !vehicleReservationsDataView.isFranchise {
                return true
            }
        }
        return false
    }
    
    var reservation: VehicleReservationsDataView? {
        if let lastReservation = model.lastReservation {
            let vehicleReservationDataView = VehicleReservationsDataView(model: lastReservation)
            
            return vehicleReservationDataView
        }
        return nil
    }
}
