//
//  File.swift
//  Unidas
//
//  Created by Anderson Vieira on 09/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct ReservationRulesDataView {
    let userValidated: Bool
    let reservationNumber: String
    let checkinAvailable: Bool
    let reservationPaid: Bool
    let hoursToCheckin: Int?
    let userAvailableToCheckin: Bool
    let preAuthorizationPaid: Bool
    let isFranchise: Bool
    let dueDate: Date?
    let dueTime: Int?
    let express: Bool
    let financialPendency: Bool
}
