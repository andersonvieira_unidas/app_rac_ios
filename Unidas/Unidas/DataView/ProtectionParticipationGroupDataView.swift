//
//  ProtectionParticipationGroupDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 12/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionParticipationGroupDataView {
    
    let model: ProtectionParticipationGroup
    
    var groups: String? {
        return model.groups
    }
    
    var value: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.value))
    }
    
}
