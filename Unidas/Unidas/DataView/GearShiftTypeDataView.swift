//
//  GearShiftTypeDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 30/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct GearShiftTypeDataView {
    
    let model: GearShiftType
    
    var description: String {
        switch model {
        case .automatic: return NSLocalizedString("Automatic", comment: "Automatic gearshift type")
        case .manual: return NSLocalizedString("Manual", comment: "Manual gearshift type")
        }
    }
    
}
