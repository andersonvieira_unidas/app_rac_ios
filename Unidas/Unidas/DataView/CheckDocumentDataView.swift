//
//  CheckDocumentDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CheckDocumentDataView {
    let model:CheckDocument
    
    let dateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter
    }()
    
    var updateMessage: String? {
        guard let validateAt = model.validateAt else { return nil }
        let validateAtFormatted = String(validateAt.prefix(10))
        guard let date = dateFormatter.date(from: validateAtFormatted) else { return nil }

        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "pt-br")
        let componentsFormatter = DateComponentsFormatter()
        componentsFormatter.calendar = calendar
       
        let interval = date.timeIntervalSince(Date())
        componentsFormatter.allowedUnits = [.year, .month, .day]
        componentsFormatter.unitsStyle = .full
        componentsFormatter.maximumUnitCount = 1
        
        guard let dateComponentString = componentsFormatter.string(from: interval) else { return nil }
        
        return String(format: NSLocalizedString("Next update in %@", comment: "Next update in %@ message"), dateComponentString)
    }
    
    var lastUpdateMessage: String? {
        guard let updateAt = model.updateAt else { return nil }
        let updateAtFormatted =  String(updateAt.prefix(10))
        guard let date = dateFormatter.date(from: String(updateAtFormatted)) else { return nil }
        let dateString  = DateFormatter.shortDateFormatterAPI.string(from: date)
        return String(format: NSLocalizedString("Last update %@ ago", comment: "Next update %@ ago message"), dateString)
    }
}
