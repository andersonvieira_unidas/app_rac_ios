//
//  VehicleGroupDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleGroupDataView {
    
    let model: VehicleGroup
    
    var name: String? {
        return model.name
    }
}
