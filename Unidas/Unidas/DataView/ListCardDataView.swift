//
//  ListCardDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

struct ListCardDataView {
    let model: ListCard
    
    var holder: String {
        return model.clinom
    }
    
    var fourLastNumberCard: String {
        let openPan = model.openPan.index(model.openPan.endIndex, offsetBy: -4)
        let numberCard = model.openPan[openPan..<model.openPan.endIndex]
        return String(numberCard)
    }
    
    var cardNumberFormatted: String {
        return "●●●● ●●●● ●●●● \(fourLastNumberCard)"
    }
    
    var cardNumberShortFormatted: String {
        return "●●●● \(fourLastNumberCard)"
    }
    
    var cardDate: String {
        var expDate = model.val
        expDate.insert("/", at: expDate.index(expDate.startIndex, offsetBy: 2))
        
        return expDate
    }
    
    var brandImage: UIImage {
        return CardUtils.findByName(for: model.tknbnddes).brandImage
    }
    
    var isFavoriteCard: Bool {
        return (model.metodoPagamentoCartaoCreditoPreferencial == 1)
    }
    
    var isValid: Bool {
        return CardUtils.isValid(expiration: model.val, for: Date())
    }
}
