//
//  CallCenterDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

struct CallCenterDataView {
    let model: CallCenter
    
    var title: String? {
        return model.title
    }

    var description: String? {
        return model.description
    }

    var image: UIImage? {
        return model.type.image
    }

    var phoneNumber: String? {
        guard let phoneNumber = model.phoneNumber else { return nil }
        return phoneNumber.trimmingCharacters(in: CharacterSet(charactersIn: "\""))
    }

    var formattedPhone: String? {
        guard let phoneNumber = phoneNumber else { return nil }
        return phoneNumber.replacingOccurrences(of: " ", with: "")
    }
}
