//
//  HomeDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct HomeDataView {
    
    let model: Home
    
    var userValidated: Bool {
        guard let pendencies = model.pendencies else { return false }
        return !pendencies
    }
    
    var reservationNumber: String {
        guard let reservationNumber = model.lastReservation?.numeroReserva else { return "" }
        return String(reservationNumber)
    }
       
    var checkinAvailable: Bool {
        guard let checkinAvailable = model.lastReservation?.checkInAvailable else { return false }
        return checkinAvailable
    }
    
    var reservationPaid: Bool {
        guard let reservationPaid = model.lastReservation?.paymentStatus else { return false }
        return reservationPaid == .paymentConfirmed ? true : false
    }

    var hoursToCheckin: Int? {
        return model.lastReservation?.hoursToCheckin
    }
    
    var userAvailableToCheckin: Bool {
        guard let userAvailableToCheckin = model.userAvailableToCheckin else { return false }
        return userAvailableToCheckin
    }
    
    var preAuthorizationPaid: Bool {
        guard let prePaymentPaid = model.lastReservation?.preAuthorizationStatus else { return false }
        return prePaymentPaid == .paymentConfirmed ? true : false
    }
    
    var isFranchise: Bool {
        guard let isFranchise = model.lastReservation?.franquia else { return false }
        return isFranchise
    }
    
    var dueDate: Date? {
        guard let dueDate = model.lastReservation?.dataVencimento,
              let dueTime = model.lastReservation?.horaVencimento else { return nil }
        
        let modifiedHour = (dueTime/60)
        let modifiedDate = Calendar.current.date(byAdding: .hour, value: modifiedHour, to: dueDate)
    
        return modifiedDate
    }
    
    var dueTime: Int? {
        return model.lastReservation?.horaVencimento
    }
    
    var express: Bool {
        guard let garageType = model.lastReservation?.lojaRetiradaCodigoTipo else { return false}
        return garageType == .express
    }
    
    var financialPendency: Bool {
        guard let financialPendency = model.financialPendency else { return false }
        return financialPendency
    }
}
