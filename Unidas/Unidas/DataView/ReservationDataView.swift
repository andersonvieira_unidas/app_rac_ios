//
//  ReservationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ReservationDataView {
    
    let model: Reservation
    
    var preAuthorizationValue: Double?
  
    var number: String? {
        return model.number
    }

    var pickUp: GarageReservationDetailsDataView? {
        guard let details = model.pickUp else { return nil }
        return GarageReservationDetailsDataView(model: details)
    }
    
    var `return`: GarageReservationDetailsDataView? {
        guard let details = model.return else { return nil }
        return GarageReservationDetailsDataView(model: details)
    }
    
    var totalValue: String? {
        return NumberFormatter.currencyFormatterWithouRounding.string(from: NSNumber(value: model.totalValue ?? 0.0))
    }
    
    var prePaymentValue: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.prePaymentValue ?? 0.0))
    }
    
    var group: VehicleCategoryDataView? {
        guard let group = model.group else { return nil }
        return VehicleCategoryDataView(model: group)
    }
    
    var pictureURL: URL? {
        guard let url = model.pictureURL else { return nil }
        return url
    }
    
    var duration: Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MM-dd-yyyy"
         
        guard let startDate = pickUp?.dateOnly, let endDate = `return`?.dateOnly,
            let pickupDate = dateFormatter.date(from: startDate),
            let returnDate = dateFormatter.date(from: endDate) else {
            return nil
        }
               
        let calendar = Calendar.current

        let date1 = calendar.startOfDay(for: pickupDate)
        let date2 = calendar.startOfDay(for: returnDate)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        
        return components.day
    }
    
    var express: Bool {
        guard let pickupType = model.pickUp?.garage.type else { return false }
        return pickupType == .express
    }
    
    var qrCodeExpress: String? {
        guard let reservationCode = model.number,
        let documentNumber = session.currentUser?.account.documentNumber else { return nil}

        return "\(String(describing: reservationCode))|\(String(describing: documentNumber))|EXPRESS"
    }
    
    var paid: Bool {
        if model.paymentStatus != .success {
            return false
        }
        return true
    }
    
    var paidDenied: Bool {
        if model.paymentStatus == .denied {
            return true
        }
        return false
    }
}
