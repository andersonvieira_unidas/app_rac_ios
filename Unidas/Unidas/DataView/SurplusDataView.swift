//
//  SurplusDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 24/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct SurplusDataView {
    
    let model: Surplus
    
    var description: String {
        return model.description
    }
    
    var value: String? {
        let number = NSNumber(value: model.value)
        return NumberFormatter.currencyFormatter.string(from: number)
    }
    
}
