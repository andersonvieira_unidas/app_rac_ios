//
//  UserDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 13/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import InputMask

struct UserUnidasDataView {
    let model: UserUnidas
    
    var statusDescription: String? {
        return model.contaStatusDescricao
    }
    
    var name: String? {
        return model.contaClienteNome?.localizedCapitalized
    }
    
    var email: String? {
        return model.contaClienteLoginEmail?.localizedLowercase
    }

    var mobilePhone: String? {
        guard let codePhoneNumber = model.contaClienteLoginCelularDDD,
            let phoneNumber = model.contaClienteLoginCelular else { return nil }
        
        if codePhoneNumber != "0" && phoneNumber != "0" {
            let areaCode = String(codePhoneNumber)
            let phoneNumber = String(phoneNumber)
            
            return "\(areaCode)\(phoneNumber)"
        }

        return nil
    }
    
    var formattedMobilePhoneNumber: String? {
        guard let mobilePhone = mobilePhone else { return nil }
        guard let mask = try? Mask(format: "([00]) [00000]-[0000]") else { return nil }
        let toText = CaretString(string: mobilePhone, caretPosition: mobilePhone.endIndex, caretGravity: .forward(autocomplete: true))
        return mask.apply(toText: toText).formattedText.string
    }
    
    var residencePhone: String? {
        guard let areaCode = model.contaClienteTelResDDD, let phoneNumber = model.contaClienteTelRes else { return nil }
        
        if areaCode != "0" && phoneNumber != "0" {
            let areaCodeString = String(areaCode)
            let phoneNumberString = String(phoneNumber)
            
            return "\(areaCodeString)\(phoneNumberString)"
        }

        return nil
    }
    
    var formattedResidencePhoneNumber: String? {
        guard let mask = try? Mask(format: "([00]) [0000]-[0000]"), let phone = residencePhone else { return nil }
        let toText = CaretString(string: phone, caretPosition: phone.endIndex, caretGravity: .forward(autocomplete: true))
        return mask.apply(toText: toText).formattedText.string
    }
    
    var motherName: String? {
        return model.contaClienteNomeMae?.localizedCapitalized
    }
    
    var birthDate: Date? {
        guard let birthDate = model.contaClienteNascimento?.prefix(10) else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"

        return dateFormatter.date(from: String(birthDate))
    }
    
    var formattedZipcode: String? {
        guard let mask = try? Mask(format: "[00000]-[000]"), let zipcode = model.contaClienteEnderecoResidencialCEP else { return nil }
        let toText = CaretString(string: zipcode, caretPosition: zipcode.endIndex, caretGravity: .forward(autocomplete: true))
        return mask.apply(toText: toText).formattedText.string
    }
    
    var street: String? {
        return model.contaClienteEnderecoResidencialLogradouro?.localizedCapitalized
    }
    
    var neighborhood: String? {
        return model.contaClienteEnderecoResidencialBairro?.localizedCapitalized
    }
    
    var number: String? {
        return model.contaClienteEnderecoResidencialLogradouroNumero
    }
    
    var complement: String? {
        return model.contaClienteEnderecoResidencialLogradouroComplemento
    }
}
