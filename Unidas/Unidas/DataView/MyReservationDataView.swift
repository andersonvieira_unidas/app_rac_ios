//
//  MyReservationDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct MyReservationDataView {
    let userValidated: Bool
    let userAvailableToCheckin: Bool
    let financialPendency: Bool
    let vehicleReservation: VehicleReservations
    
    var reservationNumber: String {
        return String(vehicleReservation.numeroReserva)
    }
    
    var pickupDate: Date {
        return vehicleReservation.dataHoraRetirada
    }
    
    var status: String {
        vehicleReservation.statusDescricao ?? ""
    }
    
    var checkinAvailable: Bool {
        guard let checkinAvailable = vehicleReservation.checkInAvailable else { return false }
        return checkinAvailable
    }
    
    var reservationPaid: Bool {
        guard let reservationPaid = vehicleReservation.paymentStatus else { return false }
        return reservationPaid == .paymentConfirmed ? true : false
    }
    
    var hoursToCheckin: Int? {
        return vehicleReservation.hoursToCheckin
    }
    
    var picture: URL? {
        return vehicleReservation.pictureURL
    }
    
    var preAuthorizationPaid: Bool {
        guard let prePaymentPaid = vehicleReservation.preAuthorizationStatus else { return false }
        return prePaymentPaid == .paymentConfirmed ? true : false
    }
    
    var isFranchise: Bool {
        guard let isFranchise = vehicleReservation.franquia else { return false }
        return isFranchise
    }
    
    var dueDate: Date? {
        guard let dueDate = vehicleReservation.dataVencimento,
              let dueTime = vehicleReservation.horaVencimento else { return nil }
        
        let modifiedHour = (dueTime/60)
        let modifiedDate = Calendar.current.date(byAdding: .hour, value: modifiedHour, to: dueDate)
    
        return modifiedDate
    }
    
    var dueTime: Int? {
        return vehicleReservation.horaVencimento
    }
    
    var express: Bool {
        guard let garageType = vehicleReservation.lojaRetiradaCodigoTipo else { return false }
        return garageType == .express
    }
    
    init(vehicleReservation: VehicleReservations, pendencies: Bool, userAvailableToCheckin: Bool, financialPendency: Bool) {
        self.vehicleReservation = vehicleReservation
        self.userValidated = !pendencies
        self.userAvailableToCheckin = userAvailableToCheckin
        self.financialPendency = financialPendency
    }    
}

