//
//  ProtectionsDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 23/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionsDataView {
    
    let model: PricedCoverages
    
    var name: String? {
        return model.details?.first?.text
    }
    
    var charge: ChargeDataView? {
        let charge = Charge(  amount: model.amount,
                            unitCharge: model.unitCharge,
                            unitChargePer: model.unitChargePer,
                            unitName: model.unitName,
                            quantity: model.quantity,
                            percentage: model.percentage,
                            unitPrePayment: model.unitPrePayment)
        return ChargeDataView(model: charge)
    }
    
    var value: String? {
        return charge?.calculation?.unitCharge
    }
    
    var description: String? {
        return model.details?.last?.text
    }
    
    var hasDescription: Bool {
        guard let details = model.details else { return false }
        return details.count > 1 && details[1].text != nil
    }
    
    var valueDescription: String? {
        guard let value = value else { return nil }
        let localized = NSLocalizedString("%@ per day", comment: "Per day protections value description")
        return String(format: localized, value)
        
        
    }
    
    var totalValue: String? {
        return charge?.amount
    }
}
