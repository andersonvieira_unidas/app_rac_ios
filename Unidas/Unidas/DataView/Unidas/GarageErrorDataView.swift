//
//  GarageErrorDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct GarageErrorDataView {
    let garage: Garage?
    let selected: GaragesPickerSelected
}
