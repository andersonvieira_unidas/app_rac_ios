//
//  UserInValidateDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 04/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct UserInValidateDataView {
    
    let model: UserCheck?
    
    var documentLicenceInfo: String? {
        if let documentLicenceIsValid = model?.documentLicenceIsValid, !documentLicenceIsValid {
            return NSLocalizedString("Pendencies Document Number", comment: "Pendencies Document Number description")
        }
        return nil
    }
    
    var emailInfo: String? {
        if let emailIsValid = model?.emailIsValid, !emailIsValid {
            return NSLocalizedString("Pendencies E-mail", comment: "Pendencies E-mail description")
        }
        return nil
    }
    
    var signatureInfo: String? {
        if let signatureIsValid = model?.signatureIsValid, !signatureIsValid {
            return NSLocalizedString("Pendencies Signature", comment: "Pendencies Signature description")
        }
        return nil
    }
    
    var mobileInfo: String? {
        if let mobileIsValid = model?.mobilePhoneIsValid, !mobileIsValid {
            return NSLocalizedString("Pendencies Mobile", comment: "Pendencies Mobile description")
        }
        return nil
    }
    
    var userHasPendencies: Bool {
        if let mobileIsValid = model?.mobilePhoneIsValid, !mobileIsValid {
            return true
        }
        
        if let emailIsValid = model?.emailIsValid, !emailIsValid {
            return true
        }
        
        if let credDefenseIsValid = model?.credDefenseIsValid, !credDefenseIsValid {
            return true
        }
        
        if let serasaIsValid = model?.serasaIsValid, !serasaIsValid {
            return true
        }
        
        if let documentLicenceIsValid = model?.documentLicenceIsValid, !documentLicenceIsValid {
            return true
        }
        
        if let signatureIsValid = model?.signatureIsValid, !signatureIsValid {
            return true
        }
        
        if let accountIsValid = model?.accountIsValid, !accountIsValid {
            return true
        }
        
        if let selfieIsValid = model?.selfieIsValid, !selfieIsValid {
            return true
        }
        
        return false
    }
}
