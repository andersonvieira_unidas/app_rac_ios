//
//  GarageDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

struct GarageDataView {
    
    let model: Garage
    
    var name: String {
        return model.description.localizedCapitalized
    }
    
    var code: String {
        return model.code
    }
    
    var address: AddressDataView? {
        guard let address = model.address else { return nil }
        return AddressDataView(model: address)
    }
    
    var isOpenNow: String? {
        let calendar = Calendar.current
        let dateNow = Date()
        
        if !isGarage(model, openAt: dateNow, workday: workday(for: dateNow)) {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        } else if model.holidays?.first(where: { calendar.isDate($0.date, inSameDayAs: dateNow) }) != nil, !isGarage(model, openAt: dateNow, workday: .holiday) {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        }
        
        return NSLocalizedString("Opened", comment: "Opened garage status")
    }
    
    private func isGarage(_ garage: Garage, openAt date: Date, workday: Workday) -> Bool {
        return garage.openingHours.periods.contains(where: { (period) -> Bool in
            if period.day == workday,
                let open = self.date(bySettingTimeString: period.open, of: date),
                let close = self.date(bySettingTimeString: period.close, of: date) {
                if close < open {
                    return !isDate(date, between: close, and: open, inclusive: false)
                }
                return isDate(date, between: open, and: close)
            }
            return false
        })
    }
    
    private func isDate(_ date: Date, between initalDate: Date, and finalDate: Date, inclusive: Bool = true) -> Bool {
        if inclusive {
            return (initalDate...finalDate).contains(date)
        } else {
            return initalDate < date && finalDate > date
        }
    }
    
    private func workday(for date: Date) -> Workday {
        let weekday = Calendar.current.component(.weekday, from: date)
        switch weekday {
        case 7: return .saturday
        case 1: return .sunday
        default: return .weekday
        }
    }
    
    private func date(bySettingTimeString timeString: String, of date: Date) -> Date? {
        let components = timeString.components(separatedBy: ":").compactMap { Int($0) }
        let hour = components[0]
        let minute = components[1]
        let second = 0
        return Calendar.current.date(bySettingHour: hour, minute: minute, second: second, of: date)
    }
    
    var weekdaysWorkingHours: String? {
        let weekdaysWorkingHours = model.openingHours.periods.filter { $0.day == .weekday }.map { "\($0.open) - \($0.close)" }.joined(separator: "\n")
        if weekdaysWorkingHours.isEmpty {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        } else {
            return weekdaysWorkingHours
        }
    }
    
    var saturdayWorkingHours: String? {
        let saturdayWorkingHours = model.openingHours.periods.filter { $0.day == .saturday }.map { "\($0.open) - \($0.close)" }.joined(separator: "\n")
        if saturdayWorkingHours.isEmpty {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        } else {
            return saturdayWorkingHours
        }
    }
    
    var sundayWorkingHours: String? {
        let sundayWorkingHours = model.openingHours.periods.filter { $0.day == .sunday }.map { "\($0.open) - \($0.close)" }.joined(separator: "\n")
        if sundayWorkingHours.isEmpty {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        } else {
            return sundayWorkingHours
        }
    }
    
    var holidayWorkingHours: String? {
        let holidayWorkingHours = model.openingHours.periods.filter { $0.day == .holiday }.map { "\($0.open) - \($0.close)" }.joined(separator: "\n")
        if holidayWorkingHours.isEmpty {
            return NSLocalizedString("Closed", comment: "Closed garage status")
        } else {
            return holidayWorkingHours
        }
    }

    var icon: UIImage? {
        switch model.type {
        case .airport: return #imageLiteral(resourceName: "ico-plane")
        case .store: return #imageLiteral(resourceName: "ico-store")
        case .terminal: return #imageLiteral(resourceName: "ico-bus")
        case .market: return #imageLiteral(resourceName: "ico-market")
        case .shopping: return #imageLiteral(resourceName: "ico-shopping")
        case .express: return #imageLiteral(resourceName: "ico-express")
        default: return #imageLiteral(resourceName: "ico-store")
        }
    }
    
    var icon24Hours: UIImage? {
        return model.icon24Hours == true ? #imageLiteral(resourceName: "ico-24orange") : nil
    }
    
    var iconStore: UIImage? {
        switch model.type {
        case .airport: return #imageLiteral(resourceName: "pin_aviao")
        case .store: return #imageLiteral(resourceName: "pin_loja")
        case .terminal: return #imageLiteral(resourceName: "pin_bus")
        case .shopping: return #imageLiteral(resourceName: "pin_shopping")
        case .market: return #imageLiteral(resourceName: "pin_mercado")
        case .express: return #imageLiteral(resourceName: "pin_express")
        default: return #imageLiteral(resourceName: "pin_loja")
        }
    }
    
    var isFranchise: Bool? {
        if let franchiseCode = model.franchiseCode {
            if franchiseCode != 5 {
                return true
            }
        }
        return false
    }
    
    var isExpress: Bool {
       return model.type == .express
    }
}
