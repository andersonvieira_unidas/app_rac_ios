//
//  CalculationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 4/2/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CalculationDataView {
    
    let model: Calculation
    
    var unitCharge: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: unitChargeValue))
    }
    
    var unitChargeValue: Double {
        return model.unitCharge ?? 0.0
    }
    
    var unitChargeHourExtra: String? {
        guard let unitChargeHourExtraValue = unitChargeHourExtraValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: unitChargeHourExtraValue))
    }
    
    var unitChargeHourExtraValue: Double? {
        guard let extraHour = model.unitChargeHourExtra, !extraHour.isZero else { return nil }
        return extraHour
    }
    
    /* PAGAMENTO ANTECIPADO */
    var unitPrePaymentValue: Double {
        return model.unitPrePayment ?? 0.0
    }
    
    var totalPrePaymentValue: Double {
        return Double(quantityValue) * unitPrePaymentValue
    }
    
    var totalDiscount: String? {
        guard let totalDiscountValue = totalDiscountValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: totalDiscountValue))
    }
    
    var totalDiscountValue: Double? {
        guard let discountValue = discountValue else { return nil }
        return Double(quantityValue) * discountValue
    }
    
    var totalDiscountDescription: String? {
        guard let totalDiscount = totalDiscount else { return nil }
        let localizedFormat = NSLocalizedString("-%@", comment: "Discount value description")
        return String(format: localizedFormat, totalDiscount)
    }
    
    var discountPercent: String? {
        guard let discountPercentValue = discountPercentValue else { return nil }
        return NumberFormatter.percentFormatter.string(from: NSNumber(value: discountPercentValue))
    }
    
    var totalDiscountPercentDescription: String? {
        guard let discountPercent = discountPercent else { return nil }
        let localizedFormat = NSLocalizedString("%@ of discount", comment: "Percent Discount description")
        return String(format: localizedFormat, discountPercent)
    }
    
    var discountPercentValue: Double? {
        guard let discountValue = discountValue else { return nil }
        return discountValue / unitChargeValue
    }
    
    var discountValue: Double? {
        guard let unitChargePerValue = unitChargePerValue else { return nil }
        return unitChargePerValue - unitChargeValue
    }
    
    var unitChargePerValue: Double? {
        guard let value = model.unitChargePer, !value.isZero else { return nil }
        return value
    }
    
    var totalCharge: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: totalChargeValue))
    }
    
    var totalChargeValue: Double {
        return Double(quantityValue) * unitChargeValue
    }
    
    var quantity: String? {
        return quantityValue.description
    }
    
    var quantityValue: Int {
        guard let value = model.quantity else { return 0 }
        let quantity = Int(value)
        return quantity
    }
    
    var description: String? {
        guard let quantity = quantity, let unitCharge = unitCharge else { return nil }
        let localized = NSLocalizedString("%@x %@", comment: "Quantity times unit charge description")
        return String(format: localized, quantity, unitCharge)
    }
    
    var hasDiscount: Bool {
        guard let discountValue = discountValue else { return false }
        return !discountValue.isZero
    }
    
    var prePaymentDiscountPercentValue: Double? {
        //guard let totalPrePaymentValue = totalPrePaymentValue else { return nil }
        return (totalChargeValue - totalPrePaymentValue) / totalChargeValue
    }
    
    var prePaymentDiscountPercent: String? {
        guard let prePaymentDiscountPercentValue = prePaymentDiscountPercentValue,
            let percentString = NumberFormatter.percentFormatter.string(from: NSNumber(value: prePaymentDiscountPercentValue)) else { return nil }
        let localizedString = NSLocalizedString("%@ of discount", comment: "Discount label")
        return String(format: localizedString, percentString)
    }
    
}
