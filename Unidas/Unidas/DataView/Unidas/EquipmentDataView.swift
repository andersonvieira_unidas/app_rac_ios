//
//  EquipmentDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 23/03/18.
//  Copyright © 2018 Unidas. All rights reserved.
//

import Foundation

struct EquipmentDataView {
    
    let model: PricedEquips
    
    var name: String? {
        return model.description
    }
    
    var charge: ChargeDataView? {
        guard let charge = model.charge else { return nil }
        return ChargeDataView(model: charge)
    }
    
    var value: String? {
        return charge?.calculation?.unitCharge
    }
    
    var valueQuantityDescription: String? {
        guard let value = value else { return nil }
        let localized = NSLocalizedString("%dx %@ per day", comment: "Per day equipment value description")
        return String(format: localized, selectedQuantity, value)
    }
    
    var totalValueWithQuantity: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: Double(selectedQuantity) * (model.charge?.amount ?? 0.0)))
    }
    
    var selectedQuantity: Int {
        return model.selectedQuantityItem ?? 1
    }
    
    var hasQuantity: Bool{
        return model.quantityItem == "S"
    }
}
