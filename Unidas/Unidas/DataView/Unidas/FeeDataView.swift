//
//  FeeDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 4/2/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct FeeDataView {
    
    let model: Fee
    
    var amount: String? {
        guard let amount = model.amount else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: amount))
    }
    
    var description: String? {
        return model.description?.localizedCapitalized
    }
    
    var calculation: CalculationDataView? {
        
        let calculation = Calculation(unitCharge: model.unitCharge,
                                      unitChargePer: model.unitChargePer,
                                      unitName: model.unitName,
                                      quantity: model.quantity,
                                      percentage: model.percentage,
                                      unitPrePayment: model.unitPrePayment,
                                      unitChargeTotal: nil,
                                      unitChargeHourExtra: nil)
   
        return CalculationDataView(model: calculation)
    }
    
}
