//
//  ContractDetailListItemDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ContractDetailListItemDataView {
    
    let model: ContractDetailListItem
    
    var description: String? {
        return model.description?.trimmingCharacters(in: CharacterSet(charactersIn: " "))
    }
    
    var observation: String? {
        return model.observation
    }
    
    var value: String? {
        guard let value = model.value else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value))
    }
}
