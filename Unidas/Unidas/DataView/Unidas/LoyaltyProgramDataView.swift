//
//  LoyaltyProgramDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct LoyaltyProgramDataView {
    
    let model: LoyaltyProgram
    
    var categoryDescription: String? {
        return model.categoryDescription
    }
    
    var accumulatedPoints: String? {
        return NumberFormatter.decimalNumberFormatter.string(from: NSNumber(value: model.accumulatedPoints))
    }
    
    var accumulatedPointsPercent: Float {
        return Float(model.availablePoints) / Float(model.minimumBalanceNextCategory)
    }
    
    var availablePoints: String? {
        return NumberFormatter.decimalNumberFormatter.string(from: NSNumber(value: model.availablePoints))
    }
    
    var formattedAvailablePoints: String? {
        guard let points = NumberFormatter.decimalNumberFormatter.string(from: NSNumber(value: model.availablePoints)) else { return nil }
        let localizedString = NSLocalizedString("%@ points", comment: "Formatted points")
        
        return String(format: localizedString, points)
    }
    
    var nextCategoryPoints: String? {
        guard let points = NumberFormatter.decimalNumberFormatter.string(from: NSNumber(value: model.nextCategoryPoints)) else { return nil }
        let localizedString = NSLocalizedString("%@ points until next level", comment: "Keep category points")
        return String(format: localizedString, points)
    }
    
    var minimumBalanceNextCategory: String? {
        return NumberFormatter.decimalNumberFormatter.string(from: NSNumber(value: model.minimumBalanceNextCategory))
    }
    
}
