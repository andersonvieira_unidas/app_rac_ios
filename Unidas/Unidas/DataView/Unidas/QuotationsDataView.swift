//
//  QuotationsDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 23/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct QuotationsDataView {

    let model: VehAvail
    
    var group: String? {
        return model.vehicle?.vehicleCategory
    }
    
    var formattedGroup: String? {
        guard let group = group else { return nil }
        let localizedString = NSLocalizedString("%@ group", comment: "Formatted group label")
        return String(format: localizedString, group)
    }
    
    var vehicle: String? {
        return model.vehicle?.name?.components(separatedBy: " - ").last
    }
    
    var description: String? {
        return model.vehicle?.name?.components(separatedBy: " - ").first
    }
    
    var vehicleGroups: String? {
        return model.vehicle?.vehicleGroups
    }
    
    var formattedGroupWithDescription: String? {
        guard let group = group, let description = description else { return nil }
        let localizedString = NSLocalizedString("Group %@ - %@", comment: "Group name")
        return String(format: localizedString, group, description)
    }
    
    var picture: URL? {
        return model.vehicle?.pictureURL
    }
    
    var valueTotalDiary: Double {
        return 0.0
    }
    
    var valueTotalDiaryString: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: valueTotalDiary))
    }
    
    var numberOfDays: String? {
        guard let days = vehicleChargeCalculation?.quantity else { return nil }
        let localizedString = NSLocalizedString("%@ days", comment: "Number of days")
        return String(format: localizedString, days)
    }
    
    var vehicleChargeCalculation: CalculationDataView? {
        let calculation = Calculation(unitCharge: model.vehicleCharge?.unitCharge,
                                      unitChargePer: model.vehicleCharge?.unitChargePer,
                                      unitName: model.vehicleCharge?.unitName,
                                      quantity: model.vehicleCharge?.quantity,
                                      percentage: model.vehicleCharge?.percentage,
                                      unitPrePayment: model.vehicleCharge?.unitPrePayment,
                                      unitChargeTotal: model.vehicleCharge?.unitChargeTotal,
                                      unitChargeHourExtra: model.vehicleCharge?.unitChargeHourExtra)
        return CalculationDataView(model: calculation)
    }
    
    var returnTaxValue: String? {
        let value = model.fees?.first(where: { $0.purpose == "19" })?.amount ?? 0.0
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value))
    }
    
    var extraHourTax: String? {
        return vehicleChargeCalculation?.unitChargeHourExtra
    }
}
