//
//  ContractDetailPaymentListItemDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

struct ContractDetailPaymentListItemDataView {
    
    let model: ContractDetailPaymentListItem
    
    var paymentType: String {
        guard let type = model.type else {
            return NSLocalizedString("Paid at pickup", comment: "Paid at pickup payment type description")
        }
        return type.rawValue
    }
    
    var type: ContractDetailPaymentType {
        guard let type = model.type else { return .unknown }
        return type
    }
    
    var cardLastNumbers: String? {
        guard let cardNumber = model.card, cardNumber.count >= 4 else { return nil }
        let startIndex = cardNumber.index(cardNumber.endIndex, offsetBy: -4)
        let lastNumbers = cardNumber[startIndex..<cardNumber.endIndex]
        return "•••• \(lastNumbers)"
    }
    
    var cardLastNumbersWithoutDots: String? {
        guard let cardNumber = model.card, cardNumber.count >= 4 else { return nil }
        let startIndex = cardNumber.index(cardNumber.endIndex, offsetBy: -4)
        let lastNumbers = cardNumber[startIndex..<cardNumber.endIndex]
        return "\(lastNumbers)"
    }
    
    var value: String? {
        guard let value = model.value else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value))
    }
    
    var cardBrandImage: UIImage? {
        guard let brandName = model.cardBrand else { return nil }
        let charSet = CharacterSet(charactersIn: " ")
        return UIImage(named: "card-brand-\(brandName.trimmingCharacters(in: charSet).lowercased())")
    }
    
    var installmentsDescription: String? {
        guard let installments = model.installments, let value = model.value, installments > 0, let installmentsValue = NumberFormatter.currencyFormatter.string(from: NSNumber(value: value / Float(installments))) else { return nil }
        return "\(installments)x \(installmentsValue)"
    }
    
    var paymentStatus: String? {
        return model.prePaymentStatus
    }
}
