//
//  VehicleReservationsDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 03/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import CoreLocation

struct VehicleReservationsDataView {
    let model: VehicleReservations
    
    static let dateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMM dd")
        return dateFormatter
    }()
    
    static let shortDateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd/MM/YYYY")
        return dateFormatter
    }()
    
    static let timeFormatter: DateFormatter = { () -> DateFormatter in
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        return timeFormatter
    }()
    
    var rateQualifier: String? {
        return String(model.tarifa)
    }
    
    var checkinHours: Date? {
        return model.dateCheckIn
    }
    
    var dateShortFormatted: String? {
        guard let checkinHours = checkinHours else { return nil }
        return VehicleReservationsDataView.shortDateFormatter.string(from: checkinHours)
    }
    
    var timeFormatted: String? {
        guard let checkinHours = checkinHours else { return nil }
        return VehicleReservationsDataView.timeFormatter.string(from: checkinHours)
    }
    
    var isHiddenCheckin: Bool {
        guard let checkinHours = checkinHours else { return true }
        
        return (checkinHours >= Date())
    }
    
    var isHiddenChange: Bool {
        if !isHiddenCheckin || hasVoucherApplied {
            return true
        } else {
           return (model.canUpdateReserve == 0)
        }
    }
    
    var reservationNumberWithText: String? {
        let localizedString = NSLocalizedString("Reservation %d", comment: "Reservation number label")
        return String(format: localizedString, model.numeroReserva)
    }

    var reserveNumber: String {
        let reserve = String(model.numeroReserva)
        return reserve
    }
    
    //var shortFormattedPickUp: String? {
    //    return VehicleReservationsDataView.shortDateFormatter.string(from: model.dataHoraRetirada)
    //}
    
    var pickUpDate: String {
        return string(from: model.dataHoraRetirada)
    }
    
    var returnDate: String {
        return string(from: model.dataHoraDevolucao)
    }
    
    var pickUpDateOnly: String {
        return DateFormatter.dateShortFormatter.string(from: model.dataHoraRetirada)
    }
    
    var returnDateOnly: String {
        return DateFormatter.dateShortFormatter.string(from: model.dataHoraDevolucao)
    }
    
    var pickUpHourOnly: String {
        return DateFormatter.timeOnlyFormatter.string(from: model.dataHoraRetirada)
    }
    
    var returnHourOnly: String {
        return DateFormatter.timeOnlyFormatter.string(from: model.dataHoraDevolucao)
    }
    
    var pickUpDateFormatted: String {
        let localizedString = NSLocalizedString("Pickup Date %@ at %@", comment: "")
        return String(format: localizedString, pickUpDateOnly, pickUpHourOnly)
    }
    
    var returnDateFormatted: String {
        let localizedDateString = NSLocalizedString("Return Date %@ at %@", comment: "")
        return String(format: localizedDateString, returnDateOnly, returnHourOnly)
    }

    var pickUpIATA: String? {
        return model.lojaRetiradaIATA ?? ""
    }
    
    var pickUpLocation: String? {
        guard let lojaRetiradaNome = model.lojaRetiradaNome else {
            return nil
        }
        
        guard let cidadeRetirada = model.cidadeRetirada else {
            return lojaRetiradaNome
        }
        
        guard let ufRetirada = model.ufRetirada else {
            return "\(lojaRetiradaNome), \(cidadeRetirada)"
        }
        
        
        return "\(lojaRetiradaNome), \(cidadeRetirada), \(ufRetirada)"
    }
    
    var pickUpStoreAndState: String? {
        guard let pickUpStore = model.lojaRetiradaNome else {
            return nil
        }
        
        guard let pickUpCity = model.ufRetirada else {
            return pickUpStore
        }
        
        return "\(pickUpStore), \(pickUpCity)"
    }
    
    var formattedPickUpLocation: String? {
        let localizedString = NSLocalizedString("at %@", comment: "Location Store")
        
        guard let pickUpLocation = pickUpLocation else { return nil }
        
        guard let pickUpIATA = pickUpIATA else {
            return String(format: localizedString, pickUpLocation)
        }
        
        if pickUpIATA.isEmpty {
            return String(format: localizedString, pickUpLocation)
        }
        
        let formattedString = "\(pickUpIATA) - \(pickUpLocation)"
        
        return String(format: localizedString, formattedString)
    }
    
    var returnIATA: String? {
        return model.lojaDevolucaoIATA ?? ""
    }
    
    var returnLocation: String? {
        guard let lojaDevolucaoNome = model.lojaDevolucaoNome else {
            return nil
        }
        
        guard let cidadeDevolucao = model.cidadeDevolucao else {
            return lojaDevolucaoNome
        }
        
        guard let ufDevolucao = model.ufDevolucao else {
            return "\(lojaDevolucaoNome), \(cidadeDevolucao)"
        }
        
        return "\(lojaDevolucaoNome), \(cidadeDevolucao), \(ufDevolucao)"
    }
    
    var returnStoreAndState: String? {
        guard let returnStore = model.lojaDevolucaoNome else {
            return nil
        }
        
        guard let returnUf = model.ufDevolucao else {
            return "\(returnStore)"
        }
        
        return "\(returnStore), \(returnUf)"
    }
    
    var formattedReturnLocation: String? {
        let localizedString = NSLocalizedString("at %@", comment: "Location Store")
        
        guard let returnLocation = returnLocation else { return nil }
        
        guard let returnIATA = returnIATA else {
            return String(format: localizedString, returnLocation)
        }
        
        if returnIATA.isEmpty {
            return String(format: localizedString, returnLocation)
        }
        
        let formattedString = "\(returnIATA) - \(returnLocation)"
        
        return String(format: localizedString, formattedString)
    }
    
    var group: String {
        let localizedString = NSLocalizedString("%@ group", comment: "")
       
        return String(format: localizedString, model.grupo)
    }
    
    var groupDescription: String {
        return  model.grupoDescricao
    }
    
    var vehicleGroup: String? {
        return [model.grupoVeiculos, model.motorizacao].compactMap { $0 }.joined(separator: "\n")
    }
    
    var totalValue: Double {
        return Double(model.valorTotal)
    }
    
    var totalValueString: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.valorTotal))
    }
    
    var isHiddenPickUp: Bool {
        guard let observation = model.lojaRetiradaObs, !observation.isEmpty else { return true }        
        return false
    }
    
    var isHiddenReturn: Bool {
        guard let observation = model.lojaDevolucaoObs, !observation.isEmpty else { return true }
        return false
    }
    
    var storePickup: String {
        return model.lojaRetirada
    }
    
    var pickUpObservation: String? {
        return model.lojaRetiradaObs
    }
    
    var returnObservation: String? {
        return model.lojaDevolucaoObs
    }
    
    var pickupGeolocation: CLLocationCoordinate2D? {
        guard let lojaRetiradaGeo = model.lojaRetiradaGeoLoc else { return nil }
        
        let geolocation = lojaRetiradaGeo.split(separator: ",")
        
        if !geolocation.isEmpty {

            guard let lat = Double(geolocation[0].trimmingCharacters(in: .whitespacesAndNewlines)), let lng = Double(geolocation[1].trimmingCharacters(in: .whitespacesAndNewlines)) else { return nil }
            return CLLocationCoordinate2D(latitude: lat, longitude: lng)
        }

        return nil
    }
    
    var returnGeolocation: CLLocationCoordinate2D? {
        guard let lojaDevolucaoGeo = model.lojaDevolucaoGeoLoc else { return nil }
        
        let geolocation = lojaDevolucaoGeo.split(separator: ",")
        
        if !geolocation.isEmpty {
            guard let lat = Double(geolocation[0].trimmingCharacters(in: .whitespacesAndNewlines)), let lng = Double(geolocation[1].trimmingCharacters(in: .whitespacesAndNewlines)) else { return nil }
            
            return CLLocationCoordinate2D(latitude: lat, longitude: lng)
        }
        return nil
    }
    
    var reserveIsPaid: Bool {
        return (model.numeroCartao != nil)
    }
    
    var paymentTitle: String {
        if let paymentStatusLabel = model.paymentStatus {
            return paymentStatusLabel.rawValue
        }
        return NSLocalizedString("Pay in up to", comment: "Pay in up to label description")
    }
    
    var paymentStatus: PaymentStatus? {
        return model.paymentStatus
    }
    
    var prePaymentStatus: PaymentStatus? {
        return model.preAuthorizationStatus
    }
    
    var fourLastNumberCard: String? {
        guard let cardNumber = model.numeroCartao, !cardNumber.isEmpty else { return nil }
        
        let openPan = cardNumber.index(cardNumber.endIndex, offsetBy: -4)
        let fourLast = cardNumber[openPan..<cardNumber.endIndex]
        return String(fourLast)
    }
    
    var fourLastNumberCardPreAuthorization: String? {
        return model.numeroCartaoPreAutorizacao
    }
    
    var cardNumberShortFormatted: String? {
        if let fourLastNumberCard = fourLastNumberCard {
            return "**** \(fourLastNumberCard)"
        }
        return "****"
    }
    
    var brandImage: UIImage {
        guard let brandCard = model.bandeiraCartao else { return #imageLiteral(resourceName: "card-off") }
        
        return CardUtils.findByName(for: brandCard).brandImage
    }
    
    var prePaymentBrandImage: UIImage {
        guard let brandCard = model.bandeiraCartaoPreAutorizacao else { return #imageLiteral(resourceName: "card-off") }
        
        return CardUtils.findByName(for: brandCard).brandImage
    }
    
    func valuePayment(installments: String?) -> String {
        if reserveIsPaid {
            return paymentMade
        }

        guard let installments = installments else { return "" }
        
        let installmentsDouble = Double(installments) ?? 0.0
        let amountInstallments = Double(model.valorTotal) / installmentsDouble
        
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: amountInstallments)) else { return "" }
        
        return "\(installments)x \(amountString)"
    }
    
    var preAuthorizationValue: String? {
        guard let value = model.valorPreAutorizacao else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value))
    }
    
    var paymentMade: String {
        guard let installments = model.quantidadeParcela else { return "-" }
        
        let installmentsDouble = Double(installments)
        
        var amountInstallments = Double(model.valorTotal) / installmentsDouble
        
        if let status = model.paymentStatus, status == .paymentConfirmed {
            amountInstallments = Double(model.valorPago) / installmentsDouble
        }
        
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: amountInstallments)) else { return "" }
        
        return "\(installments)x \(amountString)"
    }
    
    var totalReservationPayed: String {

        var amountInstallments = Double(model.valorTotal)
        
        if let status = model.paymentStatus, status == .paymentConfirmed {
            amountInstallments = Double(model.valorPago)
        }
        
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: amountInstallments)) else { return "" }
        
        return amountString
    }
    
    var isFranchise: Bool {
        if let isFranchise = model.franquia {
            return isFranchise
        }
        return false
    }
    
    var isLegalEntityRequiredPayment: Bool {
        if let requiredPayment = model.obrigatorioPgto {
            return requiredPayment
        }
        return false
    }
    
    var isLegalEntityRequiredPrePayment: Bool {
        if let requiredPrePayment = model.obrigatorioPre {
            return requiredPrePayment
        }
        return false
    }
    
    var legalEntityName: String {
        if let empresaNome = model.empresaNome {
            return empresaNome
        }
        return ""
    }
    
    var isLegalEntity: Bool {
        if model.reservaPessoaJuridica == true {
            return true
        }
        return false
    }
    
    var isCheckin24HoursAvailable: Bool {
        if model.checkInAvailable == true {
            return true
        }
        return false
    }
    
    var hideCheckinButton: Bool {
        if isFranchise {
            return true
        }
        
        if isPreAuthorizationPaid && isPaid {
            return true
        }
        
        if isLegalEntity {
            return false
        }

        return false
    }
    
    var hiddenButtonPaymentWhenStoreIsAcceptPayment: Bool {
        if isFranchise {
            return true
        }
        if isLegalEntity {
            if isLegalEntityRequiredPayment && !isPaid {
                return false
            } else {
                return true
            }
        } else {
            return isPaid
        }
    }
    
    var hiddenLegalEntityCheckinPayment: Bool {
        if (isLegalEntity && !isLegalEntityRequiredPayment) || isPaid  {
            return true
        }
        return false
    }
    
    var hiddenLegalEntityPaymentInformation: Bool {
        if (isLegalEntity && isLegalEntityRequiredPayment) {
            return isPaid
        }
        return false
    }
    
    var hiddenLegalEntityCheckinPrePayment: Bool {
        if isLegalEntity && !isLegalEntityRequiredPrePayment {
            return true
        }
        return false
    }
    
    var isPaid: Bool {
        guard let paymentStatus = model.paymentStatus else { return false }
        if paymentStatus == .paymentConfirmed || paymentStatus == .waitingConfirmation {
            return true
        }
        return false
    }
    
    var isPreAuthorizationPaid: Bool {
        guard let paymentStatus = model.preAuthorizationStatus else { return false }
        if paymentStatus == .paymentConfirmed || paymentStatus == .waitingConfirmation {
            return true
        }
        return false
    }
    
    var isPassStepPaymentCheckin: Bool {
        if isLegalEntity, !isLegalEntityRequiredPayment, !isLegalEntityRequiredPrePayment {
            return true
        } else if isLegalEntity, isLegalEntityRequiredPayment, !isLegalEntityRequiredPrePayment && isPaid {
            return true
        }

        return false
    }
    
    var requireAllPayments: Bool {
        if isLegalEntityRequiredPayment, isLegalEntityRequiredPrePayment {
            return true
        }
        return false
    }
    
    var noRequiredPayments: Bool {
        if !isLegalEntityRequiredPayment, !isLegalEntityRequiredPrePayment {
            return true
        }
        return false
    }
    
    var requiresPaymentOnly: Bool {
        if isLegalEntityRequiredPayment, !isLegalEntityRequiredPrePayment {
            return true
        }
        return false
    }
    
    var requiresPrePaymentOnly: Bool {
        if !isLegalEntityRequiredPayment, isLegalEntityRequiredPrePayment {
            return true
        }
        return false
    }
    
    var hasVoucherApplied: Bool {
        if let hasVoucherApplied = model.cupomPromocional, !hasVoucherApplied.isEmpty { return true
        }
        return false
    }
        
    private func string(from date: Date) -> String {
        let dateString = VehicleReservationsDataView.dateFormatter.string(from: date)
        let timeString = VehicleReservationsDataView.timeFormatter.string(from: date)
        let localizedString = NSLocalizedString("%@ at %@", comment: "Date")
        return String(format: localizedString, dateString, timeString)
    }
    
    var daysInterval: Int? {
        let pickUpDate = model.dataHoraRetirada
        let returnDate = model.dataHoraDevolucao
       
        let calendar = Calendar.current
        
        let dateOne = calendar.startOfDay(for: pickUpDate)
        let dateTwo = calendar.startOfDay(for: returnDate)
        let components = calendar.dateComponents([.day], from: dateOne, to: dateTwo)
        
        if components.day == 0 {
            return 1
        }
        
        return components.day
    }
    
    var picture: URL? {
        return model.pictureURL
    }
    
    var checkinAvailable: Bool {
        guard let checkinAvailable = model.checkInAvailable else { return false }
        return checkinAvailable
    }
   
    var checkinDateAvailable: Bool {
        guard let checkinDateAvailable = model.checkInAvailable else { return false }
        return checkinDateAvailable
    }
    
    var hoursToCheckin: Int? {
        return model.hoursToCheckin
    }
    
    var reservationPaid: Bool {
        guard let reservationPaid = model.paymentStatus else { return false }
        return reservationPaid == .paymentConfirmed ? true : false
    }
    
    var preAuthorizationPaid: Bool {
        guard let paymentStatus = model.preAuthorizationStatus else { return false }
        if paymentStatus == .paymentConfirmed {
            return true
        }
        return false
    }
    
    var contractNumber: Int? {
        guard let contractNumber = model.numeroContrato else { return nil }
        return contractNumber
    }
    
    var voucher: String? {
        if isLegalEntity, let type = model.voucher?.voucherType {
           return type
        }
        return nil
    }
    
    var installments: String {
        guard let installments = model.quantidadeParcela else { return "" }
        return String(installments)
    }
    
    var dueDate: Date? {
        guard let dueDate = model.dataVencimento,
              let dueTime = model.horaVencimento else { return nil }
        
        let modifiedHour = (dueTime/60)
        let modifiedDate = Calendar.current.date(byAdding: .hour, value: modifiedHour, to: dueDate)
    
        return modifiedDate
    }
    
    var dueTime: Int? {
        return model.horaVencimento
    }
    
    var express: Bool {
        guard let garageType = model.lojaRetiradaCodigoTipo else { return false }
        return garageType == .express
    }
    
    var qrCode: String? {
        if !express {
            return model.qrCode
        } else {
            guard let documentNumber = session.currentUser?.account.documentNumber else { return nil}
            return "\(String(describing: model.numeroReserva))|\(String(describing: documentNumber))|EXPRESS"
        }
    }
}
