//
//  UserResponseDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 10/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import PhoneNumberKit

struct UserResponseDataView {
    let model: UserResponse

    var account: AccountDataView {
        return AccountDataView(model: model.account)
    }

    var userUnidas: UserUnidasDataView? {
        guard let userUnidas = model.unidas?.first else { return nil }
        
        return UserUnidasDataView(model: userUnidas)
    }
}
