//
//  CancellationReasonDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CancellationReasonDataView {
    
    let model: CancellationReason
    
    var reasonsDescription: String {
        return model.motivoUsuario.localizedCapitalized
    }
}
