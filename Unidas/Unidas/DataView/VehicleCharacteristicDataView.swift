//
//  VehicleCharacteristicDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 02/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleCharacteristicDataView {
    
    let model: VehicleCharacteristic
    
    var image: URL? {
        return model.image
    }
    
    var text: String {
        return model.name
    }
    
}

