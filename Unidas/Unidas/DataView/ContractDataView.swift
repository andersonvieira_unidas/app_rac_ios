//
//  ContractDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/07/2018.
//  Copyright © 2018 Unidas. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct ContractDataView {
    let model: Contract
    
    var formattedContract: String? {
        guard let contractId = model.id else { return nil }
        let localizedContract = NSLocalizedString("Contract %@", comment: "Formatted contract number")
        return String(format: localizedContract, contractId.description)
    }
    
    var contract: String? {
        guard let contractId = model.id else { return nil }
        return contractId.description
    }
    
    var pickUpDate: Date? {
        return dateFrom(dateString: model.pickupDate, hourString: model.pickupHour)
    }
    
    var pickUpHour: String? {
        return model.pickupHour
    }
    
    var pickUpDateOnly: String? {
        if let date = model.pickupDate, let dateTransform = DateFormatter.dateFormatterAPI.date(from: date){
            return DateFormatter.dateShortFormatter.string(from: dateTransform)
            
        }
        return nil
    }
    
    var returnDate: Date? {
        return dateFrom(dateString: model.returnDate, hourString: model.returnHour)
    }
    
    var returnHour: String? {
        return model.returnHour
    }
    
    var pickUpGarageDescription: String? {
        return model.pickupGarageDescription?.localizedCapitalized
    }
    
    var pickUpStoreAndState: String? {
        guard let pickUpStore = model.pickupGarageDescription else {
            return nil
        }
        
        guard let pickupState = model.pickupGarageState else {
            return pickUpStore
        }
        
        return "\(pickUpStore), \(pickupState)"
    }
    
    var returnGarageDescription: String? {
        return model.returnGarageDescription?.localizedCapitalized
    }
    
    var pickUpObservation: String? {
        return model.pickupGarageObservation
    }
    
    var returnObservation: String? {
        return model.returnGarageObservation
    }
    
    var fullRentTimeInterval: TimeInterval? {
        guard let pickUpDate = pickUpDate, let returnDate = returnDate else { return nil }
        return returnDate.timeIntervalSince(pickUpDate)
    }
    
    var formattedDateInterval: String? {
        guard let pickUpDate = pickUpDate, let returnDate = returnDate else { return nil }
        
        let dateIntervalFormatter = DateIntervalFormatter()
        dateIntervalFormatter.dateStyle = .long
        dateIntervalFormatter.timeStyle = .none
        
        return dateIntervalFormatter.string(from: pickUpDate, to: returnDate)
    }
    
    var daysInterval: Int? {
        guard let pickUpDate = pickUpDate, let returnDate = returnDate else { return nil}
        
        let calendar = Calendar.current
        
        let dateOne = calendar.startOfDay(for: pickUpDate)
        let dateTwo = calendar.startOfDay(for: returnDate)
        let components = calendar.dateComponents([.day], from: dateOne, to: dateTwo)
        
        if components.day == 0 {
            return 1
        }
        
        return components.day
    }
    
    /*var imageForReservationStatus: UIImage? {
     guard let status = model.status else { return nil }
     switch status {
     case .registered:
     return #imageLiteral(resourceName: "car-pickup-big")
     case .open:
     return #imageLiteral(resourceName: "car-return-big")
     default:
     return nil
     }
     }*/
    
    var pickupGeolocation: CLLocationCoordinate2D? {
        return geolocation(from: model.pickupGarageGeolocalization)
    }
    
    var returnGeolocation: CLLocationCoordinate2D? {
        return geolocation(from: model.returnGarageGeolocalization)
    }
    
    var returnPatioGeolocalization: CLLocationCoordinate2D? {
        return geolocation(from: model.returnPatioGeolocalization)
    }
    
    var vehicle: ContractVehicleDataView? {
        guard let vehicle = model.vehicle else { return nil }
        return ContractVehicleDataView(model: vehicle)
    }
    
    private func dateFrom(dateString: String?, hourString: String?) -> Date? {
        guard let dateString = dateString,
            let hourComponents = hourString?.components(separatedBy: ":"),
            let hour = Int(hourComponents[0]),
            let minute = Int(hourComponents[1]),
            let date = DateFormatter.dateFormatter(withFormat: "YYYY-MM-dd'T'HH:mm:ss").date(from: dateString) else { return nil }
        let calendar = Calendar.current
        return calendar.date(bySettingHour: hour, minute: minute, second: 0, of: date)
    }
    
    var formattedPickUpLocation: String? {
        guard let pickUpLocation = pickUpGarageDescription else { return nil }
        
        let localizedString = NSLocalizedString("at %@", comment: "Location Store")
        
        return String(format: localizedString, pickUpLocation)
    }
    
    var formattedStorePickUpLocation: String? {
        guard let pickUpLocation = pickUpGarageDescription else { return nil }
        let storeString = NSLocalizedString("Store", comment: "")
        
        return "\(storeString) \(pickUpLocation)"
    }
    
    var pickUpDateFormatted: String? {
        guard let pickUpDate = pickUpDate else { return nil }
        return string(from: pickUpDate)
    }
    
    var pickUpDateShortFormatted: String? {
        guard let pickUpDate = pickUpDate else { return nil }
        return DateFormatter.shortDateFormatterAPI.string(from: pickUpDate)
    }
    
    var formattedReturnLocation: String? {
        guard let returnLocation = returnGarageDescription else { return nil }
        let localizedString = NSLocalizedString("at %@", comment: "Location Store")
        return String(format: localizedString, returnLocation)
    }
    
    var formattedStoreReturnLocation: String? {
        guard let returnLocation = returnGarageDescription else { return nil }
        let storeString = NSLocalizedString("Store", comment: "")
               
        return "\(storeString) \(returnLocation)"
    }
  
    var returnDateFormatted: String? {
        guard let returnDate = returnDate else { return nil }
        return string(from: returnDate)
    }
    
    var returnDateShortFormatted: String? {
        guard let returnDate = returnDate else { return nil }
        return DateFormatter.shortDateFormatterAPI.string(from: returnDate)
    }
    
    var isHiddenVehicleInformation: Bool {
        if let plate = vehicle?.licensePlate, plate.isEmpty {
            return true
        }
        
        return vehicle?.modelName == nil
    }
    
    var groupCode: String? {
        guard let groupCode = model.group?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) else { return nil }
        let localized = NSLocalizedString("%@ Group", comment: "Group code label")
        return String(format: localized, groupCode)
    }
    
    var groupDescription: String? {
        return model.groupDescription
    }
    
    var groupVehicles: String? {
        return model.groupVehicles
    }
    
    var qrCodeInformation: String? {
        return model.qrCodeInfo
    }
    
    var legalEntity: Bool{
        if let legalEntity = model.reservaPessoaJuridica, legalEntity {
            return true
        }
        return false
    }
    
    var isLegalEntityPrePaymentRequired: Bool {
        if let required = model.obrigatorioPre, required, legalEntity {
            return true
        }
        return false
    }
    
    var isLegalEntityPaymentRequired: Bool {
        if let required = model.obrigatorioPgto, required, legalEntity {
            return true
        }
        return false
    }
    
    var companyName: String? {
        return model.empresaNome
    }
    
    private func string(from date: Date) -> String {
        let dateString = VehicleReservationsDataView.dateFormatter.string(from: date)
        let timeString = VehicleReservationsDataView.timeFormatter.string(from: date)
        let localizedString = NSLocalizedString("%@ at %@", comment: "Date")
        return String(format: localizedString, dateString, timeString)
    }
    
    private func geolocation(from string: String?) -> CLLocationCoordinate2D? {
        guard let string = string?.trimmingCharacters(in: CharacterSet(charactersIn: " ")) else { return nil }
        let components = string.components(separatedBy: ",")
        guard components.count > 1,
            let latitude = CLLocationDegrees(components[0].trimmingCharacters(in: CharacterSet(charactersIn: " "))),
            let longitude = CLLocationDegrees(components[1].trimmingCharacters(in: CharacterSet(charactersIn: " "))) else { return nil }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var pictureURL: URL?{
        guard let pictureURL = model.pictureURL else { return nil }
        return pictureURL
    }
}
