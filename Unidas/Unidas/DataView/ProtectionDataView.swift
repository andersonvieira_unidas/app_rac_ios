//
//  ProtectionDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 12/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionDataView: Equatable {
    
    let model: Protection
    
    var title: String? {
        return model.title
    }
    
    var price: String? {
        guard let price = model.price else { return nil }
        let number = NSNumber(floatLiteral: price)
        return NumberFormatter.currencyFormatter.string(from: number)
    }
    
    private var dailyCosts: String? {
        guard let dailyCosts = model.costs?.daily else { return nil }
        let number = NSNumber(floatLiteral: dailyCosts)
        return NumberFormatter.currencyFormatter.string(from: number)
    }
    
    var promotionalDailyCostsText: String? {
        guard let dailyCosts = dailyCosts else { return nil }
        let localizedString = NSLocalizedString("%@ daily", comment: "Promotional daily costs text")
        return String(format: localizedString, dailyCosts)
    }
    
    var hasDetails: Bool {
        return model.details != nil
    }
    
    var details: ProtectionDetailsDataView? {
        guard let details = model.details else { return nil }
        return ProtectionDetailsDataView(model: details)
    }
    
    var disclaimer: String? {
        return model.disclaimer
    }
    
    var type: ProtectionTypeDataView {
        return ProtectionTypeDataView(model: model.type)
    }
    
}
