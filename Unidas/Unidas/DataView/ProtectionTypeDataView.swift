//
//  ProtectionTypeDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionTypeDataView: CustomStringConvertible {
    
    let model: ProtectionType
    
    var description: String {
        switch model {
        case .vehicle: return NSLocalizedString("Vehicle protections", comment: "Vehicle protections group name")
        case .complementary: return NSLocalizedString("Complementary protections", comment: "Complementary protections group name")
        case .thirdParty: return NSLocalizedString("Third party protections", comment: "Third party protections group name")
        }
    }
}

