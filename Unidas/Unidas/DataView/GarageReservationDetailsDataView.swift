//
//  GarageReservationDetailsDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct GarageReservationDetailsDataView {
    
    let model: GarageReservationDetails
    
    var date: String {
        return string(from: model.date)
    }
    
    var dateOnly: String {
        return DateFormatter.dateShortFormatter.string(from: model.date)
    }
    
    var hourOnly: String {
        return DateFormatter.timeOnlyFormatter.string(from: model.date)
    }
    
    var garage: GarageDataView? {
        return GarageDataView(model: model.garage)
    }
    
    var locationDescription: String? {
        guard let name = garage?.name, let state = garage?.address?.state else { return nil }
        let localizedString = NSLocalizedString("at %@, %@", comment: "")
        return String(format: localizedString, name, state)
    }
    
    var storeAndState: String? {
        guard let pickUpStore = garage?.name else {
            return nil
        }
        
        guard let pickUpCity = garage?.address?.city else {
            return pickUpStore
        }
        
        return "\(pickUpStore), \(pickUpCity)"
    }
    
    private func string(from date: Date) -> String {
        
        let dateString = DateFormatter.dayWithMonthExtensionDateFormatter.string(from: date)
        let timeString = DateFormatter.timeOnlyFormatter.string(from: date)
        let localizedString = NSLocalizedString("%@ at %@", comment: "Date")
        return String(format: localizedString, dateString, timeString)
    }
    
}
