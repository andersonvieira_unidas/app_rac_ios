//
//  ConfigurationItemDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//


import UIKit

struct ConfigurationItemDataView {
    
    let model: ConfigurationItem
    
    var image: UIImage? {
        return UIImage(named: model.image.lowercased())
    }
    
    var name: String {
        return NSLocalizedString(model.localizedString, comment: model.localizedString)
    }
    
    var action: String? {
        return model.action
    }
    
    var isVisibleDotted: Bool {
        return model.separator
    }
}
