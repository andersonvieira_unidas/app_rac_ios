//
//  ResumeInformationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ResumeInformationDataView {
    
    let model: ResumeInformation
    
    var title: String? {
        return model.title
    }
    
    var subtitle: String? {
        return model.subtitle
    }
    
    var value: String? {
        return model.value
    }
    
}

