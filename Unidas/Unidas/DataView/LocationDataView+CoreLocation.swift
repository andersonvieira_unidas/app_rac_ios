//
//  LocationDataView+CoreLocation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 06/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import CoreLocation

extension LocationDataView {
    
    init(coordinate: CLLocationCoordinate2D) {
        self.init(model: Location(coordinate: coordinate))
    }
    
    init(location: CLLocation) {
        self.init(model: Location(location: location))
    }
    
}
