//
//  VehicleCategoryDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleCategoryDataView {
    
    let model: VehMakeModel
    
    var formattedGroupName: String? {
        guard let code = model.code, let description = model.name?.components(separatedBy: " - ").first else { return nil }
        let localizedString = NSLocalizedString("Group %@ - %@", comment: "Group name")
        return String(format: localizedString, code, description)
    }
    
    var groupCode: String? {
        return model.codeCategory
    }
    
    var groupName: String? {
        return model.name?.components(separatedBy: " - ").first
    }
    
    var vehicles: String? {
        return model.vehiclesGroups
    }
    
}
