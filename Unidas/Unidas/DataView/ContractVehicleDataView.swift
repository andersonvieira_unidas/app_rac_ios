//
//  ContractVehicleDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit



struct ContractVehicleDataView {
    
    let model: ContractVehicle
    
    let charset = CharacterSet(charactersIn: " ")
    
    var modelName: String? {
        return model.model?.trimmingCharacters(in: charset).localizedCapitalized
    }
    
    var manufacturer: String? {
        return model.manufacturer?.trimmingCharacters(in: charset).localizedCapitalized
    }

    var description: String? {
        return [model.color, model.modelType].compactMap { $0 }.joined(separator: " • ").localizedCapitalized
    }
    
    var licensePlate: String? {
        return model.licensePlate
    }
    
    var gearshift: GearShiftTypeDataView? {
        guard let gearShift = model.gearType else { return nil }
        return GearShiftTypeDataView(model: gearShift)
    }
    
    var color: String? {
        return model.color
    }
    
}
