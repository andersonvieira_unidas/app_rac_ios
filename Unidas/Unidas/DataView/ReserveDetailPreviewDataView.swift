//
//  ReserveDetailPreviewDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/07/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct ReserveDetailPreviewDataView {
    let vehicleReservationsDataView: VehicleReservationsDataView
    let rulesDataView: RulesDataView
}
