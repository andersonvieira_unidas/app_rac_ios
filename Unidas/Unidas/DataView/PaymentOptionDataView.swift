//
//  PaymentOptionDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct PaymentOptionDataView {
    
    let model: PaymentOption
    let discount: Double?
    
    var name: String {
        switch model {
        case .payInAdvance: return NSLocalizedString("Pay in advance", comment: "Pay in advance option name")
        case .payAtPickup: return NSLocalizedString("Pay at pickup", comment: "Pay at pickup option name")
        }
    }
    
    var description: String? {
        switch model {
        case .payInAdvance:
            guard let discount = discount, let percentString = NumberFormatter.percentFormatter.string(from: NSNumber(value: discount)) else { return nil }
            let localizedString = NSLocalizedString("By paying now you'll receive a\ndiscount of %@", comment: "Pre-payment description label")
            return String(format: localizedString, percentString)
        case .payAtPickup: return NSLocalizedString("Are you in a hurry?\nWe understand.", comment: "Pay at pickup description label")
        }
    }
    
}
