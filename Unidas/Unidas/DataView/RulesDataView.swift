//
//  RulesDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation


//
//  MyReservationDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct RulesDataView {
    let userValidated: Bool
    let userAvailableToCheckin: Bool
    let financialPendency: Bool
    
    init(pendencies: Bool, userAvailableToCheckin: Bool, financialPendency: Bool) {
        self.userValidated = !pendencies
        self.userAvailableToCheckin = userAvailableToCheckin
        self.financialPendency = financialPendency
    }
}


