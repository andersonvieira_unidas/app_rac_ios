//
//  VehicleModelManufactureDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct VehicleModelManufactureDataView {
    
    let model: VehicleModelManufacture
    
    var name: String {
        return model.name
    }
    
}
