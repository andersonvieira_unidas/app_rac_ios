//
//  VehicleDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleDataView {
    
    let model: Vehicle
    
    var vehicleModel: VehicleModelDataView {
        return VehicleModelDataView(model: model.model)
    }
    
    var licensePlateWithLabel: String? {
        let localized = NSLocalizedString("License Plate: %@", comment: "License plate")
        return String(format: localized, model.licensePlate)
    }
    
    var telemetryInformation: VehicleTelemetryInformationDataView? {
        guard let lastUpdatedTelemetryInformation = model.lastUpdatedTelemetryInformation else { return nil }
        return VehicleTelemetryInformationDataView(model: lastUpdatedTelemetryInformation)
    }
    
    var group: VehicleGroupDataView? {
        guard let group = model.group else { return nil }
        return VehicleGroupDataView(model: group)
    }
}
