//
//  OpenContractDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 17/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct OpenContractDataView {
    let model: OpenContract
    
    var contractNumber: String? {
        return String(model.contractNumber)
    }
}
