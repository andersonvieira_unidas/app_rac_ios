//
//  PreAuthorizationEstimateDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 17/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct PreAuthorizationEstimateDataView {
    
    let model: PreAuthorizationEstimate
    
    var message: String? {
        guard let message = model.message else { return nil }
        return "\(message) \(value)"
    }
    
    var value: String {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.value)) ?? "\"Unable to get value\""
    }
    
}
