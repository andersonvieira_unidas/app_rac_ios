//
//  AirlineInformationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 6/20/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct AirlineInformationDataView {
    let model: AirlineInformation
    
    var description: String {
        let localizedFormatted = NSLocalizedString("Flight: %@, Company: %@", comment: "Airline information description")
        return String(format: localizedFormatted, model.flightNumber, model.company.description)
    }
    
}
