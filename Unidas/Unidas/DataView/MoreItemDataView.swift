//
//  MoreItemDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 22/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

struct MoreItemDataView {
    
    let model: MoreItem
    
    var image: UIImage? {
        return UIImage(named: model.image.lowercased())
    }
    
    var name: String {
        return NSLocalizedString(model.localizedString, comment: model.localizedString)
    }
    
    var action: String {
        return model.action
    }
    
    var isVisibleDotted: Bool {
        return model.separator
    }
    
}
