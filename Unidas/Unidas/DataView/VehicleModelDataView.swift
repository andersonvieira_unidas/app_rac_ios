//
//  VehicleModelDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleModelDataView {
    
    let model: VehicleModel
    
    var name: String? {
        return model.name
    }
    
    var illustrativePicture: URL? {
        return model.illustrativePicture
    }
    
    var manufacturer: VehicleModelManufactureDataView {
        return VehicleModelManufactureDataView(model: model.manufacture)
    }
    
    var nameWithModel: String? {
        guard let name = model.name else { return nil }
        return "\(name) \(manufacturer.name)"
    }
    
}
