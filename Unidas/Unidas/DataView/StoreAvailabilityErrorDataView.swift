//
//  StoreAvailabilityErrorDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 19/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct StoreAvailabilityErrorDataView {
    
    let model: StoreAvailabilityError
    
    var title: String? {
        return model.localizedDescription
    }
    
}
