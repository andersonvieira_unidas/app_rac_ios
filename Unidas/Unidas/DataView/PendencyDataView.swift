//
//  PendencyDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct PendencyDataView {
    let pendencieType: PendencyType
    let pendencieStatus: Bool
}

