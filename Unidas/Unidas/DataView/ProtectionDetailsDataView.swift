//
//  ProtectionDetailsDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 12/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionDetailsDataView {
    
    let model: ProtectionDetails
    
    var iconURL: URL? {
        return model.iconURL
    }
    
    var description: String {
        return model.description
    }
    
    var requiredParticipationGroups: [ProtectionParticipationGroupDataView] {
        return model.requiredParticipationGroups.map { ProtectionParticipationGroupDataView(model: $0) }
    }
    
}
