//
//  DriverLicenseDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct DriverLicenseFormDataView {
    let model: DriverLicenseForm
}
