//
//  ZipcodeDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 23/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ZipcodeDataView {
    let model: Zipcode
    
    var address: String? {
        guard let logradouro = model.nmLogradouroAbreviacao else { return nil }
        guard let logradouroTipo = model.logradouroTipo else {
            return logradouro
        }
        
        return "\(logradouroTipo) \(logradouro)"
    }
    
    var neighborhood: String? {
        return model.bairro
    }
    
    var state: String {
        return model.uf
    }
    
    var city: String {
        return model.localidade
    }
}
