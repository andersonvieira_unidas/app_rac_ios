//
//  AddressDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import InputMask

struct AddressDataView {
    
    let model: Address
    
    var firstAddressLine: String? {
        if let complement = model.complement?.localizedCapitalized {
            return "\(model.street?.localizedCapitalized ?? "") - \(complement)"
        }
        return "\(model.street?.localizedCapitalized ?? "")"
    }
    
    var secondAddressLine: String? {
        return "\(model.neighborhood?.localizedCapitalized ?? "") - \(model.city?.localizedCapitalized ?? "") / \(model.state ?? "")"
    }
    
    var formattedDistance: String? {
        return model.formattedDistance
    }
    
    var location: LocationDataView? {
        guard let location = model.location else { return nil }
        return LocationDataView(model: location)
    }
    
    func calculeDistance(at locationDataView: LocationDataView?) -> Double? {
        guard let storeLocation = location?.clLocationCoordinate, let currentLocation = locationDataView?.clLocationCoordinate else { return nil }
        
        let distance = storeLocation.distance(from: currentLocation)
        
        return distance
    }
    
    func formattedCalculeDistance(at locationDataView: LocationDataView?) -> String? {
        guard let distance = calculeDistance(at: locationDataView) else { return nil }
        
        let measurementFormatter = MeasurementFormatter()
        measurementFormatter.locale = .current
        measurementFormatter.numberFormatter.maximumFractionDigits = 2
        measurementFormatter.unitStyle = .short
        
        return measurementFormatter.string(from: Measurement(value: distance, unit: UnitLength.meters))
    }
    
    func shortFormattedCalculeDistance(at locationDataView: LocationDataView?) -> String? {
        guard let distance = calculeDistance(at: locationDataView) else { return nil }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 0
        let distanceInKm = distance / 1000
        
        return numberFormatter.string(from: NSNumber(value: distanceInKm))
    }
    
    
    var state: String? {
        return model.state
    }
    
    var city: String? {
        return model.city
    }
    
    var neighborhood: String? {
        return model.neighborhood
    }
    
    var street: String? {
        return model.street
    }
    
    var distance: String? {
        guard let distance = model.distance else { return nil }

        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 0

        return numberFormatter.string(from: NSNumber(value: distance))
    }
    
    var phoneNumber: String? {
        return model.phoneNumber
    }
}
