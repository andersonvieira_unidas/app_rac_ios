//
//  ProfileItemDataView.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

struct ProfileItemDataView {
    
    let model: ProfileItem
    
    var image: UIImage? {
        return UIImage(named: model.image.lowercased())
    }
    
    var name: String {
        return NSLocalizedString(model.localizedString, comment: model.localizedString)
    }
    
    var action: String? {
        return model.action
    }
    
    var isVisibleDotted: Bool {
        return model.separator
    }
}
