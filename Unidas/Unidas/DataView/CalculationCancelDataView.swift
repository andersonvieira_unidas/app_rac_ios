//
//  CalculationCancelDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CalculationCancelDataView {
    let model: CalculationCancel
    
    func formattedDiscount() -> String? {
        guard let returnValue = model.reservation.first?.returnValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: returnValue))
    }
}
