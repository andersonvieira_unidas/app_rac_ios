//
//  AccountViewModel.swift
//  Unidas
//
//  Created by Mateus Padovani on 18/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import PhoneNumberKit
import InputMask

struct AccountDataView {
    let model: Account
    
    var name: String {
        return model.name.localizedCapitalized
    }
    
    var firstName: String {
        var components = model.name.components(separatedBy: " ")
        
        if(components.count > 0){
            return components.removeFirst()
        }
        return model.name
    }
    
    var greatings: String {
        let localizedGreeting = NSLocalizedString("Hello, %@", comment: "User greetings")
        return String(format: localizedGreeting, name)
    }
    
    var email: String? {
        return model.email.localizedLowercase
    }
    
    var formattedMobilePhone: String {
        let phoneNumberKit = PhoneNumberKit()
        
        do {
            var mobilePhoneFmt = ""
            if !model.mobilePhone.contains("+55"){
                mobilePhoneFmt.append("+55")
                mobilePhoneFmt.append(model.mobilePhone)
            }
            let mobilePhone = try phoneNumberKit.parse(mobilePhoneFmt)
            
            return phoneNumberKit.format(mobilePhone, toType: .national, withPrefix: false)
        }
        catch {
            return model.mobilePhone
        }
    }
    
    var profileImage: URL? {
        return model.profileImage
    }
    
    var formattedDocumentNumber: String? {
        guard let mask = try? Mask(format: "[000].[000].[000]-[00]") else { return nil }
        return mask.apply(toText: CaretString(string: model.documentNumber, caretPosition: model.documentNumber.endIndex, caretGravity: .forward(autocomplete: true))).formattedText.string
    }
    
}
