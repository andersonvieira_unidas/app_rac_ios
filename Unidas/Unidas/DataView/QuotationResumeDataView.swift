//
//  QuotationResumeDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 07/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct QuotationResumeDataView {
    
    let model: QuotationResume
    
    var subtotalValue: Double? {
        guard var subtotal = totalDiaryWithFeesAndDiscountValue else { return nil }
        subtotal += [totalProtectionsValue, totalEquipmentsValue].reduce(0.0) { $0 + $1 }
        return subtotal
    }
    
    var subtotalPrePaymentValue: Double? {
        guard var value = totalPrePaymentDiaryWithFeesAndDiscountValue else { return nil }
        value += [totalProtectionsValue, totalEquipmentsValue].reduce(0.0) { $0 + $1 }
        return value
    }
    
    var totalDiaryWithFeesAndDiscount: String? {
        guard let totalDiaryWithFeesAndDiscountValue = totalDiaryWithFeesAndDiscountValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: totalDiaryWithFeesAndDiscountValue))
    }
    
    var totalDiaryWithFeesAndDiscountValue: Double? {
        guard let calculation = calculation else { return nil }
        let total = calculation.totalChargeValue + totalFeesValue - (calculation.totalDiscountValue ?? 0.0)
        return total
    }
    
    var totalWithAdministrativeTax: String? {
        guard let totalWithAdministrativeTaxValue = totalWithAdministrativeTaxValue else { return nil }
        return NumberFormatter.currencyFormatterWithouRounding.string(from: NSNumber(value: totalWithAdministrativeTaxValue))
    }
    
    var totalWithAdministrativeTaxValue: Double? {
        guard let subtotalValue = subtotalValue, let administrativeTaxValue = administrativeTaxValue else { return nil }
        return (subtotalValue + administrativeTaxValue).fractionDigits(to: 2)
    }
    
    var prePaymentTotalWithAdministrativeTaxValue: Double? {
        guard let subtotalValue = subtotalPrePaymentValue, let administrativeTaxValue = administrativeTaxPrePaymentValue else { return nil }
        return (subtotalValue + administrativeTaxValue).fractionDigits(to: 2)
    }
    
    var prePaymentTotalWithAdministrativeTax: String? {
        guard let value = prePaymentTotalWithAdministrativeTaxValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: value))
    }
    
    var totalPrePaymentDiaryWithFeesAndDiscountValue: Double? {
        guard let calculation = calculation else { return nil }
        return calculation.totalPrePaymentValue + totalFeesValue - (calculation.totalDiscountValue ?? 0.0)
    }
    
    var administrativeTax: String? {
        guard let administrativeTaxValue = administrativeTaxValue else { return nil }
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: administrativeTaxValue))
    }
    
    var administrativeTaxValue: Double? {
        guard let subtotal = subtotalValue, let administrativeTaxPercentage = administrativeTaxPercentageValue else { return nil }
        return (subtotal * administrativeTaxPercentage).roundedFractionDigits(to: 2)
    }
    
    var administrativeTaxPrePaymentValue: Double? {
        guard let value = subtotalPrePaymentValue, let administrativeTaxPercentage = administrativeTaxPercentageValue else { return 0.0 }
        return (value * administrativeTaxPercentage).roundedFractionDigits(to: 2)
    }
    
    var administrativeTaxPercentage: String? {
        guard let administrativeTaxPercentageValue = administrativeTaxPercentageValue, let adminstrative = NumberFormatter.percentFormatter.string(from: NSNumber(value: administrativeTaxPercentageValue)) else { return nil }
        let localized = NSLocalizedString("%@ of subtotal", comment: "Administrative tax label")
        return String(format: localized, adminstrative)
    }

    var administrativeTaxPercentageValue: Double? {
         guard let adminstrativeTaxPercentValue = model.quotation?.fees?.first(where: { $0.purpose == "7" })?.percentage  else { return nil }
        return Double(adminstrativeTaxPercentValue / 100.0).roundedFractionDigits(to: 2)
    }
    
    var resumeInformations: [ResumeInformationDataView] {
        var resumeInfos: [ResumeInformation] = []
        
        if let calculation = calculation {
            let diaryDescription = NSLocalizedString("Diary", comment: "Diary resume label")
            let diary = ResumeInformation(title: diaryDescription,
                                          subtitle: calculation.description,
                                          value: calculation.totalCharge)
            resumeInfos.append(diary)
            
            if calculation.hasDiscount {
                let discountDescription = NSLocalizedString("Discount", comment: "Discount resume label")
                let discount = ResumeInformation(title: discountDescription,
                                                 subtitle: calculation.totalDiscountPercentDescription,
                                                 value: calculation.totalDiscountDescription)
                resumeInfos.append(discount)
            }
        }
        
        let feesResumeInformation = fees.filter { !($0.calculation?.totalChargeValue.isZero ?? true)  }
                                        .map { (fee) -> ResumeInformation in
            return ResumeInformation(title: fee.description,
                                     subtitle: fee.calculation?.description,
                                     value: fee.amount)
        }
        resumeInfos.append(contentsOf: feesResumeInformation)
        
        return resumeInfos.map { ResumeInformationDataView(model: $0) }
    }

    var fees: [FeeDataView] {
        return feesModel.map { FeeDataView(model: $0) }
    }
    
    private var totalFeesValue: Double {
        return feesModel.reduce(0.0) { $0 + ($1.amount ?? 0.0) }
    }
    
    private var feesModel: [Fee] {
        guard let fees = model.quotation?.fees else { return [] }
        let filteredFees = fees.filter { $0.purpose != "7" }
        return filteredFees
    }
    
    var protections: [ProtectionsDataView] {
        return protectionsModel.map { ProtectionsDataView(model: $0) }
    }
    
    var totalProtections: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: totalProtectionsValue))
    }
    
    private var totalProtectionsValue: Double {
        return protectionsModel.reduce(0.0) { $0 + ($1.amount ?? 0.0) }
    }
    
    private var protectionsModel: [PricedCoverages] {
        guard let protections = model.selectedProtections else { return [] }
        return protections
    }
    
    var equipments: [EquipmentDataView] {
        return equipmentsModel.map { EquipmentDataView(model: $0) }
    }
    
    var totalEquipments: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: totalEquipmentsValue))
    }
    
    private var totalEquipmentsValue: Double {
        let value = equipmentsModel.reduce(0.0, { (result, equip) -> Double in
            guard let amount = equip.charge?.amount else {
                return 0.0
            }
            guard let selectedQuantity = equip.selectedQuantityItem, selectedQuantity > 0 else { return result + amount }
            return result + amount * Double(selectedQuantity)
        })
        return value
    }
    
    private var equipmentsModel: [PricedEquips] {
        guard let equipments = model.selectedEquipments else { return [] }
        return equipments
    }
    
    var pickUp: GarageReservationDetailsDataView? {
        guard let details = model.pickUp else { return nil }
        return GarageReservationDetailsDataView(model: details)
    }
    
    var `return`: GarageReservationDetailsDataView? {
        guard let details = model.return else { return nil }
        return GarageReservationDetailsDataView(model: details)
    }
    
    var calculation: CalculationDataView? {
        return quotation?.vehicleChargeCalculation
    }
    
    var quotation: QuotationsDataView? {
        guard let quotation = model.quotation else { return nil }
        return QuotationsDataView(model: quotation)
    }
    
    var mileageDescription: String? {
        guard let unlimitedMileage = model.quotation?.rateDistance?.unlimited else { return nil }
        if unlimitedMileage {
            return NSLocalizedString("Unlimited mileage", comment: "Unlimited milegae description")
        } else {
            return NSLocalizedString("Limited mileage", comment: "Limited mileage description")
        }
    }
    
    var isUpgrade: Bool {
        return upgradeDescription != nil
    }
    
    var upgradeDescription: String? {
        let charset = CharacterSet(charactersIn: ":;")
        guard let group = model.quotation?.rateQualifier?.promoDesc?.components(separatedBy: charset).dropFirst().joined() else { return nil }
        let localized = NSLocalizedString("Upgrade to group: %@", comment: "Upgrade to group promotional text")
        return String(format: localized, group)
    }
    
    var voucherDescription: String? {
        if let hasDiscount = calculation?.hasDiscount, hasDiscount {
            return NSLocalizedString("Promotional Code Applied", comment: "Promotional code applied label")
        } else if isUpgrade {
            return upgradeDescription
        }
        return nil
    }
    
    var showsVoucherDescription: Bool {
        return voucherDescription != nil
    }
    
}
