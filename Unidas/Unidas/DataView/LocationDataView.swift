//
//  LocationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import CoreLocation

struct LocationDataView {
    
    let model: Location
    
    private var latitude: Double {
        return model.latitude
    }
    private var longitude: Double {
        return model.longitude
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var clLocationCoordinate: CLLocation {
        return CLLocation(latitude: model.latitude, longitude: model.longitude)
    }
    
}
