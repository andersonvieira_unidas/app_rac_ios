//
//  PurchaseDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct PurchaseDataView {
    let model: Purchase
    
    var name: String? {
        return model.nome
    }
    
    var description: String? {
        return model.descricao?.trimmingCharacters(in: CharacterSet(charactersIn: " "))
    }
    
    var value: String? {
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.valorUnitario))
    }
}
