//
//  CreditCardReservePaymentDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CreditCardReservePaymentDataView {
    let model: CreditCardReservePayment
    
    var card: ListCardDataView {
        return ListCardDataView(model: model.card)
    }
    
    var installmentsOption: String? {
        return model.installmentsOption.description
    }
    
    var installmentValue: String? {
        let installmentValue = model.amount / Double(model.installmentsOption)
        return NumberFormatter.currencyFormatter.string(from: NSNumber(value: installmentValue))
    }
    
    var installmentDescription: String? {
        guard let option = installmentsOption, let value = installmentValue else { return nil }
        return "\(option)x \(value)"
    }
    
}
