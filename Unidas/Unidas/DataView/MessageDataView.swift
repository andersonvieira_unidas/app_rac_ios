//
//  MessageDataView.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct MessageDataView {
    let model: Message
    
    static let dateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSS" //27/11/2019 13:28:41
        return dateFormatter
    }()
    
    func getFormattedDate(string: String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"
        
        guard let date = dateFormatterGet.date(from: string) else {return ""}
        return dateFormatterPrint.string(from: date)
    }
    
    var title: String? {
        return model.title
    }
    
    var message: String? {
        return model.message
    }
    
    var date: String? {
        //guard let date = MessageDataView.shortDateFormatter.date(from: model.date) else { return nil }
        //print(date)
        let formattedDate = getFormattedDate(string: model.date)
        return formattedDate
    }
    
    var read: Bool {
        return (model.read == 1)
    }
}
