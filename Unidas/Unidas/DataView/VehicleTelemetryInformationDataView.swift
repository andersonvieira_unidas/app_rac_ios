//
//  VehicleTelemetryInformationDataView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleTelemetryInformationDataView {
    
    let model: VehicleTelemetryInformation
    
    var estimatedFuelLevel: String? {
        return NumberFormatter.percentNumberFormatter.string(from: NSNumber(floatLiteral: model.estimatedFuelLevel))
    }
    
    var estimatedFuelLevelDescription: String? {
        guard let estimatedFuelLevel = estimatedFuelLevel else {
            return nil
        }
        let localizedString = NSLocalizedString("%@ of fuel", comment: "Fuel level description")
        return String(format: localizedString, estimatedFuelLevel)
    }
}
