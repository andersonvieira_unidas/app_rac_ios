//
//  CardPresentationAnimationController.swift
//  Box Delivery
//
//  Created by Leonardo Oliveira on 07/11/16.
//  Copyright © 2016 Box Delivery. All rights reserved.
//

import UIKit

class CardPresentationAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    let presenting: Bool
    let duration: TimeInterval
    
    init(presenting: Bool, duration: TimeInterval = 0.5) {
        self.presenting = presenting
        self.duration = duration
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if presenting {
            animatePresentationWith(transitionContext: transitionContext)
        }
        else {
            animateDismissalWith(transitionContext: transitionContext)
        }
    }
    
    func animatePresentationWith(transitionContext: UIViewControllerContextTransitioning) {
        guard
            let presentedViewController = transitionContext.viewController(forKey: .to),
            let presentedView = transitionContext.view(forKey: .to) else {
                return
        }
        let containerView = transitionContext.containerView
        
        presentedView.frame = transitionContext.finalFrame(for: presentedViewController)
        presentedView.center.y = containerView.frame.height + presentedView.frame.height / 2
        
        let topRoundCornersMask = CAShapeLayer()
        topRoundCornersMask.path = UIBezierPath(roundedRect: presentedView.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 13, height: 13)).cgPath
        presentedView.layer.mask = topRoundCornersMask
        presentedView.layer.masksToBounds = true
        
        containerView.addSubview(presentedView)
        
        if transitionContext.isInteractive {
            UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .curveLinear, animations: {
                presentedView.center.y = (containerView.frame.size.height - presentedView.frame.size.height) / 2 + containerView.center.y
            }) { (completed) in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        } else {
            UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
                presentedView.center.y = (containerView.frame.size.height - presentedView.frame.size.height) / 2 + containerView.center.y
            }, completion: {(completed: Bool) -> Void in
                transitionContext.completeTransition(completed)
            })
        }
        
    }
    
    func animateDismissalWith(transitionContext: UIViewControllerContextTransitioning) {
        
        guard let presentedView = transitionContext.view(forKey: .from) else {
            return
        }
        
        let containerView = transitionContext.containerView
        if transitionContext.isInteractive {
            UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .curveLinear, animations: {
                presentedView.center.y = containerView.frame.height + presentedView.frame.height / 2
            }) { (completed) in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
        } else {
            UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .allowUserInteraction, animations: {
                presentedView.center.y = containerView.frame.height + presentedView.frame.height / 2
            }, completion: {(completed: Bool) -> Void in
                transitionContext.completeTransition(completed)
            })
        }
        
    }
}
