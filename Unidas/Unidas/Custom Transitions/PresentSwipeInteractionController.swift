//
//  PresentSwipeInteractionController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 4/26/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class PresentSwipeInteractionController: UIPercentDrivenInteractiveTransition {
    
    var isInteractionInProgress: Bool = false
    
    private var shouldCompleteTransition: Bool = false
    private var currentY: CGFloat = 0.0
    private weak var viewController: (UIViewController & PresentingSwipeInteractionControllerDraggable)?
    
    init(viewController: UIViewController & PresentingSwipeInteractionControllerDraggable) {
        self.viewController = viewController
        super.init()
        prepareGestureRecognizer(in: viewController.draggableView)
    }
    
    func prepareGestureRecognizer(in view: UIView) {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    @objc func handleGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view?.superview)
        var progress = -(translation.y - currentY) / (viewController?.view.frame.height ?? 0.0)
        let velocity = gestureRecognizer.velocity(in: gestureRecognizer.view?.superview)
        progress = min(max(progress, 0.0), 1.0)
        
        switch gestureRecognizer.state {
        case .possible:
            break
        case .began:
            isInteractionInProgress = true
            guard let viewController = viewController else { return }
            currentY = viewController.draggableView.frame.height - gestureRecognizer.location(in: viewController.draggableView).y + 50
            let viewControllerToPresent = viewController.viewControllerToPresent()
            viewController.present(viewControllerToPresent, animated: true) {
                viewController.didPresent(viewController: viewControllerToPresent as! DraggableViewController)
            }
            break
        case .changed:
            shouldCompleteTransition = progress > 0.5 || velocity.y > 0
            update(progress)
            break
        case .ended:
            isInteractionInProgress = false
            if shouldCompleteTransition {
                finish()
            } else {
                cancel()
            }
            break
        case .cancelled:
            isInteractionInProgress = false
            cancel()
            break
        case .failed:
            break
        @unknown default:
            break
        }
    }

    
}
