//
//  CardPresentationTransitioningDelegate.swift
//  Box Delivery
//
//  Created by Leonardo Oliveira on 07/11/16.
//  Copyright © 2016 Box Delivery. All rights reserved.
//

import UIKit

class CardPresentationTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    var dismissalInteractionController: DismissSwipeInteractionController?
    var presentationInteractionController: PresentSwipeInteractionController?
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return CardPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardPresentationAnimationController(presenting: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CardPresentationAnimationController(presenting: false)
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard let interactionController = presentationInteractionController,
            interactionController.isInteractionInProgress else { return nil }
        return interactionController
    }
    
    
    
}
