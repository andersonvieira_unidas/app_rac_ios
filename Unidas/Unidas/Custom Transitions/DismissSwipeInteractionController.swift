//
//  DismissSwipeInteractionController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 25/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class DismissSwipeInteractionController: UIPercentDrivenInteractiveTransition {

    var isInteractionInProgress: Bool = false
    
    private var shouldCompleteTransition: Bool = false
    private unowned var viewController: UIViewController
    
    init(viewController: DraggableViewController) {
        self.viewController = viewController
        super.init()
        prepareGestureRecognizer(in: viewController.draggableView)
    }
    
    func prepareGestureRecognizer(in view: UIView) {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    @objc func handleGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: gestureRecognizer.view?.superview)
        var progress = translation.y / viewController.view.frame.height
        let velocity = gestureRecognizer.velocity(in: gestureRecognizer.view?.superview)
        progress = min(max(progress, 0.0), 1.0)
        
        switch gestureRecognizer.state {
        case .possible:
            break
        case .began:
            isInteractionInProgress = true
            viewController.dismiss(animated: true)
            break
        case .changed:
            shouldCompleteTransition = progress > 0.5 || velocity.y > 0
            update(progress)
            break
        case .ended:
            isInteractionInProgress = false
            if shouldCompleteTransition {
                finish()
            } else {
                cancel()
            }
            break
        case .cancelled:
            isInteractionInProgress = false
            cancel()
            break
        case .failed:
            break
        @unknown default:
            break
        }
    }
    
}
