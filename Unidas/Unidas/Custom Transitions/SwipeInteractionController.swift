//
//  SwipeInteractionController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 4/26/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

typealias DraggableViewController = UIViewController & SwipeInteractionControllerDraggable

protocol SwipeInteractionControllerDraggable: class {
    var draggableView: UIView { get }
}

protocol PresentingSwipeInteractionControllerDraggable: SwipeInteractionControllerDraggable {
    
    func viewControllerToPresent() -> UIViewController
    func didPresent(viewController: DraggableViewController)
    
}
