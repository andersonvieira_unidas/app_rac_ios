//
//  CardPresentationController.swift
//  Box Delivery
//
//  Created by Leonardo Oliveira on 07/11/16.
//  Copyright © 2016 Box Delivery. All rights reserved.
//

import UIKit

class CardPresentationController: UIPresentationController {
    
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            let marginTop: CGFloat = 80.0
            return CGRect(x: 16, y: marginTop, width: containerView!.bounds.width - 32, height: containerView!.bounds.height - marginTop)
        }
    }
    
    lazy var dimmingView: UIView = {
        let view = UIView(frame: self.containerView!.bounds)
        view.backgroundColor = UIColor.black
        view.alpha = 0.0
        return view
    }()
    
    override func presentationTransitionWillBegin() {
        guard let containerView = containerView, let presentedView = presentedView else {
            return
        }
        
        dimmingView.frame = containerView.bounds
        containerView.addSubview(dimmingView)
        containerView.addSubview(presentedView)
        
        let transitionCoordinator = presentedViewController.transitionCoordinator
        transitionCoordinator?.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext) in
            self.dimmingView.alpha = 0.5
        }, completion: nil)
        
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            self.dimmingView.removeFromSuperview()
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let transitionCoordinator = presentedViewController.transitionCoordinator
        transitionCoordinator?.animate(alongsideTransition: { (context: UIViewControllerTransitionCoordinatorContext) in
            self.dimmingView.alpha = 0.0
        }, completion: nil)
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            self.dimmingView.removeFromSuperview()
        }
    }
}
