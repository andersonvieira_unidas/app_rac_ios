//
//  CustomAlert.swift
//  ModalView
//
//  Created by Anderson Vieira on 20/07/2020.
//

import UIKit

@objc protocol MultipleChoiceAlertDelegate: class {
    @objc func choiceFirstTap(tag: Int)
    @objc func choiceSecondTap(tag: Int)
}

class MultipleChoiceAlert: UIView, Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    var delegate: MultipleChoiceAlertDelegate?
    
    lazy var fontBold = {
        return UIFont.medium(ofSize: 14)
    }()
    
    lazy var okText = {
        return NSLocalizedString("OK", comment: "")
    }()
    
    lazy var closeText = {
        return NSLocalizedString("Close Button", comment: "")
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 4
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return imageView
    }()
    
    private lazy var headerView: UIView = {
        let headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.themeButton
        headerView.clipsToBounds = true
        
        return headerView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.medium(ofSize: 14.0)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.textColor = UIColor.themeTitle
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let subTitleLabel = UILabel()
        
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.textAlignment = .center
        subTitleLabel.lineBreakMode = .byTruncatingTail
        subTitleLabel.numberOfLines = 0
        subTitleLabel.font = UIFont.regular(ofSize: 14)
        subTitleLabel.adjustsFontSizeToFitWidth = true
       // subTitleLabel.minimumScaleFactor = 0.5
        subTitleLabel.textColor = UIColor.themeSubTitle
        return subTitleLabel
    }()
    
    
    private lazy var buttonStackView: UIStackView = {
        let buttonStackView = UIStackView()
        
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.axis = .horizontal
        buttonStackView.distribution = .fillEqually
        buttonStackView.alignment = .center
        buttonStackView.spacing = 0
        return buttonStackView
    }()
    
    private lazy var firstButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(UIColor.themeButton, for: .normal)
        button.titleLabel?.font = UIFont.bold(ofSize: 14)
        
        return button
    }()
    
    private lazy var secondButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.textAlignment = .center
        button.setTitleColor(UIColor.themeButton, for: .normal)
        button.titleLabel?.font = UIFont.bold(ofSize: 14)
     
        return button
    }()
    
    private lazy var separatorLineView: UIView = {
        let separatorLineView = UIView()
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
        return separatorLineView
    }()
    
    convenience init(title:String, subTitle: String, firstChoiceButtonLabel: String, secondChoiceButtonLabel: String, image: UIImage, firstChoiceButtonEnabled: Bool? = true, secondChoiceButtonEnabled: Bool? = true, firstChoiceButtonColor: UIColor? = UIColor.themeButton, secondChoiceButtonColor: UIColor? = UIColor.themeButton, firstButtonTag: Int? = 0, secondButtonTag: Int? = 0) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title, subTitle: subTitle, image: image, firstChoiceButtonLabel: firstChoiceButtonLabel, secondChoiceButtonLabel: secondChoiceButtonLabel, firstChoiceButtonEnabled: firstChoiceButtonEnabled, secondChoiceButtonEnabled: secondChoiceButtonEnabled, firstChoiceButtonColor: firstChoiceButtonColor, secondChoiceButtonColor: secondChoiceButtonColor, firstButtonTag: firstButtonTag, secondButtonTag: secondButtonTag)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(title:String,
                    subTitle: String,
                    image: UIImage,
                    firstChoiceButtonLabel: String? = nil,
                    secondChoiceButtonLabel: String? = nil,
                    firstChoiceButtonEnabled: Bool? = true,
                    secondChoiceButtonEnabled: Bool? = true,
                    firstChoiceButtonColor: UIColor? = UIColor.themeButton,
                    secondChoiceButtonColor:UIColor? = UIColor.themeButton,
                    firstButtonTag: Int? = 0,
                    secondButtonTag: Int? = 0 ){
        
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeAlert)))
        addSubview(backgroundView)
        
        dialogView.frame.origin = CGPoint(x: 30, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-60, height: 266)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 10
        addSubview(dialogView)
        
        //set data
        imageView.image = image.imageWithColor(color: UIColor.white)
        imageView.backgroundColor = UIColor.clear
        titleLabel.text = title
        subTitleLabel.text = subTitle
        
        firstButton.isEnabled = firstChoiceButtonEnabled ?? true
        firstButton.setTitle(firstChoiceButtonLabel, for: .normal)
        firstButton.titleLabel?.adjustsFontSizeToFitWidth = true
        firstButton.setTitleColor(firstChoiceButtonColor, for: .normal)
        firstButton.setTitleColor(#colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1), for: .disabled)
        firstButton.tag = firstButtonTag ?? 0
        firstButton.addTarget(self, action: #selector(tapFirstChoice(_:)), for: .touchUpInside)
        
        secondButton.isEnabled = secondChoiceButtonEnabled ?? true
        secondButton.setTitle(secondChoiceButtonLabel, for: .normal)
        secondButton.titleLabel?.adjustsFontSizeToFitWidth = true
        secondButton.setTitleColor(secondChoiceButtonColor, for: .normal)
        secondButton.setTitleColor(#colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1), for: .disabled)
        secondButton.tag = secondButtonTag ?? 0
        secondButton.addTarget(self, action: #selector(tapSecondChoice(_:)), for: .touchUpInside)
            
        if firstButton.isEnabled == false {
            buttonStackView.addArrangedSubview(secondButton)
        }else if secondButton.isEnabled == false {
            buttonStackView.addArrangedSubview(firstButton)
        }else{
            buttonStackView.addArrangedSubview(firstButton)
            buttonStackView.addArrangedSubview(secondButton)
        }
    
        headerView.addSubview(imageView)
        dialogView.addSubview(headerView)
        dialogView.addSubview(titleLabel)
        dialogView.addSubview(subTitleLabel)
        dialogView.addSubview(separatorLineView)
        dialogView.addSubview(buttonStackView)
        
        NSLayoutConstraint.activate([
            headerView.heightAnchor.constraint(equalToConstant: 60),
            headerView.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 0),
            headerView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 0),
            headerView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: 0),
            
            imageView.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 10),
            imageView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
            imageView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 50),
            
            titleLabel.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            titleLabel.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            titleLabel.bottomAnchor.constraint(equalTo: subTitleLabel.topAnchor, constant: -20),
         
            subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            subTitleLabel.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            subTitleLabel.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            subTitleLabel.bottomAnchor.constraint(equalTo: separatorLineView.topAnchor, constant: -20),
            
            separatorLineView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            separatorLineView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            separatorLineView.heightAnchor.constraint(equalToConstant: 1),
            
            buttonStackView.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 20),
            buttonStackView.bottomAnchor.constraint(equalTo: dialogView.bottomAnchor, constant: -20),
            buttonStackView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            buttonStackView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            buttonStackView.heightAnchor.constraint(equalToConstant: 30),

        ])
    }
 
    @objc func tapFirstChoice(_ sender: UIButton){
        dismiss(animated: true)        
        delegate?.choiceFirstTap(tag: sender.tag)
    }
    
    @objc func tapSecondChoice(_ sender: UIButton){
        dismiss(animated: true)
        delegate?.choiceSecondTap(tag: sender.tag)
    }
    
    @objc func closeAlert(){
        dismiss(animated: true)
    }
}
