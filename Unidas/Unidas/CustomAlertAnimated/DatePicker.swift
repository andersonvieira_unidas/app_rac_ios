//
//  DatePicker.swift
//  Unidas
//
//  Created by Anderson Vieira on 01/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol DatePickerDelegate: class {
    func setDate(date: Date, type: DateType)
    func close()
}

class DatePicker: UIView, Modal {
    var backgroundView = UIView()
    var delegate: DatePickerDelegate?
    var type: DateType?
    
    lazy var dialogView: UIView = {
        let dialogView = UIView()
        dialogView.translatesAutoresizingMaskIntoConstraints = false
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 10
        return dialogView
    }()
    
    private lazy var headerView: UIView = {
        let headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.clipsToBounds = true
        return headerView
    }()
    
    private lazy var headerStackView: UIStackView = {
        let headerStackView = UIStackView()
        headerStackView.translatesAutoresizingMaskIntoConstraints = false
        headerStackView.distribution = .fillEqually
        headerStackView.axis = .vertical
        return headerStackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.font = UIFont.bold(ofSize: 12)
        titleLabel.numberOfLines = 1
        titleLabel.textColor = UIColor.themeTitle
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let subTitleLabel = UILabel()
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.textAlignment = .center
        subTitleLabel.lineBreakMode = .byTruncatingTail
        subTitleLabel.numberOfLines = 1
        subTitleLabel.font = UIFont.regular(ofSize: 12)
        subTitleLabel.textColor = UIColor.themeSubTitle
        return subTitleLabel
    }()
    
    public lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.backgroundColor = UIColor.init(named: "background")
        datePicker.datePickerMode = .dateAndTime

        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.calendar = .current
        }
        return datePicker
    }()
    
    private lazy var separatorLineView: UIView = {
        let separatorLineView = UIView()
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
        return separatorLineView
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let buttonStackView = UIStackView()
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.distribution = .fillEqually
        buttonStackView.axis = .horizontal
        return buttonStackView
    }()
    
    private lazy var buttonSelect: UIButton = {
        let buttonSelect = UIButton()
        buttonSelect.setTitleColor(UIColor.init(named: "tintColor"), for: .normal)
        buttonSelect.setTitle(NSLocalizedString("Select", comment: ""), for: .normal)
        buttonSelect.titleLabel?.font = UIFont.regular(ofSize: 14)
        buttonSelect.addTarget(self, action: #selector(changed), for: .touchUpInside)
    
        return buttonSelect
    }()
    
    private lazy var buttonCancel: UIButton = {
        let buttonCancel = UIButton()
        buttonCancel.setTitleColor(UIColor.init(named: "tintColor"), for: .normal)
        buttonCancel.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        buttonCancel.titleLabel?.font = UIFont.bold(ofSize: 14)
        buttonCancel.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        
        return buttonCancel
    }()
    
    convenience init(title:String, subTitle: String, minuteInterval: Int, minimumDate: Date?, date: Date, type: DateType) {
        self.init(frame: UIScreen.main.bounds)
        self.type = type
        initialize(title: title, subTitle: subTitle, minuteInterval: minuteInterval, minimumDate: minimumDate, date: date, type: type)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(title:String, subTitle: String, minuteInterval: Int, minimumDate: Date?, date: Date, type: DateType){
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapCloseButton)))
        addSubview(backgroundView)
        addSubview(dialogView)
        
        //set data
        titleLabel.text = title
        subTitleLabel.text = subTitle
        datePicker.minimumDate = minimumDate
        datePicker.date = date
        datePicker.minuteInterval = minuteInterval
        
        headerStackView.addArrangedSubview(titleLabel)
        headerStackView.addArrangedSubview(subTitleLabel)
        headerView.addSubview(headerStackView)
        
        dialogView.addSubview(headerView)
        dialogView.addSubview(separatorLineView)
        dialogView.addSubview(datePicker)
        
        buttonStackView.addArrangedSubview(buttonCancel)
        buttonStackView.addArrangedSubview(buttonSelect)
       
        dialogView.addSubview(buttonStackView)
        
        NSLayoutConstraint.activate([
            dialogView.bottomAnchor.constraint(equalTo: backgroundView.saferAreaLayoutGuide.bottomAnchor, constant: -10),
            dialogView.leftAnchor.constraint(equalTo: backgroundView.leftAnchor, constant: 10),
            dialogView.rightAnchor.constraint(equalTo: backgroundView.rightAnchor, constant: -10),
            dialogView.heightAnchor.constraint(equalToConstant: 300),
            
            headerView.heightAnchor.constraint(equalToConstant: 60),
            headerView.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 0),
            headerView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 0),
            headerView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: 0),
            
            headerStackView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor, constant: 0),
            headerStackView.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: 0),
            
            separatorLineView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            separatorLineView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            separatorLineView.heightAnchor.constraint(equalToConstant: 1),
            separatorLineView.topAnchor.constraint(equalTo: headerStackView.bottomAnchor, constant: 10),
            
            datePicker.topAnchor.constraint(equalTo: separatorLineView.bottomAnchor, constant: 0),
            datePicker.bottomAnchor.constraint(equalTo: buttonStackView.topAnchor, constant: 0),
            datePicker.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 0),
            datePicker.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: 0),
            
            buttonStackView.bottomAnchor.constraint(equalTo: dialogView.bottomAnchor, constant: -5),
            buttonStackView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 0),
            buttonStackView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: 0),
            buttonStackView.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    @objc func changed() {
        guard let type = type else { return }
        delegate?.setDate(date: datePicker.date, type: type)
        self.dismiss(animated: true)
    }
    
    @objc func tapCloseButton(){
        self.dismiss(animated: true)
        delegate?.close()
    }
}
