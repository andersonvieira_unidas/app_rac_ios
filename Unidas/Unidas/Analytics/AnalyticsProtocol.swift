//
//  AnalyticsProtocol.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

enum AnalyticsEvents {
    
    case signUpPersonalDataStep,
    signUpSMSConfirmationStep,
    signUpResidentialDataStep,
    signUpResidencialDataContinueLater,
    signUpPersonalPhoto,
    signUpPersonalPhotoContinueLater,
    signUpDriverLicense,
    signUpDriverLicenseLater,
    signUpCreditCard,
    signUpCreditCardLater,
    signUpSignature,
    signUpSignatureLater,
    signUpReady,
    cancelReservation,
    openReservation,
    selectedPayment,
    protectionsAndEquipments,
    finishReservation,
    changeReservation,
    activateWallet,
    documentOcrError,
    selfieError,
    showCharacteristic,
    ecommercePurchase,
    addPaymentoInfo,
    forgotPassword,
    firstOpenAfterUpdate,
    sessionEnd,
    promoCode
    
    var name: String {
        switch self {
        case .signUpPersonalDataStep: return "sign_up_personal_data"
        case .signUpSMSConfirmationStep: return "sign_up_sms_confirmation"
        case .signUpResidentialDataStep: return "sign_up_residencial_data"
        case .signUpResidencialDataContinueLater: return "sign_up_residencial_data_continue_later"
        case .signUpPersonalPhoto: return "sign_up_personal_photo"
        case .signUpPersonalPhotoContinueLater: return "sign_up_personal_photo_continue_later"
        case .signUpDriverLicense: return "sign_up_driver_license"
        case .signUpDriverLicenseLater: return "sign_up_driver_license_later"
        case .signUpCreditCard: return "sign_up_credit_card"
        case .signUpCreditCardLater: return "sign_up_credit_card_later"
        case .signUpSignature: return "sign_up_signature"
        case .signUpSignatureLater: return "sign_up_signature_later"
        case .signUpReady: return "sign_up_ready"
        case .cancelReservation: return "cancel_reservation"
        case .openReservation: return "open_reservation"
        case .selectedPayment: return "selected_payment"
        case .protectionsAndEquipments: return "protections_and_equipments"
        case .finishReservation: return "finish_reservation"
        case .changeReservation: return "change_reservation"
        case .activateWallet: return "activate_wallet"
        case .documentOcrError: return "document_ocr_error"
        case .selfieError: return "selfie_error"
        case .showCharacteristic: return "show_characteristic"
        case .ecommercePurchase: return "ecommerce_purchase"
        case .addPaymentoInfo: return "add_paymento_info"
        case .forgotPassword: return "forgot_password"
        case .firstOpenAfterUpdate: return "first_open_after_update"
        case .sessionEnd: return "session_end"
        //case .openReservation: return "open_reservation"
        case .promoCode: return "promo_code"
        }
    }
}

enum AnalyticsUserProperty {
    
    case documentNumber
     
     var name: String? {
         switch self {
         case .documentNumber: return "user_document_number"
         }
     }
}

protocol AnalyticsProtocol: class {
    func addEvent(withEvent event: String, withValue value: String)
    
    func addEvent(withEvent event: AnalyticsEvents, withValue value: String)
    
    func addEvent(withEvent event: String, withValue values: [String: Any])
    
    func addEvent(withEvent event: String, withValue values: [AnalyticsEvents: String])

    func addUserEvent(withProperty property: String, withValue value: String)
    
    func addUserEvent(withProperty property: AnalyticsUserProperty, withValue value: String)
}
