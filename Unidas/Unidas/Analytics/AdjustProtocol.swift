//
//  AdjustProtocol.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 11/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation



enum AdjustEvents {
    
    case addToCart,
    addToWishList,
    findLocation,
    initiatedCheckout,
    purchase
    
    var hash: String {
        switch self {
        case .addToCart: return "xft469"
        case .addToWishList: return "roobyq"
        case .findLocation: return "9ta3iy"
        case .initiatedCheckout: return "g28k8x"
        case .purchase: return "3ddm8r"
        }
    }
}


protocol AdjustProtocol: class {
    func addEvent(withEvent event: AdjustEvents, withValue value: String)
    func addEvent(withEvent event: AdjustEvents, withValue values: [String: String], amount: Double?, currency: String?)
}
