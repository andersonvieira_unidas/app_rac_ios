//
//  AdjustAnalyticsService.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 11/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import Adjust

class AdjustAnalyticsService: NSObject, AdjustProtocol {
   
    func addEvent(withEvent event: AdjustEvents, withValue value: String) {
        let adjustEvent = ADJEvent(eventToken: event.hash)
        Adjust.trackEvent(adjustEvent)
    }

    func addEvent(withEvent event: AdjustEvents, withValue values: [String : String], amount: Double? = nil, currency: String? = nil) {
        let adjustEvent = ADJEvent(eventToken: event.hash)
        
        for parameters in values{
            adjustEvent?.addPartnerParameter(parameters.key, value: parameters.value)
        }
        
        if let amount = amount, let currency = currency {
            adjustEvent?.setRevenue(amount, currency: currency)
        }
    
        Adjust.trackEvent(adjustEvent)
    }
}
