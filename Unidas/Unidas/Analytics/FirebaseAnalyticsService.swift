//
//  FirebaseAnalyticsService.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation
import FirebaseAnalytics

class FirebaseAnalyticsService: NSObject, AnalyticsProtocol {
    
    func addEvent(withEvent event: String, withValue value: String) {
        Analytics.logEvent(event, parameters: [event: value as NSObject])
    }
    
    func addEvent(withEvent event: AnalyticsEvents, withValue value: String) {
        Analytics.logEvent(event.name, parameters: [event.name: value as NSObject])
    }
    
    func addEvent(withEvent event: AnalyticsEvents, withValue values: [String : Any]) {
        Analytics.logEvent(event.name, parameters: values)
    }
    
    func addEvent(withEvent event: String, withValue values: [String : Any]) {
        Analytics.logEvent(event, parameters: values)
    }
    
    func addEvent(withEvent event: String, withValue values: [AnalyticsEvents : String]) {
        var parameters: [String: String] = [:]
        
        for event in values {
            parameters[event.key.name] =  event.value
        }
        
        Analytics.logEvent(event, parameters: parameters)
    }
    
    func addUserEvent(withProperty name: String, withValue value: String) {
        Analytics.setUserProperty(value, forName: name)
    }
    
    func addUserEvent(withProperty userProperty: AnalyticsUserProperty, withValue value: String) {
        guard let name = userProperty.name else { return }
        Analytics.setUserProperty(value, forName: name)
    }
}
