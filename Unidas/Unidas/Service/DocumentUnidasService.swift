//
//  DocumentUnidasService.swift
//  Unidas
//
//  Created by Mateus Padovani on 24/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class DocumentUnidasService: Service {
  
    func generateDocument(contractNumber: Int, pickupGarage: String, response: EmptyCustomResponse) {
        let url = "\(endPointURL)documento/SalvarDocumentoRA"
        
        let parameters: Parameters = [
            "R_ANUM": contractNumber,
            "R_ALOJRET": pickupGarage,
            "DocumentoTipoId": 14,
            "DocumentoStatusId": 2,
            "DocumentoSolicitacaoSistema": "AppRAC",
            "DocumentoSolicitacaoFuncionalidade": "GerarRA",
            "DocumentoSolicitacaoUsuario": "AppRAC",
            "DocumentoSolicitacaoAcao": "GerarPDF",
            "TipoExtensao": 2
        ]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)

    }
    
    func fetch(contractNumber: String, pickupGarage: String, response: ModelResponse<[String]>) {
        let url = "\(endPointURL)documento/ConsultarDocumentoRA?r_anum=\(contractNumber)&r_alojret=\(pickupGarage)&documentoTipoId=14&tipoRetorno=2"
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
}
