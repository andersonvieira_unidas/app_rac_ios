//
//  ReservationService.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire
import Foundation
class ReservationService: Service {
    
    func make(reservation: Reservation, user: UserResponse, paymentInformation: CreditCardReservePayment?, and reservationUUID: UUID, response: ModelResponse<NewReservationResponse>) {
        
       let url = endPointURL.appendingPathComponent("V4")
            .appendingPathComponent("Reserva")
            .appendingPathComponent("Incluir")
        
      // let url = "https://run.mocky.io/v3/58bdabc4-227c-496c-a67b-7d89d8f4eed6"
        
        let parameters = NewReservationRequest(reservation: reservation, user: user, paymentInformation: paymentInformation, uuid: reservationUUID).dictionaryRepresentation
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func update(reservation: Reservation, user: UserResponse,
                paymentInformation: CreditCardReservePayment?,
                and reservationUUID: UUID,
                response: ModelResponse<UpdateReservationResponse>) {
        
        let url = endPointURL.appendingPathComponent("v3")
            .appendingPathComponent("reserva")
            .appendingPathComponent("alterar")
        
        let parameters = NewReservationRequest(reservation: reservation,
                                               user: user,
                                               paymentInformation: paymentInformation,
                                               uuid: reservationUUID).dictionaryRepresentation
        request(url,
                method: .put,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func makePayment(reservation: Reservation, user: UserResponse,
                     paymentInformation: CreditCardReservePayment?,
                     and reservationUUID: UUID,
                     response: ModelResponse<PaymentReservationResponse>) {
        
        guard let reservationNumber = reservation.id else { return }
        
        let url = endPointURL.appendingPathComponent("V4")
            .appendingPathComponent("Reserva").appendingPathComponent("\(reservationNumber)").appendingPathComponent("Pagamento")
        
        let parameters = ["cartaoID":paymentInformation?.card.tkncodseq ?? 0,
                          "cartaoCodigoSeguranca": paymentInformation?.securityCode ?? "",
                          "parcela":paymentInformation?.installmentsOption ?? 0,
                          "valor":paymentInformation?.amount ?? 0,
                          "aceitoTermoLocacao": reservation.acceptTermLocation ?? false,
                          "aceitoTermoReembolso": reservation.acceptTermRefound ?? false] as [String : Any]
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func fetchNotifications(documentNumber: String, response: ModelResponse<[NotificationConfiguration]?>) {
        let url = endPointURL
            .appendingPathComponent("notificacao")
            .appendingPathComponent(documentNumber)
        
        request(url,
                method: .get,
                headers: configuration.headers,
                keyPath: "Objeto",
                response: response)
    }
    
    func updateNotifications(documentNumber: String, id: Int, active: Bool, response: EmptyCustomResponse) {
        let url = endPointURL.appendingPathComponent("notificacao")
            .appendingPathComponent(documentNumber)
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let token = AppPersistence.getValue(withKey: AppPersistence.token){
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        
        let param = [
            [
                "id" : id,
                "ativo": active
            ]
        ]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        
        AF.request(request)
            .responseJSON { dataResponse in
                switch dataResponse.result {
                case .failure(let error):
                    response.failure(error)
                    break
                case .success( _):
                    response.success()
                    break
                }
                response.completion()
        }
    }
}

