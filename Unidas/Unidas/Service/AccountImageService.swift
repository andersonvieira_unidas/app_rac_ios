//
//  AccountImageService.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import UIKit

class AccountImageService: Service {
    
    func getImage(documentNumber: String, type: ImageType, response: ModelResponse<Data>) {
        
        let url = "\(endPointURL)documento/ConsultarDocumentoCliente?cliente=\(documentNumber)&documentoTipoId=\(type.rawValue)&tipoRetorno=3"
        
        AF.request(url, method: .get, headers: configuration.headers).debugLog().responseString(completionHandler: { (dataResponse: AFDataResponse<String>) in
            switch dataResponse.result {
            case .success(var content):
                
                content = content.replacingOccurrences(of: "[", with: "")
                content = content.replacingOccurrences(of: "]", with: "")
                
                if let decodedData = Data(base64Encoded: content, options: .ignoreUnknownCharacters) {
                    response.success(decodedData)
                    response.completion()
                } else {
                    response.failure(ServiceError(message: "Imagem não processada", code: 415))
                }
                break
            case .failure(let error):
                response.failure(error)
                break
            }
        })
    }
}
