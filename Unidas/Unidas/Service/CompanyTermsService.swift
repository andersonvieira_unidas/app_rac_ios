//
//  CompanyTermsService.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

enum TermsCompanyType: Int {
    case about = 1
    case thiefsReport = 7
    case phoneNumberSac = 21
    case contractConditions = 15
    case rentalRequirements = 16
    case accountOpeningPolicy = 17
    case termsAndConditionsOfUse = 18
    case refundPolicy = 19
}

class CompanyTermsService: Service {
    var endPoint: String = ""
    
    func fetchTerms(termsCompanyType: TermsCompanyType, response: ModelResponse<[CompanyTerms]>) {
        let url = endPointURL.appendingPathComponent("NormasPoliticasEmpresa")
        
        let parameters: Parameters = ["id": termsCompanyType.rawValue]
        
        request(url,
                parameters: parameters,
                headers: configuration.headers,
                response: response)
    }
}
