//
//  LoginPciService.swift
//  Unidas
//
//  Created by Anderson Vieira on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Alamofire

class LoginPciService: Service {
    
    let configuration: ServiceConfiguration = .pci
    
    func login(user: String, token: String, response: ModelResponse<LoginPci>) {
        
        let parameters = ["usuario": user,
                          "token": token]
        
        let url = endPointURL.appendingPathComponent("api/Login")
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, response: response)
    }
}

