//
//  CreditCardService.swift
//  Unidas
//
//  Created by Anderson Vieira on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Alamofire

class CreditCardPciService: Service {
    
    let configuration: ServiceConfiguration = .pci
    
    func createCard(user: UserResponse, codeStore: String, creditCardForm: CreditCardForm, numberSequenceStore: String, addInfo: String, token: String, response: ModelResponse<CreditCardResponse>) {
        let card: [String: Any] = ["expiry_date": creditCardForm.expirationDate,
                    "number": creditCardForm.cardNumber,
        ]
        let parameters: [String:Any] = ["card": card,
                                        "authorizer_id": creditCardForm.authorizerId?.authorizeString ?? "",
                          "merchant_usn": numberSequenceStore,
                          "customer_id": user.account.documentNumber]
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json"
        ]
 
        let url = endPointURL.appendingPathComponent("api/v1/Armazenamento")
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers, response: response)
    }
}

