//
//  CancellationReasonService.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class CancellationReasonService: Service {
  
    func get(response: ModelResponse<[CancellationReason]>) {
        let url = endPointURL.appendingPathComponent("MotivoCancelamento")

        request(url,
                headers: configuration.headers,
                response: response)
    }
}

