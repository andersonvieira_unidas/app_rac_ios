//
//  AccountPhotoUnidasService.swift
//  Unidas
//
//  Created by Mateus Padovani on 24/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire
import Foundation

class AccountPhotoUnidasService: Service {
    
    var endPoint = ""

    func saveDocument(user: UserResponse, userImage: String, imageType: ImageType, response: EmptyCustomResponse) {
        
        endPoint = "Cliente/DadosComplentarConta"
        
        let documentList : [String] = [ userImage ]
        let now = ValueFormatter.format(date: Date(), format: "YYYY-MM-dd'T'HH:mm:ss.SSS")
        
         var parameters: [String: Any] = [:]
        
        if let accountId = user.unidas?[0].contaClienteId {
            parameters["DocumentoId"] = accountId
        }
        parameters["CLIDOC"] = user.account.documentNumber
        parameters["DocumentoTipoId"] = imageType.rawValue
        parameters["ListaDocumentos"] = documentList
        parameters["DocumentoSolicitacaoSistema"] = "Aplicativo RAC - IOS"
        parameters["DocumentoSolicitacaoFuncionalidade"] = imageType.resourceName
        parameters["DocumentoSolicitacaoUsuario"] = "foto enviada via aplicativo"
        parameters["DocumentoSolicitacaoAcao"] = "atualização/inclusão"
        parameters["DocumentoSolicitacaoData"] = now
        parameters["TipoExtensao"] = 2
        
        request(endPointURL,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
        
    }
    
    func updateTerms(documentNumber: String, privacyTerm:Bool, response: EmptyCustomResponse) {
        
         endPoint = "Cliente/AtualizaTermoPoliticaPrivacidade"
        
         let url = "\(endPointURL)\(documentNumber)"
         
         let parameters: Parameters = ["termoPoliticaPrivacidade": privacyTerm]
         
         request(url,
                 method: .put,
                 parameters: parameters,
                 encoding: JSONEncoding.default,
                 headers: configuration.headers,
                 response: response)
     }
}
