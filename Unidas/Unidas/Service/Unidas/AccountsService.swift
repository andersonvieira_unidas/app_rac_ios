//
//  AccountService.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class AccountsService: Service {
    
    func updateUser(user: UserForm, unidasToken: String?, password: String?, response: EmptyCustomResponse) {
        
        let url = endPointURL.appendingPathComponent("AtualizarClienteContaInfoPessoal", isDirectory: false)
        
        var parameters: Parameters = ["CLIDOC": user.documentNumber,
                                      "ContaClienteNome": user.name,
                                      "ContaClienteLoginEmail": user.email,]
        
        if let motherName = user.motherName {
            parameters["ContaClienteNomeMae"] = motherName
        }
        
        if let birthDate = user.birthDate {
            parameters["ContaClienteNascimento"] = ValueFormatter.format(date: birthDate, format: "YYYY-MM-dd")
        }
        
        let phoneWithoutSpecialChars = user.phoneNumber.removePhoneCharacters
        
        parameters["ContaClienteLoginCelularDDD"] = "\(String(describing: phoneWithoutSpecialChars.areaCode!))"
        parameters["ContaClienteLoginCelular"] = "\(String(describing: phoneWithoutSpecialChars.phoneNumber!))"
        
        request(url,
                method: .put,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func updateDriverLicense(user: UserResponse, driverLicense: DriverLicenseForm, response: EmptyCustomResponse) {
        
        let url = endPointURL.appendingPathComponent("Cliente/CNH", isDirectory: false)
        guard let birthDate = user.unidas?.first?.contaClienteNascimento else { return }
        
        let validThru = ValueFormatter.format(date: driverLicense.validThru, format: "YYYY-MM-dd'T'HH:mm:ss.SSS'Z'")
        let firstDriverLicense = ValueFormatter.format(date: driverLicense.firstDriverLicense, format: "YYYY-MM-dd'T'HH:mm:ss.SSS'Z'")
        
        let parameters: Parameters = ["nome": user.account.name,
                                      "rg": driverLicense.registrationId,
                                      "orgao_emissor_rg": driverLicense.issuingBody,
                                      "estado_emissao_rg": driverLicense.state,
                                      "cpf": user.account.documentNumber,
                                      "data_de_nascimento": birthDate,
                                      "nome_do_pai": driverLicense.fatherName,
                                      "nome_da_mae": driverLicense.motherName,
                                      "numero_registro": driverLicense.driverLicenseNumber,
                                      "validade": validThru,
                                      "data_primeira_habilitacao": firstDriverLicense]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
