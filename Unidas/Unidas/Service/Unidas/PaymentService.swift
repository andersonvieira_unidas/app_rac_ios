//
//  PaymentService.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class PaymentService: Service {
    func listCard(user: UserResponse, response: ModelResponse<[ListCard]>) {
        let url = "\(endPointURL)ListarCartaoCreditoCliente/\(user.account.documentNumber)"
        
        request(url,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func createCard(user: UserResponse, codeStore: String, creditCardForm: CreditCardForm, numberSequenceStore: String, addInfo: String, response: ModelResponse<Card>) {
        let url = endPointURL.appendingPathComponent("armazenar/cartao")
        
        let parameters: [String: Any] = [ "codLoja": codeStore, "numeroCartao": creditCardForm.cardNumber, "dataExpiracaoCartao": creditCardForm.expirationDate, "codAutorizadora": creditCardForm.authorizerId?.authorizeString ?? "", "numeroSequencialLoja": numberSequenceStore, "infoAdicional": addInfo, "codIdentificacaoCliente": user.account.documentNumber ]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func newCreateCard(user: UserResponse, codeStore: String, creditCardForm: CreditCardForm, numberSequenceStore: String, addInfo: String, token: String, nita: String, cardNumber: String, response: EmptyCustomResponse){
        
        let url = endPointURL.appendingPathComponent("Inserir").appendingPathComponent("cartao")
        
        let parameters: [String: Any] = [
            "numeroSequencialLoja": numberSequenceStore,
            "codIdentificacaoCliente": user.account.documentNumber,
            "number": cardNumber,
            "token": token,
            "nita": nita,
            "infoAdicional": addInfo,
            "dataExpiracaoCartao": creditCardForm.expirationDate,
            "bandeiraCartao": creditCardForm.authorizerId?.authorizeName ?? "",
            "codAutorizadora": creditCardForm.authorizerId?.authorizeString ?? ""
        ]
        
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: configuration.headers, response: response)
    }
    
    func performPayment(of amount: String, with card: ListCard, codeStore: String, cardSecurityCode: String, reserveNumber: String, installments: Int, response: ModelResponse<PaymentResponse>) {
        let url = "\(endPointURL)incluir/hash?merchantId=\(codeStore)&amount=\(amount)&merchantUSN=\(reserveNumber)&tipoPagamento=R"
        
        var hashPaymentRequestField: [String: Any] = ["authorizerIdField": CardUtils.findByName(for: card.tknbnddes).authorizeId.authorizeString]
        hashPaymentRequestField["autoConfirmationField"] = true
        hashPaymentRequestField["cardHashField"] = card.pan
        hashPaymentRequestField["cardSecurityCodeField"] = cardSecurityCode
        hashPaymentRequestField["customerIdField"] = card.clidoc
        hashPaymentRequestField["extraFieldField"] = ""
        hashPaymentRequestField["installmentTypeField"] = 4
        hashPaymentRequestField["installmentTypeFieldSpecified"] = true
        hashPaymentRequestField["installmentsField"] = installments
        
        let parameters = ["hashPaymentRequestField": hashPaymentRequestField ]
        
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: configuration.headers).validate(statusCode: 200...201).responseDecodableObject(keyPath: "paymentResponseField") { (dataResponse: AFDataResponse<PaymentResponse>) in
            
            switch dataResponse.result {
            case .success(let data):
                response.success(data)
                break
            case .failure(let error):
                response.failure(error)
                break
            }
            response.completion()
        }
    }
    
    func getInstallments(of amount: String, response: ModelResponse<[Installments]>) {
        let url = "\(endPointURL)pagamento/PagamentoParcelado?Valor=\(amount)"
        
        request(url,
                method: .post,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func fetchPreAuthorizationValue(for protections: [Int], groupCode: String, rateQualifier: Int, response: ModelResponse<PreAuthorizationEstimate>) {
        let url = endPointURL.appendingPathComponent("prepagamento").appendingPathComponent("obtervalor")
        let parameters: [String: Any] = ["Tarifa": rateQualifier,
                                         "CodigoGrupo": groupCode,
                                         "CodigoProtecao": protections]
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: configuration.headers, response: response)
    }
    
    
    func markFavoriteCard(user: UserResponse, tknCodSeq: Int, response: EmptyCustomResponse) {
        let url = "\(endPointURL)AtualizarCartaoCreditoPreferencial?Documento=\(user.account.documentNumber)&CartaoIdPreferencial=\(tknCodSeq)"
        
        request(url,
                method: .put,
                headers: configuration.headers,
                response: response)
    }
    
    func deleteCard(user: UserResponse, tknCodSeq: Int, response: EmptyCustomResponse) {
        let url = "\(endPointURL)Cliente/Cartao/Excluir?Documento=\(user.account.documentNumber)&CartaoId=\(tknCodSeq)"
        
        request(url,
                method: .delete,
                headers: configuration.headers,
                response: response)
    }
}
