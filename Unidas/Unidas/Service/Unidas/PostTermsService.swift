//
//  PostTermsService.swift
//  Unidas
//
//  Created by Felipe Machado on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import Alamofire

class PostTermsService: Service {
  
    func updateTerms(documentNumber: String, privacyTerm:Bool, response: EmptyCustomResponse) {
        
        let url = "\(endPointURL)Cliente/AtualizaTermoPoliticaPrivacidade/\(documentNumber)"
        
        let parameters: Parameters = ["termoPoliticaPrivacidade": privacyTerm]
        
        request(url,
                method: .put,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}

