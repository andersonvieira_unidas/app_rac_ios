//
//  UserCheckService.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class UserCheckService: Service {
    
    func checkDocument(documentNumber: String, response: ModelResponse<[UserUnidas]>) {
        let url = endPointURL.appendingPathComponent("ObterClienteConta").appendingPathComponent(documentNumber)
        
        request(url,
                headers: configuration.headers,
                response: response)
    }
    
    func fetchUserInformationForAccountRecovery(documentNumber: String, unidasToken: UnidasToken, response: ModelResponse<[UserUnidas]>) {
        let url = endPointURL.appendingPathComponent("ObterClienteConta").appendingPathComponent(documentNumber)
        let parameters = ["logado": "false"]
        var headers = configuration.headers
        headers?.add(name: "Authorization", value: unidasToken.token)
        request(url, parameters: parameters, headers: headers, response: response)
    }
    
    func generateExpiredToken(documentNumber: String, response: ModelResponse<UnidasToken>) {
        let url = endPointURL.appendingPathComponent("ValidarDocumentoCliente")
        let parameters: Parameters = ["numeroDocumento": documentNumber]

        request(url,
                method: .post,
                parameters: parameters,
                headers: configuration.headers,
                response: response)
    }
    
    
    func checkUser(documentNumber: String, email: String, ddd: Int, phone: Int, response: ModelResponse<String>){
        let url = "\(endPointURL)v2/ValidarDadosCadastroConta?documento=\(documentNumber)&email=\(email)&ddd=\(ddd)&numero=\(phone)"
        
        request(url,
                method: .get,
                response: response)
    }
}

