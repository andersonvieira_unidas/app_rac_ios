//
//  AccountAddressesUnidasService.swift
//  Unidas
//
//  Created by Mateus Padovani on 24/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class AccountAddressesUnidasService: Service {

    func updateAddress(user: UserResponse, addressForm: AddressForm, response: ModelResponse<String>) {
        
        let url = endPointURL.appendingPathComponent("IncluirClienteContaInfoResidencial", isDirectory: false)
        
        var parameters: Parameters = ["CLIDOC": user.account.documentNumber,
                                      "ContaClienteEnderecoResidencialLogradouro": addressForm.street,
                                      "ContaClienteEnderecoResidencialLogradouroNumero": addressForm.number,
                                      "ContaClienteEnderecoResidencialBairro": addressForm.neighborhood,
                                      "ContaClienteEnderecoResidencialCEP": addressForm.zipcode.cep,
                                      "ContaClienteEnderecoResidencialCidade": addressForm.zipcode.localidade,
                                      "ContaClienteEnderecoResidencialUF": addressForm.zipcode.uf,
                                      "ContaClienteEnderecoResidencialPais":"BRA"]
        
        if let complement = addressForm.complement {
            parameters["ContaClienteEnderecoResidencialLogradouroComplemento"] = complement
        }
        
        if let residencialPhone = addressForm.residencialPhone{
            
            if residencialPhone == ""{
                parameters["ContaClienteTelResDDD"] = "00"
                parameters["ContaClienteTelRes"] = "00000000"
            }else{
                let residencialPhoneNoChars = residencialPhone.removePhoneCharacters

                if let areaCode = residencialPhoneNoChars.areaCode,
                    let phoneNumber = residencialPhoneNoChars.phoneNumber {
                    parameters["ContaClienteTelResDDD"] = String(describing: areaCode)
                    parameters["ContaClienteTelRes"] = String(describing: phoneNumber)
                }
            } 
        }
        
        request(url,
                method: .post,
                parameters: parameters,
                headers: configuration.headers,
                response: response)
    }
}
