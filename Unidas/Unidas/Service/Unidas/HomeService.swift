//
//  HomeService.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import Alamofire

class HomeService: Service {
    func get(documentNumber: String, response: ModelResponse<Home?>) {
        let url = "\(endPointURL)V2/UltimaReserva/\(documentNumber)" // pendencies false
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
    
    func checkUserDocuments(documentNumber: String, response: ModelResponse<UserCheckResult>){
        let url = "\(endPointURL)v1/indicadorcontaclientestatus?clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func updateFcmToken(documentNumber: String, token:String, response: EmptyCustomResponse) {
        let url = "\(endPointURL)Cliente/TokenAPP"
        
        let parameters: Parameters = ["Token": token,
                                      "CPF": documentNumber]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func fetchTerms(documentNumber: String, response: ModelResponse<PostTerms>){
        let url = "\(endPointURL)Cliente/TermoPoliticaPrivacidade/\(documentNumber)"
        
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
