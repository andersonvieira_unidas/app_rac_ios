//
//  UnidasUserStatusService.swift
//  Unidas
//
//  Created by Mateus Padovani on 17/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class UnidasUserStatusService: Service {
    
    func get(documentNumber: String, response: ModelResponse<[UserUnidasStatus]>) {
        let url = endPointURL.appendingPathComponent("ConsultarContaStatusCliente")
        let parameters = ["numeroDocumento": documentNumber]

        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func checkUserDocuments(documentNumber: String, response: ModelResponse<UserCheckResult>){
        let url = "\(endPointURL)v1/indicadorcontaclientestatus?clidoc=\(documentNumber)"
    
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
