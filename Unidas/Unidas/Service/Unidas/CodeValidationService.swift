//
//  CodeValidationService.swift
//  Unidas
//
//  Created by Mateus Padovani on 10/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class CodeValidationService: Service {
    
    func validate(user: UserResponse, code: String, response: EmptyCustomResponse) {
        let url = "\(endPointURL)ValidarNovoSecretCode?nrDoc=\(user.account.documentNumber)&code=\(code)"
   
        request(url,
                method: .post,
                response: response)
    }
    
    func validate(documentNumber: String, code: String, response: EmptyCustomResponse) {
         let url = "\(endPointURL)ValidarNovoSecretCode?nrDoc=\(documentNumber)&code=\(code)"
    
         request(url,
                 method: .post,
                 response: response)
     }
    
    func validateRemember(user: UserResponse, code: String, unidasToken: UnidasToken?, response: EmptyCustomResponse) {
        let url = "\(endPointURL)ValidarSecretCodeSMS?nrDoc=\(user.account.documentNumber)&code=\(code)"
        
        var headers: HTTPHeaders? = nil
        
        if let accountToken = unidasToken?.token {
            headers = .init(["Authorization": accountToken])
        }
        request(url,
                method: .post,
                headers: headers,
                response: response)
    }
}
