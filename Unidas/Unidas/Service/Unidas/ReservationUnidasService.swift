//
//  ReservationUnidasService.swift
//  Unidas
//
//  Created by Mateus Padovani on 06/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class ReservationUnidasService: Service {
    
    func get(by reservationNumber: String, documentNumber: String, response: ModelResponse<MyReservationDetail>) {
        let url = endPointURL.appendingPathComponent("V2")
            .appendingPathComponent("Reserva")
            .appendingPathComponent("Detalhe")
            .appendingPathComponent(documentNumber)
            .appendingPathComponent(reservationNumber)
        
        request(url,
                method: .get,
                response: response)
    }
    
    func fetch(by documentNumber: String, response: ModelResponse<MyReservations>) {
        //let url = "https://run.mocky.io/v3/a0cb8831-106a-4ee7-8dd3-77e8667d5776"
        let url = endPointURL.appendingPathComponent("V2/ListarMinhasReservas").appendingPathComponent(documentNumber)
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
    
    func get(by documentNumber: String, response: ModelResponse<[VehicleReservations]>) {
        let url = endPointURL.appendingPathComponent("ListarMinhasReservas").appendingPathComponent(documentNumber)
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
    
    func checkinVerify(by documentNumber:String, response: ModelResponse<CheckinVerifyResponse>){
        let url = "\(endPointURL)v1/indicadorcheckin?clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
        
    }
    
    func checkUserDocuments(documentNumber: String, response: ModelResponse<UserCheckResult>){
        let url = "\(endPointURL)v1/indicadorcontaclientestatus?clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
