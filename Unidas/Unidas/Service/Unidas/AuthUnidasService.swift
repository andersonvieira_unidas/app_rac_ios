//
//  AuthUnidasService.swift
//  Unidas
//
//  Created by Mateus Padovani on 20/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class AuthUnidasService: Service {

    func auth(documentNumber: String, pin: String, response: ModelResponse<UserUnidas>) {
        let url = "\(endPointURL)LoginContaCliente"
        
        let parameters: [String: Any] = [ "nrDoc": documentNumber, "nrPin": pin, "mobileGUID": "123" ]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                response: response)
    }
    
    func update(user: UserResponse, unidasToken: String?, pin: String, response: EmptyCustomResponse) {
        let url = endPointURL.appendingPathComponent("CadastrarPIN")
        let parameters: [String: Any] = ["Documento": user.account.documentNumber, "PIN": pin]
        var headers = configuration.headers
        if let unidasToken = unidasToken {
            headers?.add(name: "Authorization", value: unidasToken)
        }
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers, response: response)
    }
    
    func create(documentNumber: String,
                name: String,
                motherName: String,
                birthday: String,
                areaCode: String,
                mobilePhone: String,
                email: String,
                pin: String,
                response: EmptyCustomResponse) {
        
        let url = endPointURL.appendingPathComponent("IncluirClienteContaInfoPessoalCompleto")
        let userTypeId = 1
        
        let parameters: [String: Any] = ["ContaClienteTipoid" : userTypeId,
                                         "CLIDOC": documentNumber,
                                         "ContaClienteNome" : name,
                                         "ContaClienteNomeMae": motherName,
                                         "ContaClienteNascimento": birthday,
                                         "ContaClienteLoginCelularDDD" : areaCode,
                                         "ContaClienteLoginCelular": mobilePhone,
                                         "ContaClienteLoginEmail": email,
                                         "ContaClienteLoginCelularDeviceID": "appRac",
                                         "PIN": pin]
        
        request(url, method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                response: response)
    }
}
