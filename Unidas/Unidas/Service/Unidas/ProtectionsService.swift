//
//  ProtectionsService.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class ProtectionsService: Service {
    
    func fetchProtectionRules(response: ModelResponse<[ProtectionCombination]>) {
        let url = endPointURL.appendingPathComponent("ListarProtecaoCombinacao")
        request(url, response: response)
    }
    
    func fetchPreAuthorizationValue(for protections: [PricedCoverages], groupCode: String, rateQualifier: Int, response: ModelResponse<PreAuthorizationEstimate>) {
        let url = endPointURL.appendingPathComponent("prepagamento").appendingPathComponent("obtervalor")
        let parameters: [String: Any] = ["Tarifa": rateQualifier,
                                         "CodigoGrupo": groupCode,
                                         "CodigoProtecao": protections.compactMap { $0.coverageType }.compactMap { Int($0) }]
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: configuration.headers, response: response)
    }
    
}
