//
//  UnidasGarageService.swift
//  Unidas
//
//  Created by Mateus Padovani on 23/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class UnidasGarageService: Service {
    var endPoint = "LojasAll"
    
    @discardableResult
    func fetchGarages(filter: String? = nil, garageCode: String? = nil, response: ModelResponse<[Garage]>) -> DataRequest {
        
        let url = endPointURL.appendingPathComponent("0").appendingPathComponent("S")
        var parameters: [String: Any] = [:]
        
        if let filter = filter {
            parameters["filtro"] = filter
        }
        
        if let garageCode = garageCode {
            parameters["prasig"] = garageCode
        }
        
        return request(url,
                       parameters: parameters,
                       headers: configuration.headers,
                       response: (success: { (responseUnidasGarages: [GarageUnidas]) in
                        let mapGarages = responseUnidasGarages.map { Garage(garageUnidas: $0) }
                        let garagesFiltered = mapGarages.filter({ (garage) -> Bool in
                            guard let _ = garage.address?.location else { return false }
                            return true
                        })
                        response.success(garagesFiltered)
                       }, failure: { error in
                        response.failure(error)
                       }, completion: {
                        response.completion()
                       }))
    }
    
    @discardableResult
    func get(by garageCode:String, response: ModelResponse<[Garage]>) -> DataRequest {
        let url = "\(endPointURL)0/S?prasig=\(garageCode)"
        
        return request(url,
                       response: (success: { (responseUnidasGarages: [GarageUnidas]) in
                        response.success(responseUnidasGarages.map { Garage(garageUnidas: $0) })
                       }, failure: { error in
                        response.failure(error)
                       }, completion: {
                        response.completion()
                       }))
        
    }
    
    func checkUserDocuments(documentNumber: String, response: ModelResponse<UserCheckResult>){
        endPoint = ""
        let url = "\(endPointURL)v1/indicadorcontaclientestatus?clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
