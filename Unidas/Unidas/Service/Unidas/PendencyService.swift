//
//  PendencyService.swift
//  Unidas
//
//  Created by Anderson Vieira on 22/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Alamofire

class PendencyService: Service {
    
    func resendEmail(documentNumber: String, response: EmptyCustomResponse) {
        
        let url = "\(endPointURL)Cliente/Conta/EnviarEmailConfirmacao?Documento=\(documentNumber)"
        request(url, method: .post,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
