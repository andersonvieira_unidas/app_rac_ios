//
//  UnidasLoginService.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class UnidasLoginService: Service {
  
    func auth(documentNumber: String, pin: String, response: ModelResponse<UserUnidas>) {
        let url = "\(endPointURL)LoginContaCliente"
        
        let parameters: [String: Any] = [ "nrDoc": documentNumber, "nrPin": pin, "mobileGUID": "123" ]
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                response: response)
    }
}
