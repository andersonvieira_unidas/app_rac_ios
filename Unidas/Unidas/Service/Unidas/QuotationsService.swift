//
//  QuotationsService.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class QuotationsService: Service {
    
    func fetchQuotations(pickUpDate: String, returnDate: String, garagePickUp: GarageDataView, garageReturn: GarageDataView, voucher: String? = nil, rateQualifier: Int, response: ModelResponse<Results>) {
        
        let url = endPointURL.appendingPathComponent("RealizarCotacaoMobile")
        
        var parameters: Parameters = [:]
        parameters["companyName"] = "380"
        parameters["rateQualifier"] = rateQualifier
        parameters["pickUpLocation"] = garagePickUp.code
        parameters["returnLocation"] = garageReturn.code
        parameters["pickUpDateTime"] = pickUpDate
        parameters["returnDateTime"] = returnDate
        
        if let voucher = voucher, !voucher.isEmpty {
            parameters["promotionCode"] = voucher
        }
 
        request(url,
                parameters: parameters,
                headers: configuration.headers,
                keyPath: "Objeto",
                response: response)
    }
    
}

