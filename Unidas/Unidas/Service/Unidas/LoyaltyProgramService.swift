//
//  LoyaltyProgramService.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

class LoyaltyProgramService: Service {
   
    func fetchLoyaltyProgramPoints(for user: UserResponse, response: ModelResponse<[LoyaltyProgram]>) {
        let url = endPointURL.appendingPathComponent("GetExtratoPontuacaoSintetico")
                             .appendingPathComponent(user.account.documentNumber)
        
        request(url, headers: configuration.headers, response: response)
        
    }
    
}
