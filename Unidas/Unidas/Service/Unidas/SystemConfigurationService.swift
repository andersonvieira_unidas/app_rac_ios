//
//  SystemConfigurationService.swift
//  Unidas
//
//  Created by Anderson on 13/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

class SystemConfigurationService: Service {

    func fetch(codeKey: String, response: ModelResponse<[SystemConfiguration]?>) {
        let url = "\(endPointURL)v1/listarconfiguracaosyscfg?chave=\(codeKey)"
        
        request(url,
                method: .get,
                headers: configuration.headers,
                keyPath: "Objeto",
                response: response)
    }
}
