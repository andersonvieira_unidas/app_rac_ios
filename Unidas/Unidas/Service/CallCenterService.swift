//
//  CallCenterService.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class CallCenterService: Service {
    var endPoint: String = ""
    
    func fetch(response: ModelResponse<CallCenterResponse>) {
        let url = endPointURL.appendingPathComponent("ListarInformacoesCentralAtendimento")
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
}
