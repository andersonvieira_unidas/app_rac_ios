//
//  CepService.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class ZipcodeService: Service {
   
    func fetch(for cep: String, response: ModelResponse<[Zipcode]>) {
        let url = endPointURL.appendingPathComponent("logradouro").appendingPathComponent("consultaporcep")
        let parameters: Parameters = ["nCep": cep]
        
        request(url,
                method: .get,
                parameters: parameters,
                headers: configuration.headers,
                response: response)
    }
}

