//
//  RecoveryService.swift
//  Unidas
//
//  Created by Mateus Padovani on 07/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class RecoveryService: Service {
    var endPoint = ""
   
    func resendSecretCode(name: String,
                          phoneCodeNumber: String,
                          phoneNumber: String,
                          documentNumber: String,
                          email: String,
                          response: EmptyCustomResponse) {
    
        let url = endPointURL.appendingPathComponent("EnviarNovoSecretCode").absoluteString
        
        let parameters: Parameters = ["CPF": documentNumber,
                                      "CelularDDD": phoneCodeNumber,
                                      "Celular": phoneNumber,
                                      "Email" : email,
                                      "Nome": name,
                                      "Id": 0]
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                response: response)
    }
    
    func sendSecretCode(for user: UserResponse, method: AccountRecoveryMethod, response: EmptyCustomResponse) {
        var url = endPointURL.appendingPathComponent("EnviarCodigoConfirmacao").appendingPathComponent(user.account.documentNumber).absoluteString
        url.append("?tipoEnvio=\(method.rawValue + 1)")
        request(url,
                method: .post,
                headers: configuration.headers,
                response: response)
    }
    
    func sendSecretCode(documentNumber: String, method: AccountRecoveryMethod, response: EmptyCustomResponse) {
        var url = endPointURL.appendingPathComponent("EnviarCodigoConfirmacao").appendingPathComponent(documentNumber).absoluteString
        url.append("?tipoEnvio=\(method.rawValue + 1)")
        request(url,
                method: .post,
                headers: configuration.headers,
                response: response)
    }
}


