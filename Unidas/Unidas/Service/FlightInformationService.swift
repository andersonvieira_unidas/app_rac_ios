//
//  FlightInformationService.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 9/20/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

class FlightInformationService: Service {
    
   let endPoint: String = ""
    
    func fetchFlightCompanies(response: ModelResponse<[FlightCompany]>) {
        let url = endPointURL.appendingPathComponent("ListCompanhiaArea")
        request(url, response: response)
    }
    
}
