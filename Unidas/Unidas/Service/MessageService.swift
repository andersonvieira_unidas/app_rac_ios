//
//  MessageService.swift
//  Unidas
//
//  Created by Mateus Padovani on 09/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class MessageService: Service {
  
    func fetch(documentNumber: String, response: ModelResponse<[Message]>) {
        let url = "\(endPointURL)mensagem/cliente/avisos?clidoc=\(documentNumber)"
       
        request(url,
                method: .get,
                headers: configuration.headers,
                keyPath: "Objeto",
                response: response)
    }
    
    func unreadMessage(documentNumber: String, message: Message, response: EmptyCustomResponse) {
        let url = endPointURL.appendingPathComponent("AtualizarClienteMensagem")
        
        let parameters: Parameters = [
            "ClienteMensagemSolicitacaoSistema": "APPRAC",
            "ClienteMensagemSolicitacaoFuncionalidade": "Mensagens",
            "ClienteMensagemSolicitacaoUsuario": documentNumber,
            "ClienteMensagemSolicitacaoAcao": "Atualização Mensagem",
            "MensagemId": message.id
        ]

        request(url,
                method: .put,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func checkUserDocuments(documentNumber: String, response: ModelResponse<UserCheckResult>){
        let url = "\(endPointURL)v1/indicadorcontaclientestatus?clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
}
