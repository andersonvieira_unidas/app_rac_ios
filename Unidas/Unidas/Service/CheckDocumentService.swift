//
//  CheckDocumentService.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire

class CheckDocumentService: Service {
    
    func fetch(documentNumber: String, response: ModelResponse<[CheckDocument]>) {
        let url = "\(endPointURL)ListarValidadeDocumentoCliente?Clidoc=\(documentNumber)"
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
}

