//
//  Service.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import Alamofire
import FirebaseCrashlytics

protocol Service: class {
    var configuration: ServiceConfiguration { get }
    var endPoint: String { get }
}

extension Service {
    
    var configuration: ServiceConfiguration {
        return .unidas
    }
    
    var endPoint: String {
        return ""
    }
    
    typealias ModelResponse<T> = (success: (T) -> Void, failure: (Error) -> Void, completion: () -> Void)
    typealias EmptyCustomResponse = (success: () -> Void, failure: (Error) -> Void, completion: () -> Void)
    typealias StringResponse = (success: (String) -> Void, failure: (Error) -> Void, completion: () -> Void)
    
    var endPointURL: URL {
        return (endPoint.isEmpty) ? configuration.baseURL : configuration.baseURL.appendingPathComponent(endPoint, isDirectory: true)
    }
        
    @discardableResult
    func request<T: Decodable>(_ url: URLConvertible, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, keyPath: String? = nil, response: ModelResponse<T>) -> DataRequest {
        
        return AF.request(url,
                          method: method,
                          parameters: parameters,
                          encoding: encoding,
                          headers: headers)
            .validate()
            .debugLog()
            .responseDecodableObject(keyPath: keyPath,
                                     decoder: configuration.decoder,
                                     completionHandler: completionHandler(for: response))
    }
    
    @discardableResult
    func request(_ url: URLConvertible, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, response: EmptyCustomResponse) -> DataRequest {
        return AF.request(url,
                                 method: method,
                                 parameters: parameters,
                                 encoding: encoding,
                                 headers: headers)
            .validate()
            .debugLog()
            .responseData(completionHandler: completionHandler(for: response))
    }
    
    @discardableResult
    func request(_ url: URLConvertible, method: HTTPMethod = .get, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, keyPath: String? = nil, response: StringResponse) -> DataRequest {
        return AF.request(url,
                                 method: method,
                                 parameters: parameters,
                                 encoding: encoding,
                                 headers: headers)
            .validate()
            .debugLog()
            .responseString(completionHandler: completionHandler(for: response))
    }
    
   func completionHandler(for response: EmptyCustomResponse, shouldLogData: Bool = false) -> (AFDataResponse<Data>) -> Void {
        return { [weak self] dataResponse in
            if shouldLogData {
                self?.log(data: dataResponse.data)
            }
            
            switch dataResponse.result {
            case .success:
                response.success()
                break
            case .failure(let error):
                if error.responseCode == 401 && self?.configuration == .unidas {
                    session.requestLoginForExpiredSession()
                    response.failure(error)
                    response.completion()
                    return
                } else if let responseCode = error.responseCode, responseCode == 404 {
                    if let data = dataResponse.data, let text = String(data: data, encoding: .utf8), !text.isEmpty {
                        response.failure(ServiceError(message: text, code: dataResponse.response?.statusCode))
                    } else {
                        response.failure(ServiceError(message: ServiceError.notFoundError.message, code: dataResponse.response?.statusCode))
                    }
                }
                else if let responseCode = error.responseCode, (500...599).contains(responseCode) {
                    response.failure(ServiceError.unexpectedError)
                }
                if let data = dataResponse.data, let text = String(data: data, encoding: .utf8), !text.isEmpty, !error.isResponseSerializationError {
                    if let errorResponse = self?.converterErrorResponse(body: data) {
                        response.failure(ServiceError(message: errorResponse, code: dataResponse.response?.statusCode))
                        return
                    }
                    response.failure(ServiceError(message: text, code: dataResponse.response?.statusCode))
                } else {
                    response.failure(error)
                }
                
                Crashlytics.crashlytics().record(error: error)
                //Crashlytics.crashlytics().record(error, withAdditionalUserInfo: ["responseBody": body])
                break
            }
            response.completion()
        }
    }
    
    func completionHandler<T : Decodable>(for response: ModelResponse<T>, shouldLogData: Bool = false) -> (AFDataResponse<T>) -> Void {
        return { [weak self] dataResponse in
            defer { response.completion() }
            if shouldLogData {
                self?.log(data: dataResponse.data)
            }
            switch dataResponse.result {
            case .success(let value):
                response.success(value)
                break
            case .failure(let error):

                if error.responseCode == 401 && self?.configuration == .unidas {
                    session.requestLoginForExpiredSession()
                    response.failure(error)
                    return
                } else if let responseCode = error.responseCode, responseCode == 404 {
                    
                    if let data = dataResponse.data, let errorResponse = self?.converterErrorResponse(body: data) {
                        response.failure(ServiceError(message: errorResponse, code: dataResponse.response?.statusCode))
                        return
                    } else {
                        response.failure(ServiceError(message: ServiceError.notFoundError.message, code: dataResponse.response?.statusCode))
                    }
                }
                else if let responseCode = error.responseCode, (500...599).contains(responseCode) {
                    response.failure(ServiceError.unexpectedError)
                }
                else if let responseCode = dataResponse.response?.statusCode, responseCode == 200 && !error.isResponseSerializationError {
                    return
                }
                else {
                    if let data = dataResponse.data, let text = String(data: data, encoding: .utf8), !text.isEmpty, !error.isResponseSerializationError {
                        
                        if let errorResponse = self?.converterErrorResponse(body: data) {
                            response.failure(ServiceError(message: errorResponse, code: dataResponse.response?.statusCode))
                            return
                        }
                        response.failure(ServiceError(message: text, code: dataResponse.response?.statusCode))
                    } else {
                        if error._code == NSURLErrorTimedOut {
                             response.failure(ServiceError.timeoutError)
                        }
                        response.failure(error)
                    }
                    Crashlytics.crashlytics().record(error: error)
                    
                   // Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: ["responseBody": body])
                    break
                }
            }
        }
    }
    
    private func converterErrorResponse(body: Data) -> String?{
        do {
            let decoder = JSONDecoder()
            let errorMessage = try decoder.decode(ErrorResponse.self, from: body)
            return errorMessage.mensagem
        } catch {
            return nil
        }
    }
    
    func completionHandler(for response: StringResponse, shouldLogData: Bool = false) -> (AFDataResponse<String>) -> Void {
        return { [weak self] dataResponse in
            if shouldLogData {
                self?.log(data: dataResponse.data)
            }
            switch dataResponse.result {
            case .success(let string):
                response.success(string)
                break
            case .failure(let error):
                if error.responseCode == 401 && self?.configuration == .unidas {

                    session.requestLoginForExpiredSession()
                    response.failure(error)
                    response.completion()
                    return
                } else if let responseCode = error.responseCode, responseCode == 404 {
                    response.failure(ServiceError(message: ServiceError.notFoundError.message, code: dataResponse.response?.statusCode))
                }
                else if let responseCode = error.responseCode, (500...599).contains(responseCode) {
                    response.failure(ServiceError.unexpectedError)
                }
                if let data = dataResponse.data, let text = String(data: data, encoding: .utf8), !text.isEmpty, !error.isResponseSerializationError {
                    if let errorResponse = self?.converterErrorResponse(body: data) {
                        response.failure(ServiceError(message: errorResponse, code: dataResponse.response?.statusCode))
                        return
                    }
                    response.failure(ServiceError(message: text, code: dataResponse.response?.statusCode))
                } else {
                    response.failure(error)
                }
                Crashlytics.crashlytics().record(error: error)
                
                //Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: ["responseBody": body])
                break
            }
            response.completion()
        }
    }
    
    private func log(data: Data?) {
        guard let data = data, let string = String(data: data, encoding: .utf8) else { return }
        print(string)
    }
    
}

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        self.cURLDescription { description in
            print("=======================================")
            print(description)
            print("=======================================")
        }
        #endif
        return self
    }
}
