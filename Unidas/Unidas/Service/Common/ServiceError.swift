//
//  ServiceError.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 05/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ServiceError: Codable, LocalizedError {
    
    let message: String?
    let code: Int?
    
    static var unexpectedError = ServiceError(message: NSLocalizedString("An unexpected error occurred", comment: "An unexpected error occurred error description"))
    
    static var timeoutError = ServiceError(message: NSLocalizedString("Timeout Error", comment: "Timeout error description"))
    
    static var notFoundError = ServiceError(message: NSLocalizedString("Not found", comment: "Not found description"))

    init(message: String?, code: Int? = nil) {
        if let messageFormatted = message?.replacingOccurrences(of: "\"", with: "") {
            self.message = messageFormatted
        } else {
            self.message = message
        }
       
        self.code = code
    }
    
    var errorDescription: String? {
        return message
    }
    
}
