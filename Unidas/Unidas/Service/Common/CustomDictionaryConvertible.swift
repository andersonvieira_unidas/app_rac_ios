//
//  CustomDictionaryConvertible.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

protocol CustomDictionaryConvertible {
    var dictionaryRepresentation: [String: Any]? { get }
}

extension CustomDictionaryConvertible where Self: Encodable {
    var dictionaryRepresentation: [String : Any]? {
        let encoder = JSONEncoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        if let data = try? encoder.encode(self), let object = try? JSONSerialization.jsonObject(with: data, options: .allowFragments), let dictionary = object as? [String: Any] {
            
            return dictionary
        }
        
        return nil
    }
}
