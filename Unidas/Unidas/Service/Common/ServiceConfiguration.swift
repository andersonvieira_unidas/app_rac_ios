//
//  ServiceConfiguration.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import Alamofire

class ServiceConfiguration: Equatable {
    static func == (lhs: ServiceConfiguration, rhs: ServiceConfiguration) -> Bool {
        return lhs.baseURL == rhs.baseURL
    }
    
    let baseURL: URL
    var headers: HTTPHeaders?
    let decoder: JSONDecoder
    let encoder: JSONEncoder
    let dateFormatter: DateFormatter
    
    init(baseURL: URL, headers: HTTPHeaders? = nil, decoder: JSONDecoder, encoder: JSONEncoder, dateFormatter: DateFormatter) {
        self.baseURL = baseURL
        self.headers = headers
        self.decoder = decoder
        self.encoder = encoder
        self.dateFormatter = dateFormatter
    }
    
    static var unidas: ServiceConfiguration = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "pt_BR")
        let decoder = JSONDecoder()
        let encoder = JSONEncoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        
        var headers: HTTPHeaders? = nil
        
        if let token = AppPersistence.getValue(withKey: AppPersistence.token){
            headers = HTTPHeaders.init(["Authorization": token])
        }
        #if DEVELOPMENT
        let baseURL = URL(string: "https://hml-gtw-apprac.unidas.com.br/")!
        #else
        let baseURL = URL(string: "https://gtw-apprac.unidas.com.br/")!
        #endif
        print(baseURL)
        return ServiceConfiguration(baseURL: baseURL, headers: headers, decoder: decoder, encoder: encoder, dateFormatter: dateFormatter)
    }()
    
    static var pci: ServiceConfiguration = {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
         dateFormatter.locale = Locale(identifier: "pt_BR")
         let decoder = JSONDecoder()
         let encoder = JSONEncoder()
         decoder.dateDecodingStrategy = .formatted(dateFormatter)
         encoder.dateEncodingStrategy = .formatted(dateFormatter)
         
         #if DEVELOPMENT
         let baseURL = URL(string: "https://hml-pc-es-front.unidas.net.br")!
         #else
         let baseURL = URL(string: "https://pc-es-front.unidas.net.br")!
         #endif
         print(baseURL)
         return ServiceConfiguration(baseURL: baseURL, decoder: decoder, encoder: encoder, dateFormatter: dateFormatter)
     }()
    
    static var firebaseFunctions: ServiceConfiguration = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        let decoder = JSONDecoder()
        let encoder = JSONEncoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        encoder.dateEncodingStrategy = .formatted(dateFormatter)
        
        let baseURL = URL(string: "https://us-central1-app-unidas-92ae2.cloudfunctions.net/endpoints/")!
        print(baseURL)
        return ServiceConfiguration(baseURL: baseURL, decoder: decoder, encoder: encoder, dateFormatter: dateFormatter)
    }()
}
