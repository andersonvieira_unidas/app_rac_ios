
//
//  File.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import Alamofire

class NotificationConfigurationService: Service {
    
    func fetch(documentNumber: String, response: ModelResponse<[NotificationConfiguration]?>) {
        let url = endPointURL
            .appendingPathComponent("notificacao")
            .appendingPathComponent(documentNumber)
        
        request(url,
                method: .get,
                headers: configuration.headers,
                keyPath: "Objeto",
                response: response)
    }
    
    func update(documentNumber: String, id: Int, active: Bool, response: EmptyCustomResponse) {
        let url = endPointURL.appendingPathComponent("notificacao")
            .appendingPathComponent(documentNumber)
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let token = AppPersistence.getValue(withKey: AppPersistence.token){
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        
        let param = [
            [
                "id" : id,
                "ativo": active
            ]
        ]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        
        AF.request(request)
            .responseJSON { dataResponse in
                switch dataResponse.result {
                case .failure(let error):
                    response.failure(error)
                    break
                case .success( _):
                    response.success()
                    break
                }
                response.completion()
        }
    }
    
    func updateNotifications(documentNumber: String, notifications: [NotificationConfiguration], response: EmptyCustomResponse) {
        
        let url = endPointURL.appendingPathComponent("notificacao")
            .appendingPathComponent(documentNumber)
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if let token = AppPersistence.getValue(withKey: AppPersistence.token){
            request.setValue(token, forHTTPHeaderField: "Authorization")
        }
        
        var parameters: [Any] = []
        
        for notification in notifications {
            let paramNotification = [
                "id" : notification.code,
                "ativo" : notification.active
                ] as [String : Any]
            parameters.append(paramNotification)
        }
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        
        AF.request(request)
        .debugLog()
            .responseJSON { dataResponse in
                switch dataResponse.result {
                case .failure(let error):
                    response.failure(error)
                    break
                case .success( _):
                    response.success()
                    break
                }
                response.completion()
        }
    }
}
