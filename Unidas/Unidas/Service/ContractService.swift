//
//  ContractService.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import Foundation

class ContractService: Service {
  
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        return dateFormatter
    }()
    
    lazy var timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }()
    
    func generateContract(reserveNumber: String, paymentCard: CreditCardReservePayment?, prePaymentCard: CreditCardPrePayment, preInstallments: Int, agreements: Bool, response: ModelResponse<OpenContractResponse>) {
        let url = endPointURL
            .appendingPathComponent("V3")
            .appendingPathComponent("contrato")
            .appendingPathComponent("GerarContrato")
        
        var parameters: Parameters = [ "NumeroReserva": reserveNumber ]
        
        if let prePaymentTknCodSeq = prePaymentCard.card.tkncodseq {
            parameters["TknCodSeqPreAutorizacao"] = prePaymentTknCodSeq
            parameters["ValorPreAutorizacao"] = NumberFormatter.apiNumberFormatter.string(from: prePaymentCard.amount as NSNumber)!
            parameters["ParcelasPreAutorizacao"] = preInstallments
            parameters["CVVPreAutorizacao"] = prePaymentCard.securityCode
        }
        
        if let paymentCard = paymentCard, let paymentTknCodSeq = paymentCard.card.tkncodseq {
            parameters["TknCodSeqPagamento"] = paymentTknCodSeq
            parameters["ValorPagamento"] = NumberFormatter.apiNumberFormatter.string(from: paymentCard.amount as NSNumber)!
            parameters["ParcelasPagamento"] = paymentCard.installmentsOption
            parameters["CVVPagamento"] = paymentCard.securityCode
            
        }
        parameters["AceitoTermoCheckin"] = agreements
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func generateContractLegalEntity(reserveNumber: String, voucherType: String, paymentCard: CreditCardReservePayment?, prePaymentCard: CreditCardPrePayment?, preInstallments: Int, agreements: Bool, response: ModelResponse<OpenContractResponse>) {
        
        let url = endPointURL
                .appendingPathComponent("V4")
                .appendingPathComponent("contrato")
                .appendingPathComponent("GerarContratoPJ")
        
        var parameters: Parameters = [ "NumeroReserva": reserveNumber ]
        
        if let prePaymentCard = prePaymentCard, let prePaymentTknCodSeq = prePaymentCard.card.tkncodseq {
            parameters["TknCodSeqPreAutorizacao"] = prePaymentTknCodSeq
            parameters["ValorPreAutorizacao"] = NumberFormatter.apiNumberFormatter.string(from: prePaymentCard.amount as NSNumber)!
            parameters["ParcelasPreAutorizacao"] = preInstallments
            parameters["CVVPreAutorizacao"] = prePaymentCard.securityCode
        }
        
        if let paymentCard = paymentCard, let paymentTknCodSeq = paymentCard.card.tkncodseq {
            parameters["TknCodSeqPagamento"] = paymentTknCodSeq
            parameters["ValorPagamento"] = NumberFormatter.apiNumberFormatter.string(from: paymentCard.amount as NSNumber)!
            parameters["ParcelasPagamento"] = paymentCard.installmentsOption
            parameters["CVVPagamento"] = paymentCard.securityCode
        }
        
        parameters["TipoVoucher"] = voucherType
        parameters["AceitoTermoCheckin"] = agreements
        
        request(url,
                method: .post,
                parameters: parameters,
                encoding: JSONEncoding.default,
                headers: configuration.headers,
                response: response)
    }
    
    func openContract(reserveNumber: String, response: ModelResponse<String>) {
        let url = "\(endPointURL)contrato/incluir?numeroReserva=\(reserveNumber)"
        
        AF.request(url, method: .post, headers: configuration.headers).validate().responseString { (dataResponse) in
            switch(dataResponse.result) {
            case .success(let data):
                response.success(data)
            case .failure(let error):
                response.failure(error)
                break
            }
            response.completion()
        }
    }
    
    func fetchContracts(for user: UserResponse, response: ModelResponse<[Contract]>) {
        let url = endPointURL.appendingPathComponent("GetContrato")
        let parameters: Parameters = ["cliente": user.account.documentNumber]
        
        request(url, parameters: parameters, headers: configuration.headers, response: response)
    }
    
    func fetchContracts(for user: UserResponse, and contractNumber: Int, response: ModelResponse<[Contract]>) {
        
        let url = endPointURL.appendingPathComponent("GetContrato")
        
        var parameters: Parameters = ["cliente": user.account.documentNumber]
        parameters["contrato"] = contractNumber
        request(url, parameters: parameters, headers: configuration.headers, response: response)
    }
}

