//
//  PassbookService.swift
//  Unidas
//
//  Created by Anderson Vieira on 15/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Alamofire
import Foundation

class PassbookService: Service {
    var configuration: ServiceConfiguration = .firebaseFunctions
    
    func generatePassbook(passbook: PassbookRequest,
                          response: ModelResponse<Data>) {
        let url = "\(endPointURL)unidasPassbook"
        
        let parameters: [String: String] = [
            "reservationCode" : passbook.reservationCode,
            "pickupLocation" : passbook.pickupLocation,
            "returnLocation" : passbook.returnLocation,
            "qrCode" : passbook.qrCode,
            "pickupDate" : passbook.pickupDate,
            "returnDate" : passbook.returnDate,
            "pickupTime" : passbook.pickupTime,
            "returnTime" : passbook.returnTime,
            "name": passbook.name,
            "group" : passbook.group,
            "groupDescription" : passbook.groupDescription,
            "vehiclesGroup" : passbook.vehiclesGroup]

          AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseData(completionHandler: { (dataResponse: AFDataResponse<Data>) in

            if let result = dataResponse.response?.statusCode {
                switch result {
                case 200:
                    
                    if let passValue = dataResponse.data{
                        response.success(passValue)
                        response.completion()
                    }
                break
                case 400:
                     response.failure(ServiceError(message: "Imagem não processada", code: 415))
                break
                default:
                    break
                }
            }
        })
    }
}
