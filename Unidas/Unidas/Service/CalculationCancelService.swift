//
//  CalculationCancelService.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Alamofire
import CodableAlamofire

class CalculationCancelService: Service {
   
    func calcule(reserveNumber: String, response: ModelResponse<CalculationCancel>) {
        let url = endPointURL.appendingPathComponent("DadosReservaCheckin")
            .appendingPathComponent(reserveNumber)
        
           request(url,
                   method: .get,
                   headers: configuration.headers,
                   response: response)
       }
    
    func cancelReservation(id: Int, userDocument: String?, reason: Int, response: EmptyCustomResponse) {
        let url = endPointURL.appendingPathComponent("Cancelar").appendingPathComponent(id.description)
        
        let parameters: [String : Any] = [
            "motivoCancelamento": reason,
            "notificar" : true,
            "canal": 380
            ]

        request(url,
                method: .put,
                parameters: parameters,
                encoding: URLEncoding.default,
                headers: configuration.headers,
                response: response)
        
    }    
}

