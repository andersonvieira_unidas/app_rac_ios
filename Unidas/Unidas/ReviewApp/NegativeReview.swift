//
//  NegativeReview.swift
//  Unidas
//
//  Created by Felipe Machado on 15/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

@objc protocol NegativeReviewDelegate: class {
    func closeNegativeReview()
}

class NegativeReview: UIView, Modal {
    
    var backgroundView = UIView()
    var dialogView = UIView()
    var delegate: NegativeReviewDelegate?
    var action: (() -> Void)?
    
    lazy var fontBold = {
        return UIFont.medium(ofSize: 14)
    }()
    
    lazy var closeText = {
        return NSLocalizedString("Close Button", comment: "")
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        //imageView.layer.cornerRadius = 4
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        //imageView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        return imageView
    }()
    
    private lazy var headerView: UIView = {
        let headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor.themeButton
        headerView.clipsToBounds = true
        
        return headerView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.font = UIFont.medium(ofSize: 14.0)
        titleLabel.numberOfLines = 2
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.textColor = UIColor.themeButton
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let subTitleLabel = UILabel()
        
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.textAlignment = .left
        subTitleLabel.lineBreakMode = .byTruncatingTail
        subTitleLabel.numberOfLines = 4
        subTitleLabel.font = UIFont.regular(ofSize: 14)
        subTitleLabel.adjustsFontSizeToFitWidth = true
        subTitleLabel.minimumScaleFactor = 0.5
        subTitleLabel.textColor = UIColor.standardGray
        return subTitleLabel
    }()
    
    private lazy var unidasButton: UnidasButton = {
        let unidasButton = UnidasButton()
        unidasButton.translatesAutoresizingMaskIntoConstraints = false
        unidasButton.addTarget(self, action: #selector(tapCloseButton), for: .touchUpInside)
        return unidasButton
    }()
    
    private lazy var separatorLineView: UIView = {
        let separatorLineView = UIView()
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
        return separatorLineView
    }()
    
    convenience init() {
        self.init(frame: UIScreen.main.bounds)
        initialize()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(){
        
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapCloseButton)))
        addSubview(backgroundView)
        
        dialogView.frame.origin = CGPoint(x: 30, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-60, height: 294)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 10
        addSubview(dialogView)
        
        //set data
        imageView.image = UIImage(named: "ic_robot_face")
        imageView.backgroundColor = UIColor.clear
        titleLabel.text = NSLocalizedString("App Review Negative Thankyou", comment: "")
        subTitleLabel.text = NSLocalizedString("App Review Negative Description", comment: "")
        unidasButton.setTitle(closeText, for: .normal)

        dialogView.addSubview(titleLabel)
        dialogView.addSubview(subTitleLabel)
        dialogView.addSubview(imageView)
        dialogView.addSubview(unidasButton)
        
        NSLayoutConstraint.activate([
            titleLabel.heightAnchor.constraint(equalToConstant: 34),
            titleLabel.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -60),
            
            subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            subTitleLabel.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            subTitleLabel.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            subTitleLabel.heightAnchor.constraint(equalToConstant: 30),
            
            imageView.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: 20),
            imageView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            imageView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            imageView.heightAnchor.constraint(equalToConstant: 80),
            
            unidasButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20),
            unidasButton.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            unidasButton.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            unidasButton.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    func whenButtonIsClicked(action: @escaping () -> Void) {
        self.action = action
        unidasButton.addTarget(self, action: #selector(clicked), for: .touchUpInside)
    }
    
    @objc func clicked() {
        action?()
    }
    
    @objc func tapCloseButton(){
        self.dismiss(animated: true)
        delegate?.closeNegativeReview()
    }
}

