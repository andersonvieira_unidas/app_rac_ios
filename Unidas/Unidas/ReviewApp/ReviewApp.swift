//
//  ReviewApp.swift
//  Unidas
//
//  Created by Felipe Machado on 15/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import StoreKit

@objc protocol ReviewAppDelegate: class {
    @objc func choiceFirstTapReview(tag: Int)
    @objc func choiceSecondTapReview(tag: Int)
}

class ReviewAppUtilities: NSObject {
    
    static var iMinSessions = Int(AppPersistence.getValue(withKey: "app_review_count") ?? "5")
    static var iTryAgainSessions = 4
    
    //MARK: - RateAPP
    static func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        
        guard let url = URL(string : "https://itunes.apple.com/br/app/unidas-aluguel-de-carros/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: completion)
    }
    
    static func showRateMe(review: ReviewApp) {
        review.show(animated: true)
    }
    
    static func rateMe(review: ReviewApp) {
        
        var neverRate = UserDefaults.standard.bool(forKey: "neverRate")
        var numLaunches = UserDefaults.standard.integer(forKey: "numLaunches") + 1
        
        if (!neverRate && (numLaunches == iMinSessions || numLaunches >= (iMinSessions ?? 10 + iTryAgainSessions + 1)))
        {
            if neverRate != true {
                numLaunches = iMinSessions ?? 10  + 1
                neverRate = false
                showRateMe(review: review)
            }
        }
        UserDefaults.standard.set(numLaunches, forKey: "numLaunches")
    }
}

class ReviewApp: UIView, Modal {
    var backgroundView = UIView()
    var dialogView = UIView()
    var delegate: ReviewAppDelegate?
    
    lazy var fontBold = {
        return UIFont.medium(ofSize: 14)
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.textAlignment = .left
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.numberOfLines = 3
        titleLabel.font = UIFont.medium(ofSize: 14.0)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.textColor = UIColor.themeButton
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    private lazy var bodyStackView: UIStackView = {
        let bodyStackView = UIStackView()
        
        bodyStackView.translatesAutoresizingMaskIntoConstraints = false
        bodyStackView.axis = .horizontal
        bodyStackView.distribution = .fillEqually
        bodyStackView.alignment = .center
        bodyStackView.spacing = 0
        return bodyStackView
    }()
    
    private lazy var bodyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var bodyTitleLabel: UILabel = {
        let bodyTitleLabel = UILabel()
        bodyTitleLabel.textAlignment = .left
        bodyTitleLabel.lineBreakMode = .byTruncatingTail
        bodyTitleLabel.numberOfLines = 3
        bodyTitleLabel.font = UIFont.regular(ofSize: 14.0)
        bodyTitleLabel.adjustsFontSizeToFitWidth = true
        bodyTitleLabel.minimumScaleFactor = 0.5
        bodyTitleLabel.textColor = UIColor.standardGray
        bodyTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        return bodyTitleLabel
    }()
    
    private lazy var buttonStackView: UIStackView = {
        let buttonStackView = UIStackView()
        
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.axis = .horizontal
        buttonStackView.distribution = .fillEqually
        buttonStackView.alignment = .center
        buttonStackView.spacing = 20
        return buttonStackView
    }()
    
    private lazy var firstButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
//        button.backgroundColor = #colorLiteral(red: 0.1845769286, green: 0.5008790493, blue: 0.9274390936, alpha: 1)
//        button.layer.cornerRadius = 25
        //button.titleLabel?.textAlignment = .center
        //button.setTitleColor(UIColor.themeButton, for: .normal)
        //button.titleLabel?.font = UIFont.bold(ofSize: 14)
        
        return button
    }()
    
    private lazy var secondButton: UIButton = {
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
//        button.backgroundColor = #colorLiteral(red: 1, green: 0.6040984392, blue: 0.08827053756, alpha: 1)
//        button.layer.cornerRadius = 25
        //button.titleLabel?.textAlignment = .center
        //button.setTitleColor(UIColor.themeButton, for: .normal)
        //button.titleLabel?.font = UIFont.bold(ofSize: 14)
        
        return button
    }()
    
    convenience init(title:String, firstChoiceButtonEnabled: Bool? = true, secondChoiceButtonEnabled: Bool? = true, firstButtonTag: Int? = 0, secondButtonTag: Int? = 0) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title, firstChoiceButtonEnabled: firstChoiceButtonEnabled, secondChoiceButtonEnabled: secondChoiceButtonEnabled, firstButtonTag: firstButtonTag, secondButtonTag: secondButtonTag)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(title:String,
                    firstChoiceButtonEnabled: Bool? = true,
                    secondChoiceButtonEnabled: Bool? = true,
                    firstButtonTag: Int? = 0,
                    secondButtonTag: Int? = 0 ){
        
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeAlert)))
        addSubview(backgroundView)
        
        dialogView.frame.origin = CGPoint(x: 30, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-60, height: 220)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 10
        addSubview(dialogView)
        
        //set data
        bodyImageView.image = UIImage(named: "ic_robot")
        bodyImageView.backgroundColor = UIColor.clear
        titleLabel.text = title
        bodyTitleLabel.text = NSLocalizedString("App Review Description", comment: "")
        
        firstButton.isEnabled = firstChoiceButtonEnabled ?? true
        let first_img = UIImage(named: "ic_like")
        firstButton.setImage(first_img, for: .normal)
        firstButton.tag = firstButtonTag ?? 0
        firstButton.addTarget(self, action: #selector(tapFirstChoiceReview(_:)), for: .touchUpInside)
        
        secondButton.isEnabled = secondChoiceButtonEnabled ?? true
        let second_img = UIImage(named: "ic_deslike")
        secondButton.setImage(second_img, for: .normal)
        secondButton.tag = secondButtonTag ?? 0
        secondButton.addTarget(self, action: #selector(tapSecondChoiceReview(_:)), for: .touchUpInside)
        
        bodyStackView.addArrangedSubview(bodyTitleLabel)
        
        buttonStackView.addArrangedSubview(firstButton)
        buttonStackView.addArrangedSubview(secondButton)
        
        dialogView.addSubview(titleLabel)
        dialogView.addSubview(bodyImageView)
        dialogView.addSubview(bodyStackView)
        dialogView.addSubview(buttonStackView)
        
        NSLayoutConstraint.activate([
            
            titleLabel.heightAnchor.constraint(equalToConstant: 17),
            titleLabel.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -20),
            
            bodyImageView.heightAnchor.constraint(equalToConstant: 133),
            bodyImageView.widthAnchor.constraint(equalToConstant: 70),
            bodyImageView.topAnchor.constraint(equalTo: dialogView.topAnchor, constant: 5),
            bodyImageView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -10),
            
            bodyStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            bodyStackView.trailingAnchor.constraint(equalTo: dialogView.trailingAnchor, constant: -100),
            bodyStackView.leadingAnchor.constraint(equalTo: dialogView.leadingAnchor, constant: 20),
            
            buttonStackView.topAnchor.constraint(equalTo: bodyStackView.bottomAnchor, constant: 40),
            buttonStackView.heightAnchor.constraint(equalToConstant: 50),
            buttonStackView.widthAnchor.constraint(equalToConstant: 120),
            buttonStackView.centerXAnchor.constraint(equalTo: dialogView.centerXAnchor),
            
            firstButton.heightAnchor.constraint(equalToConstant: 50),
            secondButton.heightAnchor.constraint(equalToConstant: 50),
        ])
    }
    
    @objc func tapFirstChoiceReview(_ sender: UIButton){
        dismiss(animated: true)
        delegate?.choiceFirstTapReview(tag: sender.tag)
    }
    
    @objc func tapSecondChoiceReview(_ sender: UIButton){
        dismiss(animated: true)
        delegate?.choiceSecondTapReview(tag: sender.tag)
    }
    
    @objc func closeAlert(){
        dismiss(animated: true)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

