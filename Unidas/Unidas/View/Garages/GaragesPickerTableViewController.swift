//
//  GaragesPickerTableViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 27/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol GaragesPickerTableViewControllerDelegate: class {
    func garagePicker(_ garagesTableViewController: GaragesPickerTableViewController, didSelect garage: GarageDataView)
    func garagePickerBackPressed()
}

class GaragesPickerTableViewController: GaragesTableViewController {

    weak var delegate: GaragesPickerTableViewControllerDelegate?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isUserInteractionEnabled = false
        delegate?.garagePicker(self, didSelect: presenter.garage(at: indexPath.row))        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.garagePickerBackPressed()
    }
}
