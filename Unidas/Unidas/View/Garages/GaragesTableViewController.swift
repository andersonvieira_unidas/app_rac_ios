//
//  GaragesTableViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class GaragesTableViewController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating, GaragesTableViewDelegate {

    var presenter: GaragesTableViewPresenter!
    var searchController: UISearchController!
    var activityIndicatorView: UIActivityIndicatorView!
    
    var findStoreToMaps = false
    var removeExpressStores = false
    
    var delegate2: FindStoreViewControllerDelegate?
    
    lazy var emptyResultsView: EmptyResultsView = {
        return EmptyResultsView.loadFromNib()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = GaragesTableViewPresenter(garageService: UnidasGarageService(), firebaseAnalytics: FirebaseAnalyticsService(), locationManager: LocationManager())
        
        configureBackButton()
        setupSearchController()
        setupActivityIndicator()
        setupTableView()
        setupNavigationController()
        presenter.delegate = self
        presenter.fetchCurrentLocation(filterExpress: removeExpressStores)
        
        setHeaderUnidas()
        self.navigationController?.change()
    }
    
    func setupSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.tintColor = .white
        searchController.searchBar.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        searchController.searchBar.clearButtonMode = .never
        searchController.searchBar.textColor = .white
        searchController.searchBar.setPlaceholderTextColor(UIColor.white.withAlphaComponent(0.5))
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.extendedLayoutIncludesOpaqueBars = true
        definesPresentationContext = true
    }
    
    func setupTableView() {
        
        //registerForPreviewing(with: self, sourceView: tableView)
        tableView.register(cellType: GarageTableViewCell.self)
        tableView.backgroundView = activityIndicatorView
        tableView.tableHeaderView = searchController.searchBar
        //tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        tableView.separatorStyle = .none
        tableView.bounces = false
        tableView.tableHeaderView?.frame = CGRect(x: self.tableView.tableHeaderView?.frame.origin.x ?? 0, y: self.tableView.tableHeaderView?.frame.origin.y ?? 0, width: self.tableView.tableHeaderView?.frame.width ?? 0, height: 0)
        
        let headerNib = UINib.init(nibName: "GarageTableViewHeader", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "GarageTableViewHeader")
    }
    
    func setupNavigationController() {
        let searchButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ico-lens"), style: .done, target: self, action: #selector(pressSearchButton))
        searchButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = searchButton
    }
    
    func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSearch(_ sender: UIButton) {
        pressSearchButton(value: true)
    }
    
    func scrollToTop(){
        DispatchQueue.main.async {
            if self.presenter.numberOfGarages != 0 {
                self.tableView.setContentOffset(.zero, animated: true)
            }
        }
    }
    
    // MARK: - View controller previewing delegate
    
    /*func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location) else {
            return nil
        }
        previewingContext.sourceRect = tableView.rectForRow(at: indexPath)
        let viewController = storyboard?.instantiateViewController(withIdentifier: "GarageVehiclesCollectionViewController") as? GarageVehiclesCollectionViewController
        viewController?.garage = presenter.garage(at: indexPath.row)
        return viewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: nil)
    }*/
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfGarages
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: GarageTableViewCell.self)
        cell.setup(with: presenter.garage(at: indexPath.row), currentLocation: presenter.currentLocation)
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? GarageTableViewCell else { return }
        cell.isUserInteractionEnabled = false
        if findStoreToMaps == true {
            delegate2?.setLocation(garage: presenter.garage(at: indexPath.row))
            
            self.navigationController?.popViewController(animated: true)
        }else{
            presenter.didSelectGarage(at: indexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 126
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GarageTableViewHeader") as? GarageTableViewHeader {
            return headerView
        }
        
        return UIView() 
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    // MARK: - UISearch bar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter.searchButtonTapped(with: searchController.searchBar.text, filterExpress: removeExpressStores)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchGarages(with: searchText, filterExpress: removeExpressStores)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter.fetchCurrentLocation(filterExpress: removeExpressStores)
    }
    
    // MARK: - Garage view delegate
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func startLoading() {
        tableView.tableFooterView = UIView()
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func showVehicles(for garage: GarageDataView) {
        performSegue(withIdentifier: "showVehicles", sender: garage)
    }

    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showEmptyResultsView() {
        tableView.tableFooterView = emptyResultsView
    }
    
    //MARK: - Private Functions
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
        delegate2?.backButton()
    }
    
    @objc func pressSearchButton(value: Bool) {
        self.navigationController?.change()
        self.scrollToTop()
        
        UIView.animate(withDuration: 0.1, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
            self.tableView.tableHeaderView?.frame = CGRect(x: self.tableView.tableHeaderView?.frame.origin.x ?? 0, y: self.tableView.tableHeaderView?.frame.origin.y ?? 0, width: self.tableView.tableHeaderView?.frame.width ?? 0, height: 56.0)
        }, completion: { (finished) -> Void in
            self.searchController.searchBar.becomeFirstResponder()
        })
    }
}
