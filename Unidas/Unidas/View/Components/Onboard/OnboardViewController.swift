//
//  OnboardViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//


import UIKit


protocol OnboardViewControllerDelegate {
    func tapActionButton(onboardingType: OnboardingType)
}

final public class OnboardViewController: UIViewController, MultipleChoiceAlertDelegate {

    
    private let pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                          navigationOrientation: .horizontal,
                                                          options: nil)
    private let pageItems: [OnboardPage]
    private let appearanceConfiguration: AppearanceConfiguration
    private let completion: (() -> Void)?
    var delegate: OnboardViewControllerDelegate?
    
    private lazy var unidasHeaderView: EnhancedView = {
        let unidasHeaderView = EnhancedView()
        unidasHeaderView.translatesAutoresizingMaskIntoConstraints = false
        unidasHeaderView.cornerRadius = 25
        unidasHeaderView.roundTopLeftCorner = false
        unidasHeaderView.roundTopRightCorner = false
        unidasHeaderView.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        
        let logo = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        logo.translatesAutoresizingMaskIntoConstraints = false
        
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let buttonBack = UIButton(type: .system)
        buttonBack.frame = CGRect(x: 15, y: 12, width: 20, height: 20)
        buttonBack.tintColor = .white
        buttonBack.setImage(backButtonImage, for: .normal)
        buttonBack.addTarget(self, action: #selector(touchCancelItem), for: .touchUpInside)
        
        unidasHeaderView.addSubview(logo)
        unidasHeaderView.addSubview(buttonBack)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: unidasHeaderView.centerXAnchor),
            logo.centerYAnchor.constraint(equalTo: unidasHeaderView.centerYAnchor),
        ])
   
        return unidasHeaderView
    }()
    
    private lazy var unidasLogoView: UIImageView = {
        let unidasLogoView = UIImageView(image: #imageLiteral(resourceName: "logo-unidas-onboard"))
        unidasLogoView.translatesAutoresizingMaskIntoConstraints = false
        return unidasLogoView
    }()
    
    private lazy var unidasButton: UnidasButton = {
        let unidasButton = UnidasButton()
        unidasButton.translatesAutoresizingMaskIntoConstraints = false
        return unidasButton
    }()
    
    private lazy var viewPageViewController: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        return pageControl
    }()
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(pageItems: [OnboardPage],
                appearanceConfiguration: AppearanceConfiguration = AppearanceConfiguration(),
                completion: (() -> Void)? = nil) {
        self.pageItems = pageItems
        self.appearanceConfiguration = appearanceConfiguration
        self.completion = completion
        super.init(nibName: nil, bundle: nil)
    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override public func loadView() {
        view = UIView(frame: CGRect.zero)
        let onboardType = appearanceConfiguration.onboardingType
        
        view.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        
        let viewBackground = UIView()
        viewBackground.translatesAutoresizingMaskIntoConstraints = false
        viewBackground.backgroundColor = onboardType.backgroundColor
        view.addSubview(viewBackground)
        
        NSLayoutConstraint.activate([
            viewBackground.topAnchor.constraint(equalTo: view.saferAreaLayoutGuide.topAnchor, constant: 20),
            viewBackground.bottomAnchor.constraint(equalTo: view.saferAreaLayoutGuide.bottomAnchor),
            viewBackground.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor),
            viewBackground.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor)
        ])
     
        pageViewController.setViewControllers([pageViewControllerFor(pageIndex: 0)!],
                                              direction: .forward,
                                              animated: false,
                                              completion: nil)
        pageViewController.dataSource = self
        pageViewController.delegate = self
        
        pageViewController.view.frame = viewPageViewController.bounds
        viewPageViewController.addSubview(pageViewController.view)
        viewBackground.addSubview(viewPageViewController)

        pageControl.pageIndicatorTintColor = onboardType.pageIndicatorColor
        pageControl.currentPageIndicatorTintColor = onboardType.currentPageIndicatorColor
        pageControl.pageIndicatorTintColor = onboardType.pageIndicatorColor
        pageControl.backgroundColor = onboardType.backgroundColor
        
        pageControl.numberOfPages = pageItems.count
        pageControl.currentPage = 0
        
        addChild(pageViewController)
        viewBackground.addSubview(viewPageViewController)
        viewBackground.addSubview(pageControl)
        viewBackground.addSubview(unidasButton)
        
        if appearanceConfiguration.onboardingType == .introduction {
            viewBackground.addSubview(unidasLogoView)
            
            NSLayoutConstraint.activate([
                unidasLogoView.topAnchor.constraint(equalTo: view.saferAreaLayoutGuide.topAnchor, constant: 20),
                unidasLogoView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                unidasLogoView.heightAnchor.constraint(equalToConstant: 33.0),
                viewPageViewController.topAnchor.constraint(equalTo: unidasLogoView.bottomAnchor, constant: 10),
            ])
        }
        
        if appearanceConfiguration.onboardingType == .howToRent {
           viewBackground.addSubview(unidasHeaderView)
            
            NSLayoutConstraint.activate([
                unidasHeaderView.heightAnchor.constraint(equalToConstant: 44.0),
                unidasHeaderView.topAnchor.constraint(equalTo: view.saferAreaLayoutGuide.topAnchor, constant: 0),
                unidasHeaderView.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor, constant: 0),
                unidasHeaderView.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor, constant: 0),
                viewPageViewController.topAnchor.constraint(equalTo: unidasHeaderView.bottomAnchor, constant: 10),
            ])
 
            NSLayoutConstraint.activate([
                viewPageViewController.topAnchor.constraint(equalTo: unidasHeaderView.bottomAnchor, constant: 0),
            ])
        }
        
        NSLayoutConstraint.activate([
            pageControl.centerXAnchor.constraint(equalTo: viewPageViewController.centerXAnchor, constant: 0),
            pageControl.centerYAnchor.constraint(equalTo: viewPageViewController.bottomAnchor, constant: 0),
            pageControl.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            viewPageViewController.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor, constant: 0),
            viewPageViewController.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor, constant: 0),
            viewPageViewController.bottomAnchor.constraint(equalTo: unidasButton.topAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            unidasButton.bottomAnchor.constraint(equalTo: view.saferAreaLayoutGuide.bottomAnchor, constant: -10),
            unidasButton.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor, constant: 20),
            unidasButton.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor, constant: -20),
            unidasButton.heightAnchor.constraint(equalToConstant: 50.0)
        ])
        
        unidasButton.borderWidth = 1
        unidasButton.borderColor = onboardType.buttonBorderColor
        unidasButton.setBackgroundColor(color: onboardType.buttonBackgroundColor, forState: .normal)
        unidasButton.addTarget(self, action: #selector(tapActionButton), for: .touchUpInside)
        unidasButton.setTitle(appearanceConfiguration.buttonTitle, for: .normal)
        pageViewController.didMove(toParent: self)
        if let currentPage = pageViewController.viewControllers?.first as? OnboardPageViewController {setupButton(pageIndex: currentPage.pageIndex)}
    }
    
    @objc func touchCancelItem() {
        if appearanceConfiguration.onboardingType == .howToRent {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func compareAppVersionToUpdate() -> Bool {
        let compared = VersionControl.compareAppVersion()
        return compared
    }
    
    @objc func tapActionButton(){
        
        if compareAppVersionToUpdate() == true {
            askToUpdateApp()
        }else{
            let onboardingType = appearanceConfiguration.onboardingType
            self.navigationController?.popViewController {
                self.delegate?.tapActionButton(onboardingType: onboardingType)
            }
        }
    }
    
    private func pageViewControllerFor(pageIndex: Int) -> OnboardPageViewController? {
        let pageVC = OnboardPageViewController(pageIndex: pageIndex, appearanceConfiguration: appearanceConfiguration)
        guard pageIndex >= 0 else { return nil }
        guard pageIndex < pageItems.count else { return nil }
        pageVC.configureWithPage(pageItems[pageIndex])
        pageVC.delegate = self
        return pageVC
    }
    
    func askToUpdateApp() {
        
        let title = NSLocalizedString("App Update Title", comment: "")
        let subTitle = NSLocalizedString("App Update Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("App Update No", comment: "")
        let secondChoiceLabel = NSLocalizedString("App Update Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-alert"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 3, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        pageControl.widthAnchor.constraint(equalToConstant: pageControl.bounds.width + 20).isActive = true
    }
    
    private func setupButton(pageIndex: Int) {
        
        unidasButton.removeTarget(self, action: #selector(tapActionButton), for: .touchUpInside)
        unidasButton.removeTarget(self, action: #selector(touchCancelItem), for: .touchUpInside)
        
        let onboardingType = appearanceConfiguration.onboardingType == .introduction ? NSLocalizedString("Skip Introduction", comment: "") : NSLocalizedString("New Reserve", comment: "")
        
        let buttonTitle = pageIndex == pageControl.numberOfPages - 1 ? NSLocalizedString("Close", comment: "") : onboardingType
        
        unidasButton.setTitle(buttonTitle, for: .normal)
        
        if appearanceConfiguration.onboardingType == .howToRent {
            if pageIndex == pageControl.numberOfPages - 1 {
                unidasButton.addTarget(self, action: #selector(touchCancelItem), for: .touchUpInside)
            }else{
                unidasButton.addTarget(self, action: #selector(tapActionButton), for: .touchUpInside)
            }
        }else if appearanceConfiguration.onboardingType == .introduction {
            unidasButton.addTarget(self, action: #selector(touchCancelItem), for: .touchUpInside)
        }
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {

    }
    
    func choiceSecondTap(tag: Int) {
        if tag == 2 {
            VersionControl.openAppStore()
        }
    }
}

// MARK: Presenting
public extension OnboardViewController {
    func presentFrom(_ viewController: UIViewController, animated: Bool) {
        viewController.present(self, animated: animated)
    }
}

extension OnboardViewController: UIPageViewControllerDataSource {
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let pageVC = viewController as? OnboardPageViewController else { return nil }
        let pageIndex = pageVC.pageIndex
        guard pageIndex != 0 else { return nil }        
        return pageViewControllerFor(pageIndex: pageIndex - 1)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController,
                                   viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let pageVC = viewController as? OnboardPageViewController else { return nil }
        
        let pageIndex = pageVC.pageIndex
        return pageViewControllerFor(pageIndex: pageIndex + 1)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let currentPage = pageViewController.viewControllers?.first as? OnboardPageViewController {
                DispatchQueue.main.async {
                    self.pageControl.currentPage = currentPage.pageIndex
                    self.setupButton(pageIndex: currentPage.pageIndex)
                }
            }
            pageControl.currentPage = 0
        }
    }
}

extension OnboardViewController: UIPageViewControllerDelegate {
    
}

extension OnboardViewController: OnboardPageViewControllerDelegate {
    func openHelp(_ pageVC: OnboardPageViewController, actionTappedAt index: Int) {
        let onboardPage = pageItems[index]
        
        let storyboard = UIStoryboard(name: "OnboardHelp", bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: "onboardHelpViewController") as? OnboardHelpViewController {
            
            controller.modalPresentationStyle = .pageSheet
            if let helpContent = onboardPage.helpContent {
                let helpContentOrdered = helpContent.sorted { $0.step ?? 0 < $1.step ?? 0 }
                controller.items = helpContentOrdered
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
}

// MARK: - AppearanceConfiguration
public extension OnboardViewController {
    
    typealias ButtonStyling = ((UIButton) -> Void)
    
    struct AppearanceConfiguration {
        let buttonTitle: String
        let onboardingType: OnboardingType
        
        public init(buttonTitle: String? = "Button",
                    onboardingType: OnboardingType = .howToRent) {
            self.buttonTitle = buttonTitle ?? ""
            self.onboardingType = onboardingType
        }
    }
}
