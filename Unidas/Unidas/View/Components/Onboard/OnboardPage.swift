//
//  OnboardPage.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

public typealias OnboardPageCompletion = ((_ success: Bool, _ error: Error?) -> Void)
public typealias OnboardPageAction = (@escaping OnboardPageCompletion) -> Void

public struct OnboardPage {
    let title: String?
    let subTitle: String?
    var imageUrl: URL?
    let description: String?
    let step: String?
    var helpContent: [OnboardHelpContent]?
    
    public init(title: String? = nil,
                subTitle: String? = nil,
                imageUrl: URL? = nil,
                description: String?,
                step: String? = nil) {
        self.title = title
        self.subTitle = subTitle
        self.imageUrl = imageUrl
        self.description = description
        self.step = step
    }
}
