//
//  OnboardPageViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//


import UIKit

internal protocol OnboardPageViewControllerDelegate: class {
    func openHelp(_ pageVC: OnboardPageViewController, actionTappedAt index: Int)
}
internal final class OnboardPageViewController: UIViewController {
    private lazy var rectangleView: EnhancedView = {
        let rectangleView = EnhancedView()
        rectangleView.translatesAutoresizingMaskIntoConstraints = false
        rectangleView.borderColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        rectangleView.borderWidth = 1.0
        rectangleView.cornerRadius = 10
        return rectangleView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = PaddingLabel(withInsets: 0, 0, 10, 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.textAlignment = .center
        label.sizeToFit()
        label.lineBreakMode = .byTruncatingTail
        return label
    }()

    private lazy var helpButton: UIButton = {
        let helpButton = UIButton()
        let buttonTitle = NSLocalizedString("More details", comment: "")
        
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.bold(ofSize: 14),
            .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]

        let attributeString = NSMutableAttributedString(string: buttonTitle,
        attributes: attrs)
        helpButton.setAttributedTitle(attributeString, for: .normal)
        helpButton.translatesAutoresizingMaskIntoConstraints = false
        return helpButton
    }()
    
    let pageIndex: Int
    
    weak var delegate: OnboardPageViewControllerDelegate?
    
    private let appearanceConfiguration: OnboardViewController.AppearanceConfiguration
    
    init(pageIndex: Int, appearanceConfiguration: OnboardViewController.AppearanceConfiguration) {
        self.pageIndex = pageIndex
        self.appearanceConfiguration = appearanceConfiguration
        super.init(nibName: nil, bundle: nil)
        customizeStyleWith(appearanceConfiguration)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func customizeStyleWith(_ appearanceConfiguration: OnboardViewController.AppearanceConfiguration) {
        
        let onboardingType = appearanceConfiguration.onboardingType
        
        view.backgroundColor = onboardingType.backgroundColor
        imageView.contentMode = .scaleAspectFit
        titleLabel.textColor = onboardingType.titleColor
        titleLabel.font = UIFont.bold(ofSize: 14)
        subTitleLabel.textColor = onboardingType.subtitleColor
        subTitleLabel.font = UIFont.bold(ofSize: 14)
        descriptionLabel.textColor = onboardingType.textColor
        descriptionLabel.font = UIFont.regular(ofSize: 14)
    }
    
    override func loadView() {
        view = UIView(frame: CGRect.zero)
        
        view.addSubview(rectangleView)
        
        NSLayoutConstraint.activate([
            rectangleView.topAnchor.constraint(equalTo: view.saferAreaLayoutGuide.topAnchor, constant: 20),
            rectangleView.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor, constant: 20.0),
            rectangleView.bottomAnchor.constraint(equalTo: view.saferAreaLayoutGuide.bottomAnchor, constant: 0),
            rectangleView.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor, constant: 20.0),
            rectangleView.widthAnchor.constraint(equalTo: view.saferAreaLayoutGuide.widthAnchor, constant: -40.0)
        ])
        
        view.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: rectangleView.topAnchor),
        ])
        
        rectangleView.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20.0),
            imageView.centerXAnchor.constraint(equalTo: rectangleView.centerXAnchor),
        ])
        
        rectangleView.addSubview(subTitleLabel)
        NSLayoutConstraint.activate([
            subTitleLabel.centerXAnchor.constraint(equalTo: rectangleView.centerXAnchor),
            subTitleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20)
        ])
        
        rectangleView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: 20.0),
            descriptionLabel.centerXAnchor.constraint(equalTo: rectangleView.centerXAnchor)
        ])
        
        rectangleView.addSubview(helpButton)

        NSLayoutConstraint.activate([
            helpButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 20),
            helpButton.centerXAnchor.constraint(equalTo: rectangleView.centerXAnchor)
        ])
    
        helpButton.isHidden = true
        helpButton.addTarget(self,
                             action: #selector(OnboardPageViewController.helpTapped),
                             for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       //  titleLabel.widthAnchor.constraint(equalToConstant: titleLabel.bounds.width + 20).isActive = true
    }
    
    func configureWithPage(_ page: OnboardPage) {
        configureTitleLabel(page.title ?? "")
        configureImageView(page.imageUrl)
        configureSubTitleLabel(page.subTitle)
        configureDescriptionLabel(page.description)
        configureHelpButton(page.helpContent)
    }
    
    private func configureTitleLabel(_ title: String) {
        titleLabel.text = title
        titleLabel.backgroundColor = appearanceConfiguration.onboardingType.backgroundColor
    }
    
    private func configureSubTitleLabel(_ subTitle: String?) {
        if let subTitle = subTitle {
            subTitleLabel.text = subTitle
            
            NSLayoutConstraint.activate([
                subTitleLabel.widthAnchor.constraint(equalTo: rectangleView.widthAnchor, multiplier: 0.8)
            ])
        } else {
            subTitleLabel.isHidden = true
        }
    }
    
    private func configureImageView(_ imageUrl: URL?) {
        if let imageUrl = imageUrl {
            DispatchQueue.main.async {
                self.imageView.setImage(withURL: imageUrl)
            }
            imageView.heightAnchor.constraint(equalToConstant: 156).isActive = true
        } else {
            imageView.isHidden = true
        }
    }
    
    private func configureDescriptionLabel(_ description: String?) {
        if let pageDescription = description {
            descriptionLabel.text = pageDescription
            NSLayoutConstraint.activate([
                descriptionLabel.widthAnchor.constraint(equalTo: rectangleView.widthAnchor, multiplier: 0.8)
            ])
        } else {
            descriptionLabel.isHidden = true
        }
    }
    
    private func configureHelpButton(_ helpContent: [OnboardHelpContent]?){
        if let _ = helpContent {
            helpButton.isHidden = false
        }
    }
    
    @objc fileprivate func helpTapped() {
        delegate?.openHelp(self, actionTappedAt: pageIndex)
    }
}
