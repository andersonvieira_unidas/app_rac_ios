//
//  ImageGalleryPageViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol ImageGalleryPageViewControllerDelegate: class {
    func imageGallery(_ imageGallery: ImageGalleryPageViewController, didDeleteImageAt index: Int)
}

class ImageGalleryPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, ImageViewerViewControllerDelegate {

    var images: [UIImage] = []
    var currentIndex: Int = 0 {
        didSet {
            updateTitle()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return navigationController?.isNavigationBarHidden ?? false
    }
    
    lazy var closeButtonItem: UIBarButtonItem = { () -> UIBarButtonItem in
        let localizedTitle = NSLocalizedString("Close", comment: "Close bar button item title")
        let button = UIBarButtonItem(title: localizedTitle, style: .plain, target: self, action: #selector(closeButtonItemTapped(_:)))
        return button
    }()
    
    lazy var trashButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(trashButtonItemTapped(_:)))
    lazy var shareButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareButtonItemTapped(_:)))
    
    weak var imageGalleryDelegate: ImageGalleryPageViewControllerDelegate?
    
    var showsCloseButton: Bool = true
    var showsToolbar: Bool = true
    
    var showsNavigationBar: Bool = true {
        didSet {
            guard showsNavigationBar != oldValue else { return }
            if showsNavigationBar {
                showNavigationBar()
            } else {
                hideNavigationBar()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    private func initialSetup() {
        dataSource = self
        delegate = self
        view.backgroundColor = .white
        updateTitle()
        if showsCloseButton {
            navigationItem.setLeftBarButton(closeButtonItem, animated: false)
        }
        
        if showsToolbar {
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            navigationController?.setToolbarHidden(false, animated: false)
            setToolbarItems([shareButtonItem, flexibleSpace, trashButtonItem], animated: true)
        }
        
        guard let initalViewController = viewController(for: currentIndex) else { return }
        setViewControllers([initalViewController], direction: .forward, animated: false, completion: nil)
        
    }
    
    private func viewController(for index: Int) -> UIViewController? {
        let validRange = 0..<images.count
        guard validRange.contains(index) else { return nil }
        let image = images[index]
        let imageViewer = storyboard?.instantiateViewController(withIdentifier: "ImageViewerViewController") as? ImageViewerViewController
        imageViewer?.image = image
        imageViewer?.view.tag = index
        imageViewer?.delegate = self
        imageViewer?.view.backgroundColor = .clear
        return imageViewer
    }
    
    private func hideNavigationBar() {
        let navigationController = self.navigationController ?? parent?.navigationController
        navigationController?.setNavigationBarHidden(true, animated: true)
        navigationController?.setToolbarHidden(true, animated: true)
        set(view: view, backgroundColor: .black, animated: true)
    }
    
    private func showNavigationBar() {
        let navigationController = self.navigationController ?? parent?.navigationController
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.setToolbarHidden(false, animated: true)
        set(view: view, backgroundColor: .white, animated: true)
    }
    
    private func set(view: UIView, backgroundColor: UIColor, animated: Bool) {
        if animated {
            let propertyAnimator = UIViewPropertyAnimator(duration: 0.3, dampingRatio: 1.0, animations: {
                view.backgroundColor = backgroundColor
            })
            propertyAnimator.startAnimation()
        } else {
            view.backgroundColor = backgroundColor
        }
    }
    
    private func deleteImage(at index: Int) {
        images.remove(at: index)
        imageGalleryDelegate?.imageGallery(self, didDeleteImageAt: index)
        let propertyAnimator = UIViewPropertyAnimator(duration: 0.3, dampingRatio: 1.0) {
            if let viewController = self.viewControllers?.first(where: { $0.view.tag == index}) {
                viewController.view.alpha = 0.0
            }
            var direction: UIPageViewController.NavigationDirection = .forward
            if self.currentIndex >= self.images.count {
                self.currentIndex -= 1
                direction = .reverse
            }
            if let viewController = self.viewController(for: self.currentIndex) {
                self.setViewControllers([viewController], direction: direction, animated: true, completion: nil)
            }
            self.updateTitle()
        }
        propertyAnimator.addCompletion { _ in
            if self.images.isEmpty {
                self.dismiss(animated: true, completion: nil)
            }
        }
        propertyAnimator.startAnimation()
    }
    
    private func updateTitle() {
        let localizedTitle = NSLocalizedString("%d of %d", comment: "Gallery title")
        let title = String(format: localizedTitle, currentIndex + 1, images.count)
        navigationItem.title = title
    }
    
    // MARK: - Interface builder actions
    
    @objc func trashButtonItemTapped(_ sender: UIBarButtonItem) {
        let title = NSLocalizedString("Are you sure you want to delete this photo?", comment: "")
        let message = NSLocalizedString("This action can't be undone", comment: "")
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        actionSheet.addAction(.cancel())
        actionSheet.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: { (action) in
            self.deleteImage(at: self.currentIndex)
        }))
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func shareButtonItemTapped(_ sender: UIBarButtonItem) {
        guard !images.isEmpty else { return }
        let activityViewController = UIActivityViewController(activityItems: [images[currentIndex]], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func closeButtonItemTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Image viewer view controller delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        showsNavigationBar = !(scrollView.zoomScale > scrollView.minimumZoomScale)
    }
    
    // MARK: - Page view controller data source
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return self.viewController(for: viewController.view.tag - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return self.viewController(for: viewController.view.tag + 1)
    }
    
    // MARK: - Page view controller delegate
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed, let index = pageViewController.viewControllers?.first?.view.tag {
            currentIndex = index
        }
    }
    
}
