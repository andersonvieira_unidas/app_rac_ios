//
//  ImageViewerViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 31/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol ImageViewerViewControllerDelegate: class {
    func scrollViewDidScroll(_ scrollView: UIScrollView)
}

class ImageViewerViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var delegate: ImageViewerViewControllerDelegate?
    
    var imageView: UIImageView?
    var image: UIImage?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView = UIImageView(image: image)
        scrollView.addSubview(imageView!)
        let imageSize = image?.size ?? .zero
        scrollView.contentSize = imageSize
        imageView?.center = center(from: imageSize)
        adjustScrollViewScale(for: scrollView.frame.size)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        resetZoomScale(animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        adjustPadding()
    }
    
    // MARK: - Interface builder actions
    
    @IBAction func doubleTapGestureRecognizer(_ sender: UITapGestureRecognizer) {
        if scrollView.zoomScale == scrollView.minimumZoomScale {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        }
    }
    
    func adjustScrollViewScale(for size: CGSize) {
        
        let imageSize: CGSize = image?.size ?? .zero
        let widthRatio = min(size.width / imageSize.width, 1.0)
        let heightRatio = min(size.height / imageSize.height, 1.0)
        let minimumZoomScale = min(widthRatio, heightRatio)
        let maximumZoomScale = CGFloat(2.0)
        
        scrollView.minimumZoomScale = minimumZoomScale
        scrollView.maximumZoomScale = maximumZoomScale
    }
    
    func adjustPadding() {
        let imageViewSize = imageView?.frame.size ?? .zero
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
    
    func center(from size: CGSize) -> CGPoint {
        return CGPoint(x: size.width / 2, y: size.height / 2)
    }
    
    func resetZoomScale(animated: Bool) {
        scrollView.setZoomScale(scrollView.minimumZoomScale, animated: animated)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        adjustPadding()
        delegate?.scrollViewDidScroll(scrollView)
    }
    
    // MARK: - Scroll view delegate

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { _ in
            let currentZoomScale = self.scrollView.zoomScale
            let previousMinimumZoomScale = self.scrollView.minimumZoomScale
            let scrollViewSize = self.scrollView.systemLayoutSizeFitting(size)
            self.adjustScrollViewScale(for: scrollViewSize)
            if currentZoomScale < self.scrollView.minimumZoomScale || currentZoomScale == previousMinimumZoomScale {
                self.resetZoomScale(animated: false)
            }
            self.adjustPadding()
        })
    }
    
}
