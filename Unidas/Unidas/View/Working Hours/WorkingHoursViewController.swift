//
//  WorkingHoursViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 01/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class WorkingHoursViewController: UIViewController {

    // MARK: - Instance variables
    
    // MARK: Outlets
    
    @IBOutlet weak var garageNameLabel: UILabel!
    @IBOutlet weak var weekdaysWorkingHoursLabel: UILabel!
    @IBOutlet weak var saturdayWorkingHoursLabel: UILabel!
    @IBOutlet weak var sundayWorkingHoursLabel: UILabel!
    @IBOutlet weak var holidayWorkingHoursLabel: UILabel!
    
    // MARK: Parameters
    var garageDetails: GarageReservationDetailsDataView!
    var storeSelected: GaragesPickerSelected!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup(for: garageDetails)
    }
    
    func setup(for garageDetails: GarageReservationDetailsDataView) {
        let garage = garageDetails.garage
        
        var store = ""
    
        if storeSelected == .pickup {
            store = NSLocalizedString("Pickup Store %@", comment: "")
            garageNameLabel.text = String(format: store, garage?.name ?? "").uppercased()
        } else if storeSelected == .`return` {
            store = NSLocalizedString("Return Store %@", comment: "")
            garageNameLabel.text = String(format: store, garage?.name ?? "").uppercased()
        } else {
            garageNameLabel.text = garage?.name
        }
    
        weekdaysWorkingHoursLabel.text = garage?.weekdaysWorkingHours
        saturdayWorkingHoursLabel.text = garage?.saturdayWorkingHours
        sundayWorkingHoursLabel.text = garage?.sundayWorkingHours
        holidayWorkingHoursLabel.text = garage?.holidayWorkingHours
    }
    
    @IBAction func doneButtonItemTapped(_ sender: Any) {
        dismiss(animated: true)
    }

}
