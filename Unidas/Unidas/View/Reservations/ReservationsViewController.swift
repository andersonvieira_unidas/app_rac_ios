//
//  ReservationsViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import MapKit

protocol ReservationsViewControllerDelegate {
    func reloadReservations()
}

class
ReservationsViewController: UIViewController, ReservationsViewDelegate, EmptyDataViewDelegate,
                            UpcomingReservationTableViewCellDelegate, UpcomingReservationExpressTableViewCellDelegate, NextReservationTableViewDelegate, ReservationsViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, MultipleChoiceAlertDelegate, NextReservationExpressTableViewDelegate {
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newReserveView: UIView!
    @IBOutlet weak var newReserveButton: UnidasButton!
    @IBOutlet weak var newReserveViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var navigationActivityIndicatorView: UIActivityIndicatorView!
    var newReserveViewHeight: CGFloat = 0
    var reservation: VehicleReservationsDataView?
    
    lazy var presenter: ReservationsPresenterDelegate = {
        let presenter = ReservationsPresenter(
            reservationUnidasService: ReservationUnidasService(),
            firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        return presenter
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewDidApppearReload()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupActivityIndicatorView()
        checkForMaintenanceScreen()
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        
        if let height = newReserveViewHeightConstraint?.constant {
            newReserveViewHeight = height
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Private functions
    private func viewDidApppearReload(){
        presenter.refreshReservations()
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        setupToggle()
    }
    private func setupTableView() {
        tableView.register(cellType: NextReservationTableViewCell.self)
        tableView.register(cellType: NextReservationExpressTableViewCell.self)
        tableView.register(cellType: UpcomingReservationTableViewCell.self)
        tableView.register(cellType: UpcomingReservationExpressTableViewCell.self)
        tableView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didRefreshControl), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    private func setupActivityIndicatorView() {
        navigationActivityIndicatorView = UIActivityIndicatorView(style: .white)
        navigationActivityIndicatorView.hidesWhenStopped = true
        let activityItem = UIBarButtonItem(customView: navigationActivityIndicatorView)
        navigationItem.rightBarButtonItem = activityItem
    }
    
    private func checkForMaintenanceScreen() {
        FirebaseRealtimeDatabase.toggleListenerValue(withKey: .maintenance) { (toggle) in
            if toggle {
                if let navigation = self.navigationController {
                    MaintenanceInvoker.presenterMaintenanceController(navigation: navigation)
                }     
            }
        }
    }
    
    private func showPayment(with reservation: VehicleReservationsDataView){
        let storyBoard = UIStoryboard(name: "StandartCarRent", bundle: .main)
        
        if let reservationDetailsController = storyBoard.instantiateViewController(withIdentifier: "ReservationPaymentStepVC") as? ReservationPaymentStepViewController {
            reservationDetailsController.oldReservation = reservation
            reservationDetailsController.isAntecipatedPayment = true
            navigationItem.setBackButtonTitle(title: "")
            show(reservationDetailsController, sender: nil)
        }
    }
    
    private func showPendencies(){
        performSegue(withIdentifier: "showPendencies", sender: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return presenter.numberOfCurrentReservations
        case 1: return presenter.numberOfUpcomingReservations
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reservation = presenter.reservation(at: indexPath)
        switch indexPath.section {
        case 0:
            
            if reservation.express {
                let cell = tableView.dequeueReusableCell(for: indexPath, cellType: NextReservationExpressTableViewCell.self)
                
                cell.setup(myReservationDataView: reservation)
                cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
                cell.delegate = self
                cell.selectionStyle = .none
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(for: indexPath, cellType: NextReservationTableViewCell.self)
                
                cell.setup(myReservationDataView: reservation)
                cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
                cell.delegate = self
                cell.selectionStyle = .none
                
                return cell
            }
            
        case 1:
            if reservation.express {
                let cell = tableView.dequeueReusableCell(for: indexPath, cellType: UpcomingReservationExpressTableViewCell.self)
                cell.delegate = self
                cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
                let vehicleReservationDataView = VehicleReservationsDataView(model: reservation.vehicleReservation)
                let rules = presenter.rules()
                cell.setup(with: vehicleReservationDataView, rules: rules, upcoming: true)
                cell.selectionStyle = .none
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(for: indexPath, cellType: UpcomingReservationTableViewCell.self)
                cell.delegate = self
                cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
                let vehicleReservationDataView = VehicleReservationsDataView(model: reservation.vehicleReservation)
                let rules = presenter.rules()
                cell.setup(with: vehicleReservationDataView, rules: rules)
                cell.selectionStyle = .none
                return cell
            }
            
        default:
            fatalError("Invalid section")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reservation = presenter.reservation(at: indexPath)
        let vehicleReservationDataView = VehicleReservationsDataView(model: reservation.vehicleReservation)
        presenter.didSelectRowAt(at: indexPath, reservation: vehicleReservationDataView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 240
        }
        if indexPath.section ==  1 {
            return 125
        }
        return UITableView.automaticDimension
    }
    
    // MARK: - Reservations View Delegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
        tableView.refreshControl?.endRefreshing()
    }
    
    func reloadData() {
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showEmptyReservationsView() {
        tableView.backgroundView = nil
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        emptyView.delegate = self
        emptyView.frame = tableView.frame
        emptyView.imageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        emptyView.setup(title: NSLocalizedString("Reservations Empty State Title", comment: ""), message: NSLocalizedString("Reservations Empty State Description", comment: ""), image: #imageLiteral(resourceName: "ico_reservations_empty_state"), buttonTitle: NSLocalizedString("New Reservations Empty State Button", comment: ""), buttonIsHidden: false, squareViewColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                emptyView.actionButton.isHidden = true
            }
            self.tableView.backgroundView = emptyView
        }
    }
    
    func showEmptyOffLineView() {
        tableView.backgroundView = nil
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        emptyView.delegate = self
        emptyView.frame = tableView.frame
        emptyView.imageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        
        var empty = Empty(title: NSLocalizedString("Reservations Empty State Title OffLine", comment: ""), message: NSLocalizedString("Reservations Empty State Description OffLine", comment: ""), image: #imageLiteral(resourceName: "ico_reservations_empty_state"), buttonTitle: NSLocalizedString("New Reservations Empty State Button", comment: ""), buttonIsHidden: false)
        empty.squareViewColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        
        let yourLogin = NSLocalizedString("New Reservations Empty State Your Login", comment: "")
        empty.link = "login"
        empty.linkText = yourLogin
        
        emptyView.setup(empty: empty)
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                emptyView.actionButton.isHidden = true
            }
            self.tableView.backgroundView = emptyView
        }
        tableView.backgroundView = emptyView
        
        hideNewReserve()
        endLoading()
    }
    
    func goToReserveDetails(reservation: VehicleReservationsDataView) {
        showUpcomingReservationDetails(reservation: reservation)
    }
    
    func showNotConnectedView() {
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = tableView.frame
        emptyView.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        emptyView.delegate = self
        emptyView.actionButton.tag = 2
        emptyView.setup(title: NSLocalizedString("No internet connection", comment: "No internet connection error title"), message: NSLocalizedString("You must be connected to internet to see your previous resevations", comment: "No internet connection error message"), image: #imageLiteral(resourceName: "icon-connection-off"), buttonTitle: NSLocalizedString("Try again", comment: "Try again button title"), buttonIsHidden: false, squareViewColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        tableView.backgroundView = emptyView
        
        hideNewReserve()
        endLoading()
    }
    
    func hideNewReserve() {
        newReserveViewHeightConstraint?.constant = 0
        newReserveView.isHidden = true
        newReserveButton.isHidden = true
    }
    
    func showNewReserve() {
        newReserveView.isHidden = false
        newReserveButton.isHidden = false
        newReserveViewHeightConstraint?.constant = newReserveViewHeight
    }
    
    func setupToggle(){
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                self.hideNewReserve()
            }
        }
    }
    
    func showCheckIn(vehicleReservationDataView: VehicleReservationsDataView) {
        performSegue(withIdentifier: "showPayment", sender: vehicleReservationDataView)
    }
    
    func showConfirmCheckin(reservation: VehicleReservationsDataView) {
        self.reservation = reservation
        
        let title = NSLocalizedString("Checkin are you sure title", comment: "")
        let message = NSLocalizedString("Checkin are you sure", comment: "")
        let firstButtonLabel = NSLocalizedString("Checkin ok", comment: "")
        let secondButtonLabel = NSLocalizedString("Checkin cancel", comment: "")
        let image = #imageLiteral(resourceName: "icon-cancel-reservation-alert")
        let multipleChoiceAlert = MultipleChoiceAlert(title: title, subTitle: message, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel, image: image, firstChoiceButtonColor: UIColor.themeImportantButton)
        
        multipleChoiceAlert.delegate = self
        multipleChoiceAlert.show(animated: true)
        
    }
    
    // MARK: - NextReservationTableViewDelegate
    func showDetails(reservation: VehicleReservationsDataView) {
        performSegue(withIdentifier: "showPreview", sender: reservation)
    }
    
    func reloadNextReservationExpired() {
        viewDidApppearReload()
    }
    
    //MARK: -UpcomingReservationTableViewCellDelegate
    func showUpcomingReservationDetails(reservation: VehicleReservationsDataView) {
        performSegue(withIdentifier: "showPreview", sender: reservation)
    }
    
    func reloadUpcomingReservationExpired() {
        viewDidApppearReload()
    }
    
    func canShowPendencies(value: Bool) {
        if value {
            showPendencies()
        }else{
            showGoToStoreModal()
        }
    }
    
    func showGoToStoreModal() {
        guard let storyboard = UIStoryboard(name: "Pendencies", bundle: nil) as? UIStoryboard else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "SerasaCredDefenseNotificationVC") as? SerasaCredDefenseNotificationViewController {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func showAction(action: ReservationAction, reservation: VehicleReservationsDataView) {
        switch action {
        case .checkin:
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                presenter.checkInReservation(reservation: reservation, isCheckinConfirmed: false)
            }
        case .editProfile:
            presenter.fetchCredDefenseSerasaStatus()
        case .payment:
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                showPayment(with: reservation)
            }
        case .qrcode:
            if reservation.express {
                guard let storyboard = UIStoryboard(name: "ContractHistory", bundle: nil) as? UIStoryboard else {return}
                if let vc = storyboard.instantiateViewController(withIdentifier: "ContractQRCodeViewController") as? ContractQRCodeViewController {
                    vc.qrCodeInformation = reservation.qrCode
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            } else {
                performSegue(withIdentifier: "showMyCar", sender: reservation.contractNumber)
            }
            
        }
    }
    
    func didTapLogin() {
        performSegue(withIdentifier: "showLogin", sender: nil)
    }
    
    func startLoadingAtIndexPath(indexPath: IndexPath) {
        navigationActivityIndicatorView.startAnimating()
        presenter.isEnabledNextReservation(false)
        tableView.allowsSelection = false
    }
    
    func endLoadingAtIndexPath(indexPath: IndexPath) {
        navigationActivityIndicatorView.stopAnimating()
        
        presenter.isEnabledNextReservation(true)
        tableView.allowsSelection = true
    }
    
    func isEnabledNextReservation(_ isEnabled: Bool) {
        guard let nextReservation = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ReservationTableViewCell else { return }
        nextReservation.setFieldsEnabled(isEnabled)
    }
    
    func askToUpdateApp() {
        let title = NSLocalizedString("App Update Title", comment: "")
        let subTitle = NSLocalizedString("App Update Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("App Update No", comment: "")
        let secondChoiceLabel = NSLocalizedString("App Update Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-alert"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 3, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    // MARK: - InterfaceBuilder Actions
    @IBAction func tapNewReserve(_ sender: Any) {
        if presenter.compareAppVersionToUpdate() == true {
            askToUpdateApp()
        }else{
            performSegue(withIdentifier: "showStandartCar", sender: nil)
        }
    }
    
    @objc func didRefreshControl() {
        presenter.refreshReservations()
    }
    
    // MARK: - EmptyDataViewDelegate
    
    func emptyDataView(_ emptyDataView: EmptyDataView, didTapActionButton actionButton: UIButton) {
        if actionButton.tag == 2 {
            presenter.refreshReservations()
        } else {
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                performSegue(withIdentifier: "showStandartCar", sender: nil)
            }   
        }
    }
    
    func touchLink(link: String) {
        if link == "login" {
            self.tabBarController?.selectedIndex = 3
        }
    }
    
    func call(URL: String) {
        print(URL)
    }
    
    //MARK: - ReservationsTableViewControllerDelegate
    func reloadReservations() {
        reloadData()
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        if let reservation = reservation {
            presenter.checkInReservation(reservation: reservation, isCheckinConfirmed: true)
        }
    }
    
    func choiceSecondTap(tag: Int) {
        if tag == 2 {
            VersionControl.openAppStore()
        }
    }
    
    //MARK: - NextReservationExpressTableViewDelegate
    func showUpcomingReservationExpressDetails(reservation: VehicleReservationsDataView) {
        performSegue(withIdentifier: "showPreview", sender: reservation)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStandartCar" {
            if let reservation = sender as? VehicleReservationsDataView {
                let navigation = segue.destination as? UINavigationController
                let standartCarRent = navigation?.viewControllers.first as? StandartCarInformationViewController
                standartCarRent?.redirectToSelectGarage = false
                standartCarRent?.oldReservation = reservation
            } else {
                let navigation = segue.destination as? UINavigationController
                let standartCarRent = navigation?.viewControllers.first as? StandartCarInformationViewController
                standartCarRent?.redirectToSelectGarage = true
            }
        }
        else if segue.identifier == "showPreview" {
            let navigation = segue.destination as? UINavigationController
            let reservationDetailsViewController = navigation?.viewControllers.first as? ReserveDetailsViewController
            navigation?.modalPresentationStyle = .fullScreen
            reservationDetailsViewController?.vehicleReservationsDataView = sender as? VehicleReservationsDataView
            reservationDetailsViewController?.rules = presenter.rules()
        }
        else if segue.identifier == "showPayment" {
            let navigation = segue.destination as? UINavigationController
            let paymentViewController = navigation?.viewControllers.first as? PaymentViewController
            paymentViewController?.vehicleReservationsDataView = sender as? VehicleReservationsDataView
            //paymentViewController?.delegate = self
        }
        else if segue.identifier == "showMyCar" {
            let navigation = segue.destination as? UINavigationController
            let myCarViewController = navigation?.viewControllers.first as? MyCarViewController
            myCarViewController?.contractNumber = sender as? Int
        }
        
    }
}
