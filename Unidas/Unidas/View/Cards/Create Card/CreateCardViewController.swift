//
//  CreateCardViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import InputMask

protocol CreateCardDelegate: class {
    func cardCreated()
}

class CreateCardViewController: UIViewController, MaskedTextFieldDelegateListener, CreditCardsDetailsFormViewDelegate,
CreditCardFormViewCameraDelegate, CustomCameraOcrDelegate, UIGestureRecognizerDelegate{
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var creditCardFormView: CreditCardFormView!
    @IBOutlet weak var helperView: UIView?

    private var presenter: CreditCardsDetailsFormPresenter!
    weak var delegate: CreateCardDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderUnidas()
        
        presenter = CreditCardsDetailsFormPresenter(paymentService: PaymentService(), firebaseAnalyticsService: FirebaseAnalyticsService(), loginPciService: LoginPciService(), creditCardPciService: CreditCardPciService())
        presenter.delegate = self
        
        setupNavigationController()
        
        creditCardFormView.setup(titleIsHidden: true)
        creditCardFormView.delegate = self
    }
    
    private func setupNavigationController() {
        self.navigationController?.navigationBar.shouldRemoveShadow(true)

        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchCreateCard(_ sender: Any) {
        presenter.create(creditCardFormDataView: creditCardFormView.presenter.creditCardFormDataView)
    }

    // MARK: - SignUpCreditCarViewDelegate
    
    func startLoading() {
        nextButton.setTitleColor(.white, for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        nextButton.isUserInteractionEnabled = false
        nextButton.loading(true, activityIndicatorViewStyle: .white)
    }
    
    func endLoading() {
        nextButton.setTitleColor(.white, for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        nextButton.isUserInteractionEnabled = true
        nextButton.loading(false)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func finishCreateCard() {
        dismiss(animated: true) {
            self.delegate?.cardCreated()
        }
    }
    
    func showCamera() {
         performSegue(withIdentifier: "segueToCamera", sender: nil)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToCamera" {
            if let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.viewControllers.first as? CustomCameraOcrViewController {
                viewController.devicePosition = .back
                viewController.backgroundImage = #imageLiteral(resourceName: "crop-mask-creditcard")
                viewController.customCameraOcrDelegate = self
            }
        }
    }
    
    func ocrResult(results: [String]) {
        guard let dateValidThru = CreditCardOcrUtil.extractValidThru(results: results), let cardNumber = CreditCardOcrUtil.extractCardNumber(results: results) else {
            showMessageErrorOcr()
            return
        }
        let dateValidThruFormatted = ValueFormatter.format(date: dateValidThru, format: "MM/yyyy")
        creditCardFormView.updateValues(cardNumber: cardNumber, dateValidThru: dateValidThruFormatted)
    }
    
    private func showMessageErrorOcr() {
        DispatchQueue.main.async {
            let title = NSLocalizedString("Unidas", comment: "Unidas Comment")
            let message = NSLocalizedString("Unable to process OCR", comment: "Unable to process OCR comment")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(.ok())
           self.present(alert, animated: true, completion: nil)
        }
    }

}
