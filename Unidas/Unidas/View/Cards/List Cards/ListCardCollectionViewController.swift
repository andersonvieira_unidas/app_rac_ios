//
//  ListCardCollectionViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol ListCardDelegate: class {
    func selectedCard(at listCardDataView: ListCardDataView)
}

class ListCardCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, ListCardViewDelegate, CreateCardDelegate, ListCardCollectionViewCellDelegate, MultipleChoiceAlertDelegate {

    private var presenter: ListCardPresenterDelegate!
    weak var delegate: ListCardDelegate?
    var indexPath: IndexPath?
    
    var isPickerMode: Bool = true
    let backView = UIView()
    
    var isHiddenCheckbox = false
    var isHiddenFavoriteIcon = true
    var isHiddenMoreActions = true
    
    let button = UIButton(type: .custom)
    var fromPendencies = false
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isToolbarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        presenter = ListCardPresenter(paymentService: PaymentService())
        presenter.delegate = self        
        setupCollectionView()
        //setupFooterView(customView: backView)
        setupAddButton(button: button)
        setupNavigationController()
        presenter.fetchCard()
        self.navigationController?.change()
        setHeaderUnidas()
    }
    
    func setupNavigationController() {
        //let title = (isPickerMode) ? NSLocalizedString("Switch Card", comment: "Switch Card navigation title") : NSLocalizedString("Payment", comment: "Payment navigation title")
        
        //self.title = title
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
                
        if isPickerMode {
            let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(touchDoneItem))
            navigationItem.rightBarButtonItem = doneItem
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func setupCollectionView() {
        
        let listCardCollectionViewLayout = ListCardCollectionViewLayout()
        collectionView?.collectionViewLayout = listCardCollectionViewLayout
        collectionView?.register(cellType: ListCardsCollectionViewCell.self)
        collectionView?.register(PaymentHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView")
    }
    
    func addButtonIsHidden(isHidden: Bool) {
         button.isHidden = isHidden
     }

    func setupAddButton(button: UIButton) {
        
        let bottonHeight = fromPendencies == true ? -69 : -20
        
        //let backgroundView = UIView()
        //backgroundView.backgroundColor = UIColor.blue
        //let height = CGFloat(90)
        //backgroundView.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        //self.view.addSubview(backgroundView)
        self.view.addSubview(button)
//        if #available(iOS 11.0, *) {
//        NSLayoutConstraint.activate([
//            backgroundView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
//            backgroundView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
//            backgroundView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor,constant: -height),
//            backgroundView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
//        ])
//        }else{
//            backgroundView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//            backgroundView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
//            backgroundView.leftAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
//            backgroundView.heightAnchor.constraint(equalToConstant: 70).isActive = true
//            backgroundView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
//        }
        if #available(iOS 11.0, *) {
            button.centerXAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerXAnchor).isActive = true
            button.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -20).isActive = true
            button.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 20).isActive = true
            button.heightAnchor.constraint(equalToConstant: 50).isActive = true
            button.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: CGFloat(bottonHeight)).isActive = true
        } else {
            button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
            button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
            button.leftAnchor.constraint(equalTo: view.rightAnchor, constant: 20).isActive = true
            button.heightAnchor.constraint(equalToConstant: 50).isActive = true
            button.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: CGFloat(bottonHeight)).isActive = true
        }
        button.addTarget(self, action: #selector(touchAddNewCard), for: .touchUpInside)
        button.layer.cornerRadius = 20
        button.layer.masksToBounds = true
        button.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        button.setTitle(NSLocalizedString("Add new card", comment: "Add new card toolbar button title"), for: .normal)
        button.titleLabel?.font =  UIFont.regular(ofSize: 16)
        button.tintColor = UIColor.white
    }
    
    @objc func touchAddNewCard() {
        performSegue(withIdentifier: "showCreateCard", sender: nil)
    }
    
    @objc func touchCancelItem() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func touchDoneItem() {
        dismiss(animated: true) {
            self.delegate?.selectedCard(at: self.presenter.getSelectedCard())
        }
    }

    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfCards
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: ListCardsCollectionViewCell.self)
        
        cell.selectedCard(presenter.selectedCard(at: indexPath.row))
        cell.isHiddenButton(isHiddenCheckbox: isHiddenCheckbox, isHiddenMoreActions: isHiddenMoreActions)
        cell.setup(listCardDataView: presenter.card(at: indexPath.row), isHiddenFavoriteIcon: isHiddenFavoriteIcon)
        
        cell.delegate = self
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectItemAt(at: indexPath.row)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerView", for: indexPath)
            let label = UILabel()
            label.text = NSLocalizedString("PAGAMENTO", comment: "")
            label.frame = CGRect(x: 60, y: 20, width: 100, height: 25)
            label.font = UIFont.medium(ofSize: 14)
            label.textColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
            label.textAlignment = .left
            let icon = UIImageView()
            icon.image = #imageLiteral(resourceName: "icon-creditcard-signup").withRenderingMode(.alwaysTemplate)
            icon.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
            icon.frame = CGRect(x: 20, y: 20, width: 30, height: 25)
            headerView.addSubview(icon)
            headerView.addSubview(label)
            headerView.backgroundColor = UIColor.white
            
            return headerView
            
//        case UICollectionView.elementKindSectionFooter:
//            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footerView", for: indexPath)
//
//            footerView.backgroundColor = UIColor.green
//            return footerView
            
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return .init(width: view.frame.width, height: 60)
    }
    
    // MARK - ListCardViewDelegate
    
    func startLoading() {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.startAnimating()
        collectionView?.backgroundView = activityIndicatorView
    }
    
    func endLoading() {
        collectionView?.backgroundView = nil
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func reloadData() {
        collectionView?.reloadData()
    }
    
    func enableDone() {
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    // MARK - CreateCardDelegate
    
    func cardCreated() {
        presenter.fetchCard()
    }
    
    // MARK: - ListCardCollectionViewCellDelegate
    
    func didTapMoreAction(_ listCardsCollectionViewCell: ListCardsCollectionViewCell) {
        guard let indexPath = collectionView?.indexPath(for: listCardsCollectionViewCell) else { return }
        
        presenter.didTapMoreAction(at: indexPath)
    }
    
    func showEmptyCardView() {
        
        let backView = UIView()
        backView.frame = collectionView!.frame
        backView.backgroundColor = UIColor.white
        collectionView?.backgroundView = backView
        
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = CGRect(x: 0,y: 60 ,width: backView.frame.width,height: 400)
        emptyView.imageView.image = #imageLiteral(resourceName: "icon-empty-creditcard")
        emptyView.imageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        emptyView.titleLabel.text = NSLocalizedString("No credit card found", comment: "No credit card found message")
        emptyView.messageTextView.text = NSLocalizedString("Seems like you didn't registered your cards here, register a card right now", comment: "Seems like you didn't registered your cards here, register a card right now message")
        emptyView.messageTextView.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        emptyView.titleLabel.textColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        emptyView.actionButton.isHidden = true
        backView.addSubview(emptyView)
        
        let cardButton = UIButton(type: .system)
        cardButton.setTitle(NSLocalizedString("Add new card", comment: "Add new card toolbar button title"), for: .normal)
        cardButton.addTarget(self, action: #selector(touchAddNewCard), for: .touchUpInside)
        cardButton.frame = CGRect(x: 20, y: 400, width: backView.frame.width - 40, height: 50)
        cardButton.layer.cornerRadius = 20
        cardButton.layer.masksToBounds = true
        cardButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        cardButton.tintColor = UIColor.white
        cardButton.titleLabel?.font = UIFont.medium(ofSize: 16)
        backView.addSubview(cardButton)
        reloadData()
    }
    
    func showActions(at indexPath: IndexPath, isMarkFavoriteHidden: Bool) {
        presentCustomAlert(indexPath: indexPath)
    }
    
    private func presentCustomAlert(indexPath: IndexPath) {
        
        self.indexPath = indexPath
        
        let title = NSLocalizedString("Card selected", comment: "")
        let subTitle = NSLocalizedString("What do you want to do?", comment: "")
        let image = #imageLiteral(resourceName: "icon-empty-creditcard")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: NSLocalizedString("CreditCard choice favourite", comment: ""), secondChoiceButtonLabel: NSLocalizedString("CreditCard choice exclude", comment: ""), image: image, firstChoiceButtonColor: UIColor.themeImportantButton)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    // Mark: - MultipleChoicAlertDelegate
    func choiceFirstTap(tag: Int) {
        if let indexPath = indexPath {
            presenter.markFavoriteCard(at: indexPath)
        }
    }
    
    func choiceSecondTap(tag: Int) {
       if let indexPath = indexPath {
            presenter.deleteCard(at: indexPath)           
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {        
        if segue.identifier == "showCreateCard" {
            let destination = segue.destination as? UINavigationController
            let createCardViewController = destination?.viewControllers.first as? CreateCardViewController
            createCardViewController?.delegate = self
            destination?.navigationController?.navigationItem.setBackButtonTitle(title: "")
        }
    }
}
