//
//  MaintenanceViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 05/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class MaintenanceViewController: UIViewController {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var messageSubTitle: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupImageNavigation()
        setupListener()
    }
    
    func setupListener() {
        var enviroment = "config_ios_prd"
        #if DEBUG && DEVELOPMENT
        enviroment = "config_ios_dev"
        #endif
        
        let ref = Database.database().reference()
        ref.child("menu").child(enviroment).child("maintenance").observe(.value, with: { snapshot in
            if let value = snapshot.value as? Bool {
                self.dismissController(value: value)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func setupLayout() {
        backView.layer.cornerRadius = 5
        messageTitle.text = RemoteConfigUtil.recoverParameterValue(with: "maintenance_message_title")
        messageSubTitle.text = RemoteConfigUtil.recoverParameterValue(with: "maintenance_message")
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        setupStatusBar(lightContent: true)
    }
    
    func setupStatusBar(lightContent: Bool) {
        if lightContent == true {
            UINavigationBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
            UINavigationBar.appearance().isTranslucent = false
        }else{
            UINavigationBar.appearance().tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
            UINavigationBar.appearance().isTranslucent = false
        }
    }
    
    func setupImageNavigation() {
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 270, height: 30))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "logo-nav-light-content")
        imageView.image = image
        logoContainer.addSubview(imageView)
        navigationItem.titleView = logoContainer
    }
    
    func dismissController(value: Bool) {
        if value == false {
            setupStatusBar(lightContent: false)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
