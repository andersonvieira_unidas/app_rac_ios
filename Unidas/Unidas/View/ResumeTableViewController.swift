//
//  ResumeTableViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 03/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class ResumeTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SwipeInteractionControllerDraggable {

    @IBOutlet weak var dragIndicatorView: UIView!
    @IBOutlet weak var downIndicatorImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpLocationLabel: UILabel!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnLocationLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var administrativeTaxValueLabel: UILabel!
    @IBOutlet weak var administrativeTaxPercentValueLabel: UILabel!
    @IBOutlet weak var totalPrePaymentValueLabel: UILabel!
    @IBOutlet weak var totalPrePaymentDiscountValueLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var payInAdvanceStack: UIStackView!
    @IBOutlet weak var payAtPickupStack: UIStackView!
    
    var draggableView: UIView {
        return dragIndicatorView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var quotationResume: QuotationResumeDataView!
    var isVoucherApplied: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let downIndicator = downIndicatorImageView {
            downIndicator.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }

        setup(tableView: tableView)
        setup(for: quotationResume)
        setupPickUp(for: quotationResume.pickUp)
        setupReturn(for: quotationResume.return)
        setup(for: quotationResume.quotation)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        resizeTableViewHeaderViewIfNeeded()
    }
    
    func resizeTableViewHeaderViewIfNeeded() {
        guard let headerView = tableView.tableHeaderView else { return }
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            tableView.tableHeaderView = headerView
            tableView.layoutIfNeeded()
        }
    }
    
    func setup(for quotationResume: QuotationResumeDataView) {
        //pay in advanced
        totalPrePaymentValueLabel.text = quotationResume.prePaymentTotalWithAdministrativeTax
        //pay at pickup
        totalValueLabel.text = quotationResume.totalWithAdministrativeTax
        //administrative tax
        administrativeTaxValueLabel.text = quotationResume.administrativeTax
        
        if isVoucherApplied {
             totalPrePaymentDiscountValueLabel.text = NSLocalizedString("By paying now you'll receive a\ndiscount of %@ with voucher applied", comment: "Pre-payment description label")
        } else {
            
            var description = quotationResume.calculation?.prePaymentDiscountPercent
            
            if let type = quotationResume.model.pickUp?.garage.type {
                if type == .express {
                    description = NSLocalizedString("Unidas Express Payment Description", comment: "")
                    totalValueLabel.text = ""
                    payAtPickupStack.isHidden = true
                    totalPrePaymentValueLabel.text = quotationResume.totalWithAdministrativeTax
                }
            }
            
            totalPrePaymentDiscountValueLabel.text = description
        }
       
        if let adminstrativeTaxPercentValue = quotationResume.administrativeTaxPercentage {
            administrativeTaxPercentValueLabel.text = "(\(adminstrativeTaxPercentValue))"
        }
       
        if let isFranchise = quotationResume.pickUp?.garage?.isFranchise, isFranchise {
            payInAdvanceStack.isHidden = true
        }
    }
    
    func setup(tableView: UITableView) {
        tableView.register(cellType: ResumeTableViewCell.self)
        tableView.register(headerFooterViewType: ResumeHeaderFooterView.self)
    }
    
    // MARK: - Adding values to view
    
    func setupPickUp(for pickUp: GarageReservationDetailsDataView?) {
        
        let localizedString = NSLocalizedString("Date %@ at %@", comment: "")
        
        if let date = pickUp?.dateOnly, let hour = pickUp?.hourOnly {
             pickUpDateLabel.text = String(format: localizedString, date, hour)
        }
        pickUpLocationLabel.text = pickUp?.locationDescription
    }
    
    func setupReturn(for return: GarageReservationDetailsDataView?) {
        
        let localizedString = NSLocalizedString("Date %@ at %@", comment: "")
        
        if let date = `return`?.dateOnly, let hour = `return`?.hourOnly {
             returnDateLabel.text = String(format: localizedString, date, hour)
        }
        returnLocationLabel.text = `return`?.locationDescription
    }
    
    func setup(for quotation: QuotationsDataView?) {
        groupNameLabel.text = quotation?.formattedGroupWithDescription
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return quotationResume.resumeInformations.count
        case 1: return quotationResume.protections.count
        case 2: return quotationResume.equipments.count
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ResumeTableViewCell.self)
        
        switch indexPath.section {
        case 0: cell.setup(with: quotationResume.resumeInformations[indexPath.row])
            break
        case 1: cell.setup(with: quotationResume.protections[indexPath.row])
            break
        case 2: cell.setup(with: quotationResume.equipments[indexPath.row])
            break
        default: break
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0 where !quotationResume.resumeInformations.isEmpty: return 40.0
        case 1 where !quotationResume.protections.isEmpty: return 40.0
        case 2 where !quotationResume.equipments.isEmpty: return 40.0
        default: return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(ResumeHeaderFooterView.self)
        switch section {
        case 0 where !quotationResume.resumeInformations.isEmpty:
            headerView?.titleLabel.text = NSLocalizedString("Daily and hours", comment: "Daily and hours section title")
            headerView?.detailLabel.text = quotationResume.totalDiaryWithFeesAndDiscount
            break
        case 1 where !quotationResume.protections.isEmpty:
            headerView?.titleLabel.text = NSLocalizedString("Protections", comment: "Protections section title")
            headerView?.detailLabel.text = quotationResume.totalProtections
            break
        case 2 where !quotationResume.equipments.isEmpty:
            headerView?.titleLabel.text = NSLocalizedString("Equipments", comment: "Equipments section title")
            headerView?.detailLabel.text = quotationResume.totalEquipments
            break
        default: return nil
        }
        return headerView
    }
    
    @IBAction func tapDownIndicator() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
