//
//  LaunchScreenViewController.swift
//  Unidas
//
//  Created by Anderson on 01/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import UIKit
import Lottie

class LaunchScreenViewController: UIViewController{
    @IBOutlet weak var animationView: AnimationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOrientation()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didFinishLaunchingNotification, object: nil)

        setupAnimation()
    }
    
    @objc func appMovedToBackground(){
        guard let view = animationView else { return }
        view.contentMode = .scaleAspectFill
        view.pause()
    }
    
    @objc func appMovedToForeground(){
        guard let view = animationView else { return }
        
        if view.isAnimationPlaying {
            view.play()
        } else {
            redirectToMain()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupAnimation(){
        guard let view = animationView else { return }
        
        let intro = Animation.named("intro", subdirectory: "Animations")
        view.animation = intro
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let view = animationView else { return }
        view.loopMode = .playOnce
        view.play { (finished) in
            if finished {
                self.redirectToMain()
            }
        }
    }
    
    private func setupOrientation(){
       AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    private func redirectToMain(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let homeVC = storyboard.instantiateViewController(withIdentifier: "initController") as? MainTabBarViewController {
            appDelegate.window!.rootViewController = homeVC
        }
        
    }
}
