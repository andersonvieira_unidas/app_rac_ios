//
//  ContractQRCodeViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class ContractQRCodeViewController: UIViewController {

    @IBOutlet weak var qrCodeView: QRCodeView!
    
    var qrCodeInformation: String?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qrCodeView.message = qrCodeInformation
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    // MARK: - Interface Builder Actions
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
