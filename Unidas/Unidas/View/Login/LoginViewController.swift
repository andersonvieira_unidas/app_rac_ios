//
//  LoginViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import InputMask
import LocalAuthentication

class LoginViewController: UIViewController, MaskedTextFieldDelegateListener, LoginViewDelegate, PINViewControllerDelegate, LoginDismissDelegate, UITextFieldDelegate, MultipleChoiceAlertDelegate {
  
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var cpfTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var newAccountButton: UIButton!
    
    private var cpfMaskedDelegate: MaskedTextFieldDelegate!
    private var presenter: LoginPresenterDelegate!
    
    lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    
    lazy var toolbarAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toolbarDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    private var biometryState: BiometryState {
        let authContext = LAContext()
        var error: NSError?
        
        let biometryAvailable = authContext.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error)
        if #available(iOS 11.0, *) {
            if let laError = error as? LAError, laError.code == LAError.Code.biometryLockout {
                return .locked
            }
        } else {
            return .notAvailable
        }
        return biometryAvailable ? .available : .notAvailable
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderUnidas()
        
        presenter = LoginPresenter( unidasLoginService: UnidasLoginService(),
                                    userCheckService: UserCheckService(),
                                    imageTemporaryDirectory: ImageTemporaryDirectory(accountImageService: AccountImageService()),
                                    unidasUserStatusService: UnidasUserStatusService(),
                                    firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        setupView()
        setupMaskTextFields()
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back-button"), style: .done, target: self, action: #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        keyboardScrollViewHandler.unregisterForKeyboardNotifications()
    }
    
    private func setupView(){
        let colorFont = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        let dontHaveAnAccount = NSLocalizedString("Don't have an account - New account", comment: "")
        let attributedString = NSMutableAttributedString(string: dontHaveAnAccount, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 14), NSAttributedString.Key.foregroundColor: colorFont])
        let signupText = NSLocalizedString("Sign up", comment: "")
        
        let attrs: [NSAttributedString.Key: Any] = [
            .font: UIFont.bold(ofSize: 14),
            .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let rangeLink = (dontHaveAnAccount as NSString).range(of: signupText)
        attributedString.addAttributes(attrs, range: rangeLink)
        
        newAccountButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    private func setupMaskTextFields() {
        cpfMaskedDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[000].[000].[000]-[00]")
        cpfMaskedDelegate.listener = self
        cpfTextField.delegate = cpfMaskedDelegate
        cpfTextField.inputAccessoryView = toolbarAccessoryView
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        return true
    }
    
    @IBAction func touchAuthCpf(_ sender: UIButton) {
        
        let documentNumber = cpfTextField.text ?? ""
        let entryExists = KeychainHelper.available(key: documentNumber.removeDocumentNumberCharacters)
        
        cpfTextField.resignFirstResponder()
        cpfTextField.endEditing(true)
        
        if entryExists {
            checkFaceTouchId(documentNumber: documentNumber.removeDocumentNumberCharacters)
        } else {
            self.presenter.authWithCpf(for: documentNumber)
        }
    }
    
    private func checkFaceTouchId(documentNumber: String){
        checkBiometryState { success in
            guard success else {
                return
            }
            let authContext = LAContext()
            let accessControl = KeychainHelper.getBioSecAccessControl()
            let reasonBio = NSLocalizedString("Login Biometric Reason Access", comment: "")
            authContext.evaluateAccessControl(accessControl,
                                              operation: .useItem,
                                              localizedReason: reasonBio) {
                                                (success, error) in
                                                if success, let data = KeychainHelper.loadBioProtected(key: documentNumber,
                                                                                                       context: authContext) {
                                                    let dataStr = String(decoding: data, as: UTF8.self)
                                                    self.presenter.authWithPin(documentNumber: documentNumber, pin: dataStr)
                                                } else {
                                                    self.showAlertManuallyLogin()
                                                }
            }
        }
    }
    
    private func checkBiometryState(_ completion: @escaping (Bool)->Void) {
        //showBiometryState()
        let bioState = self.biometryState
        guard bioState != .notAvailable else {
            presenter.removeKeyChain(withKey: cpfTextField.text ?? "")
            completion(false)
            return
        }
        if bioState == .locked {
            // To unlock biometric authentication iOS requires user to enter a valid passcode
            let authContext = LAContext()
            let reasonBio = NSLocalizedString("Login Biometric Reason Access", comment: "")
            authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                       localizedReason: reasonBio,
                                       reply: { (success, error) in
                                        DispatchQueue.main.async {
                                            if success {
                                                completion(true)
                                            } else {
                                                // self.showStatus("Can't read entry, error: \(error?.localizedDescription ?? "-")")
                                                completion(false)
                                            }
                                        }
            })
        } else {
            completion(true)
        }
    }
    
    @objc func touchCancelItem() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - LoginViewDelegate
    
    func startLoading() {
        authButton.isEnabled = false
        authButton.isUserInteractionEnabled = false
        authButton.loading(true, activityIndicatorViewStyle: .white)
    }
    
    func endLoading() {
        authButton.isUserInteractionEnabled = true
        authButton.isEnabled = true
        authButton.loading(false)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func present(error: String) {
        alertError(errorMessage: error)
    }
    
    func presentFormError() {
        cpfTextField.changeColorBorder(isOn: true)
    }
    
    func finishPin(pinViewController: PINViewController) {
        pinViewController.dismiss(animated: true, completion: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func pinEndLoading(pinViewController: PINViewController) {
        pinViewController.endLoading()
    }
    
    func pinPresent(error: Error, pinViewController: PINViewController) {
        pinViewController.present(error: error, callbackDidFailFinishPin: true)
    }
    
    func pinPresent(title: String, error: String, pinViewController: PINViewController) {
        let simpleAlert = SimpleAlert(title: title, subTitle: error, image:  #imageLiteral(resourceName: "icon-more-about"))
        simpleAlert.show(animated: true)
    }
      
    func performSignUp(userResponseDataView: UserResponseDataView?, formAction: FormsAction) {
        let sender: [String: Any?] = ["userReponseDataView": userResponseDataView, "formAction": formAction]
        performSegue(withIdentifier: "showSignup", sender: sender)
    }
    
    func performPin(userResponseDataView: UserResponseDataView) {
        performSegue(withIdentifier: "showPin", sender: userResponseDataView)
    }
    
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlertManuallyLogin(){
        DispatchQueue.main.async {
            let title = NSLocalizedString("Login Biometric Fail Title", comment: "")
            let subtitle = NSLocalizedString("Login Biometric Fail Description", comment: "")
            let yes = NSLocalizedString("Yes", comment: "")
            let no = NSLocalizedString("No", comment: "")
            
            let image = #imageLiteral(resourceName: "icon-profile-signup").imageWithColor(color: UIColor.white) ?? #imageLiteral(resourceName: "icon-profile-signup")
    
            let multipleChoice = MultipleChoiceAlert(title: title, subTitle: subtitle , firstChoiceButtonLabel: no, secondChoiceButtonLabel: yes, image: image )
            multipleChoice.delegate = self
            multipleChoice.show(animated: true)
        }
    }
    
    @objc func toolbarDoneButtonTapped(sender: UIBarButtonItem) {
        cpfTextField.resignFirstResponder()
    }
    // MARK: - PINViewControllerDelegate
    
    func didTapCancelPin() {
        
    }
    
    func didFailFinishPin(error: Error, pinViewController: PINViewController) {
        
    }
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        presenter.authWithPin(userResponseDataView: userResponseDataView, pin: pin, pinViewController: pinViewController)
        
        checkBiometryState { success in
            guard success else {
                return
            }
            if let documentNumber = userResponseDataView?.model.account.documentNumber{
                self.presenter.addCredentials(documentNumber: documentNumber, pin: pin)
            }
        }
        
    }
    
    // MARK: - ForgotPINViewControllerProtocol
    
    func dismissLogin() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
    }
    
    func choiceSecondTap(tag: Int) {
        presenter.removeKeyChain(withKey: cpfTextField.text ?? "")
        self.presenter.authWithCpf(for: cpfTextField.text ?? "")
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPin" {
            if let navigationController = segue.destination as? UINavigationController {
                if let viewController = navigationController.viewControllers.first as? PINViewController {
                    viewController.userResponseDataView = sender as? UserResponseDataView
                    viewController.navigationTitle = NSLocalizedString("Type your PIN", comment: "Type your pin navigation title")
                    viewController.isHiddenForgotPin = false
                    viewController.isHiddenCancelButton = false
                    viewController.isHiddenBackButton = false
                    viewController.delegate = self
                    viewController.loginDismissDelegate = self
                }
            }
        }
        else if segue.identifier == "showSignup" {
            if let viewController = segue.destination as? UserDetailsFormViewController {
                viewController.loginDismissDelegate = self
                
                guard let sender = sender as? [String:Any] else { return }
                viewController.userResponseDataView = sender["userReponseDataView"] as? UserResponseDataView
                viewController.formsAction = sender["formAction"] as! FormsAction
            }
        }
    }
}
