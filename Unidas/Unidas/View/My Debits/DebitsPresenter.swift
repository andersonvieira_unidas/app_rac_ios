//
//  DebitsPresenter.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import Alamofire

protocol DebitsViewDelegate: class {
    func startLoading()
    func endLoading()
    func present(error: Error)
    func reloadData()
    func showEmptyDebitsView()
}

protocol DebitsPresenterDelegate {
    var delegate: DebitsViewDelegate? { get set }
    
    var numberOfRows: Int { get }
    func debit(at row: Int) -> DebitsDataView
    func fetchDebits()
}

class DebitsPresenter: DebitsPresenterDelegate {
    weak var delegate: DebitsViewDelegate?
    
    private var debitsService: DebitsService
    private var debits: [Debits] = []
    
    init(debitService: DebitsService) {
        self.debitsService = debitService
        NotificationCenter.default.addObserver(self, selector: #selector(fetchDebits), name: .UNDidUnreadMessage, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func fetchDebits() {
        guard let currentUser = session.currentUser else { return }
        self.delegate?.startLoading()
        
        debitsService.fetch(documentNumber: currentUser.account.documentNumber, response: (success: { [weak self] debits in
            self?.debits = debits
            
            if debits.count == 0 {
                self?.delegate?.showEmptyDebitsView()
                return
            }
            
            self?.delegate?.reloadData()
            }, failure: { [weak self] error in                
                if let error = error as? ServiceError, error.code == 404 {
                    self?.delegate?.showEmptyDebitsView()
                    return
                }
                self?.delegate?.present(error: error)
            }, completion:{ [weak self] in
                self?.delegate?.endLoading()
        }))
    }
    
    var numberOfRows: Int {
        return debits.count
    }
    
    func debit(at row: Int) -> DebitsDataView {
        return DebitsDataView(model: debits[row])
    }
}
