//
//  Debits.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

struct Debits: Codable {
    let empresa: String?
    let tipoTitulo: String?
    let praca: String?
    let titulo: Int?
    let prefixo: String?
    let emissao: String?
    let vencimento: String?
    let contrato: Int?
    let loja: String?
    let dataRetirada: String?
    let dataDevolucao: String?
    let clienteDocumento: String?
    let clienteNome: String?
    let placa: String?
    let centroCusto: String?
    let natureza: String?
    let total: Double?
    let multa: Double?
    let juros: Double?
    let parcela: String?
    let tituloSiga: String?
    
    enum CodingKeys: String, CodingKey {
        case empresa = "Empresa"
        case tipoTitulo = "TipoTitulo"
        case praca = "Praca"
        case titulo = "Titulo"
        case prefixo = "Prefixo"
        case emissao = "Emissao"
        case vencimento = "Vencimento"
        case contrato = "Contrato"
        case loja = "Loja"
        case dataRetirada = "DataRetirada"
        case dataDevolucao = "DataDevolucao"
        case clienteDocumento = "ClienteDocumento"
        case clienteNome = "ClienteNome"
        case placa = "Placa"
        case centroCusto = "CentroCusto"
        case natureza = "Natureza"
        case total = "Total"
        case multa = "Multa"
        case juros = "Juros"
        case parcela = "Parcela"
        case tituloSiga = "TituloSiga"
    }
}
