//
//  DebitsTableViewCell.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

class DebitsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var backView: UIView!
    
    
    func setup(debitsDataView: DebitsDataView) {
        self.title.text = debitsDataView.nature
        self.descriptionLabel.text = debitsDataView.contract
        self.date.text = debitsDataView.dueDate
        self.total.text = debitsDataView.total
        
        backView.layer.cornerRadius = 4
        backView.layer.borderWidth = 1
        backView.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        
        //self.backgroundColor = (debitsDataView.read) ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) :  #colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 0.05)
    }
}
