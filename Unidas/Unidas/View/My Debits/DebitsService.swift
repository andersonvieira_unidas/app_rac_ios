//
//  DebitsService.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

class DebitsService: Service {
    var configuration: ServiceConfiguration = .unidas
    
    func fetch(documentNumber: String, response: ModelResponse<[Debits]>) {
        let url = "\(endPointURL)Cliente/AtrasoFinanceiro?cliente=\(documentNumber)"
        //let url = "http://www.mocky.io/v2/5dee5cda33000091959846da"
        
        request(url,
                method: .get,
                headers: configuration.headers,
                response: response)
    }
}
