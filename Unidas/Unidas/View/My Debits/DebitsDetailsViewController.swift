//
//  DebitsDetailsViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

class DebitsDetailsViewController: UIViewController, DebitsDetailsViewDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var debitsDataView: DebitsDataView!
    var shouldMarkAsReadOnViewDidLoad: Bool = true
    
    lazy var presenter: DebitsDetailsPresenterDelegate = {
        let presenter = DebitsDetailsPresenter(debitService: DebitsService())
        presenter.delegate = self
        return presenter
    }()
    
    override var previewActionItems: [UIPreviewActionItem] {
        let markAsReadAction = UIPreviewAction(title: NSLocalizedString("Mark as Read", comment: "Mark as Read preview action"), style: .default) { (action, viewController) in
            self.presenter.markMessageAsRead()
        }
        return [markAsReadAction]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setup(for debit: DebitsDataView) {
        titleLabel.text = debit.nature
        dateLabel.text = debit.dueDate
    }
}
