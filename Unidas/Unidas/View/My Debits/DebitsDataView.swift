//
//  DebitsDataView.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

struct DebitsDataView {
    let model: Debits
    
    static let dateFormatter: DateFormatter = { () -> DateFormatter in
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
        return dateFormatter
    }()
    
    var nature: String? {
        return model.natureza
    }
    
    var contract: String? {
        if let contrato = model.contrato {
            return "\(contrato)"
        }
        return ""
    }
    
    var dueDate: String? {
        guard let date = DebitsDataView.dateFormatter.date(from: model.vencimento ?? "") else { return nil }
        return "\(DateFormatter.shortDateFormatterAPI.string(from: date))"
    }
    
    var total: String? {
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: model.total ?? 0.0)) else { return ""}
        return amountString
    }
}
