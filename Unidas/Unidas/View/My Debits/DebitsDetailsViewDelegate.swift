//
//  DebitsDetailsViewDelegate.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

protocol DebitsDetailsViewDelegate: class {
    
    func setup(for debit: DebitsDataView)
}

protocol DebitsDetailsPresenterDelegate {
    
    var delegate: DebitsDetailsViewDelegate? { get set }
    
    func viewDidLoad(debit: DebitsDataView, shouldMarkAsRead: Bool)
    func markMessageAsRead()
}

class DebitsDetailsPresenter: DebitsDetailsPresenterDelegate {
    
    weak var delegate: DebitsDetailsViewDelegate?
    
    var debitService: DebitsService
    
    var debit: Debits!
    
    init(debitService: DebitsService) {
        self.debitService = debitService
    }
    
    func viewDidLoad(debit: DebitsDataView, shouldMarkAsRead: Bool) {
        self.debit = debit.model
        delegate?.setup(for: debit)
        if shouldMarkAsRead {
            //markMessageAsRead()
        }
    }
    
    func markMessageAsRead() {
//        guard let documentNumber = session.currentUser?.account.documentNumber else { return }
//        messageService.unreadMessage(documentNumber: documentNumber, message: message, response: (success: {
//            NotificationCenter.default.post(name: .UNDidUnreadMessage, object: nil)
//        }, failure: { _ in }, completion: { }))
    }
    
}
