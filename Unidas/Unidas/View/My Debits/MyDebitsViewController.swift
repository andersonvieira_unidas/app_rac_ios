//
//  MyDebitsViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 09/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

class MyDebitsViewController: UIViewController, DebitsViewDelegate, UIViewControllerPreviewingDelegate, UITableViewDelegate, UITableViewDataSource {
    private var presenter: DebitsPresenterDelegate!
    
    @IBOutlet weak var debitsTableView: UITableView!
    @IBOutlet weak var debitsImage: UIImageView!
    
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DebitsPresenter(debitService: DebitsService())
        presenter.delegate = self
        setupView()
        presenter.fetchDebits()
        reloadData()
        configureBackButton()
    }
    
    func setupView() {
        debitsImage.image = #imageLiteral(resourceName: "icon-debit").withRenderingMode(.alwaysTemplate)
        debitsImage.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)   
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DebitsTableViewCell
        cell.setup(debitsDataView: presenter.debit(at: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "showDebitDetail", sender: tableView.cellForRow(at: indexPath))
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    
    
    // MARK: - MessagesViewDelegate
    
    func startLoading() {
        //tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func reloadData() {
        debitsTableView.backgroundView = nil
        debitsTableView.reloadData()
    }
    
    func showEmptyDebitsView() {
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = debitsTableView.frame
        emptyView.imageView.image = #imageLiteral(resourceName: "icon-mydebits").withRenderingMode(.alwaysTemplate)
        emptyView.imageView.tintColor = #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)        
        emptyView.titleLabel.text = RemoteConfigUtil.recoverParameterValue(with: "mydebits_title_empty_state").uppercased()
        emptyView.messageTextView.text = RemoteConfigUtil.recoverParameterValue(with: "mydebits_sub_title_empty_state")
        emptyView.actionButton.isHidden = true
        debitsTableView.backgroundView = emptyView
    }
    
    // MARK: - Previewing delegate
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = debitsTableView.indexPathForRow(at: location) else { return nil }
        let debitsDetailsViewController = storyboard?.instantiateViewController(withIdentifier: "DebitsDetailsViewController") as? DebitsDetailsViewController
        debitsDetailsViewController?.debitsDataView = presenter.debit(at: indexPath.row)
        debitsDetailsViewController?.shouldMarkAsReadOnViewDidLoad = false
        previewingContext.sourceRect = debitsTableView.rectForRow(at: indexPath)
        return debitsDetailsViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let debitDetailsViewController = viewControllerToCommit as? DebitsDetailsViewController {
            debitDetailsViewController.presenter.markMessageAsRead()
        }
        show(viewControllerToCommit, sender: nil)
    }
    
    //MARK: - Private Functions
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDebitDetail", let cell = sender as? UITableViewCell, let indexPath = debitsTableView.indexPath(for: cell) {
            let destination = segue.destination as? DebitsDetailsViewController
            destination?.debitsDataView = presenter.debit(at: indexPath.row)
            destination?.shouldMarkAsReadOnViewDidLoad = true
        }
    }
    
}
