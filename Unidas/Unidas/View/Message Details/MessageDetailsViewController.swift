//
//  MessageDetailsViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class MessageDetailsViewController: UIViewController, MessageDetailsViewDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    
    var messageDataView: MessageDataView!
    var shouldMarkAsReadOnViewDidLoad: Bool = true
    
    lazy var presenter: MessageDetailsPresenterDelegate = {
        let presenter = MessageDetailsPresenter(messageService: MessageService())
        presenter.delegate = self
        return presenter
    }()
    
    override var previewActionItems: [UIPreviewActionItem] {
        let markAsReadAction = UIPreviewAction(title: NSLocalizedString("Mark as Read", comment: "Mark as Read preview action"), style: .default) { (action, viewController) in
            self.presenter.markMessageAsRead()
        }
        return [markAsReadAction]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextView()
        self.navigationController?.change()
        setHeaderUnidas()
        configureBackButton()
        presenter.viewDidLoad(message: messageDataView, shouldMarkAsRead: shouldMarkAsReadOnViewDidLoad)
    }
    
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setup(for message: MessageDataView) {
        
        titleLabel.text = message.title
        dateLabel.text = message.date
        messageTextView.text = message.message
    }
    
    private func setupTextView() {
        messageTextView.textContainerInset = UIEdgeInsets.zero
        messageTextView.textContainer.lineFragmentPadding = 0
    }
}
