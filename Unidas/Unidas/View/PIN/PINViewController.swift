//
//  PINViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol PINViewControllerDelegate: class {
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController)
    func didFailFinishPin(error: Error, pinViewController: PINViewController)
    func didTapCancelPin()
}


class PINViewController: UIViewController, PINViewDelegate, SimpleAlertDelegate{
    @IBOutlet weak var pinStackView: UIStackView!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    private var presenter: PINPresenterDelegate!
    private var activityIndicatorView: UIActivityIndicatorView!
    
    var navigationTitle: String?
    var isHiddenForgotPin: Bool = true
    var isHiddenCancelButton: Bool = false
    var isHiddenBackButton: Bool = true
    var callbackDidFailFinishPin = false
    var confirmationChange = false
    var error: Error? = nil
    
    var userResponseDataView: UserResponseDataView?
    
    weak var delegate: PINViewControllerDelegate?
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.AppUtility.lockOrientation(.portrait)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        
        presenter = PINPresenter()
        
        presenter.delegate = self
        setupView()
        setupActivityIndicator()
        
        presenter.viewDidLoad(navigationTitle: navigationTitle, isHiddenForgotPin: isHiddenForgotPin, isHiddenCancelButton: isHiddenCancelButton)
        
        AppDelegate.AppUtility.lockOrientation(.portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    @objc func touchCancelItem() {
        self.delegate?.didTapCancelPin()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchNewPin(_ sender: PINButton) {
        presenter.selectNewPinValue(at: sender.tag)
    }
    
    @IBAction func touchBackspace(_ sender: PINButton) {
        presenter.backspacePin()
    }
    
    // MARK: - Private Functions
    
    func enableNotifications() {
        self.tabBarController?.tabBar.items?[1].isEnabled = true
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func setupView() {
        if !isHiddenCancelButton {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back-button"), style: .done, target: self, action: #selector(touchCancelItem))
            navigationItem.leftBarButtonItem = backButton
        }
        
        self.title = navigationTitle
        forgotPasswordButton.isHidden = isHiddenForgotPin
    }
    
    func backToPreviousView() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - PIN View Delegate
    
    func startLoading() {
        forgotPasswordButton.isEnabled = false
        pinStackView.isUserInteractionEnabled = false
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        forgotPasswordButton.isEnabled = true
        pinStackView.isUserInteractionEnabled = true
        activityIndicatorView.stopAnimating()
        removeAllPins()
    }
    
    func present(error: Error, callbackDidFailFinishPin: Bool) {
        self.error = error
        let error = error.localizedDescription.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: ".", with: "")
        let title = NSLocalizedString("Error title", comment: "")
        
        let image = #imageLiteral(resourceName: "icon-more-about")
        let simpleAlert = SimpleAlert(title: title, subTitle: error, image: image)
        simpleAlert.delegate = self
        simpleAlert.show(animated: true)
    }
    
    func successPin(title: String?, message: String?) {
        let image = #imageLiteral(resourceName: "icon-change-password-menu")
        let simpleAlert = SimpleAlert(title: title ?? "", subTitle: message ?? "", image: image)
        confirmationChange = true
        simpleAlert.delegate = self
        simpleAlert.show(animated: true)
    }
    
    func showPin(at index: Int) {
        let pinView = EnhancedView()
        pinView.borderWidth = 1.0
        pinView.borderColor = .white
        pinView.cornerRadius = 5.0
        pinView.backgroundColor = .clear
        pinView.clipsToBounds = true
        pinView.tag = index
        let heightContraints = NSLayoutConstraint(item: pinView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 10)
        let widthConstraints = NSLayoutConstraint(item: pinView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 10)
        
        pinView.addConstraints([heightContraints, widthConstraints])
        pinStackView.addArrangedSubview(pinView)
    }
    
    func addNewPin(count: Int) {
        let newIndex = count - 1
        let pinView = pinStackView.subviews[newIndex] as! EnhancedView
        pinView.backgroundColor = .white
    }
    
    func removePin(count: Int) {
        let pinView = pinStackView.subviews[count] as! EnhancedView
        pinView.backgroundColor = .clear
    }
    
    func removeAllPins() {
        presenter.removeAllValue()
        
        pinStackView.subviews.forEach { (response) in
            let pinView = response as! EnhancedView
            pinView.backgroundColor = .clear
        }
    }
    
    func didFinishTyping(at pin: String) {
        delegate?.didFinishPin(userResponseDataView: userResponseDataView, pin: pin, pinViewController: self)
    }
    
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?) {
        let mainStoryboard = UIStoryboard(name: "PIN", bundle: .main)
        let pinViewController = mainStoryboard.instantiateViewController(withIdentifier: "PINViewController") as! PINViewController
        pinViewController.modalPresentationStyle = .fullScreen
        pinViewController.navigationTitle = title
        pinViewController.userResponseDataView = userResponseDataView
        pinViewController.isHiddenForgotPin = isHiddenForgotPin
        pinViewController.isHiddenCancelButton = isHiddenCancelButton
        pinViewController.userResponseDataView = userResponseDataView
        pinViewController.delegate = delegate
        
        self.show(pinViewController, sender: nil)
    }
    
    //MARK: - SimpleAlertDelegate
    func close() {
        if confirmationChange {
            self.enableNotifications()
            self.dismiss(animated: true, completion: nil)
        }
        if callbackDidFailFinishPin, let error = self.error{
            self.delegate?.didFailFinishPin(error: error, pinViewController: self)
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToForgotPin" {
            
            let navigation = segue.destination as? UINavigationController
            if let viewController = navigation?.viewControllers.first as? ForgotPINViewController {
                viewController.userResponseDataView = userResponseDataView
                viewController.delegate = loginDismissDelegate
            }
        }
    }
}

