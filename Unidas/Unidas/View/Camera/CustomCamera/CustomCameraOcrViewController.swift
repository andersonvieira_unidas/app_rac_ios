//
//  CustomCameraOcrViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 26/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit
import AVFoundation
//import FirebaseMLVision
//import FirebaseMLModelInterpreter
//import FirebaseMLCommon

protocol CustomCameraOcrDelegate: class {
    func ocrResult(results: [String])
}

class CustomCameraOcrViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    @IBOutlet weak var backgroundMask: UIImageView?
    
    var captureSession = AVCaptureSession()
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var customCameraDetailsViewController: CustomCameraDetailsViewController?
    var devicePosition: AVCaptureDevice.Position = .back
    var backgroundImage: UIImage?
    
    weak var customCameraOcrDelegate: CustomCameraOcrDelegate?
    
   // private lazy var vision = Vision.vision()
    //private lazy var textRecognizer = vision.onDeviceTextRecognizer()

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let backgroundMask = backgroundMask else { return }
        backgroundMask.image = backgroundImage
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
        setupOrientationToPortrait()
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    @IBAction func touchDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchCapture(_ sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    // MARK - Camera Configuration
    
    func setupOrientationToPortrait() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        currentCamera = devices.first { $0.position == devicePosition }
    }
    
    func setupInputOutput() {
        do {
            guard let camera = currentCamera else { return }
            let captureDeviceInput = try AVCaptureDeviceInput(device: camera)
            captureSession.addInput(captureDeviceInput)
            
            if #available(iOS 11.0, *) {
                let capturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([capturePhotoSettings], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            } else {
                let settings = AVCapturePhotoSettings()
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([settings], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            }
        }
        catch {
            print(error)
        }
    }
    
    func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
        
    }
    
    // MARK - AVCapturePhotoCaptureDelegate
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let dataImage = photo.fileDataRepresentation() {
            if let image = UIImage(data: dataImage) {
               runTextRecognition(with: image)
            }
        }
    }
    
    func runTextRecognition(with image: UIImage) {
        //let visionImage = VisionImage(image: image)
        
       // textRecognizer.process(visionImage) { features, error in
        //    self.processResult(from: features, error: error)
       // }
    }
    
    //func processResult(from text: VisionText?, error: Error?) {
        /*guard error == nil, let text = text else {
            let errorString = error?.localizedDescription ?? ""
            print("Text recognizer failed with error: \(errorString)")
            return
        }
        
        var results: [String] = []
        
        // Blocks.
        for block in text.blocks {
            // Lines.
            for line in block.lines {
                // Elements.
                for element in line.elements {
                    results.append(element.text)
                }
            }
        }*/
        //customCameraOcrDelegate?.ocrResult(results: results)
     //   dismiss(animated: true, completion: nil)
    //}
}
