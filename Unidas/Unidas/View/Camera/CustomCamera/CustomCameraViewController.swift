//
//  CustomCameraViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import AVFoundation

class CustomCameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    @IBOutlet weak var backgroundMask: UIImageView!
    
    var captureSession = AVCaptureSession()
    var currentCamera: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    var customCameraDetailsViewController: CustomCameraDetailsViewController?
    var devicePosition: AVCaptureDevice.Position = .back
    var backgroundImage: UIImage?
    var showLoadingMessage = false
    
    @IBOutlet weak var messageCNHVertical: UILabel!
    @IBOutlet weak var messageCNHHorizontal: UILabel!
    
    var messageFlag = false
    
    weak var customCameraDetailsDelegate: CustomCameraDetailsDelegate?
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundMask.image = backgroundImage
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
        setupMessages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupOrientationToPortrait()
        AppDelegate.AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func touchDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchCapture(_ sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    // MARK - Camera Configuration
    func setupMessages() {
        if messageFlag == true {
            messageCNHVertical.isHidden = false
            messageCNHHorizontal.isHidden = true
        }
        messageCNHVertical.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
    }
    
    func setupOrientationToPortrait() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        currentCamera = devices.first { $0.position == devicePosition }
    }
    
    func setupInputOutput() {
        do {
            guard let camera = currentCamera else { return }
            let captureDeviceInput = try AVCaptureDeviceInput(device: camera)
            captureSession.addInput(captureDeviceInput)
            
            if #available(iOS 11.0, *) {
                let capturePhotoSettings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([capturePhotoSettings], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            } else {
                let settings = AVCapturePhotoSettings()
                photoOutput = AVCapturePhotoOutput()
                photoOutput?.setPreparedPhotoSettingsArray([settings], completionHandler: nil)
                captureSession.addOutput(photoOutput!)
            }
        }
        catch {
            print(error)
        }
    }
    
    func setupPreviewLayer() {
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = self.view.frame
        self.view.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession() {
        captureSession.startRunning()
        
    }
    
    // MARK - AVCapturePhotoCaptureDelegate

    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let dataImage = photo.fileDataRepresentation() {
            if let image = UIImage(data: dataImage) {
                var imageWidth = image.size.width
                var compactImage = image
                let size = recoverImageSizeDriverLicense()
                let quality = recoverImageQualityDriverLicense()
                while imageWidth > size {
                    compactImage = compactImage.resized(withPercentage: 0.8) ?? image
                    imageWidth = compactImage.size.width
                }
                if let jpegImage = compactImage.jpegData(compressionQuality: quality), let image = UIImage(data: jpegImage) {
                    performSegue(withIdentifier: "segueToCameraDetails", sender: image)
                } else {
                    performSegue(withIdentifier: "segueToCameraDetails", sender: compactImage)
                }
            }
        }
    }
    
   /* func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let dataImage = photo.fileDataRepresentation() {
            if let image = UIImage(data: dataImage) {
                
                let imageWidth = image.size.width
                
                if imageWidth > 2000 {
                    let compactImage = image.resized(withPercentage: 0.4)
                    
                    performSegue(withIdentifier: "segueToCameraDetails", sender: compactImage)
                } else {
                    let compactImage = image.resized(withPercentage: recoverPercentageImageCompress())
                    performSegue(withIdentifier: "segueToCameraDetails", sender: compactImage)
                }
            }
        }
    }*/
    
    /*func recoverPercentageImageCompress() -> CGFloat {
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        guard let imagePorcentageCompress = remoteConfig["percentage_image_resize"].numberValue else { return CGFloat(1.0) }
        return CGFloat(truncating: imagePorcentageCompress)
    }*/

    func recoverImageSizeDriverLicense() -> CGFloat {
        return CGFloat(RemoteConfigUtil.recoverParameterIntValue(with: "image_size_driver_license"))
    }
    
    func recoverImageQualityDriverLicense() -> CGFloat {
        let qualityString = RemoteConfigUtil.recoverParameterValue(with: "image_quality_driver_license")
        return CGFloat((qualityString as NSString).floatValue)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToCameraDetails" {
            if let viewController = segue.destination as? CustomCameraDetailsViewController {
                viewController.camera = sender as? UIImage
                viewController.delegate = customCameraDetailsDelegate
                viewController.showLoadingMessage = showLoadingMessage
            }
        }
    }
}

