//
//  CustomCameraDetailsViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol CustomCameraDetailsDelegate: class {
    func acceptPhoto(image: UIImage, viewController: CustomCameraDetailsViewController)
}

class CustomCameraDetailsViewController: UIViewController {
    var camera: UIImage?
    weak var delegate: CustomCameraDetailsDelegate?
    @IBOutlet weak var loadingBackground: UIView!
    @IBOutlet weak var photoBackground: UIImageView!
    @IBOutlet weak var rejectedButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: EnhancedView!
    @IBOutlet weak var messageLoadingView: UIView!
    @IBOutlet weak var messageLoadingLabel: UILabel!
    
    
    var showLoadingMessage = false
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoBackground.image = camera
        setupView()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func touchAccept(_ sender: Any) {
        if let image = camera?.updateImageOrientionUpSide() {
            self.delegate?.acceptPhoto(image: image, viewController: self)
        }
    }
    
    @IBAction func touchRecuse(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func startLoading() {
        loadingBackground.isHidden = false
        acceptButton.isHidden = true
        rejectedButton.isHidden = true
        loadingView.isHidden = false
        loadingActivityIndicator.startAnimating()
        
        if showLoadingMessage {
            messageLoadingView.isHidden = false
        }
    }
    
    func endLoading() {
        loadingBackground.isHidden = true
        acceptButton.isHidden = false
        rejectedButton.isHidden = false
        loadingView.isHidden = true
        messageLoadingView.isHidden = true
        loadingActivityIndicator.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    private func setupView(){
        
        let loadingText = NSLocalizedString("Driver License Loading Text", comment: "")
        let loadingBoldText = NSLocalizedString("Driver License Loading Bold Text", comment: "")
               
        let muttableString = NSMutableAttributedString(string: loadingText)
       
        if let boldRange = loadingText.range(of: loadingBoldText) {
            let range = NSRange(boldRange, in: loadingText)
            muttableString.addAttribute(NSAttributedString.Key.font, value: UIFont.bold(ofSize: 14), range: range)
        }
        messageLoadingLabel.attributedText = muttableString
  
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
