//
//  DriverLicenseFormView.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import Reusable
import InputMask

protocol DriverLicenseFormDelegate: class {
    func present(error: Error, driverLicenseFormView: DriverLicenseFormView)
}

class DriverLicenseFormView: UIView, NibOwnerLoadable, MaskedTextFieldDelegateListener, UITextFieldDelegate, DriverLicenseFormViewDelegate {
    
    @IBOutlet weak var documentNumberTextField: UITextField!
    @IBOutlet weak var entityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var registrationNumberTextField: UITextField!
    @IBOutlet weak var validityTextField: UITextField!
    @IBOutlet weak var fatherNameTextField: UITextField!
    @IBOutlet weak var motherNameTextField: UITextField!
    @IBOutlet weak var firstLicenseTextField: UITextField!
 
    //MARK: - Mask Delegate
    lazy var documentNumberMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[______________]")
    lazy var stateMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[AA]")
    lazy var registrationNumberMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[000000000000]")
    
    lazy var registrationNumberToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(registrationNumberToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var validityDatePicker = { () -> UIDatePicker in
        var datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        return datePicker
    }()
    
    lazy var firstLicenseDatePicker = { () -> UIDatePicker in
        var datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        return datePicker
    }()
    
    lazy var validityToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(validityToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var firstLicenseToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(firstLicenseToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    weak var delegate: DriverLicenseFormDelegate?
    
    var documentNumber: String? {
        if let documentNumber = documentNumberTextField.text {
            return documentNumber.removeDocumentNumberCharacters
        }
        return ""
    }
    
    var entity: String? {
        return entityTextField.text
    }
    
    var state: String? {
        return stateTextField.text
    }
    
    var registrationNumber: String? {
        return registrationNumberTextField.text
    }
    
    var validity: Date? {
        guard let text = validityTextField.text, !text.isEmpty else { return nil }
        return validityDatePicker.date
    }
    
    var firstDocument: Date? {
        guard let text = firstLicenseTextField.text, !text.isEmpty else { return nil }
        return firstLicenseDatePicker.date
    }
    
    var fatherName: String? {
        return fatherNameTextField.text
    }
    
    var motherName: String? {
        return motherNameTextField.text
    }
    
    var validThru: Date? {
        guard let text = validityTextField.text, !text.isEmpty else { return nil }
        return validityDatePicker.date
    }
    
    var driverLicenseFormDataView: DriverLicenseFormDataView? {
        return presenter.driverLicenseFormDataView
    }

    func documentNumberError(isOn: Bool) {
        documentNumberTextField.changeColorBorder(isOn: isOn)
    }
    
    func entityError(isOn: Bool) {
        entityTextField.changeColorBorder(isOn: isOn)
    }
    
    func stateError(isOn: Bool) {
        stateTextField.changeColorBorder(isOn: isOn)
    }
    
    func registrationNumber(isOn: Bool) {
        registrationNumberTextField.changeColorBorder(isOn: isOn)
    }
    
    func validityError(isOn: Bool) {
        validityTextField.changeColorBorder(isOn: isOn)
    }
    
    func firstLicenseError(isOn: Bool) {
        firstLicenseTextField.changeColorBorder(isOn: isOn)
    }
    
    func fatherNameError(isOn: Bool) {
        fatherNameTextField.changeColorBorder(isOn: isOn)
    }

    func motherNameError(isOn: Bool) {
        motherNameTextField.changeColorBorder(isOn: isOn)
    }
    
    var presenter: DriverLicenseFormPresenterDelegate!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
        setupDelegates()
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    func initialSetup() {
        loadNibContent()
        presenter = DriverLicenseFormPresenter()
        presenter.delegate = self
    }
    
    func setupDelegates() {
        documentNumberTextField.delegate = documentNumberMaskDelegate
        entityTextField.delegate = self
        stateTextField.delegate = stateMaskDelegate
        registrationNumberTextField.delegate = registrationNumberMaskDelegate
        validityTextField.delegate = self
        firstLicenseTextField.delegate = self
        fatherNameTextField.delegate = self
        motherNameTextField.delegate = self
    }
    
    func setupView(){
        registrationNumberTextField.inputAccessoryView = registrationNumberToolbar
        setupValidityTextField()
        setupFirstLicenseTextField()
        
        if let motherName = session.currentUser?.account.motherName {
            motherNameTextField.text = motherName
        }
    }
    
    func fieldsEnabled(_ enabled: Bool) {
        documentNumberTextField.isEnabled = enabled
        stateTextField.isEnabled = enabled
        stateTextField.isEnabled = enabled
        registrationNumberTextField.isEnabled = enabled
        validityTextField.isEnabled = enabled
        firstLicenseTextField.isEnabled = enabled
        fatherNameTextField.isEnabled = enabled
        motherNameTextField.isEnabled = enabled
    }
    
    func present(error: Error) {
        delegate?.present(error: error, driverLicenseFormView: self)
    }
    
    private func setupValidityTextField() {
        validityTextField.inputView = validityDatePicker
        validityTextField.inputAccessoryView = validityToolbar
    }
    
    private func setupFirstLicenseTextField() {
        firstLicenseTextField.inputView = firstLicenseDatePicker
        firstLicenseTextField.inputAccessoryView = firstLicenseToolbar
    }
    
    //MARK: - DriverLicenseFormViewDelegate
    
    func setValidityDateTextField(text: String) {
        validityTextField.text = text
    }
    
    func setFirstDocumentDateTextField(text: String) {
        firstLicenseTextField.text = text
    }
    
    //MARK: - Accessory Type Toolbar
    @objc func registrationNumberToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        registrationNumberTextField.resignFirstResponder()
    }
    
    @objc func validityToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        presenter.didSetValidityDate(to: validityDatePicker.date)
        validityTextField.resignFirstResponder()
    }
    
    @objc func firstLicenseToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        presenter.didSetFirstDocumentDate(to: firstLicenseDatePicker.date)
        firstLicenseTextField.resignFirstResponder()
    }
    
    // MARK: - Masked text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
   func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.changeColorBorder(isOn: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        
        if textField == fatherNameTextField {
            return count <= 40
        }
        
        if textField == motherNameTextField {
            return count <= 40
        }
        
        if textField == entityTextField {
            return count <= 10
        }
        return true
    }

}

