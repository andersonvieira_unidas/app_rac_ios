//
//  CreditCardWidgetView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

protocol CreditCardWidgetViewDelegate: class {
    func creditCardWidgetDidTapChangeCardButton(_ creditCardWidget: CreditCardWidgetView)
}

class CreditCardWidgetView: UIView, NibOwnerLoadable {

    @IBOutlet weak var creditCardView: CreditCardReservePaymentView!
    @IBOutlet weak var creditCardViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardPickerButton: UIButton!

    weak var delegate: CreditCardWidgetViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
        
    }
    
    func initialSetup() {
        loadNibContent()
        creditCardView.setHidden(!creditCardView.hasCard)
        creditCardViewHeightConstraint.priority = .defaultLow
        setNeedsLayout()
    }
    
    @IBAction func showCardListButtonTapped(_ sender: Any) {
        delegate?.creditCardWidgetDidTapChangeCardButton(self)
    }
    
    func set(value: Double) {
        creditCardView.setInstallments(at: value)
        
        //guard let amount = amount else { return }
        
        let numberOfInstallments = String(0 + 1)
        //let amountInstallments = amount / Double(0 + 1)
        
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: value)) else { return }
        
        print("\(String(numberOfInstallments))x \(amountString)")
        
        creditCardView?.paymentOptionsTextField.text = "  \(String(numberOfInstallments))x \(amountString)"
    }
    
    func set(card: ListCardDataView) {
        creditCardView.setCard(at: card)
        creditCardView.setHidden(!creditCardView.hasCard)
        creditCardViewHeightConstraint.priority = .init(900)
        cardPickerButton.setTitle(NSLocalizedString("Change card", comment: "Change card button title"), for: .normal)
        setNeedsLayout()
       
    }
    
}
