//
//  AddressFormView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable
import InputMask

protocol AddressFormDelegate: class {
    func present(error: Error, addressFormView: AddressFormView)
}

class AddressFormView: UIView, NibOwnerLoadable, MaskedTextFieldDelegateListener, AddressFormViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var neighborhoodTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var complementTextField: UITextField!
    @IBOutlet weak var residencialPhoneField: UITextField!
    @IBOutlet weak var postalCodeActivityIndicatorView: UIActivityIndicatorView!
    
    lazy var postalCodeMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[00000]-[000]")
    lazy var residencePhoneMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "([00]) [0000]-[0000]")
    weak var delegate: AddressFormDelegate?
    
    var formsAction: FormsAction = .create
    
    var presenter: AddressFormPresenterDelegate!
    
    var zipCode: String? {
        return postalCodeTextField.text
    }
    
    var neighborhood: String? {
        return neighborhoodTextField.text
    }
    
    var street: String? {
        return streetTextField.text
    }
    
    var number: String? {
        return numberTextField.text
    }
    
    var complement: String? {
        return complementTextField.text
    }
    
    var residencialPhone: String? {
        return residencialPhoneField.text
    }
    
    func zipCodeError(isOn: Bool) {
        postalCodeTextField.changeColorBorder(isOn: isOn)
    }
    
    func neighborhoodError(isOn: Bool) {
        neighborhoodTextField.changeColorBorder(isOn: isOn)
    }
    
    func cityError(isOn: Bool) {
        cityTextField.changeColorBorder(isOn: isOn)
    }
    
    func streetError(isOn: Bool) {
        streetTextField.changeColorBorder(isOn: isOn)
    }
    
    func numberError(isOn: Bool) {
        numberTextField.changeColorBorder(isOn: isOn)
    }
    
    func complementError(isOn: Bool) {
        complementTextField.changeColorBorder(isOn: isOn)
    }
    
    func phoneError(isOn: Bool) {
        residencialPhoneField.changeColorBorder(isOn: isOn)
    }
    
    func setupDelegates() {
        //postalCodeTextField.delegate = self
        stateTextField.delegate = self
        cityTextField.delegate = self
        neighborhoodTextField.delegate = self
        streetTextField.delegate = self
        numberTextField.delegate = self
        complementTextField.delegate = self
        //residencialPhoneField.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
        setupDelegates()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    func setTextFieldData(userResponseDataView: UserResponseDataView?, formsAction: FormsAction) {
        guard let userResponseDataView = userResponseDataView else { return }
 
        postalCodeTextField.text = userResponseDataView.userUnidas?.formattedZipcode
        
        if let zipcode = userResponseDataView.userUnidas?.formattedZipcode {
            presenter.didFillPostalCodeField(zipcode, formsAction: formsAction)
        }
        
        streetTextField.text = userResponseDataView.userUnidas?.street
        neighborhoodTextField.text = userResponseDataView.userUnidas?.neighborhood
        numberTextField.text = userResponseDataView.userUnidas?.number
        complementTextField.text = userResponseDataView.userUnidas?.complement
        residencialPhoneField.text = userResponseDataView.userUnidas?.formattedResidencePhoneNumber
        
    }
    
    func initialSetup() {
        presenter = AddressFormPresenter(zipcodeService: ZipcodeService())
        loadNibContent()
        setupPostalCodeTextField()
        setupResidenceTextField()
        presenter.delegate = self
    }
    
    func setupPostalCodeTextField() {
        postalCodeTextField.delegate = postalCodeMaskDelegate
        postalCodeMaskDelegate.listener = self
    }
    
    func setupResidenceTextField() {
        residencialPhoneField.delegate = residencePhoneMaskDelegate
        residencePhoneMaskDelegate.listener = self
    }

    // MARK: - TextFields Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.changeColorBorder(isOn: false)
    }
    
    // MARK: - Address form view delegate
    
    func fieldsEnabled(_ enabled: Bool) {
        postalCodeTextField.isEnabled = enabled
        stateTextField.isEnabled = enabled
        cityTextField.isEnabled = enabled
        neighborhoodTextField.isEnabled = enabled
        streetTextField.isEnabled = enabled
        numberTextField.isEnabled = enabled
        complementTextField.isEnabled = enabled
        residencialPhoneField.isEnabled = enabled
    }
    
    func startLoadingPostalCode() {
        fieldsEnabled(false)
        postalCodeActivityIndicatorView.startAnimating()
    }
    
    func endLoadingPostalCode() {
        fieldsEnabled(true)
        
        streetTextField.changeColorBorder(isOn: false)
        cityTextField.changeColorBorder(isOn: false)
        neighborhoodTextField.changeColorBorder(isOn: false)

        postalCodeActivityIndicatorView.stopAnimating()
    }
    
    func setAddress(_ address: ZipcodeDataView, formsAction: FormsAction) {
        stateTextField.text = address.state
        stateTextField.isEnabled = false
        stateTextField.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
        cityTextField.text = address.city
        cityTextField.isEnabled = false
        cityTextField.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
        
        if formsAction == .create {
            streetTextField.text = address.address
            neighborhoodTextField.text = address.neighborhood
        }
    }
    
    func present(error: Error) {
        delegate?.present(error: error, addressFormView: self)
    }
    
    func clearAddress() {
        stateTextField.text = nil
        cityTextField.text = nil
        streetTextField.text = nil
        neighborhoodTextField.text = nil
    }
    
    // MARK: - Masked text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == numberTextField {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        if textField == postalCodeTextField, complete {
            presenter.didFillPostalCodeField(value, formsAction: formsAction)
        }
    }
}
