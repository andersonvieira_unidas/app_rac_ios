//
//  FormsAction.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum FormsAction {
    case create, update, preUpdate
    
    var titleIsHidden: Bool {
        switch self {
        case .create, .preUpdate:
            return false
        case .update:
            return true
        }
    }
    
    var buttonTitle: String {
        switch self {
        case .create, .preUpdate:
            return NSLocalizedString("Next Button", comment: "")
        case .update:
            return NSLocalizedString("Save Button", comment: "")
        }
    }
    
    var finishLaterIsHidden: Bool {
        switch self {
        case .create, .preUpdate:
            return false
        case .update:
            return true
        }
    }
    
    var setHidesBackButton: Bool {
        switch self {
        case .create, .preUpdate:
            return true
        case .update:
            return false
        }
    }
}
