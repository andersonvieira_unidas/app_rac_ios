//
//  CreditCardFormView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 10/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable
import InputMask


protocol CreditCardFormViewCameraDelegate: class {
    func showCamera()
}

class CreditCardFormView: UIView, NibOwnerLoadable, CreditCardFormViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MaskedTextFieldDelegateListener, UITextFieldDelegate {
  
    @IBOutlet weak var cardBrandImageView: UIImageView!
    @IBOutlet weak var cardOCRImageView: UIButton!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var expirationDateTextField: UITextField!
    @IBOutlet weak var cardHoldersNameTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backViewHeightConstraint: NSLayoutConstraint!
    
    var presenter: CreditCardFormPresenterDelegate!
    weak var delegate: CreditCardFormViewCameraDelegate?
    var creditCardOcr: Bool = false
    
    lazy var cardNumberMaskDelegate = MaskedTextFieldDelegate(primaryFormat: "[0000] [0000] [0000] [0000]")
    lazy var cardNumberAmexMaskDelegate = MaskedTextFieldDelegate(primaryFormat: "[0000] [000000] [00000]")

    lazy var expirationDatePicker = { () -> UIPickerView in
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        return pickerView
    }()
    
    lazy var expirationDateToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(expirationDateToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var cardNumberToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(cardNumberToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
        self.setupDelegates()
        self.setupLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    func setup(titleIsHidden: Bool) {
    }
    
    func updateValues(cardNumber: String, dateValidThru: String) {
        let cardNumberFormatted = cardNumber.replacingOccurrences(of: "-", with: "")
        cardNumberMaskDelegate.put(text: cardNumberFormatted, into: cardNumberTextField)
        expirationDateTextField.text = dateValidThru
        presenter.cardNumberRegexUpdate(for: cardNumberFormatted)
        showBrandHiddenOCR()
        cardHoldersNameTextField.becomeFirstResponder()
        
        let dateSplit = dateValidThru.split(separator: "/")
 
        if let monthValidThru = dateSplit.first, let yearValidThru = dateSplit.last, let month = Int(monthValidThru) {
            let year = String(yearValidThru)
            presenter.set(year: year)
            presenter.set(month: month)
        }
    }
    
    private func initialSetup() {
        loadNibContent()
        presenter = CreditCardFormPresenter()
        presenter.delegate = self
        setupBirthdayTextField()
        setupCardNumberTextField()
        setupCardHoldersNameTextField()
        
        //check toggle
        FirebaseRealtimeDatabase.toggleValue(withKey: .creditCardOcr) { (toggle) in
            if !toggle{
                self.creditCardOcr = toggle
                self.showBrandHiddenOCR()
            }
        }
    }
    
    private func setupBirthdayTextField() {
        expirationDateTextField.inputView = expirationDatePicker
        expirationDateTextField.inputAccessoryView = expirationDateToolbar
    }
    
    private func setupCardHoldersNameTextField() {
        cardHoldersNameTextField.delegate = self
    }
    
    func setupCardNumberTextField() {
        cardNumberTextField.delegate = cardNumberMaskDelegate
        cardNumberMaskDelegate.listener = self
        cardNumberTextField.inputAccessoryView = cardNumberToolbar
    }
    
    @objc func expirationDateToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        presenter.didSetExpirationDate(at: expirationDatePicker.selectedRow(inComponent: 0), at: expirationDatePicker.selectedRow(inComponent: 1))
        
        expirationDateTextField.resignFirstResponder()
    }
    
    @objc func cardNumberToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        cardNumberTextField.resignFirstResponder()
    }
    
    // MARK: - Picker View
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return presenter.quantityMonths
        default:
            return presenter.quantityYears
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return presenter.month(at: row)
        default:
            return presenter.year(at: row)
        }
    }
    
    // MARK: - CreditCardFormViewDelegate
    
    func setupDelegates() {
        expirationDateTextField.delegate = self
        cardHoldersNameTextField.delegate = self
    }
    
    func setupLayout() {
        cardBrandImageView.layer.cornerRadius = 5
        backView.layer.cornerRadius = 15
        cardNumberTextField.layer.cornerRadius = 5
        cardHoldersNameTextField.layer.cornerRadius = 5
        expirationDateTextField.layer.cornerRadius = 5
        backViewHeightConstraint.constant = 190
        cardNumberTextField.setLeftPaddingPoints(10)
        cardHoldersNameTextField.setLeftPaddingPoints(10)
    }
    
    var cardNumber: String? {
        return cardNumberTextField.text
    }
    
    var holderName: String? {
        return cardHoldersNameTextField.text
    }
    
    func showExpirateDate(value: String) {
        expirationDateTextField.text = value
    }
    
    func updateBrandImage(image: UIImage) {
        cardBrandImageView.image = image
    }
    
    func changeDefaultMask() {
        cardNumberTextField.delegate = cardNumberMaskDelegate
        cardNumberMaskDelegate.listener = self
    }
    
    func changeAmexMask() {
        cardNumberTextField.delegate = cardNumberAmexMaskDelegate
        cardNumberAmexMaskDelegate.listener = self
    }

    @IBAction func tapCamera(_ sender: Any) {
        delegate?.showCamera()
    }
    
    // MARK: - MaskedTextFieldDelegateListener
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        //check toggle
        FirebaseRealtimeDatabase.toggleValue(withKey: .creditCardOcr) { (toggle) in
            if !toggle{
                self.creditCardOcr = toggle
                self.showBrandHiddenOCR()
            } else {
                if value.isEmpty {
                    self.showOCRHiddenBrand()
               } else {
                    self.showBrandHiddenOCR()
               }
            }
        }
        presenter.cardNumberRegexUpdate(for: value)
    }
    
    // MARK: - TextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    private func showOCRHiddenBrand() {
        cardOCRImageView.isHidden = false
        cardBrandImageView.isHidden = true
    }
    
    private func showBrandHiddenOCR() {
        cardOCRImageView.isHidden = true
        cardBrandImageView.isHidden = false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
        
        if textField == expirationDateTextField {
            return false
        }
 
        if textField == cardHoldersNameTextField {
            let texto = (text as NSString).replacingCharacters(in: range, with: string)
            let Regex = "[a-z A-Z ]+"
            let predicate = NSPredicate.init(format: "SELF MATCHES %@", Regex)
            if predicate.evaluate(with: texto) || string == "" {
                return true
            } else {
                return false
            }
        }
        
        if textField.tag == 2 {
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }

        return true
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
