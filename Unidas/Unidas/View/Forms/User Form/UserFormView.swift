//
//  UserFormView.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 08/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable
import InputMask

protocol UserFormDelegate: class {
    func present(error: Error, userFormView: UserFormView)
}

class UserFormView: UIView, NibOwnerLoadable, UserFormViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var cpfTextField: UITextField!
    @IBOutlet var documentNumberTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var birthdayStackView: UIStackView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var motherNameStackView: UIStackView!
    @IBOutlet weak var motherNameTextField: UITextField!
    @IBOutlet weak var titleView: UIView!
    
    weak var delegate: UserFormDelegate?

    private var documentNumberMaskDelegate: NotifyingMaskedTextFieldDelegate!
    private var phoneMaskDelegate: NotifyingMaskedTextFieldDelegate!
   
    lazy var birthdayDatePicker = { () -> UIDatePicker in
        var datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        return datePicker
    }()
    
    lazy var birthdayToolbar = { () -> UIToolbar in
        let toolbar = UIToolbar()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(birthdayToolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    weak var editingListener: NotifyingMaskedTextFieldDelegateListener?
    
    var showsBirthdayTextField: Bool = true {
        didSet {
            birthdayStackView.setHidden(!showsBirthdayTextField)
        }
    }
    var showsMotherNameTextField: Bool = true {
        didSet {
            motherNameStackView.setHidden(!showsMotherNameTextField)
        }
    }
    
    var isHiddenBirthdayTextField: Bool {
        return birthdayStackView.isHidden
    }
    
    var documentNumber: String? {
        if let documentNumber = documentNumberTextField.text {
            return documentNumber.removeDocumentNumberCharacters
        }
        return ""
    }
    
    var fullName: String? {
        return fullNameTextField.text
    }
    
    var email: String? {
        return emailTextField.text
    }
    
    var mobilePhone: String? {
        if let mobilePhone = phoneTextField.text {
            return mobilePhone.removePhoneCharacters
        }
        return ""
    }
    
    var motherName: String? {
        return motherNameTextField.text
    }
    
    var birthday: Date? {
        guard let text = birthdayTextField.text, !text.isEmpty else { return nil }
        return birthdayDatePicker.date
    }
    
    var userFormDataView: UserFormDataView? {
        return presenter.userFormDataView
    }
    
    var presenter: UserFormPresenterDelegate!
    
    var oldMobilePhone: String?
    
    func setupDelegates() {
        //cpfTextField.delegate = documentNumberMaskDelegate
        fullNameTextField.delegate = self
        emailTextField.delegate = self
        motherNameTextField.delegate = self
        birthdayTextField.delegate = self
        //phoneTextField.delegate = phoneMaskDelegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialSetup()
        self.setupDelegates()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    // MARK: - UITextfield delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.changeColorBorder(isOn: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else { return true }
    
        if textField == cpfTextField {
            defer {
                self.editingListener?.onEditingChanged(inTextField: textField)
            }            
            return true
        }
        
        if textField == phoneTextField {
            return true
        }
        
        if textField == birthdayTextField {
            return false
        }
        
        if textField == emailTextField {
            return true
        }
        
        let texto = (text as NSString).replacingCharacters(in: range, with: string)
        let Regex = "[a-z A-Z áãàâäåæªéêèęėēëīįïìíºōøœöòôõóūûùüúçaÁÃÀÂÄÅÆªÉÊÈĘĖĒËĪĮÏÌÎÍºŌØŒÖÒÔÕÓŪÛÙÜÚÇ]+"
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", Regex)
        if predicate.evaluate(with: texto) || string == "" {
            return true
        } else {
            return false
        }
    }
    
    func setTextFieldData(userResponseDataView: UserResponseDataView?, formsAction: FormsAction) {
        guard let userResponseDataView = userResponseDataView else { return }
        documentNumberTextField.text = userResponseDataView.account.formattedDocumentNumber
        fullNameTextField.text = userResponseDataView.userUnidas?.name
        emailTextField.text = userResponseDataView.userUnidas?.email
        phoneTextField.text = userResponseDataView.userUnidas?.formattedMobilePhoneNumber
        motherNameTextField.text = userResponseDataView.userUnidas?.motherName
        
        if let birthDate = userResponseDataView.userUnidas?.birthDate {
             birthdayDatePicker.setDate(birthDate, animated: true)
            presenter.didSetBirthday(to: birthDate)
        }
        
        documentNumberTextField.isEnabled = false
    }
    
    func hiddenTitle(isHidden: Bool) {
        titleView.isHidden = isHidden
    }
    
    func setOldMobilePhone(oldMobilePhone mobilePhone: String?) {
        guard let mobilePhone = mobilePhone else { return }
        oldMobilePhone = mobilePhone
    }
    
    func documentNumberError(isOn: Bool) {
        documentNumberTextField.changeColorBorder(isOn: isOn)
    }
    
    func fullNameError(isOn: Bool) {
        fullNameTextField.changeColorBorder(isOn: isOn)
    }
    
    func birthdayError(isOn: Bool) {
        birthdayTextField.changeColorBorder(isOn: isOn)
    }
    
    func emailError(isOn: Bool) {
        emailTextField.changeColorBorder(isOn: isOn)
    }
    
    func phoneError(isOn: Bool) {
        phoneTextField.changeColorBorder(isOn: isOn)
    }
    
    func motherNameError(isOn: Bool) {
        motherNameTextField.changeColorBorder(isOn: isOn)
    }
    
    private func initialSetup() {
        loadNibContent()
        presenter = UserFormPresenter()
        presenter.delegate = self
        setupBirthdayTextField()
        setupMaskTextFields()
        setupTextFieldsDelegates()
    }
    
    private func setupBirthdayTextField() {
        birthdayTextField.inputView = birthdayDatePicker
        birthdayTextField.inputAccessoryView = birthdayToolbar
    }
    
    private func setupTextFieldsDelegates() {
        fullNameTextField.delegate = self
        birthdayTextField.delegate = self
        emailTextField.delegate = self
        motherNameTextField.delegate = self
    }
    
    private func setupMaskTextFields() {
        documentNumberMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "[000].[000].[000]-[00]")
        documentNumberTextField.delegate = documentNumberMaskDelegate
        
        phoneMaskDelegate = NotifyingMaskedTextFieldDelegate(primaryFormat: "([00]) [00000]-[0000]")
        phoneTextField.delegate = phoneMaskDelegate
    }
    
    @objc func birthdayToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        presenter.didSetBirthday(to: birthdayDatePicker.date)
        birthdayTextField.resignFirstResponder()
    }
    
    override func prepareForInterfaceBuilder() {
        loadNibContent()
    }
    
    // MARK: - User form view delegate
    func setBirthdayDateTextField(text: String) {
        birthdayTextField.text = text
    }
    
    func isUserValid() -> Bool {
        return presenter.isUserValid
    }
    
    func present(error: Error) {
        delegate?.present(error: error, userFormView: self)
    }
}
