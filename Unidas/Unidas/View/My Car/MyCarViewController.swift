//
//  MyCarViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/02/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import PassKit
import MapKit

class MyCarViewController: UIViewController, MyCarViewDelegate, UITextViewDelegate, PKAddPassesViewControllerDelegate, SimpleAlertDelegate {

    @IBOutlet weak var openContractView: UIView!
    
    @IBOutlet weak var groupHeaderStackView: UIStackView!
    
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var groupVehiclesLabel: UILabel!
    @IBOutlet weak var groupImageView: UIImageView!
    
    @IBOutlet weak var qrCodeStackView: UIStackView!
    @IBOutlet weak var qrCodeView: QRCodeView!
    
    @IBOutlet weak var pickupDateLabel: UILabel!
    @IBOutlet weak var pickupStoreTextView: UITextView!
    
    
    @IBOutlet weak var appleWalletButton: UIButton!
    
    var pkPass : PKPass?
    
    var activityIndicator: UIActivityIndicatorView!
    var contractNumber: Int?
    
    lazy var presenter = { () -> MyCarPresenterDelegate in
        let presenter = MyCarPresenter(vehicleDataStore: VehicleDataStoreUserDefaults(),
                                       contractService: ContractService(),
                                       garageService: UnidasGarageService(),
                                       companyTermsService: CompanyTermsService(),
                                       passbookService: PassbookService(),
                                       firebaseService: FirebaseAnalyticsService())
        presenter.delegate = self
        return presenter
    }()
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        if let contract = contractNumber {
            presenter.viewDidLoad(contractNumber: contract)
        }
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupWalletButton()
    }
    
    //  MARK: Private Functions
    
    private func setupWalletButton() {
        FirebaseRealtimeDatabase.toggleValue(withKey: .appleWallet) { (value) in
            if !value {
                self.appleWalletButton.isEnabled = false
            }
        }
    }
    
    private func setupView(){
        setHeaderUnidas()
        
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
        
        let activityIndicatorItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activityIndicatorItem
    }
    
    private func setupActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.hidesWhenStopped = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: activityIndicator)
    }
    
    private func makeDate(date: String, hour: String, dataAndHour: String) -> NSMutableAttributedString {
        let localizedString = NSLocalizedString("Date %@ at %@", comment: "")
        let searchString = String(format: localizedString, date, hour)
        let rangeDate = (dataAndHour as NSString).range(of: searchString)
        let dateMutable = NSMutableAttributedString(string: dataAndHour, attributes: [
            NSAttributedString.Key.font : UIFont.regular(ofSize: 12),
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)]
        )
        dateMutable.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 14)], range: rangeDate)
        
        return dateMutable
    }
    
    private func makeStore(storeAndCity: String?) -> NSMutableAttributedString? {
        guard let pickUpStoreAndState = storeAndCity else { return nil }
        let localizedString = NSLocalizedString("Store %@", comment: "")
        let store = String(format: localizedString, pickUpStoreAndState)
        let pickUpStoreAndStateFormatted = NSMutableAttributedString(string: store, attributes: [NSAttributedString.Key.font : UIFont.bold(ofSize: 12)])
        return pickUpStoreAndStateFormatted
    }
    
    // MARK: - Interface Builder actions
    @IBAction func tapAddAppleWallet(_ sender: Any) {
        presenter.generateAppleWallet()
    }
    
    @IBAction func contractDetailsButtonTapped(_ sender: Any) {
        presenter.contractDetailsButtonTapped()
    }
    
    @objc func touchCancelItem(){
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - MyCarViewDelegate
    
    func startLoading() {
        openContractView.isHidden = true
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        openContractView.isHidden = false
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func alertErrorWallet() {
        alertError(errorMessage: NSLocalizedString("Apple Wallet Error Message", comment: ""))
    }
    
    func processWallet(dataWallet: Data?) {
        if let passValue = dataWallet{
            do {
                let pkPass = try PKPass(data: passValue)
                self.pkPass = pkPass
                if pkPass.passTypeIdentifier == "pass.br.com.unidas.apprac.ios" {
                    let pkLibrary = PKPassLibrary()
                    if pkLibrary.containsPass(pkPass) {
                        if let url = pkPass.passURL {
                            UIApplication.shared.open(url, options:[:] , completionHandler: nil)
                        }
                    }else{
                        let vc = PKAddPassesViewController(pass: pkPass)!
                        vc.delegate = self
                        self.present(vc, animated: true, completion: nil)
                    }
                } else {
                    alertErrorWallet()
                }
            } catch {
                alertErrorWallet()
            }
        }
    }
    
    func startLoadingWallet() {
        LoadingScreen.setLoadingScreen(view: self.view)
    }
    
    func endLoadingWallet() {
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func showContractDetails() {
        performSegue(withIdentifier: "showContractDetails", sender: nil)
    }
    
    func updateView() {
        groupHeaderStackView.setHidden(!presenter.showsGroupHeader)
        qrCodeStackView.setHidden(!presenter.showsQRCodeView)
    }
    
    func updateViewWithError(error: Error) {
        
        groupHeaderStackView.setHidden(!presenter.showsGroupHeader)
        qrCodeStackView.setHidden(!presenter.showsQRCodeView)
        
        let title = NSLocalizedString("Contract", comment: "")
        let simpleAlert = SimpleAlert(title: title, subTitle: error.localizedDescription, image: #imageLiteral(resourceName: "icon-signature-menu"))
        simpleAlert.show(animated: true)
        
        simpleAlert.delegate = self
    }
    
    func setup(for contract: ContractDataView) {
        groupDescriptionLabel.text = contract.groupDescription?.lowerCapitalize()
        groupNameLabel.text = contract.groupCode
        groupVehiclesLabel.text = contract.groupVehicles
        groupImageView.setImage(withURL: contract.pictureURL)
        qrCodeView.message = contract.qrCodeInformation
        
        if let pickUpStoreAndState = contract.pickUpStoreAndState,
            let pickupStoreMuttableString = makeStore(storeAndCity: pickUpStoreAndState){
            
            let range = pickupStoreMuttableString.string.nsRange
            pickupStoreMuttableString.addAttribute(.link, value: "pickup", range: range)
            pickupStoreTextView.attributedText = pickupStoreMuttableString
            pickupStoreTextView.linkTextAttributes = NSAttributedString.linkAttributed
            pickupStoreTextView.delegate = self
        }
        
        if let pickUpDate = contract.pickUpDateOnly, let pickUpHour = contract.pickUpHour {
            let pickUpLocalizedString = NSLocalizedString("Pickup Date %@ at %@", comment: "")
            let pickUpFormatted = String(format: pickUpLocalizedString, pickUpDate, pickUpHour)
            
            let pickupMuttableString = makeDate(date: pickUpDate, hour: pickUpHour, dataAndHour: pickUpFormatted)
            pickupDateLabel.attributedText = pickupMuttableString
        }
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
            
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
    
    //MARK: - SimpleAlterDelegate
    func close() {
         dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TextView delegate
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url.absoluteString.contains("pickup") {
            presenter.routePickUpReserve()
        }
        
        return false
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContractDetails" {
            let destination = segue.destination as? ContractDetailsTableViewController
            destination?.contractDataView = presenter.contractDataView
        }
    }
    
}

