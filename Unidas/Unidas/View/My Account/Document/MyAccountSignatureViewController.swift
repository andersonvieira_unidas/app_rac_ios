//
//  MyAccountSignatureViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 19/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class MyAccountSignatureViewController: UIViewController, MyAccountSignatureViewDelegate, DocumentRecomendationsDelegate, SignatureUpdateRecomendationsProtocol {
    
    @IBOutlet weak var emergencyPhoneStackView: UIStackView!
    @IBOutlet weak var descriptionSignature: UILabel!
    @IBOutlet weak var updateSignature: UnidasButton!
    @IBOutlet weak var previewSignature: UnidasButton!
    @IBOutlet weak var documentStackView: UIStackView!
    @IBOutlet weak var centerActivityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var borderBackView: UIView!
    @IBOutlet weak var signatureImageView: UIImageView!
    
    private var activityIndicatorView: UIActivityIndicatorView!
    private var presenter: MyAccountSignaturePresenterDelegate!
    let emergencyPhoneView = EmergencyPhoneView.loadFromNib()
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.checkDocument()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MyAccountSignaturePresenter(accountImageService: AccountImageService(),
                                                checkDocumentService: CheckDocumentService(),
                                                companyTermsService: CompanyTermsService())
        presenter.delegate = self
        setupView()
        setupActivityIndicator()
        presenter.fetchSacPhoneNumber()
        self.navigationController?.change()
        setHeaderUnidas()
        configureBackButton()
    }
    
    func setupView() {
        //self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        borderBackView.layer.borderWidth = 1
        borderBackView.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        borderBackView.layer.cornerRadius = 5
        signatureImageView.image = #imageLiteral(resourceName: "icon-signature-menu").withRenderingMode(.alwaysTemplate)
        signatureImageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    //MARK: - Private functions
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    
    @IBAction func tapPreviewSignature(_ sender: EnhancedButton) {
        presenter.loadImageGalery(imageType: .signature)
    }
    
    @IBAction func tapUpdateSignature(_ sender: EnhancedButton) {
        performSegue(withIdentifier: "showSignature", sender: nil)
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - MyAccountDocumentViewDelegate
    
    func setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView) {
        let description = companyTermsDataView.description
        emergencyPhoneView.setupView()
        emergencyPhoneView.setNumber(phone: description)
        emergencyPhoneStackView.addArrangedSubview(emergencyPhoneView)
    }
    
    func centerStartLoading() {
        documentStackView.isHidden = true
        centerActivityIndicatorView.startAnimating()
    }
    
    func centerEndLoading() {
        documentStackView.isHidden = false
        centerActivityIndicatorView.stopAnimating()
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func didOpenGalery(data: Data, imageType: ImageType) {
        guard let image = UIImage(data: data) else { return }
        performSegue(withIdentifier: "showImageGalery", sender: image)
    }
    
    func hidePreviewSignature() {
        previewSignature.isHidden = true
    }
    
    func hideUpdateSignature() {
        updateSignature.isHidden = true
    }
    
    func showPreviewSignature() {
        previewSignature.isHidden = false
        //previewSignature.setTitle(NSLocalizedString("Visualize", comment: "Visualize button title"), for: .normal)
    }
    
    func showUpdateSignature() {
        updateSignature.isHidden = false
        //updateSignature.setTitle(NSLocalizedString("Update", comment: "Update button title"), for: .normal)
    }
    
    func showFinishSignature() {
        descriptionSignature.text = nil
        updateSignature.setTitle(NSLocalizedString("Finish account", comment: "Finish account button title"), for: .normal)
        updateSignature.isHidden = false
        previewSignature.isHidden = true
    }
    
    func showSignatureMessage(checkDocumentDataView: CheckDocumentDataView) {
        descriptionSignature.text = checkDocumentDataView.lastUpdateMessage
    }
    
    // MARK: - DocumentRecomendationsDelegate
    
    func finishUpdateDocumentDriver() {
        presenter.checkDocument()
    }
    
    // MARK: - SignatureUpdateRecomendationsProtocol
    
    func finishUpdateSignature() {
        presenter.checkDocument()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUpdateDocument" {
            let destination = segue.destination as? DocumentRecomendationsViewController
            destination?.formsAction = .update
            destination?.messageFlagForCNH = true
            destination?.delegate = self
            
        }
        else if segue.identifier == "showSignature" {
            let destination = segue.destination as? SignatureUpdateRecomendationsViewController
            destination?.delegate = self
        }
        else if segue.identifier == "showImageGalery" {
            let navigation = segue.destination as? UINavigationController
            let imageGalleryController = navigation?.viewControllers.first as? ImageGalleryPageViewController
            imageGalleryController?.images = [sender as! UIImage]
            imageGalleryController?.currentIndex = 0
            imageGalleryController?.showsToolbar = false
        }
    }
}
