//
//  SignatureUpdateRecomendationsViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol SignatureUpdateRecomendationsProtocol: class {
    func finishUpdateSignature()
}

class SignatureUpdateRecomendationsViewController: UIViewController, CustomSignatureDelegate, SignatureUpdateRecomendationsViewDelegate, SignatureSuccessViewControllerDelegate {
    
    private var presenter: SignatureUpdateRecomendationsPresenterDelegate!
    weak var delegate: SignatureUpdateRecomendationsProtocol?
    
    @IBOutlet weak var signature2ImageView: UIImageView!
    @IBOutlet weak var signatureImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        configureButtonBack()
        presenter = SignatureUpdateRecomendationsPresenter(accountPhotoUnidasService: AccountPhotoUnidasService())
        presenter.delegate = self
    }

    @IBAction func touchShowSignature(_ sender: UIButton) {
        performSegue(withIdentifier: "showSignature", sender: nil)
    }
        
    func setupView() {
        signatureImageView.image = #imageLiteral(resourceName: "icon-signature-menu").withRenderingMode(.alwaysTemplate)
        signatureImageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        signature2ImageView.image = #imageLiteral(resourceName: "icon-assinatura-update").withRenderingMode(.alwaysTemplate)
        signature2ImageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        backView.layer.cornerRadius = 4
        backView.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        backView.layer.borderWidth = 1
    }
    
    func finishShowSignatureSuccessView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func configureButtonBack(){
         let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
         let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
         navigationItem.leftBarButtonItem = backButton
     }
     
     @objc func touchCancelItem(){
         self.navigationController?.popViewController(animated: true)
     }

    
    // MARK: - CustomSignatureDelegate
    
    func acceptSignature(data: Data, viewController: CustomSignatureViewController) {
        presenter.addSignature(image: data, viewController: viewController)
    }
    
    // MARK: - SignatureUpdateRecomendationsViewDelegate
    
    func finishUploadSignature(viewController: CustomSignatureViewController) {
        viewController.dismiss(animated: true) {
            self.performSegue(withIdentifier: "showSignatureSuccess", sender: nil)
            //self.updateAlertMessage()
        }
    }
    
    func customSignatureStartLoading(viewController: CustomSignatureViewController) {
        viewController.startLoading()
    }
    
    func customSignatureEndLoading(viewController: CustomSignatureViewController) {
        viewController.endLoading()
    }
    
    func customSignaturePresent(error: Error, viewController: CustomSignatureViewController) {
        viewController.present(error: error)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSignature" {
            let destination = segue.destination as? CustomSignatureViewController
            
            destination?.delegate = self
            destination?.showsCloseButton = true
            destination?.showsLaterButton = false
        }
        if segue.identifier == "showSignatureSuccess" {
            let destination = segue.destination as? SignatureSucessViewController
            destination?.delegate = self
        }
    }
}
