//
//  MyAccountDocumentViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class MyAccountDocumentViewController: UIViewController, MyAccountDocumentViewDelegate, DocumentRecomendationsDelegate, SignatureUpdateRecomendationsProtocol {
    @IBOutlet weak var emergencyPhoneStackView: UIStackView!
    @IBOutlet weak var descriptionDriverDocument: UILabel!
    @IBOutlet weak var updateDriverDocument: UnidasButton!
    @IBOutlet weak var previewDriverDocument: UnidasButton!
    @IBOutlet weak var documentStackView: UIStackView!
    @IBOutlet weak var centerActivityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var driverDocumentPhotoInstructions: UILabel!
    
    @IBOutlet weak var borderBackView: UIView!
    @IBOutlet weak var documentImageView: UIImageView!
    
    private var activityIndicatorView: UIActivityIndicatorView!
    private var presenter: MyAccountDocumentPresenterDelegate!
    let emergencyPhoneView = EmergencyPhoneView.loadFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MyAccountDocumentPresenter(accountImageService: AccountImageService(),
                                               checkDocumentService: CheckDocumentService(),
                                               companyTermsService: CompanyTermsService())
        presenter.delegate = self
        setupView()
        setupActivityIndicator()
        presenter.fetchSacPhoneNumber()
        presenter.checkDocument()
        self.navigationController?.change()
        setHeaderUnidas()
        configureBackButton()
    }
    
    //MARK: - Private Functions
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func setupView() {
       // self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        borderBackView.layer.borderWidth = 1
        borderBackView.layer.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        borderBackView.layer.cornerRadius = 5
        documentImageView.image =  #imageLiteral(resourceName: "icon-document-menu").withRenderingMode(.alwaysTemplate)
        documentImageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    @IBAction func tapPreviewDriverDocument(_ sender: EnhancedButton) {
        presenter.loadImageGalery(imageType: .document)
    }
    
    @IBAction func tapUpdateDriverDocument(_ sender: EnhancedButton) {
        performSegue(withIdentifier: "showUpdateDocument", sender: nil)
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - MyAccountDocumentViewDelegate
    
    func setupEmergencyPhoneView(companyTermsDataView: CompanyTermsDataView) {
        let description = companyTermsDataView.description
        emergencyPhoneView.setupView()
        emergencyPhoneView.setNumber(phone: description)
        emergencyPhoneStackView.addArrangedSubview(emergencyPhoneView)
    }
    
    func centerStartLoading() {
        documentStackView.isHidden = true
        centerActivityIndicatorView.startAnimating()
    }
    
    func centerEndLoading() {
        documentStackView.isHidden = false
        centerActivityIndicatorView.stopAnimating()
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func didOpenGalery(data: Data, imageType: ImageType) {
        guard let image = UIImage(data: data) else { return }
        performSegue(withIdentifier: "showImageGalery", sender: image)
    }
    
    func hidePreviewDocumentCnh() {
        previewDriverDocument.isHidden = true
    }
    
    func hideUpdateDocumentCnh() {
        updateDriverDocument.isHidden = true
    }
    
    func hideDriverLicensePhotoInstruction() {
        driverDocumentPhotoInstructions.isHidden = true
    }
    
    func showPreviewDocumentCnh() {
        previewDriverDocument.isHidden = false
        previewDriverDocument.setTitle(NSLocalizedString("Visualize", comment: "Visualize button title"), for: .normal)
    }
    
    func showUpdateDocumentCnh() {
        updateDriverDocument.isHidden = false
        updateDriverDocument.setTitle(NSLocalizedString("Update", comment: "Update button title"), for: .normal)
    }
    
    func showFinishDocument() {
        descriptionDriverDocument.text = nil
        updateDriverDocument.setTitle(NSLocalizedString("Finish account", comment: "Finish account button title"), for: .normal)
        updateDriverDocument.isHidden = false
        previewDriverDocument.isHidden = true
    }
    
    func showDocumentDriverMessage(checkDocumentDataView: CheckDocumentDataView) {
        descriptionDriverDocument.text = checkDocumentDataView.updateMessage
    }
    
    // MARK: - DocumentRecomendationsDelegate
    
    func finishUpdateDocumentDriver() {
        presenter.checkDocument()
    }
    
    // MARK: - SignatureUpdateRecomendationsProtocol
    
    func finishUpdateSignature() {
        presenter.checkDocument()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUpdateDocument" {
            let destination = segue.destination as? DocumentRecomendationsViewController
            destination?.formsAction = .update
            destination?.messageFlagForCNH = true
            destination?.delegate = self
            
        }
        else if segue.identifier == "showSignature" {
            let destination = segue.destination as? SignatureUpdateRecomendationsViewController
            destination?.delegate = self
        }
        else if segue.identifier == "showImageGalery" {
            let navigation = segue.destination as? UINavigationController
            let imageGalleryController = navigation?.viewControllers.first as? ImageGalleryPageViewController
            imageGalleryController?.images = [sender as! UIImage]
            imageGalleryController?.currentIndex = 0
            imageGalleryController?.showsToolbar = false
        }
    }
}
