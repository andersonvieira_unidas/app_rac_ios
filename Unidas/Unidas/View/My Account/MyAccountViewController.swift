//
//  MyAccountViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
protocol MyAccountViewControllerDelegate{
    func addBadge(index: Int)
    func removeBadge(index: Int)
}

class MyAccountViewController: UIViewController, MyAccountViewDelegate, MyAccountHeaderDelegate, PINViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, HeaderLoginDelegate, MyAccountViewControllerDelegate {
    
    private var presenter: MyAccountPresenterDelegate!
    private var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var profileHeaderView: UIView?
    @IBOutlet weak var tableView: UITableView!
    
    private var headerLoginOnView = HeaderLoginOnView.loadFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        presenter = MyAccountPresenter(accountImageService: AccountImageService(),
                                       unidasLoginService: UnidasLoginService(),
                                       accountsService: AccountsService(),
                                       authUnidasService: AuthUnidasService(),
                                       recoveryService: RecoveryService(),
                                       userCheckService: UserCheckService())
        presenter.delegate = self
        presenter.showMyAccountHeaderView()
        
        setupActivityIndicator()
        setupTableView()
        setupNavigationController()
        setHeaderUnidas()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.loadUserAccount()
        headerLoginOnView.reloadPendencies()
    }
    
    private func setupTableView(){
        guard let tableView = tableView else { return }
        tableView.register(cellType: MyAccountItemTableViewCell.self)
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func setupNavigationController() {
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showMyAccountHeaderView(userResponseDataView: UserResponseDataView) {
        headerLoginOnView.delegate = self
        headerLoginOnView.delegateMyAccount = self
        
        headerLoginOnView.setup(userResponseDataView: userResponseDataView, showMaskPhotoButton: true)
        
        profileHeaderView?.addSubview(headerLoginOnView)
    }
    
    // MARK: - MyAccountHeaderViewDelegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func performSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: nil)
    }
    
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController) {
        pinViewController.showNewPinController(title: title, isHiddenForgotPin: isHiddenForgotPin, isHiddenCancelButton: isHiddenCancelButton, userResponseDataView: userResponseDataView)
    }
    
    func didFinishUpdatePin(pinViewController: PINViewController) {
        pinViewController.successPin(title: NSLocalizedString("Password updated successfully title", comment: ""),
                                     message: NSLocalizedString("Password updated successfully", comment: ""))
    }
    
    func didFailConfirmPin(pinViewController: PINViewController, error: Error, callbackDidFailFinishPin: Bool) {
        pinViewController.present(error: error, callbackDidFailFinishPin: callbackDidFailFinishPin)
    }
    
    func didPinEndLoading(pinViewController: PINViewController) {
        pinViewController.endLoading()
    }
    
    func didShowChooseProfileImage(isPreview: Bool, isUpdate: Bool, userResponseDataView: UserResponseDataView) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Photo Options", comment: "Photo Options"), message: nil, preferredStyle: .actionSheet)
        
        if isPreview {
            let previewProfile = UIAlertAction(title: NSLocalizedString("Preview", comment: "Preview photo"), style: .default, handler: { (action) -> Void in
                self.presenter.loadProfileImageGalery(userResponseDataView: userResponseDataView)
            })
            
            actionSheet.addAction(previewProfile)
        }
        
        if isUpdate {
            if userResponseDataView.account.profileImage == nil {
                let updateProfile = UIAlertAction(title: NSLocalizedString("Register", comment: "Update photo"), style: .default, handler: { (action) -> Void in
                    self.presenter.showUpdatePhotoPerfil()
                })
                actionSheet.addAction(updateProfile)
            }else{
                let updateProfileB = UIAlertAction(title: NSLocalizedString("Update", comment: "Update photo"), style: .default, handler: { (action) -> Void in
                    self.presenter.showUpdatePhotoPerfil()
                })
                actionSheet.addAction(updateProfileB)
            }
        }
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func didOpenProfileGalery(data: Data) {
        guard let image = UIImage(data: data) else { return }
        performSegue(withIdentifier: "showImageGalery", sender: image)
    }
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        presenter.didFinishPin(userReponseDataView: userResponseDataView, pin: pin, pinViewController: pinViewController)
    }
    
    func didFailFinishPin(error: Error, pinViewController: PINViewController) {
        pinViewController.backToPreviousView()
    }
    
    func didTapCancelPin() {
        presenter.didTapCancelPin()
    }
    
    func didTapProfileImage() {
        presenter.didTapProfileImage()
    }
    
    /* MARK: - Table View Delegate */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.myAccountItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: MyAccountItemTableViewCell.self)
        cell.setup(myAccountItemDataView: presenter.myAccountItem(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modelItem = presenter.myAccountItem(at: indexPath.row)
        presenter.to(segueIdentifier: modelItem.action)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - MyAccountViewControllerDelegate
    func addBadge(index: Int) {
        self.tabBarController?.addDotAtTabBarItemIndex(index: index, radius: 7, color : UIColor.red)
    }
    
    func removeBadge(index: Int) {
        self.tabBarController?.removeDotAtTabBarItemIndex(index: index)
    }
    
    /* MARK: - Prepare For Segue */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUserFormsDetails" {
            guard let user = currentUser else { return }
            let userResponseDataView = UserResponseDataView(model: user)
            let destination = segue.destination as? UserDetailsFormViewController
            destination?.formsAction = .update
            destination?.userResponseDataView = userResponseDataView
        }        
        else if segue.identifier == "showAddress" {
            guard let user = currentUser else { return }
            let userResponseDataView = UserResponseDataView(model: user)
            let destination = segue.destination as? AddressDetailsFormViewController
            destination?.formsAction = .update
            destination?.userResponseDataView = userResponseDataView
        }
        else if segue.identifier == "showUpdatePhotoPerfil" {
            let destination = segue.destination as? SelfieRecomendationsViewController
            destination?.formsAction = .update
        }
        else if segue.identifier == "showPayments" {
            let destination = segue.destination as? ListCardCollectionViewController
            destination?.isPickerMode = false
            destination?.isHiddenCheckbox = true
            destination?.isHiddenMoreActions = false
            destination?.isHiddenFavoriteIcon = false
            
        }
        else if segue.identifier == "showPIN" {
            guard let user = currentUser else { return }
            let userResponseDataView = UserResponseDataView(model: user)
            
            let navigation = segue.destination as? UINavigationController
            let destination = navigation?.viewControllers.first as? PINViewController
            destination?.navigationTitle = NSLocalizedString("Type your current PIN", comment: "")
            destination?.isHiddenForgotPin = true
            destination?.isHiddenCancelButton = false
            destination?.delegate = self
            destination?.userResponseDataView = userResponseDataView
        }
        else if segue.identifier == "showImageGalery" {
            let navigation = segue.destination as? UINavigationController
            let imageGalleryController = navigation?.viewControllers.first as? ImageGalleryPageViewController
            imageGalleryController?.images = [sender as! UIImage]
            imageGalleryController?.currentIndex = 0
            imageGalleryController?.showsToolbar = false
        }
        navigationItem.setBackButtonTitle(title: "")
    }
}
