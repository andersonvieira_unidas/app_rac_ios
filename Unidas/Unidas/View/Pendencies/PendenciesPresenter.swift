//
//  PendenciesPresenter.swift
//  Unidas
//
//  Created by Felipe Machado on 07/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

protocol PendenciesViewDelegate: class {
    func reloadData()
    func startLoading()
    func endLoading()
    func readData(array:[PendencyDataView])
    func present(error: Error)
    func showMessageEmailConfirmation(email: String)
    func showMobileConfirmation()
}

protocol PendenciesPresenterDelegate {
    var delegate: PendenciesViewDelegate? { get set }
    func fetchUserStatus()
    func sendEmailConfirmation()
    func sendMobileConfirmation()
}

class PendenciesPresenter: PendenciesPresenterDelegate {
    weak var delegate: PendenciesViewDelegate?
    private var unidasUserStatusService: UnidasUserStatusService
    private var pendencyService: PendencyService
    private var recoveryService: RecoveryService
     
    init(unidasUserStatusService: UnidasUserStatusService,
         pendencyService: PendencyService,
         recoveryService: RecoveryService) {
        self.unidasUserStatusService = unidasUserStatusService
        self.pendencyService = pendencyService
        self.recoveryService = recoveryService
    }
    
    private var currentUser: UserResponse? {
        return session.currentUser
    }
    
    func fetchUserStatus() {
        self.delegate?.startLoading()
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        
        unidasUserStatusService.checkUserDocuments(documentNumber: documentNumber, response: (success: { [weak self] usersDocumentsUser in
            
            var pendenciesArray: [PendencyDataView] = []
            
            if let userStatus = usersDocumentsUser.userCheck {

                let emailIsValid = userStatus.emailIsValid ?? false
                let documentsIsValid = userStatus.documentLicenceIsValid ?? false
                let signatureIsValid = userStatus.signatureIsValid ?? false
                let paymentsIsValid = userStatus.paymentIsValid ?? false
                let mobileIsValid = userStatus.mobilePhoneIsValid ?? false
                let selfieIsValid = userStatus.selfieIsValid ?? false
                
                pendenciesArray.append(PendencyDataView(pendencieType: .Documents, pendencieStatus: documentsIsValid))
                pendenciesArray.append(PendencyDataView(pendencieType: .Payments, pendencieStatus: paymentsIsValid))
                pendenciesArray.append(PendencyDataView(pendencieType: .Signatures, pendencieStatus: signatureIsValid))
                pendenciesArray.append(PendencyDataView(pendencieType: .Email, pendencieStatus: emailIsValid))
                pendenciesArray.append(PendencyDataView(pendencieType: .Mobile, pendencieStatus: mobileIsValid))
                pendenciesArray.append(PendencyDataView(pendencieType: .Selfie, pendencieStatus: selfieIsValid))
               
                self?.delegate?.readData(array: pendenciesArray)

                self?.delegate?.endLoading()
            }
            
            }, failure: { error in
                self.delegate?.present(error: error)
        }, completion: {
            
        }))
    }
    
    func sendEmailConfirmation() {
        self.delegate?.startLoading()
        
        guard let currentUser = currentUser else { return }
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        let email = currentUser.account.email
        
        pendencyService.resendEmail(documentNumber: documentNumber, response: (success: { [weak self] in
            self?.delegate?.showMessageEmailConfirmation(email: email)
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: {
                self.delegate?.endLoading()
        }))
    }
    
    func sendMobileConfirmation() {
        self.delegate?.startLoading()
        
        guard let currentUser = currentUser else { return }
        
        let documentNumber = currentUser.account.documentNumber.removeDocumentNumberCharacters
        let name = currentUser.account.name
        let email = currentUser.account.email
        let phoneCodeNumber = String(currentUser.account.mobilePhone.prefix(2))
        let phoneNumber = String(currentUser.account.mobilePhone.suffix(9))
       
        recoveryService.resendSecretCode(name: name, phoneCodeNumber: phoneCodeNumber, phoneNumber: phoneNumber, documentNumber: documentNumber, email: email ,  response: (success: { [weak self] in
            self?.delegate?.showMobileConfirmation()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: {
        }))
       /* pendencyService.resendMobileConfirmation(documentNumber: documentNumber, method: AccountRecoveryMethod.sms, response: (success: { [weak self] in
            self?.delegate?.showMobileConfirmation()
            }, failure: { [weak self] error in
                self?.delegate?.present(error: error)
            }, completion: {
        }))*/
    }
}
