//
//  PendenciesViewController.swift
//  Unidas
//
//  Created by Felipe Machado on 06/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class PendenciesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PendenciesButtonDelegate, PendenciesViewDelegate {
    
    var pendenciesArray = [PendencyDataView]()
    var setupIsLoadedView = false
    var setupBottomTableView = false
    
    @IBOutlet weak var pendenciesTableView: UITableView!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private var presenter: PendenciesPresenterDelegate!
    
    private lazy var unidasHeaderView: EnhancedView = {
        let unidasHeaderView = EnhancedView()
        unidasHeaderView.translatesAutoresizingMaskIntoConstraints = false
        unidasHeaderView.cornerRadius = 25
        unidasHeaderView.roundTopLeftCorner = false
        unidasHeaderView.roundTopRightCorner = false
        unidasHeaderView.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        
        let logo = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        logo.translatesAutoresizingMaskIntoConstraints = false
        
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let buttonBack = UIButton(type: .system)
        buttonBack.frame = CGRect(x: 20, y: 12, width: 20, height: 20)
        buttonBack.tintColor = .white
        buttonBack.setImage(backButtonImage, for: .normal)
        buttonBack.addTarget(self, action: #selector(touchCancelItem), for: .touchUpInside)
        
        unidasHeaderView.addSubview(logo)
        unidasHeaderView.addSubview(buttonBack)
        
        NSLayoutConstraint.activate([
            logo.centerXAnchor.constraint(equalTo: unidasHeaderView.centerXAnchor),
            logo.centerYAnchor.constraint(equalTo: unidasHeaderView.centerYAnchor),
        ])
        
        return unidasHeaderView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.fetchUserStatus()
    }
    
    func configureLoadView() {
        
        viewTopConstraint.constant += 44
        view.addSubview(unidasHeaderView)
        
        NSLayoutConstraint.activate([
            unidasHeaderView.heightAnchor.constraint(equalToConstant: 44.0),
            unidasHeaderView.topAnchor.constraint(equalTo: view.saferAreaLayoutGuide.topAnchor, constant: 0),
            unidasHeaderView.leftAnchor.constraint(equalTo: view.saferAreaLayoutGuide.leftAnchor, constant: -1),
            unidasHeaderView.rightAnchor.constraint(equalTo: view.saferAreaLayoutGuide.rightAnchor, constant: 1)
        ])
        
        let backView = UIView()
        backView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        backView.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        view.addSubview(backView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if setupIsLoadedView {
            configureLoadView()
        }
        
        if setupBottomTableView {
            configureBottomTableView()
        }
        
        setHeaderUnidas()
        setupTableView()
        setupView()
        presenter = PendenciesPresenter(unidasUserStatusService: UnidasUserStatusService(),
                                        pendencyService: PendencyService(),
                                        recoveryService: RecoveryService())
        presenter.delegate = self
    }
    
    func setupView() {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back-button"), style: .done, target: self, action: #selector(touchCancelItem))
        backButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func readData(array: [PendencyDataView]) {
        pendenciesArray =  array
        reloadData()
    }
    
    func setupTableView() {
        pendenciesTableView.register(cellType: PendenciesCell.self)
        pendenciesTableView.backgroundView = activityIndicatorView
        pendenciesTableView.delegate = self
        pendenciesTableView.dataSource = self
        pendenciesTableView.separatorStyle = .none
    }
    
    
    //MARK: - PendenciesDelegate
    func reloadData() {
        pendenciesTableView.reloadData()
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showMessageEmailConfirmation(email: String) {
        
        let title = NSLocalizedString("Pendency Email Confirmation Title", comment: "")
        let description = NSLocalizedString("Pendency Email Confirmation %@ Text Email", comment: "")
        let descriptionText = String(format: description, email.lowercased())
        let simpleAlert = SimpleAlert(title: title, subTitle: descriptionText, image: #imageLiteral(resourceName: "ico-email"))
        
        simpleAlert.show(animated: true)
    }
    
    func showMobileConfirmation() {
        goTo(controller: "MobileConfirmation")
    }
    
    func getPendencieButtonIndex(cell: PendenciesCell) {
        switch cell.pendencieType {
        case .Documents:
            goTo(controller: "MyAccountDocumentVC")
        case .Payments:
            goTo(controller: "ListCard")
        case .PersonalData:
            goTo(controller: "UserDetailsForm")
        case .Signatures:
            goTo(controller: "MyAccountSignatureVC")
        case .Selfie:
            goTo(controller: "SelfieRecomendations")
        case .Email:
            presenter.sendEmailConfirmation()
        case .Mobile:
            presenter.sendMobileConfirmation()
        }
    }
    
    func goTo(controller: String) {
        
        switch controller {
        case "UserDetailsForm":
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "UserDetailsForm") as? UserDetailsFormViewController {
                guard let user = currentUser else { return }
                let userResponseDataView = UserResponseDataView(model: user)
                viewController.formsAction = .update
                viewController.userResponseDataView = userResponseDataView
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        case "ListCard":
            let storyboard = UIStoryboard(name: "Card", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "ListCard") as? ListCardCollectionViewController {
                viewController.isPickerMode = false
                viewController.isHiddenCheckbox = true
                viewController.isHiddenMoreActions = false
                viewController.isHiddenFavoriteIcon = false
                viewController.fromPendencies = true
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        case "MyAccountDocumentVC":
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "MyAccountDocumentVC") as? MyAccountDocumentViewController {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        case "MyAccountSignatureVC":
            let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "MyAccountSignatureVC") as? MyAccountSignatureViewController {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        case "SelfieRecomendations":
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "SelfieRecomendations") as? SelfieRecomendationsViewController {
                viewController.formsAction = .update
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        case "MobileConfirmation":
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "ValidatePhone") as? SignUpValidatePhoneViewController {
                viewController.formsAction = .update
                viewController.backButtonHidden = false
                viewController.isValidate = true
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        default:
            break
        }
    }
    
    //MARK: - Private Functions
    func configureBottomTableView(){
        let height = self.tabBarController?.tabBar.frame.height ?? 49.0
        viewBottomConstraint.constant = height
    }
    
    //MARK: IBAction
    
    @IBAction func sendDocuments(_ sender: UIButton) {
    }
    
    //MARK: TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendenciesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: PendenciesCell.self)
        let pendencie = pendenciesArray[indexPath.row]
        cell.setup(type: pendencie.pendencieType, status: pendencie.pendencieStatus)
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUserDetailsForm" {
            
            let navigation = segue.destination as? UINavigationController
            let viewController = navigation?.viewControllers.first as? UserDetailsFormViewController
            guard let user = currentUser else { return }
            let userResponseDataView = UserResponseDataView(model: user)
            viewController?.formsAction = .update
            viewController?.userResponseDataView = userResponseDataView
            navigation?.modalPresentationStyle = .fullScreen
            
        }else if segue.identifier == "showMyDocumentsSignature" {
            
            let destination = segue.destination as? MyAccountSignatureViewController
            destination?.modalPresentationStyle = .fullScreen
            
        }else if segue.identifier == "showMyDocumentsDoc" {
            
            let navigation = segue.destination as? UINavigationController
            
            let _ = navigation?.viewControllers.first as? MyAccountDocumentViewController
            navigation?.modalPresentationStyle = .fullScreen
            
        }else if segue.identifier == "showMyCardsPayment" {
            
            let navigation = segue.destination as? UINavigationController
            let vc = navigation?.viewControllers.first as? ListCardCollectionViewController
            vc?.fromPendencies = true
            navigation?.modalPresentationStyle = .fullScreen
            
        }else if segue.identifier == "showPhoneValidate" {
            
            let navigation = segue.destination as? UINavigationController
            let _ = navigation?.viewControllers.first as? SignUpValidatePhoneViewController
            navigation?.modalPresentationStyle = .fullScreen
        }
    }
}

