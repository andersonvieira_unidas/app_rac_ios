//
//  SerasaCredDefenseNotificationViewController.swift
//  Unidas
//
//  Created by Felipe Machado on 25/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class SerasaCredDefenseNotificationViewController: UIViewController {

    @IBOutlet weak var storeInfoLabel: UILabel!
    @IBOutlet weak var storeInfoLabel2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLayout()        
    }
    
    func configureLayout() {
        storeInfoLabel.attributedText = setBoldToText(string: storeInfoLabel.text!)
        storeInfoLabel2.attributedText = setBoldToText(string: storeInfoLabel2.text!)
    }
    
    func setBoldToText(string: String) -> NSMutableAttributedString {
        
        let fontBold = UIFont.medium(ofSize: 14)
        let font = UIFont.regular(ofSize: 14)
        
        let boldRanges = find(inText: string, pattern: "#b#")
        let colorAttribute: [NSAttributedString.Key : Any] = [.font: fontBold]
        
        let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font : font])
        
        boldRanges.reversed().forEach { (aResult) in
            let subTextNSRange = aResult.range(at: 1)
            guard let subTextRange = Range(subTextNSRange, in: attributedString.string) else { return }
            let replacementString = String(attributedString.string[subTextRange])
            let replacementAttributedString = NSAttributedString(string: replacementString, attributes: colorAttribute)
            attributedString.replaceCharacters(in: aResult.range, with: replacementAttributedString)
        }
        return attributedString
    }
    
    func find(inText text: String, pattern: String) -> [NSTextCheckingResult] {
        let regex = try! NSRegularExpression(pattern:pattern+"(.*?)"+pattern, options: [])
        return regex.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.utf16.count))
    }
    
    @IBAction func dimissViewController(_ sender: UnidasButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
