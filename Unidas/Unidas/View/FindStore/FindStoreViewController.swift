//
//  FindStoreViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 31/05/2018.
//  Copyright © 2018 Unidas. All rights reserved.
//

import UIKit
import MapKit
import Cluster

class FindStoreViewController: UIViewController, FindStoreViewDelegate, MKMapViewDelegate, FindStoreViewControllerDelegate, UIScrollViewDelegate, StandartCarInformationViewControllerDelegate, MultipleChoiceAlertDelegate {
    

    private var presenter: FindStorePresenterDelegate!
    private var activityIndicatorView: UIActivityIndicatorView!
    private var mapHeight: CGFloat = 0
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel?
    @IBOutlet weak var distanceImage: UIImageView?
    @IBOutlet weak var previewAddressLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var findStoreView: UILabel!
    
    @IBOutlet weak var shadowSearchGarageButton: ShadowView!
    @IBOutlet weak var searchGarageButton: EnhancedButton!
    @IBOutlet weak var currentLocationButton: EnhancedButton!
    @IBOutlet weak var detailsHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var currentLocationBottomConstraint: NSLayoutConstraint?
    
    lazy var clusterManager: ClusterManager = {
        let manager = ClusterManager()
        manager.maxZoomLevel = 16
        manager.minCountForClustering = 2
        manager.clusterPosition = .nearCenter
        
        return manager
    }()
    
    var defaultTitleLabel = ""
    var removeExpressStores = Bool()
    private var mapChangedFromUserInteraction = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.change(backgroundColor:  #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
        configureData()
        setDelegates()
        configureDisplay()
        configureButtonBack()
        setupToggle()
    }
    
    // MARK : - Private Functions
    private func setDelegates() {
        presenter.delegate = self
        mapView.delegate = self
    }
    
    private func configureDisplay() {
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        let _ = self.navigationController?.navigationBar.subviews.map{ $0.removeFromSuperview() }

        defaultTitleLabel = titleLabel.text ?? ""
        
        if let mapHeight = mapViewHeightConstraint?.constant {
            self.mapHeight = mapHeight
        }
    }
    
    private func configureData() {
        presenter = FindStorePresenter(locationManager: LocationManager(), unidasGarageService: UnidasGarageService())
        
        showMapFullScreen()
        setupActivityIndicator()
        setupSearchButtons()
        
        //removeExpressStores = true
        presenter.currentUserLocation(fetchStore: true, filterExpress: removeExpressStores)
    }
    
    private func showMapFullScreen(){
        distanceView.isHidden = true
        searchGarageButton.isHidden = false
        shadowSearchGarageButton.isHidden = false
        detailsView.isHidden = true
        detailsHeightConstraint?.constant = 0
        mapViewHeightConstraint?.constant = mapHeight
        currentLocationBottomConstraint?.constant = -100
    }
    
    private func hideButtonSearchGarage(){
        searchGarageButton.isHidden = true
        shadowSearchGarageButton.isHidden = true
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    private func mapViewRegionDidChangeFromUserInteraction() -> Bool {
        let view = self.mapView.subviews[0]
        if let gestureRecognizers = view.gestureRecognizers {
            for recognizer in gestureRecognizers {
                if (recognizer.state == UIGestureRecognizer.State.began || recognizer.state == UIGestureRecognizer.State.ended ) {
                    return true
                }
            }
        }
        return false
    }
    
    private func setupToggle(){
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                self.searchGarageButton.isHidden = true
                self.shadowSearchGarageButton.isHidden = true
                self.currentLocationBottomConstraint?.constant = -20
            } else {
                self.searchGarageButton.isHidden = false
                self.shadowSearchGarageButton.isHidden = false
                self.currentLocationBottomConstraint?.constant = -100
            }
        }
    }
    
    func askToUpdateApp() {
        
        let title = NSLocalizedString("App Update Title", comment: "")
        let subTitle = NSLocalizedString("App Update Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("App Update No", comment: "")
        let secondChoiceLabel = NSLocalizedString("App Update Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-alert"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 3, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    private func openDetails(garageDataView: GarageDataView, locationDataView: LocationDataView?){
        presenter.currentGarage = garageDataView.model
        hideButtonSearchGarage()
        
        distanceView.isHidden = false
        detailsView.isHidden = false
        
        titleLabel.text = garageDataView.name
        previewAddressLabel.text = garageDataView.address?.firstAddressLine
        distanceLabel?.text = garageDataView.address?.shortFormattedCalculeDistance(at: locationDataView)
        
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
                self.detailsHeightConstraint?.constant = 256
                self.mapViewHeightConstraint?.constant = self.mapHeight - 256
                self.currentLocationBottomConstraint?.constant = -20
            }, completion: nil)
        }
    }
    
    private func hideDetails(){
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
            self.showMapFullScreen()
        }, completion: nil)
        
        titleLabel.text = defaultTitleLabel
    }
    
    private func setupSearchButtons(){
        let searchButtonItem = UIBarButtonItem(image:  #imageLiteral(resourceName: "ico-lens"), style: .plain, target: self, action: #selector(tapSearchGarages))
        navigationItem.rightBarButtonItem = searchButtonItem
    }
    
    func showExpressRouteView(image:UIImage, title:String, subtitle:String, firstButtonTag:Int, secondButtonTag:Int?, firstButtonColor:UIColor, secondButtonColor:UIColor?, firstButtonLabel:String, secondButtonLabel:String?, firstButtonEnabled:Bool, secondButtonEnabled:Bool) {
        
        let multipleChoiceAlert = MultipleChoiceAlert(title: title, subTitle: subtitle, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel ?? "", image: image, firstChoiceButtonEnabled: firstButtonEnabled, secondChoiceButtonEnabled: secondButtonEnabled, firstChoiceButtonColor: firstButtonColor, secondChoiceButtonColor: secondButtonColor, firstButtonTag: firstButtonTag, secondButtonTag: secondButtonTag)
        multipleChoiceAlert.delegate = self
        multipleChoiceAlert.show(animated: true)
    }
    
    // MARK: - Interface builder actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func touchUserLocation(_ sender: EnhancedButton) {
        hideDetails()
        presenter.currentUserLocation(fetchStore: false, filterExpress: false)
    }
    
    @IBAction func tapOpenMaps(_ sender: Any) {
        presenter.didTapOpenMaps()
    }
    
    @IBAction func tapCallPhone(_ sender: Any) {
        guard let phoneNumber = presenter.phoneNumber, let number = URL(string: "tel://" + phoneNumber) else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func tapOpenReserve(_ sender: UIButton) {

        if presenter.compareAppVersionToUpdate() == true {
            askToUpdateApp()
        }else{
            guard let garage = presenter.currentGarage else { return }
            let garageDataView = GarageDataView(model: garage)
            if garageDataView.isExpress {
                presenter.fetchStatusCliente()
            } else {
                presenter.didTapOpenReserve()
            }
        }
    }
    
    @IBAction func tapSearchGarages() {
        
        let storyboard = UIStoryboard(name: "ConnectedCarRent", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "GaragesTableViewController") as? GaragesTableViewController  {
            vc.delegate2 = self
            vc.findStoreToMaps = true
            vc.removeExpressStores = removeExpressStores
            self.navigationController?.pushViewController(vc, animated: true)
        }        
    }
    
    @IBAction func tapSeeAvailableStoreHours() {
        presenter.didTapOnAvailableStoreHours()
    }
    
    // MARK: - Map View Delegate
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        mapChangedFromUserInteraction = mapViewRegionDidChangeFromUserInteraction()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        clusterManager.reload(mapView: mapView)
        
        if (mapChangedFromUserInteraction) {
            
            if !detailsView.isHidden {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: {
                        self.mapViewHeightConstraint?.constant = self.mapHeight
                        self.detailsHeightConstraint?.constant = 0
                        self.detailsView.isHidden = true
                        self.distanceView.isHidden = true
                        self.shadowSearchGarageButton.isHidden = false
                        self.searchGarageButton.isHidden = false
                        self.currentLocationBottomConstraint?.constant = -100
                    }, completion: nil)
                }
                
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? ClusterAnnotation {
            let identifier = "Cluster"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
            if let view = view as? ClusterAnnotationView {
                view.annotation = annotation
            } else {
                view = StyledClusterAnnotationView(annotation: annotation, reuseIdentifier: identifier, style: .color( #colorLiteral(red: 0, green: 0.6549019608, blue: 1, alpha: 1), radius: 20.0))
            }
            return view
        } else {
            guard let annotation = annotation as? Annotation, let style = annotation.style else { return nil }
            let identifier = "Pin"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if let view = view {
                view.annotation = annotation
            } else {
                view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            
            if case let .image(image) = style {
                view?.image = image
            }
            
            return view
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        if let cluster = annotation as? ClusterAnnotation {
            var zoomRect = MKMapRect.null
            for annotation in cluster.annotations {
                let annotationPoint = MKMapPoint.init(annotation.coordinate)
                let pointRect = MKMapRect.init(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
                if zoomRect.isNull {
                    zoomRect = pointRect
                } else {
                    zoomRect = zoomRect.union(pointRect)
                }
            }
            mapView.setVisibleMapRect(zoomRect, animated: true)
        }
        else {
            guard let coordinate = view.annotation?.coordinate else {
                return
            }
            
            let annotation = view.annotation!
            
            mapView.removeAnnotation(annotation)
            
            if detailsView.isHidden {
                presenter.didSelectAnnotation(coordinate: coordinate)
            }
            mapView.addAnnotation(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        views.forEach { $0.alpha = 0 }
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
            views.forEach { $0.alpha = 1 }
        }, completion: nil)
    }
    
    // MARK: - FindStoreViewDelegate
    
    func reloadMap() {
        clusterManager.reload(mapView: mapView)
    }
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func present(error: String) {
        alertError(errorMessage: error)
    }
    
    func showCurrentLocation(locationDataView: LocationDataView, fetchStore: Bool, filterExpress: Bool) {
        let viewRegion = MKCoordinateRegion.init(center: locationDataView.coordinate, latitudinalMeters: 1500, longitudinalMeters: 1500)
        mapView.showsUserLocation = true
        mapView.setRegion(viewRegion, animated: true)
        
        if fetchStore {
            presenter.fetchStores(nearby: locationDataView, immediately: false, filterExpress: filterExpress)
        }
    }
    
    func setNearGarage(location: Location){
        let viewRegion = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: 1500, longitudinalMeters: 1500)
        mapView.setRegion(viewRegion, animated: false)
    }
    
    func showStores(garageDataView: [GarageDataView]) {
        
        clusterManager.removeAll()
        
        let storeFilter = garageDataView.filter({$0.address?.location != nil})
        
        if removeExpressStores {
            let storeFilteredByExpress = storeFilter.filter { (garage) -> Bool in
                if garage.isExpress == false {
                    return true
                }
                return false
            }
            
            let storeAnnotations = storeFilteredByExpress.map { (garage) -> Annotation in
                let annotation = Annotation()
                annotation.coordinate = garage.address!.location!.coordinate
                annotation.style = .image(garage.iconStore)
                
                return annotation
            }
            clusterManager.add(storeAnnotations)
        }else{
            let storeAnnotations = storeFilter.map { (garage) -> Annotation in
                let annotation = Annotation()
                annotation.coordinate = garage.address!.location!.coordinate
                annotation.style = .image(garage.iconStore)
                
                return annotation
            }
            
            clusterManager.add(storeAnnotations)
        }
    }
    
    func showGarageDetails(garageDataView: GarageDataView, locationDataView: LocationDataView?) {
        if let coordinate = garageDataView.address?.location?.coordinate {
            mapView.setCenter(coordinate, animated: false)
        }
        
        let animation = UIViewPropertyAnimator(duration: 0.5, dampingRatio: 1.0) {
            self.openDetails(garageDataView: garageDataView, locationDataView: self.presenter.currentLocationDataView)
        }
        animation.startAnimation()
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
    
    func showStandartCar(garageDataView: GarageDataView) {
        performSegue(withIdentifier: "showStandartCar", sender: garageDataView)
    }
    
    func showAvailableHours(availableHours: OpeningHours) {
        performSegue(withIdentifier: "showAvailableStoreHours", sender: availableHours)
    }
    
    func askPermissionToSettings() {
        askPermissionToSettings(withMessage: NSLocalizedString("Settings Location Description", comment: ""))
    }
    
    //MARK: FindStoreViewControllerDelegate
    func setLocation(garage: GarageDataView) {
        
        if  let lat = garage.model.address?.location?.latitude,
            let lon = garage.model.address?.location?.longitude {
            let storeLocation = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            mapView.setCenter(storeLocation, animated: true)
        }
        showGarageDetails(garageDataView: garage, locationDataView: presenter.currentLocationDataView)
    }
    
    func backButton() {
        print("back")
    }
    
    func checkForExpressAvailability(pendencie: Bool, financialPendencie: Bool) {
        let currentUser = session.currentUser
        
        if currentUser == nil {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-express-car") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Login Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Login Subtitle", comment: ""), firstButtonTag: 6, secondButtonTag: 7, firstButtonColor: #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Login Btn1", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Login Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: true)
            }
            
        }else if financialPendencie {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-car-restriction") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Profile Financial Pendencie Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Profile Financial Pendencie Subtitle", comment: ""), firstButtonTag: 1, secondButtonTag: 2, firstButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Profile Financial Pendencie Btn3", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Profile Financial Pendencie Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: false)
            }
    
        }else if pendencie {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-car-restriction") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Profile Pendencie Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Profile Pendencie Subtitle", comment: ""), firstButtonTag: 3, secondButtonTag: nil, firstButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), secondButtonColor: nil, firstButtonLabel: NSLocalizedString("Unidas Express Profile Pendencie Btn", comment: ""), secondButtonLabel: nil, firstButtonEnabled: true, secondButtonEnabled: false)
            }
        }else{

            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-express-car") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Presentation Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Presentation Subtitle", comment: ""), firstButtonTag: 4, secondButtonTag: 5, firstButtonColor: #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Presentation Btn1", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Presentation Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: true)
            }
            //TODO - colocar
            //presenter.didTapOpenReserve()
        }
    }
    
    
    //MARK: - StandartCarInformationViewControllerDelegate
    func redirectToReservationsTab() {
        dismiss(animated: true) {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    
    //MARK: - MultipleChoiceAlert Delegate
    func choiceFirstTap(tag: Int) {
        switch tag {
        case 1:
            hideDetails()
            removeExpressStores = true
            presenter.removeExpressStores()
        case 3:
            performSegue(withIdentifier: "showPendencies", sender: nil)
        default:
            print("default")
        }
    }
    
    func choiceSecondTap(tag: Int) {
        switch tag {
        case 2:
            VersionControl.openAppStore()
        case 5:
            presenter.didTapOpenReserve()
        case 7:
            performSegue(withIdentifier: "showLogin", sender: nil)
        default:
            print("default")
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStandartCar" {
            let navigation = segue.destination as? UINavigationController
            let standartCarRent = navigation?.viewControllers.first as? StandartCarInformationViewController
            standartCarRent?.modalPresentationStyle = .fullScreen
            standartCarRent?.garageDataView = sender as? GarageDataView
            standartCarRent?.delegate = self
        }
        if segue.identifier == "showAvailableStoreHours" {
            let vc = segue.destination as? AvailableHoursViewController
            let openinghours = sender as? OpeningHours
            vc?.openingHours = openinghours
        }
    }
}
