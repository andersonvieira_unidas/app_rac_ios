//
//  GarageListViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 31/07/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit


protocol FindStoreViewControllerDelegate {
    func setLocation(garage: GarageDataView)
    func backButton()
}

class GarageListViewController: UIViewController, UISearchBarDelegate, UISearchResultsUpdating, GaragesFindStoreViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var presenter: GaragesFindStoreViewPresenter!
    var searchController: UISearchController!
    var activityIndicatorView: UIActivityIndicatorView!
    var delegate: FindStoreViewControllerDelegate?
    
    lazy var emptyResultsView: EmptyResultsView = {
        return EmptyResultsView.loadFromNib()
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = GaragesFindStoreViewPresenter(garageService: UnidasGarageService(), firebaseAnalytics: FirebaseAnalyticsService(), locationManager: LocationManager())
        
        setupSearchController()
        setupActivityIndicator()
        setupTableView()
        presenter.fetchCurrentLocation()
        presenter.delegate = self
        AppDelegate.AppUtility.lockOrientation(.portrait)
    }
 
    func setupSearchController() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.tintColor = .white
        searchController.searchBar.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        searchController.searchBar.clearButtonMode = .never
        searchController.searchBar.textColor = .white
        searchController.searchBar.setPlaceholderTextColor(UIColor.white.withAlphaComponent(0.5))
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.extendedLayoutIncludesOpaqueBars = true
        definesPresentationContext = true
    }
    
    func setupTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(cellType: GarageTableViewCell.self)
        tableView.backgroundView = activityIndicatorView
        tableView.tableHeaderView = searchController.searchBar
        //tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        tableView.separatorStyle = .none
        tableView.tableHeaderView?.frame = CGRect(x: self.tableView.tableHeaderView?.frame.origin.x ?? 0, y: self.tableView.tableHeaderView?.frame.origin.y ?? 0, width: self.tableView.tableHeaderView?.frame.width ?? 0, height: 0)
        
        //let headerNib = UINib.init(nibName: "GarageTableViewHeader", bundle: Bundle.main)
        //tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "GarageTableViewHeader")
    }
    
    func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        delegate?.backButton()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSearch(_ sender: UIButton) {
        pressSearchButton(value: true)
    }
    
    func scrollToTop(){
        DispatchQueue.main.async {
            if self.presenter.numberOfGarages != 0 {
                self.tableView.setContentOffset(.zero, animated: true)
            }
        }
    }
        
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfGarages
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: GarageTableViewCell.self)
        cell.setup(with: presenter.garage(at: indexPath.row), currentLocation: presenter.currentLocation)
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.setLocation(garage: presenter.garage(at: indexPath.row))
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GarageTableViewHeader") as? GarageTableViewHeader {
            return headerView
        }
        
        return UIView()
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    // MARK: - UISearch bar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter.searchButtonTapped(with: searchController.searchBar.text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchGarages(with: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter.fetchCurrentLocation()
    }
    
    // MARK: - Garage view delegate
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func startLoading() {
        tableView.tableFooterView = UIView()
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showEmptyResultsView() {
        tableView.tableFooterView = emptyResultsView
    }
    
    func pressSearchButton(value: Bool) {
        UIView.animate(withDuration: 0.1, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
            self.tableView.tableHeaderView?.frame = CGRect(x: self.tableView.tableHeaderView?.frame.origin.x ?? 0, y: self.tableView.tableHeaderView?.frame.origin.y ?? 0, width: self.tableView.tableHeaderView?.frame.width ?? 0, height: 56.0)
        }, completion: { (finished) -> Void in
            self.view.layoutIfNeeded()
            self.tableView.reloadData()
        })
        self.scrollToTop()
    }
}
