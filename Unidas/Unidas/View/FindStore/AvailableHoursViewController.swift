//
//  AvailableHoursViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 27/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class AvailableHoursViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var availableHoursTableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    
    var openingHours: OpeningHours?
    var quantityHours = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        setupView()
        fetchAvailableHours()
    }
    
    func setupView() {
        backView.layer.cornerRadius = 10
        dismissButton.setImage(UIImage(named: "ico-close"), for: .normal)
        dismissButton.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    }
    
    func setupDelegates() {
        availableHoursTableView.delegate = self
        availableHoursTableView.dataSource = self
    }
    
    func fetchAvailableHours() {
        guard let openingHours = openingHours as? OpeningHours else { return }
        availableHoursTableView.reloadData()
    }
    
    @IBAction func dismissViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        quantityHours = openingHours?.periods.count ?? 0
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = availableHoursTableView.dequeueReusableCell(withIdentifier: "AvailableStoreCell", for: indexPath) as? AvailableStoreCell {
            
            let additionalSeparator = DottedLineView(frame: CGRect(x: 20, y: 0, width: cell.frame.size.width - 40, height: 1))
            additionalSeparator.lineWidth = 1
            additionalSeparator.round = true
            additionalSeparator.horizontal = true
            additionalSeparator.lineColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
            cell.addSubview(additionalSeparator)
            
            if let periods = openingHours?.periods {
                
                if quantityHours <= indexPath.row + 1 {
                    
                    switch quantityHours {
                    case 1:
                        cell.daysLabel.text = NSLocalizedString("weekday", comment: "")
                        cell.hoursLabel.text = NSLocalizedString("Closed", comment: "")
                        quantityHours += 1
                        return cell
                    case 2:
                        cell.daysLabel.text = NSLocalizedString("saturday", comment: "")
                        cell.hoursLabel.text = NSLocalizedString("Closed", comment: "")
                        quantityHours += 1
                        return cell
                    case 3:
                        cell.daysLabel.text = NSLocalizedString("sunday", comment: "")
                        cell.hoursLabel.text = NSLocalizedString("Closed", comment: "")
                        quantityHours += 1
                        return cell
                    case 4:
                        cell.daysLabel.text = NSLocalizedString("holiday", comment: "")
                        cell.hoursLabel.text = NSLocalizedString("Closed", comment: "")
                        return cell
                    default:
                        print("")
                    }
                }else{
                    cell.daysLabel.text = NSLocalizedString("\(periods[indexPath.row].day)", comment: "")
                    cell.hoursLabel.text = "\(periods[indexPath.row].open) - \(periods[indexPath.row].close)"
                    return cell
                }
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
