//
//  AvailableStoreCell.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 27/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class AvailableStoreCell: UITableViewCell {
    
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
