//
//  FAQViewController.swift
//  Unidas
//
//  Created by Felipe Machado on 10/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import WebKit

class FAQViewController: UIViewController, UIWebViewDelegate, WKNavigationDelegate {
    
    var webView : WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderUnidas()
        configureButtonBack()
        fetchFAQUrl()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    // MARK: - Private functions
    
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func fetchFAQUrl() {
        let remoteConfig = RemoteConfigUtil.getRemoteConfig()
        guard let url = remoteConfig["url_faq"].stringValue else {return}
        openUrl(url: url)
    }
    
    func openUrl(url: String) {
        
        LoadingScreen.setLoadingScreen(view: self.view)
        
        guard let url = NSURL(string: url) else {return}
        let urlRequest = URLRequest(url: url as URL)
        webView = WKWebView(frame: self.view.frame)
        webView.scrollView.bounces = false
        webView.navigationDelegate = self
        webView.load(urlRequest)
        self.view.addSubview(webView)
        self.view.sendSubviewToBack(webView)
    }
    
    //MARK:- WKNavigationDelegate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print("Error: \(error.localizedDescription)")
        present(error: error)
        self.navigationController?.popViewController(animated: true)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finish to load")
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
}
