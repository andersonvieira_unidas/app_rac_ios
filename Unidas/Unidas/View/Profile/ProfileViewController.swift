//
//  ProfileViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 21/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, ProfileViewDelegate {
    @IBOutlet weak var isNotLoggedContainerView: UIView!
    @IBOutlet weak var isLoggedContainerView: UIView!
    
    private var presenter: ProfilePresenterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ProfilePresenter()
        presenter.delegate = self
        presenter.checkLogged()
        registerForLoginNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        presenter.checkLogged()
    }
    
    func showLoggedView() {
        performSegue(withIdentifier: "logonSegue", sender: nil)
    }
    
    func showNotLogggedView() {
        performSegue(withIdentifier: "logoffSegue", sender: nil)
    }
    
    private func registerForLoginNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(userDidLogout(notification:)), name: .UNUserDidLogout, object: nil)
        notificationCenter.addObserver(self, selector: #selector(userDidLogin(notification:)), name: .UNUserDidLogin, object: nil)
    }
    
    @objc func userDidLogout(notification: Notification) {
        self.navigationController?.change(backgroundColor: #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
    }
    
    @objc func userDidLogin(notification: Notification) {
        setHeaderUnidas()
        showLoggedView()
    }
}
