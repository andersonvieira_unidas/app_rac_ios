//
//  ProfileOffViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class ProfileOffViewController: UIViewController{

    override func viewDidLoad() {
        navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.change(backgroundColor: #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
    }
}
