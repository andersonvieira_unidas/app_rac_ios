//
//  ProfileOnViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol ProfileOnViewControllerDelegate{
    func addBadge(index: Int)
    func removeBadge(index: Int)
}

class ProfileOnViewController: UIViewController, ProfileLoggedViewDelegate, UITableViewDataSource, UITableViewDelegate, ProfileOnViewControllerDelegate, MultipleChoiceAlertDelegate{
   
    @IBOutlet weak var profileHeaderView: UIView?
    @IBOutlet weak var tableView: UITableView?
    
    private var presenter: ProfileLoggedDelegate!
    private var headerLoginOnView = HeaderLoginOnView.loadFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ProfileLoggedPresenter(reservationDataStore: ReservationDataStoreUserDefaults(),imageTemporaryDirectory: ImageTemporaryDirectory(accountImageService: AccountImageService()),unidasUserStatusService: UnidasUserStatusService(),firebaseAnalyticsService: FirebaseAnalyticsService())
        
        presenter.delegate = self
        presenter.showUserHeaderView()
        setHeaderUnidas()
        setupHeaderProfile()
        setupTableView()
        
        navigationItem.hidesBackButton = true
    }
    
    private func setupTableView(){
        guard let tableView = tableView else { return }
        tableView.register(cellType: ProfileItemTableViewCell.self)
    }
    
    private func disableNotifications() {
        self.tabBarController?.tabBar.items?[1].isEnabled = false
    }
    
    private func setupHeaderProfile(){
        guard let profileHeader = profileHeaderView else { return }
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapProfile))
        headerLoginOnView.addGestureRecognizer(singleTap)
        
        profileHeader.addSubview(headerLoginOnView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setHeaderUnidas()
        headerLoginOnView.reloadPendencies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.setupHeaderProfile()
        if session.currentUser != nil {
            enableNotification()
        }
    }
    
    func showMoreHeaderLoginOnView(userResponseDataView: UserResponseDataView) {
        headerLoginOnView.setup(userResponseDataView: userResponseDataView, showMaskPhotoButton: false)
        headerLoginOnView.delegateProfile = self
    }
    
    func performSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: nil)
    }
    
    func removeBadge() {
        self.tabBarController?.removeDotAtTabBarItemIndex(index: 1)
    }
    
    func enableNotification() {
        self.tabBarController?.tabBar.items?[1].isEnabled = true
    }
    
    // MARK: - TableView Delegte
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.profileItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProfileItemTableViewCell.self)
        cell.setup(profileItemDataView: presenter.profileItem(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modelItem = presenter.profileItem(at: indexPath.row)
        
        if let action = modelItem.action {
            presenter.to(segueIdentifier: action)
        } else if let closure = modelItem.model.closure {
            closure()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - ProfileOnViewControllerDelegate
    func addBadge(index: Int) {
        self.tabBarController?.addDotAtTabBarItemIndex(index: index, radius: 7, color : UIColor.red)
    }
    
    func removeBadge(index: Int) {
        self.tabBarController?.removeDotAtTabBarItemIndex(index: index)
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        
    }
    
    func choiceSecondTap(tag: Int) {
        self.presenter.addEventSessionEnd(success: true)
        self.presenter.logout()
        self.disableNotifications()
    }
    
    func showLogoutConfirmationMessage() {
        let title = NSLocalizedString("Unidas Title", comment: "")
        let subtitle = NSLocalizedString("Logout are you sure", comment: "")
        let yes = NSLocalizedString("Logout ok", comment: "")
        let no = NSLocalizedString("Logout cancel", comment: "")
        
        let multipleChoice = MultipleChoiceAlert(title: title,
                                                 subTitle: subtitle,
                                                 firstChoiceButtonLabel: no,
                                                 secondChoiceButtonLabel: yes,
                                                 image: #imageLiteral(resourceName: "icon-profile-signup"),
                                                 firstChoiceButtonColor: UIColor.themeImportantButton)
        multipleChoice.delegate = self
        
        multipleChoice.show(animated: true)
    }

    @objc func tapProfile(){
        performSegue(identifier: "showMyAccount")
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContractHistory", let destination = segue.destination as? ContractsViewController{
            
            destination.navigationController?.navigationItem.setBackButtonTitle(title: "")
            
        }
    }
    
}
