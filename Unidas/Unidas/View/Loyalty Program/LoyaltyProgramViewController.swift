//
//  LoyaltyProgramViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class LoyaltyProgramViewController: UIViewController, LoyaltyProgramViewDelegate {
    
    @IBOutlet weak var loyaltyProgramStackView: UIStackView!
    @IBOutlet weak var loyaltyProgramCategoryDescriptionLabel: UILabel!
    @IBOutlet weak var loyaltyProgramAvailablePointsLabel: UILabel!
    @IBOutlet weak var loyaltyProgramKeepCategoryPointsLabel: UILabel!
    @IBOutlet weak var loyaltyProgramNextCategoryPointsLabel: UILabel!
    @IBOutlet weak var loyaltyProgramAccumulatedPointsLabel: UILabel!
    @IBOutlet weak var loyaltyProgramProgressView: UIProgressView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loyaltyProgramSlider: UISlider!
    
    @IBOutlet weak var loyaltyProgramPointsBackView: EnhancedView!
    @IBOutlet weak var dottedView: UIView!
    
    
    lazy var presenter: LoyaltyProgramPresenterDelegate = {
        let presenter = LoyaltyProgramPresenter(loyaltyProgramService: LoyaltyProgramService())
        presenter.delegate = self
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loyaltyProgramPointsBackView.alpha = 0.0
        presenter.viewDidLoad()
        configureBackButton()
    }
    
    // MARK: - Loyalty Program View delegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
        animate(view: loyaltyProgramPointsBackView, alpha: 0)
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
        animate(view: loyaltyProgramPointsBackView, alpha: 1)
    }
    
    func setupView() {
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
    }
    
    func setup(for loyaltyProgram: LoyaltyProgramDataView) {
        //        loyaltyProgramCategoryDescriptionLabel.text = loyaltyProgram.categoryDescription
        loyaltyProgramAvailablePointsLabel.text = "00"
        loyaltyProgramAvailablePointsLabel.text = loyaltyProgram.availablePoints
        loyaltyProgramKeepCategoryPointsLabel.text = loyaltyProgram.nextCategoryPoints
        loyaltyProgramNextCategoryPointsLabel.text = loyaltyProgram.minimumBalanceNextCategory
        loyaltyProgramAccumulatedPointsLabel.text = loyaltyProgram.availablePoints
        loyaltyProgramProgressView.setProgress(loyaltyProgram.accumulatedPointsPercent, animated: true)
        loyaltyProgramSlider.setThumbImage(#imageLiteral(resourceName: "icon-elipse-loyalitie"), for: .normal)
        loyaltyProgramSlider.setValue(loyaltyProgram.accumulatedPointsPercent, animated: true)
    }
    
    func present(error: Error) {
        let emptyDataView = EmptyDataView.loadFromNib()
        emptyDataView.setup(title: "Error", message: error.localizedDescription, image: nil, buttonTitle: nil, buttonIsHidden: true)
        emptyDataView.frame = view.frame
        view.addSubview(emptyDataView)
    }
    
    func presentLoyaltyProgramCallToAction() {
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = view.frame
        emptyView.imageView.image = #imageLiteral(resourceName: "icon-programa-fidelidade")
        emptyView.titleLabel.text = NSLocalizedString("Be part of this club", comment: "Loyalty program call to action title")
        emptyView.messageTextView.text = NSLocalizedString("Renting a car with more agility, having a new fleet at your disposal and being served by those who have a passion for the customer is what you already know about Unidas.", comment: "Loyalty program call to cation message")
        emptyView.actionButton.setTitle("I want to participate", for: .normal)
        emptyView.actionButton.isHidden = true
        
        view.addSubview(emptyView)
    }
    
    private func animate(view: UIView, alpha: CGFloat) {
        let animator = UIViewPropertyAnimator(duration: 0.5, curve: .easeInOut) {
            view.alpha = alpha
        }
        animator.startAnimation()
    }
    
    //MARK: - Private Functions
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callMaintenance(_ sender: UIButton) {
        if let navigation = self.navigationController {
            MaintenanceInvoker.presenterMaintenanceController(navigation: navigation)
        }
    }
    
}
