//
//  OnboardViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class OnboardHelpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    var items: [OnboardHelpContent] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        setupTableView()
    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: OnboardHelpItemTableViewCell.self)
        tableView.tableFooterView = UIView()
    }

    @IBAction func tapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: OnboardHelpItemTableViewCell.self)
        cell.setup(onboardHelpContent: items[indexPath.row])
        return cell
    }
    
  //  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   //     return UITableView.automaticDimension
   // }
    
}

