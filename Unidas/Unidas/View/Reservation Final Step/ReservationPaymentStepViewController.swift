//
//  ReservationPaymentSetpViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/05/18.
//  Copyright © 2018 Unidas. All rights reserved.
//

import UIKit
import SafariServices

protocol ReservationPaymentStepViewControllerDelegate {
    func redirectPaymentToReservationsTab()
}

class ReservationPaymentStepViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, ReservationPaymentStepViewDelegate, CreditCardWidgetViewDelegate, ListCardDelegate, CreateCardDelegate, UserFormDelegate, SFSafariViewControllerDelegate, UITextViewDelegate, UICollectionViewDelegateFlowLayout, CancellationNoticeDelegate, OpenSafatiFromContractURLDelegate, SetAirLineInformationDelegate, MultipleChoiceAlertDelegate, DocumentRecomendationsDelegate{
    
    // MARK: - Instance variables
    
    // MARK: Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var paymentCollectionView: UICollectionView!
    @IBOutlet weak var termsTableView: UITableView!
    @IBOutlet weak var tableViewConstraint: NSLayoutConstraint!

    @IBOutlet weak var preAuthorizationExpressView: UIView!
    @IBOutlet weak var preAuthorizationExpressValueLabel: UILabel!
    
    @IBOutlet weak var creditCardWidgetView: CreditCardWidgetView!
    @IBOutlet weak var loginCalloutView: UIView!
    @IBOutlet weak var createAccountCalloutView: UIView!
    @IBOutlet weak var userFormView: UserFormView!
    @IBOutlet weak var userFormDividerView: UIView!
    @IBOutlet weak var refundNoticeView: UIView!
    @IBOutlet weak var finishReservationArea: UIView!
    @IBOutlet weak var finishReservationButton: UIButton!
    
    @IBOutlet weak var refundPolicyTextView: UITextView!
    @IBOutlet weak var collectionViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var cancellationNotice: CancellationNotice!
    @IBOutlet weak var termsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerTopViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeightInsideConstant: NSLayoutConstraint!
    @IBOutlet weak var headerLineSpace: UIView!
    
    var delegate: ReservationPaymentStepViewControllerDelegate?
    
    var rentalRequirementsUrl = ""
    var contractConditionsUrl = ""
    var politicRefundUrl = ""
    var flightCompanies: [FlightCompany] = []
    var selectedIndexPaymentCollectionView = 0
    var isCiaAereaChecked: Bool = false
    var offerNotification = false
    var preAuthorization: Float?
    var expressPayment = false
    
    internal var selectedIndexPath: IndexPath? {
        didSet{
            self.termsTableView.beginUpdates()
            self.termsTableView.endUpdates()
        }
    }
    
    // MARK: Lazy initialization
    lazy var presenter = { () -> ReservationPaymentStepPresenterDelegate in
        let presenter = ReservationPaymentStepPresenter(reservationService: ReservationService(),
                                                        reservationDataStore: ReservationDataStoreUserDefaults(),
                                                        paymentService: PaymentService(),
                                                        companyTermsService: CompanyTermsService(),
                                                        flightInformationService: FlightInformationService(), unidasGarageService: UnidasGarageService(),
                                                        firebaseAnalyticsService: FirebaseAnalyticsService(),
                                                        adjustAnalyticsService: AdjustAnalyticsService())
        presenter.delegate = self
        return presenter
    }()
    
    lazy var keyboardScrollViewHandler = KeyboardScrollViewHandler(scrollView: scrollView)
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    lazy var flightCompanyPickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.sizeToFit()
        return pickerView
    }()
    
    lazy var flightCompanyToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissFlightComanyTextField(_:)))
        toolbar.setItems([flexibleSpaceItem, doneButtonItem], animated: true)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    // MARK: Variables
    
    weak var flightCompanyTextField: UITextField?
    
    var reservation: ReservationDataView?
    var oldReservation: VehicleReservationsDataView?
    var isAntecipatedPayment = false
    var isDetailScreen = false
    var voucherCode: String?
    var quotationDataView: QuotationsDataView?
    
    @IBOutlet weak var dottedView: UIView!
    
    // MARK: Reservation payment step view delegate variables
    
    var userFormData: UserFormDataView? {
        if userFormView.isUserValid() {
            return userFormView.userFormDataView
        }
        return nil
    }
    
    var creditCardReservePaymentDataView: CreditCardReservePaymentDataView? {
        return creditCardWidgetView.creditCardView.creditCardReservePaymentDataView
    }
    
    var isRefundPoliceChecked: Bool {
        guard let indexPathsForSelectedRows = termsTableView.indexPathsForSelectedRows else { return false }
        let refundPoliceCheckIndexPath = IndexPath(row: 0, section: 0)
        return indexPathsForSelectedRows.contains(refundPoliceCheckIndexPath)
    }
    
    var isComingByAirlineChecked: Bool {
        guard let indexPathsForSelectedRows = termsTableView.indexPathsForSelectedRows else { return false }
        let comingByAirlineIndexPath = IndexPath(row: 0, section: 1)
        return indexPathsForSelectedRows.contains(comingByAirlineIndexPath)
    }
    
    var isCiaAereaDataEmpty: Bool {
        
        guard let index = selectedIndexPath else {return true}
        
        if let cell = termsTableView.cellForRow(at: index) as? CheckmarkTableViewCell {
            if cell.cellNVoo.text == "" {
                cell.cellNVoo.changeColorBorder(isOn: true)
            }else{
                cell.cellNVoo.changeColorBorder(isOn: false)
            }
            if cell.cellCiaAerea.text == "" {
                cell.cellCiaAerea.changeColorBorder(isOn: true)
            }else{
                cell.cellCiaAerea.changeColorBorder(isOn: false)
            }
            
            if cell.cellCiaAerea.text == "" || cell.cellNVoo.text == "" {
                return true
            }else{
                return false
            }
        }
        return true
    }
    
    func checkGaragePickupAsAirport() -> Bool {
        if let garage = reservation?.pickUp?.garage {
            let type = garage.model.type
            if type == .airport {
                return true
            }
        }
        return false
    }
    
    var isRentalRequirementsAndContractualConditionsChecked: Bool {
        guard let indexPathsForSelectedRows = termsTableView.indexPathsForSelectedRows else { return false }
        let rentalRequirementsAndContractualConditions = IndexPath(row: 0, section: 2)
        return indexPathsForSelectedRows.contains(rentalRequirementsAndContractualConditions)
    }
    
    var isReceivePromotionalEmailChecked: Bool {
        guard let indexPathsForSelectedRows = termsTableView.indexPathsForSelectedRows else { return false }
        let receivePromotionalEmailIndexPath = IndexPath(row: 1, section: 2)
        return indexPathsForSelectedRows.contains(receivePromotionalEmailIndexPath)
    }
    
    var selectedPaymentOptionIndex: Int {
        return paymentCollectionView.indexPathsForSelectedItems?.first?.row ?? 0
    }
    
    var isVoucherApplied: Bool {
        if let voucher = voucherCode, !voucher.isEmpty {
            return true
        }
        return false
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingScreen.setLoadingScreen(view: self.view)
        //isAntecipatedPayment = reservation?.model.selectedPaymentOption == .payInAdvance ? true : false
        presenter.viewDidLoad(reservation: reservation, oldReservation: oldReservation, isAntecipatedPayment: isAntecipatedPayment)
        registerForKeyboardNotifications()
        setupExpressPayment()
        setupNavigation()
        setupCollectionView()
        setupTableView()
        setupFinishReservationButton()
        setupUserForm()
        setupCardWidget()
        setupChange()
        checkForMaintenanceScreen()
        configureBackButton()
        registerFacebookEvent()
        setupPreAuthorizationExpress()
        cancellationNotice.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterForKeyboardNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateTableViewHeight()
    }
    
    // MARK: - Setup
    
    func openContractUrl(url: URL) {
        safariUrlOpen(withUrl: url)
    }
    
    func callTerms() {
        self.presenter.fetchRentalRequirements()
        self.presenter.fetchContractConditions()
    }
    
    func setupUserForm() {
        userFormView.delegate = self
        userFormView.showsBirthdayTextField = false
        userFormView.showsMotherNameTextField = false
    }
    
    func setupNavigation() {
        setupStep()
        let activityIndicatorButtonItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.setRightBarButton(activityIndicatorButtonItem, animated: false)
    }
    
    func setupCollectionView() {
        paymentCollectionView.collectionViewLayout = PaymentOptionsCollectionViewLayout()
        paymentCollectionView.register(cellType: PaymentOptionCollectionViewCell.self)
        paymentCollectionView.allowsMultipleSelection = false
        paymentCollectionView.isScrollEnabled = false
    }
    
    func setupExpressPayment() {
        if reservation?.express == true || oldReservation?.express == true {
            presenter.setupPaymentExpressToggle()
        }
    }
    
    func showExpressPayment(value: Bool) {
        expressPayment = value
        paymentCollectionView.reloadData()
        updatePaymentCollection(with: 224)
    }
    
    func registerFacebookEvent() {
        if reservation != nil {
            presenter.logInitiateCheckoutEvent(reservation: reservation!, reserveNumber: reservation?.number, quotationDataView: quotationDataView)
        }else{
            let r = ReservationModelObject.buildReservation(oldReservation: oldReservation!)
            let r2 = ReservationDataView(model: r)
            presenter.logInitiateCheckoutEvent(reservation: r2, reserveNumber: r2.number, quotationDataView: quotationDataView)
        }
    }
    
    func setupTableView() {
        termsTableView.register(cellType: CheckmarkTableViewCell.self)
        termsTableView.separatorStyle = .none
    }
    
    func setupCardWidget() {
        let value = reservation?.express == true ? presenter.totalValue : presenter.prePaymentValue
        creditCardWidgetView.set(value: value)
        creditCardWidgetView.delegate = self
    }
    
    func setupPreAuthorizationExpress() {
        guard let amountString = NumberFormatter.currencyFormatter.string(from: NSNumber(value: preAuthorization ?? 0)) else {return}
        preAuthorizationExpressValueLabel.text = amountString
    }
    
    func setupFinishReservationButton() {
        finishReservationButton.setTitleColor(.white, for: .normal)
        finishReservationButton.setTitleColor(.white, for: .disabled)
        finishReservationButton.setBackgroundColor(color: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), forState: .normal)
        finishReservationButton.setBackgroundColor(color: #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), forState: .disabled)
    }
    
    func updateTableViewHeight() {
        DispatchQueue.main.async {
            switch self.selectedIndexPaymentCollectionView {
            case 0:
                let hideAirCia = self.checkGaragePickupAsAirport() == true ? 272 : 204
                self.termsTableViewHeightConstraint.constant = CGFloat(hideAirCia)
            case 1:
                let hideAirCia = self.checkGaragePickupAsAirport() == true ? 204 : 136
                self.termsTableViewHeightConstraint.constant = CGFloat(hideAirCia)
            default:
                break
            }
            if self.presenter.isFranchise {
                self.termsTableViewHeightConstraint.constant -= 68
            }
            if !self.presenter.isFranchise {
                if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
                    if pendencies {
                        self.termsTableViewHeightConstraint.constant -= 68
                    }
                }
            }
            //if self.termsTableView.contentSize.height != self.tableViewConstraint.constant {
            //self.tableViewConstraint.constant = self.termsTableView.contentSize.height
            //}
        }
    }
    
    func setupChange(){
        let loguedUser = session.currentUser != nil ? true : false
        
        if oldReservation == nil && loguedUser {
            finishReservationButton.setTitle(NSLocalizedString("Finalize reservation", comment: ""), for: .normal)
        }else if oldReservation == nil && !loguedUser {
            finishReservationButton.setTitle(NSLocalizedString("Complete reservation", comment: ""), for: .normal)
        }else if oldReservation != nil && loguedUser && isAntecipatedPayment == true {
            headerTopViewHeightConstraint.constant = 0
            setHeaderUnidas(barTintColor: #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
            collectionViewHeightConstant.constant -= 20
            headerLineSpace.isHidden = false
            finishReservationButton.setTitle(NSLocalizedString("Pay Reservation", comment: ""), for: .normal)
        }else if oldReservation != nil && loguedUser && isAntecipatedPayment == false {
            finishReservationButton.setTitle(NSLocalizedString("Change Reservation", comment: ""), for: .normal)
        }
        
        if presenter.isFranchise {
            collectionViewHeightConstant.constant -= 15
        }
        
        if !presenter.isFranchise {
            if let pendencies = AppPersistence.getValueBool(withKey: AppPersistence.pendencies) {
                if pendencies {
                    collectionViewHeightConstant.constant -= 15
                }
            }
        }
    }
    
    //MARK: - Maintenance Toggle
    
    func checkForMaintenanceScreen() {
        FirebaseRealtimeDatabase.toggleListenerValue(withKey: .maintenance) { (toggle) in
            if toggle {
                if let navigation = self.navigationController {
                    MaintenanceInvoker.presenterMaintenanceController(navigation: navigation)
                }
            }      
        }
    }
    
    // MARK: - Interface builder actions
    
    @IBAction func makeReservationButtonTapped(_ sender: UIButton) {
        
        presenter.makeReservation(voucher: voucherCode)
        
//        if offerNotification == false {
//            showNotificationsOffers()
//        }else{
//            presenter.makeReservation(voucher: voucherCode)
//        }
    }
    
    @objc func flightNumberTextFieldValueChanged(_ sender: UITextField) {
        if let text = sender.text, text.count > 6 {
            sender.text = String(text[text.startIndex..<text.index(text.startIndex, offsetBy: 6)])
        }
    }
    
    @objc func dismissFlightComanyTextField(_ sender: Any) {
        flightCompanyTextField?.resignFirstResponder()
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSuccessReservationView" {
            let reservation = presenter.reservationDataView
            let user = presenter.userDataView
            let destination = segue.destination as? ReservationSuccessViewController
            destination?.reservation = reservation
            destination?.user = user
            destination?.isAntecipatedPayment = isAntecipatedPayment
            destination?.isDetailsScreen = isDetailScreen
            destination?.preAuthorization = preAuthorization
            if presenter.showsCreditCardWidget {
                destination?.creditCard = creditCardWidgetView.creditCardView.creditCardReservePaymentDataView
            }
        }
        
        if segue.identifier == "showUpdateDocument" {
            navigationController?.navigationBar.subviews.first(where: { $0 is StepView })?.isHidden = true
            let destination = segue.destination as? DocumentRecomendationsViewController
            destination?.delegate = self
            destination?.headerCurved = true
            destination?.formsAction = .update
        }
    }
    
    // MARK: - Reservation payment step view delegate
    
    func fetchNotificationStatus(status: Bool) {
         offerNotification = status
     }
    
    func showNotificationsOffers() {
        let title = NSLocalizedString("Notification Offer Title", comment: "")
        let subTitle = NSLocalizedString("Notification Offer Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("Notification Offer No", comment: "")
        let secondChoiceLabel = NSLocalizedString("Notification Offer Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-config-notification"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 1, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
     }
    
    func selectPaymentOption(at index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        paymentCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: .top)
    }
    
    func updatePaymentCollection(with height: Int) {
        collectionViewHeightConstant.constant = CGFloat(height)
    }
    
    func updateView() {
        let loguedUser = session.currentUser != nil ? true : false
        loginCalloutView.isHidden = loguedUser
        //loginCalloutView.isHidden = !presenter.showsLoginCallout
        preAuthorizationExpressView.isHidden = !presenter.isExpress
        dottedView.isHidden = loguedUser
        creditCardWidgetView.isHidden = !presenter.showsCreditCardWidget
        createAccountCalloutView.isHidden = !presenter.showsCreateAccountCallout
        userFormView.isHidden = !presenter.showsUserForm
        //userFormDividerView.isHidden = !presenter.showsUserForm
        refundNoticeView.isHidden = !presenter.showsRefundNotice
        termsTableView.isHidden = !presenter.showsTermsAndAgreementsView
        finishReservationArea.isHidden = !presenter.showsFinishReservationButton
        termsTableViewReloadData()
        updateTableViewHeight()
        setupChange()
        enableFinishButtonForExpress()
        
    }
    
    func showAirlineForm() {
        let alert = UIAlertController(title: NSLocalizedString("Flight Information", comment: "Fight Information alert title"), message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = NSLocalizedString("Flight number", comment: "Flight number placeholder text")
            textField.keyboardType = .numberPad
            textField.addTarget(self, action: #selector(self.flightNumberTextFieldValueChanged(_:)), for: .editingChanged)
            textField.tag = 1
        }
        alert.addTextField { textField in
            textField.placeholder = NSLocalizedString("Company", comment: "Flight company placeholder text")
            textField.autocapitalizationType = .words
            textField.tag = 2
            textField.inputView = self.flightCompanyPickerView
            textField.inputAccessoryView = self.flightCompanyToolbar
            textField.text = self.presenter.flightCompanyName(at: 0)
            
            self.flightCompanyTextField = textField
        }
        alert.addAction(.cancel({ [weak self] (alertAction) in
            self?.flightCompanyTextField = nil
            self?.termsTableView.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .fade)
        }))
        alert.addAction(.ok({ [weak self] alertAction in
            self?.flightCompanyTextField = nil
            guard let textFields = alert.textFields, let flightNumber = textFields.first(where: { $0.tag == 1 })?.text, let row = self?.flightCompanyPickerView.selectedRow(inComponent: 0) else { return }
            self?.presenter.setAirlineInformation(flightNumber: flightNumber, companyAt: row)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func updateAirlineInformation() {
        let indexPath = IndexPath(row: 0, section: 1)
        termsTableView.reloadRows(at: [indexPath], with: .fade)
        if presenter.getAirlineInformation() != nil {
            termsTableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        updateTableViewHeight()
    }
    
    func showFlightCompanies(flightCompany: [FlightCompany]) {
        flightCompanies = flightCompany
        callTerms()
    }
    
    func setAirLineInformation(flightNumber: String, flightCompany: FlightCompany) {
        presenter.setNewAirlineInformation(flightNumber: flightNumber, flightCompany: flightCompany)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func termsTableViewReloadData() {
        termsTableView.reloadData()
    }
    
    func showUpdateDriverLicence(message: String?) {
        let messageUpdateDriverLicence = NSLocalizedString("Update Driver Licence", comment: "Update Driver Licence Description")
        let messageNotUpdateDriverLicence = NSLocalizedString("Dont Update Driver Licence", comment: "Dont Update Driver Licence Description")
        let title = NSLocalizedString("Update Driver Licence Title", comment: "")
        let image = #imageLiteral(resourceName: "icon-driverlicense-signup")
        let multipleAlert = MultipleChoiceAlert(title: title, subTitle: message ?? "", firstChoiceButtonLabel: messageUpdateDriverLicence, secondChoiceButtonLabel: messageNotUpdateDriverLicence, image: image, firstChoiceButtonColor: UIColor.themeImportantButton)
        multipleAlert.delegate = self
        multipleAlert.show(animated: true)
    }
    
    func enableFinishButtonForExpress() {
        if !expressPayment && reservation?.express == true {
            finishReservationButton.isUserInteractionEnabled = expressPayment
            finishReservationButton.setTitleColor(.white, for: .normal)
            finishReservationButton.setTitleColor(.white, for: .disabled)
            finishReservationButton.setBackgroundColor(color:#colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), forState: .normal)
            finishReservationButton.setBackgroundColor(color: #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), forState: .disabled)
        }
    }
    
    func startLoading() {
        termsTableView.allowsSelection = false
        userFormView.isUserInteractionEnabled = false
        creditCardWidgetView.isUserInteractionEnabled = false
        finishReservationButton.isEnabled = false
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        termsTableView.allowsMultipleSelection = true
        userFormView.isUserInteractionEnabled = true
        creditCardWidgetView.isUserInteractionEnabled = true
        finishReservationButton.isEnabled = true
        activityIndicatorView.stopAnimating()
    }
    
    func enableBackButton() {
        navigationItem.setLeftBarButton(nil, animated: true)
    }
    
    func disableBackButton() {
        let emptyBarButton = UIBarButtonItem(customView: UIView())
        navigationItem.setLeftBarButton(emptyBarButton, animated: true)
    }
    
    func showSuccessReservationView() {
        performSegue(withIdentifier: "showSuccessReservationView", sender: nil)
    }
    
    func setListCard(_ listCard: ListCardDataView) {
        creditCardWidgetView.set(card: listCard)
    }

    func showUpdateDocument() {
        performSegue(withIdentifier: "showUpdateDocument", sender: nil)
        setHeaderUnidas()
    }
    
    func showCardList() {
        let storyboard = UIStoryboard(name: "Card", bundle: .main)
        let navigation = storyboard.instantiateInitialViewController() as? UINavigationController
        let viewController = navigation?.viewControllers.first as? ListCardCollectionViewController
        viewController?.delegate = self
        guard let controller = navigation else { return }
        present(controller, animated: true, completion: nil)
    }
    
    func showCreateCard() {
        let storyboard = UIStoryboard(name: "Card", bundle: .main)
        let navigation = storyboard.instantiateViewController(withIdentifier: "newCardNavigation") as? UINavigationController
        let viewController = navigation?.viewControllers.first as? CreateCardViewController
        viewController?.delegate = self
        guard let controller = navigation else { return }
        present(controller, animated: true, completion: nil)
    }
    
    func updateFlightCompany(name: String) {
        flightCompanyTextField?.text = name
    }
    
    func showRentalRequirements(companyTermsDataView: CompanyTermsDataView) {
        guard let url = URL(string: companyTermsDataView.description) else { return }
        //safariUrlOpen(withUrl: url)
        rentalRequirementsUrl = "\(url)"
        updateView()
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func showContractConditions(companyTermsDataView: CompanyTermsDataView) {
        guard let url = URL(string: companyTermsDataView.description.trimmingCharacters(in: .whitespacesAndNewlines)) else { return }
        //safariUrlOpen(withUrl: url)
        contractConditionsUrl = "\(url)"
        updateView()
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func changeTableViewHeight(value: Bool) {
        let height = value == true ? 55 : -55
        UIView.animate(withDuration: 0.2) {
            self.termsTableViewHeightConstraint.constant += CGFloat(height)
        }
    }
    
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView) {
        guard let strURL = companyTermsDataView.description.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let politicRefund = URL(string: strURL) else { return }
        
        let attributedString = NSMutableAttributedString(string: NSLocalizedString("In case of cancellation, depending on how close the pickup date is, the refund value will be partial. Check the Refund Policy in case of cancellation", comment: "Refund policy localized"))
        let termsText = attributedString.string
        let accountTermsText = NSLocalizedString("Refund Policy", comment: "Refund Policy")
        
        politicRefundUrl = "\(politicRefund)"
        cancellationNotice.setUrl(urlPolicy: politicRefund)
        
        if let accountTermsRange = termsText.range(of: accountTermsText) {
            let accountTermsNSRange = NSRange(accountTermsRange, in: termsText)
            attributedString.addAttribute(.link, value: politicRefund, range: accountTermsNSRange)
        }
        
        if let fullRange = termsText.range(of: termsText) {
            let range = NSRange(fullRange, in: termsText)
            attributedString.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2901960784, green: 0.5568627451, blue: 0.6941176471, alpha: 1), range: range)
            attributedString.addAttribute(.font, value: UIFont.regular(ofSize: 14), range: range)
        }
        
        refundPolicyTextView.attributedText = attributedString
        refundPolicyTextView.textAlignment = .center
        refundPolicyTextView.delegate = self
        refundPolicyTextView.isEditable = false
        refundPolicyTextView.isSelectable = true
    }
    
    func showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView) {
        preAuthorizationExpressValueLabel.text = preAuthorizationDataView.value
    }
    
    // MARK: - Collection view
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfPaymentOptions
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 20, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: PaymentOptionCollectionViewCell.self)
        let paymentOption = presenter.paymentOption(at: indexPath.row)
        
        cell.setup(with: paymentOption)
        cell.paymentValueLabel?.text = presenter.price(for: paymentOption)
        
        if paymentOption.model == .payInAdvance && !presenter.payInAdvanceEnable {
            cell.isUserInteractionEnabled = false
            cell.alpha = 0.1
            cell.paymentValueLabel?.text = ""
            cell.paymentNameLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            cell.paymentDescriptionLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            cell.roundedCheckImageView.image = nil
            collectionView.isUserInteractionEnabled = false
        }
        
        if paymentOption.model == .payAtPickup && !presenter.payAtPickupEnable {
            cell.isUserInteractionEnabled = false
            cell.alpha = 0.1
            cell.paymentValueLabel?.text = ""
            cell.paymentNameLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            cell.paymentDescriptionLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            cell.roundedCheckImageView.image = nil
            collectionView.isUserInteractionEnabled = false
        }
        
        if paymentOption.model == .payAtPickup && reservation == nil {
            cell.isUserInteractionEnabled = false
            cell.alpha = 0.1
            cell.paymentValueLabel?.text = ""
            cell.paymentNameLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            cell.paymentDescriptionLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
        }
        
        if isAntecipatedPayment {
            cell.paymentDescriptionLabel.isHidden = true
            cell.frame.size.height = 70
            
            if oldReservation?.express == true {
                collectionViewHeightConstant.constant -= 80
                cell.paymentValueLabel.text = oldReservation?.totalValueString
                cell.paymentDescriptionLabel.text = NSLocalizedString("Unidas Express Payment Description", comment: "")
                cell.paymentDescriptionLabel.isHidden = false
                cell.selectorBackView.isHidden = true
                cell.isSelected = true
                cell.frame.size.height = 100
                                
                if !expressPayment {
                    cell.isUserInteractionEnabled = false
                    cell.alpha = 0.1
                    cell.paymentValueLabel?.text = ""
                    cell.paymentDescriptionLabel?.text = NSLocalizedString("Payment not available at this time for Unidas Express", comment: "Payment description label")
                    cell.paymentNameLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
                    cell.paymentDescriptionLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
                }
            }            
        }
        
        if presenter.isExpress && !isAntecipatedPayment {
            collectionViewHeightConstant.constant -= 100
            cell.paymentValueLabel.text = reservation?.totalValue
            cell.paymentDescriptionLabel.text = NSLocalizedString("Unidas Express Payment Description", comment: "")
            cell.paymentDescriptionLabel.isHidden = false
            cell.selectorBackView.isHidden = true
            cell.isSelected = true
            cell.frame.size.height = 100
            
            if !expressPayment {
                cell.isUserInteractionEnabled = false
                cell.alpha = 0.1
                cell.paymentValueLabel?.text = ""
                cell.paymentDescriptionLabel?.text = NSLocalizedString("Payment not available at this time for Unidas Express", comment: "Payment description label")
                cell.paymentNameLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
                cell.paymentDescriptionLabel?.textColor = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
            }
        }
        
        if paymentOption.model == .payAtPickup && presenter.isFranchise {
            cell.paymentDescriptionLabel?.text = presenter.franchisePayAtPickupDescription
            cell.frame.size.height = 70
        }
        
        if paymentOption.model == .payInAdvance && isVoucherApplied {
            cell.paymentDescriptionLabel?.text = NSLocalizedString("By paying now you'll receive a\ndiscount of %@ with voucher applied", comment: "Pre-payment description label")
        }
        
        if presenter.numberOfPaymentOptions == 1 && !presenter.isExpress {
            cell.isSelected = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.row == 0 ? 0 : 1
        
        if index != selectedIndexPaymentCollectionView {
            
            if session.currentUser == nil {
                switch indexPath.row {
                case 0:
                    loginCalloutView.isHidden = !presenter.showsLoginCallout
                    setupChange()
                case 1:
                    loginCalloutView.isHidden = presenter.showsLoginCallout
                    setupChange()
                default:
                    break
                }
            }else{
                loginCalloutView.isHidden = !presenter.showsLoginCallout
                setupChange()
            }
            presenter.didSelectPaymentOption(at: indexPath.row)
            
            let textFields = getTextfield(view: self.view)
            textFields.forEach {
                $0.resignFirstResponder()
            }
            
            self.unselectTableViewRow()
        }
        selectedIndexPaymentCollectionView = index
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0 where presenter.showsRefundPolicyCheck: return 1
        case 1:  if !checkGaragePickupAsAirport() { return 0 } else { return 1 }
        case 2: return 2
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CheckmarkTableViewCell.self)
        cell.airLineDelegate = self
        cell.flightCompanies = flightCompanies
        cell.cellNVoo.isHidden = true
        cell.cellCiaAerea.isHidden = true
        
        if indexPath.section == 0 {
            cell.cellDescription.text = NSLocalizedString("I declare that I have read the Refund Policy in case of cancellation", comment: "")
            //cell.accessoryType = .none
            let url = URL(string: politicRefundUrl)            
            cell.delegate = self
            cell.setUrl(urlPolicy: url, urlPolicy2: nil)
        } else if indexPath.section == 1 {
            cell.cellDescription.text = NSLocalizedString("Are you coming by Airline?", comment: "")
            //cell.detailTextLabel?.text = presenter.getAirlineInformation()?.description
            //cell.accessoryType = .none
            cell.delegate = self
            cell.setUrl(urlPolicy: nil, urlPolicy2: nil)
            
        } else if indexPath.section == 2 {
            switch indexPath.row {
            case 0:
                cell.cellDescription.text = NSLocalizedString("I am aware and in accordance with the Rental Requirements and the Contractual Conditions", comment: "")
                let url = URL(string: contractConditionsUrl)
                let url2 = URL(string: rentalRequirementsUrl)
                cell.delegate = self
                cell.setUrl(urlPolicy: url, urlPolicy2: url2)
                //cell.accessoryType = .detailButton
                break
            case 1:
                cell.cellDescription.text = NSLocalizedString("I agree to receive communications with news and promotions about Unidas by email and SMS", comment: "")
                cell.delegate = self
                cell.setUrl(urlPolicy: nil, urlPolicy2: nil)
                //cell.accessoryType = .none
                break
            default: break
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Rental Requirements", comment: "Rental Requirements title"), style: .default , handler:{ (result) in
            self.presenter.fetchRentalRequirements()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Contract Conditions", comment: "Contract Conditions title"), style: .default , handler:{ (result) in
            self.presenter.fetchContractConditions()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel alert action"), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath == self.selectedIndexPath {
                self.selectedIndexPath = nil
                isCiaAereaChecked = false
            }else{
                self.selectedIndexPath = indexPath
                if let cell = termsTableView.cellForRow(at: indexPath) as? CheckmarkTableViewCell {
                    cell.cellNVoo.isHidden = false
                    cell.cellCiaAerea.isHidden = false
                    isCiaAereaChecked = true
                }
            }
            //presenter.didSelectComingByAirline()
        }else{
            if let cell = termsTableView.cellForRow(at: indexPath) as? CheckmarkTableViewCell {
                cell.cellNVoo.isHidden = true
                cell.cellCiaAerea.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath == self.selectedIndexPath {
                self.selectedIndexPath = nil
                if let cell = termsTableView.cellForRow(at: indexPath) as? CheckmarkTableViewCell {
                    cell.cellNVoo.changeColorBorder(isOn: false)
                    cell.cellCiaAerea.changeColorBorder(isOn: false)
                    isCiaAereaChecked = false
                }
            }else{
                self.selectedIndexPath = indexPath
            }
            //presenter.didDeselectComingByAirline()
        }
        if let cell = termsTableView.cellForRow(at: indexPath) as? CheckmarkTableViewCell {
            cell.cellNVoo.isHidden = true
            cell.cellCiaAerea.isHidden = true
        }
    }
    
    //    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        if section == 0 && presenter.showsRefundPolicyCheck || section == 1 {
    //            //let view = UIView()
    //            //view.backgroundColor = UIColor.groupTableViewBackground
    //            return nil
    //        }
    //        return nil
    //    }
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        switch indexPath.section {
    //        case 0,2:
    //            return 68
    //        case 1:
    //            return 123
    //        default:
    //            return 68
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0,2:
            return 68
        case 1:
            if indexPath != self.selectedIndexPath {
                changeTableViewHeight(value: false)
                return 68
            }else{
                changeTableViewHeight(value: true)
                return 123
            }
        default:
            return 68
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0 where presenter.showsRefundPolicyCheck: return 0.0
        case 1: return 0.0
        default: return 0.0
        }
    }
    
    func unselectTableViewRow() {
        if session.currentUser != nil {
            self.selectedIndexPath = nil
            let rowToSelect = NSIndexPath(row: 0, section: 1)
            if let cell = termsTableView.cellForRow(at: rowToSelect as IndexPath) as? CheckmarkTableViewCell {
                cell.cellNVoo.text = ""
                cell.cellCiaAerea.text = ""
                //cell.cellNVoo.resignFirstResponder()
            }
            self.termsTableView.deselectRow(at: rowToSelect as IndexPath, animated: true)
            self.termsTableViewReloadData()
        }else{
            if selectedIndexPath != nil {
                self.selectedIndexPath = nil
                let rowToSelect = NSIndexPath(row: 0, section: 1)
                if let cell = termsTableView.cellForRow(at: rowToSelect as IndexPath) as? CheckmarkTableViewCell {
                    cell.cellNVoo.text = ""
                    cell.cellCiaAerea.text = ""
                    //cell.cellNVoo.resignFirstResponder()
                }
                self.termsTableView.deselectRow(at: rowToSelect as IndexPath, animated: true)
                
            }
            self.termsTableViewReloadData()
        }
    }
    
    func getTextfield(view: UIView) -> [UITextField] {
        var results = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let textField = subview as? UITextField {
                results += [textField]
            } else {
                results += getTextfield(view: subview)
            }
        }
        return results
    }
    
    // MARK: - Credit card widget view delegate
    
    func creditCardWidgetDidTapChangeCardButton(_ creditCardWidget: CreditCardWidgetView) {
        presenter.creditCardWidgetDidTapChangeCardButton()
    }
    
    // MARK: - List card delegate
    
    func selectedCard(at listCardDataView: ListCardDataView) {
        setListCard(listCardDataView)
    }
    
    // MARK: - Create card delegate
    
    func cardCreated() {
        presenter.fetchCards()
    }
    
    // MARK: - Keyboard events
    
    func registerForKeyboardNotifications() {
        keyboardScrollViewHandler.registerForKeyboardNotifications()
    }
    
    func unregisterForKeyboardNotifications() {
        keyboardScrollViewHandler.unregisterForKeyboardNotifications()
    }
    
    // MARK: - UserFormDelegate
    
    func present(error: Error, userFormView: UserFormView) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    // MARK: - Picker view data source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.numberOfFlightCompanies
    }
    
    // MARK: - Picker view delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.flightCompanyName(at: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectFlightCompany(at: row)
    }
    
    // MARK: - TextView delegate
    
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        safariUrlOpen(withUrl: url)
        return false
    }
    
    // MARK: - SafariView delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        if tag == 1 {
            self.presenter.makeReservation(voucher: self.voucherCode)
        }else{
          showUpdateDocument()
        }
    }
    
    func choiceSecondTap(tag: Int) {
        if tag == 2 {
            self.presenter.updateNotifications(id: 2, active: true, voucher: self.voucherCode)
        }
    }
    
    //MARK: - DocumentRecomendationsDelegate
    func finishUpdateDocumentDriver() {
        if let stepView = navigationController?.navigationBar.subviews.first(where: { $0 is StepView }) as? StepView {
            stepView.isHidden = false
        }
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
    }
    
    //MARK: - Private Functions
    private func safariUrlOpen(withUrl url: URL){
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.delegate = self
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    private func setupStep(){
        if let stepView = navigationController?.navigationBar.subviews.first(where: { $0 is StepView }) as? StepView {
            let height = stepView.frame.height
            let edgeInsets = UIEdgeInsets(top: height,
                                          left: scrollView.contentInset.left,
                                          bottom: scrollView.contentInset.bottom,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
            stepView.setupStep(total: 4, selected: 3)
        }
    }
    
    // MARK: - CancellationNoticeDelegate
    func openUrl(with url: URL) {
        safariUrlOpen(withUrl: url)
    }
    
    //MARK: - Navigation
    @IBAction func unwindToFour(_ sender: UIStoryboardSegue) {
        delegate?.redirectPaymentToReservationsTab()
    }
    
}
