//
//  ForgotValidationViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 20/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class ForgotValidationViewController: UIViewController, PINViewControllerDelegate, UITextFieldDelegate, ForgotValidationViewDelegate {
  
    var pinViewController: PINViewController?
    var userResponseDataView: UserResponseDataView!
    var unidasToken: UnidasToken!
    var accountMethodType: AccountRecoveryMethod!
    var infoSmsEmail: String = ""
    var smsArray = [String]()
    
    private var presenter: ForgotValidationPresenter!
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet var smsTextFields: [UITextField] = []
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var resendSMSLabel: UILabel!
    @IBOutlet weak var resendEmailLabel: UILabel!
    
    lazy var numberAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(keyBoardDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ForgotValidationPresenter(recoveryService: RecoveryService(), userResponseDataView: userResponseDataView, accountsService: AccountsService(), authUnidasService: AuthUnidasService(), userCheckService: UserCheckService(), codeValidationService: CodeValidationService(), unidasToken: unidasToken)
        
        setupDelegates()
        setupView()
        setupLabels()
        addKeyboardToolbar()
        configureBackButton()
    }
    
    private func setupView() {
        if accountMethodType == .sms {
            let localizedString = NSLocalizedString("Forgot Password Instruction SMS", comment: "")
            let instructions = String(format: localizedString, infoSmsEmail)
            instructionsLabel.text = instructions
            titleLabel.text = NSLocalizedString("Forgot Password Check SMS", comment: "") 
        } else if accountMethodType == .mail {
            let localizedString = NSLocalizedString("Forgot Password Instruction Email", comment: "")
            let instructions = String(format: localizedString, infoSmsEmail)
            instructionsLabel.text = instructions
            titleLabel.text = NSLocalizedString("Forgot Password Check Email", comment: "")
        }
    }
    
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupLabels(){
        let font = UIFont.regular(ofSize: 14)
        let boldFont = UIFont.medium(ofSize: 14)
        
        let attrsNormalFont = [NSAttributedString.Key.font : font]
        let attrsBoldFont = [NSAttributedString.Key.font : boldFont]
        
        let email = "e-mail"
        let sms = "SMS"
        let resendText = NSLocalizedString("Forgot Password - Resend by Type", comment: "")
        
        let attributedEmailString = NSMutableAttributedString(string: resendText, attributes: attrsNormalFont)
        let attributedBoldEmailString = NSMutableAttributedString(string: email, attributes: attrsBoldFont)
        
        let attributedSMSString = NSMutableAttributedString(string: resendText, attributes: attrsNormalFont)
        let attributedBoldSMSString = NSMutableAttributedString(string: sms, attributes: attrsBoldFont)
        
        attributedEmailString.append(attributedBoldEmailString)
        attributedSMSString.append(attributedBoldSMSString)
        
        resendSMSLabel.attributedText = attributedSMSString
        resendEmailLabel.attributedText = attributedEmailString
    }
    
    private func setupDelegates() {
        codeField.delegate = self
        smsTextFields.forEach( { $0.delegate = self } )
        
        presenter.delegate = self
    }
    
    func changeSMSBorderColors(value: Bool) {
        if value {
            smsTextFields.forEach {
                $0.layer.borderWidth = 1
                $0.layer.borderColor = UIColor.red.cgColor
            }
        }else{
            smsTextFields.forEach {
                $0.layer.borderWidth = 0
            }
        }
    }
    
    func addKeyboardToolbar()  {
        smsTextFields.forEach {
            $0.inputAccessoryView = numberAccessoryView
        }
    }
    
    func presenteCodeError() {
        changeSMSBorderColors(value: true)
    }
    
    private func fillCodeField() {
        var code = ""
        smsTextFields.forEach { (textField) in
            code += textField.text ?? ""
        }
        codeField?.text = code
    }
    
    @objc func keyBoardDoneButtonTapped(sender: UIBarButtonItem) {
        view.endEditing(true)
    }
    
    @IBAction func resendCode(_ sender: UIButton) {
        guard let recoveryMethod = AccountRecoveryMethod(rawValue: sender.tag) else { return }
        presenter.resendCode(recoveryMethod: recoveryMethod)
        
    }
    
    //MARK: - ForgotValidationViewDelegate
    func pinEndLoading() {
        pinViewController?.endLoading()
    }
    
    func pinPresent(error: Error) {
        pinViewController?.present(error: error, callbackDidFailFinishPin: true)
    }
    
    func startLoading() {
        //TODO - bloquear a tela
    }
    
    func endLoading() {
        
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }

    func requestNewPin() {
        performSegue(withIdentifier: "segueToPin", sender: nil)
    }
    
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController) {
        pinViewController.showNewPinController(title: title, isHiddenForgotPin: isHiddenForgotPin, isHiddenCancelButton: isHiddenCancelButton, userResponseDataView: userResponseDataView)
    }
    
    func didPinEndLoading(pinViewController: PINViewController) {
        pinViewController.endLoading()
    }
    
    func didFailConfirmPin(pinViewController: PINViewController, error: Error, callbackDidFailFinishPin: Bool) {
        pinViewController.present(error: error, callbackDidFailFinishPin: false)
    }
    
    func didFinishUpdatePin(pinViewController: PINViewController) {
        performSegue(withIdentifier: "showFinishChange", sender: nil)
    }
    
    //MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 7 {
            smsArray.append(string)
            if smsArray.count == 8 {
                orderSmsCode()
            }
        }
        
        if (1...6).contains(textField.tag) {
            let finalString: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            if finalString!.count > 0 {
                textField.text = string
                
                let nextResponder: UIResponder? = textField.superview?.viewWithTag(textField.tag + 1)
                if nextResponder != nil {
                    fillCodeField()
                    nextResponder?.becomeFirstResponder()
                    return true
                }else if textField.tag == 6 {
                    fillCodeField()
                    presenter.postForgotPin(code: codeField.text)
                    return false
                }else{
                    fillCodeField()
                }
            }else{
                textField.text = string
                if textField.tag == 1 {
                    fillCodeField()
                }else{
                    let nextResponder: UIResponder? = textField.superview?.viewWithTag(textField.tag - 1)
                    fillCodeField()
                    nextResponder?.becomeFirstResponder()
                    return false
                }
            }
        }
        return true
    }
    
    func orderSmsCode() {
        var index = 0
        smsArray.removeFirst(2)
    
        for item in smsArray {
            switch index {
            case 0:
                smsTextFields[0].text = item
            case 1:
                smsTextFields[1].text = item
            case 2:
                smsTextFields[2].text = item
            case 3:
                smsTextFields[3].text = item
            case 4:
                smsTextFields[4].text = item
            case 5:
                smsTextFields[5].text = item
                hideKeyboard()
            default:
                return
            }
            index += 1
        }
         presenter.postForgotPin(code: codeField.text)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: - PINViewControllerDelegate
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        presenter.didFinishPin(userResponseDataView: userResponseDataView, pin: pin, pinViewController: pinViewController)
    }
    
    func didFailFinishPin(error: Error, pinViewController: PINViewController) {
        pinViewController.present(error: error, callbackDidFailFinishPin: true)
    }
    
    func didTapCancelPin() {
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToPin" {
            let viewController = segue.destination as? PINViewController
            viewController?.navigationTitle = NSLocalizedString("Update your PIN", comment: "Update your PIN navigation title")
            viewController?.isHiddenCancelButton = false
            viewController?.isHiddenForgotPin = true
            viewController?.delegate = self
            viewController?.loginDismissDelegate = loginDismissDelegate
        }
        
        if segue.identifier == "showFinishChange" {
            let viewController = segue.destination as? ForgotPINSuccessViewController
            viewController?.loginDismissDelegate = loginDismissDelegate
        }
    }
}
