//
//  ForgotPIN2ViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 19/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import InputMask

protocol LoginDismissDelegate: class {
    func dismissLogin()
}

class ForgotPINViewController: UIViewController, ForgotPINViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var smsButton: EnhancedButton!
    @IBOutlet weak var emailButton: EnhancedButton!
    @IBOutlet var buttons: [EnhancedButton] = []
    @IBOutlet var buttonLabels: [UILabel] = []
    @IBOutlet weak var sendCodeButton: UnidasButton!
    @IBOutlet weak var smsLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    private var presenter: ForgotPINPresenter!
    private var mobilePhoneNumber = ""
    private var email = ""
    
    var userResponseDataView: UserResponseDataView!
    weak var delegate: LoginDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ForgotPINPresenter(recoveryService: RecoveryService(), userResponseDataView: userResponseDataView, userCheckService: UserCheckService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        presenter.viewDidLoad()
        presenter.addEventForgotPassword(success: true)
        setupView()
    }
    
    //override var preferredStatusBarStyle: UIStatusBarStyle {
        //return .lightContent
    //}
    
    @IBAction func touchSendCode(_ sender: UIButton) {
        presenter.sendForgotCode()
    }
    
    @IBAction func changedForgotMethod(_ sender: EnhancedButton) {
        buttons.forEach { $0.borderColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1) }
        buttonLabels.forEach { $0.textColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1) }
        
        sender.borderColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        sender.tag == 0 ?  (smsLabel.textColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)) : (emailLabel.textColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1))
        sendCodeButton.isEnabled = true
        sendCodeButton.isUserInteractionEnabled = true
        
        presenter.didChangeRecoveryMethod(to: sender.tag)
    }
    
    private func setupView() {
        setHeaderUnidas()
        sendCodeButton.isEnabled = false
        sendCodeButton.isUserInteractionEnabled = false
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back-button"), style: .done, target: self, action: #selector(touchCancel))
        navigationItem.leftBarButtonItem = backButton
        
    }
    
    // MARK: - Forgot Pin View Delegate
    func startLoading() {
        LoadingScreen.setLoadingScreen(view: self.view)
    }
    
    func endLoading() {
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func startButtonLoading() {
        sendCodeButton.isUserInteractionEnabled = false
        sendCodeButton.isEnabled = false
        sendCodeButton.loading(true)
    }
    
    func endButtonLoading() {
        sendCodeButton.isUserInteractionEnabled = true
        sendCodeButton.isEnabled = true
        sendCodeButton.loading(false)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showValidateCode() {
        performSegue(withIdentifier: "segueToValidateCode", sender: nil)
    }
    
    @objc func touchCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func updateInfoEmailAndSMS(userResponse: UserResponse) {
        let localizedSms =  NSLocalizedString("Forgot Password SMS @", comment: "")
        let localizedEmail = NSLocalizedString("Forgot Password E-mail @", comment: "")
        
        mobilePhoneNumber = userResponse.account.mobilePhone.formattedMobilePhoneMaskNumber()
        email = userResponse.account.email.formattedEmailMaskNumber()
        
        smsLabel.text = String(format: localizedSms, mobilePhoneNumber)
        emailLabel.text = String(format: localizedEmail, email)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToValidateCode"{
            let destination = segue.destination as? ForgotValidationViewController
            destination?.userResponseDataView = userResponseDataView
            destination?.unidasToken = presenter.unidasToken
            destination?.loginDismissDelegate = delegate
            
            let recoveryMethod = presenter.recoveryMethod
            destination?.accountMethodType = recoveryMethod
            
            if recoveryMethod == .sms {
                destination?.infoSmsEmail = mobilePhoneNumber
            } else if recoveryMethod == .mail {
                destination?.infoSmsEmail = email
            }
        }
    }
}
