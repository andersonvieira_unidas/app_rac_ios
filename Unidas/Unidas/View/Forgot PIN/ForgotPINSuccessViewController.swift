//
//  ForgotPINSuccessViewController.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 26/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit


class ForgotPINSuccessViewController: UIViewController {
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func tapCloseButton(_ sender: Any) {
        
        let presentingViewController: UIViewController! = self.presentingViewController

        self.dismiss(animated: false) {
            presentingViewController.dismiss(animated: false, completion: nil)
            self.loginDismissDelegate?.dismissLogin()
        }
    }
}
