//
//  ReserveCompletedViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import MapKit
import SafariServices

protocol ReserveCompletedDelegate: class {
    func dismissReserveCompleted()
}

class ReserveCompletedViewController: UIViewController, ReserveCompletedViewDelegate, UITextViewDelegate, SFSafariViewControllerDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var addToCalendarButton: UnidasButton?
    @IBOutlet weak var contractNumberLabel: UILabel!
    @IBOutlet weak var dateOpenContract: UILabel!
    @IBOutlet weak var timePickUpLabel: UILabel!
    @IBOutlet weak var pickUpLocationTextView: UITextView!
    @IBOutlet weak var timeReturnLabel: UILabel!
    @IBOutlet weak var returnLocationTextView: UITextView!
    @IBOutlet weak var groupCarLabel: UILabel!
    @IBOutlet weak var groupTypeCarLabel: UILabel!
    @IBOutlet weak var descriptionCarLabel: UILabel!
    @IBOutlet weak var reserveTotalLabel: UILabel! // Não Usa
    @IBOutlet weak var reservePaidLabel: UILabel!
    @IBOutlet weak var reserveBrandImageView: UIImageView!
    @IBOutlet weak var reserveNumberCardLabel: UILabel!
    @IBOutlet weak var preAuthorizationTotalLabel: UILabel!
    @IBOutlet weak var preAuthorizationBrandImageView: UIImageView!
    @IBOutlet weak var preAuthorizationNumberCardLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var userImageView: EnhancedImageView! // Não usa
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var observationLabel: UILabel!
    @IBOutlet weak var mapSnapshotView: MapSnapshotView! //Não usa
    @IBOutlet weak var paymentInfoStack: UIStackView!
    @IBOutlet weak var preAuthorizationInfoStack: UIStackView!
    @IBOutlet weak var separatorInfoPaymentView: DottedLineView! //Não usa
    @IBOutlet weak var paymentConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var prePaymentConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorInforPrePaymentView: DottedLineView!
    @IBOutlet weak var viewContentHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentInfoView: UIView!
    @IBOutlet weak var preAuthPaymentView: UIView!
    @IBOutlet weak var paymentsStackViewConstant: NSLayoutConstraint!
    
    
    weak var delegate: ReserveCompletedDelegate?
    
    var preAuthorizationCardDataView: ListCardDataView?
    var reservationCardDataView: ListCardDataView?
    var vehicleReservationsDataView: VehicleReservationsDataView!
    var openContractDataView: OpenContractDataView!
    var preAuthorizationDataView: PreAuthorizationEstimateDataView!
    var installments: Int?
    let attributedString = NSMutableAttributedString(string: NSLocalizedString("Em caso de No-Show em reservas pré-pagas, confira o valor a ser estornado em Política de Reembolso.", comment: "Em caso de No-Show em reservas pré-pagas, confira o valor a ser estornado em Política de Reembolso."))

    private var presenter: ReserveCompletedPresenterDelegate!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        setPopGestureRecognizer(value: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderUnidas()
        setPopGestureRecognizer(value: false)
        self.navigationController?.navigationItem.backBarButtonItem = nil
        
        presenter = ReserveCompletedPresenter(preAuthorizationCardDataView: preAuthorizationCardDataView,
                                              reservartionCardDataView: reservationCardDataView,
                                              vehicleReservationsDataView: vehicleReservationsDataView,
                                              openContractDataView: openContractDataView,
                                              preAuthorizationDataView: preAuthorizationDataView,
                                              installments: installments,
                                              companyTermsService: CompanyTermsService())
        
        presenter.delegate = self
        
        presenter.viewDidLoad()
        presenter.requestPermissionReminder()
        presenter.fetchRefundPolicy()
    }


    @IBAction func tapRoute(_ sender: Any) {
        presenter.routePickUp()
    }
    
    @IBAction func addPickupDateToCalendarButtonTapped(_ sender: Any) {
        presenter.addPickupDateToCalendarButtonTapped()
    }
    
    @IBAction func tapMyCar(_ sender: Any) {
        presenter.tapMyCar()
    }
    
    // MARK: - Ask Permission to Settings
    func askPermissionToSettings() {
        askPermissionToSettings(withMessage: NSLocalizedString("Settings Reminder Calendar Description", comment: ""))
    }
    
    func checkCalendarPermissionStatus() {
        
    }
    
    // MARK: - ReserveCompletedViewDelegate
    
    func setPopGestureRecognizer(value:Bool) {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = value
    }
    
    func showRefundPolicy() {
        
    }
    
    func finishTapMyCar() {
        dismiss(animated: true) {
            self.delegate?.dismissReserveCompleted()
        }
    }
    
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView) {
        guard let strURL = companyTermsDataView.description.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let politicRefund = URL(string: strURL) else { return }
        let accountTermsText = NSLocalizedString("Política de Reembolso.", comment: "Política de Reembolso.")
        let observationText = attributedString.string
        
        if let observationTermsRange = observationText.range(of: accountTermsText) {
            let observationTermsNSRange = NSRange(observationTermsRange, in: observationText)
            attributedString.addAttribute(.link, value: politicRefund, range: observationTermsNSRange)
        }
    }
    
    func setupView(vehicleReservationDataView: VehicleReservationsDataView, preAuthorizationCardDataView: ListCardDataView?, openContractDataView: OpenContractDataView, preAuthorizationDataView: PreAuthorizationEstimateDataView?, userResponseDataView: UserResponseDataView) {
        
        let hiddenInfoPayment = vehicleReservationDataView.hiddenLegalEntityCheckinPayment
        let hiddenInfoPrePayment = vehicleReservationDataView.hiddenLegalEntityCheckinPrePayment
        
        contractNumberLabel.text = openContractDataView.contractNumber
        dateOpenContract.text = DateFormatter.shortDateFormatter.string(from: Date())
        
        timePickUpLabel.attributedText = addTwoColorsInLabel(string1: NSLocalizedString("Checkin confirmation pickup", comment: ""), sizeColorA: 12, sizeColorB: 12, string2: " \(vehicleReservationDataView.pickUpDateOnly) às \(vehicleReservationDataView.pickUpHourOnly)")
        
        timeReturnLabel.attributedText = addTwoColorsInLabel(string1: NSLocalizedString("Checkin confirmation return", comment: ""), sizeColorA: 12, sizeColorB: 12, string2: " \(vehicleReservationDataView.returnDateOnly) às \(vehicleReservationDataView.returnHourOnly)")
        
        pickUpLocationTextView.attributedText = makeStore(storeAndCity: vehicleReservationDataView.pickUpLocation, link: "pickup")
        pickUpLocationTextView.delegate = self
            
        returnLocationTextView.attributedText = makeStore(storeAndCity: vehicleReservationDataView.returnLocation, link: "return")
        pickUpLocationTextView.delegate = self
        
        returnLocationTextView.linkTextAttributes = NSAttributedString.linkAttributed
        pickUpLocationTextView.linkTextAttributes = NSAttributedString.linkAttributed
 
        groupCarLabel.text = vehicleReservationDataView.groupDescription.lowerCapitalize()
        groupTypeCarLabel.text = vehicleReservationDataView.group
        descriptionCarLabel.text = vehicleReservationDataView.vehicleGroup
        //reserveTotalLabel.text = vehicleReservationDataView.totalValueString
        nameLabel.text = userResponseDataView.userUnidas?.name
        //userImageView.setImage(withURL: userResponseDataView.account.profileImage, placeholderImage: #imageLiteral(resourceName: "profile-avatar"))
        userEmail.text = userResponseDataView.userUnidas?.email
        userPhone.text = userResponseDataView.userUnidas?.formattedMobilePhoneNumber
        preAuthorizationTotalLabel.text = preAuthorizationDataView?.value
        preAuthorizationBrandImageView.image = preAuthorizationCardDataView?.brandImage
        preAuthorizationNumberCardLabel.text = preAuthorizationCardDataView?.fourLastNumberCard
        
        if let url = vehicleReservationsDataView.picture {
            carImageView.setImage(withURL: url)
        }
        
        if hiddenInfoPayment {
            paymentInfoView.isHidden = true
            separatorInfoPaymentView.isHidden = true
            viewContentHeight.constant -= 56
            paymentsStackViewConstant.constant -= 74
        }
        
        if hiddenInfoPrePayment {
            preAuthPaymentView.isHidden = true
            separatorInforPrePaymentView.isHidden = true
            viewContentHeight.constant -= 56
            paymentsStackViewConstant.constant -= 74
        }
    }
    
    func addTwoColorsInLabel(string1: String, sizeColorA: CGFloat, sizeColorB: CGFloat, string2:String) -> NSMutableAttributedString {
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.regular(ofSize: sizeColorA), NSAttributedString.Key.foregroundColor : UIColor.darkGray]
        
        let attrs2 = [NSAttributedString.Key.font : UIFont.bold(ofSize: sizeColorB), NSAttributedString.Key.foregroundColor : UIColor.darkGray]
        
        let attributedString1 = NSMutableAttributedString(string:string1, attributes:attrs1)
        
        let attributedString2 = NSMutableAttributedString(string:string2, attributes:attrs2)
        
        attributedString1.append(attributedString2)
        return attributedString1
    }

    func setupViewPaymentWithPrePayment(reservationCard: ListCardDataView, vehicleReservationDataView: VehicleReservationsDataView, installments: Int) {
        reservePaidLabel.text = vehicleReservationDataView.valuePayment(installments: String(installments))
        reserveBrandImageView.image = reservationCard.brandImage
        reserveNumberCardLabel.text = reservationCard.fourLastNumberCard
    }
    
    func setupViewPayment(vehicleReservationDataView: VehicleReservationsDataView) {
        reservePaidLabel.text = vehicleReservationDataView.valuePayment(installments: nil)
        reserveBrandImageView.image = vehicleReservationDataView.brandImage
        reserveNumberCardLabel.text = vehicleReservationDataView.cardNumberShortFormatted
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
    
    @IBAction func tapOpenPoliticUrl(_ sender: Any) {
        guard let refundPolicy = presenter.refundPolicy?.description, let url = URL(string: refundPolicy) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: - TextView delegate
    
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        
        if url.absoluteString.contains("pickup"){
            presenter.routePickUpReserve()
        } else if url.absoluteString.contains("return"){
            presenter.routeReturnReserve()
        } else {
            safariUrlOpen(withUrl: url)
        }
        return true
    }
    
    // MARK: - SafariView delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    private func safariUrlOpen(withUrl url: URL){
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.delegate = self
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    /** CALENDAR **/
    func enableAddToCalendarButton() {
        guard let addCalendar = addToCalendarButton else { return }
        addCalendar.isEnabled = true
    }
    
    func disableAddToCalendarButton() {
        guard let addToCalendar = addToCalendarButton else { return }
        addToCalendar.isEnabled = false
    }
    
    func showAddedToCalendar() {
        DispatchQueue.main.async {
            guard let addCalendar = self.addToCalendarButton else { return }
            addCalendar.setTitle(NSLocalizedString("Added to calendar!", comment: "Added to calendar title"), for: .normal)
            self.disableAddToCalendarButton()
        }
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    //MARK: - Private Functions
    private func makeStore(storeAndCity: String?, link: String) -> NSMutableAttributedString? {
         guard let store = storeAndCity else { return nil }
         
         let storeFormatted = NSMutableAttributedString(string: store.localizedCapitalized, attributes: [NSAttributedString.Key.font: UIFont.bold(ofSize: 12),
              NSAttributedString.Key.link: link])
         return storeFormatted
     }
}
