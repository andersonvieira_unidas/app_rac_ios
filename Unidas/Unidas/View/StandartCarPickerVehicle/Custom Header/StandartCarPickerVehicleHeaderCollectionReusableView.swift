//
//  StandartCarPickerVehicleHeaderCollectionReusableView.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import Reusable

class StandartCarPickerVehicleHeaderCollectionReusableView: UICollectionReusableView, NibReusable {
    static let size = CGSize(width: 0, height: 60)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
