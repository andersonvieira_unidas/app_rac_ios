//
//  StandartCarPickerVehicleCollectionViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class StandartCarPickerVehicleCollectionViewController: UIViewController, StandartCarPickerVehicleViewDelegate, GarageCollectionViewCellDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SimpleAlertDelegate{
    
    private var presenter: StandartCarPickerVehiclePresenterDelegate!
    var stepView: StepView?
    var quotationDataView: [QuotationsDataView]?
    var pickUp: GarageReservationDetailsDataView?
    var `return`: GarageReservationDetailsDataView?
    var oldReservation: VehicleReservationsDataView?
    var voucherCode: String?
    
    private var templateCell: GarageCollectionViewCell? = nil
    
    @IBOutlet weak var carCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: ISPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = StandartCarPickerVehiclePresenter(  quotationDataView: quotationDataView!,
                                                        firebaseAnalyticsService: FirebaseAnalyticsService(),
                                                        adjustAnalyticsService: AdjustAnalyticsService())
        presenter.delegate = self
        carCollectionView.delegate = self
        carCollectionView.dataSource = self
        
        pageControl.numberOfPages = presenter.numberOfGarages
        
        reloadData()
        
        setupCollectionView()
        setupChange()
        configureBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.stepView?.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigation()
    }
    
    func setupCollectionView() {
        carCollectionView.register(cellType: GarageCollectionViewCell.self)
        carCollectionView.register(supplementaryViewType: StandartCarPickerVehicleHeaderCollectionReusableView.self, ofKind: UICollectionView.elementKindSectionHeader)
    }
    
    func setupNavigation() {
        if let stepView = navigationController?.navigationBar.subviews.first(where: { $0 is StepView }) as? StepView {
            self.stepView = stepView
            
            if let collectionView = carCollectionView {
                let edgeInsets = UIEdgeInsets(top: collectionView.contentInset.top,
                                              left: collectionView.contentInset.left,
                                              bottom: collectionView.contentInset.bottom,
                                              right: collectionView.contentInset.right)
                collectionView.contentInset = edgeInsets
                collectionView.scrollIndicatorInsets = edgeInsets
            }
            
            stepView.setupStep(total: 4, selected: 1)
        }
    }
    
    func setupChange(){
        if oldReservation != nil {
            
            guard let quotations = quotationDataView else { return }
            
            var i = 0
            for quotation in quotations {
                if quotation.model.vehicle?.vehicleCategory == oldReservation?.model.grupo {
                    DispatchQueue.main.async {
                        let indexPath = IndexPath(item: i, section: 0)
                        self.carCollectionView.scrollToItem(at: indexPath, at: .right, animated: false)
                        self.pageControl.currentPage = i
                    }
                    break
                }
                i += 1
            }
        }
    }
    
    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfGarages
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = carCollectionView.dequeueReusableCell(for: indexPath, cellType: GarageCollectionViewCell.self)
        cell.delegate = self
        
        var isFranchise: Bool = false
        var isExpress: Bool = false
        
        if let pickupIsFranchise = pickUp?.garage?.isFranchise {
            isFranchise = pickupIsFranchise
        }
        
        if let pickupType = pickUp?.garage?.model.type {
            if pickupType == .express {
                isExpress = true
            }
        }
        
        cell.setup(with: presenter.quotationResume(at: indexPath.row), isFranchise: isFranchise, isExpress: isExpress)
        return cell
    }
    
    // MARK: - Collection view delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelectQuotations(at: indexPath.row)
        
        let group = presenter.quotationResume(at: indexPath.row).model.quotation?.vehicle?.vehicleGroups ?? ""
        let group_description = presenter.quotationResume(at: indexPath.row).model.quotation?.vehicle?.vehicleCategory ?? ""
        let totalAmount = presenter.quotationResume(at: indexPath.row).totalWithAdministrativeTaxValue ?? 0.0
        
        presenter.logAddToWishlistEvent(group: group, groupDescription: group_description, currency: "BRL", price: totalAmount)
    }
    
    // MARK: - Collection view flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let pageControlHeight = pageControl.frame.height 
        
        return UIEdgeInsets(top: 0, left: 10, bottom: pageControlHeight, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = templateCell ?? GarageCollectionViewCell.loadFromNib()
        let quotation = presenter.quotationResume(at: indexPath.row)
        
        var isFranchise: Bool = false
        var isExpress: Bool = false
        
        if let pickupIsFranchise = pickUp?.garage?.isFranchise {
            isFranchise = pickupIsFranchise
        }
        
        if let pickupType = pickUp?.garage?.model.type {
            if pickupType == .express {
                isExpress = true
            }
        }
        
        cell.setup(with: quotation, isFranchise: isFranchise, isExpress: isExpress)
        
        return cell.size(width: carCollectionView.frame.width - 20, height: carCollectionView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        pageControl.currentPage = Int(pageNumber)
    }
    
    // MARK: StandartCarPickerVehicleViewDelegate
    
    func reloadData() {
        carCollectionView?.reloadData()
    }
    
    func showVehicleCharacteristics(vehicleCharacteristicDataView: [VehicleCharacteristicDataView]) {
        let storyboard = UIStoryboard(name: "ConnectedCarRent", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "VehicleCharacteristicsVC") as? VehicleCharacteristicsViewController {
            viewController.vehicleCharacteristicDataView = vehicleCharacteristicDataView
            self.present(viewController, animated: true, completion: nil)
        }
        //showVehicleCharacteristicsTableViewController(vehicleCharacteristicDataView: vehicleCharacteristicDataView)
    }
    
    func showVehicleCharacteristics(vehicleCategory: String) {
        let storyboard = UIStoryboard(name: "ConnectedCarRent", bundle: nil)
        if let viewController = storyboard.instantiateViewController(withIdentifier: "VehicleCharacteristicsVC") as? VehicleCharacteristicsViewController {
            viewController.vehicleCategory = vehicleCategory
            self.present(viewController, animated: true, completion: nil)
        }
        //showVehicleCharacteristicsTableViewController(vehicleCategory: vehicleCategory)
    }
    
    func showProtectionsAndEquipments(for quotations: QuotationsDataView) {
        performSegue(withIdentifier: "showProtectionsAndEquipments", sender: quotations)
    }
    
    func showUnavailableMessageForQuotation() {
        let title = NSLocalizedString("Group unavailable", comment: "unavailable group alert title")
        let message = NSLocalizedString("Sorry, but this group is unavailable at this store", comment: "unavailable group alert message")
        let image = #imageLiteral(resourceName: "icon-cancel-reservation-alert")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: image)
        simpleAlert.delegate = self
        simpleAlert.show(animated: true)
    }
    
    // MARK: GarageCollectionViewCellDelegate
    
    func didTapShowCharacteristicButton(in garageCell: GarageCollectionViewCell) {
        let indexPath = carCollectionView?.indexPath(for: garageCell)
        if let index = indexPath?.row {
            presenter.didTapCharacteristic(at: index)
        }
    }
    
    //MARK: - Private Functions
    private func configureBackButton(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - SimpleAlertDelegate
    func close() {
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showProtectionsAndEquipments" {
            if let viewController = segue.destination as? ProtectionsAndEquipmentsViewController {
                viewController.quotationDataView = sender as? QuotationsDataView
                viewController.pickUp = pickUp
                viewController.return = `return`
                viewController.oldReservation = oldReservation
                viewController.voucherCode = voucherCode
            }
        }
        navigationItem.setBackButtonTitle(title: "")
    }
}
