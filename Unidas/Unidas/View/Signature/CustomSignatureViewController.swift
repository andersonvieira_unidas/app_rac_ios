//
//  SignUpSignatureViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import YPDrawSignatureView

protocol CustomSignatureDelegate: class {
    func acceptSignature(data: Data, viewController: CustomSignatureViewController)
}

protocol JumpCustumSignatureDelegate: class {
    func jumpSignature()
}

class CustomSignatureViewController: UIViewController {
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var nextButton: EnhancedButton!
    @IBOutlet weak var continuLaterButton: EnhancedButton!
    @IBOutlet weak var resetButton: EnhancedButton!
    @IBOutlet weak var closeButton: UIButton!
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    weak var delegate: CustomSignatureDelegate?
    weak var jumpDelegate: JumpCustumSignatureDelegate?
    
    var showsCloseButton: Bool = false
    var showsLaterButton: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        closeButton.isHidden = !showsCloseButton
        continuLaterButton.isHidden = !showsLaterButton
        setupOrientationToLandscape()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    @IBAction func touchClearSignuture(_ sender: Any) {
        self.signatureView.clear()
    }
    
    @IBAction func touchNext(_ sender: EnhancedButton) {
        if let signatureImage = self.signatureView.getSignature(scale: 10),
            let imageResized = signatureImage.resized(withPercentage: 0.4),
            let data = imageResized.jpegData(compressionQuality: 0.75){
            delegate?.acceptSignature(data: data, viewController: self)
        }
    }
    
    @IBAction func touchDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchContinueLater(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            self.jumpDelegate?.jumpSignature()
            //self.loginDismissDelegate?.dismissLogin()
        })
    }
    
    // MARK: - SignUpSignatureViewDelegate
    
    func setupView() {
        signatureView.layer.cornerRadius = 10
        signatureView.layer.masksToBounds = true
    }
    
    func startLoading() {
        continuLaterButton.isEnabled = false
        resetButton.isEnabled = false
        nextButton.isEnabled = false
        nextButton.setTitleColor(#colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        nextButton.isUserInteractionEnabled = false
    }
    
    func endLoading() {
        continuLaterButton.isEnabled = true
        resetButton.isEnabled = true
        nextButton.isEnabled = true
        nextButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        nextButton.isUserInteractionEnabled = true
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func setupOrientationToLandscape() {
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(.all)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
