//
//  ContractDetailsTableViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import SafariServices
import MapKit

class ContractDetailsTableViewController: UITableViewController, ContractDetailsViewDelegate, ProtectionsContractHistory, UITextViewDelegate {
    
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpTextView: UITextView!
    
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnTextView: UITextView!
    @IBOutlet weak var pickupObservationButton: UIButton!
    @IBOutlet weak var returnObservationButton: UIButton!
    
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var vehicles: UILabel!
    
    @IBOutlet weak var licensePlate: UILabel!
    @IBOutlet weak var vehicleSeparatorView: UIView!
    @IBOutlet weak var vehicleStackView: UIStackView!
    @IBOutlet weak var qrCodeStackView: UIStackView!
    @IBOutlet weak var qrCodeSeparatorView: UIView!
    @IBOutlet weak var qrCodeMessageLabel: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyStack: UIStackView!
    @IBOutlet weak var downloadContractButton: UnidasButton!
    
    var contractDataView: ContractDataView!
    var tooltipsToRemove: [TooltipView] = []
    
    private var presenter: ContractDetailsPresenterDelegate!
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ContractsDetailsPresenter(contractDataView: contractDataView,
                                              documentUnidasService: DocumentUnidasService())
        presenter.delegate = self
        presenter.viewDidLoad()
        
        setupNavigationController()
        setupActivityIndicator()
        setupTableView()
        setupView()
        configureBackButton()
        presenter.showQrCode()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchTableView)))
        
        qrCodeStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchQrCode)))
    }
    
    @objc func touchTableView() {
        removeTooltips()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        resizeTableViewHeaderIfNeeded()
    }
    
    func resizeTableViewHeaderIfNeeded() {
        guard let headerView = tableView.tableHeaderView else { return }
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var headerFrame = headerView.frame
        
        if height != headerFrame.size.height {
            headerFrame.size.height = height
            headerView.frame = headerFrame
            tableView.tableHeaderView = headerView
            tableView.layoutIfNeeded()
        }
        headerView.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func setupTableView() {
        tableView.register(headerFooterViewType: ReservationDetailsSectionHeader.self)
        tableView.register(cellType: ProtectionsContractHistoryCell.self)
        tableView.register(cellType: ContractPaymentItemTableViewCell.self)
        tableView.separatorStyle = .none
        tableView.separatorColor = UIColor.clear
    }
    
    private func setupView() {
        let pickupString = NSLocalizedString("Pickup", comment: "")
        let returnString = NSLocalizedString("Return", comment: "")
        
        pickUpDateLabel.attributedText = transformAttributedText(normalText: pickupString, boldText: contractDataView.pickUpDateFormatted)
        
        let pickupStoreMuttableString = NSMutableAttributedString(string: contractDataView.formattedStorePickUpLocation ?? "")
        let rangePickup = pickupStoreMuttableString.string.nsRange
        pickupStoreMuttableString.addAttribute(.link, value: "pickup", range: rangePickup)
        pickupStoreMuttableString.addAttribute(NSAttributedString.Key.font, value: UIFont.bold(ofSize: 12), range: rangePickup)
        pickUpTextView.attributedText = pickupStoreMuttableString
        pickUpTextView.linkTextAttributes = NSAttributedString.linkAttributed
        pickUpTextView.delegate = self
        
        returnDateLabel.attributedText = transformAttributedText(normalText: returnString, boldText: contractDataView.returnDateFormatted)
        
        let returnStoreMuttableString = NSMutableAttributedString(string: contractDataView.formattedStoreReturnLocation ?? "")
        let rangeReturn = returnStoreMuttableString.string.nsRange
        returnStoreMuttableString.addAttribute(.link, value: "return", range: rangeReturn)
        returnStoreMuttableString.addAttribute(NSAttributedString.Key.font, value: UIFont.bold(ofSize: 12), range: rangeReturn)
        
        returnTextView.attributedText = returnStoreMuttableString
        returnTextView.linkTextAttributes = NSAttributedString.linkAttributed
        returnTextView.delegate = self
        
        if let pickUpObservation = contractDataView.pickUpObservation, !pickUpObservation.isEmpty {
            pickupObservationButton.isHidden = false
        }
        
        if let returnObservation = contractDataView.returnObservation, !returnObservation.isEmpty {
            returnObservationButton.isHidden = false
        }
        
        groupDescription.text = contractDataView.groupDescription
        groupName.text = contractDataView.groupCode
        vehicles.text = contractDataView.groupVehicles
        licensePlate.text = contractDataView.vehicle?.licensePlate
        
        vehicleSeparatorView.isHidden = contractDataView.isHiddenVehicleInformation
        vehicleStackView.isHidden = contractDataView.isHiddenVehicleInformation
        
        companyName.isHidden = !contractDataView.legalEntity
        companyStack.isHidden = !contractDataView.legalEntity
        
        companyName.text = contractDataView.companyName
    }
    
    //MARK: - InterfaceBuilder Actions
    @IBAction func openTooltip(_ sender: UIButton) {
        
        if !tooltipsToRemove.isEmpty {
            removeTooltips()
            return
        }
        
        let toolTip = TooltipView.loadFromNib()
        toolTip.alpha = 0
        var description = ""
        
        if sender.tag == 1 {
            description = contractDataView.pickUpObservation ?? ""
        } else if sender.tag == 2 {
            description = contractDataView.returnObservation ?? ""
        }
        
        if description.isEmpty { return }
        
        toolTip.tag = sender.tag
        toolTip.setup(description: description)
        
        removeTooltips()
        
        toolTip.translatesAutoresizingMaskIntoConstraints = false
        tableView.addSubview(toolTip)
        
        NSLayoutConstraint.activate([
            toolTip.topAnchor.constraint(equalTo: sender.bottomAnchor, constant: 5),
            toolTip.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            toolTip.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 10),
            toolTip.widthAnchor.constraint(equalTo: tableView.widthAnchor, constant: -20)
        ])
        
        tooltipsToRemove.append(toolTip)
        UIView.animate(withDuration: 0.5) {
            toolTip.alpha = 1.0
        }
    }
    
    @IBAction func tapGenerateContractPdf(_ sender: Any) {
        downloadContractButton.isEnabled = false
        presenter.generateContractPdf()
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func touchHeader(_ sender: UIButton?){
        guard let buttonExpandClose = sender else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    @objc func headerTap(gestureRecognizer: UITapGestureRecognizer) {
        guard let view = gestureRecognizer.view,
            let header = view as? ReservationDetailsSectionHeader, let buttonExpandClose = header.expandCloseButton else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    @objc func touchQrCode(_ sender: UIButton?) {
        performSegue(withIdentifier: "showContractQRCode", sender: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSection
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return presenter.numberOfProtections
        case 1: return presenter.numberOfServices
        case 2: return presenter.numberOfEquipments
        case 3: return presenter.numberOfPayments
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section != 3 {
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProtectionsContractHistoryCell.self)
            cell.delegate = self
            
            let totalDiaries = String(contractDataView.daysInterval ?? 0)
            
            cell.setup(contractDetailListItem: presenter.contractItem(at: indexPath), totalDiaries: totalDiaries, hiddenPaymentInformation: false)
            
            return cell
        } else {
            
            let paymentItemDataView = presenter.paymentItem(at: indexPath)
            let isLegalEntityPaymentRequired = contractDataView.isLegalEntityPaymentRequired
            let isLegalEntityPrePaymentRequired = contractDataView.isLegalEntityPrePaymentRequired
            
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ContractPaymentItemTableViewCell.self)
            cell.setup(for: paymentItemDataView, isLegalEntityPaymentRequired: isLegalEntityPaymentRequired, isLegalEntityPrePaymentRequired: isLegalEntityPrePaymentRequired)
            
            return cell
        }
    }
    
    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3 {
            return 80.0
        }
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        if section == 0 || section == 2 {
            return 55
        }
        if section == 3 {
            return 80.0
        }
        
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let title = presenter.titleForSection(section: section) else { return nil }
        let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ReservationDetailsSectionHeader") as! ReservationDetailsSectionHeader
        header.titleLabel.text = title
        header.isUserInteractionEnabled = true
        header.tag = section
        header.expandCloseButton.tag = section
        
        let rowsInSection = tableView.numberOfRows(inSection: section)
        
        if rowsInSection == 0 {
            UIView.animate(withDuration: 0.5) {
                header.expandCloseButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerTap))
        header.addGestureRecognizer(tapRecognizer)
        header.expandCloseButton.addTarget(self, action: #selector(touchHeader(_:)), for: .touchUpInside)
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presenter.heightForHeaderSection(section: section)
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeTooltips()
    }
    
    // MARK: - Contract Details View Delegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func isHiddenQrCode() {
        qrCodeStackView.isHidden = true
        qrCodeSeparatorView.isHidden = true
    }
    
    func setQrCodeMessage(message: String) {
        qrCodeMessageLabel.text = message
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func openContractPdf(url: URL) {
        let safariViewController = SFSafariViewController(url: url)
        present(safariViewController, animated: true, completion: nil)
    }
    
    func enabledDownloadContractButton() {
        downloadContractButton.isEnabled = true
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContractQRCode" {
            let destination = segue.destination as? ContractQRCodeViewController
            destination?.qrCodeInformation = contractDataView.qrCodeInformation
        }
    }
    
    //MARK: - ProtectionsContractHistory
    func didPressToolTipFromProtections(cell: ProtectionsContractHistoryCell, button: UIButton, observation: String?) {
        if let observation = observation {
            openTooltip(y: cell.center.y, observation: observation)
        }
    }
    
    // MARK: - Private Functions
    private func remove(tooltip: TooltipView){
        UIView.animate(withDuration: 0.5,
                       animations: { () -> Void in
                        tooltip.alpha = 0
        }, completion: { (finished) -> Void in
            tooltip.removeFromSuperview()
        })
    }
    
    private func headerExpandClose(button: UIButton) {
        setSizeForList(listId: button.tag)
        removeTooltips()
        tableView.reloadSections(IndexSet(integer: button.tag), with: .automatic)
    }
    
    private func setupActivityIndicator() {
        let activityIndicatorItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activityIndicatorItem
    }
    
    private func setupNavigationController() {
        self.title = contractDataView.formattedContract
    }
    
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    private func removeTooltips(){
        tooltipsToRemove.forEach { (tooltip) in
            remove(tooltip: tooltip)
        }
        tooltipsToRemove = []
    }
    
    private func openTooltip(y: CGFloat, observation: String){
        
        if !tooltipsToRemove.isEmpty {
            removeTooltips()
            return
        }
        
        let toolTip = TooltipView.loadFromNib()
        toolTip.alpha = 0
        
        if observation.isEmpty { return }
        
        toolTip.setup(description: observation)
        
        removeTooltips()
        tableView.addSubview(toolTip)
        
        toolTip.topAnchor.constraint(equalTo: self.view.topAnchor, constant: y + 10).isActive = true
        toolTip.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        toolTip.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 10).isActive = true
        toolTip.widthAnchor.constraint(equalTo: tableView.widthAnchor, constant: -20).isActive = true
        tooltipsToRemove.append(toolTip)
        
        tooltipsToRemove.append(toolTip)
        
        UIView.animate(withDuration: 0.5) {
            toolTip.alpha = 1.0
        }
    }
    
    private func setSizeForList(listId: Int) {
        switch listId {
        case 0:
            if presenter.numberOfProtections == 0 {
                presenter.numberOfProtections = presenter.protectionsCount
            } else {
                presenter.numberOfProtections = 0
            }
            
        case 1:
            if presenter.numberOfServices == 0 {
                presenter.numberOfServices  = presenter.servicesCount
            } else {
                presenter.numberOfServices  = 0
            }
            
        case 2:
            if presenter.numberOfEquipments == 0 {
                presenter.numberOfEquipments = presenter.equipmentsCount
            } else {
                presenter.numberOfEquipments  = 0
            }
            
        case 3:
            if presenter.numberOfPayments == 0 {
                presenter.numberOfPayments = presenter.paymentsCount
            } else {
                presenter.numberOfPayments  = 0
            }
        default: break
            
        }
    }
    
    private func transformAttributedText(normalText: String, boldText: String?) -> NSMutableAttributedString{
        let color = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        let attrsNormalFont = [NSAttributedString.Key.font : UIFont.regular(ofSize: 12), NSAttributedString.Key.foregroundColor: color]
        let attrsBoldFont = [NSAttributedString.Key.font : UIFont.bold(ofSize: 14),
                             NSAttributedString.Key.foregroundColor: color]
        
        let attributedText = NSMutableAttributedString(string: normalText, attributes: attrsNormalFont)
        let attributedSpace = NSMutableAttributedString(string: " ", attributes: attrsNormalFont)
        let attributedBold = NSMutableAttributedString(string: boldText ?? "", attributes: attrsBoldFont)
        
        attributedText.append(attributedSpace)
        attributedText.append(attributedBold)
        
        return attributedText
    }
    
    //MARK: - UITextViewDelegate
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url.absoluteString.contains("pickup") {
            presenter.routePickUpReserve()
        }
        
        if url.absoluteString.contains("return") {
            presenter.routeReturnReserve()
        }
        return true
    }
}
