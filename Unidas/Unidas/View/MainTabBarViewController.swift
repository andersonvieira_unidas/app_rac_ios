//
//  MainTabBarViewController.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 29/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController, UITabBarControllerDelegate, MainViewDelegate, ReserveDetailsViewControllerDelegate {
    
    var reservationCode: String?
    var window: UIWindow?
    
    lazy var presenter: MainPresenterDelegate = {
        let presenter = MainPresenter(reservationUnidasService: ReservationUnidasService())
        presenter.delegate = self
        return presenter
    }()
    
    override func viewDidLoad() {
        if let reservation = reservationCode {
            presenter.fetchReservation(reservation: reservation)
        }
        
        delegate = self
        self.tabBar.tintColor = UIColor.init(named: "tintColor")
        self.tabBar.unselectedItemTintColor = UIColor.init(named: "tabBarUnselected")
        self.tabBar.backgroundColor = UIColor.init(named: "background")
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func setupNavigation(navigation: UINavigationController) {
        self.navigationItem.title = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPreview", let reserveDetailPreview = sender as? ReserveDetailPreviewDataView {
            
            let storyBoard = UIStoryboard(name: "ReserveDetails", bundle: nil)
            
            if let destination = storyBoard.instantiateViewController(withIdentifier: "ReserveDetails") as? ReserveDetailsViewController {
                
                destination.rules = reserveDetailPreview.rulesDataView
                destination.vehicleReservationsDataView = reserveDetailPreview.vehicleReservationsDataView
                destination.delegate = self
                let navigation = LogoNavigationController(rootViewController: destination)
                let navigationBarAppearace = UINavigationBar.appearance()
                navigationBarAppearace.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                // navigationBarAppearace.barTintColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
                
                navigation.modalPresentationStyle = UIModalPresentationStyle.fullScreen
                self.present(navigation, animated: false, completion: nil)
            }
        }
    }
    
    //MARK: - ReserveDetailsViewControllerDelegate
    func notFoundReserve() {
        let title = NSLocalizedString("Reservation Not Found Message Title", comment: "")
        let message = NSLocalizedString("Reservation Not Found Message", comment: "")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: #imageLiteral(resourceName: "icon-cancel-reservation-alert"))
        simpleAlert.show(animated: true)
    }
    
    func reloadReserve() {
        
    }
    
    func returnToList() {
        
    }
    
    //MARK: - MainViewDelegate
    func startLoading() {
        LoadingScreen.setLoadingScreen(view: view)
    }
    
    func endLoading() {
        LoadingScreen.removeLoadingScreen(view: view)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showDetails(vehicleReservationsDataView: VehicleReservationsDataView, rulesDataView: RulesDataView) {
        let reserveDetailPreview = ReserveDetailPreviewDataView(vehicleReservationsDataView: vehicleReservationsDataView, rulesDataView: rulesDataView)
        performSegue(withIdentifier: "showPreview", sender: reserveDetailPreview)
    }
    
    //MARK: - TabBarDelegate
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if tabBarController.tabBar.selectedItem == tabBar.items![3] {
            return viewController != tabBarController.selectedViewController
        }
        return true
    }
}
