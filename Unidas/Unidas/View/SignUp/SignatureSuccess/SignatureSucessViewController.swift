//
//  SignatureSucessViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol SignatureSuccessViewControllerDelegate {
    func finishShowSignatureSuccessView()
}

class SignatureSucessViewController: UIViewController {

    @IBOutlet weak var backView: UIView!
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    var delegate: SignatureSuccessViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
         return .portrait
     }
     
     override var shouldAutorotate: Bool {
         return false
     }
    
    func setupView() {
        backView.layer.cornerRadius = 4
        backView.layer.borderWidth = 1
        backView.layer.borderColor = #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if let delegate = self.delegate {
                delegate.finishShowSignatureSuccessView()
            }
        })
    }
}
