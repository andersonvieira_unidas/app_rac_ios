//
//  SignUpValidatePhoneViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 13/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import InputMask

class SignUpValidatePhoneViewController: UIViewController, UITextFieldDelegate, SignUpValidatePhoneViewDelegate, PINViewControllerDelegate {

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var resendButton: UnidasButton!
    @IBOutlet weak var resendLoading: UIActivityIndicatorView!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var progressBackView: UIView!
    @IBOutlet weak var didNotReceiveCodeBackView: UIView!
    
    @IBOutlet weak var smscode0TextField: UITextField!
    @IBOutlet weak var smscode1TextField: UITextField!
    @IBOutlet weak var smscode2TextField: UITextField!
    @IBOutlet weak var smscode3TextField: UITextField!
    @IBOutlet weak var smscode4TextField: UITextField!
    @IBOutlet weak var smscode5TextField: UITextField!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var smsTextFields: [UITextField]!
    
    @IBOutlet weak var infoSmsLabel: UILabel!
    
    let progress = Progress(totalUnitCount: 7)

    private var presenter: SignUpValidatePhonePresenter!
    var userDataView: UserResponseDataView!
    var unidasToken: UnidasTokenDataView!
    
    var activityIndicatorView: UIActivityIndicatorView!
    var pinViewController: PINViewController?
    var isValidate: Bool = false
    var backButtonHidden: Bool = true
    var formsAction: FormsAction = .create
    
    var smsArray = [String]()
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    weak var validatePhoneNumberDelegate: ValidatePhoneNumberDelegate?
    
    lazy var numberAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(keyBoardDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SignUpValidatePhonePresenter(recoveryService: RecoveryService(),
                                                 codeValidationService: CodeValidationService(),
                                                 accountsService: AccountsService(),
                                                 authUnidasService: AuthUnidasService(), firebaseAnalyticsService: FirebaseAnalyticsService(), accountAddressUnidasService: AccountAddressesUnidasService())
        
        setupDelegates()
        setupView()
        setupActivityIndicator()
        presenter.disabledResendButton()
        if backButtonHidden{
            self.navigationItem.setHidesBackButton(true, animated:true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !isValidate {
            progress.completedUnitCount = 3
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !isValidate {
        }
    }
    
    func setupDelegates() {
        codeField.delegate = self
        smscode0TextField.delegate = self
        smscode1TextField.delegate = self
        smscode2TextField.delegate = self
        smscode3TextField.delegate = self
        smscode4TextField.delegate = self
        smscode5TextField.delegate = self
        presenter.delegate = self
    }
    
    func setupView() {
        
        smscode0TextField.layer.cornerRadius = 1
        smscode1TextField.layer.cornerRadius = 1
        smscode2TextField.layer.cornerRadius = 1
        smscode3TextField.layer.cornerRadius = 1
        smscode4TextField.layer.cornerRadius = 1
        smscode5TextField.layer.cornerRadius = 1
        
        let tapGestureHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tapGestureHideKeyboard)
        
        addKeyboardToolbar()
        
        didNotReceiveCodeBackView.layer.cornerRadius = 5
        didNotReceiveCodeBackView.layer.borderWidth = 1
        didNotReceiveCodeBackView.layer.borderColor = UIColor.gray.cgColor
        
        countdownLabel.text = nil
        resendButtonIsEnabled(false)
        
        //if isValidate {
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
        //}
        smscode0TextField.becomeFirstResponder()
        
        infoSmsLabel.text = mobilePhoneMask()
        
        if let height = self.tabBarController?.tabBar.frame.height {
            viewBottomConstraint.constant = height
            scrollViewBottomConstraint.constant = height
        }
    }
    
    func changeSMSBorderColors(value: Bool) {
        if value {
            smsTextFields.forEach {
                $0.layer.borderWidth = 1
                $0.layer.borderColor = UIColor.red.cgColor
                $0.layer.cornerRadius = 5
            }
        }else{
            smsTextFields.forEach {
                $0.layer.borderWidth = 0
            }
        }
    }
    
    func addKeyboardToolbar()  {
        smsTextFields.forEach {
            $0.inputAccessoryView = numberAccessoryView
        }
    }
    
    @objc func keyBoardDoneButtonTapped(sender: UIBarButtonItem) {
        view.endEditing(true)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func touchCancelItem() {
        self.navigationController?.popViewController(animated: true)
        self.validatePhoneNumberDelegate?.showMessageSuccess(phoneUpdated: false)
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        let activatyItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activatyItem
    }
    
    private func confirmSms() {
        presenter.validate(at: codeField.text ?? "", formAction: formsAction, isValidate: isValidate)
    }
    
    private func mobilePhoneMask() -> String{
        var mobilePhone = ""
        if session.currentUser == nil {
            mobilePhone = AppPersistence.getValue(withKey: AppPersistence.mobilePhone) ?? ""
        } else {
            mobilePhone = session.currentUser?.account.mobilePhone ?? ""
        }
        
        let mobilePhoneFormatted = mobilePhone.formattedResidencePhoneNumber()
        let mobilePhoneFirst = String(mobilePhoneFormatted.prefix(7))
        let mobilePhoneLast  = String(mobilePhoneFormatted.suffix(2))
        
        let localized = NSLocalizedString("SMS Validation Code - Mobile Number", comment: "SMS Validation Code - Mobile Number")
        
        return String(format: localized, "\(mobilePhoneFirst)*****\(mobilePhoneLast)")
    }
    
    @IBAction func touchResendCode(_ sender: UIButton) {
        guard let codeField = codeField else { return }
        
        codeField.text = NSLocalizedString("SMS Code Place Holder", comment: "SMS Code Place Holder description")
        
        presenter.resendValidate(formAction: formsAction)
    }
    
    @IBAction func touchNextButton(_ sender: UIButton) {
        confirmSms()
    }
    
    @IBAction func smsCodeChange(_ sender: UITextField) {
        if (sender.text?.count == 6) {
            confirmSms()
            view.endEditing(true)
        }
    }
    
    // MARK: - SignUpValidatePhoneViewDelegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
        nextButton.loading(true, activityIndicatorViewStyle: .white)
        nextButton.isEnabled = false
        nextButton.isUserInteractionEnabled = false
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
        nextButton.loading(false)
        nextButton.isEnabled = true
        nextButton.isUserInteractionEnabled = true
    }
    
    func startLoadingResendCode() {
        resendLoading.startAnimating()
        resendButtonIsEnabled(false)
        //resendButton.isEnabled = false
    }
    
    func endLoadingResendCode() {
        resendLoading.stopAnimating()
        resendButtonIsEnabled(true)
        //resendButton.isEnabled = true
    }
    
    func finishValidateAccount() {
        self.navigationController?.popViewController(animated: true)
        self.validatePhoneNumberDelegate?.showMessageSuccess(phoneUpdated: true)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showPin() {
        performSegue(withIdentifier: "showPin", sender: nil)
    }
    
    func finishUpdatePassword(pinViewController: PINViewController) {
        pinViewController.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "segueToPhotoTutorial", sender: nil)
        })
    }
    
    func pinEndLoading(pinViewController: PINViewController) {
        pinViewController.endLoading()
    }
    
    func pinPresent(error: Error, pinViewController: PINViewController) {
        pinViewController.present(error: error, callbackDidFailFinishPin: true)
    }
    
    func showNewPinController(title: String, isHiddenForgotPin: Bool, isHiddenCancelButton: Bool, userResponseDataView: UserResponseDataView?, pinViewController: PINViewController) {
        pinViewController.endLoading()
        pinViewController.showNewPinController(title: title, isHiddenForgotPin: isHiddenForgotPin, isHiddenCancelButton: isHiddenCancelButton, userResponseDataView: userResponseDataView)
    }
    
    func didFinishUpdatePin(userResponseDataView: UserResponseDataView?, pinViewController: PINViewController, pin: String) {
        presenter.updatePassword(pin: pin, pinViewController: pinViewController)
    }
    
    func didFailConfirmPin(pinViewController: PINViewController, error: Error) {
        pinViewController.endLoading()
        pinViewController.present(error: error, callbackDidFailFinishPin: true)
    }
    
    func resendButtonIsEnabled(_ enabled: Bool) {
        resendButton.isEnabled = enabled
    }
    
    func showCountdown(value: String?) {
        countdownLabel.text = value
        if value != nil {
            resendLoading.stopAnimating()
        }
    }
    
    func finishResendCode() {
        presenter.disabledResendButton()
    }
    
    // MARK: - UITextfield delegate
    
    func orderSmsCode() {
        var index = 0
        smsArray.removeFirst(2)
        for item in smsArray {
            switch index {
            case 0:
                smscode0TextField.text = item
            case 1:
                smscode1TextField.text = item
            case 2:
                smscode2TextField.text = item
            case 3:
                smscode3TextField.text = item
            case 4:
                smscode4TextField.text = item
            case 5:
                smscode5TextField.text = item
                hideKeyboard()
            default:
                return
            }
            index += 1
        }
        nextButton.sendActions(for: .touchUpInside)
    }
    
    func fillCodeField() {
        codeField?.text = "\(smscode0TextField.text ?? "")\(smscode1TextField.text ?? "")\(smscode2TextField.text ?? "")\(smscode3TextField.text ?? "")\(smscode4TextField.text ?? "")\(smscode5TextField.text ?? "")"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 7 {
            smsArray.append(string)
            if smsArray.count == 8 {
                orderSmsCode()
            }
        }
        
        if textField.tag == 1 || textField.tag == 2 || textField.tag == 3 || textField.tag == 4 || textField.tag == 5 || textField.tag == 6 {
            let finalString: String? = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            if finalString!.count > 0 {
                textField.text = string
                
                let nextResponder: UIResponder? = textField.superview?.viewWithTag(textField.tag + 1)
                if nextResponder != nil {
                    fillCodeField()
                    nextResponder?.becomeFirstResponder()
                    return true
                }else if textField.tag == 6 {
                    fillCodeField()
                    nextButton.sendActions(for: .touchUpInside)
                    return false
                }else{
                    fillCodeField()
                }
            }else{
                textField.text = string
                if textField.tag == 1 {
                    fillCodeField()
                }else{
                    let nextResponder: UIResponder? = textField.superview?.viewWithTag(textField.tag - 1)
                    fillCodeField()
                    nextResponder?.becomeFirstResponder()
                    return false
                }
            }
        }
        return true
    }
    
    // MARK - PINViewControllerDelegate
    
    func didFinishPin(userResponseDataView: UserResponseDataView?, pin: String, pinViewController: PINViewController) {
        presenter.didFinishPin(userResponseDataView: userResponseDataView, pin: pin, pinViewController: pinViewController)
    }
    
    func didTapCancelPin() {
        
    }
    
    func didFailFinishPin(error: Error, pinViewController: PINViewController) {
        pinViewController.backToPreviousView()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPin" {
            if let navigationController = segue.destination as? UINavigationController {
                if let viewController = navigationController.viewControllers.first as? PINViewController {
                    viewController.delegate = self
                    viewController.userResponseDataView = userDataView
                    viewController.isHiddenBackButton = true
                    viewController.isHiddenCancelButton = true
                    viewController.isHiddenForgotPin = true
                    viewController.navigationTitle = NSLocalizedString("Register your PIN", comment: "Register your PIN navigation title")
                }
            }
        }
        else if segue.identifier == "segueToPhotoTutorial" {
            if let viewController = segue.destination as? SelfieRecomendationsViewController {
                viewController.loginDismissDelegate = loginDismissDelegate
            }
        }
    }
}
