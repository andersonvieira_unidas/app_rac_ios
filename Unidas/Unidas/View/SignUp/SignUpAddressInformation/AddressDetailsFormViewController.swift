//
//  SignUpAddressInformationViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 14/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class AddressDetailsFormViewController: UIViewController, AddressDetailsFormViewDelegate, AddressFormDelegate, ValidatePhoneNumberDelegate, MultipleChoiceAlertDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var nextStepButton: UIButton!
    @IBOutlet weak var finishLaterButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var addressFormView: AddressFormView!
    @IBOutlet weak var progressBackView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    
    var userResponseDataView: UserResponseDataView?
    
    var activityIndicatorView: UIActivityIndicatorView!
    private var presenter: AddressDetailsFormPresenterDelegate!
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    var formsAction: FormsAction = .create
    
    let progress = Progress(totalUnitCount: 7)
    
    lazy var numberAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(numberToolbarDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var residencialPhoneAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(residencialPhoneToolbarDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    lazy var addresseAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toolbarDoneButtonTapped))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = AddressDetailsFormPresenter(accountAddressesUnidasService: AccountAddressesUnidasService(), recoveryService: RecoveryService() ,firebaseAnalyticsService: FirebaseAnalyticsService(), accountRecoveryMethod: AccountRecoveryMethod(rawValue: 0)!)
        presenter.delegate = self
        addressFormView.delegate = self
        
        registerForKeyboardNotifications()
        setupActivityIndicator()
        setupView()
        addressFormView.setTextFieldData(userResponseDataView: userResponseDataView, formsAction: formsAction)
        configureBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupViewWillDisappear()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(false, animated: false)
    }
    
    //MARK: - Private Functions
    private func setupViewDidAppear() {
        if formsAction == .create {
            progress.completedUnitCount = 2
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
            stepLabel.isHidden = false
        }else{
            progressBackView.isHidden = true
            stepLabel.isHidden = true
        }
    }
    
    private func setupViewWillDisappear() {
        if formsAction == .create {
        }
    }
    
    private func setupView() {
        finishLaterButton.isHidden = formsAction.finishLaterIsHidden
        nextStepButton.setTitle(formsAction.buttonTitle, for: .normal)
        
        addressFormView.postalCodeTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.stateTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.cityTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.neighborhoodTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.streetTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.complementTextField.inputAccessoryView = addresseAccessoryView
        addressFormView.residencialPhoneField.inputAccessoryView = residencialPhoneAccessoryView
        addressFormView.numberTextField.inputAccessoryView = numberAccessoryView
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
    }
    
    private func configureBackButton(){
        //if formsAction == .update {
            let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
            navigationItem.leftBarButtonItem = backButton
        //}
    }
 
    // MARK: - Keyboard events
    
    func showMessageSuccess(phoneUpdated: Bool) {
        
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    // MARK: - Interfacebuilder Actions
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        if let keyboardHeight = keyboardFrame?.height {
            let edgeInsets = UIEdgeInsets(top: scrollView.contentInset.top,
                                          left: scrollView.contentInset.left,
                                          bottom: keyboardHeight,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let edgeInsets: UIEdgeInsets = .zero
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
    }
    
    @objc func numberToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        addressFormView.numberTextField.resignFirstResponder()
    }
    
    @objc func residencialPhoneToolbarDoneButtonTapped(sender: UIBarButtonItem) {
        addressFormView.residencialPhoneField.resignFirstResponder()
    }
    
    @objc func toolbarDoneButtonTapped(sender: UIBarButtonItem) {
        view.endEditing(true)
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func touchFinishLater(_ sender: Any) {
        
        let title = NSLocalizedString("Signup Title Message", comment: "")
        let message = NSLocalizedString("Signup Continue Later Message", comment: "")
        let firstButtonLabel = NSLocalizedString("Signup Continue", comment: "")
        let secondButtonLabel = NSLocalizedString("Signup Continue Later", comment: "")

        let multipleChoice = MultipleChoiceAlert(title: title, subTitle: message, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel, image: #imageLiteral(resourceName: "icon-profile-signup"), firstChoiceButtonColor: UIColor.themeImportantButton)
        
        multipleChoice.delegate = self
        multipleChoice.show(animated: true)
    }
    
    @IBAction func touchNextStep(_ sender: UIButton) {
        if formsAction == .create {
            presenter.createAddress(addressFormDataView: addressFormView.presenter.addressFormDataView)
        } else if formsAction == .update {
            presenter.updateAddress(addressFormDataView: addressFormView.presenter.addressFormDataView)
        }
    }
    
    // MARK: - SignUpAddressViewDelegate
    
    func startLoading() {
        activityIndicatorView.startAnimating()
        nextStepButton.setTitleColor(.white, for: .normal)
        nextStepButton.backgroundColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        nextStepButton.loading(true, activityIndicatorViewStyle: .white)
        
        nextStepButton.isUserInteractionEnabled = false
        finishLaterButton.isUserInteractionEnabled = false
        
        addressFormView.fieldsEnabled(false)
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
        nextStepButton.setTitleColor(.white, for: .normal)
        nextStepButton.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        nextStepButton.loading(false)
        
        nextStepButton.isUserInteractionEnabled = true
        finishLaterButton.isUserInteractionEnabled = true
        addressFormView.fieldsEnabled(true)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func finishCreateAddress() {
        performSegue(withIdentifier: "showPhoneValidate", sender: nil)
    }
    
    func finishUpdateAddress() {
        let message = NSLocalizedString("Address changed", comment: "")
        let title = NSLocalizedString("Address changed title", comment: "")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: #imageLiteral(resourceName: "icon-residencial-menu"))
        simpleAlert.show(animated: true)
    }
    
    // MARK: - AddressFormDelegate
    
    func present(error: Error, addressFormView: AddressFormView) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        
    }
    
    func choiceSecondTap(tag: Int) {
        dismiss(animated: true, completion: nil)
        loginDismissDelegate?.dismissLogin()
        presenter.addEventSignUpResidentialContinueLater(success: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showPhoneValidate" {
            
            let viewController = segue.destination as? SignUpValidatePhoneViewController
            viewController?.loginDismissDelegate = loginDismissDelegate
            viewController?.validatePhoneNumberDelegate = self
            viewController?.backButtonHidden = false
            
            if formsAction == .update {
                viewController?.isValidate = true
            }
            self.endLoading()
        }
    }
}
