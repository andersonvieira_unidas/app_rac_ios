//
//  SignUpCreditCarViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import InputMask

class CreditCardsDetailsFormViewController: UIViewController, CreditCardsDetailsFormViewDelegate, CreditCardFormViewCameraDelegate,
CustomCameraOcrDelegate{

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var laterButton: EnhancedButton!
    @IBOutlet weak var creditCardFormView: CreditCardFormView!
    
    @IBOutlet weak var progressBackView: UIView!
    
    let progress = Progress(totalUnitCount: 7)
    
    private var presenter: CreditCardsDetailsFormPresenter!
    weak var loginDismissDelegate: LoginDismissDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CreditCardsDetailsFormPresenter(paymentService: PaymentService(), firebaseAnalyticsService: FirebaseAnalyticsService(), loginPciService: LoginPciService(), creditCardPciService: CreditCardPciService())
        presenter.delegate = self
        
        registerForKeyboardNotifications()
        creditCardFormView.setup(titleIsHidden: true)
        creditCardFormView.delegate = self
        configureBackButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        progress.completedUnitCount = 6
        let progressFloat = Float(self.progress.fractionCompleted)
        self.progressView.setProgress(progressFloat, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    @IBAction func touchLater(_ sender: Any) {
        performSegue(withIdentifier: "segueToTerms", sender: nil)
//        dismiss(animated: true, completion: {
//            self.loginDismissDelegate?.dismissLogin()
//            self.presenter?.addEventSignUpCreditCardLaterStep(success: true)
//        })
    }
    
    @IBAction func touchCreateCard(_ sender: Any) {
        presenter.create(creditCardFormDataView: creditCardFormView.presenter.creditCardFormDataView)
    }
    
    private func configureBackButton(){
            let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
            navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Keyboard events
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        if let keyboardHeight = keyboardFrame?.height {
            let edgeInsets = UIEdgeInsets(top: scrollView.contentInset.top,
                                          left: scrollView.contentInset.left,
                                          bottom: keyboardHeight,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let edgeInsets: UIEdgeInsets = .zero
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
    }
    
    // MARK: - SignUpCreditCarViewDelegate
    
    func startLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        nextButton.isUserInteractionEnabled = false
        nextButton.loading(true, activityIndicatorViewStyle: .white)
    }
    
    func endLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        nextButton.isUserInteractionEnabled = true
        nextButton.loading(false)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    func showCamera() {
        performSegue(withIdentifier: "segueToCamera", sender: nil)
    }
    
    func finishCreateCard() {
        performSegue(withIdentifier: "segueToTerms", sender: nil)
    }
    
    func ocrResult(results: [String]) {
        guard let dateValidThru = CreditCardOcrUtil.extractValidThru(results: results), let cardNumber = CreditCardOcrUtil.extractCardNumber(results: results) else {
            showMessageErrorOcr()
            return
        }
        let dateValidThruFormatted = ValueFormatter.format(date: dateValidThru, format: "MM/yyyy")
        creditCardFormView.updateValues(cardNumber: cardNumber, dateValidThru: dateValidThruFormatted)
    }
    
    private func showMessageErrorOcr() {
        DispatchQueue.main.async {
            let title = NSLocalizedString("Unidas", comment: "Unidas Comment")
            let message = NSLocalizedString("Unable to process OCR", comment: "Unable to process OCR comment")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(.ok())
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToTerms" {
            let destination = segue.destination as? SignatureRecomendationsViewController
            
            destination?.loginDismissDelegate = loginDismissDelegate
        }
        if segue.identifier == "segueToCamera" {
            if let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.viewControllers.first as? CustomCameraOcrViewController {
                viewController.devicePosition = .back
                viewController.backgroundImage = #imageLiteral(resourceName: "crop-mask-creditcard")
                viewController.customCameraOcrDelegate = self
            }
        }
    }

}
