//
//  SelfieSuccessViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 29/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol SelfieSuccessViewControllerDelegate {
    func finishShowSelfieSuccessView()
}

class SelfieSuccessViewController: UIViewController {
    
    var delegate: SelfieSuccessViewControllerDelegate?

    weak var loginDismissDelegate: LoginDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }
    
    @IBAction func touchNext(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            if let delegate = self.delegate {
                delegate.finishShowSelfieSuccessView()
            }
        })
    }
}
