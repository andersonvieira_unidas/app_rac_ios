//
//  SelfieFailureViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 29/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class SelfieFailureViewController: UIViewController {
        
    @IBOutlet weak var borderBackView: UIView!
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func touch_finish(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupView(){
        borderBackView.layer.cornerRadius = 4
        borderBackView.layer.borderWidth = 1
        borderBackView.layer.borderColor = UIColor(red: 255/255, green: 154/255, blue: 22/255, alpha: 1.0).cgColor
    }
}
