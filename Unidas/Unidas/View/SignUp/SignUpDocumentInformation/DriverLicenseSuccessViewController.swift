//
//  DriverLicenseSuccessViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 27/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol DriverLicenseSuccessDelegate: class {
    func finishUpdateDriverLicense()
    func finishCreateDriverLicense()
}

class DriverLicenseSuccessViewController: UIViewController {
    
    @IBOutlet weak var backOrNextButton: UnidasButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    var formAction: FormsAction = .create
    
    weak var driverLicenseSuccessDelegate: DriverLicenseSuccessDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        if formAction == .create {
            changeTextValues(buttonText: NSLocalizedString("Next Button", comment: ""), descriptionText: NSLocalizedString("Driver License - Success Create", comment: ""))
        } else {
            changeTextValues(buttonText: NSLocalizedString("Close Button", comment: ""), descriptionText: NSLocalizedString("Driver License - Success Update", comment: ""))
        }
    }
    
    @IBAction func tapBackOrNextButton(_ sender: Any) {
        if formAction == .create {
            self.dismiss(animated: true) {
                self.driverLicenseSuccessDelegate?.finishCreateDriverLicense()
            }
        } else {
            self.dismiss(animated: true) {
                self.driverLicenseSuccessDelegate?.finishUpdateDriverLicense()
            }
        }
    }
    
    private func changeTextValues(buttonText: String, descriptionText: String){
        backOrNextButton.setTitle(buttonText, for: .normal)
        descriptionLabel.text = descriptionText
    }
}
