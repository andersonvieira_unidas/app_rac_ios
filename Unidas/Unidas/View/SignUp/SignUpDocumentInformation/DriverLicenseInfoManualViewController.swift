//
//  DriverLicenseInfoManualViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 02/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol DriverLicenseInfoManualDelegate: class {
    func showDriverLicenseManual()
}

class DriverLicenseInfoManualViewController: UIViewController {
    
    var formAction: FormsAction = .create
    
    weak var driverLicenseInfoManualDelegate: DriverLicenseInfoManualDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func tapNextButton(_ sender: Any) {
        self.dismiss(animated: true) {
            self.driverLicenseInfoManualDelegate?.showDriverLicenseManual()
        }
    }
}

