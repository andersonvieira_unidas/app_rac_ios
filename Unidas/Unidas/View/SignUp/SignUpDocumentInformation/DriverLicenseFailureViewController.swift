//
//  DriverLicenseFailureViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 27/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class DriverLicenseFailureViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func tapBackButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
