//
//  SignUpDocumentInformationViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol DocumentRecomendationsDelegate: class  {
    func finishUpdateDocumentDriver()
}

class DocumentRecomendationsViewController: UIViewController, CustomCameraDetailsDelegate, DocumentRecomendationsViewDelegate, DriverLicenseSuccessDelegate, DriverLicenseInfoManualDelegate, DriverLicenseDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var finishLaterButton: EnhancedButton!
    
    @IBOutlet weak var borderBackView: UIView!
    @IBOutlet weak var progressBackView: UIView!
    weak var loginDismissDelegate: LoginDismissDelegate?
    var customCameraDetailsViewController: CustomCameraDetailsViewController?
    private var presenter: DocumentRecomendationsPresenter!
    
    weak var delegate: DocumentRecomendationsDelegate?
    
    var formsAction: FormsAction = .create
    private var documentAttempts: Int = 0
    var messageFlagForCNH = false
    var limitAttempts = 2
    var headerCurved = false
    
    let progress = Progress(totalUnitCount: 7)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = DocumentRecomendationsPresenter(accountPhotoUnidasService: AccountPhotoUnidasService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        limitAttempts = RemoteConfigUtil.recoverParameterIntValue(with: "limit_attempts_driver_license")
        setupView()
        configureButtonBack()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupViewWillDisappear()
    }
    
    private func setupView() {
        titleView.isHidden = formsAction.titleIsHidden
        finishLaterButton.isHidden = formsAction.finishLaterIsHidden
        
        if formsAction == .update {
            self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
            //title = NSLocalizedString("Update CNH", comment: "Update cnh navigation title")
        }  
    }
    
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        delegate?.finishUpdateDocumentDriver()
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setupViewDidAppear() {
        if formsAction == .create {
            progress.completedUnitCount = 5
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
        }
        
        if headerCurved {
            setHeaderUnidas()
        }
    }
    
    private func setupViewWillDisappear() {
        if formsAction == .create {
        }
    }
    
    @IBAction func touchFinishLater(_ sender: Any) {
        performSegue(withIdentifier: "showCard", sender: nil)
//        dismiss(animated: true, completion: {
//            self.loginDismissDelegate?.dismissLogin()
//            self.presenter?.addEventSignUpDriverLicenseLaterStep(success: true)
//        })
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        performSegue(withIdentifier: "segueToCamera", sender: nil)
    }
    
    // MARK: - Custom Camera Details Delegate
    
    func acceptPhoto(image: UIImage, viewController: CustomCameraDetailsViewController) {
        let data = image.jpegData(compressionQuality: 0.3)
        
        customCameraDetailsViewController = viewController
        presenter.addDocument(image: data, formAction: formsAction)
    }
    
    // MARK: - SignUpDocumentInformationViewDelegate
    
    func startLoadingCamera() {
        customCameraDetailsViewController?.startLoading()
    }
    
    func endLoadingCamera() {
        customCameraDetailsViewController?.endLoading()
    }
    
    func presentCamera(error: Error) {
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.present(error: error)
        })
    }
    
    func present(error: Error) {
        documentAttempts += 1
        if  self.documentAttempts >=  self.limitAttempts {
            performSegue(withIdentifier: "segueToInfoManual", sender: nil)
            documentAttempts = 0
        }
        performSegue(withIdentifier: "segueToDriverLicenseFailure", sender: nil)
    }
    
    func finishAddDocument() {
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "segueToDriverLicenseSuccess", sender: nil)
        })
    }
    
    //MARK: - DriverLicenseSuccessDelegate
    func finishUpdateDriverLicense() {
        if formsAction == .create {
            performSegue(withIdentifier: "showCard", sender: nil)
        } else if formsAction == .update {
            self.navigationController?.popViewController(animated: true)
            self.delegate?.finishUpdateDocumentDriver()
        }
    }
    
    func finishCreateDriverLicense() {
        performSegue(withIdentifier: "showCard", sender: nil)
    }
    
    //MARK: - DriverLicenseInfoManualDelegate
    func showDriverLicenseManual() {
        performSegue(withIdentifier: "showDriverLicense", sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToCamera" {
            if let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.viewControllers.first as? CustomCameraViewController {
                viewController.devicePosition = .back
                viewController.backgroundImage = #imageLiteral(resourceName: "crop-mask-document_v2")
                viewController.messageFlag = messageFlagForCNH
                viewController.customCameraDetailsDelegate = self
                viewController.showLoadingMessage = true
            }
        }
        else if segue.identifier == "segueToDriverLicenseSuccess" {
            let destination = segue.destination as? DriverLicenseSuccessViewController
            destination?.formAction = formsAction
            destination?.driverLicenseSuccessDelegate = self
        }
        else if segue.identifier == "showCard" {
            let destination = segue.destination as? CreditCardsDetailsFormViewController
            destination?.loginDismissDelegate = loginDismissDelegate
        }
        else if segue.identifier == "segueToInfoManual" {
            let destination = segue.destination as? DriverLicenseInfoManualViewController
            destination?.formAction = formsAction
            destination?.driverLicenseInfoManualDelegate = self
        }
        else if segue.identifier == "showDriverLicense" {
            let destination = segue.destination as? DriverLicenseFormViewController
            destination?.formsAction = formsAction
            destination?.driverLicenseDelegate = self
        }
    }
}

