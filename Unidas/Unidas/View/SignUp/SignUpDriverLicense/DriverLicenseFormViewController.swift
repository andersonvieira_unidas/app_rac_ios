//
//  DriverLicenseFormViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol DriverLicenseDelegate: class {
    func finishUpdateDriverLicense()
}

class  DriverLicenseFormViewController: UIViewController, DriverLicenseFormDelegate, DriverLicenseDetailsFormViewDelegate, SimpleAlertDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var driverLicenseFormView: DriverLicenseFormView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var progressBackView: UIView!
    
    weak var loginDismissDelegate: LoginDismissDelegate?
    weak var driverLicenseDelegate: DriverLicenseDelegate?
    private var presenter: DriverLicenseDetailsFormPresenterDelegate!
    
    var formsAction: FormsAction = .create
    var isPerformSegue: Bool = true
    
    let progress = Progress(totalUnitCount: 7)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = DriverLicenseDetailsFormPresenter(accountsService: AccountsService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        setupView()
        configureButtonBack()
        
        driverLicenseFormView.delegate = self
        registerForKeyboardNotifications()
        setupNavigationController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupViewWillDisappear()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.setHidesBackButton(false, animated: false)
    }
    
    // MARK: - Private Functions
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupViewDidAppear() {
        if formsAction == .create {
            progress.completedUnitCount = 5
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
        }else{
            progressBackView.isHidden = true
        }
    }
    
    private func setupViewWillDisappear() {
        if formsAction == .create {
        }
    }
    
    private func setupView() {
        titleView.isHidden = formsAction.titleIsHidden
        nextButton.setTitle(formsAction.buttonTitle, for: .normal)
    }
    
    private func setupNavigationController() {
        if navigationController?.viewControllers.first == self {
            let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
            navigationItem.leftBarButtonItem = backButton
        }
    }
    
    private func updateAlertMessage(redirect: Bool = false) {
        let title = NSLocalizedString("Success", comment: "Success title")
        let message = NSLocalizedString("Driver License Updated", comment: "Driver License Updated message")
        let image = #imageLiteral(resourceName: "icon-document-menu")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: image)
        simpleAlert.delegate = self
        
        simpleAlert.show(animated: true)
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        presenter.updateDriverLicenseDetails(driverLicenseFormDataView: driverLicenseFormView.driverLicenseFormDataView)
    }
    
    // MARK: - SignUpPersonalInformationViewDelegate
    
    func startLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        nextButton.loading(true, activityIndicatorViewStyle: .white)
        nextButton.isUserInteractionEnabled = false
    }
    
    func endLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        nextButton.loading(false)
        nextButton.isUserInteractionEnabled = true
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func didFinishCreateDriverLicense() {
        updateAlertMessage()
    }
    
    // MARK: - Keyboard events
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        if let keyboardHeight = keyboardFrame?.height {
            let edgeInsets = UIEdgeInsets(top: scrollView.contentInset.top,
                                          left: scrollView.contentInset.left,
                                          bottom: keyboardHeight,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let edgeInsets: UIEdgeInsets = .zero
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
    }
    
    // MARK: - DriverLicenseFormDelegate
    
    func present(error: Error, driverLicenseFormView: DriverLicenseFormView) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    //SimpleAlertDelegate {
    func close() {
        self.navigationController?.popViewController(completion: {
            self.driverLicenseDelegate?.finishUpdateDriverLicense()
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToCard" {
            let destination = segue.destination as? CreditCardsDetailsFormViewController
            destination?.loginDismissDelegate = loginDismissDelegate
        }
    }
}
