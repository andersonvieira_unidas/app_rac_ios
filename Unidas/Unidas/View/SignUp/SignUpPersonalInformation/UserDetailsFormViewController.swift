//
//  SignUpPersonalInformationViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol ValidatePhoneNumberDelegate: class {
    func showMessageSuccess(phoneUpdated: Bool)
}

class UserDetailsFormViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UserDetailsFormViewDelegate, UserFormDelegate, ValidatePhoneNumberDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var userFormView: UserFormView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var progressBackView: UIView!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var userDetailsTableView: UITableView!
    
    var userResponseDataView: UserResponseDataView?
    
    private var presenter: UserDetailsFormPresenterDelegate!
    weak var loginDismissDelegate: LoginDismissDelegate?
    
    var formsAction: FormsAction = .create
    var isPerformSegue: Bool = true
    var changedMobileNumber: Bool = false
    
    let progress = Progress(totalUnitCount: 7)
    
    var offers_array = [NSLocalizedString("App Offer Whatsapp", comment: ""),NSLocalizedString("App Offer Promotions", comment: "")]
    
    lazy var toolbarAccessoryView: UIToolbar = {
        let toolbar = UIToolbar()
        let doneButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(toolbarDoneButtonTapped(sender:)))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.setItems([flexibleSpace, doneButtonItem], animated: false)
        toolbar.sizeToFit()
        return toolbar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderUnidas()
        
        presenter = UserDetailsFormPresenter(accountsService: AccountsService(),
                                             recoveryService: RecoveryService(),
                                             userCheckService: UserCheckService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        setupView()
        userFormView.delegate = self
        userFormView.setTextFieldData(userResponseDataView: userResponseDataView, formsAction: formsAction)
        userFormView.setOldMobilePhone(oldMobilePhone: userResponseDataView?.model.account.mobilePhone)
        userFormView.hiddenTitle(isHidden: true)
        registerForKeyboardNotifications()
        setupNavigationController()
        setupTableView()
        reloadData()
        self.navigationController?.change()
        setHeaderUnidas()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewDidAppear()
        endLoading()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupViewWillDisappear()
    }
    
    private func setupTableView(){
        guard let tableView = userDetailsTableView else { return }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: CheckMarkOffersTableViewCell.self)
        tableView.separatorStyle = .none
    }
    
    func reloadData() {
        guard let tableView = userDetailsTableView else { return }
        tableView.reloadData()
    }
    
    func showMessageSuccess(phoneUpdated: Bool) {
        if phoneUpdated {
            performSegue(withIdentifier: "showValidatePhoneSuccess", sender: nil)
        } else {
            updateAlertMessage()
        }
    }
    
    // MARK: - SignUpPersonalInformationViewDelegate
    
    func startLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        nextButton.loading(true, activityIndicatorViewStyle: .white)
        nextButton.isUserInteractionEnabled = false
    }
    
    func endLoading() {
        nextButton.setTitleColor(#colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1), for: .normal)
        nextButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        nextButton.loading(false)
        nextButton.isUserInteractionEnabled = true
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func didFinishCreateUser(userResponseDataView: UserResponseDataView) {
        performSegue(withIdentifier: "showAddress", sender: userResponseDataView)
    }
    
    func didFinishUpdateUser(userResponseDataView: UserResponseDataView, isPerformSegue: Bool) {
        if isPerformSegue {
            changedMobileNumber = true
            self.performSegue(withIdentifier: "showPhoneValidate", sender: userResponseDataView)
            return
        }
        updateAlertMessage()
    }
    
    // MARK: - Keyboard events
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    // MARK: - Interface builder Actions
    
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        if let keyboardHeight = keyboardFrame?.height {
            let edgeInsets = UIEdgeInsets(top: scrollView.contentInset.top,
                                          left: scrollView.contentInset.left,
                                          bottom: keyboardHeight,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
        }
    }
    
    @objc func touchCancelItem() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        presenter.updateOrCreateUserDetails(userFormDataView: userFormView.userFormDataView, oldPhoneNumber: userFormView.oldMobilePhone, formsAction: formsAction)
        
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let edgeInsets: UIEdgeInsets = .zero
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
    }
    
    @objc func toolbarDoneButtonTapped(sender: UIBarButtonItem) {
        userFormView.cpfTextField.resignFirstResponder()
        userFormView.fullNameTextField.resignFirstResponder()
        userFormView.emailTextField.resignFirstResponder()
        userFormView.phoneTextField.resignFirstResponder()
        userFormView.motherNameTextField.resignFirstResponder()
    }
    
    // MARK: - UserFormDelegate
    
    func present(error: Error, userFormView: UserFormView) {
        let alert = UIAlertController(title: "", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {(alert: UIAlertAction!) in
            
            switch error.localizedDescription {
                
            case NSLocalizedString("Required document", comment: "Required document error description"):
                self.userFormView.documentNumberTextField.becomeFirstResponder()
            case NSLocalizedString("Name is required", comment: "Name is required error description"):
                self.userFormView.fullNameTextField.becomeFirstResponder()
            case NSLocalizedString("Required email", comment: "Required email error description"):
                self.userFormView.emailTextField.becomeFirstResponder()
            case NSLocalizedString("Invalid email", comment: "Invalid email error description"):
                self.userFormView.emailTextField.becomeFirstResponder()
            case NSLocalizedString("Phone number is required", comment: "Required phone number error description"):
                self.userFormView.phoneTextField.becomeFirstResponder()
            case NSLocalizedString("Invalid phone number", comment: "Invalid phone number error description"):
                self.userFormView.phoneTextField.becomeFirstResponder()
            case NSLocalizedString("Birthday is required", comment: "Birthday is required error description"):
                self.userFormView.birthdayTextField.becomeFirstResponder()
            case NSLocalizedString("Invalid birthday date", comment: "Invalid birthday date error description"):
                self.userFormView.birthdayTextField.becomeFirstResponder()
            default:
                break
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Privte Functions
    private func setupView() {
        self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "unidas-circle"))
        nextButton.setTitle(formsAction.buttonTitle, for: .normal)
        
        if formsAction.titleIsHidden {
            progressBackView.isHidden = true
            stepsLabel.isHidden = true
        }
        userFormView.cpfTextField.inputAccessoryView = toolbarAccessoryView
        userFormView.fullNameTextField.inputAccessoryView = toolbarAccessoryView
        userFormView.emailTextField.inputAccessoryView = toolbarAccessoryView
        userFormView.phoneTextField.inputAccessoryView = toolbarAccessoryView
        userFormView.motherNameTextField.inputAccessoryView = toolbarAccessoryView
    }
    
    private func setupViewDidAppear() {
        if formsAction == .create {            
            progress.completedUnitCount = 1
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
        }
    }
    
    private func setupViewWillDisappear() {
        if formsAction == .create {
        }
    }
    
    private func setupNavigationController() {
      //  if formsAction == .update {
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
            navigationItem.leftBarButtonItem = backButton
       // }
    }
    
    private func updateAlertMessage() {
        let message = NSLocalizedString("Personal information updated", comment: "")
        let title =  NSLocalizedString("Personal information title", comment: "")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: #imageLiteral(resourceName:"icon-personal-data-menu"))
        simpleAlert.show(animated: true)
    }
    
    //MARK: - TableView Delegates

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CheckMarkOffersTableViewCell.self)
        cell.cellDescription.text = offers_array[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CheckMarkOffersTableViewCell else { return }
        
        if cell.cellImage.image == #imageLiteral(resourceName: "icon-checkmark-on-general") {
            cell.setup(selected: false)
        }else{
            cell.setup(selected: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToAddress" {
            if let viewController = segue.destination as? AddressDetailsFormViewController {
                viewController.loginDismissDelegate = loginDismissDelegate
                viewController.userResponseDataView = userResponseDataView
            }
        }
        
        if segue.identifier == "showPhoneValidate" {
            
            let viewController = segue.destination as? SignUpValidatePhoneViewController
            viewController?.userDataView = sender as? UserResponseDataView
            viewController?.loginDismissDelegate = loginDismissDelegate
            viewController?.validatePhoneNumberDelegate = self
            viewController?.backButtonHidden = false
            viewController?.formsAction = formsAction
            
            if formsAction == .update && changedMobileNumber {
                viewController?.isValidate = true
            }
            self.endLoading()
        }
    }
}
