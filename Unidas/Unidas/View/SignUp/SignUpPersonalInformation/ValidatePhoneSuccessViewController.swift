//
//  ValidatePhoneSuccessViewController.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 25/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//


import UIKit

class ValidatePhoneSuccessViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
         return .lightContent
     }
  
    @IBAction func tapClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

