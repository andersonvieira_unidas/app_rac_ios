//
//  SignUpPhotoInformationViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class SelfieRecomendationsViewController: UIViewController, CustomCameraDetailsDelegate, SelfieRecomendationsViewDelegate, SelfieSuccessViewControllerDelegate {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var finishLaterButton: EnhancedButton!
    @IBOutlet weak var updateTitleView: UIView!
    @IBOutlet weak var updateTitleViewConstantHeight: NSLayoutConstraint!
    @IBOutlet weak var titleInstructionsLabel: UILabel!
    
    var formsAction: FormsAction = .create
    weak var loginDismissDelegate: LoginDismissDelegate?
    private var presenter: SelfieRecomendationsPresenter!
    
    var customCameraDetailsViewController: CustomCameraDetailsViewController?
    
    @IBOutlet weak var progressBackView: UIView!
    @IBOutlet weak var borderBackView: UIView!
    
    let progress = Progress(totalUnitCount: 7)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        presenter = SelfieRecomendationsPresenter(accountPhotoUnidasService: AccountPhotoUnidasService(),
                                                  imageTemporaryDirectory: ImageTemporaryDirectory(accountImageService: AccountImageService()), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        setupView()
        configureBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupViewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupViewWillDisappear()
    }
    
    //MARK: - Private Functions
    private func setupViewDidAppear() {
        if formsAction == .create {
            progress.completedUnitCount = 4
            let progressFloat = Float(self.progress.fractionCompleted)
            self.progressView.setProgress(progressFloat, animated: false)
        }
    }
    
    private func setupViewWillDisappear() {
        if formsAction == .create {
            updateTitleView.isHidden = true
            updateTitleViewConstantHeight.constant = 0
        }
    }
    
    private func setupView() {        
        titleInstructionsLabel.isHidden = formsAction.titleIsHidden
        titleView.isHidden = formsAction.titleIsHidden
        finishLaterButton.isHidden = formsAction.finishLaterIsHidden
    }
    
    private func configureBackButton(){
        if formsAction == .update {
            let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
            backButton.tintColor = UIColor.white
            navigationItem.leftBarButtonItem = backButton
        } else {
            self.navigationItem.hidesBackButton = true
        }
        
    }
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func touchFinishLater(_ sender: Any) {
        performSegue(withIdentifier: "segueToDocumentTutorial", sender: nil)
//        dismiss(animated: true, completion: {
//            self.loginDismissDelegate?.dismissLogin()
//            self.presenter?.addEventSignUpPersonalPhotoContinueLaterStep(success: true)
//        })
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        performSegue(withIdentifier: "segueToCamera", sender: nil)
    }
    
    // MARK: - Custom Camera Details Delegate
    
    func acceptPhoto(image: UIImage, viewController: CustomCameraDetailsViewController) {
        let data = image.jpegData(compressionQuality: 0.3)
        
        customCameraDetailsViewController = viewController
        presenter.addDocument(image: data, formsAction: formsAction)
    }
    
    //MARK: - SelfieSuccessDelegates
    
    func finishShowSelfieSuccessView() {
        if formsAction == .create {
            performSegue(withIdentifier: "segueToDocumentTutorial", sender: nil)
        }else{
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func showSelfieFailureView() {        
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "segueToSelfieFailure", sender: nil)
        })
    }
    
    // MARK: - SignUp Photo Information View Delegate
    
    func startLoadingCamera() {
        customCameraDetailsViewController?.startLoading()
    }
    
    func endLoadingCamera() {
        customCameraDetailsViewController?.endLoading()
    }
    
    func presentCamera(error: Error) {
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.present(error: error)
        })
    }
    
    func presentCustomAlert(error: Error) {
        let title = NSLocalizedString("Unidas", comment: "Loja Indisponível")
        let subTitle = NSLocalizedString("\(error.localizedDescription)", comment: "Loja Indisponível")
        let alert = SimpleAlert(title: title, subTitle: subTitle, image:#imageLiteral(resourceName: "icon-erro2"))
        
        alert.show(animated: true)
    }
    
    func present(error: Error) {
        presentCustomAlert(error: error)
    }
    
    func finishAddDocument() {
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "segueToSelfieSuccess", sender: nil)
        })
    }
    
    func finishUpdateDocument() {
        customCameraDetailsViewController?.dismiss(animated: true, completion: {
            self.performSegue(withIdentifier: "segueToSelfieSuccess", sender: nil)
        })
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToCamera" {
            if let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.viewControllers.first as? CustomCameraViewController {
                viewController.devicePosition = .front
                viewController.backgroundImage = #imageLiteral(resourceName: "crop-mask_v2")
                viewController.modalPresentationStyle = .fullScreen
                viewController.customCameraDetailsDelegate = self
            }
        }
        else if segue.identifier == "segueToDocumentTutorial" {
            let destination = segue.destination as? DocumentRecomendationsViewController
            destination?.loginDismissDelegate = loginDismissDelegate
        }
        else if segue.identifier == "segueToSelfieSuccess" {
            if let destination = segue.destination as? SelfieSuccessViewController {
                destination.loginDismissDelegate = loginDismissDelegate
                destination.delegate = self
            }
        }
        else if segue.identifier == "segueToSelfieFailure" {
            if let destination = segue.destination as? SelfieFailureViewController{
                destination.loginDismissDelegate = loginDismissDelegate
            }
        }
    }
}
