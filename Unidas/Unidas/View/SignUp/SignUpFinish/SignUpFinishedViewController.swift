//
//  SignUpFinishedViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 02/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

class SignUpFinishedViewController: UIViewController {

    @IBOutlet weak var finishView: UIView!
    weak var loginDismissDelegate: LoginDismissDelegate?
    private var firebaseAnalyticsService = FirebaseAnalyticsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        navigationController?.setNavigationBarHidden(true, animated: false)
        setupView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupView() {
        guard let _ = session.currentUser else { return }
        
        //let userResponseDataView = UserResponseDataView(model: currentUser)
        
        self.navigationController?.title = ""
        
        finishView.layer.borderWidth = 1
        finishView.layer.borderColor = UIColor(red: 255/255, green: 154/255, blue: 22/255, alpha: 1).cgColor
    }
    
    func addEventSignUpReadyStep(success: Bool) {
        var parameters: [String : Any] = [:]
        parameters["sign_up_ready"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.signUpReady, withValue: parameters)
    }
    
    @IBAction func touchDismiss(_ sender: UIButton) {
        dismiss(animated: true) {
            self.addEventSignUpReadyStep(success: true)            
            if let loginDelegate = self.loginDismissDelegate {
                loginDelegate.dismissLogin()
            }
        }
    }
}
