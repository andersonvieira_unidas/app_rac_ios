//
//  SignUpSignatureInformationViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 27/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import SafariServices

class SignatureRecomendationsViewController: UIViewController, UITextViewDelegate, SignatureRecomendationsViewDelegate, CustomSignatureDelegate, SFSafariViewControllerDelegate, JumpCustumSignatureDelegate  {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var acceptButton: CheckedButton!
    @IBOutlet weak var acceptPrivacyButton: CheckedButton!
    @IBOutlet weak var termsTextView: UITextView!
    @IBOutlet weak var termsPrivacyTextView: UITextView!
    @IBOutlet weak var progressBackView: UIView!
    
    private var presenter: SignatureRecomendationsPresenterDelegate!
    
    let progress = Progress(totalUnitCount: 7)
    
    private var attributedString: NSMutableAttributedString = NSMutableAttributedString(string: NSLocalizedString("I declare that I have read and agree to the Account Opening Policy and the Terms and Conditions.", comment: "Declaro que li e estou  de acordo com a Política de Abertura de Conta e com os Termos e Condições de Uso."), attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
    ])

    weak var loginDismissDelegate: LoginDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = SignatureRecomendationsPresenter(accountPhotoUnidasService: AccountPhotoUnidasService(),
                                                     companyTermsService: CompanyTermsService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        configureTextView()
        configureBackButton()
        presenter.fetchTerms()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        progress.completedUnitCount = 7
        let progressFloat = Float(self.progress.fractionCompleted)
        self.progressView.setProgress(progressFloat, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func touchAccept(_ sender: CheckedButton) {
        sender.isChecked = !sender.isChecked
        
        let labelFont = sender.isChecked ? UIFont.bold(ofSize: 12) : UIFont.regular(ofSize: 12)
        
        if sender == acceptButton {
            termsTextView.font = labelFont
        }else if sender == acceptPrivacyButton {
            termsPrivacyTextView.font = labelFont
        }
    }
    
    @IBAction func touchAcceptTerms(_ sender: UIButton) {
        presenter.finishAcceptTerms(terms: acceptButton.isChecked, terms2: acceptPrivacyButton.isChecked)
    }
    
    @IBAction func touchLater(_ sender: Any) {
        performSegue(withIdentifier: "segueToSignature", sender: nil)
//        dismiss(animated: true, completion: {
//            self.presenter.addEventSignUpSignatureLaterStep(success: true)
//            self.loginDismissDelegate?.dismissLogin()
//        })
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        safariUrlOpen(withUrl: URL)
        return false
    }
    
    private func configureTextView() {
        termsTextView.isEditable = false
        termsTextView.delegate = self
        
        termsPrivacyTextView.isEditable = false
        termsPrivacyTextView.delegate = self
    }
    
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - SignUpPersonalInformationViewDelegate
    
    func jumpSignature() {
        self.performSegue(withIdentifier: "showFinished", sender: nil)
    }
    
    func showTerms(openingPolicy: CompanyTermsDataView, termsAndConditionsOfUse: CompanyTermsDataView) {
        
        let openingPolicyLink = openingPolicy.description
        let termsAndConditionsOfUseLink = termsAndConditionsOfUse.description
        
        let attributeText = attributedString.string
        let openingPolicyTermsText = NSLocalizedString("Account Opening Policy", comment: "Política de Abertura de conta")
        
        if let accountTermsRange = attributeText.range(of: openingPolicyTermsText) {
            let accountsTermsNSRange = NSRange(accountTermsRange, in: attributeText)
            attributedString.addAttribute(.link, value: openingPolicyLink, range: accountsTermsNSRange)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.regular(ofSize: 12), range: accountsTermsNSRange)
        }
        
        let termsAndConditionsOfUseTermsText = NSLocalizedString("Terms and Conditions", comment: "Termos e Condições de Uso")
        if let termsAndConditions = attributeText.range(of: termsAndConditionsOfUseTermsText) {
            let termsAndConditionsNSRange = NSRange(termsAndConditions, in: attributeText)
            attributedString.addAttribute(.link, value: termsAndConditionsOfUseLink, range: termsAndConditionsNSRange)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.regular(ofSize: 12), range: termsAndConditionsNSRange)
        }
        termsTextView.linkTextAttributes = NSAttributedString.linkAttributed
        termsTextView.attributedText = attributedString
    }
    
    func showPrivacyPolicyTerms() {

        let description = NSLocalizedString("I declare that I have read and agree with the Terms of Privacy and Protection of Personal Data.", comment: "Declaro que li e concordo com os Termos de Privacidade e Proteção de Dados Pessoais.")
        
        let linkText = NSLocalizedString("Privacy Policy Terms", comment: "")
        let colorFont = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        let font = UIFont.regular(ofSize: 12)
        let urlPolicy = RemoteConfigUtil.recoverParameterValue(with: "privacy_policy_terms")
        
        let rangeLink = (description as NSString).range(of: linkText)
        let policyText = NSMutableAttributedString(string: description, attributes: [NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor: colorFont])
        policyText.addAttribute(.link, value: urlPolicy, range: rangeLink)
                
        policyText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.foregroundColor, value: colorFont, range: rangeLink)
        policyText.addAttribute(NSAttributedString.Key.font, value: font, range: rangeLink)
        
        termsPrivacyTextView.linkTextAttributes = NSAttributedString.linkAttributed
        termsPrivacyTextView.attributedText = policyText
    }
    
    func startLoading() {
        
    }
    
    func endLoading() {
        
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func finishAcceptTerms() {
        performSegue(withIdentifier: "segueToSignature", sender: nil)
    }
    
    func finishUploadSignature(viewController: CustomSignatureViewController) {
        viewController.dismiss(animated: true) {
            self.performSegue(withIdentifier: "showFinished", sender: nil)
        }
    }
    
    func customSignatureStartLoading(viewController: CustomSignatureViewController) {
        viewController.startLoading()
    }
    
    func customSignatureEndLoading(viewController: CustomSignatureViewController) {
        viewController.endLoading()
    }
    
    func customSignaturePresent(error: Error, viewController: CustomSignatureViewController) {
        viewController.present(error: error)
    }
    
    //MARK: - CustomSignatureViewDelegate
    
    func acceptSignature(data: Data, viewController: CustomSignatureViewController) {
        presenter.addSignature(image: data, viewController: viewController)
    }
    
    //MARK: - SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    private func safariUrlOpen(withUrl url: URL){
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.delegate = self
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToSignature" {
            let destination = segue.destination as? CustomSignatureViewController
            
            destination?.loginDismissDelegate = loginDismissDelegate
            destination?.delegate = self
            destination?.jumpDelegate = self
        }
        else if segue.identifier == "showFinished" {
            let destination = segue.destination as? SignUpFinishedViewController
            destination?.loginDismissDelegate = loginDismissDelegate
        }
    }
}
