//
//  AfterTermsViewController.swift
//  Unidas
//
//  Created by Felipe Machado on 11/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import SafariServices

class AfterTermsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate, OpenUrlDelegate, AfterTermsViewDelegate {
    
    @IBOutlet weak var termsTableView: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    
    lazy var presenter: AfterTermsPresenterDelegate = {
        let presenter = AfterTermsPresenter(postTermsService: PostTermsService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        return presenter
    }()
    
    var terms_array = [NSLocalizedString("I declare that I have read and agree with the Terms of Privacy and Protection of Personal Data.", comment: "")]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        reloadData()
    }
    
    func setupTableView() {
        guard let tableView = termsTableView else { return }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: CheckMarkOffersTableViewCell.self)
        tableView.separatorStyle = .none
    }
    
    func reloadData() {
        guard let tableView = termsTableView else { return }
        tableView.reloadData()
    }
    
    func loadingScreen(value: Bool) {
        if value {
            LoadingScreen.setLoadingScreen(view: self.view)
        }else{
            LoadingScreen.removeLoadingScreen(view: self.view)
        }
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func updateTerms() {
        presenter.sendTermsToServer(privacyTerm: true)
    }
    
    func validateTerms() {
        if let cell = termsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CheckMarkOffersTableViewCell {
            if cell.cellImage.image == #imageLiteral(resourceName: "icon_afterTerms_full_elipse") {
                updateTerms()
            }else{
                let title = NSLocalizedString("Private and Personal Data Protection", comment: "")
                let description = NSLocalizedString("In order to continue you must accept the privacy policy", comment: "")
                let simpleAlert = SimpleAlert(title: title, subTitle: description, image: #imageLiteral(resourceName: "icon-alert"))
                simpleAlert.show(animated: true)
            }
        }
    }
    
    @IBAction func dismissController(_ sender: UIButton) {
        validateTerms()
    }
    
    func openUrl(with url: URL) {
        safariUrlOpen(withUrl: url)
    }
    
    private func safariUrlOpen(withUrl url: URL){
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.clearCache()
        safariViewController.delegate = self
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    //MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return terms_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CheckMarkOffersTableViewCell.self)
        
        guard let url = URL(string: RemoteConfigUtil.recoverParameterValue(with: "privacy_policy_terms")) else {return UITableViewCell()}
        cell.setUrl(description: terms_array[indexPath.row], urlPolicy: url, font: UIFont.regular(ofSize: 12))
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CheckMarkOffersTableViewCell else { return }
        
        if cell.cellImage.image == #imageLiteral(resourceName: "icon_afterTerms_full_elipse") {
            cell.setup(selected: false)
        }else{
            cell.setup(selected: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
