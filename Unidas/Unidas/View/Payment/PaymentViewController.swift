//
//  PaymentViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 07/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

protocol PaymentProtocol: class {
    func reloadScreen()
}

class PaymentViewController: UIViewController, ListCardDelegate, PaymentViewDelegate, CreditCardReservePaymentDelegate, CreditCardPrePaymentDelegate, ReserveCompletedDelegate, CreateCardDelegate, SimpleAlertDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var paymentBorderView: UIView!
    @IBOutlet weak var prePaymentBorderView: UIView!
    @IBOutlet weak var prePaymentBackView: EnhancedView!
    
    var complementContentViewSize = false
    
    @IBOutlet weak var paymentBottonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var prePaymentBottonViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var agreementsTermsView: UIStackView!
    @IBOutlet weak var agrementsTermsButton: CheckedButton!
    @IBOutlet weak var confirmPaymentButton: UIButton!
    @IBOutlet weak var changeCardReserveButton: UnidasButton!
    @IBOutlet weak var changeCardPreAuthorizationButton: UnidasButton!
    @IBOutlet weak var preAuthorizationAcitivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var creditCardReservePaymentView: CreditCardReservePaymentView!
    @IBOutlet weak var creditCardPrePaymentView: CreditCardPrePaymentView!
    @IBOutlet weak var checkinReservationNumberLabel: UILabel?
    
    @IBOutlet weak var reserveInfoPaymentView: UIView!
    @IBOutlet weak var reserveInfoPaymentContentView: UIView!
    @IBOutlet weak var reservePaymentChangeCardInfoLabel: UILabel!
    @IBOutlet weak var reserveInfoPaymentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var reserveInfoPaymentContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var reserveInfoPreAuthorizationContentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var viewContentHeight: NSLayoutConstraint!
    
    @IBOutlet weak var reserveInfoPreAuthorizationView: UIView!
    @IBOutlet weak var preAuthorizationChangeCardInfoLabel: UILabel!
    
    @IBOutlet weak var preAuthorizationContentView: UIView!
    @IBOutlet weak var reserveInfoPrePaymentViewHeight: NSLayoutConstraint!
    
    var reserveInfoPreAuthorizationContentViewHeightDefault: Int = 0
    var reserveInfoPaymentContentViewHeightDefault: Int = 0
    
    var isPassStepPaymentCheckinView = false
    
    var vehicleReservationsDataView: VehicleReservationsDataView!
    private var presenter: PaymentPresenterDelegate!
    weak var delegate: PaymentProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = PaymentPresenter(paymentService: PaymentService(),
                                     vehicleReservationsDataView: vehicleReservationsDataView,
                                     contractService: ContractService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        presenter.viewDidLoad()
        
        setupNavigationController()
        registerForKeyboardNotifications()
        setupCreditCardReservePaymentView()
        setupCreditCardPrePaymentView()
        setupReserveInfoPaymentView()
        setupAgrementsTerms()
        
        reserveInfoPreAuthorizationContentViewHeightDefault = Int(reserveInfoPreAuthorizationContentViewHeight.constant)
        reserveInfoPaymentContentViewHeightDefault = Int(reserveInfoPaymentContentViewHeight.constant)
        
        confirmPaymentButton.setBackgroundColor(color: .lightGray, forState: .disabled)
        confirmPaymentButton.setTitleColor(.white, for: .disabled)
    }
    
    private func setupCreditCardPrePaymentView() {
        let hiddenLegalEntityCheckinPrePayment = vehicleReservationsDataView.hiddenLegalEntityCheckinPrePayment
        
        if !hiddenLegalEntityCheckinPrePayment {
            creditCardPrePaymentView.delegate = self
        }
    }
    
    private func setupCreditCardReservePaymentView() {
        let hiddenLegalEntityCheckinPayment = vehicleReservationsDataView.hiddenLegalEntityCheckinPayment
        
        if !hiddenLegalEntityCheckinPayment {
            creditCardReservePaymentView.delegate = self
            creditCardReservePaymentView.setInstallments(at: vehicleReservationsDataView.totalValue)
        }
    }
    
    func setupReserveInfoPaymentView() {
        
        let hiddenLegalEntityCheckinPayment = vehicleReservationsDataView.hiddenLegalEntityCheckinPayment
        
        if hiddenLegalEntityCheckinPayment {
            // isHiddenPaymentView(true)
        } else {
            let checkinPaymentView = CheckinPaymentView.loadFromNib()
            
            checkinPaymentView.setup(remoteConfigTitleKey: "checkin_car_rental_payment_title",
                                     remoteConfigMessageKey: "checkin_car_rental_payment_message",
                                     money: vehicleReservationsDataView.totalValueString ?? "-", showMoney: true, backgroundColor: #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1))
            
            checkinPaymentView.frame = CGRect(x: 0, y: 0 , width: reserveInfoPaymentView.frame.width, height: reserveInfoPaymentView.frame.height)
            reserveInfoPaymentView.addSubview(checkinPaymentView)
            
            reservePaymentChangeCardInfoLabel.text = RemoteConfigUtil.recoverParameterValue(with: "checkin_change_card_info")
        }
    }
    
    private func setupReserveInfoPreAuthorizationView(value: String) {
        
        let hiddenLegalEntityCheckinPrePayment = vehicleReservationsDataView.hiddenLegalEntityCheckinPrePayment
        
        if hiddenLegalEntityCheckinPrePayment {
            isHiddenPrePaymentView(true)
        } else {
            setupCheckinPreAuthorizationViewNib(value: value)
        }
    }
    
    private func setupCheckinPreAuthorizationViewNib(value: String) {
        let checkinPaymentView = CheckinPaymentView.loadFromNib()
        
        let color = reserveInfoPaymentView.isHidden ? true : false
        let background = color ? #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1) : #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
        
        prePaymentBorderView.backgroundColor = background
        prePaymentBackView.backgroundColor = background
        
        checkinPaymentView.setup(remoteConfigTitleKey: "checkin_pre_authorization_payment_title",
                                 remoteConfigMessageKey: "checkin_car_rental_payment_message_new",
                                 money: value, showMoney: false, backgroundColor: background)
        
        checkinPaymentView.frame = CGRect(x: 0, y: 0 , width: reserveInfoPreAuthorizationView.frame.width, height: reserveInfoPreAuthorizationView.frame.height)
        reserveInfoPreAuthorizationView.addSubview(checkinPaymentView)
        
        preAuthorizationChangeCardInfoLabel.text = RemoteConfigUtil.recoverParameterValue(with: "checkin_change_card_info")
    }
    
    private func setupNavigationController() {
        self.navigationController?.change()
        
        // if self.navigationItem.titleView == nil && self.navigationItem.title == nil {
        //     self.navigationItem.titleView = UIImageView(image: #imageLiteral(resourceName: "logo-nav-light-content"))
        // }
        
        let reserve = vehicleReservationsDataView.reserveNumber
        let localizadFormat = NSLocalizedString("Check-in reserve %@", comment: "Check-in reserve navigation title").uppercased()
        
        guard let checkinReservation = checkinReservationNumberLabel else { return }
        
        checkinReservation.text = String(format: localizadFormat, reserve)
        
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-back-button"), style: .done, target: self, action: #selector(touchCancelItem))
        backButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = backButton
    }
    
    private func setupAgrementsTerms() {
        if !vehicleReservationsDataView.isPassStepPaymentCheckin {
            agreementsTermsView.isHidden = false
        }
        
        let tapGestureAgrementsTerms = UITapGestureRecognizer(target: self, action: #selector(tapAgrementsTerms))
        agreementsTermsView.addGestureRecognizer(tapGestureAgrementsTerms)
    }
    
    func setupPassStepPaymentCheckinView() {
        isPassStepPaymentCheckinView = true
        creditCardReservePaymentView.isHidden = true
        creditCardPrePaymentView.isHidden = true
        agrementsTermsButton.isHidden = true
        reserveInfoPaymentView.isHidden = true
        reserveInfoPaymentContentView.isHidden = false
        reserveInfoPaymentContentViewHeight.constant = 30
        reserveInfoPaymentViewHeight.constant = 0
        paymentBottonViewHeight.constant = 30
        prePaymentBorderView.isHidden = true
        paymentBorderView.isHidden = true
        reserveInfoPreAuthorizationView.isHidden = true
        preAuthorizationContentView.isHidden = true
        agreementsTermsView.isHidden = true
        changeCardReserveButton.isHidden = true
        reservePaymentChangeCardInfoLabel.isHidden = true
        confirmPaymentButton.isHidden = true
    }
    
    private func enableFields(_ enabled: Bool) {
        changeCardReserveButton.isEnabled = enabled
        changeCardPreAuthorizationButton.isEnabled = enabled
        agreementsTermsView.isUserInteractionEnabled = enabled
    }
    
    @objc func tapAgrementsTerms() {
        agrementsTermsButton.isChecked = !agrementsTermsButton.isChecked
    }
    
    @objc func touchCancelItem() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchChangeCreditCard(_ sender: UIButton) {
        presenter.changeCard(paymentCardSelection: PaymentCardSelection(rawValue: sender.tag))
    }
    
    @IBAction func touchConfirmPayment(_ sender: Any) {
        
        if vehicleReservationsDataView.isLegalEntity {
            
            let creditCardReservePaymentDataView = creditCardReservePaymentView.creditCardReservePaymentDataView != nil ? creditCardReservePaymentView.creditCardReservePaymentDataView: nil
            let creditCardPrePaymentDataView = creditCardPrePaymentView.creditCardPrePaymentDataView != nil ? creditCardPrePaymentView.creditCardPrePaymentDataView : nil
            
            presenter.performLegalEntityPayment(vehicleReservations: vehicleReservationsDataView,
                                                paymentCard: creditCardReservePaymentDataView,
                                                prePayment: creditCardPrePaymentDataView,
                                                agreements: agrementsTermsButton.isChecked)
            
        } else {
            if presenter.isReservePayment {
                if let creditCardPrePaymentDataView = creditCardPrePaymentView.creditCardPrePaymentDataView {
                    presenter.performPayment(reserveNumber: vehicleReservationsDataView.reserveNumber,
                                             paymentCard: nil, prePayment: creditCardPrePaymentDataView,
                                             agreements: agrementsTermsButton.isChecked)
                }
                
                return
            }
            
            guard let creditCardReservePaymentDataView = creditCardReservePaymentView.creditCardReservePaymentDataView, let creditCardPrePaymentDataView = creditCardPrePaymentView.creditCardPrePaymentDataView else { return }
            
            presenter.performPayment(reserveNumber: vehicleReservationsDataView.reserveNumber,
                                     paymentCard: creditCardReservePaymentDataView,
                                     prePayment: creditCardPrePaymentDataView,
                                     agreements: agrementsTermsButton.isChecked)
        }
    }
    
    // MARK: - ListCardDelegate
    
    func selectedCard(at listCardDataView: ListCardDataView) {
        presenter.updateCardInformation(listCardDataView: listCardDataView)
    }
    
    // MARK: - PaymentPresenterDelegate
    
    func showCards() {
        confirmPaymentButton.setTitleColor(.white, for: .normal)
        confirmPaymentButton.backgroundColor = #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        confirmPaymentButton.alpha = 1.0
        confirmPaymentButton.isUserInteractionEnabled = true
        creditCardReservePaymentView.isHidden = false
        creditCardPrePaymentView.isHidden = false
        
        changeCardReserveButton.setTitle(NSLocalizedString("Change card", comment: "Change card description"), for: .normal)
        changeCardPreAuthorizationButton.setTitle(NSLocalizedString("Change card", comment: "Change card description"), for: .normal)
        
        if vehicleReservationsDataView.isPaid {
            isHiddenPaymentView(true)
            if complementContentViewSize {
                setupCreditCardReservePaymentView()
                setupCreditCardPrePaymentView()
                setupReserveInfoPaymentView()
                DispatchQueue.main.async {
                    self.prePaymentBottonViewHeight.constant = 150
                    self.viewContentHeight.constant = self.contentView.frame.height + 180
                }
            }
        }else{
            if complementContentViewSize {
                setupCreditCardReservePaymentView()
                setupCreditCardPrePaymentView()
                setupReserveInfoPaymentView()
                DispatchQueue.main.async {
                    self.paymentBottonViewHeight.constant = 100
                    self.prePaymentBottonViewHeight.constant = 150
                    self.viewContentHeight.constant = self.contentView.frame.height - 80
                }
            }
        }
    }
    
    func emptyCard() {
        confirmPaymentButton.setTitleColor(.white, for: .normal)
        confirmPaymentButton.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.7058823529, blue: 0.7058823529, alpha: 1)
        confirmPaymentButton.alpha = 0.5
        confirmPaymentButton.isUserInteractionEnabled = false
        creditCardReservePaymentView.isHidden = true
        creditCardPrePaymentView.isHidden = true
        
        
        if vehicleReservationsDataView.isPaid {
            reserveInfoPaymentViewHeight.constant = 0
        }else{
            reserveInfoPaymentViewHeight.constant = 100
        }
        
        paymentBottonViewHeight.constant = 30
        prePaymentBottonViewHeight.constant = 30
        
        reserveInfoPaymentContentViewHeight.constant = reserveInfoPaymentContentViewHeight.constant -
            creditCardReservePaymentView.frame.height
        
        reserveInfoPreAuthorizationContentViewHeight.constant = reserveInfoPreAuthorizationContentViewHeight.constant - creditCardPrePaymentView.frame.height
        
        viewContentHeight.constant = contentView.frame.height - ( creditCardReservePaymentView.frame.height + creditCardPrePaymentView.frame.height)
        
        changeCardReserveButton.setTitle(NSLocalizedString("Add Credit Card", comment: "Add Credit Card description"), for: .normal)
        changeCardPreAuthorizationButton.setTitle(NSLocalizedString("Add Credit Card", comment: "Add Credit Card description"), for: .normal)
    }
    
    func adjustConstraints() {
        reserveInfoPaymentContentViewHeight.constant = CGFloat(reserveInfoPaymentContentViewHeightDefault)
        reserveInfoPreAuthorizationContentViewHeight.constant = CGFloat(reserveInfoPreAuthorizationContentViewHeightDefault)
        
        viewContentHeight.constant = contentView.frame.height + (reserveInfoPaymentViewHeight.constant + reserveInfoPaymentContentViewHeight.constant)
    }
    
    func startLoadingPreAuthorizationValue() {
        preAuthorizationAcitivityIndicator.startAnimating()
    }
    
    func endLoadingPreAuthorizationValue() {
        preAuthorizationAcitivityIndicator.stopAnimating()
    }
    
    func startLoading() {
        confirmPaymentButton.loading(true)
        enableFields(false)
    }
    
    func endLoading() {
        confirmPaymentButton.loading(false)
        enableFields(true)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func present(withDismiss: Bool, error: Error) {
        
        let title = NSLocalizedString("Error title", comment: "")
        let image = #imageLiteral(resourceName: "icon-more-about")
        
        let simpleAlert = SimpleAlert(title: title, subTitle: error.localizedDescription, image: image)
        simpleAlert.delegate = self
        simpleAlert.show(animated: true)
    }
    
    func cardCreated() {
        presenter.performFetchCard(createdNew : true)
        complementContentViewSize = true
    }
    
    func performCard() {
        performSegue(withIdentifier: "showCard", sender: nil)
    }
    
    func showCreateCard() {
        performSegue(withIdentifier: "showCreateCard", sender: nil)
    }
    
    func updateReserveCardInformation(listCardDataView: ListCardDataView) {
        creditCardReservePaymentView.setCard(at: listCardDataView)
    }
    
    func updatePreAuthorizationCardInformation(listCardDataView: ListCardDataView) {
        creditCardPrePaymentView.setCard(at: listCardDataView)
    }
    
    func setupView(listCardDataView: ListCardDataView) {
        creditCardReservePaymentView.setCard(at: listCardDataView)
        creditCardPrePaymentView.setCard(at: listCardDataView)
    }
    
    func present(message: String) {
        alertError(errorMessage: message)
    }
    
    
    func finishPayment(preAuthorizationCardDataView: ListCardDataView?, reservationCardDataView: ListCardDataView?, vehicleReservationsDataView: VehicleReservationsDataView, openContractDataView: OpenContractDataView, preAuthorizationDataView: PreAuthorizationEstimateDataView?, installments: Int?) {
        let sender: [String: Any?] = [
            "preAuthorizationCardDataView": preAuthorizationCardDataView,
            "reservartionCardDataView": reservationCardDataView,
            "vehicleReservationsDataView": vehicleReservationsDataView,
            "openContractDataView": openContractDataView,
            "preAuthorizationDataView": preAuthorizationDataView,
            "installments": installments
        ]
        
        performSegue(withIdentifier: "showReserveCompleted", sender: sender)
    }
    
    func showPreAuthorizationValue(preAuthorizationDataView: PreAuthorizationEstimateDataView) {
        setupReserveInfoPreAuthorizationView(value: preAuthorizationDataView.value)
        creditCardPrePaymentView.setAmount(at: Double(preAuthorizationDataView.model.value))
    }
    
    func isHiddenPaymentView(_ isHidden: Bool) {
        reserveInfoPaymentView.isHidden = isHidden
        reserveInfoPaymentContentView.isHidden = isHidden
        paymentBorderView.isHidden = isHidden
        
        if isHidden {
            
            viewContentHeight.constant = contentView.frame.height - (reserveInfoPaymentViewHeight.constant + reserveInfoPaymentContentViewHeight.constant)
            
            reserveInfoPaymentViewHeight.constant = 0
            reserveInfoPaymentContentViewHeight.constant = 0
            
            if vehicleReservationsDataView.isPaid {
                //viewContentHeight.constant -= 220
            }
        }
        
        if isPassStepPaymentCheckinView {
            setupPassStepPaymentCheckinView()
        }
    }
    
    func isHiddenPrePaymentView(_ isHidden: Bool) {
        preAuthorizationContentView.isHidden = isHidden
        reserveInfoPreAuthorizationView.isHidden = isHidden
        
        if isHidden {
            viewContentHeight.constant = contentView.frame.height - (reserveInfoPreAuthorizationContentViewHeight.constant + reserveInfoPrePaymentViewHeight.constant)
            
            reserveInfoPrePaymentViewHeight.constant = 0
            reserveInfoPreAuthorizationContentViewHeight.constant = 0
            
            if vehicleReservationsDataView.isPreAuthorizationPaid {
                //viewContentHeight.constant -= 220
            }
        }
    }
    
    // MARK: - Keyboard events
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardInfo = notification.userInfo
        let keyboardFrame = keyboardInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        if let keyboardHeight = keyboardFrame?.height {
            let edgeInsets = UIEdgeInsets(top: scrollView.contentInset.top,
                                          left: scrollView.contentInset.left,
                                          bottom: keyboardHeight,
                                          right: scrollView.contentInset.right)
            scrollView.contentInset = edgeInsets
            scrollView.scrollIndicatorInsets = edgeInsets
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        let edgeInsets: UIEdgeInsets = .zero
        scrollView.contentInset = edgeInsets
        scrollView.scrollIndicatorInsets = edgeInsets
    }
    
    
    // MARK: - ReserveCompletedDelegate
    func dismissReserveCompleted() {
        self.dismiss(animated: true) {
            self.delegate?.reloadScreen()
        }
    }
    
    //MARK: - SimpleAlertDelegate
    func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCard" {
            let destination = segue.destination as? UINavigationController
            let viewController = destination?.viewControllers.first as? ListCardCollectionViewController
            viewController?.delegate = self
        }
        else if segue.identifier == "showCreateCard" {
            let destination = segue.destination as? UINavigationController
            let viewController = destination?.viewControllers.first as? CreateCardViewController
            viewController?.delegate = self
        }
        else if segue.identifier == "showReserveCompleted" {
            if  let destination = segue.destination as? UINavigationController,
                let viewController = destination.viewControllers.first as? ReserveCompletedViewController,
                let sender = sender as? [String:Any?] {
                
                viewController.preAuthorizationCardDataView = sender["preAuthorizationCardDataView"] as? ListCardDataView
                viewController.reservationCardDataView = sender["reservartionCardDataView"] as? ListCardDataView
                viewController.vehicleReservationsDataView = sender["vehicleReservationsDataView"] as? VehicleReservationsDataView
                viewController.openContractDataView = sender["openContractDataView"] as? OpenContractDataView
                viewController.preAuthorizationDataView = sender["preAuthorizationDataView"] as? PreAuthorizationEstimateDataView
                viewController.installments = sender["installments"] as? Int
                viewController.delegate = self
                
            }
        }
    }
}
