//
//  StandartCarInformationMensalMais.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit


class StandartCarInformationMensalMaisViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    var message: String?
    
    override func viewDidLoad() {
        setupView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK - Private functions
    private func setupView(){
        if let message = message {
            messageLabel.text = message
        }
    }
    
    @IBAction func touchCancelItem(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapGoToMensalMais(_ sender: UIButton) {
        let urlMensalMais = RemoteConfigUtil.recoverParameterValue(with: "url_mensal_mais")
        guard let url = URL(string: urlMensalMais), UIApplication.shared.canOpenURL(url) else { return }
        UIApplication.shared.open(url)
    }
}
