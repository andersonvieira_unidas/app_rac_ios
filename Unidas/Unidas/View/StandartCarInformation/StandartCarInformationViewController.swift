//
//  StandartCarInformationViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol StandartCarInformationViewControllerDelegate {
    func redirectToReservationsTab()
}

class StandartCarInformationViewController: UIViewController, UITextFieldDelegate, StandartCarInformationViewDelegate, GaragesPickerTableViewControllerDelegate, DatePickerDelegate, MultipleChoiceAlertDelegate {

    @IBOutlet weak var returnStackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var returnView: UIView!
    @IBOutlet weak var returnSwitch: UISwitch!
    
    @IBOutlet weak var pickUpStoreView: UIView!
    @IBOutlet weak var returnStoreView: UIView!
    @IBOutlet weak var pickUpDateView: ShadowView!
    @IBOutlet weak var returnDateView: ShadowView!
    
    @IBOutlet weak var pickUpLocationNameLabel: UILabel!
    @IBOutlet weak var returnLocationNameLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpHourLabel: UILabel!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnHourLabel: UILabel!
    @IBOutlet weak var promoCodeTextField: UITextField!
    @IBOutlet weak var nextButton: UnidasButton!
    
    var oldReservation: VehicleReservationsDataView?
    var garageDataView: GarageDataView?
    var stepView: StepView?
    var redirectToSelectGarage = false
    var returnStackViewHeight: CGFloat = 0.0
    var scrollHeight: CGFloat = 0.0
    var topHeaderHeightWithStep: CGFloat = 40.0
    var garagesTableView: UITableView? = nil
    
    lazy var presenter: StandartCarInformationPresenterDelegate = {
        let presenter = StandartCarInformationPresenter(quotationsService: QuotationsService(),
                                                        systemConfigurationService: SystemConfigurationService(),
                                                        unidasGarageService: UnidasGarageService(),
                                                        firebaseAnalyticsService: FirebaseAnalyticsService(),
                                                        adjustAnalyticsService: AdjustAnalyticsService(),
                                                        garageDataView: garageDataView)
        presenter.delegate = self
        return presenter
    }()
    
    var delegate: StandartCarInformationViewControllerDelegate?
    
    private var storeAvailabilityErrors: [StoreAvailabilityErrorDataView] = []
    
    var returnAtSamePlace: Bool {
        return returnSwitch.isOn
    }
    
    var voucherCode: String? {
        return promoCodeTextField.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        setupStepView()
        setupView()
        
        if redirectToSelectGarage {
            showGaragesPickerTableView()
        }
        
        if let reservation = oldReservation {
            setupChange(old: reservation)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let step = stepView {
            step.setupStep(total: 4, selected: 0)
            navigationController?.navigationBar.addSubview(stepView!)
        }
    }
    
    /* Private Functions */
    private func setupView() {
        self.navigationController?.navigationBar.shouldRemoveShadow(true)
        returnStackViewHeight = returnStackViewHeightConstraint.constant
        returnStackViewHeightConstraint.constant = 0
        returnView.isHidden = true
        
        scrollHeight = scrollHeightConstraint.constant + topHeaderHeightWithStep
        
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancel))
        navigationItem.leftBarButtonItem = backButton
        
        let tapGesturePickUpStore = UITapGestureRecognizer(target: self, action: #selector(touchPickUpStore))
        let tapGestureReturnStore = UITapGestureRecognizer(target: self, action: #selector(touchReturnStore))
        
        pickUpStoreView.addGestureRecognizer(tapGesturePickUpStore)
        returnStoreView.addGestureRecognizer(tapGestureReturnStore)
        
        let tapGesturePickUpDate = UITapGestureRecognizer(target: self, action: #selector(touchPickUpDate))
        let tapGestureReturnDate = UITapGestureRecognizer(target: self, action: #selector(touchReturnDate))
        
        pickUpDateView.addGestureRecognizer(tapGesturePickUpDate)
        returnDateView.addGestureRecognizer(tapGestureReturnDate)
        
        promoCodeTextField.delegate = self
    }
    
    private func setupChange(old reservation: VehicleReservationsDataView){
        pickUpDateLabel.text = reservation.pickUpDateOnly
        pickUpHourLabel.text = reservation.pickUpHourOnly
        
        self.presenter.pickUpDate =  reservation.model.dataHoraRetirada
        
        returnDateLabel.text = reservation.returnDateOnly
        returnHourLabel.text = reservation.returnHourOnly
        
        self.presenter.returnDate = reservation.model.dataHoraDevolucao
        
        let samePlace = reservation.model.lojaRetirada == reservation.model.lojaDevolucao ? true : false
        
        if samePlace {
            self.scrollHeightConstraint.constant = self.scrollHeightConstraint.constant + 75
        }
        
        self.returnSwitch.isOn = samePlace
        self.presenter.samePlace = samePlace
        
        self.presenter.getGaragePickup(bycode: reservation.model.lojaRetirada)
        self.presenter.getGarageReturn(bycode: reservation.model.lojaDevolucao, samePlace: samePlace)
        
        nextButton.isUserInteractionEnabled = true
        nextButton.isEnabled = true
    }
    
    private func showGaragesPickerTableView() {
        stepView?.isHidden = true
        headerTopConstraint.constant = 0
        let garagesPickerTableViewController = GaragesPickerTableViewController(style: .grouped)
        garagesPickerTableViewController.delegate = self
        show(garagesPickerTableViewController, sender: nil)
    }
    
    private func setupStepView() {
        stepView = StepView.loadFromNib()
        var frame = stepView!.frame
        frame.origin.y = navigationController?.navigationBar.frame.height ?? 0.0
        frame.size.width = UIScreen.main.bounds.width
        stepView?.frame = frame
        navigationController?.navigationBar.addSubview(stepView!)
    }
    
    func showFindStore() {
        guard let storyboard = UIStoryboard(name: "FindStore", bundle: nil) as? UIStoryboard else {return}
        if let findStoreVC = storyboard.instantiateViewController(withIdentifier: "FindStoreViewController") as? FindStoreViewController {
            findStoreVC.removeExpressStores = true
            findStoreVC.hidesBottomBarWhenPushed = true
            findStoreVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(findStoreVC, animated: true)
        }
    }
    
    // MARK: - Interface builder actions
    @objc func touchCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func touchPickUpDate() {
        let type = DateType.pickup
        presenter.pickerDate(of: type)
    }
    
    @objc func touchReturnDate() {
        let type = DateType.return
        presenter.pickerDate(of: type)
    }
    
    @objc func touchPickUpStore() {
        presenter.garageSelected = GaragesPickerSelected.pickup
        showGaragesPickerTableView()
    }
    
    @objc func touchReturnStore() {
        presenter.garageSelected = GaragesPickerSelected.return
        showGaragesPickerTableView()
    }
    
    @IBAction func changedSamePlace(_ sender: UISwitch) {
        presenter.changedSamePlace(samePlace: sender.isOn, update: false)
    }
    
    @IBAction func touchNext(_ sender: UIButton) {
        presenter.fetchQuotations()
    }
    
    // MARK: -StandartCarInformationViewDelegate
    func showPickerDate(date: Date, of type: DateType) {

        let minuteInterval = presenter.minuteInterval
        let minimumDate = (type == .return) ? presenter.pickUpDate : presenter.minimumPickUpDate
        
        let datePicker = DatePicker(title: type.title, subTitle: NSLocalizedString("Please select a date.", comment: ""), minuteInterval: minuteInterval, minimumDate: minimumDate, date: date, type: type)
        datePicker.delegate = self
        datePicker.show(animated: true, bottomUp: true)
    
    }
    
    func pickUpDateChange(date: String, hour: String) {
        pickUpDateLabel.text = date
        pickUpHourLabel.text = hour
    }
    
    func returnDateChange(date: String, hour: String) {
        returnDateLabel.text = date
        returnHourLabel.text = hour
    }
    
    func startLoading() {
        nextButton.isEnabled = false
        nextButton.isUserInteractionEnabled = false
        nextButton.loading(true, activityIndicatorViewStyle: .white)
    }
    
    func endLoading() {
        nextButton.isUserInteractionEnabled = true
        nextButton.isEnabled = true
        nextButton.loading(false)
    }
    
    func present(error: Error) {
        garagesTableView?.isUserInteractionEnabled = true
        alertError(errorMessage: error.localizedDescription)
    }
    
    func presentCustomAlert(error: Error) {
        let title = NSLocalizedString("Unidas", comment: "Loja Indisponível")
        let subTitle = NSLocalizedString("\(error.localizedDescription)", comment: "Loja Indisponível")
        let alert = SimpleAlert(title: title, subTitle: subTitle, image:#imageLiteral(resourceName: "icon-erro2"))
        
        alert.show(animated: true)
    }
    
    func updatePickUpVehicle(garageDataView: GarageDataView) {
        pickUpLocationNameLabel.text = garageDataView.name
        
        if oldReservation == nil {
            DispatchQueue.main.async {
                let date = self.presenter.minimumPickUpDate
                self.showPickerDate(date: date, of: DateType.pickup)
            }
        }
    }
    
    func updateReturnVehicle(garageDataView: GarageDataView) {
        returnLocationNameLabel.text = garageDataView.name
    }
    
    func showReturnPlace(){
        self.returnView.isHidden = false
        self.returnStackViewHeightConstraint.constant = self.returnStackViewHeight
        self.scrollHeightConstraint.constant = self.scrollHeightConstraint.constant + self.returnStackViewHeightConstraint.constant
    }
    
    func hiddenReturnPlace() {
        self.scrollHeightConstraint.constant = self.scrollHeightConstraint.constant - self.returnStackViewHeight
        self.returnView.isHidden = true
        self.returnStackViewHeightConstraint.constant = 0
    }
    
    func removeReturnVehicle() {
        returnLocationNameLabel.text = NSLocalizedString("Where do you want to return the car?", comment: "Return car label")
    }
    
    func nextStepGroupsVehicles(quotationDataView: [QuotationsDataView]) {
        performSegue(withIdentifier: "segueToVehicleDetails", sender: quotationDataView)
    }
    
    func showStoreAvailabilityError(_ garage: GarageErrorDataView?) {
        if let garage = garage {
            nextButton.isEnabled = false
            showWorkingHours(for: garage)
        } else{
            nextButton.isEnabled = true
        }
    }
    
    func hideStoreAvailabilityError() {
        self.storeAvailabilityErrors = []
        nextButton.isEnabled = true
    }
    
    func showWorkingHours(for garage: GarageErrorDataView) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "segueToWorkStore", sender: garage)
        }
    }
    
    func showAlert(withMessage message: String) {
        alertError(errorMessage: message)
    }
    
    
    func checkForExpressAvailability(pendencie:Bool, financialPendencie:Bool) {
        
        let currentUser = session.currentUser
    
        if currentUser == nil {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-express-car") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Login Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Login Subtitle", comment: ""), firstButtonTag: 6, secondButtonTag: 7, firstButtonColor: #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Login Btn1", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Login Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: true)
            }
            
        }else if financialPendencie {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-car-restriction") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Profile Financial Pendencie Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Profile Financial Pendencie Subtitle", comment: ""), firstButtonTag: 1, secondButtonTag: 2, firstButtonColor: #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Profile Financial Pendencie Btn1", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Profile Financial Pendencie Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: true)
            }
    
        }else if pendencie {
            
            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-car-restriction") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Profile Pendencie Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Profile Pendencie Subtitle", comment: ""), firstButtonTag: 3, secondButtonTag: nil, firstButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), secondButtonColor: nil, firstButtonLabel: NSLocalizedString("Unidas Express Profile Pendencie Btn", comment: ""), secondButtonLabel: nil, firstButtonEnabled: true, secondButtonEnabled: false)
            }
        }else{

            DispatchQueue.main.async {
                self.showExpressRouteView(image: UIImage(named: "icon-express-car") ?? #imageLiteral(resourceName: "logo-full"), title: NSLocalizedString("Unidas Express Presentation Title", comment: ""), subtitle: NSLocalizedString("Unidas Express Presentation Subtitle", comment: ""), firstButtonTag: 4, secondButtonTag: 5, firstButtonColor: #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1), secondButtonColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1), firstButtonLabel: NSLocalizedString("Unidas Express Presentation Btn1", comment: ""), secondButtonLabel: NSLocalizedString("Unidas Express Presentation Btn2", comment: ""), firstButtonEnabled: true, secondButtonEnabled: true)
            }
        }
        garagesTableView?.isUserInteractionEnabled = true
    }
    
    func showExpressRouteView(image:UIImage, title:String, subtitle:String, firstButtonTag:Int, secondButtonTag:Int?, firstButtonColor:UIColor, secondButtonColor:UIColor?, firstButtonLabel:String, secondButtonLabel:String?, firstButtonEnabled:Bool, secondButtonEnabled:Bool) {
        
        let multipleChoiceAlert = MultipleChoiceAlert(title: title, subTitle: subtitle, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel ?? "", image: image, firstChoiceButtonEnabled: firstButtonEnabled, secondChoiceButtonEnabled: secondButtonEnabled, firstChoiceButtonColor: firstButtonColor, secondChoiceButtonColor: secondButtonColor, firstButtonTag: firstButtonTag, secondButtonTag: secondButtonTag)
        multipleChoiceAlert.delegate = self
        multipleChoiceAlert.show(animated: true)
    }
    
    func redirectToMensalMais(message: String?) {
        performSegue(withIdentifier: "segueToMensalMais", sender: message)
    }
    
    //MARK: - MultipleChoiceDelegate
    
    func choiceFirstTap(tag: Int) {
        switch tag {
        case 1:
            print("")
        case 3:
            performSegue(withIdentifier: "showPendencies", sender: nil)
        default:
            print("default")
        }
    }
    
    func choiceSecondTap(tag: Int) {
        switch tag {
        case 2:
            showFindStore()
        case 5:
            presenter.selectedVehicleLocation(garageDataView: garageDataView!)
            
            if redirectToSelectGarage {
                self.stepView?.isHidden = false
                headerTopConstraint.constant = 40
                navigationController?.navigationBar.addSubview(stepView!)
            }
            navigationController?.popViewController(animated: true)
        case 7:
            performSegue(withIdentifier: "showLogin", sender: nil)
        default:
            print("default")
        }
    }
    
    // MARK: - GaragesPickerTableViewControllerDelegate
    func garagePicker(_ garagesTableViewController: GaragesPickerTableViewController, didSelect garage: GarageDataView) {
        garagesTableView = garagesTableViewController.tableView
        
        if garage.model.type == .express {
            presenter.fetchStatusCliente()
            garageDataView = garage
        }else{
            presenter.selectedVehicleLocation(garageDataView: garage)
             
            if redirectToSelectGarage {
                self.stepView?.isHidden = false
                headerTopConstraint.constant = 40
                navigationController?.navigationBar.addSubview(stepView!)
            }
            navigationController?.popViewController(animated: true)
        }
    }
    
    func garagePickerBackPressed() {
        //TODO - ANDeRSON AJustar aqui
        
        self.stepView?.isHidden = false
        headerTopConstraint.constant = 40
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        return true
    }
    
    // MARK: - DatePickerDelegate
    func setDate(date: Date, type: DateType) {
        self.presenter.setDate(of: type, date: date)
    }

        
    func close() {
            
    }
    

    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToVehicleDetails" {
            if let viewController = segue.destination as? StandartCarPickerVehicleCollectionViewController {
                viewController.quotationDataView = sender as? [QuotationsDataView]
                viewController.pickUp = presenter.pickUp
                viewController.return = presenter.return
                viewController.oldReservation = oldReservation
                viewController.voucherCode = voucherCode
            }
        }
        else if segue.identifier == "segueToWorkStore" {
            if let viewController = segue.destination as? WorkingHoursViewController,
               let garageErrorDataView = sender as? GarageErrorDataView, let garage = garageErrorDataView.garage {
                let garageReservationDetails = GarageReservationDetails(date: Date(), garage: garage)
                let garageReservationDetailsDataView = GarageReservationDetailsDataView(model: garageReservationDetails)
                viewController.garageDetails = garageReservationDetailsDataView
                viewController.storeSelected = garageErrorDataView.selected
                //viewController.sel
            }
        }
        else if segue.identifier == "segueToMensalMais" {
            if let viewController = segue.destination as? StandartCarInformationMensalMaisViewController, let message = sender as? String {
                viewController.message = message
            }
        }
        
        else if segue.identifier == "showPendencies" {
            let navigation = segue.destination as? UINavigationController
            let pendenciesVC = navigation?.viewControllers.first as? PendenciesViewController
            pendenciesVC?.modalPresentationStyle = .fullScreen
        }
        
        else if segue.identifier == "showFindStore" {
            let navigation = segue.destination as? UINavigationController
            let findStoreVC = navigation?.viewControllers.first as? FindStoreViewController
            findStoreVC?.removeExpressStores = true
            findStoreVC?.hidesBottomBarWhenPushed = true
            findStoreVC?.modalPresentationStyle = .fullScreen
        }
        
        else if segue.identifier == "showLogin" {
            let navigation = segue.destination as? UINavigationController
            let findStoreVC = navigation?.viewControllers.first as? LoginViewController
            findStoreVC?.hidesBottomBarWhenPushed = true
            findStoreVC?.modalPresentationStyle = .fullScreen
        }
    }
    
    @IBAction func unwindToOne(_ sender: UIStoryboardSegue) {
        delegate?.redirectToReservationsTab()
    }
}
