//
//  MessagesViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController, MessagesViewDelegate, UIViewControllerPreviewingDelegate, UITableViewDelegate, UITableViewDataSource {
   
    private var presenter: MessagesPresenterDelegate!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        setHeaderUnidas()
        presenter.fetchMessage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForPreviewingIfAvailable()
        presenter = MessagesPresenter(messageService: MessageService())
        presenter.delegate = self
        
        setupTableView()
        setHeaderUnidas()
        self.navigationController?.change()
        tableView.tableFooterView = UIView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: MessageTableViewCell.self)
    }
    
    func registerForPreviewingIfAvailable() {
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: tableView)
        }
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: MessageTableViewCell.self)
        cell.setup(messageDataView: presenter.message(at: indexPath.row))
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? MessageTableViewCell else { return }

        if cell.messageType == MessageType.pen.rawValue {
            presenter.fetchCredDefenseSerasaStatus()
        } else {
             performSegue(withIdentifier: "showMessageDetail", sender: tableView.cellForRow(at: indexPath))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98
    }
    
    // MARK: - MessagesViewDelegate
    
    func canShowPendencies(value: Bool) {
        if value {
            showPendencies()
        }else{
            showGoToStoreModal()
        }
     }
    
    func showPendencies() {
        let storyboard = UIStoryboard(name: "Pendencies", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "PendenciesVC") as? PendenciesViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showGoToStoreModal() {
         guard let storyboard = UIStoryboard(name: "Pendencies", bundle: nil) as? UIStoryboard else {return}
         if let vc = storyboard.instantiateViewController(withIdentifier: "SerasaCredDefenseNotificationVC") as? SerasaCredDefenseNotificationViewController {
             self.navigationController?.present(vc, animated: true, completion: nil)
         }
     }
    
    func startLoading() {
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func reloadData() {
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    func showEmptyMessageView() {
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = tableView.frame
        emptyView.imageView.image = #imageLiteral(resourceName: "ico_notifications_empty_state")
        emptyView.imageView.tintColor = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        emptyView.titleLabel.text = NSLocalizedString("Notifications Empty State Title", comment: "")
        emptyView.messageTextView.text = NSLocalizedString("Notifications Empty State Description", comment: "")
        emptyView.actionButton.isHidden = true
        tableView.backgroundView = emptyView
    }
    
    // MARK: - Previewing delegate
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location) else { return nil }
        let messageDetailsViewController = storyboard?.instantiateViewController(withIdentifier: "MessageDetailsViewController") as? MessageDetailsViewController
        messageDetailsViewController?.messageDataView = presenter.message(at: indexPath.row)
        messageDetailsViewController?.shouldMarkAsReadOnViewDidLoad = false
        previewingContext.sourceRect = tableView.rectForRow(at: indexPath)
        return messageDetailsViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let messageDetailsViewController = viewControllerToCommit as? MessageDetailsViewController {
            messageDetailsViewController.presenter.markMessageAsRead()
        }
        show(viewControllerToCommit, sender: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMessageDetail", let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell) {
            let destination = segue.destination as? MessageDetailsViewController
            destination?.messageDataView = presenter.message(at: indexPath.row)
            destination?.shouldMarkAsReadOnViewDidLoad = true
        }else if segue.identifier == "showPendencies" {
            let navigation = segue.destination as? UINavigationController
            let pendenciesVC = navigation?.viewControllers.first as? PendenciesViewController
            pendenciesVC?.setupBottomTableView = true
            pendenciesVC?.modalPresentationStyle = .fullScreen
        }
    }    
}
