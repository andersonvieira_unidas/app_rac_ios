//
//  AboutViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, AboutViewDelegate {
    
    @IBOutlet weak var appVersionLabel: UILabel!
    
    @IBOutlet weak var missionDescriptionLabel: UILabel!
    @IBOutlet weak var visionDescriptionLabel: UILabel!
    @IBOutlet weak var valuesDescriptionLabel: UILabel!
    
    lazy var presenter: AboutPresenterDelegate = {
        let presenter = AboutPresenter(companyTermsService: CompanyTermsService())
        presenter.delegate = self
        return presenter
    }()
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    lazy var appVersionString: String? = {
        guard let infoDictionary = Bundle.main.infoDictionary,
            let appVersion = infoDictionary["CFBundleShortVersionString"] as? String,
            let appBuildNumber = infoDictionary[kCFBundleVersionKey as String] as? String else { return nil }
        let localizedAppVersionStringFormat = NSLocalizedString("Version %@ (%@)", comment: "App version description String")
        return String(format: localizedAppVersionStringFormat, appVersion, appBuildNumber)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        setupView()
        configureButtonBack()
    }
    
    private func setupView() {
        let activityIndicatorItem = UIBarButtonItem(customView: activityIndicatorView)
        navigationItem.rightBarButtonItem = activityIndicatorItem
        appVersionLabel.text = appVersionString
        
        let aboutValuesText = NSLocalizedString("About Values Description", comment: "")
        valuesDescriptionLabel.text = aboutValuesText
        
        let aboutMissionText = NSLocalizedString("About Mission Description", comment: "")
        missionDescriptionLabel.text = aboutMissionText
        
        let aboutVisionText = NSLocalizedString("About Vision Description", comment: "")
        visionDescriptionLabel.text = aboutVisionText
    }
    
    // MARK: - AboutViewDelegate
    func startLoading() {
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    // MARK: - Private functions
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
}
