//
//  HomeViewController.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 21/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import SuperBadges
import StoreKit

struct HomeCellObject {
    var title: String?
    var image: UIImage?
    var description: String?
    var type: HomeCellType?
}

enum HomeCellType {
    case ReservationsMade
    case Reservations
    case HowToRent
    case Pendencies
    case CheckIn
}

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, HomeViewDelegate, PaymentProtocol, OnboardViewControllerDelegate, StandartCarInformationViewControllerDelegate, MultipleChoiceAlertDelegate, ReviewAppDelegate, NegativeReviewDelegate, ReservationExpressStepCollectionViewCellDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var reservationButton: EnhancedButton!
    
    @IBOutlet weak var collectionConstraint: NSLayoutConstraint!
    
    var currentPage = 0
    
    lazy var presenter: HomePresenterDelegate = {
        let presenter = HomePresenter(homeService: HomeService(), firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        return presenter
    }()
    
    @IBOutlet weak var homeone: UnidasButton!
    @IBOutlet weak var hometwo: UnidasBorderButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        setupCollectionView()
        setupToggle()
        presenter.viewDidLoad()        
        pageControl.isEnabled = false
        pageControl.hidesForSinglePage = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.viewWillAppear()
    }
    
    func setupCollectionView() {
        collectionView.register(cellType: HomeDefaultViewCell.self)
        collectionView.register(cellType: ReservationStepCollectionViewCell.self)
        collectionView.register(cellType: ReservationExpressStepCollectionViewCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alpha = 0.0
    }
    
    private func setupToggle(){
        FirebaseRealtimeDatabase.toggleValue(withKey: .newReservation) { (value) in
            if !value {
                self.reservationButton.isHidden = true
            } else {
                self.reservationButton.isHidden = false
            }
        }
    }
    
    func setupRatingToggle(){
        FirebaseRealtimeDatabase.toggleValue(withKey: .rateHome) { (value) in
            if value {
                self.checkAppReview()
            }
        }
    }
    
    func checkAppReview() {
        let username = session.currentUser?.account.name ?? ""
        let title = username == "" ? "\(NSLocalizedString("App Review Hello", comment: ""))!" : "\(NSLocalizedString("App Review Hello", comment: "")), \(username)!"
        
        let review = ReviewApp(title: title, firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 1, secondButtonTag: 2)
        review.delegate = self
        ReviewAppUtilities.rateMe(review: review)
    }
    
    func animateCollection() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4) {
                self.collectionView.alpha = 1.0
            }
        }
    }
    
    func disableNotifications() {
        self.tabBarController?.tabBar.items?[1].isEnabled = false
    }
    
    func enableNotifications() {
        self.tabBarController?.tabBar.items?[1].isEnabled = true
    }
    
    func askToUpdateApp() {
        
        let title = NSLocalizedString("App Update Title", comment: "")
        let subTitle = NSLocalizedString("App Update Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("App Update No", comment: "")
        let secondChoiceLabel = NSLocalizedString("App Update Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-alert"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 3, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    func loadBadgePendencies(value: Bool) {
        if value {
            addBadge(index: 1)
        }else {
            removeBadge(index: 1)
        }
    }
    
    func reload(){
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func startLoading() {
        LoadingScreen.setLoadingScreen(view: self.view)
    }
    
    func endLoading() {
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func addBadge(index: Int) {
        self.tabBarController?.addDotAtTabBarItemIndex(index: index, radius: 7, color : UIColor.red)
    }
    
    func removeBadge(index: Int) {
        self.tabBarController?.removeDotAtTabBarItemIndex(index: index)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func removeBage() {
        self.tabBarController?.removeDotAtTabBarItemIndex(index: 1)
    }
    
    func showCheckin(reservation: VehicleReservationsDataView) {
        if presenter.compareAppVersionToUpdate() == true {
            askToUpdateApp()
        }else{
            performSegue(withIdentifier: "showPayment", sender: reservation)
        }        
    }
    
    @IBAction func didTapNewReservationButton(_ sender: EnhancedButton) {
        if presenter.compareAppVersionToUpdate() == true {
            askToUpdateApp()
        }else{
            performSegue(withIdentifier: "showStandartCarFromHome", sender: nil)
        }
    }
    
    func showPostTerms() {
        performSegue(withIdentifier: "showAfterTerms", sender: nil)
    }
    
    func showDeprecatedVersion() {
        performSegue(withIdentifier: "showDeprecatedVersion", sender: nil)
    }
    
    func showQrCode(qrCode: String) {
        guard let storyboard = UIStoryboard(name: "ContractHistory", bundle: nil) as? UIStoryboard else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "ContractQRCodeViewController") as? ContractQRCodeViewController {
            vc.qrCodeInformation = qrCode
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: - OnboardViewControllerDelegate
    
    func canShowPendencies(value: Bool) {
        if value {
            showPendencies()
        }else{
            showGoToStoreModal()
        }
    }
    
    func showGoToStoreModal() {
        guard let storyboard = UIStoryboard(name: "Pendencies", bundle: nil) as? UIStoryboard else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "SerasaCredDefenseNotificationVC") as? SerasaCredDefenseNotificationViewController {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func tapActionButton(onboardingType: OnboardingType) {
        switch onboardingType {
        case .howToRent:
            self.performSegue(withIdentifier: "showStandartCarFromHome", sender: nil)
            break
        default:
            break
        }
    }
    
    func showOnboarding(pages: [OnboardPage]) {
        let buttonTitle = NSLocalizedString("Skip Introduction", comment: "")
        let appearence = OnboardViewController.AppearanceConfiguration(buttonTitle: buttonTitle, onboardingType: .introduction)
        let onboardingVC = OnboardViewController(pageItems: pages, appearanceConfiguration: appearence)
        
        onboardingVC.delegate = self
        onboardingVC.modalPresentationStyle = .fullScreen
        onboardingVC.presentFrom(self, animated: true)
    }
    
    func showHowToRent(pages: [OnboardPage]) {
        let buttonTitle = NSLocalizedString("New Reserve", comment: "")
        let appearence = OnboardViewController.AppearanceConfiguration(buttonTitle: buttonTitle)
        let onboardingVC = OnboardViewController(pageItems: pages, appearanceConfiguration: appearence)
        onboardingVC.delegate = self
        self.navigationController?.pushViewController(onboardingVC, animated: true)
    }
    
    func showPreview() {
        let reservation = presenter.reservation
        performSegue(withIdentifier: "showPreview", sender: reservation)
    }
    
    func showPendencies() {
        performSegue(withIdentifier: "showPendencies", sender: nil)
    }
    
    func showConfirmCheckin(reservation: VehicleReservationsDataView) {
        let title = NSLocalizedString("Checkin are you sure title", comment: "")
        let message = NSLocalizedString("Checkin are you sure", comment: "")
        let firstButtonLabel = NSLocalizedString("Checkin ok", comment: "")
        let secondButtonLabel = NSLocalizedString("Checkin cancel", comment: "")
        let image =  #imageLiteral(resourceName: "icon-cancel-reservation-alert")
        let multipleChoiceAlert = MultipleChoiceAlert(title: title, subTitle: message, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel, image: image, firstChoiceButtonColor: UIColor.themeImportantButton, firstButtonTag: 1, secondButtonTag: 1)
        
        multipleChoiceAlert.delegate = self
        multipleChoiceAlert.show(animated: true)
    }
    
    //MARK: - PaymentProtocol
    func reloadData() {
        self.presenter.viewWillAppear()
    }
    
    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = presenter.homeCollectionsArray.count
        if let _ = presenter.reservationStep {
            count += 1
        }
        pageControl.numberOfPages = count
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let express = presenter.reservationStep?.express ?? false
        
        if presenter.reservationStep != nil && !express && indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: ReservationStepCollectionViewCell.self)
            guard let reservationStep = presenter.reservationStep else { return UICollectionViewCell() }
            cell.setup(reservationStep: reservationStep)
            return cell
            //EXPRESS
        } else if presenter.reservationStep != nil && express && indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: ReservationExpressStepCollectionViewCell.self)
            guard let reservationStep = presenter.reservationStep else { return UICollectionViewCell() }
            cell.delegate = self
            cell.setup(reservationStep: reservationStep)
            return cell
        }
        
        else if  presenter.reservationStep != nil && indexPath.row > 0 {
            let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: HomeDefaultViewCell.self)
            cell.setupView(cell: presenter.homeItem(index: indexPath.row - 1))
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(for: indexPath, cellType: HomeDefaultViewCell.self)
            cell.setupView(cell: presenter.homeItem(index: indexPath.row))
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 146
        let width = Int(presenter.homeCollectionsArray.count == 1 ? self.view.frame.width - 20 : 240)
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 && presenter.reservation != nil {
            if  collectionView.cellForItem(at: indexPath) as? ReservationStepCollectionViewCell != nil ||
                    collectionView.cellForItem(at: indexPath) as? ReservationExpressStepCollectionViewCell != nil {
                presenter.showReservationDetails()
            }
            return
        }else{
            guard let cell = collectionView.cellForItem(at: indexPath) as? HomeDefaultViewCell else {return}
            
            switch cell.cellType {
            case .ReservationsMade:
                print("Reservations")
            case .Reservations:
                self.tabBarController?.selectedIndex = 2
            case .HowToRent:
                presenter.loadHowToRent()
            case .Pendencies:
                presenter.fetchCredDefenseSerasaStatus()
            case .CheckIn:
                let express = presenter.reservation?.express ?? false
                if express {
                    presenter.openQrCode()
                } else {
                    presenter.checkInReservation(reservation: presenter.reservation, isCheckinConfirmed: false)
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageCount = pageControl.numberOfPages
        let screenSize = CheckScreenResolution.checkScreenSize() == false ? 80 : 60
        let marginDiscount = pageCount * screenSize
        let pageWidth = scrollView.frame.width - CGFloat(marginDiscount)
        currentPage = Int((scrollView.contentOffset.x + pageWidth / 2) / pageWidth)
        pageControl.currentPage = self.currentPage    
    }
    
    //MARK: - PaymentProtocol
    func reloadScreen() {
        reloadData()
    }
    
    //MARK: - StandartCarInformationViewControllerDelegate
    func redirectToReservationsTab() {
        self.tabBarController?.selectedIndex = 2
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {        
        if tag == 1 {
            if let reservation = presenter.reservation {
                presenter.checkInReservation(reservation: reservation, isCheckinConfirmed: true)
            }
        }
    }
    
    func choiceSecondTap(tag: Int) {
        if tag == 2 {
            VersionControl.openAppStore()
        }
    }
    
    //MARK: - ReviewAppDelegate
    
    func choiceFirstTapReview(tag: Int) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "neverRate")
        
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            ReviewAppUtilities.rateApp(appId: "id1436660799", completion: { (success) in
                print("RateApp \(success)")
            })
        }        
    }
    
    func choiceSecondTapReview(tag: Int) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "neverRate")
        
        let negativeReview = NegativeReview()
        negativeReview.delegate = self
        negativeReview.show(animated: true)
    }
    
    func closeNegativeReview() {
        
    }
    
    //MARK:- ReservationExpressStepCollectionViewCellDelegate {
    func reloadExpired() {
        presenter.viewWillAppear()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStandartCarFromHome" {
            let navigation = segue.destination as? UINavigationController
            let standartCarRent = navigation?.viewControllers.first as? StandartCarInformationViewController
            navigation?.modalPresentationStyle = .fullScreen
            standartCarRent?.redirectToSelectGarage = true
            standartCarRent?.delegate = self
        }
        
        else if segue.identifier == "showPayment" {
            let navigation = segue.destination as? UINavigationController
            let paymentViewController = navigation?.viewControllers.first as? PaymentViewController
            paymentViewController?.vehicleReservationsDataView = sender as? VehicleReservationsDataView
            paymentViewController?.delegate = self
        }
        
        else if segue.identifier == "showPendencies" {
            let navigation = segue.destination as? UINavigationController
            let pendenciesVC = navigation?.viewControllers.first as? PendenciesViewController
            pendenciesVC?.modalPresentationStyle = .fullScreen
        }
        
        else if segue.identifier == "showPreview" {
            let navigation = segue.destination as? UINavigationController
            let reservationDetailsViewController = navigation?.viewControllers.first as? ReserveDetailsViewController
            navigation?.modalPresentationStyle = .fullScreen
            reservationDetailsViewController?.rules = presenter.rules()
            reservationDetailsViewController?.vehicleReservationsDataView = sender as? VehicleReservationsDataView
            
        }else if segue.identifier == "showAfterTerms" {
            let destination = segue.destination as? AfterTermsViewController
            destination?.modalPresentationStyle = .fullScreen
        }
    }    
}



