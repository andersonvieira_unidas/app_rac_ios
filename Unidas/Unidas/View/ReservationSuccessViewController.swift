//
//  ReservationSuccessViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 16/05/19.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import MapKit
import StoreKit

class ReservationSuccessViewController: UIViewController, ReservationSuccessViewDelegate, UITextViewDelegate, ReviewAppDelegate, NegativeReviewDelegate {

    // MARK: - Instance variables
    
    // MARK: Outlets
    @IBOutlet weak var reservationTitleLabel: UILabel!
    @IBOutlet weak var reservationNumberLabel: UILabel!
    @IBOutlet weak var reservationCreationDateLabel: UILabel!
    @IBOutlet weak var pickupDateLabel: UILabel!
    @IBOutlet weak var pickupLocationTextView: UITextView!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnLocationTextView: UITextView!
    
    @IBOutlet weak var addToCalendarButton: UnidasButton?
    
    @IBOutlet weak var reservationGroupNameLabel: UILabel!
    @IBOutlet weak var reservationGroupDescriptionLabel: UILabel!
    
    @IBOutlet weak var reservationGroupCodeLabel: UILabel!
    @IBOutlet weak var reservationGroupImageView: UIImageView!
    @IBOutlet weak var reservationClientNameLabel: UILabel!
    @IBOutlet weak var reservationClientEmailLabel: UILabel!
    @IBOutlet weak var reservationClientPhoneLabel: UILabel!
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var dontForgetView: UIView!
    @IBOutlet weak var dontForgetInfoView: EnhancedView!
    @IBOutlet weak var preAuthorizationInfoView: UIView!
    @IBOutlet weak var preAuthorizationView: EnhancedView!
    @IBOutlet weak var paymentTransactionView: UIView!
    @IBOutlet weak var prePaymentTransactionView: UIView!
    
    @IBOutlet weak var contraintViewHeight: NSLayoutConstraint?
    
    //EXPRESS
    @IBOutlet weak var qrCodeTextView: UITextView!
    @IBOutlet weak var qrCodeButton: ShadowView!
    @IBOutlet weak var dontForgetTextView: UITextView!
    
    private var paymentTransaction = TransactionPaymentView.loadFromNib()
    private var prePaymentTransaction = TransactionPaymentView.loadFromNib()
    
    // MARK: View parameters
    
    var reservation: ReservationDataView!
    var user: UserResponseDataView!
    var creditCard: CreditCardReservePaymentDataView?
    var isAntecipatedPayment = Bool()
    var isDetailsScreen = false
    var preAuthorization: Float?
    
    // MARK: Lazy initialization
    lazy var presenter = { () -> ReservationSuccessPresenterDelegate in
        let presenter = ReservationSuccessPresenter(paymentService: PaymentService())
        presenter.delegate = self
        return presenter
    }()
    
    
    // MARK: Overrides
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        presenter.viewDidLoad(reservation: reservation, user: user, creditCard: creditCard)
        presenter.requestPermissionReminder()
        setupLogoNavigation(isAntecipated: isAntecipatedPayment)
        
        if reservation.express {
            contraintViewHeight?.constant = 1250
        } else {
            contraintViewHeight?.constant = 900
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.subviews.first(where: { $0 is StepView })?.removeFromSuperview()
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    // MARK: - Interface builder action
    @IBAction func myReservationsButtonTapped(_ sender: UIButton) {
        if isAntecipatedPayment == true && !self.isDetailsScreen {
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            dismiss(animated: true, completion: {
                if self.isAntecipatedPayment && self.isDetailsScreen {
                    self.performSegue(withIdentifier: "unwindToFour", sender: self)
                }
                else {
                    self.performSegue(withIdentifier: "unwindToOne", sender: self)
                }
            })
        }
    }
    
    @IBAction func addPickupDateToCalendarButtonTapped() {
        presenter.addPickupDateToCalendarButtonTapped()
    }
    
    @IBAction func tapQRCode() {
        
        guard let storyboard = UIStoryboard(name: "ContractHistory", bundle: nil) as? UIStoryboard, let qrCode = reservation.qrCodeExpress else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "ContractQRCodeViewController") as? ContractQRCodeViewController {
            vc.qrCodeInformation = qrCode
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    // MARK: - Ask Permission to Settings
    func askPermissionToSettings() {
        askPermissionToSettings(withMessage: NSLocalizedString("Settings Reminder Calendar Description", comment: ""))
    }
    
    func checkCalendarPermissionStatus() {
        
    }
    
    func setupLogoNavigation(isAntecipated: Bool) {
        if isAntecipated == true {
            reservationTitleLabel.text = NSLocalizedString("Your payment is payed", comment: "")
        }
    }
    
    // MARK: - Reservation Success View Delegate
    func setupView(for reservation: ReservationDataView) {
        reservationNumberLabel.text = reservation.number
        
        if reservation.paidDenied {
            let localizedPaidDenied = NSLocalizedString("Payment Denied - Title", comment: "").uppercased()
            
            reservationTitleLabel.text = localizedPaidDenied
            reservationTitleLabel.textColor = #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
        }
        
        let date = DateFormatter.dateShortFormatter.string(from: Date())
        let hour = DateFormatter.timeOnlyFormatter.string(from: Date())
        reservationCreationDateLabel.text = "\(date) - \(hour)"
        
        if let pickupDate = reservation.pickUp?.dateOnly, let pickupHour = reservation.pickUp?.hourOnly {
            let pickUpLocalizedString = NSLocalizedString("Pickup Date %@ at %@", comment: "")
            let pickUpFormatted = String(format: pickUpLocalizedString, pickupDate, pickupHour)
            let pickUpMuttableString = makeDate(date: pickupDate, hour: pickupHour, dataAndHour: pickUpFormatted)
            pickupDateLabel.attributedText = pickUpMuttableString
        }
        
        if let returnDate = reservation.return?.dateOnly, let returnHour = reservation.return?.hourOnly {
            let returnLocalizedString = NSLocalizedString("Return Date %@ at %@", comment: "")
            let returnFormatted = String(format: returnLocalizedString, returnDate, returnHour)
            
            let returnMuttableString = makeDate(date: returnDate, hour: returnHour, dataAndHour: returnFormatted)
            returnDateLabel.attributedText = returnMuttableString
        }
        
        reservationGroupNameLabel.text = reservation.group?.groupName?.lowerCapitalize()
        reservationGroupDescriptionLabel.text = reservation.group?.vehicles
        
        if !isAntecipatedPayment {
            let groupLocalized = NSLocalizedString("%@ Group", comment: "")
            reservationGroupCodeLabel.text = String(format: groupLocalized, reservation.group?.groupCode ?? "")
            pickupLocationTextView.attributedText = makeStore(storeAndCity: reservation.pickUp?.storeAndState, link: "pickup")
            returnLocationTextView.attributedText = makeStore(storeAndCity: reservation.return?.storeAndState, link: "return")
        } else {
            reservationGroupCodeLabel.text = reservation.group?.groupCode
            pickupLocationTextView.attributedText = makeStore(storeAndCity: reservation.pickUp?.garage?.model.name, link: "pickup")
            returnLocationTextView.attributedText = makeStore(storeAndCity: reservation.return?.garage?.model.name, link: "return")
        }
        
        returnLocationTextView.delegate = self
        pickupLocationTextView.delegate = self
        
        returnLocationTextView.linkTextAttributes = NSAttributedString.linkAttributed
        pickupLocationTextView.linkTextAttributes = NSAttributedString.linkAttributed
        
        if let url = reservation.pictureURL {
            reservationGroupImageView.setImage(withURL: url)
        }
        
        if reservation.express {
            setupExpress()
        } else {
            qrCodeView.translatesAutoresizingMaskIntoConstraints = false
            qrCodeView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            qrCodeButton.removeFromSuperview()
            dontForgetView.translatesAutoresizingMaskIntoConstraints = false
            dontForgetView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            dontForgetInfoView.removeFromSuperview()
            
            preAuthorizationInfoView.translatesAutoresizingMaskIntoConstraints = false
            preAuthorizationInfoView.heightAnchor.constraint(equalToConstant: 20).isActive = true
            preAuthorizationView.removeFromSuperview()
            
        }
    }
    
    // MARK: Private Function
    private func setupExpress(){
        let qrCodeInfoString = NSLocalizedString("Checkin confirmation express - QR Code Info", comment: "")
        let rangeQrCode = (qrCodeInfoString as NSString).range(of: "QR Code")
        let message = NSMutableAttributedString(string: qrCodeInfoString, attributes: [
                                                    NSAttributedString.Key.font : UIFont.regular(ofSize: 16),
                                                    NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1) ])
        message.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1), range: rangeQrCode)
        message.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 16)], range: rangeQrCode)
        qrCodeTextView.attributedText = message
        
        dontForgetTextView.contentInset = UIEdgeInsets(top: -7.0,left: -5.0,bottom: 0,right: 0.0);
        let dontForgetInfoString = NSLocalizedString("Checkin confirmation express - Dont Forget Description", comment: "")
        let dontForgetInfoBoldString = NSLocalizedString("Checkin confirmation express - Dont Forget Bold Description", comment: "")
        let rangeDontForget = (dontForgetInfoString as NSString).range(of: dontForgetInfoBoldString)
        let messageDontForget = NSMutableAttributedString(string: dontForgetInfoString, attributes: [
                                                            NSAttributedString.Key.font : UIFont.regular(ofSize: 12),
                                                            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)])
        messageDontForget.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1), range: rangeDontForget)
        messageDontForget.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 12)], range: rangeDontForget)
        dontForgetTextView.attributedText = messageDontForget
        
        let prePaymentTransactionObject = TransactionPayment(fourLastNumberCard: nil,
                                                           brandImage: nil,
                                                           paymentValue: nil,
                                                           status: nil, statusConfirmation: .noPayment)

        prePaymentTransaction.setup(type: .prePayment, transactionPayment: prePaymentTransactionObject, express: reservation.express)
         prePaymentTransactionView.addSubview(prePaymentTransaction)

        if let preAuthorizationValue = preAuthorization, let preAuthorizationFormatted = NumberFormatter.currencyFormatter.string(from: NSNumber(value: preAuthorizationValue)) {
            let preAuthorizationInfo = NSLocalizedString("Reservation Rules Reservation Express - Pre Authorization Value %@", comment: "")
            prePaymentTransaction.valueLabel.text = String(format: preAuthorizationInfo, String(preAuthorizationFormatted))
            
        }
        
        if !reservation.paid {
            qrCodeView.isUserInteractionEnabled = false
        }
    }
    private func makeDate(date: String, hour: String, dataAndHour: String) -> NSMutableAttributedString {
        
        let localizedString = NSLocalizedString("Date %@ at %@", comment: "")
        let searchString = String(format: localizedString, date, hour)
        let rangeDate = (dataAndHour as NSString).range(of: searchString)
        let dateMutable = NSMutableAttributedString(string: dataAndHour, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 12)])
        dateMutable.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 14)], range: rangeDate)
        
        return dateMutable
    }
    
    private func makeStore(storeAndCity: String?, link: String) -> NSMutableAttributedString? {
        guard let pickUpStoreAndState = storeAndCity else { return nil }
        
        let pickUpStoreAndStateFormatted = NSMutableAttributedString(string: pickUpStoreAndState, attributes: [NSAttributedString.Key.font: UIFont.bold(ofSize: 12),
                                                                                                               NSAttributedString.Key.link: link])
        
        return pickUpStoreAndStateFormatted
    }
    
    func setupView(for user: UserResponseDataView) {
        reservationClientNameLabel.text = user.account.name
        reservationClientEmailLabel.text = user.account.email
        reservationClientPhoneLabel.text = user.account.formattedMobilePhone
    }
    
    func setupView(for creditCard: CreditCardReservePaymentDataView) {

       let paymentTransactionObject = TransactionPayment(fourLastNumberCard: creditCard.card.fourLastNumberCard,
                                                          brandImage: creditCard.card.brandImage,
                                                          paymentValue: creditCard.installmentDescription,
                                                          status: nil, statusConfirmation: reservation.model.paymentStatus)

        paymentTransaction.setup(type: .payment, transactionPayment: paymentTransactionObject)
        
        paymentTransactionView.addSubview(paymentTransaction)
    }
    
    func setupInstallmentLabel(string: String) {
        
        
        let paymentTransactionObject = TransactionPayment(fourLastNumberCard: nil,
                                                           brandImage: nil,
                                                           paymentValue: string,
                                                           status: nil, statusConfirmation: .noPayment)

         paymentTransaction.setup(type: .payment, transactionPayment: paymentTransactionObject)
         paymentTransactionView.addSubview(paymentTransaction)
        //paymentTransaction.valueLabel.text = string
    }
    
    /** CALENDAR **/
    func enableAddToCalendarButton() {
        guard let addCalendar = addToCalendarButton else { return }
        addCalendar.isEnabled = true
    }
    
    func disableAddToCalendarButton() {
        guard let addToCalendar = addToCalendarButton else { return }
        addToCalendar.isEnabled = false
    }
    
    func showAddedToCalendar() {
        DispatchQueue.main.async {
            guard let addCalendar = self.addToCalendarButton else { return }
            addCalendar.setTitle(NSLocalizedString("Added to calendar!", comment: "Added to calendar title"), for: .normal)
            self.disableAddToCalendarButton()
        }
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
       
    //MARK: - ReviewAppDelegate
    
    func setupRatingToggle(){
        FirebaseRealtimeDatabase.toggleValue(withKey: .rateHome) { (value) in
            if value {
                self.checkAppReview()
            }
        }
    }
    
    func checkAppReview() {
        let neverRate = UserDefaults.standard.bool(forKey: "neverRate")
        
        if neverRate == false {
            let username = session.currentUser?.account.name ?? ""
            let title = username == "" ? "\(NSLocalizedString("App Review Hello", comment: ""))!" : "\(NSLocalizedString("App Review Hello", comment: "")), \(username)!"
            
            let review = ReviewApp(title: title, firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 1, secondButtonTag: 2)
            review.delegate = self
            ReviewAppUtilities.showRateMe(review: review)
        }
    }
    
    func choiceFirstTapReview(tag: Int) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "neverRate")
        
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
            ReviewAppUtilities.rateApp(appId: "id1436660799", completion: { (success) in
                print("RateApp \(success)")
            })
        }
    }
    
    func choiceSecondTapReview(tag: Int) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "neverRate")
        
        let negativeReview = NegativeReview()
        negativeReview.delegate = self
        negativeReview.show(animated: true)
    }
    
    func closeNegativeReview() {
        
    }
    
    // MARK: - TextView delegate
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url.absoluteString.contains("pickup") {
            presenter.routePickUpReserve()
        } else if url.absoluteString.contains("return") {
            presenter.routeReturnReserve()
        }
        return true
    }
    
}

