//
//  ReserveDetailsViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import SafariServices
import MapKit

protocol ReserveDetailsViewControllerDelegate {
    func reloadReserve()
    func returnToList()
}

class ReserveDetailsViewController: UIViewController, ReserveDetailsViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, SFSafariViewControllerDelegate, CancellationNoticeDelegate, ProtectionsContractHistory, UIScrollViewDelegate, MultipleChoiceAlertDelegate, ReserveDetailsViewControllerDelegate, StandartCarInformationViewControllerDelegate, ReservationPaymentStepViewControllerDelegate, PaymentProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reservationLabel: UILabel!
    @IBOutlet weak var pickUpDateLabel: UILabel!
    @IBOutlet weak var pickUpStoreTextView: UITextView!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var returnStoreTextView: UITextView!
    @IBOutlet weak var totalReservationLabel: UILabel!
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var payUntilLabel: UILabel!
    @IBOutlet weak var payUntilTitleLabel: UILabel!
      
    @IBOutlet weak var paymentStackView: UIStackView!
    @IBOutlet weak var vehicleGroupsLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var cancellationNotice: CancellationNotice!
    @IBOutlet weak var cancellationNoticeHeightConstrant: NSLayoutConstraint!
    @IBOutlet weak var checkinInfoLabel: UITextView!
    @IBOutlet weak var infoNoShowLabel: UITextView!
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBOutlet weak var customButton: UnidasButton!
    @IBOutlet weak var customButtonView: UIView!
    @IBOutlet weak var customButtomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var groupImageView: UIImageView!
    @IBOutlet weak var legalEntityLabel: UILabel!
    @IBOutlet weak var legalEntityView: UIView!
    @IBOutlet weak var legalEntityViewHeightConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var reservationTotalStackView: UIStackView!
    @IBOutlet weak var payUntilStackView: UIStackView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var dottedLineTopView: DottedLineView!
    
    @IBOutlet weak var paymentTransactionView: UIView!
    @IBOutlet weak var prePaymentTransactionView: UIView!
    
    private var paymentTransaction = TransactionPaymentView.loadFromNib()
    private var prePaymentTransaction = TransactionPaymentView.loadFromNib()
   
    //express
    @IBOutlet weak var dontForgetView: UIView!
    @IBOutlet weak var dontForgetTextView: UITextView!
    
    var tooltipsToRemove: [TooltipView] = []
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint?
    private var contentSizeObservation: NSKeyValueObservation?
    
    var vehicleReservationsDataView: VehicleReservationsDataView?
    var rules: RulesDataView?
    var reservationStep: ReservationStep!
    
    private var presenter: ReserveDetailsPresenterDelegate!
    var isHiddenCancelButton: Bool = false
    
    var delegate: ReserveDetailsViewControllerDelegate?
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ReserveDetailsPresenter(vehicleReservationsDataView: vehicleReservationsDataView,
                                            paymentService: PaymentService(),
                                            companyTermsService: CompanyTermsService(),
                                            reservationUnidasService: ReservationUnidasService(),
                                            firebaseAnalyticsService: FirebaseAnalyticsService())
        
        presenter.delegate = self
        cancellationNotice.delegate = self
        
        presenter.refreshPurchaseTable()
        presenter.getInstallments()
        
        if vehicleReservationsDataView?.isPaid ?? false {
            presenter.fetchRefundPolicy()
            cancellationNoticeHeightConstrant.constant = 207
        } else {
            hideCancellationNotice()
        }
        setupNavigationController()
        setupTableView()
        setupView()
        
        contentSizeObservation = tableView.observe(\UITableView.contentSize, options: [.new]) { (_, change) in
            if let newSize = change.newValue {
                self.tableViewHeightConstraint?.constant = newSize.height
            }
        }
        
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        setupPrePayment()
        checkToggle()
        setupPaymentExpressToggle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
    }
    
    func setupView() {
        startLoading()
        
        dontForgetView.isHidden = true
        
        if let vehicleReservationsDataView = presenter.vehicleReservationsDataView {
            self.vehicleReservationsDataView = vehicleReservationsDataView
        }
        guard let reservation = vehicleReservationsDataView, let rules = rules else { return }
        
        let reservationRulesDataView = createReservationRulesDataView(vehicleReservationsDataView: reservation, rulesDataView: rules)
        
        let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
        reservationStep = reservationRules.make()
        
        let currentStatus = reservationStep.currentStatus
        
        reservationLabel.text = reservation.reservationNumberWithText
        
        let pickupDate = makeDate(date: reservation.pickUpDateOnly, hour: reservation.pickUpHourOnly, dataAndHour: reservation.pickUpDateFormatted)
        pickUpDateLabel.attributedText = pickupDate
        
        let returnDate = makeDate(date: reservation.returnDateOnly, hour: reservation.returnHourOnly, dataAndHour: reservation.returnDateFormatted)
        returnDateLabel.attributedText = returnDate
        
        
        //TODO - incluir em uma funcao private
        if let pickUpStoreAndState = reservation.pickUpStoreAndState,
            let pickupStoreMuttableString = makeStore(storeAndCity: pickUpStoreAndState){
            
            let range = pickupStoreMuttableString.string.nsRange
            pickupStoreMuttableString.addAttribute(.link, value: "pickup", range: range)
            pickUpStoreTextView.attributedText = pickupStoreMuttableString
            pickUpStoreTextView.linkTextAttributes = NSAttributedString.linkAttributed
            pickUpStoreTextView.delegate = self
        }
        
        if let returnStoreAndState = reservation.returnStoreAndState,
            let returnStoreMuttableString = makeStore(storeAndCity: returnStoreAndState){
            
            let range = returnStoreMuttableString.string.nsRange
            returnStoreMuttableString.addAttribute(.link, value: "return", range: range)
            returnStoreTextView.attributedText = returnStoreMuttableString
            returnStoreTextView.linkTextAttributes = NSAttributedString.linkAttributed
            returnStoreTextView.delegate = self
        }
        
        let isPayed = reservation.isPaid == true ? reservation.totalReservationPayed : reservation.totalValueString        
        totalReservationLabel.text = isPayed
        
        if reservation.isPaid {
            payUntilLabel.text = reservation.valuePayment(installments: reservation.installments)
            payUntilTitleLabel.text = NSLocalizedString("Pay Until Title", comment: "")
        } else {
            payUntilLabel.text = reservation.valuePayment(installments: nil)
        }
        
        vehicleGroupsLabel.sizeToFit()
        vehicleGroupsLabel.text = reservation.vehicleGroup
        groupLabel.text = reservation.group
        groupDescriptionLabel.text = reservation.groupDescription.lowerCapitalize()
        
        if let groupImageURL = reservation.picture {
            groupImageView.setImage(withURL: groupImageURL)
        }
        
        if let dateCheckInAvailable = reservation.dateShortFormatted {
            checkinInfo(date: dateCheckInAvailable)
        } else {
            checkinInfoLabel.text = ""
        }
        
        customButton.setTitle(reservationStep.buttonTitle, for: .normal)
        customButton.isEnabled = reservationStep.buttonEnabled
        
        if reservation.isLegalEntity {
            optionsButton.isHidden = true
            
            legalEntityLabel.text = reservation.legalEntityName
            
            if reservation.isLegalEntityRequiredPayment == false {
                payUntilStackView.isHidden = true
            }
            
            //APRESENTACAO
            if currentStatus == .checkin && reservation.isPassStepPaymentCheckin {
                customButton.isHidden = false
            } else {
                
                if currentStatus == .checkin && !reservation.isLegalEntityRequiredPrePayment {
                    customButton.isHidden = true
                }
                
                if currentStatus == .paid && !reservation.isLegalEntityRequiredPayment {
                    customButton.isHidden = true
                }
            }
            
        } else {
            legalEntityViewHeightConstraint?.constant = 0
            legalEntityView.isHidden = true
        }
        
       
        if reservation.express {
            configureExpress()
        }
        
        let paymentTransactionObject = TransactionPayment(fourLastNumberCard: reservation.fourLastNumberCard,
                                                          brandImage: reservation.brandImage,
                                                          paymentValue: reservation.valuePayment(installments: reservation.installments),
                                                          status: reservation.paymentStatus, statusConfirmation: nil)
                                                          
                                                
        paymentTransaction.setup(type: .payment, transactionPayment: paymentTransactionObject)
        
        let prePaymentTransactionObject = TransactionPayment(fourLastNumberCard: reservation.fourLastNumberCardPreAuthorization,
                                                          brandImage: reservation.prePaymentBrandImage,
                                                          paymentValue: reservation.preAuthorizationValue,
                                                          status: reservation.prePaymentStatus, statusConfirmation: nil)
        
        prePaymentTransaction.setup(type: .prePayment, transactionPayment: prePaymentTransactionObject, express: reservation.express)
        
        paymentTransactionView.addSubview(paymentTransaction)
        prePaymentTransactionView.addSubview(prePaymentTransaction)
        
        scrollView.delegate = self
        endLoading()
        
    }
    
    // MARK: - ReserveDetailsViewDelegate
    func reloadData() {
        tableView.reloadData()
    }
    
    func showInstallmentsValue(installments: String) {
        guard let dataView = presenter.vehicleReservationsDataView else { return }
        payUntilLabel.text = dataView.valuePayment(installments: installments)
    }
    
    func updateTransactionPayment(installments: String) {
        guard let dataView = presenter.vehicleReservationsDataView else { return }
        let valuePayment = dataView.valuePayment(installments: installments)
        
        paymentTransaction.update(valuePayment: valuePayment)
    }
    
    func showRefundPolicy(companyTermsDataView: CompanyTermsDataView) {
        LoadingScreen.setLoadingScreen(view: self.view)
        guard let strURL = companyTermsDataView.description.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let politicRefund = URL(string: strURL) else { return }
        
        cancellationNotice.setUrl(urlPolicy: politicRefund)
        
        let cancellationPolicy = NSLocalizedString("In case of No-Show on prepaid reservations, check the amount to be refunded in the Cancellation Policy.", comment: "")
        let cancellationPolicyText = "Política de cancelamento."
        
        let cancellationPolicyFormatted = makeMuttableLinkString(text: cancellationPolicy, linkString: cancellationPolicyText, url: politicRefund)
        infoNoShowLabel.delegate = self
        infoNoShowLabel.isEditable = false
        infoNoShowLabel.isSelectable = true
        infoNoShowLabel.attributedText = cancellationPolicyFormatted
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func setupNavigationController() {
        if !isHiddenCancelButton {
            let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
            let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancel))
            navigationItem.leftBarButtonItem = backButton
        }
    }
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: "ReservationDetailsSectionHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "ReservationDetailsSectionHeader")
        tableView.register(cellType: ReservationDetailsTableViewCell.self)
        tableView.register(cellType: ProtectionsContractHistoryCell.self)
        tableView.separatorStyle = .none
        tableView.separatorColor = UIColor.clear
    }
    
    func dismiss() {
        dismiss(animated: false, completion: nil)
        //delegate?.notFoundReserve()
    }
    
    func startLoading() {
        LoadingScreen.setLoadingScreen(view: self.view)
        tableView.isHidden = true
    }
    
    func endLoading() {
        LoadingScreen.removeLoadingScreen(view: self.view)
        tableView.isHidden = false
    }
    
    func showAlertMaps(coordinate: CLLocationCoordinate2D, maps: [URL], appsName: [String]) {
        let actionSheet = UIAlertController(title: NSLocalizedString("Route Options", comment: "Reservation Options"), message: nil, preferredStyle: .actionSheet)
        
        let openAppleMaps = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.openAppleMaps(coordinate: coordinate)
        })
        
        actionSheet.addAction(openAppleMaps)
        
        for i in 0..<maps.count {
            let openRandomMap = UIAlertAction(title: appsName[i], style: .default, handler: { (action) -> Void in
                UIApplication.shared.open(maps[i], options: [:], completionHandler: nil)
            })
            
            actionSheet.addAction(openRandomMap)
            
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Dismiss", comment: "Dimiss"), style: .cancel)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openAppleMaps(coordinate: CLLocationCoordinate2D) {
        
        let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: place)
        mapItem.name = NSLocalizedString("Route", comment: "Route title")
        
        mapItem.openInMaps(launchOptions: nil)
    }
    
    func present(error: Error) {
        alertError(errorMessage:  error.localizedDescription)
    }
    
    func showCheckIn(vehicleReservationDataView: VehicleReservationsDataView) {
       performSegue(withIdentifier: "showPayment", sender: vehicleReservationDataView)
    }
    
    func showConfirmCheckin(reservation: VehicleReservationsDataView) {
        
        let title = NSLocalizedString("Checkin are you sure title", comment: "")
        let message = NSLocalizedString("Checkin are you sure", comment: "")
        let firstButtonLabel = NSLocalizedString("Checkin ok", comment: "")
        let secondButtonLabel = NSLocalizedString("Checkin cancel", comment: "")
        let image = #imageLiteral(resourceName: "icon-cancel-reservation-alert")
        let multipleChoiceAlert = MultipleChoiceAlert(title: title, subTitle: message, firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel, image: image, firstChoiceButtonColor: UIColor.themeImportantButton, firstButtonTag: 1, secondButtonTag: 1)
        
        multipleChoiceAlert.delegate = self
        multipleChoiceAlert.show(animated: true)
    }
    
    func hideCancellationNotice(){
        cancellationNoticeHeightConstrant.constant = 0
        cancellationNotice.isHidden = true
        infoNoShowLabel.isHidden = true
    }
    
    func setupReservationRules(rules: RulesDataView) {
        self.rules = rules
    }
    
    func updatePrePayment(value: String) {
        prePaymentTransaction.valueLabel.text = value
    }
    
    func startPrePaymentLoading() {
        
       /* let frame = prePaymentTransaction.valueLabel.frame
        
        activityIndicatorView.frame = frame
        activityIndicatorView.addSubview(prePaymentTransactionView)
        activityIndicatorView.startAnimating()*/
        
    }
    
    func endPrePaymentLoading() {
       // activityIndicatorView.stopAnimating()
    }
    
    // MARK: Private Function
    private func makeDate(date: String, hour: String, dataAndHour: String) -> NSMutableAttributedString {
        
        let localizedString = NSLocalizedString("Date %@ at %@", comment: "")
        let searchString = String(format: localizedString, date, hour)
        let rangePickupDate = (dataAndHour as NSString).range(of: searchString)
        let dateMutable = NSMutableAttributedString(string: dataAndHour, attributes: [NSAttributedString.Key.font : UIFont.regular(ofSize: 12),
                                                                                      NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)])
        dateMutable.addAttributes([
            NSAttributedString.Key.font : UIFont.bold(ofSize: 14),
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)], range: rangePickupDate)
        
        return dateMutable
    }
    
    private func configureExpress(){
        infoNoShowLabel.isHidden = true
        dontForgetView.isHidden = false
        
        dontForgetTextView.contentInset = UIEdgeInsets(top: -7.0,left: -5.0,bottom: 0,right: 0.0);
        let dontForgetInfoString = NSLocalizedString("Checkin confirmation express - Dont Forget Description", comment: "")
        let dontForgetInfoBoldString = NSLocalizedString("Checkin confirmation express - Dont Forget Bold Description", comment: "")
        let rangeDontForget = (dontForgetInfoString as NSString).range(of: dontForgetInfoBoldString)
        let messageDontForget = NSMutableAttributedString(string: dontForgetInfoString, attributes: [
                                                            NSAttributedString.Key.font : UIFont.regular(ofSize: 12),
                                                            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)])
        messageDontForget.addAttribute(NSAttributedString.Key.foregroundColor, value: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1), range: rangeDontForget)
        messageDontForget.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 12)], range: rangeDontForget)
        dontForgetTextView.attributedText = messageDontForget
        
        checkinInfoLabel.isHidden = true
    }
    
    private func makeStore(storeAndCity: String?) -> NSMutableAttributedString? {
        guard let storeAndCity = storeAndCity else { return nil }
        let localizedString = NSLocalizedString("Store %@", comment: "")
        let store = String(format: localizedString, storeAndCity)
        let muttableString = NSMutableAttributedString(string: store)
        let range = muttableString.string.nsRange
        
        muttableString.addAttribute(.font, value: UIFont.bold(ofSize: 12), range: range)
        return muttableString
    }
    
    private func headerExpandClose(button: UIButton) {
        setSizeForList(listId: button.tag)
        removeTooltips()
        tableView.reloadSections(IndexSet(integer: button.tag), with: .automatic)
    }
    
    private func safariUrlOpen(withUrl url: URL){
        let safariViewController = SFSafariViewController(url: url)
        safariViewController.clearCache()
        safariViewController.delegate = self
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    private func createReservationRulesDataView(vehicleReservationsDataView: VehicleReservationsDataView,
                                                rulesDataView: RulesDataView) -> ReservationRulesDataView {
        return ReservationRulesDataView(userValidated: rulesDataView.userValidated,
                                        reservationNumber: vehicleReservationsDataView.reserveNumber,
                                        checkinAvailable: vehicleReservationsDataView.checkinAvailable,
                                        reservationPaid: vehicleReservationsDataView.reservationPaid,
                                         hoursToCheckin: vehicleReservationsDataView.hoursToCheckin,
                                        userAvailableToCheckin: rulesDataView.userAvailableToCheckin,
                                        preAuthorizationPaid: vehicleReservationsDataView.preAuthorizationPaid,
                                        isFranchise: vehicleReservationsDataView.isFranchise,
                                        dueDate: vehicleReservationsDataView.dueDate,
                                        dueTime: vehicleReservationsDataView.dueTime,
                                        express: vehicleReservationsDataView.express,
                                        financialPendency: rulesDataView.financialPendency)
    }
    
    private func setSizeForList(listId: Int) {
        
        switch listId {
        case 0:
            if presenter.numberOfProtections == 0 {
                presenter.numberOfProtections = presenter.protectionsCount
            } else {
                presenter.numberOfProtections = 0
            }
            
        case 1:
            if presenter.numberOfEquipments == 0 {
                presenter.numberOfEquipments = presenter.equipmentsCount
            } else {
                presenter.numberOfEquipments = 0
            }
        default: break
        }
    }
    
    private func openTooltip(button: UIButton, observation: String){
        
        if !tooltipsToRemove.isEmpty {
            removeTooltips()
            return
        }
        
        let toolTip = TooltipView.loadFromNib()
        toolTip.alpha = 0
        
        if observation.isEmpty { return }
        
        toolTip.setup(description: observation)
        
        removeTooltips()
        view.addSubview(toolTip)
        
        toolTip.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 5).isActive = true
        toolTip.leadingAnchor.constraint(equalTo: self.tableView.leadingAnchor, constant: 10).isActive = true
        toolTip.trailingAnchor.constraint(equalTo: self.tableView.trailingAnchor, constant: 10).isActive = true
        toolTip.widthAnchor.constraint(equalTo: tableView.widthAnchor, constant: -20).isActive = true
        tooltipsToRemove.append(toolTip)
        
        UIView.animate(withDuration: 0.5) {
            toolTip.alpha = 1.0
        }
    }
    
    private func removeTooltips(){
        tooltipsToRemove.forEach { (tooltip) in
            remove(tooltip: tooltip)
        }
        tooltipsToRemove = []
    }
    
    private func remove(tooltip: TooltipView){
        UIView.animate(withDuration: 0.5,
                       animations: { () -> Void in
                        tooltip.alpha = 0
        }, completion: { (finished) -> Void in
            tooltip.removeFromSuperview()
        })
    }
    
    private func makeMuttableLinkString(text: String, linkString: String, url: URL) -> NSMutableAttributedString {
        let localizedString = NSLocalizedString(text, comment: "")
        let range = (text as NSString).range(of: linkString)
        let color = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        let attrs: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: color,
            NSAttributedString.Key.font: UIFont.regular(ofSize: 14)
        ]
        
        let stringMuttable = NSMutableAttributedString(string: localizedString, attributes: attrs)
        stringMuttable.addAttribute(.link, value: url, range: range)
        stringMuttable.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 14)], range: range)
        stringMuttable.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        
        return stringMuttable
    }
    
    
    private func checkinInfo(date: String){
        let checkinInfoLocalized = NSLocalizedString("You can check in via the app or store from %@", comment: "")
        let checkInInfoString = String(format: checkinInfoLocalized, date)
        let attrs: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1),
            NSAttributedString.Key.font: UIFont.regular(ofSize: 14)]
        
        let checkinInfoAttributted = NSMutableAttributedString(string: checkInInfoString, attributes: attrs)
        let range = (checkinInfoAttributted.string as NSString).range(of: "check-in")
        checkinInfoAttributted.addAttributes([NSAttributedString.Key.font : UIFont.bold(ofSize: 14)], range: range)
        self.checkinInfoLabel.attributedText = checkinInfoAttributted
    }
    
    private func checkToggle(){
        if !reservationStep.express {
            guard let currentStatus = reservationStep?.currentStatus else {
                customButton.isEnabled = false
                return
            }
            
            if reservationStep?.action == .editProfile {
                customButton.isEnabled = true
                return
            }
            
            if currentStatus == .paid || currentStatus == .checkin {
                customButton.isEnabled = false
            }
            
            if currentStatus == .paid {
                FirebaseRealtimeDatabase.toggleValue(withKey: .doPayment) { (toggle) in
                    if !toggle {
                        self.customButton.isEnabled = false
                    }else{
                        self.customButton.isEnabled = true
                    }
                }
            }
            
            if currentStatus == .checkin {
                FirebaseRealtimeDatabase.toggleValue(withKey: .checkinReservation) { (toggle) in
                    if !toggle {
                        self.customButton.isEnabled = false
                    }else{
                        self.customButton.isEnabled = true
                    }
                }
            }
        }
    }
    
    func setupPaymentExpressToggle() {
        
        guard let reservation = vehicleReservationsDataView else {return}
        
        if reservation.express {
            FirebaseRealtimeDatabase.toggleValue(withKey: .doPaymentExpress) { (value) in
                if !value {
                    self.customButton.isEnabled = false
                }
            }
        }
    }

    private func showQrCodeExpress(){
        guard let storyboard = UIStoryboard(name: "ContractHistory", bundle: nil) as? UIStoryboard, let qrCode = vehicleReservationsDataView?.qrCode else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "ContractQRCodeViewController") as? ContractQRCodeViewController {
            vc.qrCodeInformation = qrCode
            self.navigationController?.present(vc, animated: true, completion: nil)

        }
    }
    
    private func setupPrePayment(){
        if let vehicleReservationsDataView = presenter.vehicleReservationsDataView {
            self.vehicleReservationsDataView = vehicleReservationsDataView
        }
        guard let reservation = vehicleReservationsDataView else { return }
        
        if let prePaymentStatus = reservation.prePaymentStatus, prePaymentStatus == .notPay {
            presenter.fetchPrePaymentValue()
        }
    }
    
    func canShowPendencies(value: Bool) {
        if value {
            showPendencies()
        }else{
            showGoToStoreModal()
        }
    }
    
    func showGoToStoreModal() {
        guard let storyboard = UIStoryboard(name: "Pendencies", bundle: nil) as? UIStoryboard else {return}
        if let vc = storyboard.instantiateViewController(withIdentifier: "SerasaCredDefenseNotificationVC") as? SerasaCredDefenseNotificationViewController {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    private func showPendencies(){
        performSegue(withIdentifier: "showPendencies", sender: nil)
    }
    
    //MARK: Actions
    @IBAction func tapCustomButton(_ sender: Any) {
        
        switch reservationStep?.action {
        case .checkin:
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                if let reservation = vehicleReservationsDataView {
                    presenter.checkInReservation(reservation: reservation, isCheckinConfirmed: false)
                }
            }
        case .payment:
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                let storyBoard = UIStoryboard(name: "StandartCarRent", bundle: .main)
                
                if let reservationDetailsController = storyBoard.instantiateViewController(withIdentifier: "ReservationPaymentStepVC") as? ReservationPaymentStepViewController {
                    
                    if let reservation = vehicleReservationsDataView {
                        reservationDetailsController.oldReservation = reservation
                        reservationDetailsController.isAntecipatedPayment = true
                        reservationDetailsController.isDetailScreen = true
                        reservationDetailsController.delegate = self
                    }
                    //navigationItem.setBackButtonTitle(title: "Cancelar")
                    show(reservationDetailsController, sender: nil)
                }
            }
        case .qrcode:
            if reservationStep.express {
                showQrCodeExpress()
            }
            performSegue(withIdentifier: "showMyCar", sender: nil)
        case .editProfile:
            presenter.fetchCredDefenseSerasaStatus()
        default:
            break    
        }
    }
    
    @objc func touchCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func touchTableView() {
        removeTooltips()
    }
    
    @IBAction func tapButtonOptions(_ sender: Any) {
        guard let reservation = vehicleReservationsDataView else { return }
        
        let reservationNumber = reservation.reserveNumber
        
        let localizedString = NSLocalizedString("Reservation %@ Selected", comment: "")
        let title = String(format: localizedString, reservationNumber)
        let subTitle = NSLocalizedString("What do you want to do?", comment: "")
        let firstChoiceLabel = NSLocalizedString("Cancel Button", comment: "")
        let secondChoiceLabel = NSLocalizedString("Change Button", comment: "")
        
        FirebaseRealtimeDatabase.toggleValue(withKey: .changeReservation) { (toggle) in
            guard let reservation = self.vehicleReservationsDataView else { return }
            
            var firstChoiceEnabled = true
            var secondChoiceEnabled = reservation.isPaid ? false : true
            
            if !toggle {
                secondChoiceEnabled = false
            }else {
                
                if reservation.isLegalEntity {
                    firstChoiceEnabled = false
                    secondChoiceEnabled = false
                }
            }
            
            let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-car-check-general"), firstChoiceButtonEnabled: firstChoiceEnabled, secondChoiceButtonEnabled: secondChoiceEnabled)
            alert.delegate = self
            alert.show(animated: true)
        }
    }
    
    @objc func touchHeader(_ sender: UIButton?){
        guard let buttonExpandClose = sender else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    @objc func headerTap(gestureRecognizer: UITapGestureRecognizer) {
        guard let view = gestureRecognizer.view,
            let header = view as? ReservationDetailsSectionHeader, let buttonExpandClose = header.expandCloseButton else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    //MARK: - TableViewDelegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return presenter.numberOfProtections
        case 1: return presenter.numberOfEquipments
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProtectionsContractHistoryCell.self)
        cell.delegate = self
        
        guard let reservation = vehicleReservationsDataView else { return UITableViewCell() }
        
        let daysInterval = reservation.daysInterval ?? 1
        let totalDiaries = String(daysInterval)
        
        guard let purchaseDataView = presenter.purchaseDataView(indexPath: indexPath) else { return UITableViewCell() }
        
        var hiddenPaymentInformation = false
        
        if reservation.isLegalEntity, reservation.isLegalEntityRequiredPayment == false {
            hiddenPaymentInformation = true
        }
        
        cell.setup(purchaseDataView: purchaseDataView, totalDiaries: totalDiaries, hiddenPaymentInformation: hiddenPaymentInformation)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ReservationDetailsSectionHeader") as! ReservationDetailsSectionHeader
        
        header.titleLabel.text = presenter.titleForSection(section: section)
        
        header.isUserInteractionEnabled = true
        header.tag = section
        header.expandCloseButton.tag = section
        
        let rowsInSection = tableView.numberOfRows(inSection: section)
        
        if rowsInSection == 0 {
            UIView.animate(withDuration: 0.5) {
                header.expandCloseButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerTap))
        header.addGestureRecognizer(tapRecognizer)
        header.expandCloseButton.addTarget(self, action: #selector(touchHeader(_:)), for: .touchUpInside)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presenter.heightForHeaderSection(section: section)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeTooltips()
    }
    
    // MARK: - TextView delegate
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange) -> Bool {
        if url.absoluteString.contains("check-in") {
            if let reservation = vehicleReservationsDataView {
                performSegue(withIdentifier: "showPayment", sender: reservation)
            }
        } else if url.absoluteString.contains("pickup") {
            presenter.routePickUpReserve()
        } else if url.absoluteString.contains("return") {
            presenter.routeReturnReserve()
        } else {
            safariUrlOpen(withUrl: url)
        }
        
        return false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
    }
    
    // MARK: - SafariView delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - CancellationNoticeDelegate
    func openUrl(with url: URL) {
        safariUrlOpen(withUrl: url)
    }
    
    //MARK: - ProtectionsContractHistory    
    func didPressToolTipFromProtections(cell: ProtectionsContractHistoryCell, button: UIButton, observation: String?) {
        if let observation = observation, !observation.isEmpty {
            openTooltip(button: button, observation: observation)
        }
    }
    
    func askToUpdateApp() {
        
        let title = NSLocalizedString("App Update Title", comment: "")
        let subTitle = NSLocalizedString("App Update Message", comment: "")
        let firstChoiceLabel = NSLocalizedString("App Update No", comment: "")
        let secondChoiceLabel = NSLocalizedString("App Update Yes", comment: "")
        
        let alert = MultipleChoiceAlert(title: title, subTitle: subTitle, firstChoiceButtonLabel: firstChoiceLabel, secondChoiceButtonLabel: secondChoiceLabel, image: #imageLiteral(resourceName: "icon-alert"), firstChoiceButtonEnabled: true, secondChoiceButtonEnabled: true, firstButtonTag: 3, secondButtonTag: 2)
        alert.delegate = self
        alert.show(animated: true)
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        if tag == 1, let reservation = vehicleReservationsDataView {
            presenter.checkInReservation(reservation: reservation, isCheckinConfirmed: true)
        } else if tag != 3 {
            if let reservation = vehicleReservationsDataView {
                performSegue(withIdentifier: "showCancel", sender: reservation)
            }
        }
    }
    
    func choiceSecondTap(tag: Int) {
        
        if tag == 2 {
            VersionControl.openAppStore()
        }else if tag != 1, let reservation = vehicleReservationsDataView {
            
            if presenter.compareAppVersionToUpdate() == true {
                askToUpdateApp()
            }else{
                presenter.addEventChange(reservation: reservation)
                performSegue(withIdentifier: "showStandartCar", sender: reservation)
            }
        }
    }
    
    // MARK: - ReserveDetailsViewControllerDelegate
    func reloadReserve() {
        presenter.viewDidLoad()
    }
    
    func returnToList() {
        dismiss(animated: true) {
            //reload list
        }
    }
    
    func notFoundReserve() {
        
    }
    
    //MARK: - StandartCarInformationViewControllerDelegate
    func redirectToReservationsTab() {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - ReservationPaymentStepViewControllerDelegate {
    func redirectPaymentToReservationsTab() {
        dismiss(animated: true) {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    //MARK: - PaymentProtocol
    func reloadScreen() {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStandartCar" {
            let navigation = segue.destination as? UINavigationController
            let reservationViewController = navigation?.viewControllers.first as? StandartCarInformationViewController
            reservationViewController?.oldReservation = vehicleReservationsDataView
            reservationViewController?.delegate = self
        }
        
        if segue.identifier == "showCancel" {
            if let destination = segue.destination as? CancelConfirmationViewController {
                destination.vehicleReservationsDataView = sender as? VehicleReservationsDataView
                destination.delegate = self
            }
        } else if segue.identifier == "showPayment" {
            //PEDRO
            let navigation = segue.destination as? UINavigationController
            let paymentViewController = navigation?.viewControllers.first as? PaymentViewController
            paymentViewController?.vehicleReservationsDataView = sender as? VehicleReservationsDataView
            paymentViewController?.delegate = self
        } else if segue.identifier == "showMyCar" {
            let navigation = segue.destination as? UINavigationController
            let myCarViewController = navigation?.viewControllers.first as? MyCarViewController
            myCarViewController?.contractNumber = vehicleReservationsDataView?.contractNumber
        }else if segue.identifier == "" {
            
        }
    }
}
