//
//  ContractsViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 17/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class ContractsViewController: UIViewController, ContractsViewDelegate, UIViewControllerPreviewingDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView?
    
    private var presenter: ContractsPresenterDelegate!
    private var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = ContractsPresenter(contractService: ContractService())
        presenter.delegate = self
        
        setupActivityIndicator()
        setupTableView()
        setupForceTouchIfAvailable()
        
        presenter.fetchContract()
        setHeaderUnidas()
        configureBackButton()
    }
    // MARK: - Private Functions
    private func setupTableView() {
        guard let tableView = tableView else { return }
        tableView.register(cellType: ContractTableViewCell.self)
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        tableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    private func setupForceTouchIfAvailable() {
        guard let tableView = tableView else { return }
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: tableView)
        }
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
    }
    
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
    
    //MARK: - Interface Builder Actions
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ContractTableViewCell.self)
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = #colorLiteral(red: 0.9782002568, green: 0.9782230258, blue: 0.9782107472, alpha: 1)
        cell.selectedBackgroundView = bgColorView
        
        cell.selectionStyle = .none
        
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: tableView)
        }
        
        cell.setup(contractDataView: presenter.contract(at: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(at: indexPath.row)
    }
    
    
    // MARK: - ContractsViewDelegate
    
    func startLoading() {
        guard let tableView = tableView else { return }
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func showEmptyView(title: String, message: String) {
        guard let tableView = tableView else { return }
        let emptyView = EmptyDataView.loadFromNib()
        emptyView.frame = tableView.frame
        emptyView.setup(title: title, message: message, image: #imageLiteral(resourceName: "ico-empty-state-contract-history"), buttonTitle: nil, buttonIsHidden: true)
        tableView.backgroundView = emptyView
    }
    
    func reloadData() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func didSelectRow(contractDataView: ContractDataView) {
        performSegue(withIdentifier: "showContractDetails", sender: contractDataView)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as? ContractDetailsTableViewController
        let contractDataView = sender as? ContractDataView
        destination?.contractDataView = contractDataView
    }
    
    // MARK: - Previewing delegate
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let tableView = tableView, let indexPath = tableView.indexPathForRow(at: location) else { return nil }
        let contractDetailsController = storyboard?.instantiateViewController(withIdentifier: "ContractDetails") as? ContractDetailsTableViewController
        contractDetailsController?.contractDataView = presenter.contract(at: indexPath.row)
        previewingContext.sourceRect = tableView.rectForRow(at: indexPath)
        return contractDetailsController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        navigationController?.show(viewControllerToCommit, sender: nil)
    }
    
}

