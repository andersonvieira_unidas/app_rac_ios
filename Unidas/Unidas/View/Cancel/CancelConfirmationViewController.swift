//
//  CancelConfirmationViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

protocol CancelConfirmationDelegate: class {
    func keepReservation()
    func reloadReservations()
}

class CancelConfirmationViewController: UIViewController, CancelConfirmationViewDelegate, CancelConfirmationDelegate {
    var vehicleReservationsDataView: VehicleReservationsDataView?
    var presenter: CancelConfirmationPresenter!
    var delegate: ReserveDetailsViewControllerDelegate?
    
    @IBOutlet weak var cancelTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CancelConfirmationPresenter(calculationCancelService: CalculationCancelService())
        presenter.delegate = self
        
        if let vehicleReservation = vehicleReservationsDataView {
            presenter.viewDidLoad(vehicleReservationsDataView: vehicleReservation)
        }
    }
    
    @IBAction func tapKeepReservation(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapCancelReservation(_ sender: Any) {
        performSegue(withIdentifier: "showReasonCancelation", sender: nil)
    }
    // MARK: - CancelConfirmationViewDelegate
    func startLoading() {
        LoadingScreen.setLoadingScreen(view: self.view)
    }
    
    func endLoading() {
        LoadingScreen.removeLoadingScreen(view: self.view)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func updateDescription(calculationCancel: CalculationCancelDataView?) {
        
        guard let vehicleReservation = vehicleReservationsDataView else { return }
        let fontBold = UIFont.medium(ofSize: 12)
        let font = UIFont.regular(ofSize: 12)
        
        if let calculationCancel = calculationCancel {
            guard let totalValue = calculationCancel.formattedDiscount() else { return }
            
            let localizedString = NSLocalizedString("We've identified that your reservation %@ is already paid. As informed at the time of hiring, we enforce the reversal rules in accordance with the date of withdrawal.\n\nYou'll receive a refound the amount of %@ in your credit card in up 3 hours.", comment: "Cancellation message paid out")
            let finalString = String(format: localizedString, vehicleReservation.reserveNumber, totalValue)
            
            let rangeReservation = (finalString as NSString).range(of: vehicleReservation.reserveNumber)
            let rangeValue = (finalString as NSString).range(of: totalValue)
            
            let message = NSMutableAttributedString(string: finalString, attributes: [NSAttributedString.Key.font : font])
            message.addAttributes([NSAttributedString.Key.font : fontBold], range: rangeReservation)
            message.addAttributes([NSAttributedString.Key.font : fontBold], range: rangeValue)
            cancelTextLabel.attributedText = message
        } else {
            
            let localizedString = NSLocalizedString("Are you sure you want to cancel your reservation %@?\n\n You wont be charged in case you continue with the cancellation.", comment: "Cancellation message not pay")
            let finalString = String(format: localizedString, vehicleReservation.reserveNumber)
            let range = (finalString as NSString).range(of: vehicleReservation.reserveNumber)
            
            let message = NSMutableAttributedString(string: finalString, attributes: [NSAttributedString.Key.font : font])
            message.addAttributes([NSAttributedString.Key.font : fontBold], range: range)
            cancelTextLabel.attributedText = message
        }
    }
    
    // MARK: - CancelConfirmationDelegate
    func keepReservation() {
        dismiss(animated: true, completion: nil)
    }
    
    func reloadReservations() {
        dismiss(animated: true) {
            self.delegate?.returnToList()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReasonCancelation" {
            if let destination = segue.destination as? CancelViewController {
                destination.vehicleReservationsDataView = vehicleReservationsDataView
                destination.delegate = self
            }
        }
    }
}
