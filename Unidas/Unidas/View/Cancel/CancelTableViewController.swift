//
//  CancelTableViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class CancelViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CancelViewDelegate, SimpleAlertDelegate {
    @IBOutlet weak var cancelButton: UnidasBorderButton!
    var delegate: CancelConfirmationDelegate?
    
    var presenter: CancelPresenter!
    var vehicleReservationsDataView: VehicleReservationsDataView?
    var userDocument: String?
    
    lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.color = UIColor.black
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = CancelPresenter(cancellationReasonService: CancellationReasonService(),
                                    calculationCancelService: CalculationCancelService(),
                                    firebaseAnalyticsService: FirebaseAnalyticsService())
        presenter.delegate = self
        
        setupTableView()
        presenter.getCancellationReasons()
        cancelButton.isEnabled = false
    }
    
    
    func setupTableView() {
        tableView.register(cellType: CancellationReasonTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundView = activityIndicatorView
    }
    
    @objc func touchCancelItem() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfReasons
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CancellationReasonTableViewCell.self)
        let cancellationReasons = presenter.cancellationReason(at: indexPath.row)
        cell.setup(reason: cancellationReasons.reasonsDescription)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cancelButton.isEnabled = true
        guard let cell = tableView.cellForRow(at: indexPath) as? CancellationReasonTableViewCell else { return }
        cell.reasonLabel.font = UIFont.bold(ofSize: 14)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? CancellationReasonTableViewCell else { return }
        cell.reasonLabel.font = UIFont.regular(ofSize: 14)
    }
    
    @IBAction func touchConfirmCancellation(_ sender: UIButton) {
        presenter.confirmCancellation(at: tableView.indexPathForSelectedRow, vehicleReservationsDataView: vehicleReservationsDataView, userDocument: userDocument)
    }
    
    @IBAction func touchKeepReservation(_ sender: Any) {
        dismiss(animated: true) {
            if let delegate = self.delegate {
                delegate.keepReservation()
            }
        }
    }
    
    
    // MARK: - Cancel View Delegate
    
    func finishConfirmCancellation() {
        let title = NSLocalizedString("Your reservation has been successfully canceled", comment: "Reservation Canceled successfully alert title")
        let subtitle = NSLocalizedString("We`re sorry your desistance.\nWhenever you need, count with Unidas.", comment: "Reservation desistance alert message")
        
        let alert = SimpleAlert(title: title,
                                subTitle: subtitle,
                                image:  #imageLiteral(resourceName: "icon-cancel-reservation-alert"))
        
        alert.delegate = self
        alert.show(animated: true)
    }
    
    
    func dismiss() {
        self.dismiss(animated: false, completion: nil)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func startLoading() {
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
        cancelButton.isHidden = true
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true
        cancelButton.isHidden = false
    }
    
    func startCancelLoading() {
        cancelButton.isEnabled = false
        cancelButton.loading(true, activityIndicatorViewStyle: .gray)
    }
    
    func endCancelLoading() {
        cancelButton.isEnabled = true
        cancelButton.loading(false)
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    // MARK:- CustomAlertCloseDelegate
    func close() {
        endLoading()
        self.dismiss(animated: true) {
            self.delegate?.reloadReservations()
        }
    }
}
