//
//  MoreTableViewController.swift
//  Unidas
//
//  Created by Mateus Padovani on 24/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MoreViewDelegate {
    private var presenter: MorePresenterDelegate!
    
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MorePresenter()
        presenter.delegate = self
        presenter.viewDidLoad()
        
        setupTableView()
        setHeaderUnidas()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
             self.setHeaderUnidas()
        }       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    private func setupTableView(){
        guard let tableView = tableView else { return }
        tableView.register(cellType: MoreItemTableViewCell.self)
    }
    
    func reloadData() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
    }
    
    func performSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: nil)
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modelItem = presenter.moreItem(at: indexPath.row)
        presenter.to(segueIdentifier: modelItem.action)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.moreItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: MoreItemTableViewCell.self)
        cell.setup(moreItemDataView: presenter.moreItem(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showFindStore", let destination = segue.destination as? FindStoreViewController {
            destination.hidesBottomBarWhenPushed = true
        }
    }
}
