//
//  CallCenterViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class CallCenterViewController: UIViewController, CallCenterViewDelegate, UITableViewDataSource, UITableViewDelegate {
    private var presenter: CallCenterPresenterDelegate!
    private var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderUnidas(barTintColor: #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1))
        
        presenter = CallCenterPresenter(callCenterService: CallCenterService())
        presenter.delegate = self
        setupTableView()
        setupActivityIndicator()
        configureButtonBack()
        presenter.fetchCallCenter()
    }
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return presenter.numberOfRows
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CallCenterBusinessChatTableViewCell.self)
            
            let title = RemoteConfigUtil.recoverParameterValue(with: "business_chat_title")
            let description = RemoteConfigUtil.recoverParameterValue(with: "business_chat_description")
            
            cell.setup(title: title, description: description)
            cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            cell.selectionStyle = .none
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: CallCenterTableViewCell.self)
            
            cell.setup(callCenterDataView: presenter.callCenter(at: indexPath.row))
            cell.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                return 130
            case 1:
                return 100
            case 2:
                return 150
            default:
                return 100
            }
        } else {
            return 120
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0{
            presenter.didSelectRowAt(at: indexPath)
        }
    }
    
    // MARK: - CallCenterViewDelegate
    
    func startLoading() {
        guard let tableView = tableView else { return }
        tableView.backgroundView = activityIndicatorView
        activityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        activityIndicatorView.stopAnimating()
    }
    
    func reloadData() {
        guard let tableView = tableView else { return }
        tableView.reloadData()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func openBusinessChat(url: URL) {
        UIApplication.shared.open(url)
    }
     
    // MARK: - Interface Builder Functions
       
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Private functions
    
    private func setupTableView() {
        guard let tableView = tableView else { return }
        tableView.register(cellType: CallCenterTableViewCell.self)
        tableView.register(cellType: CallCenterBusinessChatTableViewCell.self)
        tableView.delegate = self
        
        tableView.tableFooterView = UIView()
    }
    
    private func setupActivityIndicator() {
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
    }
    
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
}
