//
//  NotificationConfigViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class NotificationConfigViewController: UIViewController, NotificationConfigurationViewDelegate, UITableViewDelegate, UITableViewDataSource, NotificationConfigurationTableViewCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var iconTitleImageView: UIImageView!
    
    lazy var presenter: NotificationConfigurationPresenterDelegate = {
        let presenter = NotificationConfigurationPresenter(notificationConfigurationService: NotificationConfigurationService())
        presenter.delegate = self
        return presenter
    }()
    
    lazy var ativityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .gray)
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicatorView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureButtonBack()
        setupTableView()
        setupView()
        presenter.viewDidLoad()
    }
    
    //MARK: - NotificationConfigurationViewDelegate
    func reloadData() {
        tableView.reloadData()
    }
    
    func startLoading() {
        ativityIndicatorView.startAnimating()
    }
    
    func endLoading() {
        ativityIndicatorView.stopAnimating()
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func openNotificationSettings() {
        let message = NSLocalizedString("Settings Notification Description", comment: "")
        askPermissionToSettings(withMessage: message)
    }
    
    
    //MARK: TableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfNotifications
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationConfiguration = presenter.notificationConfiguration(at: indexPath)
        
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: NotificationConfigurationTableViewCell.self)
        
        cell.setup(notificationConfiguration: notificationConfiguration)
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //MARK: - NotificationConfigurationTableViewCellDelegate {
    func changeActivate(id: Int, active: Bool) {
        presenter.update(id: id, active: active)
    }
    
    //MARK: - Private Functions
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
    }
    
    func setupTableView() {
        guard let tableView = tableView else { return }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cellType: NotificationConfigurationTableViewCell.self)
        tableView.separatorStyle = .none
    }
    
    private func setupView(){
        view.addSubview(ativityIndicatorView)
        
        NSLayoutConstraint.activate([
            ativityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            ativityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ativityIndicatorView.heightAnchor.constraint(equalToConstant: 30),
            ativityIndicatorView.widthAnchor.constraint(equalToConstant: 30)
        ])
        
        let image = iconTitleImageView.image?.imageWithColor(color: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1))
        iconTitleImageView.image = image
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}
