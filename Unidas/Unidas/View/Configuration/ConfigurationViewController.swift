//
//  ConfigurationViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate, ConfigurationViewDelegate {

    lazy var presenter: ConfigurationPresenterDelegate = {
        let presenter = ConfigurationPresenter()
        presenter.delegate = self
        return presenter
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var iconTitleImageView: UIImageView!
    
    override func viewDidLoad() {
        setupView()
        setupTableView()
        configureButtonBack()
        presenter.viewDidLoad()
    }
    
    // MARK: - Private functions
    private func setupView(){
        let image = iconTitleImageView.image?.imageWithColor(color: #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1))
        iconTitleImageView.image = image
    }
    private func setupTableView(){
        guard let tableView = tableView else { return }
        tableView.register(cellType: ConfigurationItemTableViewCell.self)
    }
    
    private func configureButtonBack(){
        let backButtonImage = #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        backButton.tintColor = .white
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.configurationItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ConfigurationItemTableViewCell.self)
        cell.setup(configurationItemDataView: presenter.configurationItem(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let modelItem = presenter.configurationItem(at: indexPath.row)
        
        if let action = modelItem.action {
            presenter.to(segueIdentifier: action)
        } else if let closure = modelItem.model.closure {
            closure()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK: - ConfigurationViewDelegate
    func performSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: nil)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
}
