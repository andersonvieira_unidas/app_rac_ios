//
//  ProtectionsAndEquipmentsViewController.swift
//  Unidas
//
//  Created by Anderson Vieira on 05/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import UIKit

class ProtectionsAndEquipmentsViewController: UIViewController, ProtectionsAndEquipmentsViewDelegate, PresentingSwipeInteractionControllerDraggable, ProtectionTableViewCellDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, MultipleChoiceAlertDelegate {
    
    enum Section: Int {
        case protections, equipments
        
        var title: String {
            switch self {
            case .protections: return NSLocalizedString("Protections", comment: "Protections section title")
            case .equipments: return NSLocalizedString("Equipments", comment: "Equipments section title")
            }
        }
        
        static let allValues: [Section] = [.protections, .equipments]
    }
    
    // MARK: - Interface Builder Outlets
    
    
    @IBOutlet weak var payInAdvancedStack: UIStackView!
    
    @IBOutlet weak var payAtPickupStack: UIStackView!
    @IBOutlet weak var numberOfDaysLabel: UILabel!
    @IBOutlet weak var selectedGroupLabel: UILabel!
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var prePaymentValueLabel: UILabel!
    @IBOutlet weak var pickupDateLabel: UILabel!
    @IBOutlet weak var returnDateLabel: UILabel!
    @IBOutlet weak var resumeView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var prePaymentLabel: UILabel!
    @IBOutlet weak var headerView: EnhancedView!
    @IBOutlet weak var headerTopHeightConstraint: NSLayoutConstraint?
    var tooltipsToRemove: [TooltipView] = []
    
    var numberOfRowsEquipments = 0
    var numberOfRowsProtections = 0
    var selectedProtectionsAndEquipmentsChange = [String]()
    var hasChanged = false
    var headerSelected: [Int: Bool] = [:]
    var voucherCode: String?
    var preAuthorizationAmount: Float?
    
    // MARK: - Protections and equipments view delegate (Properties)
    
    var indexesForSelectedProtection: [Int] = []
    var indexesForSelectedEquipment: [Int] = []
    
    // MARK: - View properties
    
    var presenter: ProtectionsAndEquipmentsPresenterDelegate!
    var quotationDataView: QuotationsDataView?
    var pickUp: GarageReservationDetailsDataView!
    var `return`: GarageReservationDetailsDataView!
    var oldReservation: VehicleReservationsDataView?
    var cardPresentationTransitioningDelegate = CardPresentationTransitioningDelegate()
    var interactivePresentation: PresentSwipeInteractionController?
    
    var draggableView: UIView {
        return resumeView
    }
    
    lazy var selectionFeedbackGenerator = UISelectionFeedbackGenerator()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProtectionsAndEquipmentsPresenter(quotations: quotationDataView!, protectionsService: ProtectionsService(), firebaseAnalyticsService: FirebaseAnalyticsService(), adjustAnalyticsService: AdjustAnalyticsService())
        presenter.delegate = self
        setupTableView()
        setup(for: quotationDataView!)
        setup(for: pickUp, and: `return`)
        setupInteractiveTransition()
        presenter.viewDidLoad()
        presenter.oldReservation = oldReservation
        configureBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if oldReservation != nil && !hasChanged {
            selectedProtectionsAndEquipmentsChange.removeAll()
            self.setupChange()
        }
        for section in Section.allValues{
            updateSelected(section: section.rawValue)
        }
        
        if oldReservation != nil && !hasChanged {
            updateSelected(section: 1, quantity: chargeEquipments().count)
        }
        
        //load toggle
        FirebaseRealtimeDatabase.toggleValues(withKeys: [.payAtPickup, .payInAdvance]) { (toggles) in
            
            if let payInAdvance = toggles[.payInAdvance], let payAtPickup = toggles[.payAtPickup] {
                let configToggle =  ConfigToggle.init(payInAdvance: payInAdvance, payAtPickup: payAtPickup)
                ToggleProfile.model = ToggleMenu(configToggle: configToggle)
            }
        }
        
    }
    
    func setupTableView() {
        numberOfRowsProtections = presenter.numberOfProtections
        tableView.allowsMultipleSelection = true
        tableView.register(cellType: ProtectionTableViewCell.self)
        tableView.register(headerFooterViewType: UNTableViewHeaderFooterView.self)
        
        headerSelected[Section.protections.rawValue] = true
        headerSelected[Section.equipments.rawValue] = false
    }
    
    func setupNavigation() {
        if let stepView = navigationController?.navigationBar.subviews.first(where: { $0 is StepView }) as? StepView {
            headerTopHeightConstraint?.constant = 40
            //headerTopHeightConstraint?.constant = stepView.frame.height
            stepView.setupStep(total: 4, selected: 2)
        }
    }
    
    func setup(for quotations: QuotationsDataView) {
        selectedGroupLabel.text = quotations.formattedGroupWithDescription
    }
    
    func setup(for pickup: GarageReservationDetailsDataView, and return: GarageReservationDetailsDataView) {
        pickupDateLabel.text = pickup.dateOnly
        returnDateLabel.text = `return`.dateOnly
    }
    
    func setupInteractiveTransition() {
        interactivePresentation = PresentSwipeInteractionController(viewController: self)
        cardPresentationTransitioningDelegate.presentationInteractionController = interactivePresentation
    }
    
    func setupChange(){
        loadProtectionsAndEquipmentsSelected()
        chargeProtections()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allValues.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.section(with: section) {
        case .protections:
            return numberOfRowsProtections
        case .equipments:
            return numberOfRowsEquipments
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ProtectionTableViewCell.self)
        cell.delegate = self
        switch section(with: indexPath) {
        case .protections:
            cell.setup(with: presenter.protection(at: indexPath.row), row: indexPath.row)
            cell.isUserInteractionEnabled = presenter.isProtecationSelectable(at: indexPath)
            break
        case .equipments:
            cell.setup(with: presenter.equipment(at: indexPath.row))
            cell.isUserInteractionEnabled = true
            //cell.showsStepperWhenSelected = presenter.equipment(at: indexPath.row).hasQuantity
            break
        }
        
        return cell
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return presenter.isProtecationSelectable(at: indexPath)
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        
        if section == .protections {
            indexesForSelectedProtection.append(indexPath.row)
            
        } else if section == .equipments {
            indexesForSelectedEquipment.append(indexPath.row)
        }
        presenter.selectionDidChange(at: indexPath)
        updateSelected(section: indexPath.section)
        hasChanged = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        
        if section == .protections {
            if let index = indexesForSelectedProtection.firstIndex(of: indexPath.row){
                indexesForSelectedProtection.remove(at: index)
            }
            
        } else if section == .equipments {
            if let index = indexesForSelectedEquipment.firstIndex(of: indexPath.row){
                indexesForSelectedEquipment.remove(at: index)
            }
        }
        presenter.selectionDidChange(at: indexPath)
        updateSelected(section: indexPath.section)
        hasChanged = true
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(UNTableViewHeaderFooterView.self)
        
        let sectionProtectionEquipments = Section(rawValue: section)
        header?.titleLabel.text = sectionProtectionEquipments?.title.uppercased() ?? ""
        header?.isUserInteractionEnabled = true
        header?.buttonExpandClose.tag = section
        
        if sectionProtectionEquipments == .protections {
            header?.buttonExpandClose.isSelected = true
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerTap))
        header?.addGestureRecognizer(tapRecognizer)
        header?.buttonExpandClose.addTarget(self, action: #selector(touchHeader(_:)), for: .touchUpInside)
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            let background = UIView()
            
            let insets = tableView.separatorInset
            let width = tableView.bounds.width - insets.left - insets.right - 20
            let sepFrame = CGRect(x: insets.left, y: 10, width: width, height: 0.5)
            let sep = CALayer()
            sep.frame = sepFrame
            sep.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
            background.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            background.layer.addSublayer(sep)
            
            return background
        }
        let background = UIView()
        background.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        return background
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let headerView = view as? UNTableViewHeaderFooterView else { return }
        updateSelected(withView: headerView, section: section)
    }
    
    // MARK: - Protections and Equipments view delegate
    
    func fetchPreAuthorizationValue(preAuthorization: Float?) {
        preAuthorizationAmount = preAuthorization
    }
    
    func updateResume(totalValue: String, prePaymentValue: String?) {
        totalValueLabel.text = totalValue
        
        if let isFranchise = pickUp.garage?.isFranchise, isFranchise {
            prePaymentValueLabel.text = ""
            prePaymentLabel.text = ""
        } else {
            prePaymentValueLabel.text = prePaymentValue
        }
        
        if let type = pickUp.garage?.model.type {
            if type == .express {
                payAtPickupStack.isHidden = true
                prePaymentValueLabel.text = totalValue
            }
        }
    }
    
    func continueReservation() {
        performSegue(withIdentifier: "showSignUpProcess", sender: nil)
    }
    
    func startLoading() {
        tableView.allowsSelection = false
    }
    
    func endLoading() {
        tableView.allowsMultipleSelection = true
    }
    
    func present(error: Error) {
        alertError(errorMessage: error.localizedDescription)
    }
    
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func reloadRows(at indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let index = indexesForSelectedProtection.firstIndex(of: indexPath.row){
                indexesForSelectedProtection.remove(at: index)
            }
        }
        tableView.reloadRows(at: indexPaths, with: .fade)
    }
    
    func startLoadingPreAuthorizationValue() {
        startLoading()
        continueButton.loading(true, activityIndicatorViewStyle: .white)
    }
    
    func endLoadingPreAuthorizationValue() {
        endLoading()
        continueButton.loading(false)
    }
    
    func show(preAuthorizationValueEstimate: PreAuthorizationEstimateDataView) {
        
        let title = NSLocalizedString("No protection selected", comment: "")
        let firstButtonLabel = NSLocalizedString("Select a Protection", comment: "")
        let secondButtonLabel = NSLocalizedString("Proceed", comment: "")
        let image = #imageLiteral(resourceName: "icon-cancel-reservation-alert")
        let multipleChoice = MultipleChoiceAlert(title: title, subTitle: preAuthorizationValueEstimate.message ?? "", firstChoiceButtonLabel: firstButtonLabel, secondChoiceButtonLabel: secondButtonLabel, image: image, firstChoiceButtonColor: UIColor.themeImportantButton)
        multipleChoice.delegate = self
        multipleChoice.show(animated: true)
    }
    
    func selectProtection(at indexPath: IndexPath) {
        guard let section = Section.init(rawValue: indexPath.section) else { return }
        if section == .protections {
            indexesForSelectedProtection.append(indexPath.row)
        } else if section == .equipments {
            indexesForSelectedEquipment.append(indexPath.row)
        }
        
        self.tableView.isUserInteractionEnabled =  false
        self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        self.tableView.isUserInteractionEnabled =  true
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSignUpProcess" {
            let destination = segue.destination as? ReservationPaymentStepViewController
            destination?.reservation = presenter.reservation
            destination?.oldReservation = oldReservation
            destination?.voucherCode = voucherCode
            destination?.quotationDataView = quotationDataView
            destination?.preAuthorization = preAuthorizationAmount
        }
        navigationItem.setBackButtonTitle(title: "")
    }
    
    @IBAction func didTapResumeViewArea(_ sender: Any) {
        guard let resume = resumeViewController() else { return }
        
        present(resume, animated: true) {
            self.cardPresentationTransitioningDelegate.dismissalInteractionController = DismissSwipeInteractionController(viewController: resume)
        }
    }
    // MARK: - Interface Builder actions
    
    /*  @IBAction func didTapResumeViewArea(sender: Any) {
     guard let resume = resumeViewController() else { return }
     
     present(resume, animated: true) {
     self.cardPresentationTransitioningDelegate.dismissalInteractionController = DismissSwipeInteractionController(viewController: resume)
     }
     }*/
    
    @IBAction func didTapContinueButton(_ sender: Any) {
        presenter.didTapContinueButton()
    }
    
    @objc func touchHeader(_ sender: UIButton?){
        guard let buttonExpandClose = sender else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    @objc func headerTap(gestureRecognizer: UITapGestureRecognizer) {
        guard let view = gestureRecognizer.view,
            let header = view as? UNTableViewHeaderFooterView, let buttonExpandClose = header.buttonExpandClose else { return }
        headerExpandClose(button: buttonExpandClose)
    }
    
    
    //MARK: - InterfaceBuilder Actions
    @objc func touchCancelItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func viewControllerToPresent() -> UIViewController {
        return resumeViewController()!
    }
    
    func didPresent(viewController: DraggableViewController) {
        cardPresentationTransitioningDelegate.dismissalInteractionController = DismissSwipeInteractionController(viewController: viewController)
    }
    
    // MARK: - Protection cell delegate
    
    func protectionTableViewCell(_ cell: ProtectionTableViewCell, didUpdateStepperValue newValue: Int) {
        guard let indexPath = tableView.indexPath(for: cell), indexPath.section == 1 else { return }
        presenter.didUpdateQuantityForEquipment(at: indexPath, to: newValue)
        cell.detailTextLabel?.text = presenter.equipment(at: indexPath.row).valueQuantityDescription
        selectionFeedbackGenerator.prepare()
        selectionFeedbackGenerator.selectionChanged()
    }
    
    func showTooltip(_ row: Int, button: UIButton) {
        if !tooltipsToRemove.isEmpty  {
            removeTooltips()
            return
        }
        let protection = presenter.protection(at: row)
        
        let toolTip = TooltipView.loadFromNib()
        toolTip.alpha = 0
        
        guard let description = protection.description, !description.isEmpty else { return }
        
        toolTip.setup(description: description)
        
        removeTooltips()
        view.addSubview(toolTip)
        
        toolTip.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 0).isActive = true
        toolTip.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        toolTip.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 15).isActive = true
        toolTip.widthAnchor.constraint(equalTo: tableView.widthAnchor, constant: -30).isActive = true
        tooltipsToRemove.append(toolTip)
        
        UIView.animate(withDuration: 0.5) {
            toolTip.alpha = 1.0
        }
    }
    
    
    // MARK: - UIScrollViewDelegate
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        removeTooltips()
    }
    
    //MARK: - MultipleChoiceAlertDelegate
    func choiceFirstTap(tag: Int) {
        
    }
    
    func choiceSecondTap(tag: Int) {
        presenter.continueWithoutProtection()
    }
    
    // MARK: - Private functions
    private func resumeViewController() -> ResumeTableViewController? {
        guard let resume = storyboard?.instantiateViewController(withIdentifier: "ResumeView") as? ResumeTableViewController else { return nil }
        resume.quotationResume = presenter.quotationResume
        resume.modalPresentationStyle = .custom
        resume.transitioningDelegate = cardPresentationTransitioningDelegate
        resume.modalPresentationCapturesStatusBarAppearance = true
        
        if let voucher = voucherCode, !voucher.isEmpty {
            resume.isVoucherApplied = true
        }
        return resume
    }
    
    private func section(with indexPath: IndexPath) -> Section {
        return section(with: indexPath.section)
    }
    
    private func section(with number: Int) -> Section {
        guard let section = Section(rawValue: number) else {
            fatalError("invalid section number: (\(number))")
        }
        return section
    }
    
    private func recoverIndexPath(sectionIndex: Int) -> [IndexPath]{
        
        var indexPaths = [IndexPath]()
        let section = Section.init(rawValue: sectionIndex)
        
        if section == .equipments{
            for row in presenter.getEquipments.indices {
                let indexPath = IndexPath(row: row, section: sectionIndex)
                indexPaths.append(indexPath)
            }
        }
        
        if section == .protections{
            for row in presenter.getProtections.indices {
                let indexPath = IndexPath(row: row, section: sectionIndex)
                indexPaths.append(indexPath)
            }
        }
        return indexPaths
    }
    
    private func updateSelected(section: Int, quantity: Int? = nil){
        if let headerView = tableView.headerView(forSection: section) as? UNTableViewHeaderFooterView {
            updateSelected(withView: headerView, section: section, quantity: quantity)
        }
    }
    
    private func updateSelected(withView headerView: UNTableViewHeaderFooterView, section: Int, quantity: Int? = nil){
        let sectionProtectionsAndEquipments = Section.init(rawValue: section)
        
        if sectionProtectionsAndEquipments == .protections {
            let quantityValue = quantity != nil ? quantity : indexesForSelectedProtection.count
            let femaleIndicator = NSLocalizedString("Protection And Equipments Selections Female Indicator", comment: "Protection And Equipments Selections Female Indicator String")
            setTextSelect(in: headerView, withQuantity: quantityValue ?? 0, indicator: femaleIndicator)
        } else if sectionProtectionsAndEquipments == .equipments {
            let quantityValue = quantity != nil ? quantity : indexesForSelectedEquipment.count
            let maleIndicator = NSLocalizedString("Protection And Equipments Selections Male Indicator", comment: "Protection And Equipments Selections String")
            setTextSelect(in: headerView, withQuantity: quantityValue ?? 0, indicator: maleIndicator)
        }
    }
    
    private func setTextSelect(in headerView: UNTableViewHeaderFooterView, withQuantity quantity: Int, indicator: String){
        var headerString = ""
        
        if quantity > 1 {
            headerString = NSLocalizedString("Protection And Equipments Selections Plural", comment: "Protection And Equipments Selections String")
        } else {
            headerString = NSLocalizedString("Protection And Equipments Selections", comment: "Protection And Equipments Selections String")
        }
        let quantityFormatter = String(format: headerString, String(quantity), indicator)
        
        headerView.selectedLabel.text = quantityFormatter
        headerView.selectedLabel.font = UIFont.regular(ofSize: 14.0)
        headerView.titleLabel.font = UIFont.regular(ofSize: 14.0)
        
        if quantity != 0 {
            headerView.selectedLabel.font = UIFont.regular(ofSize: 14.0)
            headerView.titleLabel.font = UIFont.bold(ofSize: 14.0)
        } else {
            headerView.selectedLabel.text = ""
            headerView.titleLabel.font = UIFont.regular(ofSize: 14.0)
        }
    }
    
    private func headerExpandClose(button: UIButton){
        let sectionIdx = button.tag
        let section = Section.allValues[sectionIdx]
        
        guard let selected = headerSelected[section.rawValue] else { return }
        
        if !selected {
            button.isSelected = true
            if section == .equipments {
                numberOfRowsEquipments = presenter.numberOfEquipments
            } else if section == .protections {
                numberOfRowsProtections = presenter.numberOfProtections
                if oldReservation == nil && indexesForSelectedProtection.isEmpty {
                    self.presenter.fetchProtectionRules()
                }
            }
            
            let indexPaths = recoverIndexPath(sectionIndex: section.rawValue)
            tableView.insertRows(at: indexPaths, with: .fade)
            
            if section == .protections {
                for idx in indexesForSelectedProtection {
                    let indexPath = IndexPath(row: idx, section: 0)
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                    presenter.selectionDidChange(at: indexPath)
                }
            }
            
            if section == .equipments {
                for idx in indexesForSelectedEquipment {
                    let indexPath = IndexPath(row: idx, section: 1)
                    tableView.selectRow(at: indexPath, animated: false, scrollPosition:
                        .none)
                }
            }
            headerSelected[section.rawValue] = true
            
        } else {
            if section == .equipments {
                numberOfRowsEquipments = 0
            } else if section == .protections {
                numberOfRowsProtections = 0
            }
            
            
            let indexPaths = recoverIndexPath(sectionIndex: section.rawValue)
            tableView.deleteRows(at: indexPaths, with: .fade)
            button.isSelected = false
            headerSelected[section.rawValue] = false
            
        }
    }
    
    private func loadProtectionsAndEquipmentsSelected(){
        guard let purchases = oldReservation?.model.servico else { return }
        
        for purchase in purchases {
            if let _ = purchase.codigo {
                selectedProtectionsAndEquipmentsChange.append(purchase.nome)
            }
        }
    }
    
    private func chargeProtections(){
        for item in selectedProtectionsAndEquipmentsChange {
            var i = 0
            for protection in presenter.getProtections{
                if let description = protection.details?.first {
                    if item.uppercased() == description.text {
                        let indexPath = IndexPath(item: i, section: 0)
                        self.selectProtection(at: indexPath)
                        self.presenter.selectionDidChange(at: indexPath)
                        break
                    }
                }
                i += 1
            }
        }
    }
    
    private func chargeEquipments() -> [Int] {
        
        var selectedEquipments : [Int] = []
        
        for item in selectedProtectionsAndEquipmentsChange {
            var i = 0
            for equipments in presenter.getEquipments{
                if item.uppercased() == equipments.description {
                    selectedEquipments.append(i)
                    let indexPath = IndexPath(item: i, section: 1)
                    self.selectProtection(at: indexPath)
                    self.presenter.selectionDidChange(at: indexPath)
                }
                i += 1
            }
        }
        return selectedEquipments
    }
    
    private func removeTooltips(){
        tooltipsToRemove.forEach { (tooltip) in
            remove(tooltip: tooltip)
        }
        tooltipsToRemove = []
    }
    
    private func remove(tooltip: TooltipView){
        UIView.animate(withDuration: 0.5,
                       animations: { () -> Void in
                        tooltip.alpha = 0
        }, completion: { (finished) -> Void in
            tooltip.removeFromSuperview()
        })
    }
    
    //MARK: - Private Functions
    private func configureBackButton(){
        let backButtonImage =  #imageLiteral(resourceName: "icon-back-button")
        let backButton = UIBarButtonItem(image: backButtonImage, style: .plain, target: self, action:  #selector(touchCancelItem))
        navigationItem.leftBarButtonItem = backButton
    }
}
