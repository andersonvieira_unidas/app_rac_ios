//
//  VehicleCharacteristicsViewController.swift
//  Unidas
//
//  Created by Felipe Machado on 29/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

class VehicleCharacteristicsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, VehicleCharacteristicsViewDelegate {
        
    @IBOutlet weak var vehicleCharacteristicsTableView: UITableView!
    
    var vehicleCharacteristicDataView: [VehicleCharacteristicDataView]?
    var vehicleCategory: String = ""
    var presenter: VehicleCharacteristicsPresenterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = VehicleCharacteristicsPresenter()
        presenter.delegate = self
        setupTableView()
        presenter.viewDidLoad(vehicleCategory: vehicleCategory)
    }
    
    func setupTableView() {
        vehicleCharacteristicsTableView.delegate = self
        vehicleCharacteristicsTableView.dataSource = self
        vehicleCharacteristicsTableView.register(cellType: VehicleCharacteristicTableViewCell.self)
    }
    
    func reloadData() {
        vehicleCharacteristicsTableView.reloadData()
    }
    
    @IBAction func dismissViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Delegates
    
    func showEmptyView() {
        let emptyDataView = EmptyDataView.loadFromNib()
        let title = NSLocalizedString("Empty Characteristics Title", comment: "Empty Characteristics Title Comment")
        let message = NSLocalizedString("Empty Characteristics Message", comment: "Empty Characteristics Message Comment")
        emptyDataView.setup(title: title, message: message, image: nil, buttonTitle: nil, buttonIsHidden: true)
        
        vehicleCharacteristicsTableView.backgroundView = emptyDataView
    }
    
    func hideEmptyView() {
        
    }
    
    //MARK: - TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfCharacteritics
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: VehicleCharacteristicTableViewCell.self)
        cell.setup(with: presenter.characterict(at: indexPath.row))
        let additionalSeparator = DottedLineView(frame: CGRect(x: 20, y: cell.frame.size.height, width: cell.frame.size.width - 40, height: 1))
        additionalSeparator.lineWidth = 1
        additionalSeparator.round = true
        additionalSeparator.horizontal = true
        additionalSeparator.lineColor = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
        cell.addSubview(additionalSeparator)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return NSLocalizedString("Characteristics", comment: "Vehicle Characteristics section title")
//    }
}
