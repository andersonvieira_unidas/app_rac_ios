//
//  PostTerms.swift
//  Unidas
//
//  Created by Felipe Machado on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct PostTerms: Codable {
    let statusCode: Int?
    let message: String?
    let termoPrivacidade: TermoPrivacidade?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "HttpStatusCode"
        case message = "Mensagem"
        case termoPrivacidade = "Objeto"
    }
}

struct TermoPrivacidade: Codable {
    let termoPoliticaPrivacidade: Bool?
    
    enum CodingKeys: String, CodingKey {
        case termoPoliticaPrivacidade = "termoPoliticaPrivacidade"
    }
}



