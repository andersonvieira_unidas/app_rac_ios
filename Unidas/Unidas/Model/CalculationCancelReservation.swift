//
//  CalculationCancelReservation.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/07/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct CalculationCancelReservation : Codable {
    let antecipatedDays: Int
    let porcentage: Double
    let returnValue: Double
    let paidValue: Double
    
    enum CodingKeys: String, CodingKey {
        case antecipatedDays = "diasAntecipacao"
        case porcentage = "porcentagem"
        case returnValue = "valorRembolsado"
        case paidValue = "valorPago"
    }
}
