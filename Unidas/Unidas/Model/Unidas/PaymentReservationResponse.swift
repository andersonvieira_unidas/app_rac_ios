//
//  PaymentReservationResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 19/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct PaymentReservationResponse: Codable {
    
    let code: Int?
    let message: String?
    let object: NewReservationObjectResponse?

    enum CodingKeys: String, CodingKey {
        case code = "HttpStatusCode"
        case message = "Mensagem"
        case object = "Objeto"
    }
}


