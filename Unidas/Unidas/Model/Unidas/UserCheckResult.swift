//
//  UserCheckResult.swift
//  Unidas
//
//  Created by Anderson on 03/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct UserCheckResult: Codable {
    let statusCode: Int?
    let message: String?
    let userCheck: UserCheck?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "HttpStatusCode"
        case message = "Mensagem"
        case userCheck = "Objeto"
    }
}
