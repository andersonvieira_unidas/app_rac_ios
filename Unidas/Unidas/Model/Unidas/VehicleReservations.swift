//
//  VehicleReservations.swift
//  Unidas
//
//  Created by Mateus Padovani on 03/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleReservations : Codable {
    let numeroReserva : Int
    let canalDeVenda : Int
    let tarifa : Int
    let valorTotal : Float
    let dataHoraRetirada: Date
    let dataHoraDevolucao: Date
    let grupo: String
    let grupoDescricao : String
    let status : String?
    let statusDescricao : String?
    let lojaRetirada : String
    let cidadeRetirada : String?
    let ufRetirada : String?
    let lojaDevolucao : String
    let cidadeDevolucao : String?
    let ufDevolucao : String?
    let cupomPromocional :String?
    let tag : String?
    let grupoVeiculos: String?
    let lojaRetiradaIATA: String?
    let lojaDevolucaoIATA: String?
    let lojaRetiradaObs: String?
    let franquia: Bool?
    let lojaDevolucaoObs: String?
    let lojaRetiradaGeoLoc: String?
    let lojaDevolucaoGeoLoc: String?
    let servico: [Purchase]?
    let quantidadeParcela: Int?
    let numeroCartao: String?
    let numeroCartaoPreAutorizacao: String?
    let bandeiraCartao: String?
    let bandeiraCartaoPreAutorizacao: String?
    let motorizacao: String?
    let lojaRetiradaNome: String?
    let lojaDevolucaoNome: String?
    let canUpdateReserve: Int
    let dateCheckIn: Date?
    let paymentStatus: PaymentStatus?
    let preAuthorizationStatus: PaymentStatus?
    let reservaPessoaJuridica: Bool?
    let empresaCNPJ: String?
    let empresaNome: String?
    let voucher: VoucherLegalEntity?
    let obrigatorioPre: Bool?
    let obrigatorioPgto: Bool?
    let checkIn: Bool?
    let hoursToCheckin: Int?
    let checkInAvailable: Bool?
    let pictureURL: URL?
    let numeroContrato: Int?
    let valorPago: Float
    let valorPreAutorizacao: Float?
    let dataVencimento: Date?
    let horaVencimento: Int?
    let lojaRetiradaCodigoTipo: GarageType?
    let qrCode: String?
    
    enum CodingKeys: String, CodingKey {
        case numeroReserva = "NumeroReserva"
        case canalDeVenda = "CanalDeVenda"
        case tarifa = "Tarifa"
        case valorTotal = "valorTotal"
        case dataHoraRetirada = "DataHoraRetirada"
        case dataHoraDevolucao = "DataHoraDevolucao"
        case grupo = "Grupo"
        case grupoDescricao = "GrupoDescricao"
        case status = "Status"
        case statusDescricao = "StatusDescricao"
        case lojaRetirada = "LojaRetirada"
        case cidadeRetirada = "CidadeRetirada"
        case ufRetirada = "UfRetirada"
        case lojaDevolucao = "LojaDevolucao"
        case cidadeDevolucao = "CidadeDevolucao"
        case ufDevolucao = "UfDevolucao"
        case cupomPromocional = "CupomPromocional"
        case tag = "Tag"
        case grupoVeiculos = "GrupoVeiculos"
        case lojaRetiradaIATA = "LojaRetiradaIATA"
        case lojaDevolucaoIATA = "LojaDevolucaoIATA"
        case lojaRetiradaObs = "LojaRetiradaObs"
        case franquia = "LojaRetiradaFranquia"
        case lojaDevolucaoObs = "LojaDevolucaoObs"
        case lojaRetiradaGeoLoc = "LojaRetiradaGeoLoc"
        case lojaDevolucaoGeoLoc = "LojaDevolucaoGeoLoc"
        case servico = "Servico"
        case quantidadeParcela = "QuantidadeParcela"
        case numeroCartao = "NumeroCartao"
        case numeroCartaoPreAutorizacao = "PreCartao"
        case bandeiraCartao = "BandeiraCartao"
        case bandeiraCartaoPreAutorizacao = "PreBandeira"
        case motorizacao = "Motorizacao"
        case lojaRetiradaNome = "LojaRetiradaNome"
        case lojaDevolucaoNome = "LojaDevolucaoNome"
        case canUpdateReserve = "PodeAlterarReserva"
        case dateCheckIn = "DataHoraCheckIn"
        case paymentStatus = "StatusPagamento"
        case preAuthorizationStatus = "StatusPreAutorizacao"
        case reservaPessoaJuridica = "ReservaPessoaJuridica"
        case empresaCNPJ = "EmpresaCNPJ"
        case empresaNome = "EmpresaNome"
        case voucher = "Voucher"
        case obrigatorioPre = "ObrigatorioPre"
        case obrigatorioPgto = "ObrigatorioPgto"
        case checkIn = "CheckIn"
        case hoursToCheckin = "HorasAntesDoCheckin"
        case checkInAvailable = "CheckinDisponivel"
        case pictureURL = "GrupoImagemURL"
        case numeroContrato = "NumeroContrato"
        case valorPago = "ValorPago"
        case valorPreAutorizacao = "PreValor"
        case dataVencimento = "dataOperacao"
        case horaVencimento = "MinutosCancelamentoReserva"
        case lojaRetiradaCodigoTipo = "CodigotipoLoja"
        case qrCode = "QRCode"
    }
}


