//
//  LoyaltyProgram.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct LoyaltyProgram: Codable {
    let categoryCode: Int
    let categoryDescription: String?
    let accumulatedPoints: Int
    let availablePoints: Int
    let nextCategoryCode: Int
    let nextCategoryDescription: String?
    let nextCategoryPoints: Int
    let minimumBalanceNextCategory: Int
    
    enum CodingKeys: String, CodingKey {
        case categoryCode = "codigoCategoriaAtual"
        case categoryDescription = "descricaoCategoriaAtual"
        case accumulatedPoints = "PontosAcumulados"
        case availablePoints = "PontosDisponiveis"
        case nextCategoryCode = "codigoProximaCategoria"
        case nextCategoryDescription = "descricaoProximaCategoria"
        case nextCategoryPoints = "quantidadePontosProximaCategoria"
        case minimumBalanceNextCategory = "saldoMinimoProximaCategoria"
    }
}
