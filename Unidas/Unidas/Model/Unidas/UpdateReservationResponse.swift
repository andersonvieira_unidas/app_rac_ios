//
//  UpdateReservationResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 19/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

struct UpdateReservationResponse: Codable {
    
    let code: Int?
    let message: String?
    let object: UpdateReservationObjectResponse?
    
    enum CodingKeys: String, CodingKey {
        case code = "HttpStatusCode"
        case message = "Mensagem"
        case object = "Objeto"
    }
}
