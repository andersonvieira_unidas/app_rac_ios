//
//  NewReservationObjectResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 19/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct NewReservationObjectResponse: Codable {
    let reserveCode: String?
    let payment: NewReservationObjectPaymentResponse?
    
    enum CodingKeys: String, CodingKey {
        case reserveCode = "Reserva"
        case payment = "Pagamento"
    }
}
