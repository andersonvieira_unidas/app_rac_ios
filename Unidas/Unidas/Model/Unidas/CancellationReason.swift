//
//  CancellationReason.swift
//  Unidas
//
//  Created by Mateus Padovani on 12/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CancellationReason: Codable {
    let id: Int
    let motivoUsuario: String
    let motivoInterno: String

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case motivoUsuario = "MotivoUsuario"
        case motivoInterno = "MotivoInterno"
    }
}
