//
//  QuotationLocationAddress.swift
//  Unidas
//
//  Created by Anderson on 12/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct QuotationLocationAddress: Codable {
    let cityName: String
    let state: QuotationAddressState?
    
    enum CodingKeys: String, CodingKey {
        case cityName = "CityName"
        case state = "StateProv"
    }
}

