//
//  NewReservationObjectPaymentResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 19/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

struct NewReservationObjectPaymentResponse: Codable {
    let status: ReservationObjectPaymentStatus?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Mensagem"
    }
}
