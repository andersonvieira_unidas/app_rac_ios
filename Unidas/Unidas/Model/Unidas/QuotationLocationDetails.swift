//
//  QuotationLocationDetails.swift
//  Unidas
//
//  Created by Anderson on 12/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct QuotationLocationDetails: Codable {
    let name: String?
    let airport: String?
    let address: QuotationLocationAddress?
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case airport = "AtAirport"
        case address = "Address"
    }
}
