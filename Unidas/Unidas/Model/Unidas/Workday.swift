//
//  Workday.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum Workday: Int, Codable {
    case weekday, saturday, sunday, holiday
}
