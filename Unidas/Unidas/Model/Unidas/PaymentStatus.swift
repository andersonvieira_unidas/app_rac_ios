//
//  PaymentStatus.swift
//  Unidas
//
//  Created by Anderson Vieira on 25/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

enum PaymentStatus: String {
    case paymentConfirmed = "Pagamento Confirmado"
    case cancellationConfirmed = "Cancelamento Confirmado"
    case notAuthorized = "Não Autorizado"
    case waitingConfirmation = "Aguardando Confirmação"
    case notPay = "Não Pago"
    
    var colorText: UIColor {
        switch self {
        case .notAuthorized, .cancellationConfirmed:
            return #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
        case .waitingConfirmation:
             return #colorLiteral(red: 0.2274329066, green: 0.5870787501, blue: 0.9447389245, alpha: 1)
        case .notPay:
            return #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
        case .paymentConfirmed:
            return #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
        }
    }
    
   var text: String {
        switch self {
        case .notAuthorized, .cancellationConfirmed:
            return NSLocalizedString("Payment Status - Denied", comment: "")
        case .waitingConfirmation:
            return NSLocalizedString("Payment Status - Processing", comment: "")
        case .notPay:
            return NSLocalizedString("Payment Status - No Payment", comment: "")
        case .paymentConfirmed:
            return NSLocalizedString("Payment Status - Success", comment: "")
        }
    }
}

extension PaymentStatus: Codable {
    public init(from decoder: Decoder) throws {
        self = try PaymentStatus(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notPay
    }
}

