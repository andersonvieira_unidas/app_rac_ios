//
//  NewReservationResponse.swift
//  Unidas
//
//  Created by Anderson Vieira on 17/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//
import Foundation

struct NewReservationResponse: Codable {
    
    let code: Int?
    let message: String?
    let object: NewReservationObjectResponse?
    
    enum CodingKeys: String, CodingKey {
        case code = "HttpStatusCode"
        case message = "Mensagem"
        case object = "Objeto"
    }
}

