//
//  ContractStatus.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum ContractStatus: String, Codable {
    case open = "Aberto"
    case closed = "Fechado"
    case replacement = "Substituição"
    case cancelled = "Cancelado"
    case registered = "Cadastrado"
}
