//
//  UserUnidas.swift
//  Unidas
//
//  Created by Mateus Padovani on 20/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct UserUnidas: Codable {
    var contaClienteId: Int?
    var flgAreaLogada: Bool?
    var contaStatusId: UserStatus?
    var contaStatusDescricao: String?
    var ContaClienteTipoDescricao: String?
    var clidoc: String?
    var contaClienteNome: String?
    var contaClienteNomeMae: String?
    var contaClienteNascimento: String?
    var contaClienteTelResDDD: String?
    var contaClienteTelRes: String?
    var contaClienteAutenticacaoChave: String?
    var contaClienteLoginEmail: String?
    var contaClienteLoginEmailAtivo: Int?
    var contaClienteLoginEmailMascarado: String?
    var contaClienteLoginEmailValidacao: String?
    var contaClienteLoginCelularDDD: String?
    var contaClienteLoginCelular: String?
    var contaClienteLoginCelularValidacao: String?
    var contaClienteLoginCelularMascarado: String?
    var contaClienteLoginCelularDeviceID: String?
    var contaClienteEnderecoResidencialPais: String?
    var contaClienteEnderecoResidencialCEP: String?
    var contaClienteEnderecoResidencialLogradouro: String?
    var contaClienteEnderecoResidencialLogradouroNumero: String?
    var contaClienteEnderecoResidencialLogradouroComplemento: String?
    var contaClienteEnderecoResidencialBairro: String?
    var contaClienteEnderecoResidencialCidade: String?
    var contaClienteEnderecoResidencialUF: String?
    var contaClienteToken: String?
    var contaClienteAlerta: String?
    var emailConfirmado: Bool
    var contaPendencias: Bool?
    
    enum CodingKeys: String, CodingKey {
        case contaClienteId = "ContaClienteid"
        case flgAreaLogada = "FlgAreaLogada"
        case contaStatusId = "ContaStatusid"
        case contaStatusDescricao = "ContaStatusDescricao"
        case ContaClienteTipoDescricao = "ContaClienteTipoDescricao"
        case clidoc = "CLIDOC"
        case contaClienteNome = "ContaClienteNome"
        case contaClienteNomeMae = "ContaClienteNomeMae"
        case contaClienteNascimento = "ContaClienteNascimento"
        case contaClienteTelResDDD = "ContaClienteTelResDDD"
        case contaClienteTelRes = "ContaClienteTelRes"
        case contaClienteAutenticacaoChave = "ContaClienteAutenticacaoChave"
        case contaClienteLoginEmail = "ContaClienteLoginEmail"
        case contaClienteLoginEmailAtivo = "ContaClienteLoginEmailAtivo"
        case contaClienteLoginEmailMascarado = "ContaClienteLoginEmailMascarado"
        case contaClienteLoginEmailValidacao = "ContaClienteLoginEmailValidacao"
        case contaClienteLoginCelularDDD = "ContaClienteLoginCelularDDD"
        case contaClienteLoginCelular = "ContaClienteLoginCelular"
        case contaClienteLoginCelularValidacao = "ContaClienteLoginCelularValidacao"
        case contaClienteLoginCelularMascarado = "ContaClienteLoginCelularMascarado"
        case contaClienteLoginCelularDeviceID = "ContaClienteLoginCelularDeviceID"
        case contaClienteEnderecoResidencialPais = "ContaClienteEnderecoResidencialPais"
        case contaClienteEnderecoResidencialCEP = "ContaClienteEnderecoResidencialCEP"
        case contaClienteEnderecoResidencialLogradouro = "ContaClienteEnderecoResidencialLogradouro"
        case contaClienteEnderecoResidencialLogradouroNumero = "ContaClienteEnderecoResidencialLogradouroNumero"
        case contaClienteEnderecoResidencialLogradouroComplemento = "ContaClienteEnderecoResidencialLogradouroComplemento"
        case contaClienteEnderecoResidencialBairro = "ContaClienteEnderecoResidencialBairro"
        case contaClienteEnderecoResidencialCidade = "ContaClienteEnderecoResidencialCidade"
        case contaClienteEnderecoResidencialUF = "ContaClienteEnderecoResidencialUF"
        case contaClienteToken = "ContaClienteToken"
        case contaClienteAlerta = "ContaclienteAlerta"
        case emailConfirmado = "EmailConfirmado"
        case contaPendencias = "Pendencias"
    }
}

