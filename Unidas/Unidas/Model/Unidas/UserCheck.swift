//
//  UserCheck.swift
//  Unidas
//
//  Created by Anderson on 03/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct UserCheck: Codable {

    let mobilePhoneIsValid: Bool?
    let emailIsValid: Bool?
    let serasaIsValid: Bool?
    let credDefenseIsValid: Bool?
    let documentLicenceIsValid: Bool?
    let signatureIsValid: Bool?
    let paymentIsValid: Bool?
    let accountIsValid: Bool?
    let selfieIsValid: Bool?
    let financialPendencie: Bool?
    
    enum CodingKeys: String, CodingKey {
        case mobilePhoneIsValid = "Celular"
        case emailIsValid = "Email"
        case serasaIsValid = "Serasa"
        case credDefenseIsValid = "CredDefense"
        case documentLicenceIsValid = "CNH"
        case signatureIsValid = "Assinatura"
        case accountIsValid = "Conta"
        case paymentIsValid = "Cartao"
        case selfieIsValid = "FotoPerfil"
        case financialPendencie = "PendenciaFinanceira"
    }
}
