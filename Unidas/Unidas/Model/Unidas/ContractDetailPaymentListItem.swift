//
//  ContractDetailPaymentListItem.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ContractDetailPaymentListItem: Codable {
    
    let type: ContractDetailPaymentType?
    let date: String?
    let hour: String?
    let card: String?
    let value: Float?
    let installments: Int?
    let cardBrand: String?
    let paymentStatus: String?
    let prePaymentStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case type = "Tipo"
        case date = "DataMovimento"
        case hour = "HoraMovimento"
        case card = "Cartao"
        case value = "Valor"
        case installments = "Parcelas"
        case cardBrand = "Bandeira"
        case paymentStatus = "StatusPagamento"
        case prePaymentStatus = "StatusPrePagamento"
    }
    
}
