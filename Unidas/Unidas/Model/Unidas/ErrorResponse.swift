//
//  ErrorResponse.swift
//  Unidas
//
//  Created by Anderson on 24/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct ErrorResponse: Codable {
    
    let mensagem : String
    let statusCode: Int
    
    enum CodingKeys: String, CodingKey {
        case mensagem = "Mensagem"
        case statusCode = "HttpStatusCode"
    }
}
