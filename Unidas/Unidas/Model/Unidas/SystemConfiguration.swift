//
//  SystemConfiguration.swift
//  Unidas
//
//  Created by Anderson on 13/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct SystemConfiguration: Codable {
    
    let id: String?
    let value: String?
    let description: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "CfgID"
        case value = "CfgValor"
        case description = "CfgDescric"
    }
}
