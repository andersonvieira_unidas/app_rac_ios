//
//  PreAuthorizationEstimate.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 17/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct PreAuthorizationEstimate: Codable {
    
    let message: String?
    let value: Float
    
    enum CodingKeys: String, CodingKey {
        case value = "ValorPreAutorizacao"
        case message = "Mensagem"
    }
    
}
