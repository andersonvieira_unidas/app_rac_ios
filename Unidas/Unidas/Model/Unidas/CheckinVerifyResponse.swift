//
//  CheckinVerifyResponse.swift
//  Unidas
//
//  Created by Jean Paull on 22/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation


struct CheckinVerifyResponse: Codable {
    
    let httpStatusCode : Int
    let Mensagem : String
    let Objeto : Bool
    
    enum CodingKeys: String, CodingKey {
         case httpStatusCode = "HttpStatusCode"
         case Mensagem = "Mensagem"
         case Objeto = "Objeto"
    }
}
