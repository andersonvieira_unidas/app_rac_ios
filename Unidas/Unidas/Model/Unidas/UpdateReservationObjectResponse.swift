//
//  UpdateReservationObjectResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 22/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct UpdateReservationObjectResponse: Codable {
    let reserveCode: String?
    let payment: UpdateReservationObjectPaymentResponse?
    
    enum CodingKeys: String, CodingKey {
        case reserveCode = "Reserva"
        case payment = "Pagamento"
    }

}
