//
//  VoucherLegalEntity.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

enum VoucherLegalEntity: String {
    case open = "ABERTO", closed = "FECHADO", presentation = "APRESENTACAO", unknown
    
    var voucherType: String {
        switch self {
        case .open:
            return "O"
        case .closed:
            return "F"
        case .presentation:
            return "A"
        case .unknown:
            return ""
        }
    }
}

extension VoucherLegalEntity: Codable {
    public init(from decoder: Decoder) throws {
        self = try VoucherLegalEntity(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}
