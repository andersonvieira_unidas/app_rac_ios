//
//  checkinVerify.swift
//  Unidas
//
//  Created by Jean Paull on 22/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct checkinVerify: Codable {
    
    let HttpStatusCode : Int
    let Mensagem : String
    let Objeto : Bool
    
    enum CodingKeys: String,CodingKey {
        case HttStatusCode = "HttpStatusCode"
        case Mensagem = "Mensagem"
        case Objeto = "Objeto"
        
    }
    
}
