//
//  UpdateReservationObjectPaymentResponse.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 22/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct UpdateReservationObjectPaymentResponse: Codable {
    let status: ReservationObjectPaymentStatus?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Mensagem"
    }
}
