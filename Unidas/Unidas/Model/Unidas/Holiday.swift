//
//  Holiday.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Holiday: Codable {
    let date: Date
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case date = "Data"
        case description = "Descricao"
    }
}
