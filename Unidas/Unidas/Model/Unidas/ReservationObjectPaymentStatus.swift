//
//  ReservationObjectPaymentStatus.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 22/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

enum ReservationObjectPaymentStatus: String, Codable {
    case noPayment = "SemPagamento"
    case success = "Sucesso"
    case denied = "Negado"
    case processing = "Processando"
    
    var colorText: UIColor {
        switch self {
        case .denied:
            return #colorLiteral(red: 0.9215686275, green: 0.3411764706, blue: 0.3411764706, alpha: 1)
        case .processing:
             return #colorLiteral(red: 0.2274329066, green: 0.5870787501, blue: 0.9447389245, alpha: 1)
        case .noPayment:
            return #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
        case .success:
            return #colorLiteral(red: 0.1529411765, green: 0.6823529412, blue: 0.3764705882, alpha: 1)
        }
    }
    
    var text: String {
        switch self {
        case .denied:
            return NSLocalizedString("Payment Status - Denied", comment: "")
        case .processing:
            return NSLocalizedString("Payment Status - Processing", comment: "")
        case .noPayment:
            return NSLocalizedString("Payment Status - No Payment", comment: "")
        case .success:
            return NSLocalizedString("Payment Status - Success", comment: "")
        }
    }
}

