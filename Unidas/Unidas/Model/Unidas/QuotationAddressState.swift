//
//  QuotationAddressState.swift
//  Unidas
//
//  Created by Anderson on 12/05/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct QuotationAddressState: Codable {
    let code: String
    
    enum CodingKeys: String, CodingKey {
        case code = "StateCode"
    }
}
