//
//  ContractDetailPaymentType.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

enum ContractDetailPaymentType: String {
    case prePayment = "Pre-Autorização"
    case payment = "Pagamento"
    case unknown
}

extension ContractDetailPaymentType: Codable {
    public init(from decoder: Decoder) throws {
        self = try ContractDetailPaymentType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}
