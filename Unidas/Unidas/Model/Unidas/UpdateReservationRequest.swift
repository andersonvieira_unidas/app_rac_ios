//
//  UpdateReservationRequest.swift
//  Unidas
//
//  Created by Anderson Simões Vieira on 18/09/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct UpdateReservationRequest: Codable {
    
    let reservationNumber: Int?
    let saleChanel: String?
    let rate: Int?
    let pickUpStore: String?
    let pickUpDate: String?
    let returnStore: String?
    let returnDate: String?
    let groupCode: String?
    let driversName: String?
    let driversEmail: String?
    let driversDocument: String?
    let driversPhone: String?
    let flightNumber: String?
    let flightCompany: String?
    let caster: Int?
    let sendSMS: Bool?
    let paymentOption: PaymentOption?
    let cardIdentifier: Int?
    let cardSecurityCode: String?
    let numberOfInstallments: Int?
    let value: String?
    let equipments: [[String: Int]]?
    let protections: [Int]?
    let voucher: String?
    let acceptanceLocationTerms: Bool?
    let acceptanceRefoundTerms: Bool?
    
    enum CodingKeys: String, CodingKey {
        case reservationNumber = "numeroReserva"
        case saleChanel = "canalDeVenda"
        case rate = "tarifa"
        case pickUpStore = "lojaRetirada"
        case pickUpDate = "dataRetirada"
        case returnStore = "lojaDevolucao"
        case returnDate = "dataDevolucao"
        case groupCode = "grupoSIPP"
        case driversName = "CondutorNome"
        case driversEmail = "CondutorEmail"
        case driversDocument = "CondutorDocumento"
        case driversPhone = "CondutorTelefone"
        case flightNumber = "numeroVoo"
        case flightCompany = "ciaAerea"
        case caster = "rodizio"
        case sendSMS = "enviaSMS"
        case paymentOption = "OpcaoPagamento"
        case cardIdentifier = "CartaoId"
        case cardSecurityCode = "CartaoCodigoSeguranca"
        case numberOfInstallments = "Parcela"
        case value = "Valor"
        case equipments = "equipamentos"
        case protections = "protecoes"
        case voucher = "cupom"
        case acceptanceLocationTerms = "aceitoTermoLocacao"
        case acceptanceRefoundTerms = "aceitoTermoReembolso"
    }
    
}

extension UpdateReservationRequest: CustomDictionaryConvertible {
    
    init(reservation: Reservation, user: UserResponse, paymentInformation: CreditCardReservePayment?, uuid: UUID) {
         self.reservationNumber = reservation.number == nil ? nil : Int(reservation.number!)
        self.saleChanel = "APP"
        self.rate = reservation.rateQualifier == nil ? nil : Int(reservation.rateQualifier!)
        self.pickUpStore = reservation.pickUp?.garage.code
        self.pickUpDate = reservation.pickUp?.dateAPI
        self.returnStore = reservation.return?.garage.code
        self.returnDate = reservation.return?.dateAPI
        self.groupCode = reservation.group?.code
        self.driversName = user.account.name
        self.driversEmail = user.account.email
        self.driversDocument = user.account.documentNumber
        self.driversPhone = user.account.mobilePhone
        self.flightNumber = reservation.airlineInformation?.flightNumber
        self.flightCompany = reservation.airlineInformation?.company.code
        self.caster = reservation.selectedCasterWeekday
        self.sendSMS = reservation.sendSMS
        self.paymentOption = reservation.selectedPaymentOption
        self.cardIdentifier = paymentInformation?.card.tkncodseq
        self.cardSecurityCode = paymentInformation?.securityCode
        self.numberOfInstallments = paymentInformation?.installmentsOption
        self.acceptanceRefoundTerms = reservation.acceptTermRefound
        self.acceptanceLocationTerms = reservation.acceptTermLocation
        self.value = NumberFormatter.apiNumberFormatter.string(from: NSNumber(value: reservation.selectedPaymentOption == .payInAdvance ? reservation.prePaymentValue ?? 0.0 : reservation.totalValue ?? 0.0))
        self.equipments = reservation.equipments?.compactMap({ (equipment) -> [String : Int]? in
            guard let codeStr = equipment.equipType, let code = Int(codeStr) else { return nil }
            return ["codigo" : code, "quantidade": equipment.selectedQuantityItem ?? 1]
        }) ?? []
        self.protections = reservation.protections?.compactMap({ protection -> Int? in
            guard let codeStr = protection.coverageType else { return nil }
            return Int(codeStr)
        }) ?? [100]
        self.voucher = reservation.promotionCode
    }
    
}


