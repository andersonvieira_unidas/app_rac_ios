//
//  ContractDetailListItem.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ContractDetailListItem: Codable {
    
    let description: String?
    let value: Float?
    let observation: String?
    
    enum CodingKeys: String, CodingKey {
        case description = "Descricao"
        case value = "Valor"
        case observation = "Observacao"
    }
    
}
