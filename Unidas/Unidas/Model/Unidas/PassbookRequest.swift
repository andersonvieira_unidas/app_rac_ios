//
//  PassbookRequest.swift
//  Unidas
//
//  Created by Anderson Vieira on 15/08/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

struct PassbookRequest: Codable {
    let reservationCode: String
    let pickupLocation: String
    let returnLocation: String
    let qrCode: String
    let pickupDate: String
    let returnDate: String
    let pickupTime: String
    let returnTime: String
    let name: String
    let group: String
    let groupDescription: String
    let vehiclesGroup: String
}
