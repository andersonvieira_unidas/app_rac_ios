//
//  UserResponse.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct UserResponse: Codable {
    var token: String?
    var account: Account
    var unidas: [UserUnidas]?
}

extension UserResponse {
    
    init?(userUnidas: UserUnidas) {
        guard let account = Account(userUnidas: userUnidas) else { return nil }
        self.token = userUnidas.contaClienteToken
        self.account = account
        self.unidas = [userUnidas]
    }
    
}
