//
//  Garage.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Garage: Codable {
    let id: Int
    let code: String
    let name: String
    let description: String
    let type: GarageType
    let openingHours: OpeningHours
}
