//
//  ContractVehicle.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ContractVehicle: Codable {
    
    let licensePlate: String?
    let model: String?
    let modelType: String?
    let color: String?
    let year: Int?
    let manufacturer: String?
    let currentFuel: Float?
    let currentMileage: Int?
    let business: String?
    let group: String?
    let groupDescription: String?
    let sample: String?
    let gearType: GearShiftType?
    let engine: String?
    let luggage: String?
    let vip: String?
    
    enum CodingKeys: String, CodingKey {
        case licensePlate = "Placa"
        case model = "Modelo"
        case modelType = "TipoModelo"
        case color = "Cor"
        case year = "AnoFabricacao"
        case manufacturer = "Fabricante"
        case currentFuel = "CombustivelAtual"
        case currentMileage = "KmAtual"
        case business = "Negocio"
        case group = "Grupo"
        case groupDescription = "GrupoDescricao"
        case sample = "Exemplo"
        case gearType = "TipoCambio"
        case engine = "Motor"
        case luggage = "Bagagem"
        case vip = "Vip"
    }
    
}
