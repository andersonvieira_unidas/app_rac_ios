//
//  Location+CoreLocation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 06/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import CoreLocation

extension Location {
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    init(coordinate: CLLocationCoordinate2D) {
        latitude = coordinate.latitude
        longitude = coordinate.longitude
    }
    
    init(location: CLLocation) {
        self.init(coordinate: location.coordinate)
    }
    
}
