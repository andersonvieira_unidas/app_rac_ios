//
//  Location.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Location: Codable, Equatable, CustomDictionaryConvertible {
    let latitude: Double
    let longitude: Double
}

extension Location {
    init?(storeCoordinate: String?) {
        guard let storeCoordinate = storeCoordinate, !storeCoordinate.isEmpty else {
            return nil
        }
        
        let coordinate = storeCoordinate.split(separator: ",")
        
        guard coordinate.count > 1 else {
            return nil
        }
        
        guard let latitude = Double(coordinate[0].replacingOccurrences(of: " ", with: "")), let longitude = Double(coordinate[1].replacingOccurrences(of: " ", with: "")) else { return nil }
        
        self.latitude = latitude
        self.longitude = longitude
    }
}
