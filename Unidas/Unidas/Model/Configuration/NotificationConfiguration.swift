//
//  NotificationConfiguration.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/08/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct NotificationConfiguration: Codable {
    let code: Int
    let description: String
    var active: Bool
    
    enum CodingKeys: String, CodingKey {
        case code = "id"
        case description = "descricao"
        case active = "ativo"
    }
}
