//
//  Surplus.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 24/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Surplus {
    let description: String
    let value: Float
}
