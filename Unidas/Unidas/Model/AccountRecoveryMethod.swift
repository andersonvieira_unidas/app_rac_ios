//
//  AccountRecoveryMethod.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 10/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum AccountRecoveryMethod: Int, CustomStringConvertible {
    case sms, mail
    
    var description: String {
        switch self {
        case .sms:
            return NSLocalizedString("SMS", comment: "")
        case .mail:
            return NSLocalizedString("Email", comment: "")
        }
    }
    
}
