//
//  CreditStore.swift
//  Unidas
//
//  Created by Felipe Machado on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct CreditStore: Codable {
    
    let status: String?
    let nsua: String?
    let merchant_usn: String?
    let customer_id: String?
    let authorizer_id: String?
    let nita: String?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case nsua = "nsua"
        case merchant_usn = "merchant_usn"
        case customer_id = "customer_id"
        case authorizer_id = "authorizer_id"
        case nita = "nita"
    }
}
