//
//  CreditCardResponse.swift
//  Unidas
//
//  Created by Anderson Vieira on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct CreditCardResponse: Codable {
    
    let dados: CreditCardData?
    let chaveRastreio: String?
    let mensagens: [String]?
    
    enum CodingKeys: String, CodingKey {
        case dados = "dados"
        case chaveRastreio = "chaveRastreio"
        case mensagens = "mensagens"
    }
}
