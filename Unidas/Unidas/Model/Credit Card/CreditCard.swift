//
//  CreditCard.swift
//  Unidas
//
//  Created by Felipe Machado on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct CreditCard: Codable {

    let expiry_date: String?
    let security_code: String?
    let number: String?
    let token: String?
    let brand: String?
    
    enum CodingKeys: String, CodingKey {
        case expiry_date = "expiry_date"
        case security_code = "security_code"
        case number = "number"
        case token = "token"
        case brand = "brand"
    }
}
