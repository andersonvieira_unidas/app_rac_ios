//
//  Dados.swift
//  Unidas
//
//  Created by Felipe Machado on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct CreditCardData: Codable {
    
    let code: String?
    let message: String?
    let card: CreditCard?
    let store: CreditStore?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case card = "card"
        case store = "store"
    }
}
