//
//  ProtectionType.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 13/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum ProtectionType: String, Codable {
    case vehicle, complementary, thirdParty
}
