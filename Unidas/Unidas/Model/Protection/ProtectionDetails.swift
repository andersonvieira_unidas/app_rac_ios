//
//  ProtectionDetails.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ProtectionDetails: Codable {
    let iconURL: URL
    let description: String
    let requiredParticipationGroups: [ProtectionParticipationGroup]
}
