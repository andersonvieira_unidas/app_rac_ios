//
//  ProtectionParticipationGroup.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ProtectionParticipationGroup: Codable {
    let groups: String
    let value: Double
}
