//
//  Protection.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Protection: Codable, Equatable {
    let id: String
    let title: String
    let isDefaultOption: Bool
    let disclaimer: String?
    let optionEncompassesTypes: [ProtectionType]?
    let type: ProtectionType
    let price: Double?
    let costs: Costs?
    let details: ProtectionDetails?
}

func ==(lhs: Protection, rhs: Protection) -> Bool {
    return lhs.id.hashValue == rhs.id.hashValue
}
