//
//  LoginPci.swift
//  Unidas
//
//  Created by Anderson Vieira on 29/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct LoginPci: Codable{
    let data: String
    let trackingKey: String
    
    enum CodingKeys: String, CodingKey {
        case data = "dados"
        case trackingKey = "chaveRastreio"
    }
}
