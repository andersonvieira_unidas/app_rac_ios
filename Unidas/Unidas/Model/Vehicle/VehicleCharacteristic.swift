//
//  VehicleCharacteristic.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 02/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleCharacteristic: Codable {
    let image: URL?
    let name: String
}
