//
//  Manufacture.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct VehicleModelManufacture: Codable {
    let id: Int
    let name: String
}
