//
//  Vehicle.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//


struct Vehicle: Codable, CustomDictionaryConvertible {
    let id: Int
    let licensePlate: String
    let model: VehicleModel
    var lastUpdatedTelemetryInformation: VehicleTelemetryInformation?
    var group: VehicleGroup?
    let characteristics: [VehicleCharacteristic]?
    var reservationId: String?
    let fuelLevel: Float?
}
