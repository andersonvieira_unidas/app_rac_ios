//
//  Model.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleModel: Codable {
    let id: Int?
    let name: String?
    let illustrativePicture: URL?
    let manufacture: VehicleModelManufacture
}
