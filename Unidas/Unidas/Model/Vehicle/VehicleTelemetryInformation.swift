//
//  TelemetryInformation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct VehicleTelemetryInformation: Codable {
    let estimatedFuelLevel: Double
    let odometer: Int
    let location: Location
}
