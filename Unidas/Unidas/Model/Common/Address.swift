//
//  Address.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Address: Codable, CustomDictionaryConvertible {
    let id: Int?
    let street: String?
    let number: String?
    let complement: String?
    let neighborhood: String?
    let city: String?
    let state: String?
    let postalCode: String?
    let formattedDistance: String?
    let phoneNumber: String?
    let location: Location?
    let distance: Float?
}

extension Address {
    init(from garageUnidas: GarageUnidas) {
        id = nil
        street = garageUnidas.endereco
        number = nil
        complement = nil
        neighborhood = garageUnidas.bairro
        city = garageUnidas.municipio
        state = garageUnidas.uf
        postalCode = garageUnidas.cep
        formattedDistance = nil
        phoneNumber = garageUnidas.telefoneLoja
        location = Location(storeCoordinate: garageUnidas.geolocalizacao)
        distance = garageUnidas.distancia
    }
}

