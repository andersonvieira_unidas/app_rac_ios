//
//  Costs.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Costs: Codable {
    let daily: Double
}
