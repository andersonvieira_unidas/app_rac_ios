//
//  GarageUnidas.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 26/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct GarageUnidas: Codable {
    let codigoTipoLoja: GarageType
    let codigoLoja: String
    let descricaoLoja: String
    let nomeLoja: String
    let taxaServico: Float?
    let endereco: String?
    let bairro: String?
    let municipio: String?
    let cep: String?
    let uf: String?
    let telefoneLoja: String?
    let geolocalizacao: String?
    let distancia: Float?
    let abreSemana1: String?
    let fechaSemana1: String?
    let abreSemana2: String?
    let fechaSemana2: String?
    let abreSemana3: String?
    let fechaSemana3: String?
    let abreSemana4: String?
    let fechaSemana4: String?
    let abreSabado1: String?
    let fechaSabado1: String?
    let abreSabado2: String?
    let fechaSabado2: String?
    let abreSabado3: String?
    let fechaSabado3: String?
    let abreSabado4: String?
    let fechaSabado4: String?
    let abreDomingo1: String?
    let fechaDomingo1: String?
    let abreDomingo2: String?
    let fechaDomingo2: String?
    let abreDomingo3: String?
    let fechaDomingo3: String?
    let abreDomingo4: String?
    let fechaDomingo4: String?
    let abreFeriadoDefault: String?
    let fechaFeriadoDefault: String?
    let abreFeriado2: String?
    let fechaFeriado2: String?
    let abreFeriado3: String?
    let fechaFeriado3: String?
    let abreFeriado4: String?
    let fechaFeriado4: String?
    let Loj24h: Bool?
    let holidays: [Holiday]?
    let codigoFranqueado: Int?
    
    enum CodingKeys: String, CodingKey {
        case codigoTipoLoja = "CodigoTipoLoja"
        case codigoLoja = "CodigoLoja"
        case descricaoLoja = "DescricaoLoja"
        case nomeLoja = "NomeLoja"
        case taxaServico = "TaxaServico"
        case endereco = "Endereco"
        case bairro = "Bairro"
        case municipio = "Municipio"
        case cep = "Cep"
        case uf = "Uf"
        case telefoneLoja = "TelefoneLoja"
        case geolocalizacao = "Geolocalizacao"
        case distancia = "Distancia"
        case abreSemana1 = "AbreSemana1"
        case fechaSemana1 = "FechaSemana1"
        case abreSemana2 = "AbreSemana2"
        case fechaSemana2 = "FechaSemana2"
        case abreSemana3 = "AbreSemana3"
        case fechaSemana3 = "FechaSemana3"
        case abreSemana4 = "AbreSemana4"
        case fechaSemana4 = "FechaSemana4"
        case abreSabado1 = "AbreSabado1"
        case fechaSabado1 = "FechaSabado1"
        case abreSabado2 = "AbreSabado2"
        case fechaSabado2 = "FechaSabado2"
        case abreSabado3 = "AbreSabado3"
        case fechaSabado3 = "FechaSabado3"
        case abreSabado4 = "AbreSabado4"
        case fechaSabado4 = "FechaSabado4"
        case abreDomingo1 = "AbreDomingo1"
        case fechaDomingo1 = "FechaDomingo1"
        case abreDomingo2 = "AbreDomingo2"
        case fechaDomingo2 = "FechaDomingo2"
        case abreDomingo3 = "AbreDomingo3"
        case fechaDomingo3 = "FechaDomingo3"
        case abreDomingo4 = "AbreDomingo4"
        case fechaDomingo4 = "FechaDomingo4"
        case abreFeriadoDefault = "AbreFeriadoDefault"
        case fechaFeriadoDefault = "FechaFeriadoDefault"
        case abreFeriado2 = "AbreFeriado2"
        case fechaFeriado2 = "FechaFeriado2"
        case abreFeriado3 = "AbreFeriado3"
        case fechaFeriado3 = "FechaFeriado3"
        case abreFeriado4 = "AbreFeriado4"
        case fechaFeriado4 = "FechaFeriado4"
        case Loj24h = "Loj24h"
        case holidays = "Feriado"
        case codigoFranqueado = "CodigoFranqueado"
    }
    
}
