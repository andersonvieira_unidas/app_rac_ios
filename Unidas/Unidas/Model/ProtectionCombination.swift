//
//  ProtectionCombination.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ProtectionCombination: Codable {
    
    let code: Int
    let notAllowedProtectionCodes: [Int]
    
    enum CodingKeys: String, CodingKey {
        case code = "Codigo"
        case notAllowedProtectionCodes = "ListaNaoPermitidos"
    }
    
}
