//
//  ProfileItem.swift
//  Unidas
//
//  Created by Anderson Vieira on 23/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct ProfileItem {
    let image: String
    let localizedString: String
    let separator: Bool
    let action: String?
    let closure: (()->())?
    
    init(image: String, localizedString: String, action: String?, separator: Bool = true, closure: (()->())? = nil) {
        self.image = image
        self.localizedString = localizedString
        self.action = action
        self.separator = separator
        self.closure = closure
    }
}
