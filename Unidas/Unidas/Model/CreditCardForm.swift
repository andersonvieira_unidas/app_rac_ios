//
//  CreditCardForm.swift
//  Unidas
//
//  Created by Mateus Padovani on 11/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CreditCardForm {
    let cardNumber: String
    let holderName: String
    let expirationDate: String
    let defaultCard: Bool
    let authorizerId: AuthorizerId?
}
