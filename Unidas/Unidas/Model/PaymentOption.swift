//
//  PaymentOption.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 15/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum PaymentOption: Int, Codable {
    case payInAdvance = 1
    case payAtPickup
}

