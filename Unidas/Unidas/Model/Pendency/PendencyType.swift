//
//  PendencyType.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

enum PendencyType {
    case PersonalData
    case Documents
    case Payments
    case Signatures
    case Email
    case Mobile
    case Selfie
}
