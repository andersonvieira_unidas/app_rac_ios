//
//  CompanyTerms.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CompanyTerms: Codable {
    let id: Int
    let title: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case title = "Titulo"
        case description = "Descricao"
    }
}
