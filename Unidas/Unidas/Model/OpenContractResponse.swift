//
//  OpenContractResponse.swift
//  Unidas
//
//  Created by Anderson Vieira on 25/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

struct OpenContractResponse: Codable {
    let statusCode: Int
    let message: String?
    let contract: OpenContract?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "HttpStatusCode"
        case message = "Mensagem"
        case contract = "Objeto"
    }
}
