//
//  Zipcode.swift
//  Unidas
//
//  Created by Mateus Padovani on 21/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Zipcode: Codable {
    let idLocalidade: Int
    let idUf: Int?
    let logradouro: String?
    let bairro: String?
    let nmLogradouroTipo: String?
    let nmLogradouroAbreviacao: String?
    let uf: String
    let localidade: String
    let cep: String
    let logradouroTipo: String?

    
    enum CodingKeys: String, CodingKey {
        case idLocalidade = "IdLogradouro"
        case idUf = "IdUf"
        case logradouro = "logradouro"
        case bairro = "bairro"
        case nmLogradouroTipo = "NmLogradouroTipo"
        case nmLogradouroAbreviacao = "NmLogradouroAbreviacao"
        case uf = "uf"
        case localidade = "localidade"
        case cep = "cep"
        case logradouroTipo = "LogradouroTipo"

    }
}

