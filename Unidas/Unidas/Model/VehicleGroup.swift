//
//  Group.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct VehicleGroup: Codable {
    let id: String
    let name: String
    let vehicleNames: [String]
    let mainCharacteristic: String
    let characteristics: [VehicleCharacteristic]
    let vehiclePicture: URL?
    let costs: VehicleGroupCosts
    let isFreeMileageAvailable: Bool
    let isAvailable: Bool
}
