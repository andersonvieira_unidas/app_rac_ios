//
//  UserUnidasStatus.swift
//  Unidas
//
//  Created by Mateus Padovani on 17/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct UserUnidasStatus: Codable {
    let status: UserStatus
    let statusDescription: String
    
    enum CodingKeys: String, CodingKey {
        case status = "ContaStatusId"
        case statusDescription = "ContaStatusDescricao"
    }
}
