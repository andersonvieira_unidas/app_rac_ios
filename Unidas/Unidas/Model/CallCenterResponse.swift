//
//  CallCenterResponse.swift
//  Unidas
//
//  Created by Mateus Padovani on 29/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CallCenterResponse: Codable {
    let titleSAC: String?
    let descriptionSAC: String?
    let phoneNumberSAC: String?
    let tileCentralReserve: String?
    let descriptionCentralReserve: String?
    let phoneNumberCentralReserve: String?
    let titleAssistence: String?
    let descriptionAssistence: String?
    let phoneNumberAssistence: String?
    
    enum CodingKeys: String, CodingKey {
        case titleSAC = "TituloSAC"
        case descriptionSAC = "DescricaoSAC"
        case phoneNumberSAC = "TelefoneSAC"
        case tileCentralReserve = "TituloCentralReservas"
        case descriptionCentralReserve = "DescricaoCentralReservas"
        case phoneNumberCentralReserve = "TelefoneCentralReservas"
        case titleAssistence = "TituloAssistencia24h"
        case descriptionAssistence = "DescricaoAssistencia24h"
        case phoneNumberAssistence = "TelefoneAssistencia24h"
        
    }
}
