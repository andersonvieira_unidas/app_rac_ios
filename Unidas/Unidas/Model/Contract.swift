//
//  Contract.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/07/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Contract: Codable {
    
    let id: Int?
    let pickupGarage: String?
    let pickupGarageDescription: String?
    let pickupGarageObservation: String?
    let pickupGarageState: String?
    let pickupGarageGeolocalization: String?
    let returnGarage: String?
    let returnGarageDescription: String?
    let returnGarageObservation: String?
    let returnGarageGeolocalization: String?
    let pickupDate: String?
    let returnDate: String?
    let pickupHour: String?
    let returnHour: String?
    let renterId: String?
    let renterName: String?
    let status: ContractStatus?
    let reservationId: Int?
    let salesChannel: Int?
    let group: String?
    let groupDescription: String?
    let groupVehicles: String?
    var vehicle: ContractVehicle?
    let protections: [ContractDetailListItem]?
    let services: [ContractDetailListItem]?
    let additional: [ContractDetailListItem]?
    var payments: [ContractDetailPaymentListItem]?
    let pickUpRadius: Float?
    let pickupPatioGeolocalization: String?
    let returnPatioGeolocalization: String?
    let returnRadius: Float?
    let qrCodeInfo: String?
    let reservaPessoaJuridica: Bool?
    let empresaCNPJ: String?
    let empresaNome: String?
    let voucher: VoucherLegalEntity?
    let obrigatorioPre: Bool?
    let obrigatorioPgto: Bool?
    let pictureURL: URL?
    
    enum CodingKeys: String, CodingKey {
        case id = "Contrato"
        case pickupGarage = "LojaRetirada"
        case pickupGarageDescription = "LojaRetiradaDescricao"
        case pickupGarageGeolocalization = "LojaRetiradaGeolocalizacao"
        case pickupGarageObservation = "LojaRetiradaObservacao"
        case pickupGarageState = "LojaRetiradaUF"
        case returnGarage = "LojaDevolucao"
        case returnGarageDescription = "LojaDevolucaoDescricao"
        case returnGarageGeolocalization = "LojaDevolucaoGeolocalizacao"
        case returnGarageObservation = "LojaDevolucaoObservacao"
        case pickupDate = "DataRetirada"
        case returnDate = "DataDevolucao"
        case pickupHour = "HoraRetirada"
        case returnHour = "HoraDevolucao"
        case renterId = "Locatario"
        case renterName = "LocatarioNome"
        case status = "Status"
        case reservationId = "Reserva"
        case salesChannel = "CanalVenda"
        case group = "Grupo"
        case groupDescription = "GrupoDescricao"
        case groupVehicles = "GrupoVeiculos"
        case vehicle = "Veiculo"
        case protections = "ListaProtecoes"
        case services = "ListaServicos"
        case additional = "ListaAdicionais"
        case payments = "ListaPagamentos"
        case pickUpRadius = "RetiradaMetrosRaioAceitavel"
        case pickupPatioGeolocalization = "PatioRetiradaGeolocalizacao"
        case returnPatioGeolocalization = "PatioDevolucaoGeolocalizacao"
        case returnRadius = "DevolucaoMetrosRaioAceitavel"
        case qrCodeInfo = "QRCode"
        case reservaPessoaJuridica = "ContratoPessoaJuridica"
        case empresaCNPJ = "EmpresaCNPJ"
        case empresaNome = "FaturadoNome"
        case voucher = "Voucher"
        case obrigatorioPre = "ObrigatorioPre"
        case obrigatorioPgto = "ObrigatorioPgto"
        case pictureURL = "GrupoImagemURL"
    }
    
}
