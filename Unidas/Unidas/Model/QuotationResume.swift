//
//  QuotationResume.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 07/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct QuotationResume {
    
    var quotation: VehAvail?
    var pickUp: GarageReservationDetails?
    var `return`: GarageReservationDetails?
    var selectedProtections: [PricedCoverages]?
    var selectedEquipments: [PricedEquips]?
    
    init() {
        quotation = nil
        pickUp = nil
        `return` = nil
        selectedProtections = nil
        selectedEquipments = nil
    }
    
}
