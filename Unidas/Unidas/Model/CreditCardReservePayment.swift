//
//  CreditCardReservePayment.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CreditCardReservePayment {
    let card: ListCard
    let amount: Double
    let securityCode: String
    let installmentsOption: Int
}
