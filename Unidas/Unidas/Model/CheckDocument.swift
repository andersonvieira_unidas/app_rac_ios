//
//  CheckDocument.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum CheckDocumentType: String, Codable {
    case profile = "Selfie"
    case cnhDocument = "CNH"
    case signature = "Assinatura do Cliente"
}

struct CheckDocument: Codable {
    let documentNumber: String
    let updateAt: String?
    let validateAt: String?
    let type: CheckDocumentType
    
    enum CodingKeys: String, CodingKey {
        case documentNumber = "Documento"
        case updateAt = "DocumentoInclusao"
        case validateAt = "DocumentoValidade"
        case type = "DocumentoTipo"
    }
}
