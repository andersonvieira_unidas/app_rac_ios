//
//  MyReservationDetail.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/07/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

struct MyReservationDetail: Codable {
    let reservation: VehicleReservations?
    let pendencies: Bool?
    let userAvailableToCheckin: Bool?
    
    enum CodingKeys: String, CodingKey {
        case reservation = "Reserva"
        case pendencies = "Pendencia"
        case userAvailableToCheckin = "UsuarioPodeFazerCheckin"
    }
}
