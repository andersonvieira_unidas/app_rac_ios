//
//  ReservationStatus.swift
//  Unidas
//
//  Created by Anderson Vieira on 12/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

enum ReservationStatus: String, Codable{
    case reservation
    case paid
    case checkin
    case qrCode
}

