//
//  File.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct MyReservations: Codable {
    let reservations: [VehicleReservations]?
    let pendencies: Bool?
    let userAvailableToCheckin: Bool?
    let financialPendency: Bool?
    
    enum CodingKeys: String, CodingKey {
        case reservations = "MinhaReserva"
        case pendencies = "Pendencia"
        case financialPendency = "PendenciaFinanceira"
        case userAvailableToCheckin = "UsuarioPodeFazerCheckin"
    }
}
