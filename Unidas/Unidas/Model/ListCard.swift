//
//  ListCard.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct ListCard: Codable {
    let metodoPagamentoTipoid: Int
    let metodoPagamentoTipoDescricao: String
    let pan: String?
    let openPan: String
    let val: String
    let tknbnddes: String
    let clinom: String
    let metodoPagamentoCartaoCreditoPreferencial: Int
    let metodoPagamentoCartaoCreditoAtivo: Int
    let metodoPagamentoCartaoCreditoExpirado: Int
    let metodoPagamentoCartaoCreditoValidado: Int
    let clidoc: String
    let tkncodseq: Int?
    
    enum CodingKeys: String, CodingKey {
        case metodoPagamentoTipoid = "metodoPagamentoTipoid"
        case metodoPagamentoTipoDescricao = "metodoPagamentoTipoDescricao"
        case pan = "PAN"
        case openPan = "open_PAN"
        case val = "val"
        case tknbnddes = "tknbnddes"
        case clinom = "clinom"
        case metodoPagamentoCartaoCreditoPreferencial = "metodoPagamentoCartaoCreditoPreferencial"
        case metodoPagamentoCartaoCreditoAtivo = "metodoPagamentoCartaoCreditoAtivo"
        case metodoPagamentoCartaoCreditoExpirado = "metodoPagamentoCartaoCreditoExpirado"
        case metodoPagamentoCartaoCreditoValidado = "metodoPagamentoCartaoCreditoValidado"
        case clidoc = "CLIDOC"
        case tkncodseq = "TKNCODSEQ"
    }
}
