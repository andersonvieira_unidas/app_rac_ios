//
//  GenericError.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum GenericError: LocalizedError {
    case unknown
    case error(message: String)
    
    var errorDescription: String? {
        switch self {
        case .unknown: return NSLocalizedString("An unknown error occurred", comment: "Generic unknown error description")
        case .error(let message): return message
        }
    }
}
