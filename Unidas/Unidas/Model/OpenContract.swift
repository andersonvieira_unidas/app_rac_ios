//
//  OpenContract.swift
//  Unidas
//
//  Created by Mateus Padovani on 17/08/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct OpenContract: Codable {
    let contractNumber: Int
    let message: String?
    let preAuthorizationFinished: Bool
    let paymentFinished: Bool
    
    enum CodingKeys: String, CodingKey {
        case contractNumber = "NumeroContrato"
        case message = "Mensagem"
        case preAuthorizationFinished = "PreAutorizacaoProcessada"
        case paymentFinished = "PagamentoProcessado"
    }
}
