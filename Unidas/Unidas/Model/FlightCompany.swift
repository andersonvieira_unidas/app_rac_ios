//
//  FlightCompany.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 9/20/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct FlightCompany: Codable {
    let code: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case code = "Codigo"
        case description = "Descricao"
    }
}
