//
//  Card.swift
//  Unidas
//
//  Created by Mateus Padovani on 02/05/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

enum AuthorizerId: Int, Codable {
    case unknow, visa, masterCard, americanExpress, multiCheck, hiper, aura, diners, elo
    
    var authorizeString: Int {
        switch self {
        case .visa: return 1
        case .masterCard: return 2
        case .americanExpress: return 3
        case .hiper: return 5
        case .aura: return 6
        case .diners: return 33
        case .elo: return 41
        default: return 0
        }
    }
    
    var authorizeName: String {
        switch self {
        case .masterCard: return "MASTERCARD"
        case .visa: return "VISA"
        case .americanExpress: return "AMEX"
        case .hiper: return "HIPER"
        case .aura: return "AURA"
        case .diners: return "DINERS"
        case .elo: return "ELO"
        default: return ""
        }
    }
    
    var brandImage: UIImage {
        switch self {
        case .visa:
            return #imageLiteral(resourceName: "card-brand-visa")
        case .masterCard:
            return #imageLiteral(resourceName: "card-brand-mastercard")
        case .americanExpress:
            return #imageLiteral(resourceName: "card-brand-amex")
        case .hiper:
            return #imageLiteral(resourceName: "card-brand-hipercard")
        case .diners:
            return #imageLiteral(resourceName: "card-brand-diners")
        case .elo:
            return #imageLiteral(resourceName: "minicard-brand-elo")
        default:
            return #imageLiteral(resourceName: "card-off")
        }
    }
}

struct Card: Codable {
    let authorizerIdField: AuthorizerId
    let cardHashField: String
    let customerIdField: String
}
