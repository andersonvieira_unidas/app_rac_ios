//
//  MyAccountItem.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct MyAccountItem {
    let image: String
    let localizedString: String
    let separator: Bool
    let action: String?
    
    init(image: String, localizedString: String, action: String?, separator: Bool = true) {
        self.image = image
        self.localizedString = localizedString
        self.action = action
        self.separator = separator
    }
}

