//
//  StoreAvailabilityError.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 19/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum StoreAvailabilityError: LocalizedError {
    case pickUpStoreUnavailableAtThisTime
    case returnStoreUnavailableAtThisTime
    
    var errorDescription: String? {
        switch self {
        case .pickUpStoreUnavailableAtThisTime:
            return NSLocalizedString("Pick up store is not available at this time", comment: "Pick up store is not available at this time error message")
        case .returnStoreUnavailableAtThisTime:
            return NSLocalizedString("Return store is not available at this time", comment: "Return store is not available at this time error message")
        }
    }
    
}
