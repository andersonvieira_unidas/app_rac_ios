//
//  Reservation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//
import Foundation

struct Reservation: Codable {
    let id: String?
    var number: String? = nil
    var totalValue: Double? = nil
    var payedValue: Double? = nil
    var prePaymentValue: Double? = nil
    var preAuthorizationValue: Double? = nil
    var pickUp: GarageReservationDetails? = nil
    var `return`: GarageReservationDetails? = nil
    var group: VehMakeModel? = nil
    var protections: [PricedCoverages]? = nil
    var equipments: [PricedEquips]? = nil
    var rateQualifier: String? = nil
    var duration: Int? = nil
    var returnTax: Double? = nil
    var extraHourTax: Double? = nil
    var administrativeTaxValue: Double? = nil
    var administrativeTaxPercentValue: Double? = nil
    var voucher: String? = nil
    var airlineInformation: AirlineInformation? = nil
    var selectedPaymentOption: PaymentOption = .payInAdvance
    var selectedCasterWeekday: Int? = nil
    var prePaymentDiscountValue: Double? = nil
    var sendSMS: Bool? = nil
    var promotionCode: String? = nil
    var acceptTermLocation: Bool? = nil
    var acceptTermRefound: Bool? = nil
    var pictureURL: URL? = nil
    var paymentStatus: ReservationObjectPaymentStatus? = nil
    var express: Bool? = nil
}

extension Reservation {
    
    init() {
        self.id = nil
    }
    
}

