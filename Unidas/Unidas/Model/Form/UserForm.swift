//
//  UserForm.swift
//  Unidas
//
//  Created by Mateus Padovani on 25/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct UserForm {
    var documentNumber: String
    var name: String
    var birthDate: Date?
    var email: String
    var phoneNumber: String
    var motherName: String?
}
