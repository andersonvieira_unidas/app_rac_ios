//
//  DriverLicenseForm.swift
//  Unidas
//
//  Created by Anderson Vieira on 28/02/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct DriverLicenseForm {
    let registrationId: String
    let issuingBody: String
    let state: String
    let driverLicenseNumber: String
    let validThru: Date
    let firstDriverLicense: Date
    let fatherName: String
    let motherName: String
}
