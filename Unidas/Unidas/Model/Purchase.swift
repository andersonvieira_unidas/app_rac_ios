//
//  Purchase.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Purchase: Codable {
    let codigo: Int?
    let tipo: String?
    let nome: String
    let codigoOTA: Int?
    let descricao: String?
    let quantidade: Int
    let valorUnitario: Float
    let valorTotal: Float
    
    enum CodingKeys: String, CodingKey {
        case codigo = "Codigo"
        case tipo = "Tipo"
        case nome = "Nome"
        case codigoOTA = "CodigoOTA"
        case descricao = "Descricao"
        case quantidade = "Quantidade"
        case valorUnitario = "ValorUnitario"
        case valorTotal = "ValorTotal"
    }
}
