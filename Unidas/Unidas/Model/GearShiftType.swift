//
//  GearShiftType.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 30/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum GearShiftType: String, Codable {
    case automatic = "Automático"
    case manual = "Manual"
}
