//
//  ToggleMenu.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class ToggleMenu {
    var config: ConfigToggle?
    
    init(configToggle: ConfigToggle) {
        self.config = configToggle
    }
}
