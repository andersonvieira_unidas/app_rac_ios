//
//  User.swift
//  Unidas
//
//  Created by Mateus Padovani on 07/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum UserStatus: Int, Codable {
    case noUser, userCreated, userInValidate, userValidate
}

struct User: Codable {
    let id: Int?
    let status: UserStatus?
    let name: String?
    let documentNumber: String?
    let email: String?
    let mobilePhone: String?
    let phone: String?
    let birthDate: Date?
    let motherName: String?
    let token: String?
}
