//
//  RecoverReservationInformation.swift
//  Unidas
//
//  Created by Mateus Padovani on 04/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct RecoverReservationInformation: Codable, CustomDictionaryConvertible {
    let uuid: UUID
    let documentNumber: String
    let reserveNumber: String
}
