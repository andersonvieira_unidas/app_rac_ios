//
//  UnidasToken.swift
//  Unidas
//
//  Created by Mateus Padovani on 23/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct UnidasToken: Codable {
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case token = "ContaClienteToken"
    }
}
