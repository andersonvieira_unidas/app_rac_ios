//
//  Payment.swift
//  Unidas
//
//  Created by Mateus Padovani on 09/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Payment: Codable {
    let id: String
    let number: String
    let brand: String
    let holderName: String
}

