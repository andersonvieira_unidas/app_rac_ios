//
//  ResumeInformation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 04/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct ResumeInformation {
    let title: String?
    let subtitle: String?
    let value: String?
}
