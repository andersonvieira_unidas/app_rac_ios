//
//  Store.swift
//  Unidas
//
//  Created by Pedro Felipe Machado on 04/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

class Store {
    
    var store: Bool?
    
    init(json: [String:AnyObject], storeName: String) {
        
        if let value = json["\(storeName)"] as? Bool {
            store = value
        }
    }
}
