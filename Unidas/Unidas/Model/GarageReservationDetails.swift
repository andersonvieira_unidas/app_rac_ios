//
//  GarageReservationDetails.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 22/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct GarageReservationDetails: Codable {
    let date: Date
    let garage: Garage
    
    var dateAPI: String {
        return DateFormatter.dateFormatterAPI.string(from: date)
    }
}
