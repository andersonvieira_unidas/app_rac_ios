//
//  CallCenter.swift
//  Unidas
//
//  Created by Mateus Padovani on 05/07/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

enum CallCenterType: Int, Codable {
    case customerService
    case reservationCenter
    case assistance
    
    var image: UIImage? {
        switch self {
        case .customerService:
            return #imageLiteral(resourceName: "icon-more-support")
        case .reservationCenter:
            return #imageLiteral(resourceName: "ico-avatar-speak")
        case .assistance:
            return #imageLiteral(resourceName: "ico-car-protect")
        }
    }
}
struct CallCenter: Codable {
    let type: CallCenterType
    let title: String?
    let description: String?
    let phoneNumber: String?
}
