//
//  Garage.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 20/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct Garage: Codable {
    let name: String
    let code: String
    let description: String
    let address: Address?
    let returnTax: Float?
    let icon24Hours: Bool?
    let type: GarageType
    let openingHours: OpeningHours
    let holidays: [Holiday]?
    let franchiseCode: Int?
}

extension Garage {
    
    init(garageUnidas: GarageUnidas) {
        name = garageUnidas.nomeLoja
        code = garageUnidas.codigoLoja
        description = garageUnidas.descricaoLoja
        returnTax = garageUnidas.taxaServico
        type = garageUnidas.codigoTipoLoja
        openingHours = OpeningHours(from: garageUnidas)
        address = Address(from: garageUnidas)
        holidays = garageUnidas.holidays
        icon24Hours = garageUnidas.Loj24h
        franchiseCode = garageUnidas.codigoFranqueado
    }
}
