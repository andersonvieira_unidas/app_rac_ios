//
//  OpeningHours.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct OpeningHours: Codable {
    let informativeText: String?
    let periods: [OpeningHoursPeriod]
}

extension OpeningHours {
    init(from garageUnidas: GarageUnidas) {
        informativeText = nil
        periods = OpeningHours.buildPeriods(with: garageUnidas)
    }
    
    private static func buildPeriods(with garageUnidas: GarageUnidas) -> [OpeningHoursPeriod] {
        var periods: [OpeningHoursPeriod?] = []
        // Weekdays
        periods.append(openingPeriod(for: .weekday, open: garageUnidas.abreSemana1, close: garageUnidas.fechaSemana1))
        periods.append(openingPeriod(for: .weekday, open: garageUnidas.abreSemana2, close: garageUnidas.fechaSemana2))
        periods.append(openingPeriod(for: .weekday, open: garageUnidas.abreSemana3, close: garageUnidas.fechaSemana3))
        periods.append(openingPeriod(for: .weekday, open: garageUnidas.abreSemana4, close: garageUnidas.fechaSemana4))
        // Saturday
        periods.append(openingPeriod(for: .saturday, open: garageUnidas.abreSabado1, close: garageUnidas.fechaSabado1))
        periods.append(openingPeriod(for: .saturday, open: garageUnidas.abreSabado2, close: garageUnidas.fechaSabado2))
        periods.append(openingPeriod(for: .saturday, open: garageUnidas.abreSabado3, close: garageUnidas.fechaSabado3))
        periods.append(openingPeriod(for: .saturday, open: garageUnidas.abreSabado4, close: garageUnidas.fechaSabado4))
        // Sunday
        periods.append(openingPeriod(for: .sunday, open: garageUnidas.abreDomingo1, close: garageUnidas.fechaDomingo1))
        periods.append(openingPeriod(for: .sunday, open: garageUnidas.abreDomingo2, close: garageUnidas.fechaDomingo2))
        periods.append(openingPeriod(for: .sunday, open: garageUnidas.abreDomingo3, close: garageUnidas.fechaDomingo3))
        periods.append(openingPeriod(for: .sunday, open: garageUnidas.abreDomingo4, close: garageUnidas.fechaDomingo4))
        // Holiday
        periods.append(openingPeriod(for: .holiday, open: garageUnidas.abreFeriadoDefault, close: garageUnidas.fechaFeriadoDefault))
        periods.append(openingPeriod(for: .holiday, open: garageUnidas.abreFeriado2, close: garageUnidas.fechaFeriado2))
        periods.append(openingPeriod(for: .holiday, open: garageUnidas.abreFeriado3, close: garageUnidas.fechaFeriado3))
        periods.append(openingPeriod(for: .holiday, open: garageUnidas.abreFeriado4, close: garageUnidas.fechaFeriado4))
        
        return periods.compactMap { $0 }
    }
    
    private static func openingPeriod(for day: Workday, open: String?, close: String?) -> OpeningHoursPeriod? {
        guard let open = open, let close = close, !open.isEmpty && !close.isEmpty else { return nil }
        return OpeningHoursPeriod(day: day, open: open, close: close)
    }
    
}
