//
//  GarageType.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 05/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

enum GarageType: Int {
    case airport = 1
    case store = 2
    case terminal = 3
    case shopping = 8
    case market = 6
    case express = 11
    case unknown
}

extension GarageType: Codable {
    public init(from decoder: Decoder) throws {
        self = try GarageType(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}
