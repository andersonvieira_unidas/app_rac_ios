//
//  OpeningHoursPeriod.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 28/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct OpeningHoursPeriod: Codable {
    let day: Workday
    let open: String
    let close: String
}
