//
//  CalculationCancel.swift
//  Unidas
//
//  Created by Mateus Padovani on 28/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct CalculationCancel: Codable {
    let reservation: [CalculationCancelReservation]
    
    enum CodingKeys: String, CodingKey {
        case reservation = "reserva"
    }
}
