//
//  AirlineInformation.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 6/20/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

struct AirlineInformation: Codable {
    let flightNumber: String
    let company: FlightCompany
}
