//
//  Home.swift
//  Unidas
//
//  Created by Anderson Vieira on 07/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct Home: Codable {
    let lastReservation: VehicleReservations?
    let pendencies: Bool?
    let userAvailableToCheckin: Bool?
    let financialPendency: Bool?
    
    enum CodingKeys: String, CodingKey {
        case lastReservation = "MinhaReserva"
        case pendencies = "Pendencia"
        case userAvailableToCheckin = "UsuarioPodeFazerCheckin"
        case financialPendency = "PendenciaFinanceira"
    }
}
