//
//  ReservationRules.swift
//  Unidas
//
//  Created by Anderson Vieira on 06/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

protocol ReservationRules {
    var reservationRulesDataView: ReservationRulesDataView { get set }
    
    func make() -> ReservationStep
    func checkStatus() -> [ReservationStatus]
    func checkCurrentStatus() -> ReservationStatus
    func makeTitle() -> String
    func makeDescription() -> String
    func makeButtonTitle() -> String
    func makeButtonAction() -> ReservationAction?
    func checkButtonEnabled() -> Bool
    func makeReservation() -> String
}

class ReservationRulesFactory {
    func constructRule(dataView: ReservationRulesDataView) -> ReservationRules {
        if dataView.express {
            return ReservationExpressRules(reservationRulesDataView: dataView)
        } else {
            return ReservationCommomRules(reservationRulesDataView: dataView)
        }
    }
}


struct ReservationStep: Codable {
    let reservation: String
    let status: [ReservationStatus]
    let title: String
    let description: String
    let buttonTitle: String
    let action: ReservationAction?
    let buttonEnabled: Bool
    let currentStatus: ReservationStatus
    let dueDate: Date?
    let expressDateExpired: Bool?
    let express: Bool
    let financialPendency: Bool
    let paid: Bool
}

enum ReservationAction: String, Codable {
    case editProfile
    case payment
    case checkin
    case qrcode
}
