//
//  ReservationCommomRules.swift
//  Unidas
//
//  Created by Anderson Vieira on 10/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

class ReservationCommomRules: ReservationRules {
    
    var reservationRulesDataView: ReservationRulesDataView
    
    init(reservationRulesDataView: ReservationRulesDataView) {
        self.reservationRulesDataView = reservationRulesDataView
    }
    
    func make() -> ReservationStep {
        let status = checkStatus()
        let title = makeTitle()
        let description = makeDescription()
        let buttonTitle = makeButtonTitle()
        let reservation = makeReservation()
        let action = makeButtonAction()
        let buttonEnabled = checkButtonEnabled()
        let currentStatus = checkCurrentStatus()
        let paid = checkPaid()
        
        let reservationStep = ReservationStep(reservation: reservation, status: status, title: title, description: description, buttonTitle: buttonTitle, action: action, buttonEnabled: buttonEnabled, currentStatus: currentStatus, dueDate: nil, expressDateExpired: nil, express: reservationRulesDataView.express, financialPendency: reservationRulesDataView.financialPendency, paid: paid)
        
        return reservationStep
    }
    
    func checkStatus() -> [ReservationStatus] {
        var status: [ReservationStatus] = [.reservation]
        
        if reservationRulesDataView.reservationPaid {
            status.append(.paid)
        }
        
        if reservationRulesDataView.preAuthorizationPaid {
            status.append(.checkin)
        }
        
        return status
    }
    
    func checkCurrentStatus() -> ReservationStatus {
        if reservationRulesDataView.preAuthorizationPaid && reservationRulesDataView.reservationPaid {
            return .qrCode
        }
        
        if reservationRulesDataView.checkinAvailable {
            return .checkin
        }
        
        if !reservationRulesDataView.reservationPaid {
            return .paid
        }
        
        return .reservation
    }
    
    func makeTitle() -> String {
        if reservationRulesDataView.preAuthorizationPaid {
            let title = NSLocalizedString("Reservation Rules Reservation Check-in", comment: "")
            return title
        }
        
        if reservationRulesDataView.reservationPaid  {
            let title = NSLocalizedString("Reservation Rules Reservation Payment Done", comment: "")
            return title
        }
        
        let title = NSLocalizedString("Reservation Rules Reservation Done", comment: "")
        
        return title
    }
    
    func makeDescription() -> String {
        if !reservationRulesDataView.userAvailableToCheckin && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable && !reservationRulesDataView.userValidated {
            return NSLocalizedString("Reservation Rules Reservation - Complete your account", comment: "")
        }
        
        if !reservationRulesDataView.userValidated {
            return NSLocalizedString("Reservation Rules Reservation - Complete your account", comment: "")
        }
        
        if  reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return NSLocalizedString("Reservation Rules Reservation - Antecipate your payment", comment: "")
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.checkinAvailable && !reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation - Check-in and expedite your pickup", comment: "")
        }
        
        if !reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && !reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation - Check-in at the count", comment: "")
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            let localized = NSLocalizedString("Reservation Rules Reservation - Check-in in %@ hours before", comment: "")
            if let hours = reservationRulesDataView.hoursToCheckin {
                return String(format: localized, String(hours))
            }
            
            return NSLocalizedString("Reservation Rules Reservation - Check-in in %@ hours before", comment: "")
        }
        
        if reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation - Show your QRCode in pickup", comment: "")
        }
        return ""
    }
    
    func makeButtonTitle() -> String {
        if !reservationRulesDataView.userAvailableToCheckin && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable && !reservationRulesDataView.userValidated {
            return NSLocalizedString("Reservation Rules - Button Edit Profile", comment: "")
        }
        
        if !reservationRulesDataView.userValidated {
            return NSLocalizedString("Reservation Rules - Button Edit Profile", comment: "")
        }
        
        if  reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return NSLocalizedString("Reservation Rules - Button Payment", comment: "")
        }
        if !reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return NSLocalizedString("Reservation Rules - Button Check-in", comment: "")
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.checkinAvailable && !reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules - Button Check-in", comment: "")
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return NSLocalizedString("Reservation Rules - Button Check-in", comment: "")
        }
        
        if reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules - Button See QRCode", comment: "")
        }
        return ""
    }
    
    func makeButtonAction() -> ReservationAction? {
        if !reservationRulesDataView.userValidated {
            return .editProfile
        }
        
        if !reservationRulesDataView.userAvailableToCheckin && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable && !reservationRulesDataView.userValidated {
            return .editProfile
        }
        
        if  reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable  {
            return .payment
        }
        if !reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return .checkin
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.checkinAvailable && !reservationRulesDataView.preAuthorizationPaid {
            return .checkin
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return .checkin
        }
        
        if reservationRulesDataView.preAuthorizationPaid {
            return .qrcode
        }
        
        return nil
    }
    
    func checkButtonEnabled() -> Bool {
        if reservationRulesDataView.userValidated && reservationRulesDataView.isFranchise {
            return false
        }
        
        if !reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return false
        }
        
        if reservationRulesDataView.userAvailableToCheckin && reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return false
        }
        return true
    }
    
    func makeReservation() -> String {
        let localized = NSLocalizedString("Reservation Rules Reservation %@", comment: "")
        return String(format: localized, reservationRulesDataView.reservationNumber)
    }
    
    private func checkPaid() -> Bool {
        return reservationRulesDataView.reservationPaid
    }
}
