//
//  ReservationExpressRules.swift
//  Unidas
//
//  Created by Anderson Vieira on 10/10/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

class ReservationExpressRules: ReservationRules {
    
    var reservationRulesDataView: ReservationRulesDataView
    
    init(reservationRulesDataView: ReservationRulesDataView) {
        self.reservationRulesDataView = reservationRulesDataView
    }
    
    func make() -> ReservationStep {
        let status = checkStatus()
        let title = makeTitle()
        let description = makeDescription()
        let buttonTitle = makeButtonTitle()
        let reservation = makeReservation()
        let action = makeButtonAction()
        let buttonEnabled = checkButtonEnabled()
        let currentStatus = checkCurrentStatus()
        let dueDate = returnDueDate()
        let expressDateExpired = checkDateExpired()
        let paid = checkPaid()
        
        let reservationStep = ReservationStep(reservation: reservation, status: status, title: title, description: description, buttonTitle: buttonTitle, action: action, buttonEnabled: buttonEnabled, currentStatus: currentStatus, dueDate: dueDate, expressDateExpired: expressDateExpired, express: reservationRulesDataView.express, financialPendency: reservationRulesDataView.financialPendency, paid: paid)
        
        return reservationStep
    }

    func checkStatus() -> [ReservationStatus] {
        var status: [ReservationStatus] = [.reservation]
        let dateExpired = checkDateExpired()
       
        if !reservationRulesDataView.reservationPaid && dateExpired {
            status.append(.paid)
        }
        
        if reservationRulesDataView.financialPendency {
            status.append(.paid)
        }
        
        if reservationRulesDataView.reservationPaid {
            status.append(.paid)
            status.append(.checkin)
        }
 
        if reservationRulesDataView.preAuthorizationPaid {
            status.append(.checkin)
        }
        
        return status
    }
    
    func checkCurrentStatus() -> ReservationStatus {
        if !reservationRulesDataView.reservationPaid {
            return .reservation
        }
        
        if reservationRulesDataView.reservationPaid {
            return .checkin
        }
        return .reservation
    }
    
    func makeTitle() -> String {
        
        let dateExpired = checkDateExpired()
        
        if reservationRulesDataView.financialPendency {
            let title = NSLocalizedString("Reservation Rules Reservation Express - User has Financial Pendency", comment: "")
            return title
        }
        
        if dateExpired && !reservationRulesDataView.reservationPaid {
            let title = NSLocalizedString("Reservation Rules Reservation Express - Date expired", comment: "")
            return title
        }
        
        if reservationRulesDataView.reservationPaid && !reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation Express - Checkin", comment: "")
        } else if !dateExpired, !reservationRulesDataView.preAuthorizationPaid, let dueTime = reservationRulesDataView.dueTime {
            let title = NSLocalizedString("Reservation Rules Reservation Express - Date valid %@", comment: "")
            return String(format: title, String(dueTime/60))
        } else if reservationRulesDataView.reservationPaid && reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation Express - Checkin", comment: "")
        }
        return ""
    }
    
    func makeDescription() -> String {
        let dateExpired = checkDateExpired()
        
        if reservationRulesDataView.financialPendency {
            return NSLocalizedString("Reservation Rules Reservation Express - User with Financial Pendency", comment: "")
        }
        
        if dateExpired && !reservationRulesDataView.reservationPaid {
            return NSLocalizedString("Reservation Rules Reservation Express - Due Date Expired", comment: "")
        }
        
        if reservationRulesDataView.reservationPaid && !reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation Express - Check-in", comment: "")
        }
        
        if reservationRulesDataView.reservationPaid && reservationRulesDataView.preAuthorizationPaid {
            return NSLocalizedString("Reservation Rules Reservation Express - Check-in", comment: "")
        }
        return ""
    }
    
    func makeButtonTitle() -> String {
        
        if !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable && !reservationRulesDataView.userValidated {
            return NSLocalizedString("Reservation Rules - Button Edit Profile", comment: "")
        }
        
        if !reservationRulesDataView.userValidated || reservationRulesDataView.financialPendency && !reservationRulesDataView.reservationPaid {
            return NSLocalizedString("Reservation Rules - Button Edit Profile", comment: "")
        }
        
        if  reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid {
            return NSLocalizedString("Reservation Rules - Button Payment", comment: "")
        }
        
        if reservationRulesDataView.reservationPaid {
            return NSLocalizedString("Reservation Rules - Button See QRCode", comment: "")
        }
        return ""
    }
    
    func makeButtonAction() -> ReservationAction?{

        if !reservationRulesDataView.userValidated || reservationRulesDataView.financialPendency && !reservationRulesDataView.reservationPaid {
            return .editProfile
        }
        
        if !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable && !reservationRulesDataView.userValidated {
            return .editProfile
        }
        
        if  reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid  {
            return .payment
        }
        if reservationRulesDataView.userValidated && !reservationRulesDataView.reservationPaid && !reservationRulesDataView.checkinAvailable {
            return .checkin
        }
        
        if  reservationRulesDataView.userValidated && reservationRulesDataView.checkinAvailable && !reservationRulesDataView.reservationPaid {
            return .checkin
        }
        
        if reservationRulesDataView.userValidated && reservationRulesDataView.reservationPaid && !reservationRulesDataView.financialPendency {
            return .qrcode
        }
        
        return nil
    }
    
    func checkButtonEnabled() -> Bool {
        let dateExpired = checkDateExpired()
        
        if (dateExpired && !reservationRulesDataView.reservationPaid) {
            return false
        }
        
        if reservationRulesDataView.isFranchise {
            return false
        }
        
        if reservationRulesDataView.financialPendency {
            return false
        }
        
        if reservationRulesDataView.reservationPaid {
            return true
        }

        return true
    }
    
    func makeReservation() -> String {
        if !reservationRulesDataView.reservationPaid {
            let localized = NSLocalizedString("PreReservation Rules Reservation %@", comment: "")
            return String(format: localized, reservationRulesDataView.reservationNumber)
        } else {
            let localized = NSLocalizedString("Reservation Rules Reservation %@", comment: "")
            return String(format: localized, reservationRulesDataView.reservationNumber)
        }
    }
    
    private func checkPaid() -> Bool {
        return reservationRulesDataView.reservationPaid
    }
    
    private func returnDueDate() -> Date? {
        return reservationRulesDataView.dueDate
    }
    
    private func checkDateExpired() -> Bool {
        
        if reservationRulesDataView.express, let dueDate = reservationRulesDataView.dueDate {
            return dueDate.timeIntervalSince(Date()) <= 0
        }
        return false
    }
}

