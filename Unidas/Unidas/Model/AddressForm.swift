//
//  AddressForm.swift
//  Unidas
//
//  Created by Mateus Padovani on 08/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct AddressForm {
    let neighborhood: String
    let street: String
    let number: String
    let complement: String?
    let residencialPhone: String?
    let zipcode: Zipcode
}
