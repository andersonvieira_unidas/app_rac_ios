//
//  Message.swift
//  Unidas
//
//  Created by Mateus Padovani on 15/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Message: Codable {
    let id: Int
    let createdAt: String?
    let dateSend: String?
    let read: Int
    let subject: String?
    let message: String
    let type: String
    let date: String
    let title: String
    
    enum CodingKeys: String, CodingKey {
        case id = "idMensagem"
        case createdAt = "MensagemInclusaoData"
        case dateSend = "MensagemEnviadaData"
        case read = "mensagemLida"
        case subject = "Assunto"
        case message = "mensagem"
        case type = "tipo"
        case date = "data"
        case title = "titulo"
    }
}
