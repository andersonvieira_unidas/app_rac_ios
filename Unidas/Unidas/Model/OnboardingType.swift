//
//  OnboardingType.swift
//  Unidas
//
//  Created by Anderson Vieira on 26/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

public enum OnboardingType {
    case introduction, howToRent
    
    var backgroundColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    var titleColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        }
    }
    
    var subtitleColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        }
    }
    
    var textColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        }
    }
    
    var pageIndicatorColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 0.7411764706, green: 0.7411764706, blue: 0.7411764706, alpha: 1)
        }
    }
    
    var currentPageIndicatorColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
        }
    }
    
    var buttonBackgroundColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        case .howToRent:
            return #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)
        }
    }
    
    var buttonBorderColor: UIColor {
        switch self {
        case .introduction:
            return #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        case .howToRent:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        }
    }
}
