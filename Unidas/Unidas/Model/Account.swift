//
//  Account.swift
//  Unidas
//
//  Created by Mateus Padovani on 18/05/2018.
//  Copyright © 2018 Unidas. All rights reserved.
//

import Foundation

struct Account: Codable {
    var id: Int?
    var user: Int?
    var documentNumber: String
    //var documentNumberHash: String?
    var fcm: String?
    var mobilePhone: String
    var email: String
    var name: String
    var companyName: String?
    var deviceId: String?
    var profileImage: URL?
    var birthDate: Date?
    var motherName: String?
    var pendencies: Bool?
    
    var documentNumberHash: String? {
        let utf8str = documentNumber.data(using: String.Encoding.utf8)
        if let base64DocumentNumber = utf8str?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: String.Encoding.utf8.rawValue)) {
            return base64DocumentNumber
        }
        return nil
    }
}

extension Account {
    init?(userUnidas: UserUnidas) {
        guard let documentNumber = userUnidas.clidoc,
            let mobilePhone = userUnidas.contaClienteLoginCelular?.description,
            let mobilePhoneFull = userUnidas.contaClienteLoginCelularDDD?.description.appending(mobilePhone),
            let email = userUnidas.contaClienteLoginEmail,
            let name = userUnidas.contaClienteNome//,
           // let utf8str = documentNumber.data(using: String.Encoding.utf8)//,
            //let documentNumberHash = utf8str.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: String.Encoding.utf8.rawValue)) as? String
            else {
            return nil
        }
        self.id = userUnidas.contaClienteId
        self.documentNumber = documentNumber
        //self.documentNumberHash = documentNumberHash
        self.user = nil
        self.fcm = nil
        self.mobilePhone = mobilePhoneFull
        self.email = email
        self.name = name
        self.companyName = nil
        self.deviceId = userUnidas.contaClienteLoginCelularDeviceID
        self.profileImage = nil
        self.birthDate = nil
        self.motherName = userUnidas.contaClienteNomeMae
        self.pendencies = nil
    }
}
