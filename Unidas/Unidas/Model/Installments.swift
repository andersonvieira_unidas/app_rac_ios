//
//  Installments.swift
//  Unidas
//
//  Created by Mateus Padovani on 19/06/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

struct Installments: Codable {
    let installments: String
    
    enum CodingKeys: String, CodingKey {
        case installments = "PARCELAS"
    }
}
