//
//  ConfigToggle.swift
//  Unidas
//
//  Created by Anderson Vieira on 18/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import Foundation

class ConfigToggle {
    var payInAdvance: Bool = true
    var payAtPickup: Bool = true
    
    init(payInAdvance: Bool, payAtPickup: Bool) {
        self.payInAdvance = payInAdvance
        self.payAtPickup = payAtPickup
    }
}
