//
//  Onboard.swift
//  Unidas
//
//  Created by Anderson Vieira on 27/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct Onboard: Decodable {
    let title: String?
    let step: Int?
    let subtitle: String?
    let description: String?
    let urlImage: URL?
    let helpContent: [OnboardHelpContent]?
    
    enum CodingKeys: String, CodingKey {
        case title
        case step
        case subtitle
        case description
        case urlImage = "url_image_ios"
        case helpContent = "help_content"
  
    }
}
