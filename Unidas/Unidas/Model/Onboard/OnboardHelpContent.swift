//
//  OnboardHelpContent.swift
//  Unidas
//
//  Created by Anderson Vieira on 27/03/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation

struct OnboardHelpContent: Decodable {
    let step: Int?
    let description: String?
    let ulrIcon: URL?
    
    enum CodingKeys: String, CodingKey {
        case step
        case description
        case ulrIcon = "url_icon_ios"
    }
}
