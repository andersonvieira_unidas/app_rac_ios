//
//  SessionManager+UIKit.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 09/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

extension SessionManager {
    
    func requestLoginForExpiredSession() {
        let app = UIApplication.shared
        let currentViewController = app.keyWindow?.rootViewController
        let title = NSLocalizedString("Your session has expired", comment: "Expired session alert title")
        let message = NSLocalizedString("Login again to keep using the app with your account", comment: "Expired session alert message")
        
        let image = #imageLiteral(resourceName: "icon-personal-data-menu")
        let simpleAlert = SimpleAlert(title: title, subTitle: message, image: image)
        
        simpleAlert.whenButtonIsClicked { 
            let storyboard = UIStoryboard(name: "Login", bundle: .main)
            
            if let viewController = storyboard.instantiateInitialViewController() {
                viewController.modalPresentationStyle = .fullScreen
                currentViewController?.present(viewController, animated: true, completion: nil)
            }
        }
        
        simpleAlert.show(animated: true)
        addEventSessionEnd(success: true)
        logout()
    }
    
    func addEventSessionEnd(success: Bool) {
        
        let firebaseAnalyticsService = FirebaseAnalyticsService()
        
        var parameters: [String : Any] = [:]
        parameters["success"] = success
        firebaseAnalyticsService.addEvent(withEvent: AnalyticsEvents.sessionEnd, withValue: parameters)
    }
}

