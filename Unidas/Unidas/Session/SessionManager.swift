//
//  SessionManager.swift
//  Unidas
//
//  Created by Mateus Padovani on 14/03/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import JWTDecode

let session = SessionManager.shared

class SessionManager {
    static let shared = SessionManager()
    
    private var userDefaults = UserDefaults.standard
    
    private init() {
        if let data = userDefaults.data(forKey: "currentUser"), let user = try? JSONDecoder().decode(UserResponse.self, from: data) {
            currentUser = user
        }
        if let vanRequestId = userDefaults.string(forKey: "vanRequestId") {
            self.vanRequestId = vanRequestId
        }
        if let deliveryRequestId = userDefaults.string(forKey: "deliveryRequestId") {
            self.deliveryRequestId = deliveryRequestId
        }
    }
    
    var callCenter: CallCenterResponse? {
        set {
            if let callCenter = newValue {
                if let encoded = try? JSONEncoder().encode(callCenter) {
                    userDefaults.set(encoded, forKey: "callCenter")
                }
            }
            else {
                userDefaults.set(nil, forKey: "callCenter")
            }
        }
        get {
            if let objects = userDefaults.value(forKey: "callCenter") as? Data {
                let callCenterDecoded = try? JSONDecoder().decode(CallCenterResponse.self, from: objects)
                
                return callCenterDecoded
            }
            
            return nil
        }
    }
    
    var currentUser: UserResponse? {
        didSet {
            if let currentUser = currentUser {
                if let encoded = try? JSONEncoder().encode(currentUser) {
                    userDefaults.set(encoded, forKey: "currentUser")
                }
            }
            else {
                userDefaults.set(nil, forKey: "currentUser")
            }
        }
    }
    
    var vanRequestId: String? {
        didSet {
            if let vanRequestId = self.vanRequestId {
                userDefaults.set(vanRequestId, forKey: "vanRequestId")
            } else {
                userDefaults.removeObject(forKey: "vanRequestId")
            }
        }
    }
    
    var deliveryRequestId: String? {
        didSet {
            if let deliveryRequestId = self.deliveryRequestId {
                userDefaults.set(deliveryRequestId, forKey: "deliveryRequestId")
            } else {
                userDefaults.removeObject(forKey: "deliveryRequestId")
            }
        }
    }
    
    func validateSession() {
        let calendar = Calendar.current
        guard let tokenString = currentUser?.unidas?.first?.contaClienteToken,
            let token = try? decode(jwt: tokenString),
            let expiresAt = token.expiresAt,
            let nextThirtyMinutes = calendar.date(byAdding: .minute, value: 30, to: Date()), nextThirtyMinutes > expiresAt else { return }
        logout()
    }
    
    func login(user: UserResponse) {
        let notificationCenter = NotificationCenter.default
        currentUser = user
        if let token = user.token {
            ServiceConfiguration.unidas.headers?.add(name: "Authorization", value: token)
        }
        notificationCenter.post(name: .UNUserDidLogin, object: user)
    }
    
    func logout() {
        let notificationCenter = NotificationCenter.default
        currentUser = nil
        deliveryRequestId = nil
        ServiceConfiguration.unidas.headers?.remove(name: "Authorization")
        
        AppPersistence.clearData(withKey: AppPersistence.token)
        notificationCenter.post(name: .UNUserDidLogout, object: nil)
    }
    
}
