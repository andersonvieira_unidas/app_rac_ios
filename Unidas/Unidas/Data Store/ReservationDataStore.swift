//
//  ReservationDataStore.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 27/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum ReservationDataStoreError: LocalizedError {
    case unableToEncodeReservationRecoveryInformation, unableToDecodeReservationRecoveryInformation
}
protocol ReservationDataStore {
    func save(recoveryReservationInformation: RecoverReservationInformation) throws
    func fetchReservationRecoveryInformation() throws -> RecoverReservationInformation?
    func reset()
}

class ReservationDataStoreUserDefaults: ReservationDataStore {
    
    let userDefaults: UserDefaults
    let key = "reservationUUIDs"
    
    init() {
        userDefaults = UserDefaults.standard
    }
    
    func save(recoveryReservationInformation: RecoverReservationInformation) throws {
        guard let recoveryReservationInformationDictionary = recoveryReservationInformation.dictionaryRepresentation else {
            throw ReservationDataStoreError.unableToEncodeReservationRecoveryInformation
        }
        
        userDefaults.set(recoveryReservationInformationDictionary, forKey: key)
    }
    
    func fetchReservationRecoveryInformation() throws -> RecoverReservationInformation? {
        guard let dictionary = userDefaults.dictionary(forKey: key) else {
            return nil
        }
        
        guard let uuidString = dictionary["uuid"] as? String,
            let documentNumber = dictionary["documentNumber"] as? String,
            let uuid = UUID(uuidString: uuidString),
            let reserveNumber = dictionary["reserveNumber"] as? String else {
            throw ReservationDataStoreError.unableToDecodeReservationRecoveryInformation
        }
                
        return RecoverReservationInformation(uuid: uuid, documentNumber: documentNumber, reserveNumber: reserveNumber)
    }
    
    func reset() {
        userDefaults.removeObject(forKey: key)
    }
    
}
