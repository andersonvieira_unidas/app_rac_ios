//
//  VehicleDataStore.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 10/04/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

enum VehicleDataStoreError: LocalizedError {
    case unableToEncodeVehicle
    
    var errorDescription: String? {
        switch self {
        case .unableToEncodeVehicle: return NSLocalizedString("Unable to serialize vehicle", comment: "unable to serialize vehicle message")
        }
    }
}

protocol VehicleDataStore {
    func saveReserved(vehicle: Vehicle) throws
    func fetchReservedVehicle() throws -> Vehicle?
    func removeReserve()
}

class VehicleDataStoreUserDefaults: VehicleDataStore {
    
    let userDefaults: UserDefaults
    let key = "vehicle"
    
    init() {
        userDefaults = .standard
    }
    
    func removeReserve() {
        userDefaults.removeObject(forKey: key)
    }
    
    func saveReserved(vehicle: Vehicle) throws {
        guard let dictionary = vehicle.dictionaryRepresentation else {
            throw VehicleDataStoreError.unableToEncodeVehicle
        }
        userDefaults.set(dictionary, forKey: key)
    }
    
    func fetchReservedVehicle() throws -> Vehicle? {
        guard let dictionary = userDefaults.dictionary(forKey: key) else {
            return nil
        }
        let json = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        let decoder = JSONDecoder()
        return try decoder.decode(Vehicle.self, from: json)
    }
    
}
