//
//  ListCardCollectionViewLayout.swift
//  Unidas
//
//  Created by Mateus Padovani on 26/04/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class ListCardCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func prepare() {
        super.prepare()
        setupCollectionViewLayout(collectionView!)
    }
    
    func setupCollectionViewLayout(_ collectionView: UICollectionView) {
        let spacing: CGFloat = 16.0
        scrollDirection = .vertical
        minimumLineSpacing = spacing
        minimumInteritemSpacing = spacing
        sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        itemSize = self.itemSize(for: collectionView)
    }
    
    func set(headerReferenceSize: CGSize) {
        self.headerReferenceSize = headerReferenceSize
    }
    
    func itemSize(for collectionView: UICollectionView) -> CGSize {
        let safeWidth: CGFloat
        if #available(iOS 11.0, *) {
            safeWidth = UIScreen.main.bounds.width - collectionView.safeAreaInsets.right - collectionView.safeAreaInsets.left
        } else {
            safeWidth = UIScreen.main.bounds.width
        }
        
        let aspectRatio: CGFloat = 184.0 / 343.0
        let itemWidth = safeWidth - sectionInset.left - sectionInset.right
        let itemHeight: CGFloat = itemWidth * aspectRatio
        return CGSize(width: itemWidth, height: itemHeight)
    }
}
