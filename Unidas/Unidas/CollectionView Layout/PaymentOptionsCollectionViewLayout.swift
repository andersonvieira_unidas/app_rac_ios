//
//  PaymentOptionsCollectionViewLayout.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 14/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

class PaymentOptionsCollectionViewLayout: UICollectionViewFlowLayout {
    
    override func prepare() {
        super.prepare()
        let spacing: CGFloat = 8
        sectionInset = UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
        minimumInteritemSpacing = spacing
        minimumLineSpacing = spacing
        guard let collectionViewWidth = collectionView?.frame.width else { return }
        itemSize = CGSize(width: collectionViewWidth - (sectionInset.left + sectionInset.right), height: 100)
    }    
}
