//
//  UIFontExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

extension UIFont {
    private static func customFont(name: String, size: CGFloat) -> UIFont {
        let font = UIFont(name: name, size: size)
        assert(font != nil, "Can't load font: \(name)")
        return font ?? UIFont.systemFont(ofSize: size)
    }
    
    static func regular(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Roboto-Regular", size: size)
    }
    
    static func medium(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Roboto-Medium", size: size)
    }
    
    static func bold(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Roboto-Bold", size: size)
    }
    
    static func light(ofSize size: CGFloat) -> UIFont {
        return customFont(name: "Roboto-Light", size: size)
    }
}

