//
//  UIAlertActionExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 29/05/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

extension UIAlertAction {
    
    static func ok(_ handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        let title = NSLocalizedString("OK", comment: "OK alert action title")
        return UIAlertAction(title: title, style: .default, handler: handler)
    }
    
    static func dismiss(_ handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        let title = NSLocalizedString("Dismiss", comment: "Dismiss alert action title")
        return UIAlertAction(title: title, style: .default, handler: handler)
    }
    
    static func cancel(_ handler: ((UIAlertAction) -> Void)? = nil) -> UIAlertAction {
        let title = NSLocalizedString("Cancel", comment: "Cancel alert action title")
        return UIAlertAction(title: title, style: .cancel, handler: handler)
    }
    
}
