//
//  UITabBar.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/06/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import UIKit

extension UITabBarController {
  func addBadge(index: Int, value: Int, color: UIColor, font: UIFont) {
    guard let tabBarItems = tabBar.items else { return }
    let itemPosition = CGFloat(index + 1)
    let itemWidth: CGFloat = tabBar.frame.width / CGFloat(tabBarItems.count)

    let bgColor = color

    let xOffset: CGFloat = 12
    let yOffset: CGFloat = -9

    let badgeView = PGTabBadge()
    badgeView.frame.size = CGSize(width: 17, height: 17)
    badgeView.center = CGPoint(x: (itemWidth * itemPosition) - (itemWidth / 2) + xOffset, y: 20 + yOffset)
    badgeView.layer.cornerRadius = badgeView.bounds.width / 2
    badgeView.clipsToBounds = true
    badgeView.textColor = UIColor.white
    badgeView.textAlignment = .center
    badgeView.font = font
    badgeView.text = String(value)
    badgeView.backgroundColor = bgColor
    badgeView.tag = index
    tabBar.addSubview(badgeView)
  }
}

class PGTabBadge: UILabel {}
