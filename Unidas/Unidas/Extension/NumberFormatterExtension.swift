//
//  NumberFormatterExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 09/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

extension NumberFormatter {
    static var currencyFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "pt-br")
        return numberFormatter
    }
    
    static var currencyFormatterWithouRounding: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = NumberFormatter.RoundingMode.down
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "pt-br")
        return numberFormatter
    }
    
    static var decimalFormatterWithouRounding: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.roundingMode = NumberFormatter.RoundingMode.halfUp
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.numberStyle = .decimal
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }
    
    static var percentFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.locale = Locale(identifier: "pt-br")
        return numberFormatter
    }
    
    static var decimalNumberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter
    }
    
    static var apiNumberFormatter: NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.groupingSeparator = ""
        numberFormatter.decimalSeparator = "."
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }
    
    static let percentNumberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .percent
        numberFormatter.maximumFractionDigits = 0
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.minimumIntegerDigits = 1
        return numberFormatter
    }()
  
}
