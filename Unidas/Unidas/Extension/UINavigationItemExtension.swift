//
//  UINavigationItemExtension.swift
//  Unidas
//
//  Created by Mateus Padovani on 16/02/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

extension UINavigationItem {
    func setBackButtonTitle(title: String) {
        let backItem = UIBarButtonItem()
        backItem.title = title
        backBarButtonItem = backItem
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.backBarButtonItem = UIBarButtonItem()
    }
}
