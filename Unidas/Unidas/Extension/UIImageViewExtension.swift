//
//  UIImageViewExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 21/02/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView {
    
    func setImage(withURL url: URL?, placeholderImage: UIImage?) {
        self.image = placeholderImage
        guard let url = url else { return }
        self.af.setImage(withURL: url, placeholderImage: placeholderImage)
    }
    
    func setImage(withURL url: URL?) {
        guard let url = url else { return }
        self.af.setImage(withURL: url)
    }
    
    func cancelImageRequest() {
        self.af.cancelImageRequest()
    }
    
    func setImageAvatar(withURL url: URL?,  borderColor: UIColor, borderWidth: Int = 2) {
        self.layer.cornerRadius = (self.frame.size.width) / 2
        self.clipsToBounds = true
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
        
        guard let url = url else { return }
        self.af.setImage(withURL: url)
    }
    
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
