//
//  NSAttributedString.swift
//  Unidas
//
//  Created by Anderson Vieira on 16/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString{
    static var linkAttributed: [NSAttributedString.Key: Any] = {
        return [
            .font: UIFont.bold(ofSize: 12),
            .foregroundColor: #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineColor:#colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()
    
    static var linkAttributedWhite: [NSAttributedString.Key: Any] = {
        return [
            .font: UIFont.bold(ofSize: 12),
            .foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
            .underlineColor:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue]
    }()

}
