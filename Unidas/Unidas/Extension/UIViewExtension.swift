//
//  UIViewExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 02/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Workaround for iOS 10 while animating hide inside a stack view
    /// - Parameter hidden: Set the hidden property only if different from current hidden value
    func setHidden(_ hidden: Bool) {
        if self.isHidden != hidden {
            self.isHidden = hidden
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        var cornerMask = UIRectCorner()
        if(corners.contains(.topLeft)){
            cornerMask.insert(.topLeft)
        }
        if(corners.contains(.topRight)){
            cornerMask.insert(.topRight)
        }
        if(corners.contains(.bottomLeft)){
            cornerMask.insert(.bottomLeft)
        }
        if(corners.contains(.bottomRight)){
            cornerMask.insert(.bottomRight)
        }
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    var saferAreaLayoutGuide: UILayoutGuide {
        get {
            if #available(iOS 11.0, *) {
                return self.safeAreaLayoutGuide
            } else {
                return self.layoutMarginsGuide
            }
        }
    }
    
}
