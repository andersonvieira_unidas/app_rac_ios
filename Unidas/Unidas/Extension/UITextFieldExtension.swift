//
//  UITextFieldExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 11/05/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

extension UITextField {
    func changeColorBorder(isOn: Bool) {
        if isOn {
            self.layer.cornerRadius = 4
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.red.cgColor
            self.layer.masksToBounds = true
        }else{
            self.layer.borderWidth = 0
        }
    }
}
