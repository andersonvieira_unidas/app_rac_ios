//
//  UIViewControllerExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 10/07/19.
//  Copyright © 2019 KeyCar. All rights reserved.
//

import UIKit

extension UIViewController {
    
    override open func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    func askPermissionToSettings(withMessage message: String) {
        
        let alertController = UIAlertController (title: NSLocalizedString("Settings Title", comment: ""), message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (_) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func alertError(errorMessage: String){
        let error = errorMessage.replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: ".", with: "")
        let title = NSLocalizedString("Error title", comment: "")
        
        let image = #imageLiteral(resourceName: "icon-alert-generic-general")
        let simpleAlert = SimpleAlert(title: title, subTitle: error, image: image)
        simpleAlert.show(animated: true)
    }
    
    func setHeaderUnidas(barTintColor: UIColor = UIColor.init(named: "background") ?? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)){
        if let navigationController = self.navigationController{
            
            let navigationBar = navigationController.navigationBar
            let v = UIView(frame: CGRect(x: 0, y: -UIApplication.shared.statusBarFrame.height, width: view.frame.width, height: UIApplication.shared.statusBarFrame.height + (navigationController.navigationBar.frame.height )))
            v.roundCorners([.bottomLeft, .bottomRight], radius: 25)
            v.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2274509804, blue: 0.4784313725, alpha: 1)
            let image = UIImage().image(fromView: v)
            
            if #available(iOS 13.0, *) {
                let appearance = UINavigationBarAppearance()
                appearance.configureWithOpaqueBackground()
                appearance.backgroundColor = barTintColor
                appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                appearance.backgroundImage = image
                appearance.shadowImage = UIImage()
                appearance.shadowColor = UIColor.clear
                
                navigationBar.standardAppearance = appearance
                navigationBar.scrollEdgeAppearance = appearance
                navigationBar.compactAppearance = appearance
                
            } else {
                navigationBar.setBackgroundImage(image, for: .default)
                navigationBar.barTintColor = barTintColor
                navigationBar.tintColor = UIColor.white
                navigationBar.shadowImage = UIImage()
                
                navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
            }
        }
    }
    
    var currentUser: UserResponse? {
        guard let currentUser = session.currentUser else { return nil }
        return currentUser
    }
    
    var isDarkMode: Bool {
        if #available(iOS 13.0, *) {
            return self.traitCollection.userInterfaceStyle == .dark
        }
        else if self.isDarkMode {
            return true
        }
        return false
    }
}
