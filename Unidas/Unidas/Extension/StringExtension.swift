//
//  StringExtension.swift
//  Unidas
//
//  Created by Mateus Padovani on 22/06/2018.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation
import PhoneNumberKit
import InputMask

extension String {
    var isValidCPF: Bool {
        let numbers = self.compactMap({Int(String($0))})
        guard numbers.count == 11 && Set(numbers).count != 1 else { return false }
        let soma1 = 11 - ( numbers[0] * 10 +
            numbers[1] * 9 +
            numbers[2] * 8 +
            numbers[3] * 7 +
            numbers[4] * 6 +
            numbers[5] * 5 +
            numbers[6] * 4 +
            numbers[7] * 3 +
            numbers[8] * 2 ) % 11
        let dv1 = soma1 > 9 ? 0 : soma1
        let soma2 = 11 - ( numbers[0] * 11 +
            numbers[1] * 10 +
            numbers[2] * 9 +
            numbers[3] * 8 +
            numbers[4] * 7 +
            numbers[5] * 6 +
            numbers[6] * 5 +
            numbers[7] * 4 +
            numbers[8] * 3 +
            numbers[9] * 2 ) % 11
        let dv2 = soma2 > 9 ? 0 : soma2
        return dv1 == numbers[9] && dv2 == numbers[10]
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self)
    }
    
    var isValidPhoneNumber: Bool {
        let phoneNumberKit = PhoneNumberKit()
        let phoneNumber = try? phoneNumberKit.parse("+55\(self)")
        return phoneNumber != nil
    }
    
    var areaCode: Int? {
        let validPhoneStr = self.removePhoneCharacters
        guard (10...11).contains(validPhoneStr.count) else { return nil }
        let areaCodeEndingIndex = validPhoneStr.index(validPhoneStr.startIndex, offsetBy: 2)
        return Int(validPhoneStr[..<areaCodeEndingIndex])!
    }
    
    var phoneNumber: Int? {
        let validPhoneStr = self.removePhoneCharacters
        guard (10...11).contains(validPhoneStr.count) else { return nil }
        let areaCodeEndingIndex = validPhoneStr.index(validPhoneStr.startIndex, offsetBy: 2)
        return Int(validPhoneStr[areaCodeEndingIndex..<validPhoneStr.endIndex])!
    }
    
    var removeSpecialChars: String {
        let removeAccents = self.folding(options: .diacriticInsensitive, locale: .current)
        let badchar = CharacterSet(charactersIn: "\"[]()$,")
        return removeAccents.components(separatedBy: badchar).joined()
    }
    
    var eventParameter: String {
        let normalized = removeSpecialChars
        return normalized.replacingOccurrences(of: " ", with: "").lowercased()
    }
    
    var removePhoneCharacters: String {
        var returnValue = self.replacingOccurrences(of: " ", with: "")
        returnValue = returnValue.replacingOccurrences(of: "(", with: "")
        returnValue = returnValue.replacingOccurrences(of: ")", with: "")
        returnValue = returnValue.replacingOccurrences(of: "-", with: "")
        return returnValue
    }
    
    var removeZipcodeCharacters: String {
        let returnValue = self.replacingOccurrences(of: "-", with: "")
        return returnValue
    }
    
    var removeDocumentNumberCharacters: String {
        var returnValue = self.replacingOccurrences(of: ".", with: "")
        returnValue = returnValue.replacingOccurrences(of: "-", with: "")
        
        return returnValue
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    func formattedResidencePhoneNumber() -> String {
        guard let mask = try? Mask(format: "([00]) [00000]-[0000]") else { return self }
        let toText = CaretString(string: self, caretPosition: self.endIndex, caretGravity: .forward(autocomplete: true))
        return mask.apply(toText: toText).formattedText.string
    }
    
    func formattedMobilePhoneMaskNumber() -> String {
        let mobilePhoneFormatted = self.formattedResidencePhoneNumber()
        let mobilePhoneFirst = String(mobilePhoneFormatted.prefix(7))
        let mobilePhoneLast  = String(mobilePhoneFormatted.suffix(2))
        return "\(mobilePhoneFirst)*****\(mobilePhoneLast)"
    }
    
    func formattedEmailMaskNumber() -> String {
        let components = self.lowercased().components(separatedBy: "@")
        return hideMidChars(components.first!) + "@" + components.last!
    }
    
    private func hideMidChars(_ value: String) -> String {
        return String(value.enumerated().map { index, char in
            return [0, 1, value.count - 1, value.count - 2].contains(index) ? char : "*"
        })
    }
    
    var nsRange : NSRange {
        return NSRange(self.startIndex..., in: self)
    }
    
    func lowerCapitalize() -> String {
        return self.lowercased().capitalizingFirstLetter()
    }
}
