//
//  UIColorExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 17/07/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit

extension UIColor {
    static var themeButton = #colorLiteral(red: 0.262745098, green: 0.5019607843, blue: 0.8980392157, alpha: 1)
    static var themeImportantButton = #colorLiteral(red: 1, green: 0.6039215686, blue: 0.0862745098, alpha: 1)
    static var themeTitle = #colorLiteral(red: 0.3254901961, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
    static var themeSubTitle = #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1)
    static var standardGray = #colorLiteral(red: 0.3098039216, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
}
