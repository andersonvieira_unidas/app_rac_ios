//
//  DoubleExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 31/08/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import Foundation

extension Double {
    func roundedFractionDigits(to numberOfFractionDigits: Int) -> Double {
        let base = pow(10, Double(numberOfFractionDigits))
        return (self * base).rounded() / base
    }
    
    func fractionDigits(to numberOfFractionDigits: Int) -> Double {
        let base = pow(10, Double(numberOfFractionDigits))
        return (self * base) / base
    }
}
