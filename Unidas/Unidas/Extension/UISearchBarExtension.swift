//
//  UISearchBarExtension.swift
//  Unidas
//
//  Created by Leonardo Oliveira on 06/03/18.
//  Copyright © 2018 KeyCar. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    private var searchField: UITextField? {
        return value(forKey: "searchField") as? UITextField
    }
    
    var textColor: UIColor? {
        set {
            searchField?.textColor = newValue
        }
        get {
            return searchField?.textColor
        }
    }
    
    var clearButtonMode: UITextField.ViewMode? {
        set {
            searchField?.clearButtonMode = newValue ?? .never
        }
        get {
            return searchField?.clearButtonMode
        }
    }
    
    func setPlaceholderTextColor(_ color: UIColor) {
        searchField?.attributedPlaceholder = NSAttributedString(string: searchField?.placeholder ?? "", attributes: [.foregroundColor : color])
        if let searchIconImageView = searchField?.leftView as? UIImageView {
            searchIconImageView.image = searchIconImageView.image?.withRenderingMode(.alwaysTemplate)
            searchIconImageView.tintColor = color
        }
    }
    
}
