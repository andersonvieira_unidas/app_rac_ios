//
//  EKCalendarExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 13/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import EventKit

extension EKCalendar {
    
    static var eventStore = EKEventStore()
    
    static var reminderCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = eventStore.calendars(for: .reminder).first(where: { $0.title == title }) else {
            
            let newCalendar = EKCalendar(for: .reminder, eventStore: eventStore)
            newCalendar.title = title
            newCalendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            newCalendar.source = eventStore.defaultCalendarForNewReminders()?.source
            
            try? eventStore.saveCalendar(newCalendar, commit: true)
            
            return newCalendar
        }
        return calendar
    }()
    
    static var eventCalendar: EKCalendar = {
        let title = NSLocalizedString("Unidas", comment: "")
        let predicate = NSPredicate(format: "title matches %@", title)
        
        guard let calendar = eventStore.calendars(for: .event).first(where: { $0.title == title }) else {
            let calendar = EKCalendar(for: .event, eventStore: eventStore)
            
            calendar.title = title
            calendar.cgColor = CGColor(#colorLiteral(red: 0.003921568627, green: 0.431372549, blue: 0.6549019608, alpha: 1))
            calendar.source = eventStore.defaultCalendarForNewEvents?.source
            
            try? eventStore.saveCalendar(calendar, commit: true)
            
            return calendar
        }
        return calendar
    }()
}

