//
//  UINavigationBar.swift
//  Unidas
//
//  Created by Anderson Vieira on 30/07/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func shouldRemoveShadow(_ value: Bool) -> Void {
        if value {
            self.setValue(true, forKey: "hidesShadow")
        } else {
            self.setValue(false, forKey: "hidesShadow")
        }
    }
}
