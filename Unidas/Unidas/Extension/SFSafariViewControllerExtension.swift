//
//  SFSafariViewControllerExtension.swift
//  Unidas
//
//  Created by Anderson Vieira on 24/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import SafariServices

extension SFSafariViewController {
    func clearCache(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                let calendar = Calendar.current
                if let oneYearAgo = calendar.date(byAdding: .year, value: 1, to: Date()){
                    HTTPCookieStorage.shared.deleteCookie(cookie)
                    HTTPCookieStorage.shared.removeCookies(since:oneYearAgo)
                }
            }
        }
    }
}
