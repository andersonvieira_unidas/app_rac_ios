//
//  XCTestCaseExtension.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 12/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
@testable import Unidas

private class BundleIdentifyingClass { }

extension XCTestCase {
    
    func getInstanceFromJsonByFileName<T: Decodable>(_ fileName: String,
                                                            decodeTo: T.Type) -> T? {
        var result: T? = nil
        let bundle = Bundle(for: BundleIdentifyingClass.self)
        
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        
        if let path = bundle.url(forResource: fileName, withExtension: "json") {
            if let data = try? Data(contentsOf: path) {
                guard let json = try? decoder.decode(T.self, from: data) else {
                    result = nil
                    return result
                }
                result = json
            } else {
                result = nil
            }
        } else {
            result = nil
        }
        return result
    }
    
    func compareDate(withInitialDate: Date, withEndDate: Date) -> Bool{
        let calendar = Calendar.current
        
        let date1Components = calendar.dateComponents([.year, .month, .day], from: withInitialDate)
        let date2Components = calendar.dateComponents([.year, .month, .day], from: withEndDate)
        
        let isSameMonth = date1Components.month == date2Components.month
        let isSameDay = date1Components.day == date2Components.day
        let isSameYear = date1Components.year == date2Components.year
        
        return (isSameMonth && isSameDay && isSameYear)
    }
    
    func loginUser() {
        let user = getInstanceFromJsonByFileName("user_login_object", decodeTo: UserUnidas.self)
        let userResponse = UserResponse(userUnidas: user!)
        SessionManager.shared.login(user: userResponse!)
    }
    
    func logoutUser() {
        SessionManager.shared.logout()
    }
}
