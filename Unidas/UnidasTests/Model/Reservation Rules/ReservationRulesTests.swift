//
//  ReservationRulesTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 06/04/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ReservationRulesTests: XCTestCase {
    
    private func loadModel(nameOfFile file: String) -> HomeDataView {
        let home = getInstanceFromJsonByFileName(file, decodeTo: Home.self)
        return HomeDataView(model: home!)
    }
    
    /* Status */
    func testShouldStatusReservation()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_complete_account")
        let reservationRulesDataView = makeReservationDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
        let reservationStep = reservationRules.make()
        
        expect(reservationStep.status).to(contain(ReservationStatus.reservation))
        expect(reservationStep.status).toNot(contain(ReservationStatus.paid))
        expect(reservationStep.status).toNot(contain(ReservationStatus.checkin))
    }
    
    func testShouldStatusPaid()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_check_at_count")
        let reservationRulesDataView = makeReservationDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRulesFactory().constructRule(dataView: reservationRulesDataView)
        let reservationStep = reservationRules.make()
        
        expect(reservationStep.status).to(contain(ReservationStatus.reservation))
        expect(reservationStep.status).to(contain(ReservationStatus.paid))
        expect(reservationStep.status).toNot(contain(ReservationStatus.checkin))
    }
    /*
    func testShouldStatusCheckin()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_show_qrcode_pickup")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        expect(reservationStep.status).to(contain(ReservationStatus.reservation))
        expect(reservationStep.status).to(contain(ReservationStatus.paid))
        expect(reservationStep.status).to(contain(ReservationStatus.checkin))
    }
    
    /* Title */
    func testShouldTitleReservation()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_complete_account")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation Done", comment: "")
        expect(reservationStep.title).to(equal(expected))
    }
    
    func testShouldTitlePaid()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_check_at_count")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation Payment Done", comment: "")
        expect(reservationStep.title).to(equal(expected))
    }
    
    func testShouldTitleCheckin()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_show_qrcode_pickup")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation Check-in", comment: "")
        expect(reservationStep.title).to(equal(expected))
    }
    
    func testShouldReservation() {
        let reservationNumber = "19499473"
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_show_qrcode_pickup")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let localized = NSLocalizedString("Reservation Rules Reservation %@", comment: "")
        let expected = String(format: localized, reservationNumber)
        
        expect(reservationStep.reservation).to(equal(expected))
    }
    
    /* Description */
    func testShouldCompleteYourAccountDescription()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_complete_account")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Complete your account", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldAntecipateYourPaymentNewUserDescription()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_antecipate_payment")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Antecipate your payment", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldAntecipateYourPaymentBaseUserDescription()  {
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_antecipate_payment")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Antecipate your payment", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldCheckinAtTheCountDescription()  {
        let homeDataView = loadModel(nameOfFile: "home_reservation_newUser_check_at_count")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Check-in at the count", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldCheckinExpediteYourPickup()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_expedite_pickup")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Check-in and expedite your pickup", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldCheckin24HoursBefore()  {
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_checkin_hours_before")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let localized = NSLocalizedString("Reservation Rules Reservation - Check-in in %@ hours before", comment: "")
        let expected = String(format: localized, String(24))
        
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldCheckinExpediteYourPickupWithPayment()  {
        
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_expedite_pickup_with_payment")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Check-in and expedite your pickup", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    func testShouldShowYourQRCodeInPickup()  {
        let homeDataView = loadModel(nameOfFile: "home_reservation_baseUser_show_qrcode_pickup")
        let reservationRulesDataview = ReservationRulesDataView(homeDataView: homeDataView)
        let reservationRules = ReservationRules(reservationRulesDataView: reservationRulesDataview)
        let reservationStep = reservationRules.make()
        
        let expected = NSLocalizedString("Reservation Rules Reservation - Show your QRCode in pickup", comment: "")
        expect(reservationStep.description).to(equal(expected))
    }
    
    */
    private func makeReservationDataView(homeDataView: HomeDataView) -> ReservationRulesDataView {
        let reservationRulesDataView = ReservationRulesDataView(userValidated: homeDataView.userValidated,
                                                                reservationNumber: homeDataView.reservationNumber,
                                                                checkinAvailable: homeDataView.checkinAvailable,
                                                                reservationPaid: homeDataView.reservationPaid,
                                                                hoursToCheckin: homeDataView.hoursToCheckin,
                                                                userAvailableToCheckin: homeDataView.userAvailableToCheckin,
                                                                preAuthorizationPaid: homeDataView.preAuthorizationPaid,
                                                                isFranchise: homeDataView.isFranchise,
                                                                dueDate: homeDataView.dueDate,
                                                                dueTime: homeDataView.dueTime,
                                                                express: homeDataView.express,
                                                                financialPendency: homeDataView.financialPendency)
        return reservationRulesDataView
    }
}
