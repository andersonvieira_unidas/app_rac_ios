//
//  ContractDetailListItemDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 20/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class ContractDetailListItemDataViewTests: XCTestCase {
    
    var model: ContractDetailListItemDataView!
    
    override func setUp() {
        let contractDetailListItem = ContractDetailListItem(description: "DESCRIPTION WITH SPACES", value: nil, observation: "OBSERVATION")
        model = ContractDetailListItemDataView(model: contractDetailListItem)
    }
    
    // MARK : computed -> description
    func testShouldReturnDescriptionContainsValue() {
        let expected = "DESCRIPTION WITH SPACES"
        expect(self.model.description).to(equal(expected))
    }
    
    func testShouldReturnNilDescriptionContainsNilValue() {
        let contractDetailListItem = ContractDetailListItem(description: nil, value: nil, observation: nil)
        let modelCopy = ContractDetailListItemDataView(model: contractDetailListItem)
        expect(modelCopy.description).to(beNil())
    }
    
    // MARK : computed -> observation
    func testShouldReturnBoservationContainsValue() {
        let expected = "OBSERVATION"
        expect(self.model.observation).to(equal(expected))
    }
    
    func testShouldReturnNilObservationContainsNilValue() {
        let contractDetailListItem = ContractDetailListItem(description: nil, value: nil, observation: nil)
        let modelCopy = ContractDetailListItemDataView(model: contractDetailListItem)
        expect(modelCopy.observation).to(beNil())
    }
}
