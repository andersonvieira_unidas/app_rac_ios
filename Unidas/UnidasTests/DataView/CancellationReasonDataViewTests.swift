//
//  CancellationReasonDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 30/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class CancellationReasonDataViewTests: XCTestCase {
    
    var model: CancellationReasonDataView!
    
    override func setUp() {
        let cancellationReason = CancellationReason(id: 1, motivoUsuario: "USER REASON", motivoInterno: "INTERNAL REASON")
        model = CancellationReasonDataView(model: cancellationReason)
    }
    
    // MARK : computed -> reasonsDescription
    func testShouldReturnReasonsDescriptionContainsValue() {
        let expected = "User Reason"
        expect(self.model.reasonsDescription).to(equal(expected))
    }

}
