//
//  AirlineInformationDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 10/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class AirlineInformationDataViewTests: XCTestCase {
    
    // MARK : computed -> description
    func testShouldAirlineDescriptionWhenModelIsCorrect() {
        let company = FlightCompany(code: "LAT", description: "LATAM")
        let airline = AirlineInformation(flightNumber: "AA123", company: company)
        let model = AirlineInformationDataView(model: airline)

        let localizedFormatted = NSLocalizedString("Flight: %@, Company: %@", comment: "Airline information description")
        let expected = String(format: localizedFormatted, "AA123", "LATAM")
        
        expect(model.description).to(equal(expected))
    }
}
