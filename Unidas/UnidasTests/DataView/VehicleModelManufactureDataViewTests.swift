//
//  VehicleModelManufactureDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class VehicleModelManufactureDataViewTests: XCTestCase {

    // MARK : computed -> name
    func testNameWhenModelContainsValue() {
        let expected = "Name Model"
        let vehicleModelManufacture = VehicleModelManufacture(id: 123456, name: "Name Model")
        let model = VehicleModelManufactureDataView(model: vehicleModelManufacture)
        
        expect(model.name).to(equal(expected))
    }
}
