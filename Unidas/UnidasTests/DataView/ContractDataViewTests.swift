//
//  ContractDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ContractDataViewTests: XCTestCase {
    
    var model: ContractDataView!
    
    override func setUp() {
        let contract = getInstanceFromJsonByFileName("contract_view_object", decodeTo: Contract.self)
        model = ContractDataView(model: contract!)
    }
    
    // MARK : computed -> formattedContract
    func testFormattedContractWhenModelContainsValue() {
        let localizedContract = NSLocalizedString("Contract %@", comment: "Formatted contract number")
        let expected = String(format: localizedContract, "17275285")
        expect(self.model.formattedContract).to(equal(expected))
    }
    
    // MARK : computed -> pickUpDate
    func testPickUpDateWhenModelContainsValue() {
        let dateExpected = "2019-09-18T00:00:00"
        let expected = DateFormatter.dateFormatter(withFormat: "YYYY-MM-dd'T'HH:mm:ss").date(from: dateExpected)
    
        expect(self.compareDate(withInitialDate: self.model.pickUpDate!, withEndDate: expected!)).to(beTrue())
    }
    
    // MARK : computed -> returnDate
    func testReturnDateWhenModelContainsValue() {
        let dateExpected = "2019-09-19T00:00:00"
        let expected = DateFormatter.dateFormatter(withFormat: "YYYY-MM-dd'T'HH:mm:ss").date(from: dateExpected)
        
        expect(self.compareDate(withInitialDate: self.model.returnDate!, withEndDate: expected!)).to(beTrue())
    }
    
    // MARK : computed -> pickUpGarageDescription
    func testPickUpGarageDescriptionWhenModelContainsValue() {
        let expected = "Paulista"
        expect(self.model.pickUpGarageDescription).to(equal(expected))
    }
    
    // MARK : computed -> returnGarageDescription
    func testReturnGarageDescriptionWhenModelContainsValue() {
        let expected = "Paulista"
        expect(self.model.returnGarageDescription).to(equal(expected))
    }
    
    // MARK : computed -> fullRentTimeInterval
    func testFullRentTimeIntervalWhenModelContainsValue() {
        let expected: TimeInterval = 54000.0
        expect(self.model.fullRentTimeInterval).to(equal(expected))
    }
    
    // MARK : computed -> formattedDateInterval
    func testFormattedDateIntervalWhenModelContainsValue() {
        let expected = "18 de setembro de 2019 - 19 de setembro de 2019"
        expect(self.model.formattedDateInterval).to(equal(expected))
    }
    
    // MARK : computed -> pickupGeolocation
    func testPickupGeolocationWhenModelContainsValue() {
        expect(self.model.pickupGeolocation).toNot(beNil())
    }
    
    // MARK : computed -> returnGeolocation
    func testReturnGeolocationWhenModelContainsValue() {
        expect(self.model.returnGarageDescription).toNot(beNil())
    }
    
    // MARK : computed -> returnPatioGeolocalization
    func testReturnPatioGeolocalizationWhenModelContainsValue() {
        expect(self.model.returnPatioGeolocalization).toNot(beNil())
    }
    
    // MARK : computed -> vehicle
    func testVehicleWhenModelContainsValue() {
        expect(self.model.vehicle).toNot(beNil())
    }
    
    // MARK : computed -> formattedPickUpLocation
    func testFormattedPickUpLocationWhenModelContainsValue() {
        let localizedString = NSLocalizedString("at %@", comment: "Location Store")
        let expected = String(format: localizedString, model.pickUpGarageDescription!)
        
        expect(self.model.formattedPickUpLocation).to(equal(expected))
    }
    
    // MARK : computed -> pickUpDateFormatted
    func testPickUpDateFormattedWhenModelContainsValue() {
        let expected = "18 de setembro às 18:45"
        expect(self.model.pickUpDateFormatted).to(equal(expected))
    }
    
    // MARK : computed -> formattedReturnLocation
    func testFormattedReturnLocationWhenModelContainsValue() {
        let expected = "em Paulista"
        expect(self.model.formattedReturnLocation).to(equal(expected))
    }
    
    // MARK : computed -> returnDateFormatted
    func testReturnDateFormattedWhenModelContainsValue() {
        let expected = "19 de setembro às 09:45"
        expect(self.model.returnDateFormatted).to(equal(expected))
    }
    
    // MARK : computed -> isHiddenVehicleInformation
    func testIsHiddenVehicleInformationWhenModelContainsValue() {
        expect(self.model.isHiddenVehicleInformation).to(beTrue())
    }
    
    // MARK : computed -> groupCode
    func testGroupCodeWhenModelContainsValue() {
        let localized = NSLocalizedString("%@ Group", comment: "Group code label")
        let expected = String(format: localized, model.model.group!)
        
        expect("\(self.model.groupCode!) ").to(equal(expected))
    }
    
    // MARK : computed -> groupDescription
    func testGroupDescriptionWhenModelContainsValue() {
        let expected = "HATCH ECONOMICO 1.0"
        expect(self.model.groupDescription).to(equal(expected))
    }
    
    // MARK : computed -> groupVehicles
    func testGroupVehiclesWhenModelContainsValue() {
        let expected = "Renault Kwid, Fiat Mobi"
        expect(self.model.groupVehicles).to(equal(expected))
    }
    
    // MARK : computed -> qrCodeInformation
    func testQrCodeInformationWhenModelContainsValue() {
        let expected = "17275285|16537798|31662308825    "
        expect(self.model.qrCodeInformation).to(equal(expected))
    }
    
    // MARK : computed -> pickUpObservation
    func testPickUpObservationWhenModelContainsValue() {
        let expected = "Observação retirada"
        expect(self.model.pickUpObservation).to(equal(expected))
    }
    
    // MARK : computed -> returnGarageObservation
    func testReturnGarageObservationWhenModelContainsValue() {
        expect(self.model.returnObservation).to(beNil())
    }
}
