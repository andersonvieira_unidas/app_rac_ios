//
//  CreditCardReservePaymentDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class CreditCardReservePaymentDataViewTests: XCTestCase {

    var model: CreditCardReservePaymentDataView!
    
    override func setUp() {
        let listCard = getInstanceFromJsonByFileName("credit_card_object", decodeTo: ListCard.self)
        let creditCardReservePayment = CreditCardReservePayment(card: listCard!, amount: 100.0, securityCode: "234", installmentsOption: 1)
        model = CreditCardReservePaymentDataView(model: creditCardReservePayment)
    }
    
    private func createObject() -> CreditCardReservePaymentDataView{
        let listCard = getInstanceFromJsonByFileName("credit_card_object", decodeTo: ListCard.self)
        let creditCardReservePayment = CreditCardReservePayment(card: listCard!, amount: 100.0, securityCode: "234", installmentsOption: 1)
        return CreditCardReservePaymentDataView(model: creditCardReservePayment)
    }

    // MARK : computed -> card
    func testCardWhenModelContainsValue() {
        expect(self.model.card).toNot(beNil())
    }
    
    // MARK : computed -> installmentsOption
    func testInstallmentsOptionWhenModelContainsValue() {
        let expected = "1"
        expect(self.model.installmentsOption).to(equal(expected))
    }
    
    // MARK : computed -> installmentValue
    func testInstallmentValueWhenModelContainsValue() {
        let expected = "R$ 100,00"
        expect(self.model.installmentValue).to(equal(expected))
    }
    
    // MARK : computed -> installmentDescription
    func testInstallmentDescriptionWhenModelContainsValue() {
        let expected = "1x R$ 100,00"
        expect(self.model.installmentDescription).to(equal(expected))
    }
}
