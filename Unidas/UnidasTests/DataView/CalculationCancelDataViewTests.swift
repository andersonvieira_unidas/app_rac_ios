//
//  CalculationCancelDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class CalculationCancelDataViewTests: XCTestCase {
    
    var model: CalculationCancelDataView!
    
    override func setUp() {
        let calculationReservation = CalculationCancelReservation(antecipatedDays: 1, porcentage: 0.50, returnValue: 900, paidValue: 1000.0)
        let calculationCancel = CalculationCancel(reservation: [calculationReservation])
        model = CalculationCancelDataView(model: calculationCancel)
    }
    
    // MARK : computed -> formattedDiscount
    func testShouldFormattedDiscountWhenModelContainsValue() {
        let expected = "R$ 900,00"
        let calculation = model.formattedDiscount()
        expect(calculation).to(equal(expected))
    }

    func testShouldFormattedDiscountFragmentWhenModelContainsValue() {
        let expected = "R$ 900,00"
        let calculation = model.formattedDiscount()
        expect(calculation).to(equal(expected))
    }
}
