//
//  ListCardDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ListCardDataViewTests: XCTestCase {
    
    private func createModel(val: String?) -> ListCardDataView {
        let listCard = ListCard(metodoPagamentoTipoid: 1, metodoPagamentoTipoDescricao: "", pan: "", openPan: "123456789123", val: val ?? "", tknbnddes: "MASTERCARD", clinom: "Raimundo Nonato", metodoPagamentoCartaoCreditoPreferencial: 1, metodoPagamentoCartaoCreditoAtivo: 1, metodoPagamentoCartaoCreditoExpirado: 1, metodoPagamentoCartaoCreditoValidado: 1, clidoc: "", tkncodseq: 1)
        return ListCardDataView(model: listCard)
    }
    
    // MARK : computed -> "holder"
    func testShoudlReturnHolderWhenContainsValue() {
        let model = createModel(val: nil)
        let expected = "Raimundo Nonato"
        expect(model.holder).to(equal(expected))
    }
    
    // MARK : computed -> "fourLastNumberCard"
    func testShoudlReturnFourLastNumberCardWhenContainsValue() {
        let model = createModel(val: nil)
        let expected = "9123"
        expect(model.fourLastNumberCard).to(equal(expected))
    }
    
    // MARK : computed -> "fourLastNumberCard"
    func testShoudlReturnCardNumberFormattedWhenContainsValue() {
        let model = createModel(val: nil)
        let expected = "●●●● ●●●● ●●●● 9123"
        expect(model.cardNumberFormatted).to(equal(expected))
    }
    
    // MARK : computed -> "cardNumberShortFormatted"
    func testShoudlReturnCardNumberShortFormattedWhenContainsValue() {
        let model = createModel(val: nil)
        let expected = "●●●● 9123"
        expect(model.cardNumberShortFormatted).to(equal(expected))
    }
    
    // MARK : computed -> "cardDate"
    func testShoudlReturnCardDateWhenContainsValue() {
        let model = createModel(val: "0922")
        let expected = "09/22"
        expect(model.cardDate).to(equal(expected))
    }
    
    // MARK : computed -> "brandImage"
    func testShoudlReturnBrandImageWhenContainsValue() {
        let model = createModel(val: nil)
        let expected = #imageLiteral(resourceName: "minicard-brand-mastercard")
        expect(model.brandImage).to(equal(expected))
    }
    
    // MARK : computed -> "isFavoriteCard"
    func testShoudlReturnIsFavoriteCardWhenContainsValue() {
        let model = createModel(val: nil)
        expect(model.isFavoriteCard).to(beTrue())
    }

    // MARK : computed -> "cardNumberShortFormatted"
    func testShoudlReturnIsValidWhenContainsValue() {
        let model = createModel(val: "0922")
        
        expect(model.isValid).to(beTrue())
    }
}
