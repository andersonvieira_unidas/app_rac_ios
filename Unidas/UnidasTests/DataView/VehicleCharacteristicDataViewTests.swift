//
//  VehicleCharacteristicDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class VehicleCharacteristicDataViewTests: XCTestCase {
    
    // MARK : computed -> image
    func testImageWhenModelContainsValue() {
        let expected = "imageUrl"
        let vehicleCharacteristic = VehicleCharacteristic(image: URL(string: expected), name: "Name Characteristic")
        let model = VehicleCharacteristicDataView(model: vehicleCharacteristic)
        expect(model.image?.absoluteString).to(equal(expected))
    }
    
    func testImageNilWhenModelContainsValue() {
        let vehicleCharacteristic = VehicleCharacteristic(image: nil, name: "Name Characteristic")
        let model = VehicleCharacteristicDataView(model: vehicleCharacteristic)
        expect(model.image).to(beNil())
    }
    
    // MARK : computed -> text
    func testTextWhenModelContainsValue() {
        let expected = "Name Characteristic"
        let vehicleCharacteristic = VehicleCharacteristic(image: URL(string: ""), name: expected)
        let model = VehicleCharacteristicDataView(model: vehicleCharacteristic)
        expect(model.text).to(equal(expected))
    }
}
