//
//  ContractDetailPaymentListItemDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ContractDetailPaymentListItemDataViewTests: XCTestCase {
    
    var model: ContractDetailPaymentListItemDataView!
    
    override func setUp() {
        let contractDetailPaymentListItem = getInstanceFromJsonByFileName("contract_payment_list_object", decodeTo: ContractDetailPaymentListItem.self)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem!)
    }
    
    // MARK : computed -> paymentType
    func testShouldPaymentTypeWhenModelContainsValue() {
        let expected = "Pagamento"
        expect(self.model.paymentType).to(equal(expected))
        
    }
    
    // MARK : computed -> cardLastNumbers
    func testShouldCardLastNumbersWhenModelContainsValue() {
        let expected = "•••• 4738"
        expect(self.model.cardLastNumbers).to(equal(expected))
    }
    
    func testShouldNilCardLastNumbersWhenModelContainsNilValue() {
        let contractDetailPaymentListItem = ContractDetailPaymentListItem(type: nil, date: nil, hour: nil, card: nil, value: nil, installments: nil, cardBrand: nil, paymentStatus: nil, prePaymentStatus: nil)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem)
        expect(self.model.cardLastNumbers).to(beNil())
    }
    
    // MARK : computed -> value
    func testShouldValueWhenModelContainsValue() {
        let expected = "R$ 72,25"
        expect(self.model.value).to(equal(expected))
    }
    
    func testShouldNilValueWhenModelContainsNilValue() {
        let contractDetailPaymentListItem = ContractDetailPaymentListItem(type: nil, date: nil, hour: nil, card: nil, value: nil, installments: nil, cardBrand: nil, paymentStatus: nil, prePaymentStatus: nil)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem)
        expect(self.model.value).to(beNil())
    }
    
    // MARK : computed -> cardBrandImage
    func testShouldCardBrandImageWhenModelContainsValue() {
        expect(self.model.cardBrandImage).toNot(beNil())
    }
    
    func testShouldNilCardBrandImageWhenModelContainsNilValue() {
        let contractDetailPaymentListItem = ContractDetailPaymentListItem(type: nil, date: nil, hour: nil, card: nil, value: nil, installments: nil, cardBrand: nil, paymentStatus: nil, prePaymentStatus: nil)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem)
        expect(self.model.cardBrandImage).to(beNil())
    }
    
    // MARK : computed -> installmentsDescription
    func testShouldInstallmentsDescriptionWhenModelContainsValue() {
        let expected = "1x R$ 72,25"
        expect(self.model.installmentsDescription).to(equal(expected))
    }
    
    func testShouldNilInstallmentsDescriptionWhenModelContainsNilValue() {
        let contractDetailPaymentListItem = ContractDetailPaymentListItem(type: nil, date: nil, hour: nil, card: nil, value: nil, installments: nil, cardBrand: nil, paymentStatus: nil, prePaymentStatus: nil)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem)
        expect(self.model.installmentsDescription).to(beNil())
    }
    
    // MARK : computed -> paymentStatus
    func testShouldPaymentStatusWhenModelContainsValue() {
        let expected = "Pagamento Confirmado"
        expect(self.model.paymentStatus).to(equal(expected))
    }
    
    func testShouldNilPaymentStatusWhenModelContainsNilValue() {
        let contractDetailPaymentListItem = ContractDetailPaymentListItem(type: nil, date: nil, hour: nil, card: nil, value: nil, installments: nil, cardBrand: nil, paymentStatus: nil, prePaymentStatus: nil)
        model = ContractDetailPaymentListItemDataView(model: contractDetailPaymentListItem)
        expect(self.model.paymentStatus).to(beNil())
    }
}
