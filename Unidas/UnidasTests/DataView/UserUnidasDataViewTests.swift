//
//  UserUnidasDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
@testable import Unidas

class UserUnidasDataViewTests: XCTestCase {

    private func createModel() -> UserUnidas {
        return getInstanceFromJsonByFileName("user_login_object", decodeTo: UserUnidas.self)!
    }
}
