//
//  ReservationDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 10/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ReservationDataViewTests: XCTestCase {

    var reservation: Reservation!
    var model: ReservationDataView!
    
    override func setUp() {
        //set pickup
        let pickupDateAdd = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        
        let pickupGarage = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        let pickup = GarageReservationDetails(date: pickupDateAdd!, garage: Garage(garageUnidas: pickupGarage!))
        
        //set return
        let returnDateAdd = Calendar.current.date(byAdding: .day, value: 2, to: Date())
        let returnGarage = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        let `return` = GarageReservationDetails(date: returnDateAdd!, garage: Garage(garageUnidas: returnGarage!))
        
        //group
        let group = VehMakeModel(name: "HATCH ECONOMICO 1.0", code: "ECMM", vehiclesGroups: "", codeCategory: "B")
      
        reservation = Reservation(id: "12345", number: "1122334455", totalValue: 120.0,
                                  prePaymentValue: 110.0, preAuthorizationValue: 0.0, pickUp: pickup,
                                  return: `return`, group: group, protections: nil, equipments: nil,
                                  rateQualifier: "1234568", duration: 1, returnTax: nil,
                                  extraHourTax: nil, administrativeTaxValue: 12.0,
                                  administrativeTaxPercentValue: 0.12, voucher: nil, airlineInformation: nil,
                                  selectedPaymentOption: .payAtPickup, selectedCasterWeekday: nil,
                                  prePaymentDiscountValue: 10.0, sendSMS: false, promotionCode: nil, acceptTermLocation: true,
                                  acceptTermRefound: true)
        
        model = ReservationDataView(model: reservation)
    }
    
    // MARK : computed -> number
    func testShouldNumberWhenModelContainsNumber() {
        let expected = "1122334455"
        expect(self.model.number).to(equal(expected))
    }
    
    func testShouldNilNumberWhenModelNotContainsNumber() {
        var reservationCopy = reservation
        reservationCopy?.number = nil
        let modelCopy = ReservationDataView(model: reservationCopy!)
        expect(modelCopy.number).to(beNil())
    }
    
    // MARK : computed -> pickup
    func testShouldPickupWhenModelContainsPickup() {
        expect(self.model.pickUp).toNot(beNil())
    }
    
    func testShouldNilPickupWhenModelNotContainsPickup() {
        var reservationCopy = reservation
        reservationCopy?.pickUp = nil
        let modelCopy = ReservationDataView(model: reservationCopy!)
        expect(modelCopy.pickUp).to(beNil())
    }
    
    // MARK : computed -> return
    func testShouldReturnWhenModelContainsPickup() {
        expect(self.model.return).toNot(beNil())
    }
    
    func testShouldNilReturnWhenModelNotContainsPickup() {
        var reservationCopy = reservation
        reservationCopy?.return = nil
        let modelCopy = ReservationDataView(model: reservationCopy!)
        expect(modelCopy.return).to(beNil())
    }
    
    // MARK : computed -> totalValue
    func testShouldReturnTotalValueWhenModelContainsTotalValue() {
        let expected = "R$ 120,00"
        expect(self.model.totalValue).to(equal(expected))
    }
    
    func testShouldNilReturnTotalValueWhenModelNotContainsTotalValue() {
        var reservationCopy = reservation
        reservationCopy?.totalValue = nil
        let modelCopy = ReservationDataView(model: reservationCopy!)
        let expected = "R$ 0,00"
        expect(modelCopy.totalValue).to(equal(expected))
    }
    
    // MARK : computed -> prePaymentValue
    func testShouldReturnPrePaymentValueWhenModelContainsPrePaymentValue() {
        let expected = "R$ 110,00"
        expect(self.model.prePaymentValue).to(equal(expected))
    }
    
    func testShouldNilReturnPrePaymentValueWhenModelNotContainsPrePaymentValue() {
        var reservationCopy = reservation
        reservationCopy?.prePaymentValue = nil
        let modelCopy = ReservationDataView(model: reservationCopy!)
        let expected = "R$ 0,00"
        expect(modelCopy.prePaymentValue).to(equal(expected))
    }
    
    // MARK : computed -> group
    func testShouldReturnGroupWhenModelContainsGroup() {
        expect(self.model.group).toNot(beNil())
    }
    
    func testShouldNilReturnGroupWhenModelNotContainsGroup() {
        var reservationCopy = reservation
        reservationCopy?.group = nil
        expect(reservationCopy?.group).to(beNil())
    }
    
    // MARK : computed -> periodFrom
    func testShouldReturnPeriodFromWhenModelContainsGroup() {
        expect(self.model.group).toNot(beNil())
    }
    
    func testShouldNilReturnPeriodFromWhenModelNotContainsGroup() {
        var reservationCopy = reservation
        reservationCopy?.group = nil
        expect(reservationCopy?.group).to(beNil())
    }
}
