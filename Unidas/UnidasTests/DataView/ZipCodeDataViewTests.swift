//
//  ZipCodeDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation

import XCTest
import Nimble
@testable import Unidas

class ZipCodeDataViewTests: XCTestCase {
    
    private func createModel(logradouro:String?, logradouroTipo:String?) -> ZipcodeDataView {
        let zipCode = Zipcode(idLocalidade: 1, idUf: nil, logradouro: logradouro, bairro: "Aviação", nmLogradouroTipo: "", nmLogradouroAbreviacao: "", uf: "São Paulo", localidade: "Praia Grande", cep: "", logradouroTipo: logradouroTipo)
        
        let zipCodeDataView = ZipcodeDataView(model: zipCode)
        return zipCodeDataView
    }
    
    // MARK : computed -> address
    func testShoudlReturnFullAddress() {
        let model = createModel(logradouro: "São Pedro", logradouroTipo: "Rua")
        let expected = "São Pedro"
        expect(model.model.logradouro).to(equal(expected))
    }
    
    func testShoudlReturnParcialAddressReturningNil() {
        //let model = createModel(logradouro: nil, logradouroTipo: "Rua")
        //expect(model.address).to(beNil())
    }
   
    func testShoudlReturnParcialAddressReturningLogradouro() {
        let model = createModel(logradouro: "São Pedro", logradouroTipo: nil)
        let expected = "São Pedro"
        expect(model.model.logradouro).to(equal(expected))
    }
    
    // MARK : computed -> neighborhood
    func testShoulReturnNeighborhoodWhenContainsValue() {
        let model = createModel(logradouro: nil, logradouroTipo: nil)
        let expected = "Aviação"
        expect(model.neighborhood).to(equal(expected))
    }
    
    // MARK : computed -> state
    func testShoulReturnStateWhenContainsValue() {
        let model = createModel(logradouro: nil, logradouroTipo: nil)
        let expected = "São Paulo"
        expect(model.state).to(equal(expected))
    }
    
    // MARK : computed -> city
    func testShoulReturnCityWhenContainsValue() {
        let model = createModel(logradouro: nil, logradouroTipo: nil)
        let expected = "Praia Grande"
        expect(model.city).to(equal(expected))
    }
}
