//
//  PaymentOptionDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 11/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class PaymentOptionDataViewTests: XCTestCase {
    
    var paymentOptionInAdvance: PaymentOptionDataView!
    var paymentOptionAtPickup: PaymentOptionDataView!
    
    override func setUp() {
        paymentOptionAtPickup = PaymentOptionDataView(model: .payAtPickup, discount: 0.10)
        paymentOptionInAdvance = PaymentOptionDataView(model: .payInAdvance, discount: 0.10)
    }
    
    // MARK : computed -> name
    func testShouldPaymentNameWhenModelIsPaymentAtPickup() {
        let expected = NSLocalizedString("Pay at pickup", comment: "Pay at pickup option name")
        expect(self.paymentOptionAtPickup.name).to(equal(expected))
    }
    
    func testShouldPaymentNameWhenModelIsPaymentInAdvance() {
        let expected = NSLocalizedString("Pay in advance", comment: "Pay in advance option name")
        expect(self.paymentOptionInAdvance.name).to(equal(expected))
        
    }
    
    // MARK : computed -> description
    func testShouldPaymentDescriptionWhenModelIsPaymentAtPickup() {
        let expected = NSLocalizedString("Are you in a hurry?\nWe understand.", comment: "Pay at pickup description label")
        expect(self.paymentOptionAtPickup.description).to(equal(expected))
    }
    
    func testShouldPaymentDescriptionWhenModelIsPaymentInAdvance() {
        let localizedString = NSLocalizedString("By paying now you'll receive a\ndiscount of %@", comment: "Pre-payment description label")
        let percentString = NumberFormatter.percentFormatter.string(from: NSNumber(value: paymentOptionInAdvance.discount!))
        let expected =  String(format: localizedString, percentString!)
        
        expect(self.paymentOptionInAdvance.description).to(equal(expected))
    }
}
