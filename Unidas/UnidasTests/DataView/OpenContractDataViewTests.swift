//
//  OpenContractDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class OpenContractDataViewTests: XCTestCase {
    
    var model: OpenContractDataView!
    
    override func setUp() {
        let openContract = OpenContract(contractNumber: 1000, message: "Message", preAuthorizationFinished: true, paymentFinished: true)
        model = OpenContractDataView(model: openContract)
    }
    
    // MARK : computed -> contractNumber
    func testShouldContractNumberWhenModelContainsValue() {
        let expected = "1000"
        expect(self.model.contractNumber).to(equal(expected))
    }
}
