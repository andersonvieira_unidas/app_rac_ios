//
//  PurchaseDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 19/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class PurchaseDataViewTests: XCTestCase {

    var model: PurchaseDataView!
    
    override func setUp() {
        let purchase = Purchase(codigo: 100, tipo: nil, nome: "Purchase Name", codigoOTA: nil, descricao: "Purchase Description", quantidade: 10, valorUnitario: 10.0, valorTotal: 100.00)
        model = PurchaseDataView(model: purchase)
    }
    
    // MARK : computed -> name
    func testShouldNameWhenModelContainsValue() {
        let expected = "Purchase Name"
        expect(self.model.name).to(equal(expected))
    }
    
    // MARK : computed -> description
    func testShouldDescriptionWhenModelContainsValue() {
        let expected = "Purchase Description"
        expect(self.model.description).to(equal(expected))
    }
}
