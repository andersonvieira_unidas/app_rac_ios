//
//  CallCenterDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 18/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class CallCenterDataViewTests: XCTestCase {

    private func createModel(phoneNumber: String?) -> CallCenterDataView {
        let model = CallCenter(type: .assistance, title: "Title", description: "Description", phoneNumber: phoneNumber)
        return CallCenterDataView(model: model)
    }
    
    // MARK : computed -> "title"
    func testShoudlReturnTitleWhenContainsValue() {
        let model = createModel(phoneNumber: "")
        let expected = "Title"
        expect(model.title).to(equal(expected))
    }
    
    // MARK : computed -> "description"
    func testShoudlReturnDescriptionWhenContainsValue() {
        let model = createModel(phoneNumber: "")
        let expected = "Description"
        expect(model.description).to(equal(expected))
    }
    
    // MARK : computed -> "phoneNumber"
    func testShoudlReturnPhoneNumberWhenContainsValue() {
        let model = createModel(phoneNumber: "0800 771 5158")
        let expected = "0800 771 5158"
        expect(model.phoneNumber).to(equal(expected))
    }
    
    // MARK : computed -> "phoneNumberNil"
    func testShoudlReturnNilWhenNotContainsPhoneNumberValue() {
        let model = createModel(phoneNumber: nil)
        expect(model.phoneNumber).to(beNil())
    }
    
    // MARK : computed -> "formattedPhoneNumber"
    func testShoudlReturnFormattedPhoneNumberWhenContainsValue() {
        let model = createModel(phoneNumber: "0800 771 5158")
        let expected = "08007715158"
        expect(model.formattedPhone).to(equal(expected))
    }
    
    // MARK : computed -> "formattedPhoneNumberNil"
    func testShoudlReturnNilWhenNotContainsFormattedPhoneNumberValue() {
        let model = createModel(phoneNumber: nil)
        expect(model.formattedPhone).to(beNil())
    }
}
