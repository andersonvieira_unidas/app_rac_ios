//
//  CheckDocumentDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 18/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class CheckDocumentDataViewTests: XCTestCase {

    private func createModel(updateMessage:String?, lastUpdateMessage:String?) -> CheckDocumentDataView {
        let model = CheckDocument(documentNumber: "", updateAt: updateMessage ?? "", validateAt: lastUpdateMessage ?? "", type: CheckDocumentType.signature)
        return CheckDocumentDataView(model: model)
    }
    
    // TODO - realizar o teste de MARK : computed -> "updateMessage"
    
    // MARK : computed -> "updateMessageNil"
    func testShoudlReturnNilWhenNotContainsUpdateMessageValue() {
        let model = createModel(updateMessage: nil, lastUpdateMessage: nil)
        expect(model.updateMessage).to(beNil())
    }
    
    //TODO - realizar o teste de MARK : computed -> "lastUpdateMessage"
    
    // MARK : computed -> "updateMessageNil"
    func testShoudlReturnNilWhenNotContainslastUpdateMessageValue() {
        let model = createModel(updateMessage: nil, lastUpdateMessage: nil)
        expect(model.lastUpdateMessage).to(beNil())
    }  
}
