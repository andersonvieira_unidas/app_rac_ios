//
//  LocationDetaViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class LocationDetaViewTests: XCTestCase {
    var model: LocationDataView!
    
    override func setUp() {
        let location = Location(latitude: -23.436247, longitude: -46.483464)
        model = LocationDataView(model: location)
    }

    // MARK : computed -> coordinate
    func testShouldCoordinateWhenModelContainsValue() {
        let latExpected = -23.436247
        let lonExpected = -46.483464
        expect(self.model.coordinate).toNot(beNil())
        expect(self.model.coordinate.latitude).to(equal(latExpected))
        expect(self.model.coordinate.longitude).to(equal(lonExpected))
        
    }
    
    // MARK : computed -> clLocationCoordinate
    func testShouldClLocationCoordinateWhenModelContainsValue() {
        expect(self.model.clLocationCoordinate).toNot(beNil())
    }
}
