//
//  StoreAvailabilityErrorDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class StoreAvailabilityErrorDataViewTests: XCTestCase {

    // MARK : computed -> title (pickup)
    func testShouldTitlePickupTextWhenModelContainsValue() {
        let expected = NSLocalizedString("Pick up store is not available at this time", comment: "Pick up store is not available at this time error message")
        let storeAvailabilityError = StoreAvailabilityError.pickUpStoreUnavailableAtThisTime
        let model = StoreAvailabilityErrorDataView(model: storeAvailabilityError)
        
        expect(model.title).to(equal(expected))
    }
    
    // MARK : computed -> title (return)
    func testShouldTitleReturnTextWhenModelContainsValue() {
        let expected = NSLocalizedString("Return store is not available at this time", comment: "Return store is not available at this time error message")
        let storeAvailabilityError = StoreAvailabilityError.returnStoreUnavailableAtThisTime
        let model = StoreAvailabilityErrorDataView(model: storeAvailabilityError)
        
        expect(model.title).to(equal(expected))
    }
}
