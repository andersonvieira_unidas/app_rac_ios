//
//  ProtectionParticipationGroupDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ProtectionParticipationGroupDataViewTests: XCTestCase {
    var model: ProtectionParticipationGroupDataView!
    
    override func setUp() {
        let protectionParticipationGroup = ProtectionParticipationGroup(groups: "Group", value: 100.0)
        model = ProtectionParticipationGroupDataView(model: protectionParticipationGroup)
    }
    
    // MARK : computed -> groups
    func testShouldGroupValueWhenModelContainsValue() {
        let expected = "Group"
        expect(self.model.groups).to(equal(expected))
    }
    
     // MARK : computed -> value
    func testShouldValueWhenModelContainsValue(){
        let expected = "R$ 100,00"
        expect(self.model.value).to(equal(expected))
    }
}
