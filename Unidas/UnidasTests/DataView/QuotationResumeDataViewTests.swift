//
//  QuotationResumeDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 11/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class QuotationResumeDataViewTests: XCTestCase {
    
    var model : QuotationResumeDataView!
    
    override func setUp() {
        model = createMockObject(withFile: "vehavail_am_group")
    }
    
    private func createMockObject(withFile: String) -> QuotationResumeDataView {
        var quotationResume = QuotationResume()
        
        //pickup
        let pickupDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let pickupGarage = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        let pickup = GarageReservationDetails(date: pickupDate!, garage: Garage(garageUnidas: pickupGarage!))
        
        //return
        let returnDate = Calendar.current.date(byAdding: .day, value: 5, to: Date())
        let returnGarage = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        let `return` = GarageReservationDetails(date: returnDate!, garage: Garage(garageUnidas: returnGarage!))
        
        quotationResume.pickUp =  pickup
        quotationResume.return = `return`
        
        //priced equip
        let pricedEquip = getInstanceFromJsonByFileName("priced_equip_object", decodeTo: PricedEquips.self)
        let pricedEquips : [PricedEquips] = [pricedEquip!]
        quotationResume.selectedEquipments = pricedEquips
        
        //protections
        let protection = getInstanceFromJsonByFileName("priced_coverage_object", decodeTo: PricedCoverages.self)
        let protections : [PricedCoverages] = [protection!]
        quotationResume.selectedProtections = protections
        
        let vehAvail = getInstanceFromJsonByFileName(withFile, decodeTo: VehAvail.self)
        quotationResume.quotation = vehAvail
        
        return QuotationResumeDataView(model: quotationResume)
    }
    
    // MARK : computed -> subtotalValue
    func testShouldSubtotalValueWhenModelContainsValues() {
        let expected = 99.74
        expect(self.model.subtotalValue?.roundedFractionDigits(to: 2)).to(equal(expected))
    }
    
    // MARK : computed -> subtotalPrePaymentValue
    func testShouldSubtotalPrePaymentValueWhenModelContainsValues() {
        let expected = 98.54
        expect(self.model.subtotalPrePaymentValue?.roundedFractionDigits(to: 2)).to(equal(expected))
    }
    
    // MARK : computed -> totalDiaryWithFeesAndDiscount
    func testShouldTotalDiaryWithFeesAndDiscountWhenModelContainsValues() {
        let expected = "R$ 59,74"
        expect(self.model.totalDiaryWithFeesAndDiscount).to(equal(expected))
    }
    
    // MARK : computed -> totalDiaryWithFeesAndDiscountValue
    func testShouldTotalDiaryWithFeesAndDiscountValueWhenModelContainsValues() {
        let model = createMockObject(withFile: "vehavail_am_group")
        let expected = 59.74
        expect(model.totalDiaryWithFeesAndDiscountValue).to(equal(expected))
    }
    
    // MARK : computed -> totalWithAdministrativeTax
    func testShouldTotalWithAdministrativeTaxWhenModelContainsValue() {
        let model = createMockObject(withFile: "vehavail_am_group")
        let expected = "R$ 111,71"
        expect(model.totalWithAdministrativeTax).to(equal(expected))
    }
    
    func testShouldTotalWithAdministrativeTaxWhenModelContainsNilValue() {
        let modelEmpty = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelEmpty.totalWithAdministrativeTax).to(beNil())
    }
    
    // MARK : computed -> totalWithAdministrativeTaxValue
    func testShouldTotalWithAdministrativeTaxValueWhenModelContainsNilValues() {
        let modelEmpty = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelEmpty.totalWithAdministrativeTaxValue).to(beNil())
    }
    
    func testShouldTotalWithAdministrativeTaxValueWhenModelContainsValues() {
        let expected = 111.71
        expect(self.model.totalWithAdministrativeTaxValue).to(equal(expected))
    }
    
    // MARK : computed -> prePaymentTotalWithAdministrativeTax
    func testShouldPrePaymentTotalWithAdministrativeTaxWhenModelContainsValues() {
        let expected = "R$ 110,36"
        expect(self.model.prePaymentTotalWithAdministrativeTax).to(equal(expected))
    }
    
    // MARK : computed -> prePaymentTotalWithAdministrativeTaxValue
    func testShouldPrePaymentTotalWithAdministrativeTaxValueWhenModelContainsValues() {
        let expected = 110.35999999999999
        expect(self.model.prePaymentTotalWithAdministrativeTaxValue).to(equal(expected))
    }
    
    // MARK : computed -> totalPrePaymentDiaryWithFeesAndDiscountValue
    func testShouldTotalPrePaymentDiaryWithFeesAndDiscountValueWhenModelContainsValues() {
        let expected = 58.54
        expect(self.model.totalPrePaymentDiaryWithFeesAndDiscountValue).to(equal(expected))
    }
    
    // MARK : computed -> administrativeTax
    func testShouldAdministrativeTaxWhenModelContainsNilValue() {
        let modelCopy = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelCopy.administrativeTax).to(beNil())
    }
    
    func testShouldAdministrativeTaxWhenModelContainsValue() {
        let expected = "R$ 11,97"
        expect(self.model.administrativeTax).to(equal(expected))
    }
    
    // MARK : computed -> administrativeTaxValue
    func testShouldAdministrativeTaxValueWhenModelContainsNilValue() {
        let modelCopy = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelCopy.administrativeTaxValue).to(beNil())
    }
    
    func testShouldAdministrativeTaxValueWhenModelContainsValue() {
        let expected = 11.97
        expect(self.model.administrativeTaxValue?.roundedFractionDigits(to: 2)).to(equal(expected))
    }
    
    // MARK : computed -> administrativeTaxPrePaymentValue
    func testShouldAdministrativeTaxPrePaymentValueWhenModelContainsZeroValue() {
        let modelCopy = createMockObject(withFile: "vehavail_am_group_empty")
        let expected =  0.0
        expect(modelCopy.administrativeTaxPrePaymentValue).to(equal(expected))
    }
    
    func testShouldAdministrativeTaxPrePaymentValueWhenModelContainsValue() {
        let expected = 11.82
        expect(self.model.administrativeTaxPrePaymentValue?.roundedFractionDigits(to: 2)).to(equal(expected))
    }
    
    // MARK : computed -> administrativeTaxPercentage
    func testShouldAdministrativeTaxPercentageWhenModelContainsNilValue() {
        let modelCopy = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelCopy.administrativeTaxPercentage).to(beNil())
    }
    
    func testShouldAdministrativeTaxPercentageWhenModelContainsValue() {
        let expected = "12% do subtotal"
        expect(self.model.administrativeTaxPercentage).to(equal(expected))
    }
    
    // MARK : computed -> administrativeTaxPercentageValue
    func testShouldAdministrativeTaxPercentageValueWhenModelContainsNilValue() {
        let modelCopy = createMockObject(withFile: "vehavail_am_group_empty")
        expect(modelCopy.administrativeTaxPercentageValue).to(beNil())
    }
    
    func testShouldAdministrativeTaxPercentageValueWhenModelContainsValue() {
        let expected = 0.12
        expect(self.model.administrativeTaxPercentageValue?.roundedFractionDigits(to: 2)).to(equal(expected))
    }
    
    // MARK : computed -> resumeInformations
    func testShouldResumeInformationsValuesWhenModelContainsValues() {
        expect(self.model.resumeInformations).toNot(beNil())
        expect(self.model.resumeInformations[0].title).to(equal("Diária Km livre incluso"))
        expect(self.model.resumeInformations[0].subtitle).to(equal("2x R$ 29,87"))
        expect(self.model.resumeInformations[0].value).to(equal("R$ 59,74"))
    }
    
    // MARK : computed -> fees
    func testShouldFeesWhenModelContainsValues() {
        expect(self.model.fees.isEmpty).to(beFalse())
    }
    
    // MARK : computed -> protections
    func testShouldProtectionsValuesWhenModelContainsValues() {
        expect(self.model.protections).toNot(beNil())
        expect(self.model.protections[0].model.coverageType).to(equal("30"))
        expect(self.model.protections[0].model.details![0].coverageTextType).to(equal("Supplement"))
        expect(self.model.protections[0].model.details![0].text).to(equal("PROTEÇÃO PARCIAL"))
        expect(self.model.protections[0].model.quantity).to(equal(2.0))
    }
    
    // MARK : computed -> totalProtections
    func testShouldTotalProtectionsWhenModelNotContainsValues() {
        let expected = "R$ 0,00"
        expect(self.model.totalProtections).to(equal(expected))
    }
    
    // MARK : computed -> equipments
    func testShouldEquipmentsValuesWhenModelContainsValues() {
        expect(self.model.equipments).toNot(beNil())
        expect(self.model.equipments[0].model.charge?.amount).to(equal(40.0))
        expect(self.model.equipments[0].model.charge?.unitCharge).to(equal(20.0))
        expect(self.model.equipments[0].model.quantity).to(equal(2))
        expect(self.model.equipments[0].model.equipType).to(equal("8"))
        expect(self.model.equipments[0].model.description).to(equal("BEBÊ CONFORTO (0KG A 13KG)"))
    }
    
    // MARK : computed -> totalEquipments
    func testShouldTotalEquipmentsWhenModelContainsValues() {
        let expected = "R$ 40,00"
        expect(self.model.totalEquipments).to(equal(expected))
    }
    
    // MARK : computed -> pickup
    func testShouldPickupWhenModelContainsValues() {
        expect(self.model.pickUp).toNot(beNil())
        expect(self.model.pickUp?.garage?.model.name).to(equal("UNIDAS LOCADORA DE VEICULOS LTDA."))
        expect(self.model.pickUp?.garage?.model.description).to(equal("AEROPORTO DE GUARULHOS - TERMINAL 3"))
        expect(self.model.pickUp?.garage?.model.code).to(equal("GRU1"))
    }
    
    // MARK : computed -> return
    func testShouldReturnWhenModelContainsValues() {
        expect(self.model.return).toNot(beNil())
        expect(self.model.return?.garage?.model.name).to(equal("UNIDAS LOCADORA DE VEICULOS LTDA."))
        expect(self.model.return?.garage?.model.description).to(equal("AEROPORTO DE GUARULHOS - TERMINAL 3"))
        expect(self.model.return?.garage?.model.code).to(equal("GRU1"))
    }
    
    // MARK : computed -> mileageDescription
    func testShouldMileageDescriptionWhenModelContainsValues() {
        let expected = "Quilometragem livre"
        expect(self.model.mileageDescription).to(equal(expected))
    }
    
    // MARK : computed -> isUpgrade
    func testShouldIsUpgradeWhenModelContainsValues() {
        expect(self.model.isUpgrade).to(beFalse())
    }
    
    // MARK : computed -> upgradeDescription
    func testShouldIsUpgradeDescriptionWhenModelNotContainsValues() {
        expect(self.model.upgradeDescription).to(beNil())
    }
    
    // MARK : computed -> voucherDescription
    func testShouldIsVoucherDescriptionWhenModelContainsValues() {
        expect(self.model.voucherDescription).to(beNil())
        expect(self.model.showsVoucherDescription).to(beFalse())
    }
}
