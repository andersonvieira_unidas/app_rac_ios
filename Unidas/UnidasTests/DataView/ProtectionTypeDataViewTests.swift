//
//  ProtectionTypeDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ProtectionTypeDataViewTests: XCTestCase {
    // MARK : computed -> description - vehicle
    func testShouldDescriptionVehicleValueWhenModelIsVehicle() {
        let model = ProtectionTypeDataView(model: ProtectionType.vehicle)
        let expected = NSLocalizedString("Vehicle protections", comment: "Vehicle protections group name")
        expect(model.description).to(equal(expected))
    }
    
    // MARK : computed -> description - complementary
    func testShouldDescriptionComplementaryValueWhenModelIsComplementary() {
        let model = ProtectionTypeDataView(model: ProtectionType.complementary)
        let expected = NSLocalizedString("Complementary protections", comment: "Complementary protections group name")
        expect(model.description).to(equal(expected))
    }
    
    // MARK : computed -> description - thirdParty
    func testShouldDescriptionThirdPartyValueWhenModelIsThirdParty() {
        let model = ProtectionTypeDataView(model: ProtectionType.thirdParty)
        let expected = NSLocalizedString("Third party protections", comment: "Third party protections group name")
        expect(model.description).to(equal(expected))
    }
}
