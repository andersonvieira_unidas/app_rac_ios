//
//  GarageReservationDetailsDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class GarageReservationDetailsDataViewTests: XCTestCase {
    
    var model: GarageReservationDetailsDataView!
    var date: Date!
    
    override func setUp() {
        date = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        let garageUnidas = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        let garageReservationDetails = GarageReservationDetails(date: date!, garage: Garage(garageUnidas: garageUnidas!))
        
        model = GarageReservationDetailsDataView(model: garageReservationDetails)
    }
    
    // MARK : computed -> date
    func testShouldDateTextWhenModelContainsValue() {
        let expected = string(from: date)
        expect(self.model.date).to(equal(expected))
    }
    
    // MARK : computed -> garage
    func testShouldGarageTextWhenModelContainsValue() {
        let expected = "Aeroporto De Guarulhos - Terminal 3"
        expect(self.model.garage!.name).to(equal(expected))
    }
    
    // MARK : computed -> locationDescription
    func testShouldLocationDescriptionTextWhenModelContainsValue() {
        let expected = "Em Aeroporto De Guarulhos - Terminal 3, SP"
        expect(self.model.locationDescription).to(equal(expected))
    }
    
    private func string(from date: Date) -> String {
        let dateString = DateFormatter.dayWithMonthExtensionDateFormatter.string(from: date)
        let timeString = DateFormatter.timeOnlyFormatter.string(from: date)
        let localizedString = NSLocalizedString("%@ at %@", comment: "Date")
        return String(format: localizedString, dateString, timeString)
    }
}
