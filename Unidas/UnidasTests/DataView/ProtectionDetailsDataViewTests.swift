//
//  ProtectionDetailsDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ProtectionDetailsDataViewTests: XCTestCase {

    var model: ProtectionDetailsDataView!
    
    override func setUp() {
        let protectionParticipationGroups: [ProtectionParticipationGroup] = [ProtectionParticipationGroup(groups: "Group", value: 100.0)]
        let protectionDetails = ProtectionDetails(iconURL: URL(string: "http://www.unidas.com.br")!, description: "Description", requiredParticipationGroups: protectionParticipationGroups)
        model = ProtectionDetailsDataView(model: protectionDetails)
    }
    
    // MARK : computed -> iconURL
    func testShouldIconURLValueWhenModelContainsValue() {
        let expected = "http://www.unidas.com.br"
        expect(self.model.iconURL?.absoluteString).to(equal(expected))
    }
    
    // MARK : computed -> description
    func testShouldDescriptionValueWhenModelContainsValue() {
        let expected = "Description"
        expect(self.model.description).to(equal(expected))
    }
    
    // MARK : computed -> requiredParticipationGroups
    func testShouldRequiredParticipationGroupValueWhenModelContainsValue() {
        let expected = "Group"
        let expectedValue = 100.0
        expect(self.model.requiredParticipationGroups).toNot(beNil())
        expect(self.model.requiredParticipationGroups[0].model.groups).to(equal(expected))
        expect(self.model.requiredParticipationGroups[0].model.value).to(equal(expectedValue))
    }
}
