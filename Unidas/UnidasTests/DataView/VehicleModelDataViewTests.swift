//
//  VehicleModelDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class VehicleModelDataViewTests: XCTestCase {
    private func createModel (name: String? = "Name Model", illustrativePicture: URL? = nil) -> VehicleModelDataView {
        let vehicleModelManufacture = VehicleModelManufacture(id: 1, name: "Manufacture Name")
        let vehicleModel = VehicleModel(id: 123456, name: name, illustrativePicture: illustrativePicture, manufacture: vehicleModelManufacture)
        let model = VehicleModelDataView(model: vehicleModel)
        
        return model
    }
    
    // MARK : computed -> name
    func testShouldNameWhenModelContainsValue(){
        let expected = "Name Model"
        let model = createModel()
        expect(model.name).to(equal(expected))
    }
    
    // MARK : computed -> illustrativePicture
    func testShouldIllustrativePictureWhenModelContainsValue() {
        let model = createModel()
        expect(model.illustrativePicture).to(beNil())
    }
    
    func testShouldNilIllustrativePictureWhenModelNotContainsValue() {
        let expected = "illustrativePictureURL"
        let illustrativePicture = URL(string: "illustrativePictureURL")
        let model = createModel(illustrativePicture: illustrativePicture)
        expect(model.illustrativePicture?.absoluteString).to(equal(expected))
    }
    
    // MARK : computed -> manufacturer
    func testShouldManufacturerWhenModelContainsValue() {
        let model = createModel()
        expect(model.manufacturer).toNot(beNil())
    }
    
    // MARK : computed -> nameWithModel
    func testShouldNameWithModelWhenModelContainsValue() {
        let expected = "Name Model Manufacture Name"
        let model = createModel()
        expect(model.nameWithModel).to(equal(expected))
    }
    
    func testShouldNilNameWithModelWhenModelNotContainsValue() {
        let model = createModel(name: nil, illustrativePicture: nil)
        expect(model.nameWithModel).to(beNil())
    }
}
