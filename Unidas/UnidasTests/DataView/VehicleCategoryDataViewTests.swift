//
//  VehicleCategoryDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 10/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class VehicleCategoryDataViewTests: XCTestCase {

    // MARK : computed -> formattedGroupName
    func testShouldFormattedGroupNameWhenModelIsCorrect() {
        let model = VehMakeModel(name: "HATCH ECONOMICO 1.0", code: "ECMM", vehiclesGroups: "Mob e Similares", codeCategory: "B")
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
        let localizedString = NSLocalizedString("Group %@ - %@", comment: "Group name")
        let groupNameExpected = String(format: localizedString, "ECMM", "HATCH ECONOMICO 1.0")
    
        expect(vehicleCategoryDataView.formattedGroupName).to(equal(groupNameExpected))
    }
    
    func testShouldNilFormattedGroupNameWhenModelIsIncorrect() {
        let model = VehMakeModel(name: nil, code: nil, vehiclesGroups: nil, codeCategory: nil)
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
    
        expect(vehicleCategoryDataView.formattedGroupName).to(beNil())
    }
    
    func testShouldNilFormattedGroupNameWhenModelNameIsNil() {
        let model = VehMakeModel(name: nil, code: "ECMM", vehiclesGroups: nil, codeCategory: nil)
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
        
        expect(vehicleCategoryDataView.formattedGroupName).to(beNil())
    }
    
    func testShouldNilFormattedGroupNameWhenModelCodeIsNil() {
        let model = VehMakeModel(name: "HATCH ECONOMICO 1.0", code: nil, vehiclesGroups: nil, codeCategory: nil)
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
        
        expect(vehicleCategoryDataView.formattedGroupName).to(beNil())
    }
 
    // MARK : computed -> vehicles
    func testShouldVehiclesNilWhenModelIsCorrect() {
        let model = VehMakeModel(name: nil, code: "ECMM", vehiclesGroups: nil, codeCategory: nil)
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
        
        expect(vehicleCategoryDataView.vehicles).to(beNil())
    }
    
    func testShouldVehiclesWhenModelIsCorrect() {
        let model = VehMakeModel(name: "HATCH ECONOMICO 1.0", code: "ECMM", vehiclesGroups: "Mob e Similares", codeCategory: "")
        let vehicleCategoryDataView = VehicleCategoryDataView(model: model)
        let vehiclesExpected = "HATCH ECONOMICO 1.0"
        
        expect(vehicleCategoryDataView.groupName).to(equal(vehiclesExpected))
    }
}
