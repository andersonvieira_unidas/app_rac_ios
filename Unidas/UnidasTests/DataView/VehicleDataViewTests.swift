//
//  VehicleDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class VehicleDataViewTests: XCTestCase {
    
    private func createModel(vehicleGroup: VehicleGroup?, vehicleTelemetryInformation: VehicleTelemetryInformation?) -> VehicleDataView {
        let vehicleModelManufacture = VehicleModelManufacture(id: 1, name: "Manufacture")
        let vehicleModel = VehicleModel(id: 12345, name: "", illustrativePicture: URL(string: ""), manufacture: vehicleModelManufacture)
        let vehicle = Vehicle(id: 1234, licensePlate: "ABC1234", model: vehicleModel, lastUpdatedTelemetryInformation: vehicleTelemetryInformation, group: vehicleGroup, characteristics: nil, reservationId: nil, fuelLevel: 10.0)
        
        return VehicleDataView(model: vehicle)
    }
    
    // MARK : computed -> vehicleModel
     func testShouldVehicleModelWhenModelContainsValue() {
        let model = createModel(vehicleGroup: nil, vehicleTelemetryInformation: nil)
        expect(model.vehicleModel).toNot(beNil())
    }
    
    // MARK : computed -> licensePlateWithLabel
    func testShouldReturnTotalValueWhenModelContainsValue() {
        let licencePlateString = NSLocalizedString("License Plate: %@", comment: "License plate")
        let expected = String(format: licencePlateString, "ABC1234")
        
        let model = createModel(vehicleGroup: nil, vehicleTelemetryInformation: nil)
        expect(model.licensePlateWithLabel).to(equal(expected))
    }
    
    // MARK : computed -> group
    func testShouldReturnGroupWhenModelContainsValue() {
        let vehicleGroup = VehicleGroup(id: "Group")
        let model = createModel(vehicleGroup: vehicleGroup, vehicleTelemetryInformation: nil)
        
        expect(model.group).toNot(beNil())
    }
    
    func testShouldReturnNilGroupWhenModelNotContainsValue() {
        let model = createModel(vehicleGroup: nil, vehicleTelemetryInformation: nil)
        expect(model.group).to(beNil())
    }
    
    // MARK : computed -> telemetryInformation
    func testShouldReturnTelemetryInformationWhenModelContainsValue() {
        let location = Location(latitude: -23.45588, longitude: -43.55445)
        let vehicleTelemetryInformation = VehicleTelemetryInformation(estimatedFuelLevel: 10.0, odometer: 10, location: location)
        let model = createModel(vehicleGroup: nil, vehicleTelemetryInformation: vehicleTelemetryInformation)
        expect(model.telemetryInformation).toNot(beNil())
    }
    
    func testShouldReturnNilTelemetryInformationWhenModelNotContainsValue() {
        let model = createModel(vehicleGroup: nil, vehicleTelemetryInformation: nil)
        expect(model.telemetryInformation).to(beNil())
    }
}
