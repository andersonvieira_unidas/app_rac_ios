//
//  VehicleTelemetryInformationDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 17/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class VehicleTelemetryInformationDataViewTests: XCTestCase {

    var model: VehicleTelemetryInformationDataView!
    
    override func setUp() {
        let location = Location(latitude: -23.444488, longitude: -46.777799)
        let vehicleTelemetryInformation = VehicleTelemetryInformation(estimatedFuelLevel: 10, odometer: 3, location: location)
        model = VehicleTelemetryInformationDataView(model: vehicleTelemetryInformation)
    }
    
    // MARK : computed -> estimatedFuelLevel
    func testEstimatedFuelLevelWhenModelContainsValue() {
        let expected = "1.000%"
        expect(self.model.estimatedFuelLevel).to(equal(expected))
    }
    
    // MARK : computed -> estimatedFuelLevelDescription
    func testEstimatedFuelLevelDescriptionWhenModelContainsValue() {
        let localizedString = NSLocalizedString("%@ of fuel", comment: "Fuel level description")
        let expected = String(format: localizedString, "1.000%")
        expect(self.model.estimatedFuelLevelDescription).to(equal(expected))
    }
}
