//
//  LoyaltyProgramDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 30/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class LoyaltyProgramDataViewTests: XCTestCase {
    
    var model: LoyaltyProgramDataView!
    
    override func setUp() {
        let loyaltyProgram = LoyaltyProgram(categoryCode: 1, categoryDescription: "CATEGORY DESCRIPTION", accumulatedPoints: 100, availablePoints: 100, nextCategoryCode: 2, nextCategoryDescription: "PRATA", nextCategoryPoints: 200, minimumBalanceNextCategory: 200)
        model = LoyaltyProgramDataView(model: loyaltyProgram)
    }
    
    // MARK : computed -> categoryDescription
    func testShouldReturnCategoryDescriptionContainsValue() {
        let expected = "CATEGORY DESCRIPTION"
        expect(self.model.categoryDescription).to(equal(expected))
    }
    
    func testShouldReturnNilCategoryContainsNilValue() {
        let loyaltyProgram = LoyaltyProgram(categoryCode: 1, categoryDescription: nil, accumulatedPoints: 100, availablePoints: 100, nextCategoryCode: 2, nextCategoryDescription: nil, nextCategoryPoints: 200, minimumBalanceNextCategory: 200)
        let modelCopy = LoyaltyProgramDataView(model: loyaltyProgram)
        expect(modelCopy.categoryDescription).to(beNil())
    }
    
    // MARK : computed -> accumulatedPoints
    func testShouldReturnAccumulatedPointsContainsValue() {
        let expected = "100"
        expect(self.model.accumulatedPoints).to(equal(expected))
    }
    
    // MARK : computed -> accumulatedPointsPercent
    func testShouldReturnAccumulatedPointsPercentContainsValue() {
        let expected = Float(0.5)
        expect(self.model.accumulatedPointsPercent).to(equal(expected))
    }
    
    // MARK : computed -> availablePoints
    func testShouldReturnAvailablePointsContainsValue() {
        let expected = "100"
        expect(self.model.availablePoints).to(equal(expected))
    }
    
    // MARK : computed -> formattedAvailablePoints
    func testShouldReturnFormattedAvailablePointsContainsValue() {
        let expected = "100 pontos"
        expect(self.model.formattedAvailablePoints).to(equal(expected))
    }
    
    // MARK : computed -> nextCategoryPoints
    func testShouldReturnNextCategoryPointsContainsValue() {
        let expected = "200 pontos até o próximo nível"
        expect(self.model.nextCategoryPoints).to(equal(expected))
    }
    
    // MARK : computed -> minimumBalanceNextCategory
    func testShouldReturnMinimumBalanceNextCategoryContainsValue() {
        let expected = "200"
        expect(self.model.minimumBalanceNextCategory).to(equal(expected))
    }
}
