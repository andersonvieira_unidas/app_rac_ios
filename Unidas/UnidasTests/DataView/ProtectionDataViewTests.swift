//
//  ProtectionDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ProtectionDataViewTests: XCTestCase {

    var model: ProtectionDataView!
    
    override func setUp() {
        
        let optionEncompassesTyes : [ProtectionType] = [ProtectionType.vehicle]
        let costs = Costs(daily: 40.0)
        let participationGroups: [ProtectionParticipationGroup] = [ProtectionParticipationGroup(groups: "", value: 10.0)]
        let protectionDetails = ProtectionDetails(iconURL: URL(string: "http://unidas.com.br")!, description: "Details", requiredParticipationGroups: participationGroups)
        let protection = Protection(id: "", title: "Protection", isDefaultOption: true, disclaimer: "Disclaimer",
                           optionEncompassesTypes: optionEncompassesTyes, type: .vehicle, price: 100.0, costs: costs, details: protectionDetails)
        
        model = ProtectionDataView(model: protection)
    }
    
    private func createModel(price: Double?, dailyCosts: Costs?, details: ProtectionDetails?, disclaimer: String?) -> ProtectionDataView{
        let optionEncompassesTyes : [ProtectionType] = [ProtectionType.vehicle]
        let protection = Protection(id: "", title: "Protection", isDefaultOption: true, disclaimer: disclaimer,
                                    optionEncompassesTypes: optionEncompassesTyes, type: .vehicle, price: price, costs: dailyCosts, details: details)
        return ProtectionDataView(model: protection)
    }
    
    // MARK : computed -> title
    func testShouldTitleValueWhenModelContainsValue() {
        let expected = "Protection"
        expect(self.model.title).to(equal(expected))
    }
    
    // MARK : computed -> price
    func testShouldPriceValueWhenModelContainsValue() {
        let expected = "R$ 100,00"
        expect(self.model.price).to(equal(expected))
    }

    func testShouldPriceValueNilWhenModelNotContainsValue() {
        model = createModel(price: nil, dailyCosts: nil , details: nil, disclaimer: nil)
        expect(self.model.price).to(beNil())
    }
    
    // MARK : computed -> promotionalDailyCostsText
    func testShouldPromotionalDailyCostsTextValueWhenModelContainsValue() {
        let expected = "R$ 40,00 diaria"
        expect(self.model.promotionalDailyCostsText).to(equal(expected))
    }
    
    func testShouldPromotionalDailyCostsTextNilWhenModelNotContainsValue() {
        model = createModel(price: 100.0, dailyCosts: nil , details: nil, disclaimer: nil)
        expect(self.model.promotionalDailyCostsText).to(beNil())
    }
    
    // MARK : computed -> hasDetails
    func testShouldHasDetailsTextValueWhenModelContainsValue() {
        expect(self.model.hasDetails).to(beTrue())
    }
    
    func testShouldHasDetailsTextNilWhenModelNotContainsValue() {
        model = createModel(price: 100.0, dailyCosts: nil, details: nil, disclaimer: nil)
        expect(self.model.hasDetails).to(beFalse())
    }
    
    // MARK : computed -> details
    func testShouldDetailsTextValueWhenModelContainsValue() {
        let expected = "Details"
        expect(self.model.details?.description).to(equal(expected))
    }
    
    func testShouldDetailsTextNilWhenModelNotContainsValue() {
        model = createModel(price: 100.0, dailyCosts: nil, details: nil, disclaimer: nil)
        expect(self.model.details).to(beNil())
    }
    
    // MARK : computed -> disclaimer
    func testShouldDisclaimerTextValueWhenModelContainsValue() {
        let expected = "Disclaimer"
        expect(self.model.disclaimer).to(equal(expected))
    }
    
    func testShouldDisclaimerTextNilWhenModelNotContainsValue() {
        model = createModel(price: 100.0, dailyCosts: nil, details: nil, disclaimer: nil)
        expect(self.model.disclaimer).to(beNil())
    }
    
    // MARK : computed -> type
    func testShouldTypeValueWhenModelContainsValue() {
        expect(self.model.type).toNot(beNil())
    }
}
