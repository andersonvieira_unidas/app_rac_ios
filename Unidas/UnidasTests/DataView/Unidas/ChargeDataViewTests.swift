//
//  ChargeDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 23/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class ChargeDataViewTests: XCTestCase {
    
    var model: ChargeDataView!
    
    override func setUp() {
        let charge = Charge(amount: 120, unitCharge: 10, unitChargePer: 20, unitName: "UNIT NAME", quantity: 10, percentage: 0.10, unitPrePayment: 110)
        model = ChargeDataView(model: charge)
    }
    
    // MARK : computed -> amount
    func testShouldReturnAmountContainsValue() {
        let expected = "R$ 120,00"
        expect(self.model.amount!).to(equal(expected))
    }
    
    func testShouldReturnNilAmountContainsNilValue() {
        let charge = Charge(amount: nil, unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil)
        let modelCopy = ChargeDataView(model: charge)
        expect(modelCopy.amount).to(beNil())
    }
    
    // MARK : computed -> calculation
    func testShouldReturnCalculationNotNil() {
        expect(self.model.calculation).toNot(beNil())
        expect(self.model.calculation?.unitCharge).to(equal("R$ 10,00"))
        expect(self.model.calculation?.quantity).to(equal("10"))
        expect(self.model.calculation?.unitPrePaymentValue).to(equal(110))
    }
}
