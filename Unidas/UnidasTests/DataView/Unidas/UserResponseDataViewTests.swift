//
//  UserResponseDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Simões Vieira on 30/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class UserResponseDataViewTests: XCTestCase {
    
    var model: UserResponseDataView!
    
    override func setUp() {
        let userResponse = createModel(unidas: nil)
        model = UserResponseDataView(model: userResponse)
    }

    // MARK : computed -> account
    func testShouldReturnAccountContainsValue() {
        expect(self.model.account).toNot(beNil())
    }
    
    // MARK : computed -> userUnidas
    func testShouldReturnNilUserUnidasNotContainsValue() {
        expect(self.model.userUnidas).to(beNil())
    }
    
    func testShouldReturnUserUnidasContainsValue() {
        let userUnidas = UserUnidas(contaClienteId: 1, flgAreaLogada: true, contaStatusId: .userValidate, contaStatusDescricao: nil, ContaClienteTipoDescricao: nil, clidoc: nil, contaClienteNome: nil, contaClienteNomeMae: nil, contaClienteNascimento: nil, contaClienteTelResDDD: nil, contaClienteTelRes: nil, contaClienteAutenticacaoChave: nil, contaClienteLoginEmail: nil, contaClienteLoginEmailAtivo: nil, contaClienteLoginEmailMascarado: nil, contaClienteLoginEmailValidacao: nil, contaClienteLoginCelularDDD: nil, contaClienteLoginCelular: nil, contaClienteLoginCelularValidacao: nil, contaClienteLoginCelularMascarado: nil, contaClienteLoginCelularDeviceID: nil, contaClienteEnderecoResidencialPais: nil, contaClienteEnderecoResidencialCEP: nil, contaClienteEnderecoResidencialLogradouro: nil, contaClienteEnderecoResidencialLogradouroNumero: nil, contaClienteEnderecoResidencialLogradouroComplemento: nil, contaClienteEnderecoResidencialBairro: nil, contaClienteEnderecoResidencialCidade: nil, contaClienteEnderecoResidencialUF: nil, contaClienteToken: nil, contaClienteAlerta: nil, emailConfirmado: true)
        let userResponse = createModel(unidas: userUnidas)
        model = UserResponseDataView(model: userResponse)
        expect(self.model.userUnidas).toNot(beNil())
    }
    
    private func createModel(unidas: UserUnidas?) -> UserResponse {
        let account = Account(id: 1, user: 123, documentNumber: "11111111111", fcm: nil, mobilePhone: "119999999", email: "teste@unidas.com.br", name: "Cliente Unidas", companyName: nil, deviceId: nil, profileImage: nil, birthDate: nil, motherName: nil)
        if let userUnidas = unidas {
            return UserResponse(token: "token123", account: account, unidas: [userUnidas])
        }
        return UserResponse(token: "token123", account: account, unidas: nil)
    }
}
