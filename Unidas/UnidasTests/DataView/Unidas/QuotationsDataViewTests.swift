//
//  QuotationsDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Simões Vieira on 30/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class QuotationsDataViewTests: XCTestCase {
    
    var model: QuotationsDataView!
    
    // MARK : computed -> group
    func testShouldReturnGroupContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: nil, vehicleCategory: "Vehicle Category", vehicleGroups: "Vehicle Groups")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        let expected = "Vehicle Category"
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.group!).to(equal(expected))
    }
    
    func testShouldReturnNilGroupNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.group).to(beNil())
    }
    
    // MARK : computed -> formattedGroup
    func testShouldReturnFormattedGroupContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: nil, vehicleCategory: "Vehicle Category", vehicleGroups: "Vehicle Groups")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        
        let localizedString = NSLocalizedString("%@ group", comment: "Formatted group label")
        let expected = String(format: localizedString, "Vehicle Category")
        
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.formattedGroup!).to(equal(expected))
    }
    
    func testShouldReturnNilFormattedGroupNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.formattedGroup).to(beNil())
    }
    
    // MARK : computed -> vehicle
    func testShouldReturnVehicleContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: "Ford - KA", vehicleCategory: "", vehicleGroups: "")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        let expected = "KA"
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.vehicle!).to(equal(expected))
    }
    
    func testShouldReturnNilVehicleNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.vehicle).to(beNil())
    }
    
    // MARK : computed -> description
    func testShouldReturnDescriptionContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: "Ford - KA", vehicleCategory: "", vehicleGroups: "")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        let expected = "Ford"
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.description!).to(equal(expected))
    }
    
    func testShouldReturnNilDescriptionNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.description).to(beNil())
    }
    
    // MARK : computed -> vehicleGroups
    func testShouldReturnVehicleGroupsContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: nil, vehicleCategory: "Vehicle Category", vehicleGroups: "Vehicle Groups")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        let expected = "Vehicle Groups"
        
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.vehicleGroups!).to(equal(expected))
    }
    
    func testShouldReturnNilVehicleGroupsNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.vehicleGroups).to(beNil())
    }
    
    // MARK : computed -> formattedGroupWithDescription
    func testShouldReturnFormattedGroupWithDescriptionContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: nil, name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        let localizedString = NSLocalizedString("Group %@ - %@", comment: "Group name")
        let expected = String(format: localizedString, "E", "Ford")
        
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.formattedGroupWithDescription!).to(equal(expected))
    }
    
    func testShouldReturnNilFormattedGroupWithDescriptionNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.formattedGroupWithDescription).to(beNil())
    }
    
    // MARK : computed -> picture
    func testShouldReturnPictureContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: URL(string: "http://image.jpg"), name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        model = QuotationsDataView(model: vehAvail)
        let expected = "http://image.jpg"
        
        expect(self.model.picture).toNot(beNil())
        expect(self.model.picture!.absoluteString).to(equal(expected))
    }
    
    func testShouldReturnNilPictureNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.picture).to(beNil())
    }
    
    // MARK : computed -> valueTotalDiary
    func testShouldReturnValueTotalDiaryContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.valueTotalDiary).to(equal(0.0))
    }
    
    // MARK : computed -> valueTotalDiaryString
    func testShouldReturnValueTotalDiaryStringContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.valueTotalDiaryString!).to(equal("R$ 0,00"))
    }
    
    // MARK : computed -> numberOfDays
    func testShouldReturnNumberOfDaysContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: URL(string: "http://image.jpg"), name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let vehicleCharge = VehicleCharge(amount: 10.0, taxInclusive: nil, guaranteedInd: nil, purpose: nil, unitCharge: 10.0, unitChargePer: 10.0, unitName: nil, quantity: 1.0, percentage: 0.0, unitPrePayment: 10.2, unitChargeTotal: 10.0, unitChargeHourExtra: 0.0)
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: vehicleCharge, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        
        model = QuotationsDataView(model: vehAvail)

        let expected = "1 dias"
        expect(self.model.numberOfDays!).to(equal(expected))
    }
    
    func testShouldReturnNilNumberOfDaysNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        let expected = "0 dias"
        expect(self.model.numberOfDays).to(equal(expected))
    }
    
    // MARK : computed -> vehicleChargeCalculation
    func testShouldReturnVehicleChargeCalculationContainsValue() {
        
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: URL(string: "http://image.jpg"), name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let vehicleCharge = VehicleCharge(amount: 10.0, taxInclusive: nil, guaranteedInd: nil, purpose: nil, unitCharge: 10.0, unitChargePer: 10.0, unitName: nil, quantity: 1.0, percentage: 0.0, unitPrePayment: 10.2, unitChargeTotal: 10.0, unitChargeHourExtra: 0.0)
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: vehicleCharge, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        
        model = QuotationsDataView(model: vehAvail)
        expect(self.model.vehicleChargeCalculation).toNot(beNil())
    }
    
    // MARK : computed -> returnTaxValue
    func testShouldReturnReturnTaxValueContainsValue() {
        
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: URL(string: "http://image.jpg"), name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let fees = [Fee(amount: 10.25, taxInclusive: false, description: nil, purpose: "19", unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil)]
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: fees, pricedCoverages: nil)
        
        model = QuotationsDataView(model: vehAvail)
        let expected = "R$ 10,25"
        expect(self.model.returnTaxValue!).to(equal(expected))
    }
    
    func testShouldReturnNilReturnTaxValueNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        let expected = "R$ 0,00"
        expect(self.model.returnTaxValue).to(equal(expected))
    }
    
    // MARK : computed -> extraHourTax
    func testShouldReturnExtraHourTaxValueContainsValue() {
        let vehicle = UnidasVehicle(airConditionInd: nil, passengerQuantity: nil, code: nil, pictureURL: URL(string: "http://image.jpg"), name: "Ford - KA", vehicleCategory: "E", vehicleGroups: "Group")
        let vehicleCharge = VehicleCharge(amount: 10.0, taxInclusive: nil, guaranteedInd: nil, purpose: nil, unitCharge: 10.0, unitChargePer: 10.0, unitName: nil, quantity: 1.0, percentage: 0.0, unitPrePayment: 10.2, unitChargeTotal: 10.0, unitChargeHourExtra: 1.0)
        let vehAvail = VehAvail(status: nil, vehicle: vehicle, rateDistance: nil, vehicleCharge: vehicleCharge, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
        
        model = QuotationsDataView(model: vehAvail)
        let expected = "R$ 1,00"
        expect(self.model.extraHourTax!).to(equal(expected))
    }
    
    func testShouldReturnNilExtraHourTaxNotContainsValue() {
        model = QuotationsDataView(model: createModelWithNilValues())
        expect(self.model.extraHourTax).to(beNil())
    }
    
    private func createModelWithNilValues() -> VehAvail{
        return VehAvail(status: nil, vehicle: nil, rateDistance: nil, vehicleCharge: nil, rateQualifier: nil, pricedEquips: nil, fees: nil, pricedCoverages: nil)
    }
}
