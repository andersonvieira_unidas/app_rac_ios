//
//  UserInValidateDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Simões Vieira on 30/12/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class UserInValidateDataViewTests: XCTestCase {
    
    override func setUp() {
    }
    
    // MARK : computed -> documentLicenceInfo
    func testShouldReturnDocumentLicenseInfoContainsValue() {
        let userCheck = UserCheck(mobilePhoneIsValid: nil, emailIsValid: nil, serasaIsValid: nil, credDefenseIsValid: nil, documentLicenceIsValid: false, signatureIsValid: nil, paymentIsValid: true, accountIsValid: nil, selfieIsValid: true, financialPendencie: false)
        let model = UserInValidateDataView(model: userCheck)
        let expected = NSLocalizedString("Pendencies Document Number", comment: "Pendencies Document Number description")
        expect(model.documentLicenceInfo!).to(equal(expected))
    }
    
    func testShouldReturnNilDocumentLicenseInfoNotContainsValue() {
        let model = UserInValidateDataView(model: modelAllNil())
        expect(model.documentLicenceInfo).to(beNil())
    }
    
    // MARK : computed -> emailInfo
    func testShouldReturnEmailInfoContainsValue() {
        let userCheck = UserCheck(mobilePhoneIsValid: nil, emailIsValid: false, serasaIsValid: nil, credDefenseIsValid: nil, documentLicenceIsValid: nil, signatureIsValid: nil, paymentIsValid: true, accountIsValid: nil, selfieIsValid: true, financialPendencie: false)
        let model = UserInValidateDataView(model: userCheck)
        let expected = NSLocalizedString("Pendencies E-mail", comment: "Pendencies E-mail description")
        expect(model.emailInfo!).to(equal(expected))
    }
    
    func testShouldReturnNilEmailInfoNotContainsValue() {
        let model = UserInValidateDataView(model: modelAllNil())
        expect(model.emailInfo).to(beNil())
    }
    
    // MARK : computed -> signatureInfo
    func testShouldReturnSignatureInfoContainsValue() {
        let userCheck = UserCheck(mobilePhoneIsValid: nil, emailIsValid: nil, serasaIsValid: nil, credDefenseIsValid: nil, documentLicenceIsValid: nil, signatureIsValid: false, paymentIsValid: true, accountIsValid: nil, selfieIsValid: true, financialPendencie: false)
        let model = UserInValidateDataView(model: userCheck)
        let expected = NSLocalizedString("Pendencies Signature", comment: "Pendencies Signature description")
        expect(model.signatureInfo!).to(equal(expected))
    }
    
    func testShouldReturnNilSignatureInfoNotContainsValue() {
        let model = UserInValidateDataView(model: modelAllNil())
        expect(model.signatureInfo).to(beNil())
    }
    
    // MARK : computed -> mobileInfo
    func testShouldReturnMobileInfoContainsValue() {
        let userCheck = UserCheck(mobilePhoneIsValid: false, emailIsValid: nil, serasaIsValid: nil, credDefenseIsValid: nil, documentLicenceIsValid: nil, signatureIsValid: nil, paymentIsValid: true, accountIsValid: nil, selfieIsValid: true, financialPendencie: false)
        let model = UserInValidateDataView(model: userCheck)
        let expected = NSLocalizedString("Pendencies Mobile", comment: "Pendencies Mobile description")
        expect(model.mobileInfo!).to(equal(expected))
    }
    
    func testShouldReturnNilMobileInfoNotContainsValue() {
        let model = UserInValidateDataView(model: modelAllNil())
        expect(model.mobileInfo).to(beNil())
    }
    
    private func modelAllNil() -> UserCheck{
        return UserCheck(mobilePhoneIsValid: nil, emailIsValid: nil, serasaIsValid: nil, credDefenseIsValid: nil, documentLicenceIsValid: nil, signatureIsValid: nil, paymentIsValid: true, accountIsValid: nil, selfieIsValid: true, financialPendencie: false)
    }
}
