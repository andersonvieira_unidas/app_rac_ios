//
//  FeeDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 23/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class FeeDataViewTests: XCTestCase {
    
    var model: FeeDataView!
    
    override func setUp() {
        let fee = Fee(amount: 120, taxInclusive: true, description: "description", purpose: nil, unitCharge: 10.0, unitChargePer: 12.0, unitName: "Unit Name", quantity: 1, percentage: 0.2, unitPrePayment: 10)
        model = FeeDataView(model: fee)
    }
    
    // MARK : computed -> amount
    func testShouldReturnAmountContainsValue() {
        let expected = "R$ 120,00"
        expect(self.model.amount).to(equal(expected))
    }
    
    func testShouldReturnNilAmountContainsNilValue() {
        let fee = Fee(amount: nil, taxInclusive: true, description: "description", purpose: nil, unitCharge: 10.0, unitChargePer: 12.0, unitName: "Unit Name", quantity: 1, percentage: 0.2, unitPrePayment: 10)
        let modelCopy = FeeDataView(model: fee)
        expect(modelCopy.amount).to(beNil())
    }
    
    // MARK : computed -> description
    func testShouldReturnDescriptionContainsValue() {
        let expected = "Description"
        expect(self.model.description).to(equal(expected))
    }
    
    func testShouldReturnNilDescriptionContainsNilValue() {
        let fee = Fee(amount: 120, taxInclusive: true, description: nil, purpose: nil, unitCharge: 10.0, unitChargePer: 12.0, unitName: "Unit Name", quantity: 1, percentage: 0.2, unitPrePayment: 10)
        let modelCopy = FeeDataView(model: fee)
        expect(modelCopy.description).to(beNil())
    }
    
    // MARK : computed -> calculation
    func testShouldReturnCalculationNotNil() {
        expect(self.model.calculation).toNot(beNil())
        expect(self.model.calculation?.unitCharge).to(equal("R$ 10,00"))
        expect(self.model.calculation?.quantity).to(equal("1"))
        expect(self.model.calculation?.unitPrePaymentValue).to(equal(10))
    }
}
