//
//  ProtectionsDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 23/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ProtectionsDataViewTests: XCTestCase {
    
    var model: ProtectionsDataView!
    
    override func setUp() {
        let details: [Details] = [Details(coverageTextType: "coverage text type", text: "text details"), Details(coverageTextType: "Description", text: "Description text")]
        let pricedCoverages = PricedCoverages(coverageType: "", details: details, amount: 10.0, unitCharge: 11.0, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        model = ProtectionsDataView(model: pricedCoverages)
    }
    
    // MARK : computed -> name
    func testShouldReturnNameContainsValue() {
        let expected = "text details"
        expect(self.model.name).to(equal(expected))
    }
    
    func testShouldReturnNilNameContainsNilValue() {
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: 10.0, unitCharge: 11.0, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.name).to(beNil())
    }
    
    // MARK : computed -> value
    func testShouldReturnValueContainsValue() {
        let expected = "R$ 11,00"
        expect(self.model.value).to(equal(expected))
    }
    
    func testShouldReturnZeroValueContainsNilValue() {
        let expected = "R$ 0,00"
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: 10.0, unitCharge: nil, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.value).to(equal(expected))
    }
    
    // MARK : computed -> description
    func testShouldReturnDescriptionValueContainsValue() {
        let expected = "Description text"
        expect(self.model.description).to(equal(expected))
    }
    
    func testShouldReturnNilDescriptionContainsNilValue() {
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: 10.0, unitCharge: nil, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.description).to(beNil())
    }
    
    // MARK : computed -> hasDescription
    func testShouldReturnTrueHasDescriptionContainsValue() {
        expect(self.model.hasDescription).to(beTrue())
    }
    
    func testShouldReturnFalseHasDescriptionNotContainsValue() {
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: 10.0, unitCharge: nil, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.hasDescription).to(beFalse())
    }
    
    // MARK : computed -> valueDescription
    func testShouldReturnValueDescriptionValueContainsValue() {
        let expected = "R$ 11,00 por diária"
        expect(self.model.valueDescription).to(equal(expected))
    }
    
    func testShouldReturnNilValueDescriptionContainsNilValue() {
        let expected = "R$ 0,00 por diária"
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: 10.0, unitCharge: nil, unitChargePer: 12.0, unitName: "Unit name", quantity: 1.0, percentage: 1.0, unitPrePayment: 1.0)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.valueDescription).to(equal(expected))
    }
    
    // MARK : computed -> totalValue
    func testShouldReturnTotalValueContainsValue() {
        let expected = "R$ 10,00"
        expect(self.model.totalValue).to(equal(expected))
    }
    
    func testShouldReturnNilTotalValueContainsNilValue() {
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: nil, unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.totalValue).to(beNil())
    }
    
    // MARK : computed -> charge
    func testShouldReturnChargeValueContainsValue() {
        expect(self.model.charge).toNot(beNil())
    }
    
    func testShouldReturnNilChargeValueContainsNilValue() {
        let pricedCoverages = PricedCoverages(coverageType: "", details: nil, amount: nil, unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil)
        let modelCopy = ProtectionsDataView(model: pricedCoverages)
        expect(modelCopy.charge?.amount).to(beNil())
    }
}
