//
//  CalculationDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 01/11/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas
class CalculationDataViewTests: XCTestCase {
    
    // MARK : computed -> unitCharge
    func testShouldReturnUnitCharge5daysContainsValue() {
        let model = generateBasicReservationWith5Days()
        let expected = "R$ 60,41"
        expect(model.unitCharge).to(equal(expected))
    }
    
    func testShouldReturnUnitCharge10daysContainsValue() {
        let model = generateBasicReservationWith10Days()
        let expected = "R$ 42,84"
        expect(model.unitCharge).to(equal(expected))
    }
    
    func testShouldReturnUnitCharge27daysContainsValue() {
        let model = generateBasicReservationWith27Days()
        let expected = "R$ 56,39"
        expect(model.unitCharge).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeNilContainsNilValue() {
        let calculation = Calculation(unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil, unitChargeTotal: nil, unitChargeHourExtra: nil)
        let modelCopy = CalculationDataView(model: calculation)
        let expected = "R$ 0,00"
        expect(modelCopy.unitCharge).to(equal(expected))
    }
    
    // MARK : computed -> unitChargeValue
    func testShouldReturnUnitChargeValue5daysContainsValue() {
        let model = generateBasicReservationWith5Days()
        let expected = 60.414
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeValue10daysContainsValue() {
        let model = generateBasicReservationWith10Days()
        let expected = 42.843
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeValue27daysContainsValue() {
        let model = generateBasicReservationWith27Days()
        let expected = 56.39
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    // MARK : computed -> unitChargeTotalValue
    func testShouldReturnUnitChargeTotalValue5daysContainsValue() {
        let model = generateBasicReservationWith5Days()
        let expected = 60.414
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeTotalValue10daysContainsValue() {
        let model = generateBasicReservationWith10Days()
        let expected = 42.843
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeTotalValue27daysContainsValue() {
        let model = generateBasicReservationWith27Days()
        let expected = 56.39
        expect(model.unitChargeValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeTotalValueZeroContainsValue() {
        let calculation = Calculation(unitCharge: nil, unitChargePer: 0.0, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        let expected = 0.0
        expect(modelCopy.unitChargeValue).to(equal(expected))
    }
    
    // MARK : computed -> unitChargeHourExtra
    func testShouldReturnUnitChargeHourExtraContainsValue() {
        let expected = "R$ 10,12"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 0.0, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargeHourExtra).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeHourExtraNilContainsNilValue() {
        let calculation = Calculation(unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil, unitChargeTotal: nil, unitChargeHourExtra: nil)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargeHourExtra).to(beNil())
    }
    
    // MARK : computed -> unitChargeHourExtraValue
    func testShouldReturnUnitChargeHourExtraValueContainsValue() {
        let expected = 10.123
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 0.0, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargeHourExtraValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargeHourExtraValueNilContainsNilValue() {
        let calculation = Calculation(unitCharge: nil, unitChargePer: nil, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: nil, unitChargeTotal: nil, unitChargeHourExtra: nil)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargeHourExtraValue).to(beNil())
    }
    
    // MARK : computed -> unitPrePaymentValue
    func testShouldReturnUnitPrePaymentValue5daysContainsValue() {
        let model = generateBasicReservationWith5Days()
        let expected = 58.60158
        expect(model.unitPrePaymentValue).to(equal(expected))
    }
    
    func testShouldReturnUnitPrePaymentValue10daysContainsValue() {
        let model = generateBasicReservationWith10Days()
        let expected = 41.55771
        expect(model.unitPrePaymentValue).to(equal(expected))
    }
    
    func testShouldReturnUnitPrePaymentValue27daysContainsValue() {
        let model = generateBasicReservationWith27Days()
        let expected = 54.6983
        expect(model.unitPrePaymentValue).to(equal(expected))
    }
    
    // MARK : computed -> totalPrePaymentValue
    func testShouldReturnTotalPrePaymentValue5daysContainsValue() {
        let model = generateBasicReservationWith5Days()
        let expected = 293.0079
        expect(model.totalPrePaymentValue).to(equal(expected))
    }
    
    func testShouldReturnTotalPrePaymentValue10daysContainsValue() {
        let model = generateBasicReservationWith10Days()
        let expected = 415.5771
        expect(model.totalPrePaymentValue).to(equal(expected))
    }
    
    func testShouldReturnTotalPrePaymentValue27daysContainsValue() {
        let model = generateBasicReservationWith27Days()
        let expected = 1476.8541
        expect(model.totalPrePaymentValue).to(equal(expected))
    }

    // MARK : computed -> totalDiscount
    func testShouldReturnTotalDiscount5daysContainsValue() {
        let expected = "R$ 16,47"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalDiscount).to(equal(expected))
    }
    
    func testShouldReturnTotalDiscountNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.totalDiscount).to(beNil())
    }
    
    // MARK : computed -> totalDiscountValue
    func testShouldReturnTotalDiscountValueContainsValue() {
        let expected = 16.469999999999985
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalDiscountValue).to(equal(expected))
    }
    
    func testShouldReturnTotalDiscountValueNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.totalDiscountValue).to(beNil())
    }
    
    // MARK : computed -> totalDiscountDescription
    func testShouldReturnTotalDiscountDescriptionContainsValue() {
        let expected = "-R$ 16,47"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalDiscountDescription).to(equal(expected))
    }
    
    func testShouldReturnTotalDiscountDescriptionNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.totalDiscountDescription).to(beNil())
    }
    
    // MARK : computed -> discountPercent
    func testShouldReturnDiscountPercentContainsValue() {
        let expected = "1%"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.discountPercent).to(equal(expected))
    }
    
    func testShouldReturnDiscountPercentNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.discountPercent).to(beNil())
    }
    
    // MARK : computed -> totalDiscountPercentDescription
    func testShouldReturnTotalDiscountPercentDescriptionContainsValue() {
        let expected = "Desconto de 1% sobre o valor da diária. Pague agora e retire seu veículo com o QR Code direto no pátio."
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalDiscountPercentDescription).to(equal(expected))
    }
    
    func testShouldReturnTotalDiscountPercentDescriptionNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.totalDiscountPercentDescription).to(beNil())
    }
    
    // MARK : computed -> discountPercentValue
    func testShouldReturnDiscountPercentValueContainsValue() {
        let expected = 0.010817520837027831
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.discountPercentValue).to(equal(expected))
    }
    
    func testShouldReturnDiscountPercentValueNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.discountPercentValue).to(beNil())
    }
    
    // MARK : computed -> discountValue
    func testShouldReturnDiscountValueContainsValue() {
        let expected = 0.6099999999999994
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.discountValue).to(equal(expected))
    }
    
    func testShouldReturnDiscountValueNilValueContainsNilValue() {
        let model = generateBasicReservationWith10Days()
        expect(model.discountValue).to(beNil())
    }
    
    // MARK : computed -> unitChargePerValue
    func testShouldReturnUnitChargePerValueContainsValue() {
        let expected = 57.0
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargePerValue).to(equal(expected))
    }
    
    func testShouldReturnUnitChargePerValueZeroContainsValue() {
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 0, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargePerValue).to(beNil())
    }
    
    func testShouldReturnUnitChargePerValueNilValueContainsNilValue() {
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: nil, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.unitChargePerValue).to(beNil())
    }
    
    // MARK : computed -> totalCharge
    func testShouldReturnTotalChargeContainsValue() {
        let expected = "R$ 1.522,53"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalCharge).to(equal(expected))
    }
    
    // MARK : computed -> totalChargeValue
    func testShouldReturnTotalChargeValueContainsValue() {
        let expected = 1522.53
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalChargeValue).to(equal(expected))
    }
    
    // MARK : computed -> totalChargeTotalValue
    func testShouldReturnTotalChargeTotalValueContainsValue() {
        let expected = 1522.53
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.totalChargeValue).to(equal(expected))
    }
    
    // MARK : computed -> quantity
    func testShouldReturnQuantity5DaysContainsValue() {
        let expected = "5"
        let model = generateBasicReservationWith5Days()
        expect(model.quantity).to(equal(expected))
    }
    
    func testShouldReturnQuantity10DaysContainsValue() {
        let expected = "10"
        let model = generateBasicReservationWith10Days()
        expect(model.quantity).to(equal(expected))
    }
    
    func testShouldReturnQuantity27DaysContainsValue() {
        let expected = "27"
        let model = generateBasicReservationWith27Days()
        expect(model.quantity).to(equal(expected))
    }
    
    func testShouldReturnQuantity0DaysContainsValue() {
        let expected = "0"
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.quantity).to(equal(expected))
    }
    
    // MARK : computed -> quantityValue
    func testShouldReturnQuantityValue5DaysContainsValue() {
        let expected = 5
        let model = generateBasicReservationWith5Days()
        expect(model.quantityValue).to(equal(expected))
    }
    
    func testShouldReturnQuantityValue10DaysContainsValue() {
        let expected = 10
        let model = generateBasicReservationWith10Days()
        expect(model.quantityValue).to(equal(expected))
    }
    
    func testShouldReturnQuantityValue27DaysContainsValue() {
        let expected = 27
        let model = generateBasicReservationWith27Days()
        expect(model.quantityValue).to(equal(expected))
    }
    
    func testShouldReturnQuantityValue0DaysContainsValue() {
        let expected = 0
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: nil, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.quantityValue).to(equal(expected))
    }
    
    // MARK : computed -> description
    func testShouldReturnDescription5DaysContainsValue() {
        let expected = "5x R$ 60,41"
        let model = generateBasicReservationWith5Days()
       expect(model.description).to(equal(expected))
    }
    
    func testShouldReturnDescription10DaysContainsValue() {
        let expected = "10x R$ 42,84"
        let model = generateBasicReservationWith10Days()
        expect(model.description).to(equal(expected))
    }
    
    func testShouldReturnDescription27DaysContainsValue() {
        let expected = "27x R$ 56,39"
        let model = generateBasicReservationWith27Days()
        expect(model.description).to(equal(expected))
    }
    
    // MARK : computed -> hasDiscount
    func testShouldHasDiscountFalse() {
        let model = generateBasicReservationWith5Days()
        expect(model.hasDiscount).to(beFalse())
    }
    
    func testShouldHasDiscountTrue() {
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.hasDiscount).to(beTrue())
    }
    
    // MARK : computed -> prePaymentDiscountPercentValue
    func testShouldReturnPrePaymentDiscountPercentValueContainsValue() {
        let expected = 0.02999999999999997
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.prePaymentDiscountPercentValue).to(equal(expected))
    }
    
    // MARK : computed -> prePaymentDiscountPercent
    func testShouldReturnPrePaymentDiscountPercentContainsValue() {
        let expected = "Desconto de 3% sobre o valor da diária. Pague agora e retire seu veículo com o QR Code direto no pátio."
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 57.00, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: 10.123)
        let modelCopy = CalculationDataView(model: calculation)
        expect(modelCopy.prePaymentDiscountPercent).to(equal(expected))
    }
    
    // MARK : generates
    private func generateBasicReservationWith5Days() -> CalculationDataView{
        let calculation = Calculation(unitCharge: 60.414, unitChargePer: 0.0, unitName: nil, quantity: 5, percentage: nil, unitPrePayment: 58.60158, unitChargeTotal: nil, unitChargeHourExtra: nil)
        return CalculationDataView(model: calculation)
    }
    
    private func generateBasicReservationWith10Days() -> CalculationDataView{
        let calculation = Calculation(unitCharge: 42.843, unitChargePer: 0.0, unitName: nil, quantity: 10, percentage: nil, unitPrePayment: 41.55771, unitChargeTotal: nil, unitChargeHourExtra: nil)
        return CalculationDataView(model: calculation)
    }
    
    private func generateBasicReservationWith27Days() -> CalculationDataView{
        let calculation = Calculation(unitCharge: 56.39, unitChargePer: 0.0, unitName: nil, quantity: 27, percentage: nil, unitPrePayment: 54.6983, unitChargeTotal: nil, unitChargeHourExtra: nil)
        return CalculationDataView(model: calculation)
    }
}
