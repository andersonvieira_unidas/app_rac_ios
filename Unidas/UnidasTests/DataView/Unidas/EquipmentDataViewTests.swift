//
//  EquipmentDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 03/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class EquipmentDataViewTests: XCTestCase {
    
    var model: EquipmentDataView!
    
    override func setUp() {
        let pricedEquip = getInstanceFromJsonByFileName("priced_equip_object", decodeTo: PricedEquips.self)
        model = EquipmentDataView(model: pricedEquip!)
    }
    
    // MARK : computed -> name
    func testShouldReturnNameContainsValue() {
        let expected = "BEBÊ CONFORTO (0KG A 13KG)"
        expect(self.model.name).to(equal(expected))
    }
    
    // MARK : computed -> charge
    func testShouldReturnChargeContainsValue() {
        expect(self.model.charge).toNot(beNil())
    }
    
    // MARK : computed -> value
    func testShouldReturnValueContainsValue() {
        expect(self.model.value).toNot(beNil())
    }
    
    // MARK : computed -> valueQuantityDescription
    func testShouldReturnValueQuantityDescriptionContainsValue() {
        let expected = "1x R$ 20,00 por diária"
        expect(self.model.valueQuantityDescription).to(equal(expected))
    }
    
    // MARK : computed -> totalValueWithQuantity
    func testShouldReturnTotalValueWithQuantityContainsValue() {
        let expected = "R$ 40,00"
        expect(self.model.totalValueWithQuantity).to(equal(expected))
    }
    
    func testShouldReturnNilTotalValueWithContainsNilValue() {
        let pricedEquip = getInstanceFromJsonByFileName("priced_equip_charge_nil", decodeTo: PricedEquips.self)
        let modelCopy = EquipmentDataView(model: pricedEquip!)
        let expected = "R$ 0,00"
        expect(modelCopy.totalValueWithQuantity).to(equal(expected))
    }
    
    // MARK : computed -> selectedQuantity
    func testShouldReturnSelectedQuantityContainsValue() {
        expect(self.model.selectedQuantity).to(equal(1))
    }
    
    // MARK : computed -> hasQuantity
    func testShouldReturnHasQuantityContainsValue() {
        expect(self.model.hasQuantity).to(beFalse())
    }
}
