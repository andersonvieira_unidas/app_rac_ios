//
//  GarageDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 02/01/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class GarageDataViewTests: XCTestCase {
    
    var model: GarageDataView!
    
    override func setUp() {
        let garageUnidas = getInstanceFromJsonByFileName("garage_airport", decodeTo: GarageUnidas.self)
        model = GarageDataView(model: Garage(garageUnidas: garageUnidas!))
    }
    
    // MARK : computed -> name
    func testShouldReturnNameContainsValue() {
        expect(self.model.name).to(equal("Aeroporto De Guarulhos - Terminal 3"))
    }
    
    // MARK : computed -> code
    func testShouldReturnCodeContainsValue() {
        expect(self.model.code).to(equal("GRU1"))
    }
    
    // MARK : computed -> address
    func testShouldReturnAddressContainsValue() {
        expect(self.model.address).toNot(beNil())
        expect(self.model.address?.city).to(equal("GUARULHOS"))
        expect(self.model.address?.street).to(equal("HÉLIO SMIDT - NÃO É SETOR DE LOCADORA"))
    }
    
    // MARK : computed -> isOpenNow
    func testShouldReturnIsOpenNowIsOpen() {
        expect(self.model.isOpenNow).to(equal("Aberto"))
    }
    
    // MARK : computed -> weekdaysWorkingHours
    func testShouldReturnIsWeekdaysWorkingHours() {
        expect(self.model.weekdaysWorkingHours).to(equal("00:00 - 23:59"))
    }
    
    // MARK : computed -> franchise
    func testShouldReturnFranchiseIsFalse() {
        expect(self.model.isFranchise).to(beFalse())
    }
    
    func testShouldReturnFranchiseIsTrue() {
        let garageUnidas = getInstanceFromJsonByFileName("garage_franchise", decodeTo: GarageUnidas.self)
        let modelCopy = GarageDataView(model: Garage(garageUnidas: garageUnidas!))
        expect(modelCopy.isFranchise).to(beTrue())
    }
    
    // MARK : computed -> saturdayWorkingHours
    func testShouldReturnSaturdayWorkingHours() {
        expect(self.model.saturdayWorkingHours).to(equal("00:00 - 23:59"))
    }
    
    func testShouldReturnSaturdayWorkingHoursClose() {
        let garageUnidas = getInstanceFromJsonByFileName("garage_franchise", decodeTo: GarageUnidas.self)
        let modelCopy = GarageDataView(model: Garage(garageUnidas: garageUnidas!))
        expect(modelCopy.saturdayWorkingHours).to(equal("Fechado"))
    }
    
    // MARK : computed -> sundayWorkingHours
    func testShouldReturnSundayWorkingHours() {
        expect(self.model.sundayWorkingHours).to(equal("00:00 - 23:59"))
    }
    
    func testShouldReturnSundayWorkingHoursClose() {
        let garageUnidas = getInstanceFromJsonByFileName("garage_franchise", decodeTo: GarageUnidas.self)
        let modelCopy = GarageDataView(model: Garage(garageUnidas: garageUnidas!))
        expect(modelCopy.sundayWorkingHours).to(equal("Fechado"))
    }
    
    // MARK : computed -> holidayWorkingHours
    func testShouldReturnHolidayWorkingHours() {
        expect(self.model.holidayWorkingHours).to(equal("00:00 - 23:59"))
    }
    
    func testShouldReturnNilIcon24Hours() {
        let garageUnidas = getInstanceFromJsonByFileName("garage_franchise", decodeTo: GarageUnidas.self)
        let modelCopy = GarageDataView(model: Garage(garageUnidas: garageUnidas!))
        expect(modelCopy.icon24Hours).to(beNil())
    }
}
