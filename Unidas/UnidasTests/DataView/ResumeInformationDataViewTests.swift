//
//  ResumeInformationDataViewTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 11/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class ResumeInformationDataViewTests: XCTestCase {

    // MARK : computed -> title
    func testShouldTitleWhenModelContainsTitle() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: "Title", subtitle: "Subtitle", value: "R$ 100,00"))
        let expected = "Title"
        expect(model.title).to(equal(expected))
    }
    
    func testShouldNilTitleWhenModelNotContainsTitle() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: nil, subtitle: nil, value: nil))
        expect(model.title).to(beNil())
    }
    
    // MARK : computed -> subtitle
    func testShouldSubtitleWhenModelContainsSubtitle() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: "Title", subtitle: "Subtitle", value: "R$ 100,00"))
        let expected = "Subtitle"
        expect(model.subtitle).to(equal(expected))
    }
    
    func testShouldNilSubtitleWhenModelNotContainsSubtitle() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: nil, subtitle: nil, value: "R$ 100,00"))
        expect(model.subtitle).to(beNil())
    }
    
    // MARK : computed -> value
    func testShouldValueWhenModelContainsValue() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: "Title", subtitle: "Subtitle", value: "R$ 100,00"))
        let expected = "R$ 100,00"
        expect(model.value).to(equal(expected))
    }
    
    func testShouldNilValueWhenModelNotContainsValue() {
        let model = ResumeInformationDataView(model: ResumeInformation(title: nil, subtitle: nil, value: nil))
        expect(model.value).to(beNil())
    }
}
