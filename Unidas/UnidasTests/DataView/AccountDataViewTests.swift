//
//  AccountDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 16/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
@testable import Unidas
import PhoneNumberKit
import Nimble
import InputMask

class AccountDataViewTests: XCTestCase {
    
    private func createModel(profileImage: URL?) -> AccountDataView {
        let account = Account(id: nil, user: nil, documentNumber:  "99999999999", fcm: nil, mobilePhone: "1199999999", email: "PEDRO@unidas.com.br", name: "FRODO", companyName: nil, deviceId: nil, profileImage: profileImage, birthDate: nil, motherName: nil)
        return AccountDataView(model: account)
    }
    
    // MARK : computed -> name
    func testShouldLocalizedCapitalizedName() {
        let model = createModel(profileImage: nil)
        let expected = "Frodo"
        expect(model.name).to(equal(expected))
    }
    
    // MARK : computed -> greetings
    func testShouldGreatingsWhenModelContainsValue() {
        let model = createModel(profileImage: nil)
        let name = model.name
        let localizedGreeting = NSLocalizedString("Hello, %@", comment: "User greetings")
        let expected = String(format: localizedGreeting, name)
        expect(model.greatings).to(equal(expected))
    }
    
    // MARK : computed -> email
    func testShouldEmailWhenModelContainsValue() {
        let model = createModel(profileImage: nil)
        let expected = "pedro@unidas.com.br"
        expect(model.email).to(equal(expected))
    }
    
    // MARK : computed -> formattedMobilePhone
    func testShoulFormattedMobilePhoneWhenModelContainsValue() {
        let model = createModel(profileImage: nil)
        let expected = "1199999999"
        expect(model.formattedMobilePhone).to(equal(expected))
    }
    
    // MARK : computed -> profileImage
    func testShoulProfileImageWhenModelContainsValue() {
        let expected =  "file://6C841A84-8430-4897-990B-4B5692ADBA36/Documents/386BD6A4-6B43-4FAB-A29A-363DEC8C8361.png"
        let profileImageURL = URL(string: expected)
        let model = createModel(profileImage: profileImageURL)
        expect(model.profileImage?.absoluteString).to(equal(expected))
    }
    
    func testShoulNilProfileImageWhenModelNotContainsValue() {
        let model = createModel(profileImage: nil)
        expect(model.profileImage).to(beNil())
    }
    
    // MARK : computed -> formattedDocumentNumber
    func testShoulFormattedDocumentNumberWhenModelContainsValue() {
        let expected =  "999.999.999-99"
        let model = createModel(profileImage: nil)
        expect(model.formattedDocumentNumber).to(equal(expected))
    }
}

