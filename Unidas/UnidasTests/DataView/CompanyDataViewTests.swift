//
//  CompanyDataViewTests.swift
//  UnidasTests
//
//  Created by Pedro Felipe Machado on 18/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import Foundation
import Nimble
import XCTest
@testable import Unidas

class CompanyDataViewTests: XCTestCase {
    
    private func createModel() -> CompanyTermsDataView {
        let model = CompanyTerms(id: 1, title: "Title", description: "Description")
        return CompanyTermsDataView(model: model)
    }
    
    // MARK : computed -> "title"
    func testShoudlReturnTitleWhenContainsValue() {
        let model = createModel()
        let expected = "Title"
        expect(model.title).to(equal(expected))
    }
    
    // MARK : computed -> "description"
    func testShoudlReturnDescriptionWhenContainsValue() {
        let model = createModel()
        let expected = "Description"
        expect(model.description).to(equal(expected))
    }
}

