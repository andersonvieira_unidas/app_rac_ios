//
//  StringExtensionTests.swift
//  UnidasTests
//
//  Created by Anderson Vieira on 09/09/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
import Nimble
@testable import Unidas

class StringExtensionTests: XCTestCase {

    // MARK : StringExtension -> removePhoneCharacters
    func testShouldReturnPhoneNumberWhenPhoneIsValid() {
        let phone = "(11) 9999-9999"
        let phoneExpected = "1199999999"
        let phoneFormatted = phone.removePhoneCharacters
        
        expect(phoneExpected).to(equal(phoneFormatted))
    }
    
    func testShouldReturnEmptyPhoneNumberWhenPhoneNumberIsEmpty() {
        let phone = ""
        let phoneExpected = ""
        let phoneFormatted = phone.removePhoneCharacters
        
        expect(phoneExpected).to(equal(phoneFormatted))
    }
    
     // MARK : StringExtension -> isValidCPF
    func testShouldReturnDocumentNumberIsValidWhenDocumentNumberIsValid() {
        let documentNumber = "817.034.110-80"
        expect(documentNumber.isValidCPF).to(beTrue())
    }
    
    func testShouldReturnDocumentNumberIsInvalidWhenDocumentNumberIsInvalid() {
        let documentNumber = "111.222.333-44"
        expect(documentNumber.isValidCPF).to(beFalse())
    }
    
    // MARK : StringExtension -> isValidEmail
    func testShouldReturnEmailIsValidWhenEmailIsValid() {
        let email = "unidas@unidas.com.br"
        expect(email.isValidEmail).to(beTrue())
    }
    
    func testShouldReturnInvalidEmailWhenEmailIsInvalid() {
        let email = "unidasunidas.com.br"
        expect(email.isValidEmail).to(beFalse())
    }
    
    func testShouldReturnEmailIsValidWhenEmailContainsUnderline() {
        let email = "unidas_unidas123_@unidas.com.br"
        expect(email.isValidEmail).to(beTrue())
    }
    
    // MARK : StringExtension -> areaCode
    func testShouldReturnAreaCodeWhenPhoneIsCorrect() {
        let phone = "1199999999"
        let areaExpected = 11
        let areaFormatted = phone.areaCode
        
        expect(areaExpected).to(equal(areaFormatted))
    }
    
    func testShouldReturnAreaCodeNilWhenPhoneInvalid() {
        let phone = "11999"
        let areaFormatted = phone.areaCode

        expect(areaFormatted).to(beNil())
    }
    
    // MARK : StringExtension -> eventParameter
    func testShouldReturnEventFormattedParameterWhenParameterContainsBeginAndEndSpaces() {
        let eventParameter = " event_firebase "
        let eventParameterExpected = "event_firebase"
        let eventFormatted = eventParameter.eventParameter
        
        expect(eventFormatted).to(equal(eventParameterExpected))
    }
    
    func testShouldReturnEventParameterFormattedWhenParameterConstainsSpaces() {
        let eventParameter = "event firebase"
        let eventParameterExpected = "eventfirebase"
        let eventFormatted = eventParameter.eventParameter
        
        expect(eventFormatted).to(equal(eventParameterExpected))
    }
    
    // MARK : StringExtension -> phoneNumber
    func testShouldReturnPhoneNumberWhenPhoneContainsSpecialCharecters() {
        let phone = "(11) 9999-9999"
        let phoneExpected = 99999999
        let phoneFormatted = phone.phoneNumber
        
        expect(phoneExpected).to(equal(phoneFormatted))
    }
    
    func testShouldReturnNilWhenPhoneWhenInvalid() {
        let phone = "(11) 9999-999"
        let phoneFormatted = phone.phoneNumber
    
        expect(phoneFormatted).to(beNil())
    }
    
    // MARK : StringExtension -> isValidPhoneNumber
    func testShouldReturnIsValidWhenResidencialPhoneWhenValid() {
        let phone = "(11) 4444-4444"
        let phoneValid = phone.isValidPhoneNumber
        
        expect(phoneValid).to(beTrue())
    }
    
    func testShouldReturnIsValidWhenResidencialPhoneWhenIsInvalid() {
        let phone = "(11) 999-9999"
        let phoneValid = phone.isValidPhoneNumber
        
        expect(phoneValid).to(beFalse())
    }
    
    // MARK : StringExtension -> removeZipcodeCharacters
    func testShouldReturnZipcodeWhenZipcodeIsValid() {
        let zipcode = "99999-999"
        let zipcodeExpected = "99999999"
        let zipcodeFormatted = zipcode.removeZipcodeCharacters
        
        expect(zipcodeExpected).to(equal(zipcodeFormatted))
    }
    
    func testShouldReturnEmptyZipcodeWhenZipcodeIsEmpty() {
        let zipcode = ""
        let zipcodeExpected = ""
        let zipcodeFormatted = zipcode.removeZipcodeCharacters
        
        expect(zipcodeExpected).to(equal(zipcodeFormatted))
    }
    
    // MARK : StringExtension -> removeDocumentNumberCharacters
    func testShouldReturnDocumentNumberWhenDocumentNumberIsValid() {
        let documentNumber = "817.034.110-80"
        let documentNumberExpected = "81703411080"
        let documentNumberFormatted = documentNumber.removeDocumentNumberCharacters
        
        expect(documentNumberExpected).to(equal(documentNumberFormatted))
    }
    
    func testShouldReturnEmptyDocumentNumberWhenDocumentNumberIsEmpty() {
        let documentNumber = ""
        let documentNumberExpected = ""
        let documentNumberFormatted = documentNumber.removeDocumentNumberCharacters
        
        expect(documentNumberExpected).to(equal(documentNumberFormatted))
    }
    
    // MARK : NumberFormatterExtension -> currencyFormatter
    
    func testShouldReturnCurrencyWhenNumberIsValid() {
        let value = 10
        let expectedValue = "R$ 10,00"
        let formattedValue = NumberFormatter.currencyFormatter.string(from: value as NSNumber)
        
        expect(expectedValue).to(equal(formattedValue))
    }
    
    func testShouldReturnPercentWhenNumberIsValid() {
        let value = 10
        let expectedValue = "1.000%"
        let formattedValue = NumberFormatter.percentFormatter.string(from: value as NSNumber)

        expect(expectedValue).to(equal(formattedValue))
        
    }

    func testShouldReturnDecimalWhenNumberIsValid() {
        let value = 10
        let expectedValue = "10"
        let formattedValue = NumberFormatter.decimalNumberFormatter.string(from: value as NSNumber)

        expect(expectedValue).to(equal(formattedValue))
    }

    func testShouldReturnApiNumberWhenNumberIsValid() {
        let value = 10
        let expectedValue = "10.00"
        let formattedValue = NumberFormatter.apiNumberFormatter.string(from: value as NSNumber)

        expect(expectedValue).to(equal(formattedValue))
   }
   
    // MARK : NumberFormatterExtension -> DateFormatter
    
    func testShouldReturnDateFormatterWhenDateIsValid() {
        let dateStr = "2019-09-11T13:00:00"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:dateStr)!
        
        let expectedValue = "11 de set. de 2019 13:00"
        let formattedValue = DateFormatter.dateFormatter.string(from: date)
        
        expect(expectedValue).to(equal(formattedValue))
    }
    
    func testShouldReturnShortDateFormatterWhenDateIsValid() {
        let dateStr = "2019-09-11T13:00:00"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:dateStr)!
        
        let expectedValue = "11/09/2019 13:00"
        let formattedValue = DateFormatter.shortDateFormatter.string(from: date)

        expect(expectedValue).to(equal(formattedValue))
    }
    
    func testShouldReturnDateOnlyFormatterWhenDateIsValid() {
        let dateStr = "2019-09-11T13:00:00"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:dateStr)!
        
        let expectedValue = "11 de set. de 2019"
        let formattedValue = DateFormatter.dateOnlyFormatter.string(from: date)
        
        expect(expectedValue).to(equal(formattedValue))
    }
    
    func testShouldReturnTimeOnlyFormatterWhenDateIsValid() {
        let dateStr = "2019-09-11T13:00:00"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from:dateStr)!
        
        let expectedValue = "13:00"
        let formattedValue = DateFormatter.timeOnlyFormatter.string(from: date)

        expect(expectedValue).to(equal(formattedValue))
    }
}
