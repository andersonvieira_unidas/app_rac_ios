# App Unidas

Unidas Mobile para iOS

## Descrição

A Unidas é a segunda maior locadora de veículos do Brasil e líder em locação de frotas para empresas, Com o App Unidas, você agora pode alugar, checar seus contratos e navegar por todos os vehiculos disponíveis para locação, visualizar cotações e lojas no Brasil.

## Iniciando

Essas instruções farão com que você tenha uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste.

## Pre Requisitos

É importante ter a versão mais recente do Xcode.

## Arquitetura

Informações sobre arquitetura do projeto App Unidas, pode ser encontrada no Confluence com mais detalhes na seguinte URL: https://unidas.atlassian.net/wiki/spaces/PA/pages/48988242/Arquitetura+-+iOS

## Installing 

```
Clone este repositório, abra o terminal e execute a linha de comando (pod install) dentro da pasta do aplicativo e abra a Unidas.xcworkspace usando o Xcode.
```
## Algumas Telas

![alternativetext](http://tfs.unidas.com.br/tfs/UnidasExterno/Unidas.AppRac/_git/Unidas.AppRac.Ios?path=%2FUnidas%2FScreenshots%2Funidas_home.png&version=GBRelease-1.1.0&_a=contents) 
![alternativetext](http://tfs.unidas.com.br/tfs/UnidasExterno/Unidas.AppRac/_git/Unidas.AppRac.Ios?path=%2FUnidas%2FScreenshots%2Funidas_locacao.png&version=GBRelease-1.1.0&_a=contents)


## Componentes Third-party  

| Plug-ins|
| ------------------- |
|Reusable|
|Alamofire|
|AlamofireImage|
|CodableAlamofire|
|Fabric|
|Crashlytics|
|RMDateSelectionViewController|
|InputMask|
|OneTimePassword|
|YPDrawSignatureView|
|Socket.IO-Client-Swift|
|PhoneNumberKit|
|Cosmos|
|Cluster|
|JWTDecode|
|Firebase/Analytics|
|Firebase/DynamicLinks|
|RemoteConfigClient-Swift|
|lottie-ios|
|ISPageControl|

### Reusable

A Swift mixin to use UITableViewCells, UICollectionViewCells and UIViewControllers in a type-safe way, without the need to manipulate their String-typed reuseIdentifiers. This library also supports arbitrary UIView to be loaded via a XIB using a simple call to loadFromNib().
[For more information access here](https://github.com/onevcat/Kingfisher)

### Alamofire

Alamofire is an HTTP networking library written in Swift.
[For more information access here](https://github.com/Alamofire/Alamofire)

### AlamofireImage

AlamofireImage is an image component library for Alamofire.
[For more information access here](https://github.com/Alamofire/Alamofire)

### CodableAlamofire

An extension for Alamofire that converts JSON data into Decodable objects.
[For more information access here](https://github.com/Otbivnoe/CodableAlamofire)

### Fabric/Crashlytics

Fabric is an  powerful yet lightest weight crash reporting. 
[For more information access here](https://fabric.io/kits/ios/crashlytics/install)

### RMDateSelectionViewController

This is an iOS control for selecting a date using UIDatePicker in an UIAlertController like manner.
[For more information access here](https://github.com/CooperRS/RMDateSelectionViewController)

### InputMask

User input masking library repo.
[For more information access here](https://github.com/RedMadRobot/input-mask-ios)

### OneTimePassword

A small library for generating TOTP and HOTP one-time passwords on iOS.
[For more information access here](https://github.com/mattrubin/OneTimePassword)

### YPDrawSignatureView

Capture signature view in Swift and export it as a vector graphics or bitmap.
[For more information access here](https://github.com/GJNilsen/YPDrawSignatureView)

### Socket.IO-Client-Swift

Socket.io-client
[For more information access here](https://github.com/socketio/socket.io-client-swift)

### PhoneNumberKit

Phone Number Kit. 
[For more information access here](https://github.com/marmelroy/PhoneNumberKit/blob/master/PhoneNumberKit.podspec)

### Cosmos

A star rating control for iOS/tvOS written in Swift.
[For more information access here](https://github.com/evgenyneu/Cosmos)

### Cluster

Easy Map Annotation Clustering
[For more information access here](https://github.com/efremidze/Cluster)

### JWTDecode

A library to help you decode JWTs in Swift.
[For more information access here](https://github.com/auth0/JWTDecode.swift)

### Firebase/Analytics

Google Analytics for Firebase collects usage and behavior data for your app.
[For more information access here](https://firebase.google.com/docs/analytics/ios/start?hl=es-419)

### Firebase/DynamicLinks

Create dynamic links with Firebase.
[For more information access here](https://firebase.google.com/docs/dynamic-links/ios/create)

### RemoteConfigClient-Swift

Remote Configuration by parameters using Firebase Remote Config.
[For more information access here](https://firebase.google.com/docs/remote-config/use-config-ios?hl=es)

### lottie-ios

An iOS library to natively render After Effects vector animations.
[For more information access here](https://github.com/airbnb/lottie-ios)

### ISPageControl

A page control similar to that used in Instagram.
[For more information access here](https://github.com/Interactive-Studio/ISPageControl)

## License

This project is licensed under the MIT License 


