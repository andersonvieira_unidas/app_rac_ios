//
//  TodayViewController.swift
//  Unidas Widget
//
//  Created by Anderson Vieira on 10/06/20.
//  Copyright © 2020 Unidas. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var statusButton: EnhancedButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pickupDateLabel: UILabel!
    @IBOutlet weak var pickupLocationLabel: UILabel!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var expandedStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if (activeDisplayMode == NCWidgetDisplayMode.compact) {
            self.preferredContentSize = maxSize
            expandedStackView.isHidden = true
        }
        else {
            self.preferredContentSize = CGSize(width: 0, height: 140)
            expandedStackView.isHidden = false
        }
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        if  let sharedDefaults = UserDefaults.init(suiteName: "group.br.unidas.apprac.ios"),
            let reservation = sharedDefaults.value(forKey: "reservation"),
            let pickupStore = sharedDefaults.value(forKey: "pickupStore") as? String,
            let pickupDate = sharedDefaults.value(forKey: "pickupDate") as? String {
            
            let decoder = JSONDecoder()
            if let data = reservation as? Data, let reservationStep = try? decoder.decode(ReservationStep.self, from: data) {
   
                titleLabel.text = reservationStep.title
                descriptionLabel.text = reservationStep.description
                statusButton.setImage(statusImage(status: reservationStep.status), for: .normal)
                pickupLocationLabel.text = pickupStore
                pickupDateLabel.text = pickupDate
                
                noDataLabel.isHidden = true
              //  pickupLocationLabel.isHidden = true
               // pickupDateLabel.isHidden = true
               
                completionHandler(NCUpdateResult.newData)
            }
        } else {
            contentStackView.isHidden = true
            noDataLabel.isHidden = false
            completionHandler(NCUpdateResult.noData)
        }
    }
    
    
    private func statusImage(status: [ReservationStatus]) -> UIImage {
        if !status.filter({ $0 == .checkin }).isEmpty{
            return #imageLiteral(resourceName: "icon-qrcode-done")
        }
        
        if !status.filter({ $0 == .paid }).isEmpty {
            return #imageLiteral(resourceName: "icon-payment-done")
        }
        return #imageLiteral(resourceName: "icon-reservation-done")
    }
    
}
