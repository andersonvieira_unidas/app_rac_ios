//
//  UnidasUITests.swift
//  UnidasUITests
//
//  Created by Anderson Vieira on 31/10/19.
//  Copyright © 2019 Unidas. All rights reserved.
//

import XCTest
class UnidasUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    func testShouldExistsButtonNewReserve() {
        let buttonNewReserve = app.otherElements["newReserve"]
        let viewButtonExists = buttonNewReserve.waitForExistence(timeout: 40)
        XCTAssert(viewButtonExists)
        //XCTAssertEqual(app.buttons.element(matching: .button, identifier: "test").title, "Nova reserva")
    }

}
